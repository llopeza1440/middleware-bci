<%@ taglib prefix="fmt" uri="/WEB-INF/fmt.tld"%>
<%@ taglib uri="/tags/struts-bean" prefix="bean"%>
<%@ taglib uri="/tags/struts-logic" prefix="logic"%>
<jsp:useBean id="sessionBci" scope="session" class="wcorp.model.seguridad.SessionBCI"></jsp:useBean>
<jsp:setProperty name="sessionBci" property="*" />

<html>
	<head>
		<title>BCI</title>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="description" content="">
		<meta name="viewport" content="width=device-width,initial-scale=1">
		<link href="/bcinetwls/common/css/frames.css" rel="stylesheet" type="text/css">
		<script type="text/javascript" src="/bcinetwls/common/js/bcipassOffline.js"></script>
		<script type="text/javascript" src="/bcinetwls/common/js/jquery.js"></script>
	</head>
	
	<body onLoad="javascript:inicializar();cargaQRInicial();">

		<div class="entrust_container dos">
			<form name="PagueDirectoForm" id="form" action="PagoDirecto.do?metodo=validaEntrustSoftTokenOffline">
				<input type="hidden" name="canal" value="entrustSoftToken" />
				<input type="hidden" name="metodo" value="validaEntrustSoftTokenOffline" />
				<input type="hidden" name="servicioQR" id="servicioQR" value="<bean:write name="servicioQR"/>" />

				<logic:present name="imagenesqr">
					<p class="parrafo2"><bean:message key="entrustSoftToken.contenido.textoPrincipalOffline" /></p>
					<div class="col qr">
						<img id="imgQr">
					</div>

					<div class="col form">
						<div class="desc"><bean:message key="entrustSoftToken.contenido.textoComboBox" /></div>

						<p>
							<select name="codigosqr" class="input" onchange="mostrarImagenQr(this.value);">
								<logic:iterate name="imagenesqr" id="item" indexId="indice">
									<option value="<bean:write name="item" property="claveAlfanumerica"/>" <logic:equal name="indice" value="0">selected</logic:equal>><bean:write name="item" property="nombreDispositivoAsociado"/> </option>
								</logic:iterate>
							</select>
						</p>
						<p>
							<input type="password" id="codigoConfirmacionQR" name="codigoConfirmacionQR" placeholder="<bean:message key="entrustSoftToken.contenido.placeholder" />" class="input" maxlength="8">
						</p>

						<div class="paso"><bean:message key="entrustSoftToken.contenido.cantCaracteres" /></div>

					</div>

					<button id="aceptar" class="aceptar disabled" disabled><bean:message key="entrustSoftToken.boton.aceptar" /></button>

					<div id="loading"></div>

				</logic:present>
					
				<logic:notPresent name="imagenesqr">
					<div id="error" class="alert hide alert-danger">
						<i class="fa fa-times" aria-hidden="true"></i>
						<p>
							<strong><bean:message key="entrustSoftToken.error.titulo" /></strong>
							<br>
							<bean:message key="entrustSoftToken.error.imagenQr" />
						</p>
					</div>
				</logic:notPresent>
			</form>
		</div>

	</body>
</html>

<%--
/**************************************************************************************************************************
* Archivo:     autenticacionEntrustSoftTokenOffline.jsp
* Descripci�n: Se utiliza para la autenticaci�n Entrust SoftToken Offline.
* Path:        /bcinetwls/paguedirecto/monex/entrustsofttoken
* " Todos los derechos reservados por Banco de Cr�dito e Inversiones."              
* Historia Cambios
* -------------------------
* versi�n       fecha         autor                                                       cambios
* =======   ==========  ==========================================================        =================================
* 1.0       02-08-2017  Marcelo Fuentes (SEnTRA) - Ricardo Carrasco (Ing. Soft. BCI)      versi�n inicial. Ser� modificada.
*
**************************************************************************************************************************/
--%>
