package cl.bci.aplicaciones.seguridad.autenticacion.ws;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.oltu.oauth2.common.exception.OAuthProblemException;

import wcorp.model.seguridad.RepresentacionSegundaClave;
import wcorp.serv.seguridad.SeguridadException;
import wcorp.util.FechasUtil;
import wcorp.util.StringUtil;
import wcorp.util.TablaValores;

import cl.bci.aplicaciones.seguridad.autenticacion.mb.SegundaClaveMB;
import cl.bci.aplicaciones.seguridad.autenticacion.mb.SegundaClaveModelMB;
import cl.bci.aplicaciones.seguridad.autenticacion.ws.to.EstadoSegundaClaveWSTO;
import cl.bci.aplicaciones.seguridad.autenticacion.ws.to.GeneracionLlaveWSTO;
import cl.bci.aplicaciones.seguridad.autenticacion.ws.to.OpcionalidadSegundaClaveWSTO;
import cl.bci.aplicaciones.seguridad.autenticacion.ws.to.ValidacionSegundoFactorWSTO;
import cl.bci.aplicaciones.seguridad.segundaclave.configuracion.mb.OpcionalidadSegundaClaveBackingMB;
import cl.bci.infraestructura.web.seguridad.segundaclave.mb.JerarquiaClavesModelMB;
import cl.bci.infraestructura.web.seguridad.segundaclave.to.CamposDeLlaveTO;
import cl.bci.infraestructura.web.seguridad.segundaclave.to.EstadoSegundaClaveTO;
import cl.bci.infraestructura.web.seguridad.segundaclave.to.ItemDeLlaveTO;
import cl.bci.infraestructura.web.seguridad.segundaclave.to.SegundaClaveEnum;
import cl.bci.infraestructura.ws.ServicioRestWS;
import cl.bci.infraestructura.ws.excepcion.NegocioException;
import cl.bci.infraestructura.ws.excepcion.SistemaException;
import cl.bci.infraestructura.ws.to.RespuestaSegundaClaveWSTO;
import cl.bci.infraestructura.ws.to.RespuestaSimpleWSTO;
import cl.bci.infraestructura.ws.util.MessageBundleUtil;


/**
 * <b>SegundaClaveWS</b>
 * <br>
 * WS encargado de procesar las consultas de segunda clave para un usuario.
 * En este servicio se exponen los metodos de consulta de estado de segunda clave
 * y la generación de la llave para una transacción.
 * La autenticación de la segunda clave se validará en cada servicio
 * donde se realice la transacción.
 * <br>
 * Registro de versiones:
 * <ul>
 * <li>1.0, 05/06/2013 Yon Sing Sius (ExperimentoSocial) : versión inicial.</li>
 * <li>1.1, 11/08/2015 Luis Silva (TINet); Francisco González V. (SEnTRA): se agrega los métodos:
 * {@link #obtenerEstadoYLlave(GeneracionLlaveWSTO)}
 * {@link #configurarOpcionalidadSegundaClave(boolean)}
 * {@link #consultarOpcionalidadSegundaClave()}
 * {@link #getLogger()}</li>
 * <li>1.2, 28/10/2015 Ignacio González (TINet): Se modifica método
 * {@link #obtenerEstadoYLlave(GeneracionLlaveWSTO)}</li>
 * <li>1.3, 01/03/2016 Rafael Pizarro (TINet) - Juan Bustos (Ing. Soft. BCI): Se modifica método:
 * {@link #obtenerEstadoYLlave(GeneracionLlaveWSTO)}</li>
 * <li>1.4, 27/04/2016 Victor Enero (Orand) - Pablo Rompentin (Ing. Soft. BCI):se agrega el método
 *  {@link #validarSegundoFactor(ValidacionSegundoFactorWSTO)} y se modifica los métodos
 *  {@link #estadoSegundaClave(String)} y {@link #generarLlave(GeneracionLlaveWSTO)} para agregar 2da clave
 *  para autenticación Oauth.</li>
 * <li>1.5 19/06/2017 Felipe Carvajal (Imagemaker) - Mario Riffo (Ing.Soft.BCI) : Se modifica m�todo:
 * {@link #configurarOpcionalidadSegundaClave(Boolean)} se realizan cambios 
 *  para validar el multipass antes de configurar.</li> 
 *  <li>1.6 10/07/2017 Luis L�pez Alamos (SEnTRA) - Ricardo Carrasco C�ceres (Ing. Soft. BCI): Se agrega metodo {@link #consultarEstadoOperacion()}.</li>
 *  </ul>
 * <p><b>Todos los derechos reservados por Banco de Credito e Inversiones.</b></p>
 */

@Path("/segundaclave")
public class SegundaClaveWS extends ServicioRestWS {

	/**
	 * Numero version.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * bundle por defecto.
	 */
	private static final String BUNDLE_DEFAULT =
			"cl.bci.aplicaciones.seguridad.autenticacion.loginPersonas";

	/**
	 * bundle por defecto.
	 */
	private static final String MSJE_SEGUNDA_CLAVE =
			"cl.bci.infraestructura.web.seguridad.segundaclave.segundaClave";

	/**
	 * Error de permiso denegado.
	 */
	private static final int ERROR_PERMISO_DENEGADO = 401;

	/**
	 * Mensaje de usuario para cuando no tiene activado segunda clave.
	 */
	private static final String ERROR_SIN_SEGUNDA_CLAVE = "errorSinSegundaClave";

	/**
	 * Tabla de parámetros con configuraciones relacionadas con segundas claves.
	 */
	private static final String SEGUNDA_CLAVE_PARAMETROS = "SegundaClave.parametros";

	/**
	 * Constante de segunda clave multipass.
	 */
	private static final String MULTIPASS = "MULTIPASS";

	/**
	 * Constante de segunda clave safe signer.
	 */
	private static final String SAFE_SIGNER = "SAFESIGNER";

	/**
	 * Constante para valor vacio.
	 */
	private static final String VACIO = "";

	/**
	 * Id de NO OK.
	 */
	private static final int IDNOOK = -1;

	/**
	 * Constante de segunda clave para indicar que sí se requiere validar segunda clave.
	 */
	private static final String REQUIERE_SEGUNDA_CLAVE = "si";

	/**
	 * Constante de segunda clave para indicar que no se requiere validar segunda clave.
	 */
	private static final String NO_REQUIERE_SEGUNDA_CLAVE = "no";

	/**
	 * Variable para asignar log de ejecución.
	 */
	private static Logger log = (Logger)Logger.getLogger(SegundaClaveWS.class);
	
    /**
     * Nombre de la variable de seguimiento para seteo y obtencion del dato
     */
    private static final String  VERIFICA_SEGUNDA_CLAVE = "verificaSegundaClave";
    
    /** 
     * clave del mensaje de configuracion no valida. 
     */
    private static final String CONFIGURACION_INVALIDA = "configuracionInvalida";

	/**
	 * Request.
	 */
	@Context private HttpServletRequest servletRequest;

	/**
	 * Response para obtener sesion.
	 */
	@Context
	private HttpServletResponse servletResponse;

	/**
	 * Consulta de estado segunda clave para un servicio determinado.
	 * La lista de nombres de servicios posibles se pueden consultar en la tabla
	 * de parametros "SegundaClave.parametros".
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 05/06/2013 Yon Sing Sius (ExperimentoSocial): Versión inicial.</li>
	 * <li>1.1 27/04/2016 Victor Enero (Orand) - Pablo Rompentin (Ing. Soft. BCI) : En el metodo segundaClaveMB.estadoSegundaClave
	 * se pasa como parametro HttpServletRequest para obtener el rut desde la sessionBCI o desde un token OAuth,
	 * Se quita el log que loggea el rut del cliente y se coloca un if preguntando por el id del codigo del estado para saber
	 * si hubo error en la consulta.
	 *</li>
	 * </ul>
	 * <p>
	 *
	 * @param servicio para el cual se consultará por la segunda clave
	 * @since 1.0
	 * @return estado de la segunda clave
	 * @throws IOException Excepcion
	 */
	@POST
	@Path("estado")
	@Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
	@Consumes(MediaType.APPLICATION_JSON)
	public EstadoSegundaClaveWSTO estadoSegundaClave(String servicio) throws IOException {

		if (log.isEnabledFor(Level.DEBUG))
			log.debug("[estadoSegundaClave] [BCI_INI]");

		SegundaClaveMB segundaClaveMB = (SegundaClaveMB)getManagedBean("segundaClaveMB");

		if (servicio == null || servicio.trim().length() == 0)
			throw new SistemaException("Servicio no puede ser nulo");

		EstadoSegundaClaveWSTO estadoWS = new EstadoSegundaClaveWSTO();

		try {
			segundaClaveMB.setServicio(servicio.trim());
			EstadoSegundaClaveTO segundaClave = segundaClaveMB.estadoSegundaClave(servletRequest);

			estadoWS.setStatus(segundaClave.isEstatus());

			if (segundaClave.getIdCodigoEstado() != IDNOOK) {
				estadoWS.setNombre(segundaClave.getSegundaClaveEnum().getNombre());
				estadoWS.setRequiereLlave(segundaClave.getSegundaClaveEnum().isRequiereLlave());

			}
			estadoWS.setGlosa(segundaClave.getGlosa());

		}
		catch (SeguridadException e) {

			if (log.isEnabledFor(Level.ERROR))
				log.error("[estadoSegundaClave] [BCI_FINEX] SeguridadException: " + e,e);

			if (e.getCodigo() == "SAFESIGNER05") {
				String mensajeError = MessageBundleUtil.getMensajeBundle(BUNDLE_DEFAULT, ERROR_SIN_SEGUNDA_CLAVE);
				throw new NegocioException(mensajeError);
			}

			if (log.isEnabledFor(Level.ERROR))
				log.error("[estadoSegundaClave] [BCI_FINEX] SeguridadException: " + e);

			throw new SistemaException("Error al obtener segunda clave para servicio '" + servicio + "'");
		}
		catch (OAuthProblemException ex) {
			if (log.isEnabledFor(Level.ERROR))
				log.error("[estadoSegundaClave] OAuthProblemException: " ,ex);
			servletResponse.sendError(ERROR_PERMISO_DENEGADO);
			return null;
		}
		catch (Exception e) {
			if (log.isEnabledFor(Level.ERROR))
				log.error("[estadoSegundaClave] [BCI_FINEX] Exception: " + e,e);
			throw new SistemaException("Error en servicio");
		}

		if (log.isEnabledFor(Level.ERROR))
			log.error("[estadoSegundaClave] [BCI_FINOK] Segunda Clave: " + estadoWS);

		return estadoWS;
	}

	/**
	 * Consulta el estado de segunda clave para una lista de servicios.
	 * La lista de nombres de servicios posibles se pueden consultar en la tabla
	 * de parametros "SegundaClave.parametros".
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 05/06/2013 Yon Sing Sius (ExperimentoSocial): Versión inicial.</li>
	 * </ul>
	 * <p>
	 *
	 * @param servicios para los cuales se quiere consultar por la segunda clave
	 * @since 1.0
	 * @return Tabla hash con los estados de segunda clave por servicio
	 */
	@POST
	@Path("estadosSegundaClavePorServicios")
	@Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
	@Consumes(MediaType.APPLICATION_JSON)
	public HashMap<String, EstadoSegundaClaveWSTO> estadosSegundaClavePorServicios(String[] servicios) {


		log.debug("[estadosSegundaClavePorServicios] inicio");
		if (servicios == null || servicios.length == 0) {
			log.error("[estadosSegundaClavePorServicios] Lista de servicios es nulo");
			throw new SistemaException("Servicios no puede ser nulo");
		}

		HashMap<String, EstadoSegundaClaveWSTO> estadosWS = new HashMap<String, EstadoSegundaClaveWSTO>();
		SegundaClaveMB segundaClaveMB = (SegundaClaveMB)getManagedBean("segundaClaveMB");

		try {
			HashMap<String, EstadoSegundaClaveTO> estados =
					segundaClaveMB.estadosSegundaClaveParaServicios(servicios);
			Iterator<String> it = estados.keySet().iterator();
			while(it.hasNext()) {
				String key = it.next();
				EstadoSegundaClaveWSTO est = new EstadoSegundaClaveWSTO();
				EstadoSegundaClaveTO e = estados.get(key);
				if (e.isEstatus()) {
					est.setNombre(e.getSegundaClaveEnum().getNombre());
					est.setRequiereLlave(e.getSegundaClaveEnum().isRequiereLlave());
				}
				est.setStatus(e.isEstatus());
				est.setGlosa(e.getGlosa());
				estadosWS.put(key, est);
			}
		}
		catch (SeguridadException e) {
			log.error("[estadosSegundaClavePorServicios] SeguridadException: " + e);
			throw new SistemaException("Error al obtener segunda clave para los servicios solicitados"
					+ ", comprobar que los nombre de los servicios sean correctos");

		}
		log.debug("[estadosSegundaClavePorServicios] retorno: " + estadosWS);
		return estadosWS;
	}


	/**
	 * Genera una llave de seguridad dados los datos de una transacción.
	 * Esta llave luego es utilizada por la aplicación de safesigner para
	 * generar la segunda clave.
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 05/06/2013 Yon Sing Sius (ExperimentoSocial): Versión inicial.</li>
	 * <li>1.1 27/04/2016 Victor Enero (Orand) - Pablo Rompentin (Ing. Soft. BCI) : En la componente
	 * SegundaClaveMB se traspasa el parametro HttpServletRequest para luego obtener el rut del usuario.
	 * </li>
	 * </ul>
	 * <p>
	 *
	 * @param datosLlave de la transacción a firmar
	 * @since 1.0
	 * @return Tabla hash con los estados de segunda clave por servicio
	 * @throws IOException Excepcion
	 */
	@POST
	@Path("generarLlave")
	@Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
	@Consumes(MediaType.APPLICATION_JSON)
	public RespuestaSimpleWSTO<String> generarLlave(GeneracionLlaveWSTO datosLlave) throws IOException {

		if (log.isEnabledFor(Level.DEBUG))
			log.debug("[generarLlave] [BCI_INI] datosLlave: " + datosLlave);

		SegundaClaveMB segundaClaveMB = (SegundaClaveMB)getManagedBean("segundaClaveMB");
		segundaClaveMB.setServicio(datosLlave.getServicio());
		segundaClaveMB.setServletRequest(servletRequest);

		CamposDeLlaveTO campos = new CamposDeLlaveTO();
		int posicion = 0;
		for(String key: datosLlave.getCamposLlaves().keySet()) {
			ItemDeLlaveTO item = new ItemDeLlaveTO();
			item.setPosicion(posicion);
			item.setNombre(key);
			item.setValorString(datosLlave.getCamposLlaves().get(key));
			campos.agregarCampo(item);
			posicion += 1;
		}

		if (log.isEnabledFor(Level.DEBUG))
			log.debug("[generarLlave] campos: " + campos);

		RepresentacionSegundaClave segundaClave = null;

		try {
			segundaClave = segundaClaveMB.generarLlave(campos);
		}
		catch (SeguridadException e) {
			if (e.getCodigo() == "SAFESIGNER05") {

				if (log.isEnabledFor(Level.ERROR))
					log.error("[generarLlave] [BCI_FINEX] Error segunda clave disponible para servicio: "
							+ datosLlave.getServicio());
			}

			if (log.isEnabledFor(Level.ERROR))
				log.error("[generarLlave] [BCI_FINEX] SeguridadException: " + e);

			throw new SistemaException("Error al generar llave");
		}

		if (log.isEnabledFor(Level.DEBUG)) {
			log.debug("[generarLlave] segundaClave: '" + segundaClave + "'");
			log.debug("[generarLlave] segundaClave.getTipoDeClave(): '" + segundaClave.getTipoDeClave() + "'");
		}

		String llave = segundaClave.getClaveAlfanumerica();

		if (log.isEnabledFor(Level.DEBUG))
			log.debug("[generarLlave] [BCI_FINOK]  llave: '" + llave + "'");

		RespuestaSimpleWSTO<String> respuesta = new RespuestaSimpleWSTO<String>();
		respuesta.setValor(llave);
		return respuesta;
	}

	/**
	 * Servicio que permite, a partir de un servicio y un conjunto de campos llave determinar qué método de
	 * segunda clave se debe utilizar para el servicio junto con el valor  de llave, si el método así lo
	 * especifica.<br>
	 * <strong>IMPORTANTE</strong>: Sólo ciertos servicios tienen permitido generar llave mediante este servicio.
	 * La forma de determinar esto es por medio de la tabla de parámetros <code>SegundaClave.parametros</code>
	 * <p>
	 * Registro de versiones:<ul>
	 * <li>1.0 11/08/2015 Luis Silva (TINet): Versión inicial.
	 * <li>1.1 28/10/2015 Ignacio González (TINet): Se modifica forma para obtener el estado y llave usando
	 * para ello la managed bean SegundaClaveMB</li>
	 * <li>1.2 01/03/2016 Rafael Pizarro (TINet) - Juan Bustos (Ing. Soft. BCI): Se agrega validacion para
	 * determinar si la clave que se esta cargando corresponde a Actualizacion de Renta (campo fechaHora), de esta
	 * manera se setea valor de la fecha actual.</li>
	 * </ul><p>
	 * @param datosConsulta Datos necesarios para ejecutar la operación (servicio y campos de llave)
	 * @return Método de segudna clave junto con llave.
	 * @since 1.1
	 */
	@POST
	@Path("estadoLlave")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
	public RespuestaSegundaClaveWSTO obtenerEstadoYLlave(GeneracionLlaveWSTO datosConsulta) {
		try {
			String servicio = datosConsulta.getServicio();
			if (getLogger().isEnabledFor(Level.INFO)) {
				getLogger().info("[obtenerEstadoYLlave][BCI_INI] servicio[" + servicio + "]");
			}
			if (servicio == null || servicio.trim().length() == 0) {
				getLogger().error("[obtenerEstadoYLlave] No viene establecido el identificador de servicio");
				throw new NegocioException("Se debe especificar el identificador de servicio");
			}
			if (getLogger().isDebugEnabled()) {
				getLogger().debug("[obtenerEstadoYLlave] Se verifica si servicio se encuentra configurado");
			}

			SegundaClaveMB segundaClaveMB = (SegundaClaveMB) getManagedBean("segundaClaveMB");
			segundaClaveMB.setServicio(servicio);
			EstadoSegundaClaveTO estadoSegundaClave = segundaClaveMB.estadoSegundaClave();

			if (estadoSegundaClave.getIdCodigoEstado() == -1) {
				getLogger().error("[obtenerEstadoYLlave][NegocioException]"
						+ MessageBundleUtil.getMensajeBundle(MSJE_SEGUNDA_CLAVE, "msjeNoSegundaClave"));
				throw new NegocioException(MessageBundleUtil.getMensajeBundle(MSJE_SEGUNDA_CLAVE,
						"msjeNoSegundaClave"));
			}

			if (getLogger().isDebugEnabled()) {
				getLogger().debug(
						"[obtenerEstadoYLlave] Se verifica si servicio puede generar llave mediante este servicio");
			}
			boolean permitido = Boolean.parseBoolean(TablaValores.getValor(SEGUNDA_CLAVE_PARAMETROS,
					"GENERACION_LLAVE_REST_DIRECTO", servicio));
			if (!permitido) {
				getLogger().error("[obtenerEstadoYLlave] Servicio no tiene permitido generar llave por esta via");
				throw new SeguridadException("AUTORIZA");
			}

			if (getLogger().isDebugEnabled()) {
				getLogger().debug("[obtenerEstadoYLlave] estadoSegundaClave [" + estadoSegundaClave + "]");
			}
			SegundaClaveEnum metodoSegundaClave = estadoSegundaClave.getSegundaClaveEnum();

			RespuestaSegundaClaveWSTO respuesta = new RespuestaSegundaClaveWSTO();
			respuesta.setMetodoSegundaClave(metodoSegundaClave.getNombre());
			respuesta.setRequiereLlave(metodoSegundaClave.isRequiereLlave());

			if (metodoSegundaClave.isRequiereLlave()) {
				if (getLogger().isDebugEnabled()) {
					getLogger().debug("[obtenerEstadoYLlave] Se va a generar llave para servicio ["
							+ servicio + "]");
				}
				CamposDeLlaveTO campos = new CamposDeLlaveTO();
				int posicion = 0;
				for (String key : datosConsulta.getCamposLlaves().keySet()) {
					ItemDeLlaveTO item = new ItemDeLlaveTO();
					item.setPosicion(posicion);
					item.setNombre(key);
					if (key.equals("fechaHora")) {
						Date fechaActual = new Date();
						SegundaClaveModelMB segundaClaveModelMB = (SegundaClaveModelMB) this
								.getManagedBean("segundaClaveModelMB");
						segundaClaveModelMB.setFechaOperacion(fechaActual);
						item.setValorString(FechasUtil.convierteDateAString(fechaActual, "yyyyMMddHHmm"));
					}
					else {
						item.setValorString(datosConsulta.getCamposLlaves().get(key));
					}
					campos.agregarCampo(item);
					posicion += 1;
				}

				if (getLogger().isDebugEnabled()) {
					getLogger().debug("[obtenerEstadoYLlave] campos: " + campos);
				}
				RepresentacionSegundaClave repSegundaClave = segundaClaveMB.generarLlave(campos);
				if (repSegundaClave == null || StringUtil.esVacio(repSegundaClave.getClaveAlfanumerica())) {
					getLogger().error("[obtenerEstadoYLlave] No hay llave generada");
					throw new SeguridadException("SERV");
				}
				respuesta.setLlave(repSegundaClave.getClaveAlfanumerica());
			}
			if (getLogger().isEnabledFor(Level.INFO)) {
				getLogger().info("[obtenerEstadoYLlave][BCI_FINOK] respuesta [" + respuesta + "]");
			}
			return respuesta;
		}
		catch (NegocioException ne) {
			getLogger().error(
					"[obtenerEstadoYLlave][BCI_FINEX][NegocioException] Error al consultar estado y llave, mensaje=<"
							+ ne.getMessage() + ">", ne);
			throw ne;
		}
		catch (SeguridadException se) {
			getLogger().error(
					"[obtenerEstadoYLlave][BCI_FINEX][SeguridadException] Error al consultar estado y llave, mensaje=<"
							+ se.getMessage() + ">", se);
			if (se.getCodigo().equals("SERV")) {
				throw new SistemaException("");
			}
			else {
				throw new NegocioException(se.getInfoAdic());
			}
		}
		catch (Exception e) {
			getLogger().error(
					"[obtenerEstadoYLlave][BCI_FINEX][Exception] Error al consultar estado y llave, mensaje=<"
							+ e.getMessage() + ">", e);
			throw new SistemaException(e.getMessage());
		}
	}

	/**
	 * Permite consultar la configuración de la opcionalidad de la segunda clave para el login.
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 11/08/2015 Francisco González V. (SEnTRA): Versión inicial.</li>
	 * </ul>
	 * <p>
	 *
	 * @return Configuración de la opcionalidad obtenida.
	 * @since 1.1
	 */
	@POST
	@Path("/configuracion")
	@Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
	public OpcionalidadSegundaClaveWSTO consultarOpcionalidadSegundaClave(){
		getLogger().info("[consultarOpcionalidadSegundaClave] [BCI_INI]");
		OpcionalidadSegundaClaveBackingMB bean =
				(OpcionalidadSegundaClaveBackingMB) getManagedBean("opcionalidadSegundaClaveBackingMB");
		Boolean opcInicial = bean.getOpcionInicial();
		if(bean.isError()){
			if(getLogger().isEnabledFor(Level.ERROR)){
				getLogger().error("[consultarOpcionalidadSegundaClave] opcInicial: " + opcInicial);
				getLogger().error("[consultarOpcionalidadSegundaClave] [BCI_FINEX] Error al obtener"
						+ "opcionalidad de segunda clave: " + bean.getMensajeError());
			}
			throw new NegocioException(bean.getMensajeError());
		}
		OpcionalidadSegundaClaveWSTO opc = new OpcionalidadSegundaClaveWSTO();
		opc.setRequeridoAutentificacion(opcInicial);
		opc.setMetodoSegundaClave(VACIO);
		if(getLogger().isDebugEnabled()){
			getLogger().debug("[consultarOpcionalidadSegundaClave] requeridoAutentificacion: "
					+ opc.isRequeridoAutentificacion());
		}
		if(bean.isMultipass()){
			getLogger().debug("[consultarOpcionalidadSegundaClave] es Multipass");
			opc.setMetodoSegundaClave(MULTIPASS);
		}
		else if(bean.isMultipassMovil()){
			getLogger().debug("[consultarOpcionalidadSegundaClave] es SafeSigner");
			opc.setMetodoSegundaClave(SAFE_SIGNER);
		}
		else{
			if(getLogger().isEnabledFor(Level.ERROR)){
				getLogger().error("[consultarOpcionalidadSegundaClave] [BCI_FINEX] No fue posible"
						+ "determinar metodo de autenticacion requerido: " + bean.getMensajeError());
			}
			throw new NegocioException(bean.getMensajeError());
		}
		getLogger().info("[consultarOpcionalidadSegundaClave] [BCI_FINOK]");
		return opc;
	}

	/**
	 * Permite configurar la opcionalidad de la segunda clave para el login.
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 11/08/2015 Francisco Gonzalez V. (SEnTRA): Version inicial.</li>
	 * <li>1.1 19/06/2017 Felipe Carvajal (Imagemaker) - Mario Riffo (Ing.Soft.BCI) : 
	 * se realizan cambios para validar la variable de seguimiento </li> 
	 * </ul>
	 * <p>
	 * @param requeridoAutentificacion Indicador si se quiere configurar para que requiera
	 *                                 la segunda clave en el login (true) o para que no se
	 *                                 requiera (false).
	 * @since 1.1
	 */
	@PUT
	@Path("/configuracion")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public void configurarOpcionalidadSegundaClave(
			@FormParam("requeridoAutentificacion") boolean requeridoAutentificacion){
		getLogger().info("[configurarOpcionalidadSegundaClave] [BCI_INI]");

		OpcionalidadSegundaClaveBackingMB bean =
				(OpcionalidadSegundaClaveBackingMB) getManagedBean("opcionalidadSegundaClaveBackingMB");

		if(getLogger().isDebugEnabled()){
			getLogger().debug("[configurarOpcionalidadSegundaClave] requeridoAutentificacion: "
					+ requeridoAutentificacion);
		}
		
		Boolean verificaSegundaClave = (Boolean)obtenerAtributo(VERIFICA_SEGUNDA_CLAVE);
		         
        if (getLogger().isEnabledFor(Level.INFO)) {
            getLogger().info("[configurarOpcionalidadSegundaClave] " 
            		+ "Validando variable de seguimiento de segunda clave ::"+verificaSegundaClave);
        }

        if (verificaSegundaClave != null && !verificaSegundaClave.booleanValue()){
        	throw new NegocioException(MessageBundleUtil.getMensajeBundle(BUNDLE_DEFAULT, CONFIGURACION_INVALIDA));
        }
		
		if(requeridoAutentificacion){
			bean.setOpcionSolicitaClave(REQUIERE_SEGUNDA_CLAVE);
		}
		else{
			bean.setOpcionSolicitaClave(NO_REQUIERE_SEGUNDA_CLAVE);
		}

		bean.configurarOpcionalidad();
		if(bean.isError()){
			if(getLogger().isEnabledFor(Level.ERROR)){
				getLogger().error("[configurarOpcionalidadSegundaClave] [BCI_FINEX] No fue posible configurar "
						+ "metodo de autenticacion requerido: " + bean.getMensajeError());
			}
			throw new NegocioException(bean.getMensajeError());
		}
		getLogger().info("[configurarOpcionalidadSegundaClave] [BCI_FINOK]");
	}


	/**
	 * Genera una llave de seguridad dados los datos de una transaccion.
	 * Esta llave luego es utilizada por la aplicacion de safesigner para
	 * generar la segunda clave.
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 14/04/2016 Victor Enero (Orand) - Pablo Rompentin (Ing. Soft. BCI): Version inicial.</li>
	 * <li>1.1 19/06/2017 Felipe Carvajal (Imagemaker) - Mario Riffo (Ing.Soft.BCI) : se realizan cambios 
	 *  para setear </li> 
	 *  </ul>
	 * <p>
	 *
	 * @param datosSegundoFactor de la transacción a firmar
	 * @since 1.0
	 * @return Tabla hash con los estados de segunda clave por servicio
	 * @throws IOException Excepcion
	 */
	@POST
	@Path("validarSegundoFactor")
	@Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
	@Consumes(MediaType.APPLICATION_JSON)
	public RespuestaSimpleWSTO<String> validarSegundoFactor(ValidacionSegundoFactorWSTO datosSegundoFactor) throws IOException {

		if (log.isEnabledFor(Level.DEBUG))
			log.debug("[validarSegundoFactor] [BCI_INI]");

		if (log.isEnabledFor(Level.INFO))
			log.debug("[validarSegundoFactor] datosLlave: " + datosSegundoFactor);

		SegundaClaveMB segundaClaveMB = (SegundaClaveMB)getManagedBean("segundaClaveMB");
		segundaClaveMB.setServicio(datosSegundoFactor.getServicio());
		segundaClaveMB.setServletRequest(servletRequest);

		CamposDeLlaveTO campos = new CamposDeLlaveTO();
		int posicion = 0;
		for(String key: datosSegundoFactor.getCamposLlaves().keySet()) {
			ItemDeLlaveTO item = new ItemDeLlaveTO();
			item.setPosicion(posicion);
			item.setNombre(key);
			item.setValorString(datosSegundoFactor.getCamposLlaves().get(key));
			campos.agregarCampo(item);
			posicion += 1;
		}

		if (log.isEnabledFor(Level.INFO))
			log.info("[validarSegundoFactor] campos: " + campos);

		try {
			segundaClaveMB.validarSegundaClave(datosSegundoFactor.getSegundoFactor(), campos);
			RespuestaSimpleWSTO<String> respuesta = new RespuestaSimpleWSTO<String>();
			respuesta.setValor("Exito");

			if (log.isEnabledFor(Level.DEBUG))
				log.debug("[validarSegundoFactor] [BCI_FINOK]");
			
			 guardarAtributo(VERIFICA_SEGUNDA_CLAVE, new Boolean(true));
			
			return respuesta;
		}
		catch (SeguridadException e) {
			if (log.isEnabledFor(Level.ERROR))
				log.error("[validarSegundoFactor] [BCI_FINEX] SeguridadException: " + e);

			RespuestaSimpleWSTO<String> respuesta = new RespuestaSimpleWSTO<String>();
			respuesta.setValor(e.getSimpleMessage());
			return respuesta;
		}
	}
	
	 /**
     * Consulta el estado de una operaci�n para resolver una transacci�n en Entrust. 
     * Produce un texto plano, porque la respuesta es solo un string con el resultado..
     * <p>
     * 
     * Registro de Versiones:
     * <ul>
     * <li>1.0 10/07/2017 Luis L�pez Alamos (SEnTRA) - Ricardo Carrasco C�ceres (Ing. Soft. BCI): Versi�n Inicial.</li>
     * </ul>
     * <P>
     * @return String con el resultado de la consulta.
     * @since 1.0
     */
    @POST
    @Path("consultarEstadoOperacion")
    @Produces("text/plain")
    public String consultarEstadoOperacion(){
        if (getLogger().isEnabledFor(Level.INFO)) {
            getLogger().info("[consultarEstadoOperacion][BCI_INI]");
        }
        JerarquiaClavesModelMB jerarquiaBci = (JerarquiaClavesModelMB) getManagedBean("jerarquiaClavesModelMB");
        String resultado = jerarquiaBci.consultarEstadoDesafioOperacion();
        if (getLogger().isEnabledFor(Level.INFO)) {
            getLogger().info("[consultarEstadoOperacion] [resultado =" + resultado + "] [BCI_FINOK]");
        }
        return resultado;
    }

	/**
	 * Método que obtiene el log.
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 11/08/2015 Francisco González V. (SEnTRA): Versión inicial.</li>
	 * </ul>
	 * <p>
	 *
	 * @return log.
	 * @since 1.1
	 */
	public Logger getLogger(){
		if(log == null){
			log = Logger.getLogger(SegundaClaveWS.class);
		}
		return log;
	}

}
