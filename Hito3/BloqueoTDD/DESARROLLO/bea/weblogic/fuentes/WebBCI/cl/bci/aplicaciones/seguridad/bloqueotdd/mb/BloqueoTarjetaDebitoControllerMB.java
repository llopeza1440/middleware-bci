package cl.bci.aplicaciones.seguridad.bloqueotdd.mb;

import java.io.Serializable;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.primefaces.event.FlowEvent;

import wcorp.serv.seguridad.SeguridadException;
import wcorp.serv.tarjetas.TarjetasDelegate;
import wcorp.util.TablaValores;
import wcorp.util.journal.Eventos;

import cl.bci.aplicaciones.cliente.mb.ClienteMB;
import cl.bci.aplicaciones.utilitarios.iconosinteligentes.mb.IconoInteligenteUtilityMB;
import cl.bci.infraestructura.web.journal.mb.JournalUtilityMB;
import cl.bci.infraestructura.web.seguridad.mb.SesionMB;
import cl.bci.infraestructura.web.seguridad.segundaclave.SegundaClaveUIInput;

/**
 * MB encargado del flujo para la bloqueo/desbloqueo de tarjeta de d�bito.
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 12/09/2013 Yon Sing Sius (ExperimentoSocial): Versi�n inicial.</li>
 * <li>1.1 10/12/2013, Yon Sing Sius. (ExperimentoSocial): Se modifica llamada a m�todos por cambios de nombre en la vista.</li>
 * </ul>
 * <B>Todos los derechos reservados por Banco de Cr�dito e Inversiones. </B>
 */
@ManagedBean
@RequestScoped
public class BloqueoTarjetaDebitoControllerMB implements Serializable {

	/**
	 * Serial de la clase.
	 */
	private static final long serialVersionUID = -8800002627317560166L;

	/**
	 * Tabla de par�metros para la obtenci�n del servicio RedBanc.
	 */
	private static final String TABLA_COD_TRANS_REDBANC = "integracionRedBanc.parametros";

	/**
	 * Archivo de mensajes de usuario.
	 */
	private static final String MENSAJES = "cl.bci.aplicaciones.seguridad.bloqueotdd.bloqueoTDD";

	/**
	 * Log de la clase.
	 */
	private transient Logger logger = (Logger) Logger.getLogger(BloqueoTarjetaDebitoControllerMB.class);

	/**
	 * Segunda clave ingresada por el cliente. 
	 */
	private SegundaClaveUIInput segundaClave;

	/**
	 * Indica si se produjo un error general.
	 */
	private boolean errorGeneral;

	/**
	 * Mensaje de error.
	 */
	private String msgError;

	/**
	 * Managed Bean inyectado que entrega datos del cliente.
	 */
	@ManagedProperty(value = "#{clienteMB}")
	private ClienteMB clienteMB;

	/**
	 * Managed Bean inyectado que entrega datos de la sesi�n.
	 */
	@ManagedProperty(value = "#{sesionMB}")
	private SesionMB sesionMB;

	/**
	 * Managed Bean inyectado que entrega datos del formulario.
	 */
	@ManagedProperty(value = "#{bloqueoTarjetaDebitoViewMB}")
	private BloqueoTarjetaDebitoViewMB bloqueoTarjetaDebitoViewMB;

	/**
	 * Propiedad inyectada MB journal.
	 */
	@ManagedProperty(value = "#{journalUtilityMB}")
	private JournalUtilityMB journalUtilityMB;  


	public JournalUtilityMB getJournalUtilityMB() {
		return journalUtilityMB;
	}

	public void setJournalUtilityMB(JournalUtilityMB journalUtilityMB) {
		this.journalUtilityMB = journalUtilityMB;
	}

	public BloqueoTarjetaDebitoViewMB getBloqueoTarjetaDebitoViewMB() {
		return bloqueoTarjetaDebitoViewMB;
	}

	public void setBloqueoTarjetaDebitoViewMB(
			BloqueoTarjetaDebitoViewMB bloqueoTarjetaDebitoViewMB) {
		this.bloqueoTarjetaDebitoViewMB = bloqueoTarjetaDebitoViewMB;
	}

	public SesionMB getSesionMB() {
		return sesionMB;
	}

	public void setSesionMB(SesionMB sesionMB) {
		this.sesionMB = sesionMB;
	}

	public SegundaClaveUIInput getSegundaClave() {
		return segundaClave;
	}

	public void setSegundaClave(SegundaClaveUIInput segundaClave) {
		this.segundaClave = segundaClave;
	}

	public String getMsgError() {
		return msgError;
	}

	public ClienteMB getClienteMB() {
		return clienteMB;
	}

	public void setClienteMB(ClienteMB clienteMB) {
		this.clienteMB = clienteMB;
	}

	/**
	 * M�todo llamado al inicio de la aplicaci�n, verifica si el usuario posee tarjetas de d�bito.
	 * 
	 * <p>Registro de versiones:<ul>
	 * <li>1.0 24/09/2013 Yon Sing Sius. (ExperimentoSocial): versi�n inicial.</li>
	 * </ul>
	 * 
	 * @since 1.0
	 */
	public void inicioSecuencia() {
		if (bloqueoTarjetaDebitoViewMB.getTarjetasDebito() == null) {
			ResourceBundle mensajes = PropertyResourceBundle
					.getBundle(BloqueoTarjetaDebitoViewMB.RECURSO_MENSAJE);
			String msg = mensajes.getString("msjeErrorProductos");
			setErrorMensaje(msg);
		}
	}

	/**
	 * Obtiene la direcci�n IP del cliente.
	 * 
	 * <p>Registro de versiones:<ul>
	 * <li>1.0 24/09/2013 Yon Sing Sius. (ExperimentoSocial): versi�n inicial.</li>
	 * </ul>
	 * 
	 * @return ip del cliente que est� conectado
	 * @since 1.0
	 */
	private String getIpCliente() {
		HttpServletRequest httpServletRequest = (HttpServletRequest) FacesContext.getCurrentInstance()
				.getExternalContext().getRequest();
		return httpServletRequest.getRemoteAddr();
	}

	/**
	 * Controla el flujo entre los tabs de la aplicaci�n.
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 25/09/2013, Yon Sing Sius. (ExperimentoSocial) - versi�n inicial
	 * </ul>
	 * </p>
	 * @param evento del flujo
	 * @return Nombre del tab que se debe desplegar 
	 * @since 1.0
	 */
	public String secuencia(FlowEvent evento) {
		String paso = evento.getOldStep();
		if (logger.isDebugEnabled()) {
			logger.debug("[secuencia][" + clienteMB.getRut() + "] evento.getOldStep(): " + paso);
		}

		paso = evento.getNewStep();
		if (logger.isDebugEnabled()) {
			logger.debug("[secuencia][" + clienteMB.getRut() + "] evento.getNewStep(): " + paso);	
		}

		if (paso.equals(IconoInteligenteUtilityMB.COMPROBANTE)) {
			boolean status = false;
			try {
				status = obtenerURLServicioRedBanc();
				if (!status) {
					paso = evento.getOldStep();
				}
			} 
			catch (Exception e) {
				paso = evento.getOldStep();
				String msg = ResourceBundle.getBundle(MENSAJES).getString("msjeErrorServicioNoDisponible");
				FacesMessage mensaje = new FacesMessage(msg);
				mensaje.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, mensaje);
			}
		}
		return paso;
	}

	/**
	 * Se encarga de llamar al servicio de RedBanc para obtener la URL de servicio,
	 * desde la cual el cliente podr� realizar distinas operaciones sobre su tarjeta de 
	 * d�bito. Esta URL de RedBanc se despliega dentro de un iframe.
	 * 
	 * <p>Registro de versiones:<ul>
	 * <li>1.0 20/09/2013, Yon Sing Sius. (ExperimentoSocial): Versi�n inicial.</li>
     * <li>1.1 10/12/2013, Yon Sing Sius. (ExperimentoSocial): Se modifica llamada a m�todos por cambios de nombre en la vista.</li>
	 * </ul>
	 * 
	 * @return status de la transacci�n.
	 * @throws Exception en caso se producirse un error inesparado
	 * @since 1.0
	 */
	public boolean obtenerURLServicioRedBanc() throws Exception {
		if (logger.isDebugEnabled()){
			logger.debug("[bloqueaTDDCliente][" + clienteMB.getRut() +"] form: <cuenta=" 
					+ bloqueoTarjetaDebitoViewMB.getCuentaSeleccionada() + ">, <tarjeta=" 
					+ bloqueoTarjetaDebitoViewMB.getTarjetaSeleccionada()	+ ">, <servicio=" 
					+ bloqueoTarjetaDebitoViewMB.getServicioSeleccionado() + ">");
		}

		try {
			if (!validaSegundaClave()) {
				return false;
			}
		} 
		catch (Exception e) {
			if (logger.isEnabledFor(Level.ERROR)) {
				logger.error("[bloqueaTDDCliente][" + clienteMB.getRut() + "] Validar Clave exception: " + e);
			}
			setErrorMensaje(null);
			throw e;
		}

		String idBanco = TablaValores.getValor(TABLA_COD_TRANS_REDBANC, "paramGenerales", "idBanco");
		String interfaz = TablaValores.getValor(TABLA_COD_TRANS_REDBANC, "paramGenerales", "interfaz");
		String desc = TablaValores.getValor(TABLA_COD_TRANS_REDBANC, "paramGenerales", "descripcion");
		String tipTrj = TablaValores.getValor(TABLA_COD_TRANS_REDBANC, "paramGenerales", "tipoTarjeta");
		String idSrv = bloqueoTarjetaDebitoViewMB.getCodServicioSeleccionado();
		String subSrv = bloqueoTarjetaDebitoViewMB.getCodSubServicio();

		TarjetasDelegate tarjDelegate = new TarjetasDelegate();
		if (logger.isDebugEnabled()) {
			logger.debug("[obtenerServicioRedBanc][" + clienteMB.getRut() + "] Obteniendo URL de RedBanc");
		}

		String url;
		try {
			url = tarjDelegate.obtenerServicioRedBanc(getIpCliente(), idBanco, interfaz, 
					desc, tipTrj, new Long(bloqueoTarjetaDebitoViewMB.getNumeroTarjetaSeleccionada()), 
					subSrv, idSrv);
		} 
		catch (Exception e) {
			if (logger.isEnabledFor(Level.ERROR)) {
				logger.error("[bloqueaTDDCliente][" + clienteMB.getRut() 
						+ "] Exception al bloquear tarjeta: " + e);
			}
			setErrorMensaje(null);
			throw new Exception("Se produce error al obtener URL");
		}

		if ((url == null) || url.equals("")) {
			if (logger.isDebugEnabled()) {
				logger.debug("[bloqueaTDDCliente][" + clienteMB.getRut() + "] Hubo un error al obtener la URL.");
			}
			throw new Exception("URL null o vacio");
		}
		bloqueoTarjetaDebitoViewMB.setUrlRedBank(url);

		return true;
	}

	/**
	 * <p>Valida segunda clave. 
	 * Cliente puede utilizar cualquier m�todo de autenticaci�n (Multipass, SafeSigner) 
	 * </p>
	 * 
	 * <p>Registro de versiones:</p><ul>
	 * <li>1.0 25/09/2013, Yon Sing Sius (ExperimentoSocial): versi�n inicial.</li>
	 * </ul>
	 *
	 * @throws Exception error al validar token
	 * @return true si la validaci�n es correcta, false si no.
	 * @since 1.0
	 */
	private boolean validaSegundaClave() throws Exception {
		boolean validacion = false;
		try{
			logger.debug("[validaSegundaClave][" + getClienteMB().getRut() + "] autenticacion SegundaClave:: inicio");
			segundaClave.verificarAutenticacion();
			logger.debug("[validaSegundaClave][" + getClienteMB().getRut() + "] autenticacion SegundaClave:: OK");
			validacion = true;
			this.journalizarAutentifica("subEventoOk", "estadoOK");
		}
		catch(SeguridadException ex){
			if(logger.isEnabledFor(Level.ERROR)){ 
				logger.error("[validaSegundaClave][" + getClienteMB().getRut() + "] SeguridadException :" + ex); 
			}
			FacesMessage mensaje = new FacesMessage(ex.getInfoAdic());
			mensaje.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage("segundaClave", mensaje);
			this.journalizarAutentifica("subEventoNOk", "estadoNOK");

		}
		catch(Exception ex) {
			this.journalizarAutentifica("subEventoError", "estadoError");
			throw ex;
		} 
		return validacion;
	}

	/**
	 * Setea mensaje de error, si es msg es nulo, se estable error gen�rico.
	 * 
	 * <p>Registro de versiones:<ul>
	 * <li>1.0 24/09/2013 Yon Sing Sius. (ExperimentoSocial): versi�n inicial.</li>
	 * </ul>
	 * 
	 * @param msg mensaje de error.
	 * @since 1.0
	 */
	private void setErrorMensaje(String msg) {
		if (msg == null) {
			ResourceBundle m = PropertyResourceBundle.getBundle(BloqueoTarjetaDebitoViewMB.RECURSO_MENSAJE);
			msg = m.getString("msjeErrorServicioNoDisponible");
		}
		this.msgError = msg;
		this.errorGeneral = true;
	}

	public boolean isErrorGeneral() {
		return this.errorGeneral;
	}


	/**
	 * Metodo encargado de journalizar la autentificacion.
	 * <p>
	 * <b>Registro de versiones:</b>
	 * <ul>
	 * <li>1.0 24/09/2013 Yon Sing Sius. (ExperimentoSocial): versi�n inicial.</li>
	 * </ul>
	 * </p>
	 * 
	 * @param tipoSubEvento	tipo subevento.
	 * @param estado Si autenticaci�n fue ok o no.
	 * @since 1.0
	 */
	private void journalizarAutentifica(String tipoSubEvento, String estado) {
		try {
			String rutCliente = String.valueOf(clienteMB.getRut());
			String dvCliente = String.valueOf(clienteMB.getDigitoVerif());
			String canal = sesionMB.getCanalTO().getNombre();
			String ipCliente = getIpCliente();

			if (logger.isDebugEnabled()) {

				logger.debug("[journalizarAutentifica] rut:" + rutCliente);
				logger.debug("[journalizarAutentifica] dv :" + dvCliente);
				logger.debug("[journalizarAutentifica] id canal :" + canal);
			}

			Eventos evento = new Eventos();
			evento.setRutCliente(String.valueOf(rutCliente));
			evento.setDvCliente(String.valueOf(dvCliente));
			evento.setIdCanal(canal);
			evento.setIdMedio(ipCliente);
			evento.setSubCodEventoNegocio(TablaValores.getValor(TABLA_COD_TRANS_REDBANC, 
					"journal", tipoSubEvento));
			evento.setEstadoEventoNegocio(TablaValores.getValor(TABLA_COD_TRANS_REDBANC, "journal", estado));
			evento.setIdProducto(TablaValores.getValor(TABLA_COD_TRANS_REDBANC, "journal", "idBanco"));
			evento.setCodEventoNegocio(TablaValores.getValor(TABLA_COD_TRANS_REDBANC, "journal", "evento"));

			if (logger.isDebugEnabled()) {
				logger.debug("[journalizarAutentifica] Se invoca journalizacion:" + evento.toString());
			}
			journalUtilityMB.journalizar(evento);
		}
		catch(Exception e) {
			if(logger.isEnabledFor(Level.WARN)){
				logger.warn("[journalizacion][" + clienteMB.getRut() + "] No Journalizo ["+ e.getMessage() + "]");
			}
		}
	}

	/**
	 * Setea las tarjetas de d�bito dada la cuenta seleccionada 
	 * 
	 * <p>Registro de versiones:<ul>
	 * <li>1.0 20/09/2013, Yon Sing Sius. (ExperimentoSocial): Versi�n inicial.</li>
	 * </ul>
	 * 
	 * @since 1.0
	 */
	public void seleccionaCuenta() {
		bloqueoTarjetaDebitoViewMB.getTarjetasDebitoPorCuenta();
	}
}
