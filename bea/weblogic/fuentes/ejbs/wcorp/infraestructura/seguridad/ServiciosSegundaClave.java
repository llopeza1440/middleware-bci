package wcorp.infraestructura.seguridad;

import java.rmi.RemoteException;

import javax.ejb.EJBObject;

import wcorp.model.seguridad.EstadoSegundaClaveTO;
import wcorp.model.seguridad.LlaveSegundaClaveTO;
import wcorp.model.seguridad.ParametrosEstrategiaSegundaClaveTO;
import wcorp.model.seguridad.ResultadoOperacionSegundaClaveTO;
import wcorp.model.seguridad.UsuarioSegundaClaveTO;
import wcorp.model.seguridad.to.EstadoActivacionTokenTO;
import wcorp.model.seguridad.to.OtpEntrustTO;
import wcorp.model.seguridad.to.ResponseStatusTO;
import wcorp.model.seguridad.to.TokenEntrustTO;

/**
 * <b>Interface de EJB ServiciosSegundaClave.</b>
 * <p>
 * Registro de versiones:
 * <ul> 
 *  <li>1.0 10/07/2017 Luis L�pez Alamos (SEnTRA) - Ricardo Carrasco C�ceres (Ing. Soft. BCI): Versi�n Inicial.</li>
 * </ul>
 * <b>Todos los derechos reservados por Banco de Cr�dito e Inversiones.</b>
 * <p>
 * @see ServiciosSegundaClaveBean
 */

public interface ServiciosSegundaClave extends EJBObject {

    /**
     * @see {@link ServiciosSegundaClaveBean#autenticarDesafioGenerico(ParametrosEstrategiaSegundaClaveTO)}
     */
    public ResultadoOperacionSegundaClaveTO autenticarDesafioGenerico(ParametrosEstrategiaSegundaClaveTO parametroEstrategiaSegundaClave) throws Exception, RemoteException;
    
    /**
     * @see {@link ServiciosSegundaClaveBean#obtenerDesafioGenerico(ParametrosEstrategiaSegundaClaveTO)}
     */    
    public LlaveSegundaClaveTO obtenerDesafioGenerico(ParametrosEstrategiaSegundaClaveTO parametroEstrategiaSegundaClave) throws Exception, RemoteException;
    
    /**
     * @see {@link ServiciosSegundaClaveBean#actualizarTokenUsuario(TokenEntrustTO, UsuarioSegundaClaveTO)}
     */    
    public ResponseStatusTO actualizarTokenUsuario(ParametrosEstrategiaSegundaClaveTO parametroEstrategiaSegundaClave)  throws RemoteException;
    
    /**
     * @see {@link ServiciosSegundaClaveBean#consultarEstadoDesafio(String)}
     */
    public String consultarEstadoDesafio(String codigoTransaccion) throws RemoteException;
    
    /**
     * @see ServiciosSegundaClaveBean#listarTokenUsuario(ParametrosEstrategiaSegundaClaveTO)
     */
    public TokenEntrustTO[] listarTokenUsuario(ParametrosEstrategiaSegundaClaveTO parametroEstrategiaSegundaClave) throws RemoteException;
    /**
     * @see ServiciosSegundaClaveBean#activarSoftTokenUsuario(ParametrosEstrategiaSegundaClaveTO)
     */
    public EstadoActivacionTokenTO activarSoftTokenUsuario(ParametrosEstrategiaSegundaClaveTO parametroEstrategiaSegundaClave) throws RemoteException;
    
    /**
     * @see ServiciosSegundaClaveBean#consultarEstadoSegundaClave(String,String,String)
     */
    public EstadoSegundaClaveTO[] consultarEstadoSegundaClave(String rut, String digitoVerificador, String codigoTipoProd) throws RemoteException;
    
    /**
     * @see ServiciosSegundaClaveBean#obtenerOTP(UsuarioSegundaClaveTO)
     */
    public OtpEntrustTO obtenerOTP(UsuarioSegundaClaveTO usuarioSegundaClaveTO) throws RemoteException;
}

