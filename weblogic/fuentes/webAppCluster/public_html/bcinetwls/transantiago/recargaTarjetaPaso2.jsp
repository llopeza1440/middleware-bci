<%@ taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles" %>
<%@ taglib uri="/WEB-INF/struts-nested.tld" prefix="nested" %>
<%@ taglib uri="/WEB-INF/struts-template.tld" prefix="template" %>
<%@taglib prefix="html"  uri="/WEB-INF/struts-html.tld"%>
<%@taglib prefix="bean"  uri="/WEB-INF/struts-bean.tld"%>
<%@taglib prefix="logic" uri="/WEB-INF/struts-logic.tld"%>
<%@taglib prefix="fmt"	 uri="http://java.sun.com/jstl/fmt"%>
<%@taglib prefix="c"	 uri="http://java.sun.com/jstl/core"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<fmt:setLocale value="es-cl"/>

<html>
<head>
<title>Tarjeta bip!</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link href="style/definitiva.css" rel="stylesheet" type="text/css" />
<link href="/bcinetwls/common/css/frames.css" rel="stylesheet" type="text/css">
<script src="scripts/funcionesRecargaTransantiago.js" type="text/javascript"></script>
<script type="text/javascript" src="/bcinetwls/common/js/bcipassOnline.js"></script>
<script type="text/javascript" src="/bcinetwls/common/js/multipass.js"></script>
<script type="text/javascript" src="/bcinetwls/common/js/jquery.js"></script>
<style type="text/css">
	.mensaje_plano{
	    padding-left: 10px;
	    vertical-align: top;
	    padding: 15px;
	    color: #000;
	    background: url(/bcinetwls/gestorpagostransferencias/img/ic_alerta.gif) no-repeat 18px 10px #FF9;
	    font-family: Arial, Helvetica, sans-serif;
	    font-size: 12px;
	    line-height: 100%;
	    padding-top: 5px;
	    padding-right: 5px;
	    padding-bottom: 5px;
	    padding-left: 60px;
	}
	
	.mensaje_plano a:visited {
	    color: #005298;
	    text-decoration: none;
	}
	
	.mensaje_plano a:link {
	    color: #005298;
	    text-decoration: none;
	}
	
	.mensaje_plano a:hover{
	    color: #005298;
	    text-decoration: underline;
	}
</style>
<script type="text/javascript">
    var yaGeneradoSoftToken = false;
    
    function recbipToggle(casilla){
      var btn1 = document.getElementById('btn1');
      var btn2 = document.getElementById('btn2');
      <c:choose>
      <c:when test="${!empty EntrustSoftToken}">
        var esSoftTokenOnline = ($('#layerOnline').css('display') != 'none' || $('#errorLayerOnline').css('display') != 'none');
        if (casilla.checked){
                if(!yaGeneradoSoftToken){
                  obtenerDesafioOnline();
                }
                $('#tablaSegundaClave').css('display','');
                
                if(esSoftTokenOnline){
                  btn1.style.display="none";
                  btn2.style.display="none";
                }else{
                  var codConfOffline = window.frames["iframe_offline"].document.getElementById("codigoConfirmacionQR");
                  if(codConfOffline.value.length == 8){
                    btn1.style.display="none";
                    btn2.style.display="";
                  }else{
                    btn1.style.display="";
                    btn2.style.display="none";
                  }
                }
                if(!yaGeneradoSoftToken){
                  inicializaValoresGenerico(<bean:write name="timeOutEntrust"/>, <bean:write name="frecuenciaEntrust"/>, this.document.recargaTarjetaBipForm);
                  resize_iframeContenido();
                  yaGeneradoSoftToken = true;
                }
        }
        else{
            if(esSoftTokenOnline){
              btn1.style.display="none";
              btn2.style.display="none";
            }else{
              btn1.style.display="";
              btn2.style.display="none";
            }
        }
      </c:when>
      
      <c:when test="${!empty EntrustToken}">
      if (casilla.checked){
          var codConfOffline = document.getElementById("pass");
          if(codConfOffline.value.length == 6){
            btn1.style.display="none";
            btn2.style.display="";
          }else{
            btn1.style.display="";
            btn2.style.display="none";
          }
      }
      else{
          btn1.style.display="";
          btn2.style.display="none";
      }
      </c:when>
      
      <c:otherwise>
      if (casilla.checked){
          btn1.style.display="none";
          btn2.style.display="";
      }
      else{
          btn1.style.display="";
          btn2.style.display="none";
      }
      </c:otherwise>
      </c:choose>
    }
    
    function mostrarBotonAceptar(){
        var btn1 = document.getElementById('btn1');
        var btn2 = document.getElementById('btn2');
        btn1.style.display="";
        btn2.style.display="none";
    }
    
</script>
<script>
var apreto=false;
function mOver(which){ which.style.background='#FFFFD2' }
function mOut(which){ which.style.background='#FFFFFF' }
function mOut2(which){ which.style.background='#F5F7FC' }
function isNumberKey(evt)
{
   var charCode = (evt.which) ? evt.which : event.keyCode
   if (charCode > 31 && (charCode < 48 || charCode > 57))
      return false;

   return true;
} 
function confirmar(){
<c:choose>
    <c:when test="${!empty EntrustSoftToken}">
        var codigoConfQR = document.getElementById("confirmationCode");
        if (codigoConfQR != null) {
            try {
                var codConfOffline = window.frames["iframe_offline"].document.getElementById("codigoConfirmacionQR");
                if (codConfOffline != null){
                  codigoConfQR.value = codConfOffline.value;
                  if (codigoConfQR.value == "") {
                      alert("Debe ingresar 8 d\u00edgitos para la confirmaci\u00F3n de Bci Pass");
                      return false;
                  }
                }else{
                  return false;
                }
            } catch(Err){
                alert(Err.description);
                return false;
            }
        }
    </c:when>
    
    <c:when test="${!empty EntrustToken}">
        if (document.recargaTarjetaBipForm.confirmationCode.value.length!=6) {
            alert("Debe ingresar 6 d\u00edgitos para la confirmaci\u00F3n de Bci Pass");
            return false;
        }
    </c:when>
    
    <c:when test="${!empty data}">
	 if (document.recargaTarjetaBipForm.confirmationCode.value.length!=7) {
		 alert("Debe ingresar 7 d\u00edgitos para la confirmaci\u00F3n de Multipass M\u00F3vil");
		 return false;
	 }
    </c:when>
</c:choose>
    
    if (apreto==false) {
        apreto=true;
        document.recargaTarjetaBipForm.submit();
    }
}

function stopRKey(evt) {
  var evt = (evt) ? evt : ((event) ? event : null);
  var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);
  if ((evt.keyCode == 13) && (node.type=="text"))  {return false;}
}

document.onkeypress = stopRKey;

</script>
<logic:present name="data"> 
<!--[if lt IE 9]>
<script type="text/javascript">
		function getImageSrc(base64Src) {
				return "/segundaClave/ImagenQR"; 
		}
</script>
<![endif]-->

<!--[if (gte IE 9)|!(IE)]><!-->
<script type="text/javascript">
			function getImageSrc(base64Src) {
			return base64Src;
			}
</script>
<!--<![endif]-->
</logic:present>

<logic:present name="data"> 
<script type="text/javascript">
function iniciar(image) {
	  var qr = document.getElementById("qr");
	  qr.src = getImageSrc(image);
}
</script>
</logic:present>

</head>

<c:choose>
	<c:when test="${!empty data}">
		<body onLoad="iniciar('data:image/png;base64,<bean:write name="data"/>');">
	</c:when>
	<c:when test="${!empty EntrustToken}">
		<body onLoad="inicializarGenerico(this.document.recargaTarjetaBipForm);">
	</c:when>
	<c:otherwise>
		<body onLoad="document.getElementById('ckbx_acepto').checked = false;">
	</c:otherwise>
</c:choose>
 <html:form action="/generarRecargaTarjetaBip">
   <input type="hidden" name="metodo"/>
    <html:hidden property="rutCliente"/>

	<html:hidden property="cuenta"/>
	<html:hidden property="saldo"/>
	<html:hidden property="sobreGiro"/>
	<html:hidden property="disponible"/>
	<html:hidden property="montoCarga"/>
	<html:hidden property="numeroTarjeta"/>
	<html:hidden property="aliasTarjeta"/>
	<html:hidden property="email"/>
	<html:hidden property="msgDestino"/>
	<html:hidden property="programaSN"/>
	<html:hidden property="tipoPrograma"/>
	<html:hidden property="periodoPrograma"/>
	<html:hidden property="nueva"/>
    <html:hidden property="nombreCuentaCte"/>
    <html:hidden property="saldoCuentaCte"/>
    <html:hidden property="saldoSobreGiroCte"/>
    <html:hidden property="saldoDisponibleCte"/>
     <html:hidden property="rutCliente"/>
  <html:hidden property="digitoVerificadorCliente"/>
  <html:hidden property="tipoCuenta"/>
  <html:hidden property="mailClienteCuenta"/>
 <html:hidden property="banco"/>
 <html:hidden property="tipoPrograma"/>
 <html:hidden property="periodoPrograma"/>
  <html:hidden property="validaSegundaClave"/>

<table width="100%" border="0" cellspacing="0" cellpadding="0" id="contenidos">
  <tr>
    <td width="1030" id="tdcontenido"> <h1>Tarjeta bip! </h1>
    <table width="280" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td><h2>Cargar Tarjeta  </h2></td>
        </tr>
    </table>
    <div id="mapitas">1. Ingresar Datos ><strong> 2. Confirmar</strong>&gt; 3. Comprobante</div>
	
	<div class="mensaje_plano">
	<table>
	    <tr>
			<td align="justify" style="font:normal 12px Arial, Helvetica, sans-serif;">
				<bean:write name="mensaje_info_1" /><br>
				<bean:write name="mensaje_info_2" />
				<a class="Gnegro_2" href="/bcinetwls/transantiago/mas_info.html">M�s informaci�n</a><br><br>
                <input type="checkbox" name="acepto" id="ckbx_acepto" onclick="recbipToggle(this);"><b>Acepto</b></input>
			</td>
	    </tr>
	</table>
	</div>
	
    <table border="0" cellpadding="0" cellspacing="0" id="borde">
      <tr>
        <th width="6"><img src="images/curvaizquierdaarriba.gif" width="6" height="6" /></th>
        <td id="bordearribatabla"></td>
        <td width="6" height="6" align="right"><img src="images/curvaderechaarriba.gif" width="6" height="6" /></td>
      </tr>
      <tr>
        <td width="6" height="6" id="bordeizqmorado">&nbsp;</td>
        <th class="morado"><h1>Confirmaci&oacute;n</h1>          </th>
        <td id="bordederechomorado">&nbsp;</td>
      </tr>
      <tr>
        <td id="tdbordeizq">&nbsp;</td>
        <td class="centrotablaformularios" style="padding-top:10px"><span class="comprobantestandar">La
            carga a&uacute;n no se ha completado</span><span class="texto">, por favor<strong> revise
            la informaci&oacute;n</strong> y <strong>confirme</strong> que corresponde
            a la carga que usted desea realizar. Si los datos est&aacute;n
          correctos, <strong>presione el bot&oacute;n Cargar Tarjeta.</strong></span></td>
        <td id="tdbordeder">&nbsp;</td>
      </tr>
      <tr>
        <td id="tdbordeizq">&nbsp;</td>
        <td class="centrotablaformularios"><table border="0" cellpadding="0" cellspacing="0" id="datostabuladosformularios" >
          <tr  >
            <td colspan="2" class="titulosdatos">Datos de Carga </td>
          </tr>
          <tr  onMouseOver="mOver(this);" onMouseOut="mOut(this);" class="overs">
            <td width="22%" align="right" class="nombredatox_l" style="text-align:right;"><strong>Monto a cargar : </strong></td>
            <td width="45%" align="left" class="importante">&nbsp;$<fmt:formatNumber value="${recargaTarjetaBipForm.montoCarga}" pattern="#,##0"/>
			</td>
          </tr>

          <tr onMouseOver="mOver(this);" onMouseOut="mOut2(this);" class="odd">
            <td align="right" class="nombredatox_l" style="text-align:right;"><strong>N&uacute;mero de Tarjeta Bip : </strong></td>
            <td align="left" class="importante">&nbsp;<fmt:formatNumber value="${recargaTarjetaBipForm.numeroTarjeta}" pattern="###0"/> </td>
          </tr>
          <tr >
            <td align="right" class="nombredatox_l" style="text-align:right;"><strong>Email destinatario :</strong>              </td>
            <td align="left" class="nombredatox_l"><bean:write name="recargaTarjetaBipForm" property="email"/></td>
          </tr>
          <tr onMouseOver="mOver(this);" onMouseOut="mOut2(this);" class="odd">
            <td align="right" class="nombredatox_l" style="text-align:right;"><strong>Esta carga se guardar&aacute; como :</strong></td>
            <td align="left" class="nombredatox_l"><bean:write name="recargaTarjetaBipForm" property="aliasTarjeta"/></td>
          </tr>
		    <logic:equal name="recargaTarjetaBipForm" property="tipoPrograma" value="1">
             <tr>
            <td align="right" class="nombredatox_l" style="text-align:right;"><strong>Periodicidad  :</strong>              </td>
            <td align="left" class="nombredatox_l">
               <logic:equal value="2" name="recargaTarjetaBipForm" property="tipoPrograma">
                 Semanal
              </logic:equal>
               <logic:equal value="3" name="recargaTarjetaBipForm" property="tipoPrograma">
                 Mensual
              </logic:equal>
            </td>
          </tr>
          </logic:equal>
        </table>
        <table border="0" cellpadding="0" cellspacing="0" id="datostabuladosformularios" >

            <tr >
              <td colspan="2" class="titulosdatos">Cuenta de origen </td>
              </tr>

            <tr onMouseOver="mOver(this);" onMouseOut="mOut(this);" class="overs">
              <td width="22%" align="right" class="nombredatox_l" style="text-align:right;"><strong>Cuenta de origen :</strong></td>
              <td width="45%" align="left" class="nombredatox_l"><bean:write name="recargaTarjetaBipForm" property="nombreCuentaCte"/></td>
            </tr>
          </table>


          <table border="0" cellpadding="0" cellspacing="0" id="datostabuladosformularios">
            <tr  >
              <td colspan="2" class="titulosdatos">Otros datos </td>
            </tr>

            <tr onMouseOver="mOver(this);" onMouseOut="mOut(this);" class="overs">
              <td width="56%" align="right" class="nombredatox_l" style="text-align:right;"><strong>Mensaje al destinatario :&nbsp;</strong></td>
              <td width="44%" align="left" class="nombredatox_l"><bean:write name="recargaTarjetaBipForm" property="msgDestino"/></td>
            </tr>            
            
          </table>          
        
          <logic:present name="data"> 
			  	<table border="0" cellpadding="0" cellspacing="0" id="datostabuladosformularios" >
			            <tr>
			              <td colspan="2" class="titulosdatos">Segunda Clave</td>
			            </tr>
						<tr>
							<td  valign="top" width="22%" align="center">
									<img id="qr" width="160" height="160" />				
							</td>
							<td valign="top" width="45%">
								<table border="0" cellpadding="0" cellspacing="0"  id="impresionCodigo" >
							 		<tr>
							 			<td style="font-size:11px;font-family:Arial,Helvetica,sans-serif;padding-right:5px" align="justify"><br/>La recarga a&uacute;n no se ha completado, revise la informaci&oacute;n y confirme que corresponde a la 
							 			recarga que usted desea realizar. <b>Escanee el c&oacute;digo QR con su aplicaci&oacute;n MultiPass M&oacute;vil</b>, si los datos concuerdan con los de la transferencia, 
							 			ingrese el c&oacute;digo num&eacute;rico en el campo de texto y presione el bot&oacute;n Recargar.</td>
							 		<tr/>
							 		<tr>
							 			<td style="font-size:11px;font-family:Arial,Helvetica,sans-serif"  align="justify">
							 			    <br/>
							 				C&oacute;digo de Confirmaci&oacute;n&nbsp;
							 				<input id="confirmationCode" maxlength="7" type="password" name="confirmationCode" onkeypress="return isNumberKey(event)" >
	
										<br/>
										&nbsp;
							 			</td>
							 		</tr>
							 	</table>
							 </td>
						</tr>
					</table> 
			</logic:present>
			
			<logic:present name="EntrustSoftToken"> 
			<div id="tablaSegundaClave" style="display:none">
				<table border="0" cellpadding="0" cellspacing="0" id="datostabuladosformularios" >
					<tr>
						<td class="titulosdatos">Segunda Clave</td>
					</tr>
					<tr>
						<td valign="top">
							<table border="0" cellpadding="0" cellspacing="0" class="ancho_completo">
								<tr><td>
									<div id="layerOnline">
										<div class="entrust_container uno">
											<div class="col img"></div>
											<div class="col texto1"><bean:message key="entrustSoftToken.contenido.textoPrincipalOnline"/></div>
											<div class="mensaje"><bean:message key="entrustSoftToken.contenido.sinInternet"/> <a href="javascript:limpiaIntervalo();generarQRActionGenerico();mostrarBotonAceptar();"><bean:message key="entrustSoftToken.contenido.clickAqui"/></a></div>
										</div>
									</div>
									<div id="errorLayerOnline" class="error_layer_online"><strong><bean:message key="entrustSoftToken.contenido.transaccionCancelada"/></strong><br/></div>
									<div id="layerOffline" style="display:none">
										<div class="entrust_container uno">
											<div id="loading_gen"></div>
											<iframe name="iframe_offline" id="iframe_offline" width="100%" height="100%" frameborder="0" marginwidth="0" marginheight="0" onload="ocultaImgLoading();resize_iframeContenido();"> </iframe>
											<input type="hidden" id="confirmationCode" name="confirmationCode">
										</div>
									</div>
								</td></tr>
							</table>
						</td>
					</tr>
				</table>
			</div>
			</logic:present>
			
			<logic:present name="EntrustToken">
				<table border="0" cellpadding="0" cellspacing="0" id="datostabuladosformularios" >
					<tr>
					  <td class="titulosdatos">Segunda Clave</td>
					</tr>
					<tr>
						<td valign="top">
							<table border="0" cellpadding="0" cellspacing="0" class="ancho_completo" >
								<tr><td>
									<div class="entrust_container opciones">
										<p class="titMultipass"><bean:message key="entrustToken.contenido.tituloMultipass"/></p>
									
										<div class="multipass">
											<div class="img"></div>
											<div class="ingreso">
												<div class="label" id="label"><bean:message key="entrustToken.contenido.subtitulo"/></div>
												<input type="password" id="pass" name="confirmationCode" placeholder="<bean:message key="entrustToken.contenido.placeholder"/>" maxlength="6">
												<div id="ver"></div>
											</div>
											<div class="paso"><bean:message key="entrustToken.contenido.cantCaracteres"/></div>
										</div>
										
										<div id="loading"></div>
									</div>
								</td></tr>
							</table>
						</td>
					</tr>
				</table> 
			</logic:present>
			
        </td>
        
        <td id="tdbordeder">&nbsp;</td>
      </tr>
      <tr>
        <td><img src="images/curvaabajoizquierda.gif" width="6" height="6" /></td>
        <td id="bordebajotabla"><img src="images/bordeblancoabajo.gif" width="6" height="6" /></td>
        <td><img src="images/curvaabajoderecha.gif" width="6" height="6" /></td>
      </tr>
    </table>
	<table border="0" cellspacing="0" cellpadding="0" id="botonera">
    <tr>
          <td><a href="#" onClick="history.go(-1)"><img src="images/btn_volver.gif" title="volver" alt="volver" width="70" height="21" border="0" /></a>

                    <logic:notEqual name="recargaTarjetaBipForm" property="tipoPrograma" value="1">
            <img width="108" height="21" border="0" alt="Cargar Tarjeta"
              title="Cargar Tarjeta" src="images/btn_cargartarjeta.gif" id="btn1"
              style="margin-left:10px;-moz-opacity: 0.4;opacity:.4;filter: alpha(opacity=40);">
            <a href="#" onClick="confirmar();" style="display:none;" id="btn2">
		  <img src="images/btn_cargartarjeta.gif" width="108" height="21" border="0" title="Cargar Tarjeta" alt="Cargar Tarjeta" style="margin-left:10px" />
            </a>
                  </logic:notEqual>
                   <logic:equal name="recargaTarjetaBipForm" property="tipoPrograma" value="1">
            <img border="0" alt="Cargar Tarjeta"
                title="Cargar Tarjeta" src="images/btn_cargartarjeta-2.gif" id="btn1"
                style="margin-left:10px;-moz-opacity: 0.4;opacity:.4;filter: alpha(opacity=40);">
            <a href="#" onClick="confirmar();" style="display:none;" id="btn2">
                <img src="images/btn_cargartarjeta-2.gif" border="0">
		  </a>
          </logic:equal>

		  </td>
        </tr>
  </table>    </td>
  </tr>
</table>
</html:form>
</body>

</html>

<%--
/**************************************************************************************************************
* Archivo:           recargaTarjetaPaso2.jsp
* Descripci�n:       Muestra los datos con los cuales se realizar� la recarga de la tarjeta bip para que el cliente 
*                    los revise, confirme que desea hacer la recarga con estos datos y se solicita la segunda clave
*                    de autenticaci�n.
* Path:             /bcinetwls/transantiago
* " Todos los derechos reservados por Banco de Cr�dito e Inversiones."
* Historia de cambios
* -------------------
*  versi�n   fecha      autor            		 cambios
*  =======   ========== =======           		============================================================
*  1.0       ??			 ????               	 versi�n inicial.
*  1.1       04/12/2012 Diego Urrutia Guerra (Imagemaker IT): Se agrega mensaje informativo acerca de la recarga de tarjeta Bip.
*  1.2       30/04/2013 Victor Hugo Enero (ORAND): Se agrega validaci�n de segunda clave SafeSigner
*  1.3       11/10/2013 Eduardo Villagr�n M (Imagemaker): Se agrega checkbox para que cliente confirme que ley�
*                                               mensaje que indica que debe activar la recarga.
*  1.4       06/08/2014 Claudio Marambio C (Imagemaker) : Se limpia checkbox al cargar la pantalla de confirmacion.		
*  1.5       30/08/2017  Marcelo Fuentes (SEnTRA) - Ricardo Carrasco (Ing. Soft. BCI) Se integra la autenticacion con Entrust.
**************************************************************************************************************/
--%>
