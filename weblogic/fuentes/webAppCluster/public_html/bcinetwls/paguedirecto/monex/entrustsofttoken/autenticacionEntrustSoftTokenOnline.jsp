<%@ taglib prefix="fmt" uri="/WEB-INF/fmt.tld"%>
<%@ taglib uri="/tags/struts-bean" prefix="bean"%>
<%@ taglib uri="/tags/struts-logic" prefix="logic"%>
<jsp:useBean id="sessionBci" scope="session" class="wcorp.model.seguridad.SessionBCI"></jsp:useBean>
<jsp:setProperty name="sessionBci" property="*" />

<html>
	<head>
		<title>BCI</title>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="description" content="">
		<meta name="viewport" content="width=device-width,initial-scale=1">
		<link href="/bcinetwls/common/css/frames.css" rel="stylesheet" type="text/css">
		<script type="text/javascript" src="/bcinetwls/common/js/bcipassOnline.js"></script>
		<script type="text/javascript" src="/bcinetwls/common/js/jquery.js"></script>
	</head>
	
	<body onLoad="javascript:inicializaValores(<bean:write name="timeOutEntrust"/>, <bean:write name="frecuenciaEntrust"/>);">
		
		<form name="PagueDirectoForm" id="form" action="PagoDirecto.do">
			<input type="hidden" name="canal" value="entrustSoftToken" />
			<input type="hidden" name="metodo" value="validaEntrustSoftTokenOnline" />
			
			<div id="layerOnline" class="entrust_container uno">
				<div class="col img"></div>
				<div class="col texto1"><bean:message key="entrustSoftToken.contenido.textoPrincipalOnline"/></div>
				<div class="mensaje"><bean:message key="entrustSoftToken.contenido.sinInternet"/> <a href="javascript:limpiaIntervalo();generarQRActionPDMonex();"><bean:message key="entrustSoftToken.contenido.clickAqui"/></a></div>
			</div>
			<div id="errorLayerOnline" class="error_layer_online"><strong><bean:message key="entrustSoftToken.contenido.transaccionCancelada"/></strong><br/></div>
		</form>
	</body>
</html>

<%--
/**************************************************************************************************************************
* Archivo:              autenticacionEntrustSoftTokenOnline.jsp
* Descripci�n:          Se utiliza para la autenticaci�n Entrust SoftToken Online.
* Path:                 /bcinetwls/paguedirecto/monex/entrustsofttoken
* " Todos los derechos reservados por Banco de Cr�dito e Inversiones."              
* Historia Cambios
* -------------------------
* versi�n       fecha         autor                                                       cambios
* =======   ==========  ==========================================================        =================================
* 1.0       02-08-2017  Marcelo Fuentes (SEnTRA) - Ricardo Carrasco (Ing. Soft. BCI)      versi�n inicial. Ser� modificada.
*
**************************************************************************************************************************/
--%>
