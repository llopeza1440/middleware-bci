﻿Este archivo permite generar el men? en BCI cl.

opcion
------
Opciones de menu

titulo
------
Nombre con el que aparece en el listado, el usuario puede hacer clic en ?l.

url
---
URL a la cual se despacha al hacer clic en la descripci?n.

permisos
--------
Permisos que necesita el cliente para poder acceder a la opcion


[REGISTRODEVERSIONES]
==========================================

. Desconocido: version inicial.
. 10/07/2015 Nelson ALvarado (SEnTRA)- CLaudia Lopez (Ing. Soft. BCI): Se agrega url para visualizacion de transferencia de fondos nuevo (transferencia-web.jsf).
. 04/02/2016 Luis Lopez A. (SEnTRA) - Claudia Lopez P. (Ing. Soft. BCI): Se realizan cambios en el menu: Se elimina "Transferir Fondos en línea" struts.
. 31/05/2016 Luis Garrido (Sermaluc) - Ricardo Carrasco (Ing. Soft. BCI): Se fusiona opción de menú de certificados tributarios y depósitos a plazo,
  quedando las nuevas opciones "cert_tributarios_ffmm" y "cert_tributarios_dap".
. 31/01/2017 Luis López Alamos (SEnTRA) - Christian Moraga Roldán (Ing. Soft. BCI) : Se cambia descripción de Recarga Celular por Recarga Celular/TV.
. 10/07/2017 Luis López Alamos (SEnTRA) - Ricardo Carrasco Cáceres (Ing. Soft. BCI) : Se agregan opciones de menu para enrolamiento Entrust.
==========================================