package wcorp.infraestructura.seguridad;

import java.rmi.RemoteException;

import javax.ejb.CreateException;
import javax.ejb.EJBHome;

/**
 * <B>Todos los derechos reservados por Banco de Cr�dito e Inversiones.</B>
 * <P>
 *
 * @see ServiciosSegundaClaveBean
 */

public interface ServiciosSegundaClaveHome extends EJBHome {

    public ServiciosSegundaClave create() throws CreateException, RemoteException;
    
}