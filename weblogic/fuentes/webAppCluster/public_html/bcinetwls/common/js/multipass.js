var ver = false;
var btn_ver;
var input;
var label;
var btnAceptar;
var ndigit;
var form;
var loading;

function inicializar(){
  btn_ver = document.getElementById("ver");
  input = document.getElementById("pass");
  label = document.getElementById("label");
  btnAceptar = document.getElementById("aceptar");
  ndigit = document.getElementById("ndigit");
  form = document.getElementById("form");
  loading = document.getElementById("loading");
  
  btn_ver.onclick = function() {
    var textbox_elem = document.getElementById("pass");
    if(!ver){
      textbox_elem.setAttribute("type", "text");
      ver = true;
    }else{
      textbox_elem.setAttribute("type", "password");
      ver = false;
    }
  }
  
  input.onkeyup = function(){
    this.value = this.value.replace(/[^0-9\.]/g,'');
    ndigit.innerHTML = this.value.length;
    if(this.value.length == 6){
      label.style.opacity = 1;
      btnAceptar.classList.remove("disabled");
      btnAceptar.disabled = false;
    }else{
      label.style.opacity = 0;
      btnAceptar.classList.add("disabled");
      btnAceptar.disabled = true;
    }
  }
  
  form.onsubmit = function(){
    loading.style.display='block';
    setTimeout(function(){
      loading.style.display='none';
      parent.resize();
    }, 2000);
    return aceptar();
  }
}

function salida(w_href, v_url) {
  if (salir) {
    window.opener.location.href = w_href;
    vent = window.open(v_url, "error", "dependent=no,toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=no,resizable=no,width=600,height=475");
    vent.focus();
    salir = false;
  }
}

function aceptar() {
  salir = false;
  var clave = document.PagueDirectoForm.token.value;
  
  if(isNaN(clave) || (clave == '')){
    alert("Debe ingresar el valor que aparece en el dispositivo");
    document.PagueDirectoForm.token.value = '';
    document.PagueDirectoForm.token.focus();
    return false;
  }
  
  btnAceptar = document.getElementById("aceptar");
  if(btnAceptar != null){
    btnAceptar.classList.add("disabled");
    btnAceptar.disabled = true;
  }
  return true;
}

function iniciar() {
  document.PagueDirectoForm.token.focus();
  document.PagueDirectoForm.token.value = "";
}

function acceptNum(event){
  var key = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
  // NOTE: Backspace = 8, Enter = 13, '0' = 48, '9' = 57	
  return (key <= 13 || (key >= 48 && key <= 57));
}

function inicializarGenerico(pForm){
  btn_ver = document.getElementById("ver");
  input = document.getElementById("pass");
  label = document.getElementById("label");
  btnAceptar = document.getElementById("aceptar");
  ndigit = document.getElementById("ndigit");
  loading = document.getElementById("loading");
  
  if(pForm == null){
    form = document.getElementById("form");
  }else{
    form = pForm;
  }
  
  btn_ver.onclick = function() {
    var textbox_elem = document.getElementById("pass");
    if(!ver){
      textbox_elem.setAttribute("type", "text");
      ver = true;
    }else{
      textbox_elem.setAttribute("type", "password");
      ver = false;
    }
  }
  
  var btn1 = document.getElementById('btn1');
  var btn2 = document.getElementById('btn2');
  var ckbx_acepto = document.getElementById('ckbx_acepto');
  
  input.onkeyup = function(){
    this.value = this.value.replace(/[^0-9\.]/g,'');
    ndigit.innerHTML = this.value.length;
    
    if(this.value.length == 6){
      if(btn2 != null){
        if(ckbx_acepto == null || ckbx_acepto.checked){
          btn1.style.display = "none";
          btn2.style.display = "";
        }
      }else{
        btnAceptar.classList.remove("disabled");
        btnAceptar.disabled = false;
      }
    }else{
      if(btn2 != null){
        btn1.style.display = "";
        btn2.style.display = "none";
      }else{
        btnAceptar.classList.add("disabled");
        btnAceptar.disabled = true;
      }
    }
  }
  /*
  form.onsubmit = function(){
    loading.style.display='block';
    setTimeout(function(){
      loading.style.display='none';
      parent.resize();
    }, 2000);
    return aceptar();
  }*/
}
