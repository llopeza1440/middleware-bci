package cl.bci.infraestructura.web.seguridad.segundaclave.ws.to;

import java.io.Serializable;

/**
 * 
 * <b>PrioridadSegundaClaveIdWSTO</b>
 * <p>
 * Estructura para obtener el id de prioridad de segunda clave.
 * </p>
 * 
 * <p>Registro de versiones:</p>
 * 
 * <ul>
 *   <li>1.0 10/07/2017 Sergio Flores (Sentra) - Ricardo Carrasco (Ing. Soft. BCI) : versi�n inicial.</li>
 * </ul>
 * 
 * <p><b>Todos los derechos reservados por Banco de Cr�dito e Inversiones.</b></p>
 * 
 */
public class PrioridadSegundaClaveIdWSTO implements Serializable{    
    
    /**
     * Campo tipo de firma segura.
     */
    public static final PrioridadSegundaClaveIdWSTO SAFESIGNER = 
        new PrioridadSegundaClaveIdWSTO( 10, "SafeSigner", true);
    
    /**
     * Campo tipo multipass.
     */
    public static final PrioridadSegundaClaveIdWSTO MULTIPASS = 
        new PrioridadSegundaClaveIdWSTO( 20, "Multipass", false);

    /**
     * Campo clave internet.
     */
    public static final PrioridadSegundaClaveIdWSTO INTERNET = 
        new PrioridadSegundaClaveIdWSTO( 30, "Internet", false);
    
    /**
     * Campo firma electronica avanzada.
     */
    public static final PrioridadSegundaClaveIdWSTO ETOKEN = 
        new PrioridadSegundaClaveIdWSTO( 50, "EToken", false);
    
    /**
     * Campo Firma Electronica Avanzada Multi Browser.
     */
    public static final PrioridadSegundaClaveIdWSTO ETOKENMULTIBROWSER = 
        new PrioridadSegundaClaveIdWSTO( 60, "ETokenMultiBrowser", false);
    
    /**
     * Campo Soft Token Entrust.
     */
    public static final PrioridadSegundaClaveIdWSTO ENTRUST_SOFTTOKEN = 
       new PrioridadSegundaClaveIdWSTO( 70, "EntrustSoftToken", true);
    
    /**
     * Campo Token F�sico Entrust.
     */
    public static final PrioridadSegundaClaveIdWSTO ENTRUST_TOKEN = 
       new PrioridadSegundaClaveIdWSTO( 80, "EntrustToken", false);
    
    /**
     * Numero de version utilizado durante la serializacion.
     */
    private static final long serialVersionUID = 1L;
    
    /**
     * Campo Identificador.
     */
    private final int id;
    
    /**
     * Campo Nombre.
     */
    private final String nombre;
    
    /**
     * Indica si requiere clave.
     */
    private final boolean requiereLlave;
        

    /**
     * Constructor de la clase.
     * 
     * @param id identificador.
     * @param nombre nombre.
     * @param requiereLlave requiere llave.
     */
    private PrioridadSegundaClaveIdWSTO(int id, String nombre, boolean requiereLlave) {
        this.id = id;
        this.nombre = nombre;
        this.requiereLlave = requiereLlave;
    }    
    
    /**
     * Obtiene prioridad por nombre.
     * <p>
     * <b>Registro de versiones:</b>
     * <ul> 
     * <li>1.0 10/07/2017 Sergio Flores (Sentra) - Ricardo Carrasco (Ing. Soft. BCI) : versi�n inicial.</li>
     * </ul>
     * <p>
     * 
     * @param otroNombre parametro de entrada.
     * @return PrioridadSegundaClaveIdWSTO prioridades.
     * @since 1.0
     */
    public static PrioridadSegundaClaveIdWSTO getPorNombre(String otroNombre) {
        if (SAFESIGNER.getNombre().equals(otroNombre)) {
            return SAFESIGNER;
        }
        else if (MULTIPASS.getNombre().equals(otroNombre)) {
            return MULTIPASS ;
        }
        else if (INTERNET.getNombre().equals(otroNombre)) {
            return INTERNET;
        }
        else if (ETOKEN.getNombre().equals(otroNombre)) {
            return ETOKEN;
        }
        else if (ETOKENMULTIBROWSER.getNombre().equals(otroNombre)) {
            return ETOKENMULTIBROWSER;
        }
        else if (ENTRUST_SOFTTOKEN.getNombre().equals(otroNombre)) {
            return ENTRUST_SOFTTOKEN;
        }
        else if (ENTRUST_TOKEN.getNombre().equals(otroNombre)) {
            return ENTRUST_TOKEN;
        }

        return null;
    }    
    
    
    public int getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }

    public boolean isRequiereLlave() {
        return requiereLlave;
    }
    
	@Override
    public String toString() {
    	StringBuffer sb = new StringBuffer();
        sb.append("PrioridadSegundaClaveIdWSTO: [id=").append(id);
        sb.append("], nombre=[").append(nombre);
        sb.append("], requiereLlave=[").append(requiereLlave);  
        sb.append("]"); 
        return sb.toString();
    }
    
}

