package wcorp.recargacelulares.ui.actions;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.DynaActionForm;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import cl.bci.mensajeria.sdp.conector.ConectorServicioEnvioSDP;
import wcorp.aplicaciones.cliente.interacciones.helper.InteraccionesClienteHelper;
import wcorp.aplicaciones.operacion.pagosycobros.to.DatosCobroServicioTO;
import wcorp.aplicaciones.operacion.pagosycobros.to.ResultadoCobroServicioTO;
import wcorp.aplicaciones.productos.servicios.recargadecelulares.util.GeneradorComprobanteRecargaCelular;
import wcorp.bprocess.recargacelulares.RecargaCelularDelegate;
import wcorp.env.WCorpConfig;
import wcorp.model.seguridad.CamposDeLlaveTO;
import wcorp.model.seguridad.ControllerBCI;
import wcorp.model.seguridad.EstadoSegundaClaveTO;
import wcorp.model.seguridad.ParametrosEstrategiaSegundaClaveTO;
import wcorp.model.seguridad.RepresentacionNumericaSegundaClaveTO;
import wcorp.model.seguridad.RepresentacionSegundaClave;
import wcorp.model.seguridad.ResultadoOperacionSegundaClaveTO;
import wcorp.model.seguridad.SessionBCI;
import wcorp.model.seguridad.UsuarioSegundaClaveTO;
import wcorp.recargacelulares.EstatusVO;
import wcorp.recargacelulares.IngresoTxVO;
import wcorp.recargacelulares.ui.helpers.RecargaCelularesHelper;
import wcorp.serv.cobros.ServiciosCobros;
import wcorp.serv.cobros.ServiciosCobrosHome;
import wcorp.serv.seguridad.SeguridadException;
import wcorp.util.EnhancedServiceLocator;
import wcorp.util.ErroresUtil;
import wcorp.util.FechasUtil;
import wcorp.util.GeneralException;
import wcorp.util.StringUtil;
import wcorp.util.TablaValores;
import wcorp.util.TextosUtil;
import wcorp.util.journal.Eventos;
import wcorp.util.journal.Journal;
import wcorp.util.journal.productos.personas.JournalPersonasFactory;
import wcorp.util.journal.productos.personas.JournalizadorRecargaDeCelulares;
import wcorp.util.mime.GeneradorDeComponentesMIME;
import wcorp.util.xml.ConvierteXml;

/**
 * Action struts responsable de Capturar los datos Confirmados por el Cliente, y
 * de Comunicarse con el objeto de negocio a travez del RecargaCelularDelegate
 * para generar la transaccion de recarga de Celular. Datos: Celular a Cargar,
 * Operadora, monto a cargar, numero de cuenta
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 09/05/2006 Enrique Villar (?): Versi�n inicial
 * 
 * <li>1.1 03/10/2006 Gonzalo Silva (Neoris Chile): Se agreg� lo siguiente :
 * Logs de parametros enviados desde las paginas Jsp Logs de objetos recuperados
 * dentro del m�todo execute Se agreg� un catch Exception para captura de
 * errores no esperados Se eliminaron las comparaciones ternarias.
 * 
 * <li>1.2 20/12/2006 Miguel Quiroz Herrera (?): Se agrega el concepto de
 * validaci�n de acceso utilizando el m�todo validarAcceso perteneciente a la
 * clase ControllerBCI que es propietaria del banco.
 * 
 * <li>1.3 29/01/2007 Gonzalo Silva (Neoris Chile): Se saca del env�o de
 * correos el nombre del canal a petici�n de Juan Pablo Valenzuela (Banca
 * Virtual) Se parametrizo el from del envio de correo para que se distinga
 * desde que canal es enviado el mail. Se agreg� SessionBci a la llamada del
 * delagete.cargoCuentaCliente
 * 
 * <li>1.4 07/02/2007 Gonzalo Silva (Neoris Chile): Se agrego el nombre del
 * canal que env�a el mail a petici�n de Juan Pablo Valenzuela (Banca Virtual)
 * 
 * <li>1.5 13/04/2007 Pedro Carmona E. (SEnTRA): Se agrega validaci�n del monto
 * acumulado diario de cargas por parte del cliente, para lo cual se invoca el
 * m�todo setMontosMaximosAutorizados(), de la clase RecargaCelularesHelper.
 * 
 * <li>1.6 22/05/2007 Pablo C�ceres F. (Ada Ltda) : Se agrega validacion de
 * token (isTokenValid();) para evitar la doble recarga al celular en la llamada
 * al delegate.
 * <li>1.7 11/09/2007 Jorge Ib��ez E (BCI) : Se modifica llamada a m�todo de
 * validaci�n montos m�ximos que consultaba a tabla de moviminentos local de la
 * aplicaci�n (reccel) para que apunte a la tabla de movimiento de base de datos
 * pagos por integraci�n a producto PagueDirecto
 * <li>1.8 21/04/2008 Andr�s Mor�n O. (SEnTRA) : Se agrega seteo del valor montoAdicional
 * en el objeto ingresoTxVO.
 * <li>1.9 26/05/2008 Andr�s Mor�n O. (SEnTRA) : Se agrega seteo del valor aliasRecarga y codigoAlias
 * en el objeto ingresoTxVO.
 * 
 * <li>2.0 26/01/2009 Carlo Figueroa C. (ADA Ltda.) : Se agrega env�o de mail para Bcinet M�vil (Canal 119).
 * <li>2.1 08/01/2010 Paulina Vera (SEnTRA) : Se modifica metodo getContenidoMail(), se agrega cuerpo de mail
 * (comprobante que se le envia al cliente por la recarga) en formato Html, este se enviara en los dos formato
 * tanto texto plano como en html para esto se utilizara mime multipart.
 * <li>2.2 20/10/2010 Leonardo Espinoza (ORAND) : Se agrega soporte para los canales m�viles de BCI y TBanc,
 * se cambia forma de logueo de {@link wcorp.util.LogFile} a {@link org.apache.log4j.Logger}, y se reemplaza
 * validaci�n de duplicidad de recargas.
 * 
 * <li>2.3 22/06/2010 Eduardo Mascayano (TInet): Se modifica completamente la l�gica del m�todo
 * {@link #getContenidoMail(EstatusVO, DynaActionForm, SessionBCI)} para implementar la generaci�n del comprobante
 * HTML/Texto Plano utilizando el Generador de Componentes MIME y se agrega validaci�n de contenido previo al env�o
 * en el m�todo {@link #execute(mapping, form, request, response)}. Se reemplaza utilizaci�n de LogFile por log4j
 * en toda la clase siguiendo recomendaci�n de normativa vigente.
 * <li>
 * 2.4 (26/07/2011, Rodrigo Gonz�lez Merino (Imagemaker IT)): Se modifica la obtenci�n desde el form del
 * par�metro asociado al n�mero de celular, de obtener los par�metros 'celular11' y
 * 'celular12' a obtener el �nico par�metro 'numeroCelular'.
 * Se mejoran los mensajes de logs, se optimizan los imports y se corrige el checkstyke en
 * cuanto a los caracteres por l�nea.
 * <li>2.5 01/12/2011 Diego Urrutia Guerra(Imagemaker IT): Se modifica la manera
 * de enviar el mail al realizar una recarga de celular, ya que se debe utilizar
 * la nueva API.
 * </li>
 * <li>2.6 27/08/2012 Eduardo Villagr�n M (Imagemaker): Se agrega identificador de operacion para desplegar glosa
 * en pantalla noTransaccion.jsp.</li>
 * <li>2.7 30/01/2013, Darlyn Delgado (SEnTRA): Se estandarizan logs
 * <li>2.8 06/03/2013 Veronica Cestari (Orand S.A.): Se agrega logica para manejar SafeSigner
 * <li>2.9 16/04/2014 Gustavo Espinoza S. (TINet): Se agrega l�gica para registro de recarga en historial
 * cliente. Se agrega m�todo {@link #registrarEventoHistorialCliente()}
 * </li>
 * <li>3.0 20/08/2014 Luis Silva (TINet): Se considera el hecho de que si la operaci�n de recarga se realiza por
 * ciertos canales, se cobrar� una comisi�n en el caso de que de que la operaci�n haya sido exitosa. Producto de
 * esto se crea el m�todo {@link #realizarCobroServicio(SessionBCI, String)}.</li>
 * <li>3.1 22/09/2016 Jaime Collao (ImageMaker)- Eduardo Espinoza (Ing. de Software Bci): Se a�ade Hashmap con variables
 * para pasar por sesi�n</li>
 * <li>3.2 16/02/2017 Luis L�pez Alamos (SEnTRA) - Christian Moraga Rold�n (Ing. Soft. BCI): Se modifica {@link #execute(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)}</li>
 * <li>3.3 30/08/2017 Marcelo Fuentes (SEnTRA) - Ricardo Carrasco (Ing. Soft. BCI): Se modifica m�todo 
 * {@link #execute(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)}. Se agregan metodos
 * {@link #journalizar(SessionBCI, String, String, String, String, String, String)} y {@link #getLogger()}.
 * Ademas se agregan las constantes {@link #TABLA_PARAMETROS_ENTRUST}, {@link #ENTRUST_TOKEN}, {@link #ENTRUST_SOFTTOKEN}, {@link #ID_EVENTO}, 
 * {@link #EVENTO_OK}, {@link #EVENTO_NOK}, {@link #ID_PRODENTRUSTTOKEN}, {@link #ID_PRODENTRUSTSOFTTOKEN}, {@link #SUB_EVENTO_OK} y {@link #SUB_EVENTO_NOK}.</li>
 * </ul>
 * <p>
 * 
 * <B>Todos los derechos reservados por Banco de Cr�dito e Inversiones.</B>
 * <P>
 */
public class CargaCelularAction extends Action {

    /**
     * Tabla de parametros de Recargas.
     */
    private static final String PARAMSAFESIGNER = "safesigner.parametros";

    /**
     * Tabla de paametros de recarga de celular.
     */
    private static final String ARCHIVO_PARAMETROS = "recargaCelular.parametros";
    
    /**
     * tabla de par�metros de entrust.
     */
    private static final String TABLA_PARAMETROS_ENTRUST = "entrust.parametros";
    
    /**
     * Tipo de Segunda Clave Softtoken Entrust.
     */
    private static final String ENTRUST_TOKEN = TablaValores.getValor(TABLA_PARAMETROS_ENTRUST, "nombreToken", "desc");
    
    /**
     * Tipo de Segunda Clave Softtoken Entrust.
     */
    private static final String ENTRUST_SOFTTOKEN = TablaValores.getValor(TABLA_PARAMETROS_ENTRUST, "nombreSoftToken", "desc");
    
    /**
     * Identificador de evento.
     */
    private static final String ID_EVENTO = "LOGIN";
    
    /**
     * Identificador para cuando evento es OK.
     */
    private static final String EVENTO_OK = "P";
    
    /**
     * Identificador para cuando evento es NOK.
     */
    private static final String EVENTO_NOK = "R";
    
    /**
     * Identificador para cuando subEvento es OK.
     */
    private static final String SUB_EVENTO_OK = "OK";
    
    /**
     * Identificador para cuando subEvento es NOK.
     */
    private static final String SUB_EVENTO_NOK = "NOK";
    
    /**
     * Identificador del producto Entrust Token.
     */
    private static final String ID_PRODENTRUSTTOKEN       = "HTK";
    
    /**
     * Identificador del producto Entrust SoftToken.
     */
    private static final String ID_PRODENTRUSTSOFTTOKEN   = "STK";
    
    /**
     * El log de la clase.
     */
    private transient Logger logger = Logger.getLogger(CargaCelularAction.class);
    
    private RecargaCelularDelegate delegate = new RecargaCelularDelegate();

    private RecargaCelularesHelper helper = new RecargaCelularesHelper();

    private final SimpleDateFormat formatoFechaContable = new SimpleDateFormat("dd/MM/yyyy");

    String idName = "sessionBci";

    /**
     * Largo de ceros a utilizar.
     */
    private int largoCeros = 5;

    /**
     * separador de texto predeterminado.
     */
    private int delimitador = 4;

    String servicio = "RecargaCelular";
    /**
     * tabla donde se alojan los parametros para el envio de correo en formarto html.
     */
    private static final String MAIL_HTML = "MailHtml.parametros";

    /**
     * Tabla de par�metros del generador de componentes MIME.
     */
    public static final String TABLA_GEN_COMP_MIME = "generadordecomponentesmime/generadorDeComponentesMIME.parametros";

    /**
     * Tabla de par�metros usada para efectos de cobro en l�nea por uso de servicio.
     */
    private static final String TABLA_PARAMETROS_COBROS_LINEA = "cobrosLinea.parametros";

    /**
     * JNDI asociado al EJB ServiciosCobrosBean.
     */
    private static final String JNDI_SERVICIOS_COBROS = "wcorp.serv.cobros.ServiciosCobros";
    
    /**
     * <b>Carga Celular</b><br>
     * 
     * <br>
     * Registro de Versiones:<ul>
     * <li>1.0 xx/xx/xxxx, Desconocido. (Desconocido): versi�n inicial.</li>
     * <li>1.1 21/09/2016 Jaime Collao (ImageMaker)- Eduardo Espinoza (Ing. de Software Bci): Se a�ade Hashmap con 
     * variables a pasar por sesi�n </li>
     * <li>1.2 16/02/2017 Luis L�pez Alamos (SEnTRA) - Christian Moraga Rold�n (Ing. Soft. BCI): Se agrega l�gica de seteo para tipos de Operadora en objetos de valor y request.</li>
     * <li>1.3 30/08/2017  Marcelo Fuentes (SEnTRA) - Ricardo Carrasco (Ing. Soft. BCI): Se agrega logica que revisa 
     * si se requiere autenticaci�n Entrust Token o Entrust SoftToken y en caso de ser asi se valida. Ademas se normaliza log.<li>
     * </ul>
     * <br>
     * 
     * @param mapping Mapeo de forwards para la acci�n.
     * @param form    ActionForm con las variables de acci�n si corresponde.
     * @param request Petici�n HTTP que se est� procesando.
     * @param response Respuesta HTTP que se esta generando.
     * @return El forward correspondiente al formulario por defecto.
     * @since 1.0
     * <B>Todos los derechos reservados por Banco de Cr�dito e Inversiones.</B>
     * <P>
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
        HttpServletResponse response) {

        // Obtiene la sesi�n Http
        HttpSession sesionHttp = request.getSession(false);
        // Obtiene la sesi�n Bci para recuperar datos del cliente
        SessionBCI sesionBci = (SessionBCI) sesionHttp.getAttribute(WCorpConfig.SESION_BCI);
        if(sesionBci.getAttrib("SegundoPaso") == null || ((Boolean) sesionBci.getAttrib("SegundoPaso")).booleanValue() != true  ){
        	return mapping.findForward("fracaso");
        } 

        long rutLog = sesionBci.getCliente().getRut();
        if (getLogger().isInfoEnabled()) {
            getLogger().info("[execute][BCI_INI][" + rutLog + "]");
        }
        DynaActionForm cargaForm = (DynaActionForm) form;
        EstatusVO status = null;

        String rutCliente = (String) cargaForm.get("rut_cliente");
        String cuentaOrigen = (String) cargaForm.get("cuenta_origen");
        int horaLimite = new Integer(TablaValores.getValor("recargaCelular.parametros", "horaLimite", "Desc"))
            .intValue();
        String idConvenio = TablaValores.getValor("recargaCelular.parametros", "Convenio", "Desc");
        String fechaContable = formatoFechaContable.format(FechasUtil.diaHabilParaHoraLimite(Calendar
            .getInstance().getTime(), horaLimite));
        String rut = StringUtil.rellenaConCeros(rutCliente.substring(0, rutCliente.length() - 2), 8);
        String operadorMovil = (String) cargaForm.get("operador_movil");
        String numeroCelular = (String) cargaForm.get("numeroCelular");
        String nombreOperador = (String) cargaForm.get("nombre_operador");
        Double montoAdicional = new Double((String) sesionHttp.getAttribute("montoAdicional"));
        String aliasRecarga = (String) cargaForm.get("alias_recarga");

        String tipoOperadora = cargaForm.get("tipoOperadora") != null ? (String)cargaForm.get("tipoOperadora") : "";
        getLogger().info("[execute] verifica que exista tipoOperadora como atributo de formulario");
        if (tipoOperadora.equalsIgnoreCase("")){
            getLogger().info("[execute] No va parametro tipoOperadora");
        }
        else{
            if (getLogger().isInfoEnabled()){
                getLogger().info("[execute] se pasa atributo tipoOperadora " + tipoOperadora);
            }
            request.setAttribute("tipoOperadora", tipoOperadora);
        }
        
        String codigoAlias = (String) cargaForm.get("codigo_alias");
        if (getLogger().isDebugEnabled()) {
            getLogger().debug("[execute][" + rutLog + "] aliasRecarga:[" + aliasRecarga + "], codigo_alias:["
                + codigoAlias + "]");
        }

        IngresoTxVO ingresoTxVO = new IngresoTxVO();

        String codigoConfirmacion = (String) cargaForm.get("confirmationCode");
        if (codigoConfirmacion == null)
            codigoConfirmacion = "";

        double montoCarga = new Double((String) cargaForm.get("monto_carga")).doubleValue();

        double montoMaximoAutorizado = 0;

        try {

            if (getLogger().isInfoEnabled()) {
                getLogger().info("[execute]["
                    + sesionBci.getAttrib("identificadorPrioridadSegundaClave") 
                    + "] Identificador Prioridad");
            }
            if (getLogger().isInfoEnabled()) {
                getLogger().info("[execute][" + sesionBci.getAttrib("identificadorEstadoSegundaClave") 
                    + "] Identificador Estado");
            }

            int estatusOK = Integer.parseInt(TablaValores.getValor(PARAMSAFESIGNER, "estatusok", "valor"));
            int estatusExpirado = Integer.parseInt(TablaValores
                .getValor(PARAMSAFESIGNER, "estatusexpirado",
                "valor"));
            int estatusClaveIncorrecta = Integer.parseInt(TablaValores.getValor(PARAMSAFESIGNER,
                "estatusclaveincorrecta", "valor"));
            int estatuserrorlargo = Integer.parseInt(TablaValores.getValor(PARAMSAFESIGNER, "estatuserrorlargo",
                "valor"));
            int estatusfallido = Integer.parseInt(TablaValores.getValor(PARAMSAFESIGNER, "estatusintentofallido",
                "valor"));
            String sactive = TablaValores.getValor(PARAMSAFESIGNER, "active", "valor");
            String segundaClaveValor = TablaValores.getValor(PARAMSAFESIGNER, "segundaclave", "valor");
            
            ParametrosEstrategiaSegundaClaveTO parametrosEstrategiaSegundaClaveTO;
            parametrosEstrategiaSegundaClaveTO = new ParametrosEstrategiaSegundaClaveTO(servicio, 
                                                                                        sesionBci.getCliente(), 
                                                                                        sesionBci.getCanal(), 
                                                                                        sesionBci);
            
            EstadoSegundaClaveTO estadoSegundaClaveTO = ControllerBCI.estadoSegundaClave(parametrosEstrategiaSegundaClaveTO);
            
            String segundaClaveSS = "";
            if (estadoSegundaClaveTO != null && estadoSegundaClaveTO.getIdSegundaClave() != null) {
                segundaClaveSS = estadoSegundaClaveTO.getIdSegundaClave().getNombre();
                segundaClaveSS = segundaClaveSS != null ? segundaClaveSS : "";
                
                if (getLogger().isEnabledFor(Level.DEBUG)) {
                    getLogger().debug("[execute] " + segundaClaveSS + ", status:[" + estadoSegundaClaveTO.isEstatus() + "]");
                }
                
                if (!estadoSegundaClaveTO.isEstatus() && (segundaClaveSS.equals(ENTRUST_SOFTTOKEN) || segundaClaveSS.equals(ENTRUST_TOKEN))) {
                    return mapping.findForward("fracaso");
                }
            }
            
            if (getLogger().isEnabledFor(Level.DEBUG)) {
                getLogger().debug("[execute] Se verifica autenticacion para " + segundaClaveSS);
            }
            if (segundaClaveSS.equals(segundaClaveValor) && codigoConfirmacion.length() > 0) {
                if (getLogger().isInfoEnabled()) {
                    getLogger().info("[execute]["
                            + sesionBci.getCliente().getRut()
                            + "] identificarPrioridadSegundaClave: " 
                            +   "SafeSigner " 
                            + " - identificarEstadoSegundaClave: Active");
                }
                /* Invocacion a metodo de validacion segunda clave en controller */
                RepresentacionSegundaClave segundaClave = new RepresentacionNumericaSegundaClaveTO();
                segundaClave.setTipoDeClave(RepresentacionSegundaClave.CLAVE_ALFANUMERICA);
                segundaClave.setClaveAlfanumerica(codigoConfirmacion);

                if (getLogger().isInfoEnabled()) {
                    getLogger().info("[execute][" + sesionBci.getCliente().getRut() + "] datosValidacion"
                        + segundaClave.getClaveNumerica());
                }
                if (getLogger().isDebugEnabled()) {
                    getLogger().debug("[execute] [" + sesionBci.getCliente().getRut()  
                        + "] Entrando a metodo que valida segunda clave");
                }
                ResultadoOperacionSegundaClaveTO resultado = ControllerBCI.validarSegundaClave(request, idName,
                    servicio, segundaClave);
                if (resultado == null) {
                    request.setAttribute("descripcionNoTransaccion", 
                        TablaValores.getValor("errores.codigos", "SAFESIGNER05", "Desc"));
                    getLogger().error("La confirmacion de la segunda clave es nula");
                    return mapping.findForward("noTransaccion");
                }
                else if (!resultado.isEstatus()) {
                    if (getLogger().isInfoEnabled()) {
                        getLogger().info(" [execute][" + sesionBci.getCliente().getRut() + "] Resultado de validacion: "
                            + resultado.getIdCodigoEstado() + " Descripcion:" + resultado.getGlosa());
                    }

                    if (resultado.getIdCodigoEstado() == estatusClaveIncorrecta) {
                        request.setAttribute("descripcionNoTransaccion",
                            TablaValores.getValor("errores.codigos", "SAFESIGNER01", "Desc"));
                    }
                    else if (resultado.getIdCodigoEstado() == estatuserrorlargo) {
                        request.setAttribute("descripcionNoTransaccion",
                            TablaValores.getValor("errores.codigos", "SAFESIGNER02", "Desc"));
                    }
                    else if (resultado.getIdCodigoEstado() == estatusfallido) {
                        request.setAttribute("descripcionNoTransaccion",
                            TablaValores.getValor("errores.codigos", "SAFESIGNER03", "Desc"));
                    }
                    else if (resultado.getIdCodigoEstado() == estatusExpirado) {
                        request.setAttribute("descripcionNoTransaccion",
                            TablaValores.getValor("errores.codigos", "SAFESIGNER04", "Desc"));
                    }
                    else {
                        request.setAttribute("descripcionNoTransaccion",
                            TablaValores.getValor("errores.codigos", "SAFESIGNER05", "Desc"));
                    }

                    return mapping.findForward("noTransaccion");
                }
                montoMaximoAutorizado = helper.setMontosMaximosAutorizados(sesionBci, ARCHIVO_PARAMETROS);

            }
            else if (segundaClaveSS.equals(ENTRUST_SOFTTOKEN)) {
                String modoTransaccion = "";
                if (codigoConfirmacion.length() > 0) {
                    modoTransaccion = "OFFLINE";
                }
                else {
                    modoTransaccion = "ONLINE";
                    codigoConfirmacion = (String)sesionBci.getAttrib("transactionId");
                    sesionBci.removeAttrib("transactionId");
                }
                
                if(codigoConfirmacion.length() == 0){
                    request.setAttribute("descripcionNoTransaccion", TablaValores.getValor("errores.codigos","ENTRUSTFRASE03", "Desc"));
                    if (getLogger().isInfoEnabled()) {
                        getLogger().error("[execute] [" + sesionBci.getCliente().getRut() + "] La confirmacion de la segunda clave es nula");
                    }
                    return mapping.findForward("fracaso");
                }
                
                try {
                    CamposDeLlaveTO campoDeLlaves = sesionBci.getParametrosEstrategiaSegundaClaveTO().getCamposDeLlave();
                    
                    boolean resultado = ControllerBCI.verificarAutenticacion(ENTRUST_SOFTTOKEN, 
                                                                             modoTransaccion, 
                                                                             sesionBci.getParametrosEstrategiaSegundaClaveTO().getUsuarioSegundaClave(), 
                                                                             campoDeLlaves, 
                                                                             servicio, 
                                                                             codigoConfirmacion);
                    sesionBci.setParametrosEstrategiaSegundaClaveTO(null);
                    if(getLogger().isDebugEnabled()){
                        getLogger().debug("[execute] resultado de autenticacion: " + resultado);
                    }
                    
                    journalizar(sesionBci, ID_PRODENTRUSTSOFTTOKEN, ID_EVENTO, SUB_EVENTO_OK, rut, modoTransaccion, EVENTO_OK);
                }
                catch (SeguridadException se) {
                    journalizar(sesionBci, ID_PRODENTRUSTSOFTTOKEN, ID_EVENTO, SUB_EVENTO_NOK, rut, modoTransaccion, EVENTO_NOK);
                    request.setAttribute("descripcionNoTransaccion", se.getMessage());
                    request.setAttribute("javax.servlet.jsp.jspException", se);
                    if(getLogger().isEnabledFor(Level.ERROR)){
                        getLogger().error("[execute] [" + sesionBci.getCliente().getRut() + "] SeguridadException:" + ErroresUtil.extraeStackTrace(se));
                    }
                    return mapping.findForward("noTransaccion");
                }
                
                montoMaximoAutorizado = helper.setMontosMaximosAutorizados(sesionBci, ARCHIVO_PARAMETROS);
                
            }
            else if (segundaClaveSS.equals(ENTRUST_TOKEN)) {
                if(codigoConfirmacion.length() == 0){
                    request.setAttribute("descripcionNoTransaccion", TablaValores.getValor("errores.codigos","ENTRUSTFRASE01", "Desc"));
                    if (getLogger().isInfoEnabled()) {
                        getLogger().error("[execute] [" + sesionBci.getCliente().getRut() + "] La confirmacion de la segunda clave es nula");
                    }
                    return mapping.findForward("fracaso");
                }
                
                try {
                    String usuarioId = String.valueOf(sesionBci.getCliente().getRut()) + String.valueOf(sesionBci.getCliente().getDigito());
                    UsuarioSegundaClaveTO usuarioSegundaClave = new UsuarioSegundaClaveTO(usuarioId, 
                                                                                          ControllerBCI.obtieneDominio(sesionBci.getCanal().getCanalID()), 
                                                                                          sesionBci.getCanal().getCanalID());
                    
                    boolean resultado = ControllerBCI.verificarAutenticacion(ENTRUST_TOKEN, null, usuarioSegundaClave, null, null, codigoConfirmacion);
                    
                    if(getLogger().isDebugEnabled()){
                        getLogger().debug("[execute]resultado: " + resultado);
                    }
                    
                    journalizar(sesionBci, ID_PRODENTRUSTTOKEN, ID_EVENTO, SUB_EVENTO_OK, rut, null, EVENTO_OK);
                }
                catch (SeguridadException se) {
                    journalizar(sesionBci, ID_PRODENTRUSTTOKEN, ID_EVENTO, SUB_EVENTO_NOK, rut, null, EVENTO_NOK);
                    request.setAttribute("descripcionNoTransaccion", se.getMessage());
                    request.setAttribute("javax.servlet.jsp.jspException", se);
                    if(getLogger().isEnabledFor(Level.ERROR)){
                        getLogger().error("[execute] [" + sesionBci.getCliente().getRut() + "] SeguridadException:" + ErroresUtil.extraeStackTrace(se));
                    }
                    return mapping.findForward("noTransaccion");
                }
                
                montoMaximoAutorizado = helper.setMontosMaximosAutorizados(sesionBci, ARCHIVO_PARAMETROS);
                
            }
            else {

                /**
                 * Asignaci�n en HttpRequest de Cta Cte enviada por formulario
                 * para ser validada con las ctas del Cliente ya cargadas en
                 * sessionBCI
                 */

                ControllerBCI.validarAcceso(request, idName, servicio);
                sesionBci.removeAttrib("SegundoPaso");
                montoMaximoAutorizado = helper.setMontosMaximosAutorizados(sesionBci, ARCHIVO_PARAMETROS);
                if (getLogger().isDebugEnabled()) {
                    getLogger().debug("[execute][" + rutLog + "] montoCarga: " + montoCarga
                        + ", montoMaximoAutorizado: " + montoMaximoAutorizado);
                }

                /**
                 * Validaci�n de peticiones no realizadas por el cliente
                 * (Recargas duplicadas)
                 */
                String tokenPassed = (String) sesionHttp.getAttribute("org.apache.struts.action.TOKEN");
                if (getLogger().isDebugEnabled()) {
                    getLogger().debug("[execute][" + rutLog + "] tokenPassed: " + tokenPassed);
                }
                if (tokenPassed == null) {
                    request.setAttribute("descripcionNoTransaccion", (new GeneralException("REC"
                        + sesionBci.getCanal().getCanalID() + "-02")).getSimpleMessage());
                    if (getLogger().isInfoEnabled()) {
                        getLogger().info("[execute][BCI_FINOK][" + rutLog + "] mapping.findForward('noTransaccion')");
                    }
                    return mapping.findForward("noTransaccion");
                }
            }
            if (getLogger().isInfoEnabled()) {
                getLogger().info("[execute][" + rutLog + "]"
                    + (new StringBuffer("DATOS DE " + "RECARGA: ").append(" rutForm: ").append(rutCliente)
                        .append(" cuentaForm: ").append(cuentaOrigen).append(" : ").append(numeroCelular)
                        .append(" montoCarga: ").append(montoCarga).append("MontoAdicional:")
                        .append(montoAdicional).append(" montoMaximoAutorizado: ").append(montoMaximoAutorizado)
                        .append(" nombreOperador: ").append(nombreOperador).append(" numeroCelular: ")
                        .append(numeroCelular).append(" operadorMovil: ").append(operadorMovil)
                        .append(" numeroCelular: ").append(numeroCelular)).toString());
            }

            try {
                ingresoTxVO.setEvento("RECCELWEB");
                ingresoTxVO.setCanal(sesionBci.getCanal().getCanalID());
                ingresoTxVO.setIp(sesionBci.getIP());
                ingresoTxVO.setNroCuentaCliente(cuentaOrigen);
                ingresoTxVO.setMonto(new Double(montoCarga));
                ingresoTxVO.setMontoAdicional(montoAdicional);
                ingresoTxVO.setNroCelularCliente(numeroCelular);
                ingresoTxVO.setRutCliente(Long.toString(sesionBci.getCliente().getRut()) + "-"
                    + String.valueOf(sesionBci.getCliente().getDigito()));

                ingresoTxVO.setCanal(sesionBci.getCanal().getCanalID());
                ingresoTxVO.setNombreOperadora(nombreOperador);
                ingresoTxVO.setIdOperadora(new Integer(operadorMovil));
                ingresoTxVO.setAliasRecarga(aliasRecarga);
                if (codigoAlias.length() > 0) {
                    ingresoTxVO.setCodigoAlias(new Integer(codigoAlias));
                }
                else {
                    ingresoTxVO.setCodigoAlias(new Integer(0));
                }
                
                if (tipoOperadora != null && !tipoOperadora.equalsIgnoreCase(""))
                    ingresoTxVO.setTipoOperadra(tipoOperadora);


                /**
                 * Validaci�n de Cuenta (Cte o Prima) Recibida desde el
                 * formulario corresponda a Ctas asociadas al Cliente
                 */

                sesionBci.checkServicioObjeto(servicio, cuentaOrigen);

                if (getLogger().isDebugEnabled()) {
                    getLogger().debug("[execute][" + rutLog + "][obtiene data desde MOV] rut: [" + rut
                        + "], fechaContableEnMov: [" + fechaContable + "], idConvenio: [" + idConvenio + "]");
                }

                double montoRecargaDia = 0;
                montoRecargaDia = delegate.validarMontosMov(rut, fechaContable, idConvenio);
                if ((montoRecargaDia + (new Double(montoCarga)).doubleValue()) > montoMaximoAutorizado) {
                    getLogger().error("[execute][BCI_FINEX][" + rutLog
                        + "] GeneralException: Cliente sobrepas� el l�mite diario.");
                    throw new GeneralException("REC" + sesionBci.getCanal().getCanalID() + "-01");
                }

                status = delegate.cargarCelularDelCliente(ingresoTxVO);
				
                if (getLogger().isDebugEnabled())
                    getLogger().debug("[execute][" + rutLog + "] status :[ " + StringUtil.contenidoDe(status) + "]");
                /**
                 * Si la recarga NO fue realizada desde Transtel
                 */
                if (status.getEstado() == false) {

                    boolean cargaCelular = status.getEstado() == false;
                    request.setAttribute("cargaCelular", String.valueOf(cargaCelular));
                    request.setAttribute("descripcionNoTransaccion",
                        status.getDescripcion() != null ? status.getDescripcion() : "");
                    if (getLogger().isInfoEnabled()) {
                        getLogger().info("[execute][BCI_FINOK][" + rutLog
                            + "] status[false] No se realizo recarga. mapping.findForward('noTransaccion')");
                    }
                    return mapping.findForward("noTransaccion");
                }
                else{
                    Eventos evento = new Eventos();
                    this.registrarEventoHistorialCliente(evento, sesionBci, ingresoTxVO);
                }
                
            }
            catch (SeguridadException e) {
                if (getLogger().isEnabledFor(Level.WARN)) {
                    getLogger().warn("[execute][" + rutLog + "] Warning, SeguridadException: " + e.getMessage(), e);
                }
                /**
                 * Rutina de Journal de Evento en caso que la cuenta
                 * recibida por POST no corresponda a las asociadas al
                 * cliente.
                 */
                JournalPersonasFactory factory = new JournalPersonasFactory();
                JournalizadorRecargaDeCelulares journalizadorRecargaDeCelulares = (JournalizadorRecargaDeCelulares) factory
                    .obtenerJournalizador(0, null);

                ingresoTxVO.setSubEvento(new String("OBJNOK"));
                journalizadorRecargaDeCelulares.journaliza(ingresoTxVO);

                sesionHttp = request.getSession(false);
                sesionBci = (SessionBCI) sesionHttp.getAttribute(idName);
                request.setAttribute("descripcionNoTransaccion", e.getMessage());
                if (getLogger().isInfoEnabled()) {
                    getLogger().info("[execute][BCI_FINOK][" + rutLog + "] mapping.findForward('noTransaccion')");
                }
                return mapping.findForward("noTransaccion");

            }
            catch (Exception e) {
                getLogger().error("[execute][BCI_FINEX][" + rutLog + "] Exception: " + e.getMessage(), e);
                throw e;
            }

        }
        catch (wcorp.serv.seguridad.NoSessionException nse) {
            if (getLogger().isEnabledFor(Level.WARN)) {
                getLogger().warn("[execute][" + rutLog + "] Warning, NoSessionException: " + nse.getMessage(), nse);
            }
            request.setAttribute("descripcionNoTransaccion", "Su sesi�n ha expirado");
            if (getLogger().isInfoEnabled()) {
                getLogger().info("[execute][BCI_FINOK][" + rutLog + "] mapping.findForward('noTransaccion')");
            }
            return mapping.findForward("noTransaccion");
        }
        catch (SeguridadException se) {
            if (getLogger().isEnabledFor(Level.WARN)) {
                getLogger().warn("[execute][" + rutLog + "] Warning, SeguridadException: " + se.getMessage(), se);
            }
            sesionHttp = request.getSession(false);
            sesionBci = (SessionBCI) sesionHttp.getAttribute(idName);
            request.setAttribute("descripcionNoTransaccion", se.getMessage());
            request.setAttribute("javax.servlet.jsp.jspException", se);
            if (getLogger().isInfoEnabled()) {
                getLogger().info("[execute][BCI_FINOK][" + rutLog + "] mapping.findForward('noTransaccion')");
            }
            return mapping.findForward("fracaso");
        }
        catch (Exception e) {
            if (getLogger().isEnabledFor(Level.WARN)) {
                getLogger().warn("[execute][" + rutLog + "] Warning, Exception: " + e.getMessage(), e);
            }
            request.setAttribute("descripcionNoTransaccion", e.getMessage());
            if (getLogger().isInfoEnabled()) {
                getLogger().info("[execute][BCI_FINOK][" + rutLog + "] mapping.findForward('noTransaccion')");
            }
            return mapping.findForward("noTransaccion");
        }
        if (getLogger().isDebugEnabled()) {
            getLogger().debug("[execute][" + rutLog + "] Realizando cobro comisi�n");
        }
        realizarCobroServicio(sesionBci, cuentaOrigen);
        if (getLogger().isDebugEnabled()) {
            getLogger().debug("[execute][" + rutLog + "] Cobro comisi�n realizado");
        }
        String contenido = getContenidoMail(status, cargaForm, sesionBci);
        String contenidoPlano = "";
        String contenidoHTML = "";
        if ((contenido != null) || !(contenido.trim().equals(""))) {
            contenido = contenido.substring(delimitador, contenido.length());
            String[] contendioMail = StringUtil.divideConString(contenido, "|T&H|");
            contenidoPlano = contendioMail[0];
            contenidoHTML = contendioMail[1];

            String refContract = null;
            String refTemplate = null;
            String loginEnterprise = "";
            String subject = "";

            String idCanal = sesionBci.getCanal().getCanalID();
            refContract = TablaValores.getValor(ARCHIVO_PARAMETROS, "refContract", idCanal);
            refTemplate = TablaValores.getValor(ARCHIVO_PARAMETROS, "refTemplate", idCanal);
            loginEnterprise = TablaValores.getValor(ARCHIVO_PARAMETROS, "loginEnterprise", idCanal);
            subject = TablaValores.getValor(ARCHIVO_PARAMETROS, "subject", "desc");

            Map paramsMail = new Hashtable();

            paramsMail.put("to", (String) sesionBci.getCliente().getEmail());
            paramsMail.put("subject", subject);
            paramsMail.put("body", contenidoPlano);
            paramsMail.put("bodyHtml", contenidoHTML);
            
            String contexto = TablaValores.getValor("JNDIConfig.parametros", "cluster", "param");
            ConectorServicioEnvioSDP instance = new ConectorServicioEnvioSDP(contexto);

            int resultadoEnvio = instance.sendMail(loginEnterprise, refContract, paramsMail);

            if (getLogger().isInfoEnabled()) {
                getLogger().info("[execute][" + rutLog + "] Resultado Envio:" + resultadoEnvio);
            }

            String descMsj = "";
            if (resultadoEnvio != 0) {
                String codigo = StringUtil.rellenaConCeros(String.valueOf(resultadoEnvio), largoCeros);
                descMsj = TablaValores.getValor(ARCHIVO_PARAMETROS, "descMsj1", "Desc");
                String error = TablaValores.getValor(ARCHIVO_PARAMETROS, "envioMail", codigo);
                request.setAttribute("descMsj", descMsj);
                request.setAttribute("msjError", error);
                sesionHttp.setAttribute("msjError", error);
                if (codigo.equalsIgnoreCase("00007")) {
                    if (getLogger().isEnabledFor(Level.WARN)) {
                        getLogger().warn("[execute][" + rutLog
                            + "][La referencia del contrato esta vacia] Warning, error: " + error);
                    }
                }
                if (getLogger().isEnabledFor(Level.WARN)) {
                    getLogger().warn("[execute][" + rutLog + "][No se pudo enviar mail] Warning, error: " + error);
                }
            }
            else {
                descMsj = TablaValores.getValor(ARCHIVO_PARAMETROS, "descMsj2", "Desc");
                request.setAttribute("descMsj", descMsj);
            }
        }
        else {
            if (getLogger().isEnabledFor(Level.WARN)) {
                getLogger().warn("[execute][" + rutLog + "] Error al generar el contenido HTML/Texto Plano del mail");
            }
        }

        // Invalida esta Transferencia ya que termin� con �xito
        sesionHttp.removeAttribute("org.apache.struts.action.TOKEN");
        if (getLogger().isDebugEnabled()) {
            getLogger().debug("[execute][" + rutLog + "] email_cliente: " + sesionBci.getCliente().getEmail());
        }
        request.setAttribute("email_cliente", sesionBci.getCliente().getEmail() + ", numero_comprobante: "
            + status.getIdComprobante());
        request.setAttribute("fecha_carga", new Date());
        // se a�ade operador ternario 26 Sept 06
        request.setAttribute("monto_carga",
            cargaForm.get("monto_carga") != null ? (String) cargaForm.get("monto_carga") : "");
        request.setAttribute("numero_comprobante", status.getIdComprobante());
        
        HashMap listComprobante = new HashMap();
        listComprobante.put("codigoAutorizacion", String.valueOf(status.getIdComprobante()));
        listComprobante.put("montoAbonoRecarga", String.valueOf(cargaForm.get("monto_carga")));
        listComprobante.put("montoAbonoAdicional", String.valueOf(montoAdicional.doubleValue()));
        listComprobante.put("adicionales", "");
        listComprobante.put("cuentaCargo", String.valueOf(cargaForm.get("nombreCuentaCte")));
        listComprobante.put("compania", String.valueOf(cargaForm.get("nombre_operador")));
        listComprobante.put("numeroCelular", String.valueOf(cargaForm.get("numeroCelular")));
        listComprobante.put("vigenciaRecarga", "");
        if (tipoOperadora != null && !tipoOperadora.equalsIgnoreCase("")){
            listComprobante.put("tipoOperadora", tipoOperadora);
        }
        sesionHttp.setAttribute("listComprobante", listComprobante);
        
        if (getLogger().isInfoEnabled()) {
            getLogger().info("[execute][BCI_FINOK][" + rutLog + "] email_cliente[" + sesionBci.getCliente().getEmail()
                + "] mapping.findForward('comprobante')");
        }
        return mapping.findForward("comprobante");
    }

    /**
     * Metodo responsable de generar el cuerpo del Mail que sera enviado al
     * Cliente indicando los dastos asociados a la Recarga de Celular
     * 
     * <P>
     * Registro de versiones:
     * <UL>
     * 
     * <LI>1.0 (22/05/2006, Enrique Villar. Neoris Chile versi�n inicial
     * <LI>1.1 (22/11/2006), Gonzalo Silva. Neoris Chile Se agregao cuerpo
     * especial para cada tipo de canal
     * <LI>1.2 (29/01/2007), Gonzalo Silva. Neoris Chile <br>
     * Se sacaron los retornos de carro /n <br>
     * Se agregaron sentencias break para que no se impriman las firmas m�s
     * de una vez <br>
     * Se cambi� tablaDeSimpleDeDatos por tablaDeDatosAlineada
     * <LI>1.3 (07/02/2007), Gonzalo Silva. Neoris Chile <br>
     * Se volvi� a agregar los saltos de carro \n a petici�n de Juan Pablo
     * Valenzuela (Banca Virtual)
     * <LI>1.4 (26/01/2009), Carlo Figueroa C. (ADA Ltda.): Se agregao cuerpo
     * para canal Bcinet M�vil (119).
     * <LI>1.5 (08/01/2010), Paulina Vera (SEnTRA): Se agrega cuerpo de mail en
     * formato Html, este se enviara en los dos formato tanto texto plano como
     * en html para esto se utilizara mime multipart.
     * 
     * <li>2.0 22/06/2010 Eduardo Mascayano (TInet), Hernan Rodriguez (TINet): Se redefine completamente
     * la l�gica del m�todo utilizando el Generador de Componentes MIME para
     * armar el comprobante de Recarga Celular de los canales actualmente
     * implementados: Bcinet(110), Nova(800), Tbanc(100) y Bcinet M�vil(119).
     * se agrega condicion MODALIDAD_CORREO_RECARGA_CELULAR para definir el
     * valor "SI" para enviar correo con formato nuevo HTML y "NO" para mantener envio de correo actual.
     * </li>
     * <li>
     * 2.1 (26/07/2011, Rodrigo Gonz�lez Merino (Imagemaker IT)): Se modifica la obtenci�n desde el form del
     * par�metro asociado al n�mero de celular, de obtener los par�metros 'celular11' y
     * 'celular12' a obtener el �nico par�metro 'numeroCelular'.
     * </li>
     * </UL>
     * </P>
     * 
     * @param status
     * Estado de la Recarga
     * @param cargaForm
     * Definicion de campos de formulario por el
     * DynaActionForm
     * @param sesionBci
     * Objeto de Session
     * @return Contenido del comprobante en los formatos Texto Plano y Html.
     * En caso de error devuelve NULL.
     */
    private String getContenidoMail(EstatusVO status, DynaActionForm cargaForm, SessionBCI sesionBci) {
        long rutLog = sesionBci.getCliente().getRut();

        String modoCorreoHTML = TablaValores.getValor(TABLA_GEN_COMP_MIME, "MODALIDAD_CORREO_RECARGA_CELULAR",
            "MODO_HTML");

        if ((modoCorreoHTML == null) || (modoCorreoHTML.equalsIgnoreCase("NO"))) {

            String contenido = null;
            Calendar calendar = Calendar.getInstance();
            int dia = calendar.get(Calendar.DATE);
            int mes = calendar.get(Calendar.MONTH) + 1;
            int ano = calendar.get(Calendar.YEAR);
            String fecha = dia + "/" + mes + "/" + ano;
            contenido = "Estimado " + sesionBci.getCliente().getFullName() + ". " + "Con fecha " + fecha
                + "  se ha realizado una recarga de celular con cargo a su " + "cuenta N� "
                + (String) cargaForm.get("nombreCuentaCte") + ". El detalle de esta operacion es \nel siguiente: ";
            String[] nombres = { "Comprobante de RECARGA - Comprobante Nro", "Monto de Recarga", "Compa��a",
                    "N�mero de celular", "Cuenta de cargo"
            };
            String[] valores = { status.getIdComprobante(), (String) cargaForm.get("monto_carga"),
                    (String) cargaForm.get("nombre_operador"), (String) cargaForm.get("numeroCelular"),
                    (String) cargaForm.get("nombreCuentaCte")
            };
            contenido = contenido + TextosUtil.tablaDeDatosAlineada(nombres, valores);
            int idCanal = Integer.parseInt(sesionBci.getCanal().getCanalID());
            if (logger.isDebugEnabled())
                logger.debug("[getContenidoMail][" + rutLog + "] idCanal: " + idCanal);
            switch (idCanal) {
            // Tbanc
            case 100: {
                contenido = contenido + " Para cualquier duda al respecto de este email, escribanos en el "
                    + "formulario de contacto en tbanc.cl o llame al fono 600 524 24 24. \nAtentamente, \n"
                    + "TBanc - El Banco a Distancia";
                break;
            }
                // Bcinet
            case 110: {
                contenido = contenido + " Para cualquier duda al respecto de este email, escribanos a "
                    + "contacto@bci.cl o llame al fono 600 824 24 24. \nAtentamente, \n" + "Bci � Somos "
                    + "Diferentes";
                break;
            }
                // BancoNova
            case 800: {
                contenido = contenido + " Para cualquier duda al respecto de este email, escribanos en el "
                    + "formulario de contacto en banconova.cl o llame al fono 800 22 6682. \nAtentamente, \n"
                    + "Banco Nova - Te quiero feliz";
                break;
            }
                // Bcinet m�vil
            case 119: {
                contenido = contenido + " Para cualquier duda al respecto de este email, escribanos a "
                    + "contacto@bci.cl o llame al fono 600 824 24 24. \nAtentamente, \n" + "Bci � Somos "
                    + "Diferentes";
                break;
            }
                // Tbanc
            case 905: {
                contenido = contenido + " Para cualquier duda al respecto de este email, escribanos en el "
                    + "formulario de contacto en tbanc.cl o llame al fono 600 524 24 24. \nAtentamente, \n"
                    + "TBanc - El Banco a Distancia";
                break;
            }
                // Bcinet
            case 901: {
                contenido = contenido + " Para cualquier duda al respecto de este email, escribanos a "
                    + "contacto@bci.cl o llame al fono 600 824 24 24. \nAtentamente, \n" + "Bci � Somos "
                    + "Diferentes";
                break;
            }
            }

            try {

                /**
                 * Generando el cuerpo del mail en formato html
                 */

                DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
                DocumentBuilder builder;

                builder = factory.newDocumentBuilder();
                Document document = builder.newDocument();
                Element root = (Element) document.createElement("mensajes");
                document.appendChild(root);

                Element docElement = null;
                root = (Element) document.getFirstChild();
                docElement = (Element) document.createElement("msjCliente");
                docElement.appendChild(ConvierteXml.generaNodo(document, "cli_nombre", sesionBci.getCliente()
                    .getFullName()));
                docElement.appendChild(ConvierteXml.generaNodo(document, "cli_fecha", fecha));
                docElement.appendChild(ConvierteXml.generaNodo(document, "cli_numCelular",
                    (String) cargaForm.get("numeroCelular")));
                docElement.appendChild(ConvierteXml.generaNodo(document, "cli_montoRecarga",
                    (String) cargaForm.get("monto_carga")));
                docElement.appendChild(ConvierteXml.generaNodo(document, "cli_compa�ia",
                    (String) cargaForm.get("nombre_operador")));
                docElement.appendChild(ConvierteXml.generaNodo(document, "cli_cuentaCargo",
                    (String) cargaForm.get("nombreCuentaCte")));
                docElement.appendChild(ConvierteXml.generaNodo(document, "cli_numComprobante",
                    status.getIdComprobante()));
                docElement.appendChild(ConvierteXml.generaNodo(document, "logo",
                    TablaValores.getValor(MAIL_HTML, "logoCelular", String.valueOf(idCanal))));
                docElement.appendChild(ConvierteXml.generaNodo(document, "banner",
                    TablaValores.getValor(MAIL_HTML, "bannerCelular", String.valueOf(idCanal))));

                /**
                 * rutaRelativa: ruta donde se aloja archivo xsl para generar el html
                 */
                String rutaRelativa = this.getServlet().getServletContext().getRealPath("/")
                    + String.valueOf(sesionBci.getCanal().getPathJSP());

                root.appendChild(docElement);

                String rutaInforme = rutaRelativa
                    + TablaValores.getValor(MAIL_HTML, "generaCelular", String.valueOf(idCanal));
                ConvierteXml xml = new ConvierteXml();
                String contenidoHtml = xml.transformaHTML(document, rutaInforme);

                contenido = "MIME" + contenido + "|T&H|" + contenidoHtml;

            }
            catch (Exception ex) {
                if (logger.isEnabledFor(Level.WARN)) {
                    logger.warn("[getContenidoMail][" + rutLog + "][Error al generar mail] Warning, Exception: "
                        + ex.getMessage(), ex);
                }
            }

            return contenido;

        }
        else {

            String canal = (sesionBci != null) ? String.valueOf(sesionBci.getCanal().getCanalID()) : "";
            GeneradorComprobanteRecargaCelular comprobante = new GeneradorComprobanteRecargaCelular();
            comprobante.setNombreCliente((sesionBci != null) ? sesionBci.getCliente().getFullName() : "");
            comprobante.setFechaRecarga(FechasUtil.ahora().getTime());
            comprobante.setCuentaCargo(String.valueOf(cargaForm.get("nombreCuentaCte")));
            comprobante.setNumeroComprobante(status.getIdComprobante());
            comprobante.setMonto(Double.valueOf(String.valueOf(cargaForm.get("monto_carga"))).doubleValue());
            comprobante.setCompania(String.valueOf(cargaForm.get("nombre_operador")));
            comprobante.setCelular((String.valueOf(cargaForm.get("numeroCelular"))));

            Map parametros = new HashMap();
            parametros.put("canal", canal);
            GeneradorDeComponentesMIME generador = new GeneradorDeComponentesMIME();
            generador.setComponente(comprobante);
            generador.agregarFormatoHtml("COMPROBANTE_RECARGA_CELULAR", canal);
            Map correos = generador.generarComponentes(parametros);
            String contenidoCorreo = (String) correos.get("XSLT_COMPROBANTE_RECARGA_CELULAR_TXT_" + canal);
            if (contenidoCorreo != null) {
                String contenidoCorreoHTML = (String) correos
                    .get("XSLT_COMPROBANTE_RECARGA_CELULAR_HTML_" + canal);
                if (contenidoCorreoHTML != null) {
                    contenidoCorreo = "MIME" + contenidoCorreo + "|T&H|" + contenidoCorreoHTML;
                }
                else {
                    if (logger.isEnabledFor(Level.WARN)) {
                        logger.warn("[getContenidoMail][" + rutLog
                            + "] Warning, No se enviara correo en formato HTML. ");
                    }
                }
            }
            return contenidoCorreo;
        }

    }

    /**
     * Registra el ingreso a la web a traves de Journal para Historial de Cliente.
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 16/04/2014 Gustavo Espinoza S. (TINet): Versi�n inicial.
     * </ul>
     * <p>
     * @param evento objeto de evento a journalizar.
     * @param sessionBci objeto de la sesi�n.
     * @param cargaRealizada objeto con datos de la carga realizada.
     * @since 2.9
     */
    public void registrarEventoHistorialCliente(Eventos evento, SessionBCI sessionBci, 
    		IngresoTxVO cargaRealizada) {
        if (logger.isDebugEnabled()) {
        	logger.debug("[enriquecerEventoHistorialCliente] Inicio ");
        }

        if (cargaRealizada != null && sessionBci != null && sessionBci.getCanal().getCanalID() != null) {
        	logger.debug("[enriquecerEventoHistorialCliente] Journal habilitado para registrar en historial");
            String accionHistorial = InteraccionesClienteHelper.REGISTRO_RECARGA_CELULAR + "_"
                + sessionBci.getCanal().getCanalID();

            if (logger.isDebugEnabled()) {
            	logger.debug("[enriquecerEventoHistorialCliente] Revisando si esta configurado canal: "
                    + accionHistorial);
            }

            String codTipoAccion = TablaValores.getValor(InteraccionesClienteHelper.TABLA_PARAMETROS,
                accionHistorial, "codTipoAccion");

            if (codTipoAccion != null) {
            	logger.debug("[enriquecerEventoHistorialCliente] Accion para historial configurada");
                String formatoMoneda = TablaValores.getValor(InteraccionesClienteHelper.TABLA_PARAMETROS,
                    "FORMATO_DETALLE", "fmt_moneda");
                String formatoTexto = TablaValores.getValor(InteraccionesClienteHelper.TABLA_PARAMETROS,
                    "FORMATO_DETALLE", "fmt_texto");

                ArrayList listaCamposDetalle = new ArrayList();

                logger.debug("[enriquecerEventoHistorialCliente] Generando map de producto");
                Map productoMap = new HashMap();

                String glosaProducto = TablaValores.getValor(InteraccionesClienteHelper.TABLA_PARAMETROS,
                    accionHistorial, "producto");
                String datoProducto = TablaValores.getValor(InteraccionesClienteHelper.TABLA_PARAMETROS,
                    accionHistorial, "tipoProducto");

                productoMap.put("glosa", glosaProducto);
                productoMap.put("dato", datoProducto);
                productoMap.put("formato", formatoTexto);
                listaCamposDetalle.add(productoMap);
                if (logger.isDebugEnabled()) {
                	logger.debug("[enriquecerEventoHistorialCliente] productoMap: " + productoMap);
                }

                logger.debug("[enriquecerEventoHistorialCliente] Generando map de monto");
                HashMap monto = new HashMap();

                String glosaMonto = TablaValores.getValor(InteraccionesClienteHelper.TABLA_PARAMETROS,
                    accionHistorial, "monto");
                String datoMonto = String.valueOf(cargaRealizada.getMonto());

                monto.put("glosa", glosaMonto);
                monto.put("dato", datoMonto);
                monto.put("formato", formatoMoneda);
                listaCamposDetalle.add(monto);

                if (logger.isDebugEnabled()) {
                	logger.debug("[enriquecerEventoHistorialCliente] monto: " + monto);
                }

                logger.debug("[enriquecerEventoHistorialCliente] Generando map de tasa de interes");
                HashMap numeroCelular = new HashMap();

                String glosaNumeroCelular = TablaValores.getValor(InteraccionesClienteHelper.TABLA_PARAMETROS,
                    accionHistorial, "numeroCelular");
                String datoNumeroCelular = cargaRealizada.getNroCelularCliente();

                numeroCelular.put("glosa", glosaNumeroCelular);
                numeroCelular.put("dato", datoNumeroCelular);
                numeroCelular.put("formato", formatoTexto);
                listaCamposDetalle.add(numeroCelular);

                if (logger.isDebugEnabled()) {
                	logger.debug("[enriquecerEventoHistorialCliente] interesMap: " + numeroCelular);
                }

                logger.debug("[enriquecerEventoHistorialCliente] Seteando valores a objeto evento");
                evento.setAtributo("rutCliente", String.valueOf(sessionBci.getCliente().getRut()));
                evento.setAtributo("dvCliente", String.valueOf(sessionBci.getCliente().getDigito()));
                
                evento.setRutCliente(String.valueOf(sessionBci.getCliente().getRut()));
                evento.setDvCliente(String.valueOf(sessionBci.getCliente().getDigito()));
                
                evento.setAtributo("ejecutivo", TablaValores.getValor(
                        InteraccionesClienteHelper.TABLA_PARAMETROS, "DEFAULTS", "SIN_VALOR"));
                evento.setAtributo("codSucursal", TablaValores.getValor(
                		InteraccionesClienteHelper.TABLA_PARAMETROS, "DEFAULTS", "ID_DEFECTO"));
                evento.setAtributo("descripcion", TablaValores.getValor(
                		InteraccionesClienteHelper.TABLA_PARAMETROS, accionHistorial, "descripcion"));
                evento.setAtributo("totalCamposDetalle", listaCamposDetalle);
                
                evento.setRutOperadorCliente("" + sessionBci.getCliente().getRut());
                evento.setDvOperadorCliente("" + sessionBci.getCliente().getDigito());
                evento.setIdCanal(sessionBci.getCanal().getCanalID());
                
                InteraccionesClienteHelper interacClientehelper = new InteraccionesClienteHelper();
 
                if (logger.isDebugEnabled()) {
                	logger.debug("[journalizar] Se invoca journalizacion:" + evento.toString());
                }
                interacClientehelper.journalizarEventoHistorial(evento, accionHistorial);
                
                logger.debug("[enriquecerEventoHistorialCliente] fin del metodo");
            }
            else {
            	logger.debug("[enriquecerEventoHistorialCliente] Accion para historial no configurada");
            }
        }
        else {
        	logger.debug("[enriquecerEventoHistorialCliente] No se guarda historial del cliente.");
        }

    }

    /**
     * M�todo por el cual se realiza el cobro de una comisi�n por le hecho de haber hecho una operaci�n de
     * recarga por un canal dado.
     * <p>
     * Registro de versiones:<ul>
     * 
     * <li>1.0 20/08/2014 Luis Silva (TINet): Versi�n inicial.
     * </ul><p>
     * @param sessionBCI Sesi�n BCI activa
     * @param cuentaCargo N�mero de cuenta de cargo
     * @since 3.0
     */
    private void realizarCobroServicio(SessionBCI sessionBCI, String cuentaCargo) {
        String claveLog = String.valueOf(sessionBCI.getCliente().getRut());
        try {
            if (logger.isInfoEnabled()) {
                logger.info("[realizarCobroServicio][" + claveLog + "][BCI_INI] Inicio");
                logger.info("[realizarCobroServicio][" + claveLog + "] sessionBCI[" + sessionBCI.getIdSession()
                    + "], cuentaCargo[" + cuentaCargo + "]");
            }
            DatosCobroServicioTO datosCobroServicio = new DatosCobroServicioTO();
            datosCobroServicio.setRutCliente(sessionBCI.getCliente().getRut());
            datosCobroServicio.setDigitoVerificadorCliente(sessionBCI.getCliente().getDigito());
            datosCobroServicio.setIdCanal(sessionBCI.getCanal().getCanalID());
            datosCobroServicio.setIdMedio(sessionBCI.getIP());
            datosCobroServicio.setCodigoServicio(Integer.parseInt(TablaValores.getValor(
                TABLA_PARAMETROS_COBROS_LINEA, "SERVICIOS", "RecargaCelular")));
            datosCobroServicio.setCuentaCargo(cuentaCargo);
            datosCobroServicio.setCantidadTransacciones(1);
            if (logger.isDebugEnabled()) {
                logger.debug("[realizarCobroServicio][" + claveLog + "] obteniedo instancia EJB");
            }
            EnhancedServiceLocator locator = EnhancedServiceLocator.getInstance();
            ServiciosCobrosHome home = (ServiciosCobrosHome) locator.getGenericService(JNDI_SERVICIOS_COBROS,
                ServiciosCobrosHome.class);
            ServiciosCobros serviciosCobros = home.create();
            if (logger.isDebugEnabled()) {
                logger.debug("[realizarCobroServicio][" + claveLog + "] EJB instanciado");
                logger.debug("[realizarCobroServicio][" + claveLog + "] datosCobroServicio [" + datosCobroServicio
                    + "]");
            }
            ResultadoCobroServicioTO resultadoCobro = serviciosCobros
                .realizarCobroPorUsoServicio(datosCobroServicio);
            if (logger.isInfoEnabled()) {
                logger.info("[realizarCobroServicio][" + claveLog + "][BCI_FINOK] resultado cobro ["
                    + resultadoCobro + "]");
            }
        }
        catch (Exception e) {
            logger.error("[realizarCobroServicio][" + claveLog
                + "][BCI_FINOK] Error al cobrar por servicio, mensaje=<" + e.getMessage() + ">.", e);
        }
    }
    
    /**
     * M�todo que realiza la journalizaci�n.
     * <p>
     * Registro de versiones:<ul>
     * <li> 1.0 30/08/2017, Marcelo Fuentes (SEnTRA) - Ricardo Carrasco (Ing. Soft. BCI): Versi�n Inicial.</li>
     * </ul>
     * <p>
     *
     * @param sesionBci La sessi�n del cliente.
     * @param idProducto C�digo del producto.
     * @param idEvento C�digo del evento de negocio.
     * @param subCodNeg Sub c�digo del evento de negocio.
     * @param clavePrincipal Clave principal.
     * @param campoVariable un campo variable.
     * @param estadoEventoNegocio el estado de la operacion a journalizar.
     * @since 3.3
     */
    private void journalizar(SessionBCI sesionBci, String idProducto, String idEvento, String subCodNeg, 
            String clavePrincipal, String campoVariable, String estadoEventoNegocio) {
        try {
            Eventos eventos = new Eventos(sesionBci);
            Journal journal = new Journal(sesionBci);
            eventos.setIdProducto(idProducto);
            eventos.setCodEventoNegocio(idEvento);
            eventos.setSubCodEventoNegocio(subCodNeg);
            eventos.setClavePrincipal(clavePrincipal);
            if (campoVariable != null) {
                eventos.setCampoVariable(campoVariable);
            }
            if (estadoEventoNegocio != null) {
                eventos.setEstadoEventoNegocio(estadoEventoNegocio);
            }
            journal.journalizar(eventos);
        }
        catch (Exception exp) {
            if (logger.isEnabledFor(Level.WARN)) {
                logger.warn("Error al journalizar evento ", exp);
            }
        }
    }
    
    /**
     * Metodo encargado de obtener el logger().
     * <p>
     *
     * Registro de versiones:<ul>
     * <li> 1.0 30/08/2017, Marcelo Fuentes (SEnTRA) - Ricardo Carrasco (Ing. Soft. BCI): Versi�n Inicial.</li>
     * </ul>
     * <p>
     * @return el logger.
     * 
     * @since 3.3
     */
    private Logger getLogger() {
        if (logger == null) {
            logger = Logger.getLogger(this.getClass());
        }
        return logger;
    }
    
}
