package cl.bci.aplicaciones.seguridad.autenticacion.ws.to;

import java.io.Serializable;

/**
 * <b>ResultadoEnrolamientoWSTO</b>
 * <p>
 * TO que almacena los datos posterior a la activaci�n
 * </p>
 * Registro de versiones:<ul>
 * <li>1.0 19/06/2013 Yon Sing Sius (ExperimentoSocial): versi�n inicial.</li>
 * <li>1.1 10/07/2017 Andr�s Silva H.(SEnTRA) - Ricardo Carrasco C�ceres (Ing. Soft. BCI): Se agregan atributos necesarios por proyecto Entrust.
 * se agregan los atributos {@link #idCodigoEstado}, {@link #estatus}, {@link #glosa}, {@link #informacionAnexa}, {@link #celular} y se actualiza el metodo {@link #toString()}.</li>
 * </ul>
 *
 * <p><b>Todos los derechos reservados por Banco de Cr�dito e Inversiones.</b></p>
 */
public class ResultadoEnrolamientoWSTO implements Serializable {
	
	/**
	 *  numero serial de la clase.
	 */
	private static final long serialVersionUID = 210178130832946250L;
	 /**
     * Id Codigo de Estado.
     */
    private int idCodigoEstado;
   
	/**
	 * Estado retornado por SafeSigner.
	 */
	private int status;
    /**
     * Estatus OK o NOK.
     */
    private boolean estatus;
	/**
	 * Data retornada y que debe ser enviada a la aplicaci�n movil para firmar.
	 */
	private String data;
	/**
     * Campo glosa del estado.
     */
    private String glosa;

    /**
     * Informacion Anexa.
     */
    private String informacionAnexa;

	/**
	 * Descripci�n del estado.
	 */
	private String desc;
	
	/**
	 * Campo que representa el celular.
	 */
	private String celular;

    public int getIdCodigoEstado() {
        return idCodigoEstado;
    }

    public void setIdCodigoEstado(int idCodigoEstado) {
        this.idCodigoEstado = idCodigoEstado;
    }

    public boolean isEstatus() {
        return estatus;
    }

    public void setEstatus(boolean estatus) {
        this.estatus = estatus;
    }
	
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	
    public String getGlosa() {
        return glosa;
    }

    public void setGlosa(String glosa) {
        this.glosa = glosa;
    }
    public String getInformacionAnexa() {
        return informacionAnexa;
    }

    public void setInformacionAnexa(String informacionAnexa) {
        this.informacionAnexa = informacionAnexa;
    }
    
	public String getCelular() {
		return celular;
	}

	public void setCelular(String celular) {
		this.celular = celular;
	}

	@Override
	public String toString() {
		StringBuffer bf = new StringBuffer();
		bf.append("ResultadoEnrolamientoWSTO [idCodigoEstado=");
		bf.append(idCodigoEstado);
		bf.append(", status=");
		bf.append(status);
		bf.append(", estatus=");
		bf.append(estatus);
		bf.append(", data=");
		bf.append(data);
		bf.append(", glosa=");
		bf.append(glosa);
		bf.append(", informacionAnexa=");
		bf.append(informacionAnexa);
		bf.append(", desc=");
		bf.append(desc);
		bf.append(", celular=");
		bf.append(celular);
		bf.append("]");
		return bf.toString();
	}

	
}
