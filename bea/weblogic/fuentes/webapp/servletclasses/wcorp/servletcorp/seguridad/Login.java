package wcorp.servletcorp.seguridad;

import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLDecoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Hashtable;
import java.util.Locale;
import java.util.Vector;

import javax.naming.InitialContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import wcorp.aplicaciones.productos.servicios.pagos.paguedirecto.exception.PagueDirectoException;
import wcorp.bprocess.informaciondemorosidad.InformacionDeMorosidadProcessDelegate;
import wcorp.informaciondemorosidad.vo.data.HistoriaComplementoDVO;
import wcorp.infraestructura.seguridad.autenticacion.util.SafeSignerUtil;
import wcorp.model.actores.Cliente;
import wcorp.model.productos.PassCode;
import wcorp.model.seguridad.CamposDeLlaveTO;
import wcorp.model.seguridad.Canal;
import wcorp.model.seguridad.ControllerBCI;
import wcorp.model.seguridad.EstadoSegundaClaveTO;
import wcorp.model.seguridad.LlaveSegundaClaveTO;
import wcorp.model.seguridad.ParametrosEstrategiaSegundaClaveTO;
import wcorp.model.seguridad.RepresentacionAlfanumericaSegundaClaveTO;
import wcorp.model.seguridad.ResultadoOperacionSegundaClaveTO;
import wcorp.model.seguridad.SessionBCI;
import wcorp.model.seguridad.UsuarioSegundaClaveTO;
import wcorp.model.seguridad.activedirectory.to.ResultadoAutenticacionActiveDirectoryTO;
import wcorp.model.seguridad.activedirectory.util.AutenticaActiveDirectory;
import wcorp.serv.bciexpress.BCIExpress;
import wcorp.serv.bciexpress.BCIExpressHome;
import wcorp.serv.bciexpress.BELPerfilUsu;
import wcorp.serv.clientes.ClientesException;
import wcorp.serv.clientes.Datos;
import wcorp.serv.clientes.Operacion;
import wcorp.serv.clientes.RetornoTipCli;
import wcorp.serv.clientes.ServiciosCliente;
import wcorp.serv.clientes.ServiciosClienteHome;
import wcorp.serv.cuentas.CtasCtesEmpresas;
import wcorp.serv.cuentas.ServiciosCuentas;
import wcorp.serv.cuentas.ServiciosCuentasHome;
import wcorp.serv.misc.CodyDescTabla;
import wcorp.serv.misc.ServiciosMiscelaneos;
import wcorp.serv.misc.ServiciosMiscelaneosHome;
import wcorp.serv.pagos.ApoderadoEmpresa;
import wcorp.serv.pagos.ConvenioPDir;
import wcorp.serv.pagos.PagueDirecto;
import wcorp.serv.pagos.ServiciosPagos;
import wcorp.serv.pagos.ServiciosPagosDelegate;
import wcorp.serv.pagos.ServiciosPagosHome;
import wcorp.serv.pagos.TipoConvPDir;
import wcorp.serv.seguridad.CodigoValorBEL;
import wcorp.serv.seguridad.ConsultaClave;
import wcorp.serv.seguridad.ConsultaPreguntas;
import wcorp.serv.seguridad.EstadoPin;
import wcorp.serv.seguridad.ListaConvenios;
import wcorp.serv.seguridad.SeguridadException;
import wcorp.serv.seguridad.ServiciosSeguridad;
import wcorp.serv.seguridad.ServiciosSeguridadHome;
import wcorp.serv.seguridad.SolicitudPassCode;
import wcorp.serv.seguridad.dto.DatosParaValidacionDeClaveDTO;
import wcorp.util.Encriptacion;
import wcorp.util.GeneralException;
import wcorp.util.RUTUtil;
import wcorp.util.StringUtil;
import wcorp.util.TablaValores;
import wcorp.util.com.CtiCommand;
import wcorp.util.com.JNDIConfig;
import wcorp.util.journal.Eventos;
import wcorp.util.journal.Journal;
import wcorp.util.mensajeria.ControlErrorMsg;


/**
 * <b>LOGIN.</b>
 * <p>
 * Este servlet est� encargado del manejo de login para los diferentes canales disponibles.
 * <p>
 * Registro de versiones:<ul>
 *
 * <li>1.0 xx/xx/xxxx NN-AEAD (?):   Versi�n Inicial
 * 
 * <li>1.1 21/02/2005 DVS (?)    : Cambio en el direccionamiento
 *                                 directo al administrador de convenios
 *                                 en Mopa.
 *                                 
 * <li>1.2 24/02/2005 DVS (?)    : Banco en linea sin convenio canal 230
 * 
 * <li>1.3 31/03/2005 DVS (?)    : Cambio entrada directa canal 230 para
 *                                 creaci&oacute;n de session en Mopa
 *                                 
 * <li>1.4 06/12/2004 Rodrigo Zambrano (Novared) : Modificaciones para proyecto
 *                                 mejoras de seguridad fase 1. Requerimiento mensajeria
 *                                 distinta para bci y tbanc.
 *                                 
 * <li>1.5 01/02/2005 Rodrigo Zambrano (Novared) : Modificaciones para proyecto
 *                                 mejoras de seguridad fase 4 (Desacoplado del journal). Se cambia al nuevo sistema
 *                                 de journalizaci�n, ademas se agrego metodo journalizacion.
 *                                 
 * <li>1.6 08/07/2005 aituarte (schema) : Agrega Consulta Morosidad
 * 
 * <li>1.7 26/07/2005 Jos� Luis Barrenechea (?) : se agrega una consulta en el
 *         m�todo PagoDirectoPaso0 para verificar la existencia del serial de
 *         PagueDirecto. El prop�sito de esto es evitar suplantaciones de
 *         seriales, convenios y/o montos. El objetivo es que el mismo URL no
 *         sea gatillado en m�s de una ocasi�n.
 *
 * <li>1.8 15/07/2005 PAL (?)    :   Se corrige el setAttrib de "Message" del paso 4 a "message" y se maneja
 *                                  el retorno de los m�todos del CtiCommand para saber si tuvo o no �xito al
 *                                   enviar el mensaje al com ibm.
 *
 * <li>1.9 06/12/2004, Victor Urra (Novared): Modificaciones para proyecto mejoras
 *          de seguridad fase 3. Journalizacion de eventos de cambio de clave
 *
 * <li>1.10 14/03/2005, Mart�n Maturana (?): Se remueve atributo dispatcherPassCode en
 *           m�todo PassCodePaso2 para efectos de redireccionar correctamente una
 *           vez realizado un cambio de clave al estar realizando una transferencia
 *           de fondos por concepto de expiraci�n de clave passcode.
 *
 * <li>1.11 23/11/2005, Oliver Lopez (ADA Ltda.): Se journaliza pasos de Autenticacion y seguridad
 * 
 * <li>1.12 16/12/2005, Andr�s Mor�n (SEnTRA): Se agregan a PagueDirecto validaciones
 *                       para no aceptar montos negativos.</li>
 *                       
 * <li>1.13 16/01/2006, Aurora Leyton (Sentra): Se agregan condiciones para identificar en
 *                      canal 230 si clientes son proveedores (estos no tienen ctas ctes y
 *                      solo hacen factoring.
 *                      
 * <li>2.0 19/12/2005, Alfredo Reyes (Progesys): Se modifica la clase para que entregue
 * apartir del Objeto HttpSession, el ID de la sesion al Objeto SessionBCI, exactamente al
 * metodo setIdSession().
 * <li>2.1 20/06/2006, Paola Parra (SEnTRA): Se agrega log en el momento de autentificar token.
 * 
 * <li>2.2 28/06/2006, Claudio Perez Bravo(SEnTRA): Se agrega funcionalidad invocar al m�todo autentica
 *           de la SessionBci dependiendo del canal por el cual se realiza el ingreso, para realizar el
 *           manejo de claves internet que se encuentren expiradas.
 *           
 * <li>2.3 (08/09/2006, Paola Parra (SEnTRA)): Se agrega log para debug en paguedirecto al momento de autenticar la clave dos.
 * 
 * <li>2.4 (03/10/2006, Paola Parra (SEnTRA)): Se setea en la sesion la fecha de expiraci�n de la clave dos del cliente.
 *
 * <li>2.5 (06/07/2006 Alejandro Ituarte (schema)) : Consulta edad de mora y si supera lo establecido, impide
 *                    el acceso a la sesi�n (s�lo en BCINet, TBanc y Nova)
 *
 * <li>2.6 (09/01/2006, Rodrigo Devia): Se agrega en la session el perfil y tipoUsuario para ser utilizados en el ingreso de los 
 *
 * <li>2.7 (07/02/2007, Jorge Garcia Ruiz): Se elimina, por razones de seguridad, el registro de la variable pin_param en el Log. 
 *
 * <li>2.8 (09/01/2007, Denisse Vel�squez (SEnTRA)): Se setea variable para la creaci�n de sessi�n en Mopa, canal 230 
 *                               usuario monoconvenio.</li>
 *	
 * <li>2.9 (12/04/2007, Pedro Carmona E. (SEntra)):Se agrega la l�gica necesaria para journalizar el ingreso correcto de la clave internet para cliente que no posee Token.
 *
 * <li>2.7 07/02/2007, Jorge Garcia Ruiz (BCI): Se elimina, por razones de seguridad, el registro de la variable pin_param en el Log. 
 *
 * <li>2.8 09/01/2007, Denisse Vel�squez (SEnTRA): Se setea variable para la creaci�n de sessi�n en Mopa, canal 230 
 *                               usuario monoconvenio.
 * 
 * <li>2.9 30/05/2007, Pedro Carmona (SEnTRA): Se hace una modificaci�n en el m�todo PassCodePaso1() para corregir l�gica en 
 * 					un caso particular de claveDos.
 * <li>3.0 (26/07/2007, Rodrigo Devia): Se agrega en la session el perfil y tipoUsuario para el caso de que un cliente sea monoconvenio.
 * <li>3.1 24/05/2007, Luis Ibarra (Progesys): Se hace una modificaci�n en el m�todo PagueDirectoPaso1 para agregar validaci�n
 *                     de monto m�ximo por transacci�n para empresas.
 *                     
 *                     Modificaci�n en el m�todo PagueDirectoPaso0, Se cambian los mensajes en duro por una tabla de valor.
 *                     
 *                     Modificaci�n en el m�todo PagueDirectoPaso1, Se cambian los mensajes en duro por una tabla de valor.
 *                     
 *                     Modificaci�n en el m�todo PagueDirectoPaso2, Se cambian los mensajes en duro por una tabla de valor.

 * <li>3.2 08/08/2007,  Romina Canales C (SEnTRA): Se rescata atributo flagToken del request, en m�todo service, antes de despachar a la pagina autenticacionToken.
 * 						Luego se remueve este atributo cuando ya se ha realizado la autenticacion. 
 * <li>3.3 (10/10/2007, Jorge Garcia (BCI)): Modificaci�n a m�todo PagueDirectoPaso1 para determinar tipo de cliente seg�n existencia 
 * 						de convenio empresa.
 *
 *  <li>3.4 (30/11/2007, Jorge Garcia (BCI)):  Se incluye captura de excepci�n al m�todo autentica para permitir a los clientes cambiar su clave expirada en BCICashExpress,
 *                                                                             de otra forma la dejo pasar.
 * <li>3.5 (11/12/2007, Carlo Figueroa C. (ADA Ltda)): Se agrega modificaci�n para validar el origen de la clave Tbanc (Sybase o Tandem).</li>
 * <li>3.6 (15/05/2008, Andres Romero. (ESS)): Se agrega modificaci�n para login desde dispositivo PDA, se utilza nuevo canal 665 para identificar y direccionar a
 * 							 				la pagina de menu para PDAs, solo contempla Login exitoso, en caso de alguna Exception sigue el flujo de Webcorp.</li>
 * <li>3.7 29/07/2008, Alejandro Barra F (SEnTRA): Se  modifica el Dispatch en el Canal 230 Caso Monoconvenio (CASO 2).
 * 													Tambien en el: paso.equals("1") // viene de la seleci�n de convenio BEL.
 * 													Se ha modificado el Dispatch y se agrega logica necesaria para el Dispatch cuando el usuario es Supervisor.</li>
 *
 *   <li>3.8 06/09/2008, Nelson Cuevas Reyes (ImageMaker IT): Se elimina el chequeo de morosidad para clientes del Canal 800.
 *                                                           Se eliminan los imports que no estan en uso.
 *                                                           Esta implementaci�n corresponde a una decisi�n de negocio.
 *                                                           Hay que tener un cuidado especial para que cualquier cambio a nivel de canal bci no afecte al
 *                                                           canal banconova.
 *                                                           Esta modificaci�n se realiza bajo el proyecto de emigraci�n de bci a banconova, en el futuro
 *                                                           el Sitio Privado Banco Nova ser� desarrollado sobre la plataforma BEA Portal.
 *                                                           </li>       
 *                                          la pagina de menu para PDAs, solo contempla Login exitoso, en caso de alguna Exception sigue el flujo de Webcorp.</li> 
 * <li>3.7 (10/04/2008, Rodrigo Devia): Se habilita "Compra irresistible Empresarios", por lo que si al logearse se env�a
 *                                       el valor comprasWLS y canal 230 se despacha a la p�gina de oferta activa. Afecta al canal 230 
 *                                       y ocurre para Banco en Linea (CASO 1), Monoconvenio (CASO 2) y Multiconvenio (CASO 3).
 * <li>3.8 30/06/2009, Carlo Figueroa C (ADA Ltda.): Se agrega l�gica para el canal 152 (Aprocred) asignar m�todos de autenticaci�n.
 * <li>3.9 (12/08/2009, Marcelo Avendano(BCI)):  En el paso 0 del pague directo se modifica el orden en que se setea el atributo pd en sesion,
 *                                         para que sea realizado posteriormente a la validacion si existeSerialPagueDirecto.</li>
 * <li>3.10 06/09/2008, Etson Mora Ja�a (ImageMaker IT): Se realizan los siguientes cambios:<ul>
 *                                                       <li>Se agrega validaci�n de dos par�metros obligatorios del servicio Pague Directo.
 *                                                       Si la validaci�n no es correcta, se lanza una excepci�n del tipo {@link PagueDirectoException}.
 * <li>Se eliminan imports con asteriscos y se organizan.
 *                                                       <li>Se reemplaza clase deprecada {@link DigitoVerificador} por {@link RUTUtil}.
 *                                                       <li>Se reemplazan m�todos deprecados de clase {@link StrUtil} por los m�todos equivalentes de la 
 *                                                       clase {@link StringUtil}.
 *                                                       <li>Se agrega mensaje espec�fico para clientes empresa, en caso de que se lance una excepci�n por 
 *                                                       clave expirada en el servicio Pague Directo.</ul>
 * <li>3.11 27/05/2010, Etson Mora Ja�a (ImageMaker IT): En el m�todo {@link #PagueDirectoPaso1(HttpSession, HttpServletRequest, HttpServletResponse, Canal)}
 *                                                       se cambia la clase Integer por la clase Long en la validaci�n del monto m�ximo permitido, para soportar
 *                                                       cifras m�s altas.
 * <li>
 *     4.0 09/07/2010, Rodrigo Gonz�lez Merino (Imagemaker IT): Se modifica la redirecci�n de flujo en el m�todo
 *                     {@link #service(HttpServletRequest, HttpServletResponse)} en le bloque general try-catch al
 *                     capturar una Exception. Se controla la situaci�n en caso de que al cliente se le expire la
 *                     HttpSession al ingreso de la clave multipass (demor� minutos en ingresarla) en donde si la
 *                     sessionBCI a�n existe se reenviar� el flujo a la p�gina de cierre de sessi�n, sino, se
 *                     reenviar� el flujo a la p�gina www.bci.cl controlando adem�s cualquier ingreso inv�lido por
 *                     URL.
 * </li>
 * <li>4.1 18/10/2010, Esteban Landaeta D�az (BCI): Se agregan  logs a los controles de Excepcion  "ClienteException", "SeguridadException" y "GeneralException", para contabilizar
 *                     y generara la nueva m�trica de clientes "No Conectados".
 * </li>
 * <li>4.2 10/12/2010, Juan Buendia (BCI): 	Loggeo y validacion de varibles, para filtrar los errores del tipo "Exception", 
 * 											esto generara una nueva m�trica de clientes "No Conectados".
 * </li>
 * <li>4.3 24/01/2011, Juan Buendia (BCI): 	Loggear IP de conexion y validacion session = null y despacho = null 
 * 											esto generara una nueva m�trica de clientes "No Conectados".
 * <li>4.4 05/04/2011, Esteban Landaeta D�az (BCI): Se cambia la obtenci�n del HOST de conexion por la IP del cliente.
 *											esto generara una nueva m�trica de clientes "No Conectados".
 * <li>4.5 20/10/2010, Pedro Carmona Escobar (SEnTRA): se modifica el despacho para el canal 230 una vez autenticada la clave internet y obtenidos los convenios.<li>
 * </li>
 * <li>4.6 23/05/2011, Juan Buendia (BCI):: Se agrega return, al despacho para evitar error en el login, cuando ya se ha despachado y la pagina de destino envia una excepcion
 * </li>
 * <li>4.7 24/06/2011, Angelo Vel�squez Vel�squez (SEnTRA): Se modifica el m�todo {@link #PagueDirectoPaso0(HttpSession, HttpServletRequest, HttpServletResponse, String)}
 * y el m�todo {@link #PagueDirectoPaso1(HttpSession, HttpServletRequest, HttpServletResponse, String)} agregando el rescate de "estadoPopUp" para 
 * determinar si la p�gina de pague directo debe desplegarse como pop-up o dentro del frame de navegaci�n.
 * Se reemplaza LogFile por Log4j.
 * </li>
 * <li>4.8 07/03/2012, Rodrigo Navarro V. (Imagemaker IT): Se agrega journalizacion con sub evento NOK si ocurre
 * 														   una excepcion al validar el Token.
 * </li>
 * <li>4.9 25/05/2012, H�ctor Hern�ndez Orrego - Mauricio Retamal C. (SEnTRA): Se incorpora JSP propio al sitio Pyme para realizar 
 * la autenticaci�n de Multipass en el sitio. Tambi�n  se agrega l�gica necesaria para rescatar valores para logeo
 * desde al getAttribute del request en caso de que el canal de origen sea 132, esto ya que en caso de que el
 * cliente no tenga convenios con empresas PYME se logee automaticamente al sitio bci empresas. Se eliminan 
 * c�digos comentados.
 * </li>
 * <li>4.10 27/04/2012, Eduardo Villagr�n Morales (Imagemaker): Se modifica validaci�n para que los tipos de
 * 					clientes 'M' y 'A' sean tratados como 'T' (titular).</li>
 * <li>4.11 29/06/2012, H�ctor Hern�ndez Orrego: Se agrega validaci�n de Attribute "redirectDesdeOtroLogin", para 
 * verificar que se esta utilizando el servlet de Login, redireccionando desde otro componente.
 *
 * <li>4.12 15/02/2012, Jaime Suazo D�az (Imagemaker IT): Se crea m�todo
 * {@link Login#journalizacionIngresoSitioEmpresas(SessionBCI, String, String, String)} y en <code>if</code> para
 * canal empresas "230", se agrega l�gica para la journalizaci�n de login (eventos) exitosos por parte de los
 * usuarios del sitio empresas, es decir, para usuarios banco en l�nea, monoconvenios y usuarios multiconvenios.   
 * </li>
 * <li>4.13 25/05/2012, H�ctor Hern�ndez Orrego (SEnTRA): Se incorpora rescate de variable "paso" como atributo 
 * desde el request, esto para permitir el direccionamiento a este servlet desde el nuevo MB, 
 * SeleccionConvenioControllerMB, de selecci�n de convenio y acceder al flujo posterior a la seleccion de convenio.
 * En el flujo con "paso" igual a "1", esto es posterior a selecci�n de convenio, se a�ade el rescate de las 
 * variables de selecci�n de convenio como Attribute.
 * 
 * </li>
 * <li>4.14 03/01/2013, Eduardo Villagr�n Morales (Imagemaker): Se cambia valor pasado al journalizar
 *          cuando canal es 'token' y validaci�n de token termina en 'nok'. Ahora se pasa 'ID_PRODTOKEN' en
 *          vez de 'ID_PRODINTERNET'.</li>
 * <li>4.15 05/08/2014, Claudio Marambio C. (Imagemaker): Se agrega variable de session para el estado de restriccion de clave.</li>
 * <li>4.16 10/04/2015, Rodolfo Kafack Ghinelli (SEnTRA) - Rodrigo Briones O. (Arq. de Soluciones): Se agrega validacion de segunda clave SafeSigner.</li>
 * <li>5.0 30/10/2015, Diego Urrutia Guerra(Imagemaker) - Mario Lorca(BCI): Se agrega l�gica para realizar autenticacion de usuario en Active Directory.
 * Ademas se deja parametrico opci�n para realizar consulta en LDAP en caso de ocurrir problemas, cuando se baje LDAP, se debe eliminar
 * llamada a tabla de parametros y eliminar validaci�n en donde se decide ir a autenticar, para evitar confusiones a futuro, por lo que
 * se deber� deprecar el m�todo {@link #autenticar(SessionBCI, HttpServletRequest, HttpServletResponse, String, String)} y se deber�
 * ir siempre a autenticar mediante el nuevo m�todo {@link #autenticarActiveDirectory(String, String)}.</li>
 * <li>5.1 02/08/2017  Marcelo Fuentes (SEnTRA) - Ricardo Carrasco (Ing. Soft. BCI): Se modifica m�todo
 * {@link #service(HttpServletRequest, HttpServletResponse)}.Se sobrecarga m�todo
 * {@link #journalizacion(SessionBCI, String, String, String, String, String)}.
 * Se agregan los siguientes metodos:
 * {@link #generarLlaveQR(HttpServletRequest, HttpServletResponse, String)},
 * {@link #validaEntrustSoftToken(HttpSession, HttpServletRequest, HttpServletResponse, String, String, String)} y
 * {@link #obtenerCamposDellaves(HttpSession)}.
 * Ademas se agregan las constantes {@link #TABLA_SEGURIDAD}, {@link #TABLA_PARAMETROS_ENTRUST}, {@link #ENTRUST_TOKEN}, {@link #ENTRUST_SOFTTOKEN}, 
 * {@link #ONLINE}, {@link #OFFLINE}, {@link #ID_PRODENTRUSTTOKEN}, {@link #ID_PRODENTRUSTSOFTTOKEN}, {@link #EST_EVENTO_NEGOCIO_REC} y 
 * {@link #EST_EVENTO_NEGOCIO_PRO}.</li>
 * </UL><P>
 *
 * <B>Todos los derechos reservados por Banco de Cr�dito e Inversiones.</B>
 * <P>
 */
public class Login extends HttpServlet {
	
    /**Evento Login.*/
    public static final String EVE_LOGIN                 = "LOGIN";
    /**Evento Login super usuario.*/
    public static final String EVE_LOGINSUP              = "LOGINSUP";               
    /**Sub codigo evento Exitoso.*/
    public static final String SUB_COD_OK                = "OK";
    /**Evento No Exitoso.*/
	public static final String SUB_COD_NOK               = "NOK";
	/**Evento con errores.*/
    public static final String SUB_COD_ERROR             = "INCORR";
    /** SUB_COD_BLOQ. */
    public static final String SUB_COD_BLOQ              = "BLOQ";
    /** SUB_COD_AUTBAN. */
    public static final String SUB_COD_AUTBAN            = "AUTBAN";
    /** SUB_COD_TRAIVR. */
    public static final String SUB_COD_TRAIVR            = "TRAIVR";
    /** SUB_COD_TRASEG. */
    public static final String SUB_COD_TRASEG            = "TRASEG";
    //Producto
    /**Identificador del producto Internet.*/
    public static final String ID_PRODINTERNET           = "INT";
    /**Identificador del producto Passcode.*/
    public static final String ID_PRODPASSCODE           = "PAS";
    /**Identificador del producto Token.*/
    public static final String ID_PRODTOKEN              = "TOK";
    
    /**
     * tabla de par�metros de entrust.
     */
    private static final String TABLA_SEGURIDAD = "Seguridad.parametros";
    
    /**
     * tabla de par�metros de entrust.
     */
    private static final String TABLA_PARAMETROS_ENTRUST = "entrust.parametros";
    
    /**
     * Tipo de Segunda Clave Softtoken Entrust.
     */
    private static final String ENTRUST_TOKEN = TablaValores.getValor(TABLA_PARAMETROS_ENTRUST, "nombreToken", "desc");
    
    /**
     * Tipo de Segunda Clave Softtoken Entrust.
     */
    private static final String ENTRUST_SOFTTOKEN = TablaValores.getValor(TABLA_PARAMETROS_ENTRUST, "nombreSoftToken", "desc");
    
    /**
     * tipo de segunda clave para SoftToken Entrust.
     */
    private static final String ONLINE = TablaValores.getValor(TABLA_PARAMETROS_ENTRUST, "modoOnline", "desc");
    
    /**
     * tipo de segunda clave para SoftToken Entrust.
     */
    private static final String OFFLINE = TablaValores.getValor(TABLA_PARAMETROS_ENTRUST, "modoOffline", "desc");
    
    /**
     * Identificador del producto Entrust Token.
     */
    private static final String ID_PRODENTRUSTTOKEN       = "HTK";
    
    /**
     * Identificador del producto Entrust SoftToken.
     */
    private static final String ID_PRODENTRUSTSOFTTOKEN   = "STK";
    
    /**
     * Estado evento de negocio Rechazado.
     */
    private static final String EST_EVENTO_NEGOCIO_REC    = "R";
    
    /**
     * Estado evento de negocio Procesado.
     */
    private static final String EST_EVENTO_NEGOCIO_PRO    = "P";
    
    /**Atributo identificador de tabla de parametros.*/
    private static final String TABLA_PARAMETROS = "autenticacionldap.parametros";
    /**Nombre Servicio QR.*/
    private static final String SERVICIO_QR = "PagueDirecto";
    /**Session Bci.*/
    private static final String IDNAME = "sessionBci";  
    /**Tabla de parametros de SafeSigner.*/
    private static final String PARAMSAFESIGNER = "safesigner.parametros";
    /**Nomre del servicio.*/
    private static final String SAFE_SIGNER = "SafeSigner";
    /**Atributo identificador log.*/
    private transient Logger logger = (Logger) Logger.getLogger(this.getClass());
    /** PINID_PINCORP_TBANC. */
    private final int PINID_PINCORP_TBANC       = 100;
    /** PINID_PINCORP_WAPTBANC. */
    private final int PINID_PINCORP_WAPTBANC    = 111;
    /** PINID_PINCORP_PORTAL. */
    private final String PINID_PINCORP_PORTAL   = "150";
    /** PINID_PINCORP_EVEREST. */
    private final String PINID_PINCORP_EVEREST  = "151";
    /** PINID_PINCORP_BCI. */
    private final int PINID_PINCORP_BCI         = 110;
    /** PINID_INTRA. */
    private final int PINID_INTRA               = 120;
    /** PINID_PINCORP_BCIEXPRESS. */
    private final int PINID_PINCORP_BCIEXPRESS  = 130;
    /** PINID_CALLCENTER. */
    private final int PINID_CALLCENTER          = 160;
    /** PINID_MICROMATICOS. */
    private final int PINID_MICROMATICOS        = 200;
    /** PINID_PINCORP_BCIEXPRESSNEW. */
    private final int PINID_PINCORP_BCIEXPRESSNEW  = 230;
    /** PINID_CONOSUR. */
    private final int PINID_CONOSUR = 800;
    /** PINID_EMPRESAS_PYME. */
    private final int PINID_EMPRESAS_PYME       = 132;
    /** LARGO_RUT.*/
    private final int LARGO_RUT       = 8;
    /** pagosbean. */
    private ServiciosPagos pagosbean;
    /** pagoshome. */
    private ServiciosPagosHome pagoshome;
    /** segBean. */
    private ServiciosSeguridad segBean;
    /** segHome. */
    private ServiciosSeguridadHome segHome;
    /** miscBean. */
    private ServiciosMiscelaneos miscBean;
    /** miscHome. */
    private ServiciosMiscelaneosHome miscHome;
    /** clientesBean. */
    private ServiciosCliente clientesBean;
    /** clientesHome. */
    private ServiciosClienteHome clientesHome;
    /** cuentasbean. */
    private ServiciosCuentas cuentasbean;
    /** cuentashome. */
    private ServiciosCuentasHome cuentashome;
    /** bciexpress. */
    private BCIExpress bciexpress;
    /** bciexpresshome. */
    private BCIExpressHome bciexpresshome;
    /** ic. */
    private InitialContext ic;
    /** String para generar en base64 imagen con codigo QR.*/
    private String data;

    public void init(){
    }

    public String getServletInfo(){
        return "Controller de Login para BCI";
    }

    /**
     *
     * Registro de versiones:
     *<ul>
     * <li> 1.0 xx/xx/xxxx, Desconocido (?): Version inicial
     * 
     * <li>1.1 05/07/2013, Rodolfo Kafack Ghinelli (SEnTRA): Se agrega validacion de segunda clave SafeSigner.</li>
     * <li>1.3 30/10/2015, Diego Urrutia Guerra(Imagemaker) - Mario Lorca(BCI): Se agrega l�gica para realizar autenticacion de usuario en Active Directory.
     * Ademas se deja parametrico opci�n para realizar consulta en LDAP en caso de ocurrir problemas, cuando se baje LDAP, se debe eliminar
     * llamada a tabla de par�metros y eliminar validaci�n en donde se decide ir a autenticar.
     * <li>1.4 02/08/2017  Marcelo Fuentes (SEnTRA) - Ricardo Carrasco (Ing. Soft. BCI): Se agrega logica que 
     * revisa si se requiere autenticaci�n Entrust Token o Entrust SoftToken.
     * </li>
     *</ul>
     * @param req req.
     * @param res res.
     * @throws IOException IOException.
     * @throws ServletException ServletException.
     * @since 1.2
     */
    public void service(HttpServletRequest req, HttpServletResponse res)
            throws IOException, ServletException{
        
        if(getLogger().isInfoEnabled()){getLogger().info("[service][BCI_INI]");}
        String despacho     = null;
        String userId       = null;
        String pin          = null;
        SessionBCI session  = null;
        Canal canal         = null;
        String userIdrut    = null;
        String userIdusu    = null;
        String userIdAux    = null;
        String pin_param    = null;
        String serv_param   = null;
        String canalId      = null;
        String publicKey    = null;
        String msg          = null;
        Journal journal     = null;
        PagueDirecto pdir   = new PagueDirecto();
        String Rut          = null;
        boolean invalidSession = false;
	    String clave_param = null;
	    boolean cambiandoClave = false;
	    String vieneDeSeleccionConvenio=null;

        try{
            HttpSession ss = req.getSession(false);
            if (ss==null) {
                //se agrega logs, para saber si se esta creando por primera vez la session
                ss = req.getSession();
                if(getLogger().isInfoEnabled()){getLogger().info("[service] instanciando session "+ss.getId());}

            }
            else{   
                session = (SessionBCI)ss.getAttribute("sessionBci");
                if(getLogger().isInfoEnabled()){
                    getLogger().info("[service] obteniendo session ya creada "+ss.getId());
                    getLogger().info("[service] despacho ="+(session!=null?session.getAttrib("despacho"):""));
                }  
            }

            if(getLogger().isInfoEnabled()){
                getLogger().info("[service] obteniendo hora de creacion de la session "+ss.getCreationTime());
                getLogger().info("[service] obteniendo ultimo acceso de la session "+ss.getLastAccessedTime());
                getLogger().info("[service] obteniendo IP de acceso "+req.getRemoteAddr());
            }

            canalId = req.getParameter("canal");
            if(getLogger().isInfoEnabled()){
                getLogger().info("[service] obteniendo IP de canalId "+canalId);
                getLogger().info("[service] req.getAttribute canal "+ req.getAttribute("canal"));
                getLogger().info("[service] req.getAttribute redirectDesdeOtroLogin " + req.getAttribute("redirectDesdeOtroLogin"));
            }
        
            if(req.getAttribute("redirectDesdeOtroLogin")!=null && req.getAttribute("canal") != null ){
                int canalSitioOrigen=Integer.parseInt(String.valueOf(req.getAttribute("canal")));
                if(getLogger().isInfoEnabled()){getLogger().info("[service][LoginRedirect] canalSitioOrigen["+canalSitioOrigen+"]");}
                switch(canalSitioOrigen){
                    case PINID_EMPRESAS_PYME:
                        canalId = String.valueOf(PINID_PINCORP_BCIEXPRESSNEW);
                        break;
                }
                if(getLogger().isInfoEnabled()){getLogger().info("[service][LoginRedirect] canalId["+canalId+"]");}
                pin_param = String.valueOf(req.getAttribute("clave"));
                clave_param = String.valueOf(req.getAttribute("clave"));
                userIdrut = StringUtil.rellenaConCeros(String.valueOf(req.getAttribute("rut")), LARGO_RUT);
                if(getLogger().isInfoEnabled()){getLogger().info("[service][LoginRedirect] userIdrut["+userIdrut+"]");}
                userIdAux = String.valueOf(req.getAttribute("dig"));
                if(getLogger().isInfoEnabled()){getLogger().info("[service][LoginRedirect] userIdAux["+userIdAux+"]");}
		        cambiandoClave = (req.getAttribute("cambiaclave")!=null && String.valueOf(req.getAttribute("cambiaclave")).equalsIgnoreCase("S"));
		        if(getLogger().isInfoEnabled()){getLogger().info("[service][LoginRedirect] cambiandoClave["+cambiandoClave+"]");}
		        vieneDeSeleccionConvenio= String.valueOf(req.getAttribute("vieneDeSeleccionConvenio"));
            }
            else{
                if(getLogger().isInfoEnabled()){getLogger().info("[service][Login] Flujo con parameter");}
                pin_param = req.getParameter("clave");
                clave_param = req.getParameter("clave");
                userIdrut = req.getParameter("rut");
                if(getLogger().isInfoEnabled()){getLogger().info("[service] userIdrut["+userIdrut+"]");}
                userIdAux = req.getParameter("dig");
                if(getLogger().isInfoEnabled()){getLogger().info("[service] userIdAux["+userIdAux+"]");}
                cambiandoClave = (req.getParameter("cambiaclave")!= null && req.getParameter("cambiaclave").equalsIgnoreCase("S"));
                if(getLogger().isInfoEnabled()){getLogger().info("[service] cambiandoClave["+cambiandoClave+"]");}
            }

            //se loggean datos desde el request para validar los parametros del pague directo
            if(getLogger().isInfoEnabled()){
                getLogger().info("[service] SessionBCI["+session+"]");
                getLogger().info("[service] canal["+canalId+"]");
                getLogger().info("[service] action="+req.getParameter("action"));
                getLogger().info("[service] ip="+req.getParameter("ip"));
                getLogger().info("[service] cnvnum="+req.getParameter("cnvnum"));
                getLogger().info("[service] proto="+req.getParameter("proto"));
                getLogger().info("[service] rmiserver="+req.getParameter("rmiserver"));
                getLogger().info("[service] compra="+req.getParameter("compra"));
                getLogger().info("[service] cstenv="+req.getParameter("cstenv"));
                getLogger().info("[service] pagret="+req.getParameter("pagret"));
                getLogger().info("[service] bco="+req.getParameter("bco"));
                getLogger().info("[service] trx="+req.getParameter("trx"));
            }

            if ((req.getParameter("action") != null) && (req.getParameter("ip") != null) && (req.getParameter("cnvnum") != null) && (req.getParameter("proto") != null) && (req.getParameter("rmiserver") != null)
            		&& (req.getParameter("compra") != null) && (req.getParameter("cstenv") != null) && (req.getParameter("pagret") != null) && (req.getParameter("bco") != null) && (req.getParameter("trx") != null)) {
                canalId = "540";
            }
            if(getLogger().isInfoEnabled()){getLogger().info("[service] canal["+canalId+"]");}  
            //si el canal es null, se deja en blanco.
            if(canalId == null){
                canalId = "";
            }
            //Se agrega validacion de segundaclave para SafeSigner
            if (canalId.equals("safeSigner")) {
                if(getLogger().isInfoEnabled()){getLogger().info("[service] Se ingresa a validar safeSigner");}
                session = (SessionBCI) ss.getAttribute("sessionBci");
                pin_param = req.getParameter("codigoConfirmacionQR");

                if(getLogger().isInfoEnabled()){
                    getLogger().info("[service] Canal safeSigner");
                }

                if (session == null) {
                    if(getLogger().isInfoEnabled()){getLogger().info("[service] Session es null [" + (session == null) + "]");}
                    ss.invalidate();
                    invalidSession = true;
                    throw new SeguridadException("USERLOGIN");
                }

                Rut = session.getCliente().getFullRut();

                if(getLogger().isInfoEnabled()){getLogger().info("[service] Rut : " + Rut);}

                if (req.getAttribute("estadoPopUp") == null)
                    req.setAttribute("estadoPopUp", req
                            .getParameter("estadoPopUp") == null ? "true"
                            : String.valueOf(req.getParameter("estadoPopUp")));

                if (pin_param == null) {
                    if(getLogger().isInfoEnabled()){getLogger().info("[service] Ingresamos a solicitar el codigo QR ");}
                    SimpleDateFormat formateador = new SimpleDateFormat("yyyyMMddHHmm", new Locale("es","CL"));

                    Date fechaDate = new Date();
                    String fecha = formateador.format(fechaDate);
                    String nombresDeLLave = TablaValores.getValor(PARAMSAFESIGNER, SERVICIO_QR, "datosLlave");

                    if(getLogger().isInfoEnabled()){getLogger().info("[service] nombresDeLLave : " + nombresDeLLave);}
                    PagueDirecto pd = (PagueDirecto)ss.getAttribute("PagueDirecto");

                    String montoCompra = String.valueOf(pd.getMontoCompra()); 
                    String[] valoresDeLlave = { session.getCanal().getNombre(), fecha, montoCompra, SERVICIO_QR };

                    CamposDeLlaveTO campoDeLlaves = new CamposDeLlaveTO();
                    SafeSignerUtil safeSignerUtil = new SafeSignerUtil();

                    campoDeLlaves = safeSignerUtil.seteaCamposDeLlave(nombresDeLLave, valoresDeLlave);
                    LlaveSegundaClaveTO llaveSegundaClave = ControllerBCI.generarLlavesDeSegundaClave(req, IDNAME, SERVICIO_QR, campoDeLlaves);

                    if (llaveSegundaClave.isEstatus()) {
                        if(getLogger().isInfoEnabled()){getLogger().info("[service] Se genero la llave previa");}
                        Cliente cliente = session.getCliente();
                        ParametrosEstrategiaSegundaClaveTO 
                        parametrosEstrategiaSegundaClaveTO = new ParametrosEstrategiaSegundaClaveTO(SERVICIO_QR, cliente, session.getCanal(), session);

                        data = llaveSegundaClave.getLlave().getClaveAlfanumerica();
                        req.setAttribute("parametrosEstrategiaSegundaClaveTO", parametrosEstrategiaSegundaClaveTO);
                        req.setAttribute("data", data);
                        ss.setAttribute("imagenqr", data);
                        if(getLogger().isInfoEnabled()){getLogger().info("[service] Esta seteado el DATA : " + req.getAttribute("data"));}

                        req.setAttribute("servicioQR", SERVICIO_QR);
                        req.setAttribute("rut", String.valueOf(session.getCliente().getRut()));
                        req.setAttribute("dv", String.valueOf(session.getCliente().getDigito()));
                    }

                    this.getServletContext().getRequestDispatcher(res.encodeURL(session.getCanal().getPathJSP() + "/paguedirecto/safesigner/autenticacionSafeSigner.jsp")).forward(req, res);
                } 
                else {
                    if(getLogger().isInfoEnabled()){getLogger().info("[service] Ingresamos a Validar el codigo QR ");}
                    
                    String codigoConfirmacion = req.getParameter("codigoConfirmacionQR");
                    int estatusOK = Integer.parseInt(
                    TablaValores.getValor(PARAMSAFESIGNER, "estatusok", "valor"));
                    
                    if(getLogger().isInfoEnabled()){
                        getLogger().info("[service] Validamos parametros desde la session si es SafeSigner ");
                        getLogger().info("[service] identificadorPrioridadSegundaClave " + session.getAttrib("identificadorPrioridadSegundaClave"));
                        getLogger().info("[service] identificadorEstadoSegundaClave " + session.getAttrib("identificadorEstadoSegundaClave"));
                    }
                    if (session.getAttrib("identificadorPrioridadSegundaClave").equals("SafeSigner")
                            && session.getAttrib("identificadorEstadoSegundaClave").equals("Active")
                            && codigoConfirmacion.length() > 0) {
                        
                        RepresentacionAlfanumericaSegundaClaveTO segundaClave = new RepresentacionAlfanumericaSegundaClaveTO(pin_param);
                        segundaClave.setClaveAlfanumerica(codigoConfirmacion);
                        
                        ResultadoOperacionSegundaClaveTO resultado = ControllerBCI.validarSegundaClave(req, IDNAME, SERVICIO_QR, segundaClave);
                        if(resultado == null || resultado.getIdCodigoEstado() != estatusOK){
                            if(getLogger().isInfoEnabled()){getLogger().info("[service] Journalizamos error de login con Safesigner");}
                            journalizacion(session, EVE_LOGIN, SUB_COD_NOK, SAFE_SIGNER, ID_PRODINTERNET, null);
                            throw new SeguridadException("USERLOGIN");
                        }
                        else{
                            req.removeAttribute("flagSafeSigner");
                            session.cambiaEstadoMetodoAutenticacion("MPM",true);
                            session.removeAttrib("permitirAutentificacion");
                            despacho = (String)session.getAttrib("despacho");
                            if(despacho==null ||(despacho!=null && despacho.trim().equals(""))){
                                ss.invalidate();
                                invalidSession=true;
                                throw new SeguridadException("USERLOGIN");
                            }
                            session.removeAttrib("despacho");
                            //journalizacion caso exitoso de ingreso safeSigner
                            if(getLogger().isInfoEnabled()){getLogger().info("[service] Journalizamos login exitoso con Safesigner");}
                            journalizacion(session, EVE_LOGIN, SUB_COD_OK, SAFE_SIGNER, ID_PRODINTERNET, null);
                            this.getServletContext().getRequestDispatcher(res.encodeURL(despacho)).forward(req,res);
                            return;
                        }
                    }
                }
                return;
            }

			if (canalId.equals("passcode")){
				session = (SessionBCI)ss.getAttribute("sessionBci");                
				Rut     = session.getCliente().getFullRut();
				try {
					if (pin_param == null){
					    if(getLogger().isInfoEnabled()){getLogger().info("[service] rut["+Rut+"] Antes de llamar a Passcodepaso1");}
						if (PassCodePaso1(session)){
						    if(getLogger().isInfoEnabled()){getLogger().info("[service] rut["+Rut+"] Despues de llamar a Passcodepaso1");}
							this.getServletContext().getRequestDispatcher(res.encodeURL("/seguridadwls/RealizaCambioClave?cambio=0&paso=2")).forward(req, res);
						}
						else
							if(session.getCanal().getCanalID().equals("540") || session.getCanal().getCanalID().equals("540bci") || session.getCanal().getCanalID().equals("540tbc")){
								this.getServletContext().getRequestDispatcher(res.encodeURL(session.getCanal().getPathJSP() + "/paguedirecto/passcode/ingresopasscode.jsp")).forward(req,res);
							}
							else{
								this.getServletContext().getRequestDispatcher(res.encodeURL(session.getCanal().getPathJSP() + "/passcode/ingresopasscode.jsp")).forward(req,res);
							}
					}
					else
						PassCodePaso2(session, req, res, pin_param);

				}
				catch (wcorp.serv.seguridad.SeguridadException se) {
				    if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[service][BCI_FINEX][SeguridadException] Error ["+se.getMessage()+"]", se);}
					req.setAttribute("javax.servlet.jsp.jspException", se);
					if(session.getCanal().getCanalID().equals("540") || session.getCanal().getCanalID().equals("540bci") || session.getCanal().getCanalID().equals("540tbc")){
                        if(req.getAttribute("estadoPopUp") == null)
						    req.setAttribute("estadoPopUp", req.getParameter("estadoPopUp")==null?"true": String.valueOf(req.getParameter("estadoPopUp")));
						this.getServletContext().getRequestDispatcher(res.encodeURL(session.getCanal().getPathJSP() + "/paguedirecto/error.jsp")).forward(req,res);
					}
					else{
						this.getServletContext().getRequestDispatcher(res.encodeURL(session.getCanal().getPathJSP() + "/passcode/ErrorPassCode.jsp")).forward(req,res);
					}
				}
				return;
			}

			if (canalId.equals("token")){
			    if(getLogger().isInfoEnabled()){getLogger().info("[service] rut["+Rut+"]Canal es 'token'");}

				session = (SessionBCI)ss.getAttribute("sessionBci");
				pin_param = req.getParameter("claveToken");

				/*revisar que la session no sea null*/
				if(getLogger().isInfoEnabled()){getLogger().info("[service] SessionBCI ="+session);}

				if(session==null){
				    if(getLogger().isInfoEnabled()){getLogger().info("[service] La SessionBCI, no puede ser null cuando el canal es TOKEN");}
					ss.invalidate();
					invalidSession=true;
					throw new SeguridadException("USERLOGIN");                	 
				}

				Rut = session.getCliente().getFullRut();
                if (session.getCanal().getCanalID().equals(String.valueOf(PINID_EMPRESAS_PYME))){
                    Rut=session.getRutUsuario()+"-"+session.getDvUsuario();
                }

                if(getLogger().isInfoEnabled()){getLogger().info("[service] rut["+Rut+"] Canal es 'token'");}
                if(req.getAttribute("estadoPopUp") == null)
				req.setAttribute("estadoPopUp", req.getParameter("estadoPopUp")==null?"true": String.valueOf(req.getParameter("estadoPopUp")));
				if (pin_param == null) {
					if(session.getCanal().getCanalID().equals("540") || session.getCanal().getCanalID().equals("540bci") || session.getCanal().getCanalID().equals("540tbc")){
						this.getServletContext().getRequestDispatcher(res.encodeURL(session.getCanal().getPathJSP() + "/paguedirecto/token/autenticacionToken.jsp")).forward(req,res);
					}
					else{
					    if(getLogger().isInfoEnabled()){getLogger().info("[service] rut["+Rut+"] Solicitara token");}
						String flagToken= (String) req.getAttribute("flagToken");
						if (flagToken!= null)
							req.setAttribute("flagToken", flagToken);
						if(getLogger().isInfoEnabled()){getLogger().info("[service] session.getCanal().getCanalID()["+session.getCanal().getCanalID()+"]");}
                        if (session.getCanal().getCanalID().equals(String.valueOf(PINID_EMPRESAS_PYME))){
                            if(getLogger().isInfoEnabled()){getLogger().info("[service] despacho a ["+session.getCanal().getPathJSP() + "/token/autenticacionTokenParametrico.jsp]");}
                            this.getServletContext().getRequestDispatcher(res.encodeURL(session.getCanal().getPathJSP() + "/token/autenticacionTokenParametrico.jsp")).forward(req,res);
                        }
                        else{
                            if(getLogger().isInfoEnabled()){getLogger().info("[service] despacho a ["+session.getCanal().getPathJSP() + "/token/autenticacionToken.jsp]");}
                            this.getServletContext().getRequestDispatcher(res.encodeURL(session.getCanal().getPathJSP() + "/token/autenticacionToken.jsp")).forward(req,res);
                        }
                    }
					return;
				}
				else {
					//valida token
				    if(getLogger().isInfoEnabled()){getLogger().info("[service] rut["+Rut+"] Ejecutar� 'autenticaToken()'");}
					//autenticaToken(session, req, res, pin_param);
					try{
					    session.autentica(pin_param,canalId);
                    }
					catch(SeguridadException se){
					    if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[service]["+Rut+"][BCI_FINEX][SeguridadException] Error al autenticar Token, se journaliza NOK.");}
						journalizacion(session, EVE_LOGIN, SUB_COD_NOK, String.valueOf(session.getCliente().getRut()),ID_PRODTOKEN, null);
                    	throw se;
                    }
					if(getLogger().isInfoEnabled()){getLogger().info("[service] rut["+Rut+"] Token autenticado correctamente, cambiado estado");}
					req.removeAttribute("flagToken");
					session.cambiaEstadoMetodoAutenticacion("TOK",true);

					// codigo agregado por el proyecto de mejoras de seguridad que permite
					// interactuar con con ControllerBCI en la reautenticaci�n del token
					// para pagos y transferencias.
					String autentPagoTransfer = (String)session.getAttrib("autentToken");
					session.removeAttrib("autentToken");
					if(getLogger().isInfoEnabled()){getLogger().info("[service] rut["+Rut+"] autentPagoTransfer ["+autentPagoTransfer+"]");}
					if(autentPagoTransfer!=null && autentPagoTransfer.equals("true"))
						session.setAttrib("tokenAutorizadoPagoTransfer", "true");

					/*Validar que la session BCI no sean nula y la variable despacho exista*/                    
					despacho = (String)session.getAttrib("despacho");
					if(getLogger().isInfoEnabled()){getLogger().info("[service] despacho ="+despacho!=null?despacho:"NO_EXISTE");}

					if(despacho==null ||(despacho!=null && despacho.trim().equals(""))){
					    if(getLogger().isInfoEnabled()){getLogger().info("[service] El despacho no puede ser null o blanco cuando el canal es TOKEN");}
						ss.invalidate();
						invalidSession=true;
						throw new SeguridadException("USERLOGIN");
					}

					session.removeAttrib("despacho");
					if(canalId == null || (canalId != null && !canalId.trim().equals("215"))){
						if (session.getAttrib("origenLogin")!=null){
						    if(getLogger().isInfoEnabled()){getLogger().info("[service] journalizo ClaveInternet OK CON TOKEN");}
							session.removeAttrib("origenLogin");
							journalizacion(session,EVE_LOGIN,SUB_COD_OK,userIdrut,ID_PRODINTERNET,null);
						}
					}
					if(getLogger().isInfoEnabled()){getLogger().info("[service] rut["+Rut+"] despachando a (" + despacho + ") desde login (token)");}
					/* Mejoras Seguridad Fase 3, journalizacion de eventos claves*/
					if(canalId == null || (canalId != null && !canalId.trim().equals("215"))){
						journalizacion(session,EVE_LOGIN,SUB_COD_OK,String.valueOf(session.getCliente().getRut()),ID_PRODTOKEN,null);
					}

					this.getServletContext().getRequestDispatcher(res.encodeURL(despacho)).forward(req,res);
					if(getLogger().isInfoEnabled()){getLogger().info("[service] rut["+Rut+"] vuelve de (" + despacho + ")");}
					return;
				}
			}

			if (canalId.equals("supuser")){
			    if(getLogger().isInfoEnabled()){getLogger().info("[service] Canal if" + canalId);}
				session = (SessionBCI)ss.getAttribute("sessionBci");
				try {
					if (clave_param == null){
					    if(getLogger().isInfoEnabled()){getLogger().info("[service] Clave canal null" );}
						this.getServletContext().getRequestDispatcher(res.encodeURL(session.getCanal().getPathJSP() + "/supuser/ingresosuperuser.jsp")).forward(req,res);
					}
					else
						supUserPaso1(session, req, res, clave_param);
				}
				catch (wcorp.serv.seguridad.SeguridadException se) {
					//Agregado para proyecto Mejoras Seguridad FASE 1
					se = (SeguridadException)ControlErrorMsg.modificaCodigo((Exception)se, session);
					if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[service][BCI_FINEX][SeguridadException] Error ["+se.getMessage()+"]", se);}
					req.setAttribute("javax.servlet.jsp.jspException", se);
					this.getServletContext().getRequestDispatcher(res.encodeURL(session.getCanal().getPathJSP() + "/supuser/errorsuperuser.jsp")).forward(req,res);
				}
				return;
			}

            // login para micromaticos
			if (canalId.equals("600")) {
			    if(getLogger().isInfoEnabled()){getLogger().info("[service] Voy a inicializar un micromatico.");}
				canal = new Canal(canalId);
				int pid = Integer.parseInt(canal.getPinID());
				if(getLogger().isInfoEnabled()){getLogger().info("[service] El PinID es <"+pid+">.");}
				if (pid==PINID_MICROMATICOS) {
					HttpSession ses=req.getSession(true);
					userId=req.getRemoteAddr();
					String paso=req.getParameter("paso");
					if ((paso==null)||(paso.trim().equals(""))) 
					    throw new wcorp.util.GeneralException("ESPECIAL", "Datos incompletos.");
					String pan=req.getParameter("pan");
					if (( pan==null)||( pan.trim().equals(""))) 
					    throw new wcorp.util.GeneralException("ESPECIAL", "Datos incompletos.");
					userIdAux=paso+"_"+pan;
					if(getLogger().isInfoEnabled()){getLogger().info("[service] userId=<"+userId+">  userIdAux=<"+userIdAux+">");}
					session = new SessionBCI(canalId, userId, userIdAux);
					if(session!=null)
						session.setIdSession(ss.getId());
					ses.setAttribute("sessionBci", session);

					// ahora derivamos al servlet micromaticos
					despacho="/micromaticoswls/Micromaticos?paso="+paso+"&pan="+pan;
					if(getLogger().isInfoEnabled()){getLogger().info("[service] DESPACHO A <"+despacho+">");}
					this.getServletContext().getRequestDispatcher(res.encodeURL(despacho)).forward(req, res);

					return;
				}
				else {
				    if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[service][BCI_FINEX][GeneralException] Error: No conozco esta combinaci�n canal/pin: "+canalId+"/"+pid+".");}
					throw new wcorp.util.GeneralException("ESPECIAL", "Mecanismo de validaci�n inesperado.");
				}
			}
			
            if (canalId.equals("entrustSoftToken")) {
                getLogger().info("[service] Se ingresa a validar entrustSoftToken");
                String modoAutenticacion = req.getParameter("modoAutenticacion");
                session = (SessionBCI) ss.getAttribute("sessionBci");
                pin_param = req.getParameter("codigoConfirmacionQR");
                
                if (session == null) {
                    if(getLogger().isInfoEnabled()){
                        getLogger().info("[service] Session es null [" + (session == null) + "]");
                    }
                    ss.invalidate();
                    invalidSession = true;
                    throw new SeguridadException("USERLOGIN");
                }
                
                if (req.getAttribute("estadoPopUp") == null) {
                    req.setAttribute("estadoPopUp", req.getParameter("estadoPopUp") == null ? "true" : String.valueOf(req.getParameter("estadoPopUp")));
                }
                
                if(getLogger().isInfoEnabled()){
                    getLogger().info("[service] modoAutenticacion [" + modoAutenticacion + "]");
                }
                if (modoAutenticacion == null) {
                    String usuarioId = "";
                    if (session.getCanal().getCanalID().equals(String.valueOf(PINID_EMPRESAS_PYME))) {
                        usuarioId = session.getRutUsuario() + String.valueOf(session.getDvUsuario().charAt(0));
                    }
                    else {
                        usuarioId = String.valueOf(session.getCliente().getRut()) + String.valueOf(session.getCliente().getDigito());
                    }
                    UsuarioSegundaClaveTO usuarioSegundaClave = new UsuarioSegundaClaveTO(usuarioId, 
                                                                                          ControllerBCI.obtieneDominio(session.getCanal().getCanalID()), 
                                                                                          session.getCanal().getCanalID());
                    
                    String requiereLlave = TablaValores.getValor(TABLA_SEGURIDAD, "SegundaClaveRequiereLlave", ENTRUST_SOFTTOKEN);
                    
                    if (getLogger().isInfoEnabled()) {
                        getLogger().info("[asignaMetodosAutenticacion] Se agrega parametro para EntrustSoftToken");
                        getLogger().info("[asignaMetodosAutenticacion] EntrustSoftToken, requiereLlave[" + requiereLlave + "]");
                    }
                    
                    ParametrosEstrategiaSegundaClaveTO paramSegundaClave = new ParametrosEstrategiaSegundaClaveTO(SERVICIO_QR, session.getCliente(), canal, session);
                    paramSegundaClave.setUsuarioSegundaClave(usuarioSegundaClave);
                    EstadoSegundaClaveTO estadoSegundaClave = ControllerBCI.estadoSegundaClave(paramSegundaClave);
                    
                    if(getLogger().isInfoEnabled()){
                        getLogger().info("posT consulta estado 2da clave con valor " + estadoSegundaClave.getIdSegundaClave().getNombre() + ", " + estadoSegundaClave.toString());
                        getLogger().info("posT consulta estado 2da is status " + estadoSegundaClave.isEstatus());
                    }
                    
                    if (estadoSegundaClave.isEstatus()) {
                        if (requiereLlave != null && requiereLlave.equals("true")) {
                            CamposDeLlaveTO campoDeLlaves = obtenerCamposDellaves(ss);
                            if(getLogger().isInfoEnabled()){
                                getLogger().info("datos para Estado Clave EntrustSoftToken");
                                getLogger().info("datos para Servicio " + SERVICIO_QR);
                                getLogger().info("datos para usuarioSegundaClave " + usuarioSegundaClave.toString());
                                getLogger().info("datos para campoDeLlaves " + campoDeLlaves.toString());
                            }
                            LlaveSegundaClaveTO llave = ControllerBCI.generarLlave(ENTRUST_SOFTTOKEN, SERVICIO_QR, usuarioSegundaClave, campoDeLlaves, ONLINE);
                            if(getLogger().isInfoEnabled()){
                                getLogger().info("Llave: " + llave.toString());
                                getLogger().info("Estado de la llave : " + llave.isEstatus());
                            }
                            session.setAttrib("transactionId", llave.getSerial());
                            session.setAttrib("camposDeLLavePD", campoDeLlaves);
                            req.setAttribute("frecuenciaEntrust", ControllerBCI.getFrecuenciaEntrust());
                            req.setAttribute("timeOutEntrust", ControllerBCI.getTimeOutEntrust());
                            this.getServletContext().getRequestDispatcher(res.encodeURL(session.getCanal().getPathJSP() + "/paguedirecto/entrustsofttoken/autenticacionEntrustSoftTokenOnline.jsp")).forward(req, res);
                            return;
                        }
                    }else {
                        throw new SeguridadException("USERLOGIN");
                    }
                    
                }
                else {
                    boolean validacionOk = false;
                    if(getLogger().isInfoEnabled()){
                        getLogger().info("[service] modoAutenticacion [" + modoAutenticacion + "]");
                    }
                    if (modoAutenticacion.equals(ONLINE)) {
                        String codeConfirmation = (String)session.getAttrib("transactionId");
                        validacionOk = validaEntrustSoftToken(ss, req, res, ONLINE, codeConfirmation);
                    }
                    else if (modoAutenticacion.equals(OFFLINE)) {
                        if (pin_param == null) {
                            boolean llaveGenerada = false;
                            try {
                                llaveGenerada = generarLlaveQR(req, res, canalId);
                            
	                        }
	                        catch (Exception e) {
	                            if(getLogger().isEnabledFor(Level.ERROR)){
	                                getLogger().error("[service] Exception", e);
	                            }
	                        }
                            
                            if(getLogger().isInfoEnabled()){
                                getLogger().info("[service] llaveGenerada [" + llaveGenerada + "]");
                            }
                            if (llaveGenerada) {
                                this.getServletContext().getRequestDispatcher(res.encodeURL(session.getCanal().getPathJSP()
	                                    + "/paguedirecto/entrustsofttoken/autenticacionEntrustSoftTokenOffline.jsp")).forward(req, res);
                            }
                            else {
                                this.getServletContext().getRequestDispatcher(res.encodeURL(session.getCanal().getPathJSP() + "/paguedirecto/error.jsp")).forward(req, res);
                            }
                            return;
                        }
                        else {
                            validacionOk = validaEntrustSoftToken(ss, req, res, OFFLINE, pin_param);
                        }
                    }
                    
                    if(!validacionOk){
                        getLogger().info("[service] Journalizamos error de login con entrustSoftToken");
                        journalizacion(session, EVE_LOGIN, SUB_COD_NOK, String.valueOf(session.getCliente().getRut()), ID_PRODENTRUSTSOFTTOKEN, null, modoAutenticacion, EST_EVENTO_NEGOCIO_REC);
                        throw new SeguridadException("USERLOGIN");
                    }
                    else {
                        req.removeAttribute("flagEntrustSoftToken");
                        session.cambiaEstadoMetodoAutenticacion(ID_PRODENTRUSTSOFTTOKEN, true);
                        session.removeAttrib("permitirAutentificacion");
                        despacho = (String)session.getAttrib("despacho");
                        if(despacho==null ||(despacho!=null && despacho.trim().equals(""))){
                            ss.invalidate();
                            invalidSession=true;
                            throw new SeguridadException("USERLOGIN");
                        }
                        session.removeAttrib("despacho");
                        getLogger().info("[service] Journalizamos login exitoso con entrustSoftToken");
                        journalizacion(session, EVE_LOGIN, SUB_COD_OK, String.valueOf(session.getCliente().getRut()), ID_PRODENTRUSTSOFTTOKEN, null, modoAutenticacion, EST_EVENTO_NEGOCIO_PRO);
                        this.getServletContext().getRequestDispatcher(res.encodeURL(despacho)).forward(req,res);
                        return;
                    }
                }
                return;
            }
            
            if (canalId.equals("entrustToken")) {
                if(getLogger().isInfoEnabled()){
                    getLogger().info("[service] rut["+Rut+"]Canal es 'entrustToken'");
                }
                
                session = (SessionBCI)ss.getAttribute("sessionBci");
                pin_param = req.getParameter("claveToken");
                
                if(getLogger().isInfoEnabled()){
                    getLogger().info("[service] SessionBCI ="+session);
                }
                
                if(session == null){
                    getLogger().info("[service] La SessionBCI, no puede ser null cuando el canal es entrustToken");
                    ss.invalidate();
                    invalidSession=true;
                    throw new SeguridadException("USERLOGIN");
                }
                
                Rut = session.getCliente().getFullRut();
                if (session.getCanal().getCanalID().equals(String.valueOf(PINID_EMPRESAS_PYME))){
                    Rut=session.getRutUsuario()+"-"+session.getDvUsuario();
                }
                
                if(getLogger().isInfoEnabled()){
                    getLogger().info("[service] rut["+Rut+"] Canal es 'entrustToken'");
                }
                if(req.getAttribute("estadoPopUp") == null)
                req.setAttribute("estadoPopUp", req.getParameter("estadoPopUp")==null?"true": String.valueOf(req.getParameter("estadoPopUp")));
                if (pin_param == null) {
                    if(session.getCanal().getCanalID().equals("540") || session.getCanal().getCanalID().equals("540bci") || session.getCanal().getCanalID().equals("540tbc")){
                        this.getServletContext().getRequestDispatcher(res.encodeURL(session.getCanal().getPathJSP() + "/paguedirecto/entrusttoken/autenticacionEntrustToken.jsp")).forward(req,res);
                    }
                    else{
                        if(getLogger().isInfoEnabled()){
                            getLogger().info("[service] rut["+Rut+"] Solicitara entrustToken");
                        }
                        String flagEntrustToken= (String) req.getAttribute("flagEntrustToken");
                        if (flagEntrustToken!= null)
                            req.setAttribute("flagEntrustToken", flagEntrustToken);
                        if(getLogger().isInfoEnabled()){
                            getLogger().info("[service] session.getCanal().getCanalID()["+session.getCanal().getCanalID()+"]");
                        }
                        if (session.getCanal().getCanalID().equals(String.valueOf(PINID_EMPRESAS_PYME))){
                            if(getLogger().isInfoEnabled()){
                                getLogger().info("[service] despacho a ["+session.getCanal().getPathJSP() + "/entrusttoken/autenticacionEntrustTokenParametrico.jsp]");
                            }
                            this.getServletContext().getRequestDispatcher(res.encodeURL(session.getCanal().getPathJSP() + "/entrusttoken/autenticacionEntrustTokenParametrico.jsp")).forward(req,res);
                        }
                        else{
                            if(getLogger().isInfoEnabled()){
                                getLogger().info("[service] despacho a ["+session.getCanal().getPathJSP() + "/entrusttoken/autenticacionEntrustToken.jsp]");
                            }
                            this.getServletContext().getRequestDispatcher(res.encodeURL(session.getCanal().getPathJSP() + "/entrusttoken/autenticacionEntrustToken.jsp")).forward(req,res);
                        }
                    }
                    return;
                }
                else {
                    if(getLogger().isInfoEnabled()){
                        getLogger().info("[service] rut["+Rut+"] Ejecutar� 'autenticaentrustToken()'");
                    }
                    try{
                        String usuarioId = "";
                        if (session.getCanal().getCanalID().equals(String.valueOf(PINID_EMPRESAS_PYME))) {
                            usuarioId = session.getRutUsuario() + String.valueOf(session.getDvUsuario().charAt(0));
                        }
                        else {
                            usuarioId = String.valueOf(session.getCliente().getRut()) + String.valueOf(session.getCliente().getDigito());
                        }
                        UsuarioSegundaClaveTO usuarioSegundaClave = new UsuarioSegundaClaveTO(usuarioId, ControllerBCI.obtieneDominio(session.getCanal().getCanalID()), session.getCanal().getCanalID());
                        
                        boolean resultado = ControllerBCI.verificarAutenticacion(ENTRUST_TOKEN, "", usuarioSegundaClave, null, null, pin_param);
                    }
                    catch(SeguridadException se){
                        if(getLogger().isEnabledFor(Level.ERROR)){
                            getLogger().error("[service]["+Rut+"][BCI_FINEX][SeguridadException] Error al autenticar EntrustToken, se journaliza NOK.");
                        }
                        journalizacion(session, EVE_LOGIN, SUB_COD_NOK, String.valueOf(session.getCliente().getRut()), ID_PRODENTRUSTTOKEN, null, null, EST_EVENTO_NEGOCIO_REC);
                        throw se;
                    }
                    if(getLogger().isInfoEnabled()){
                        getLogger().info("[service] rut["+Rut+"] entrustToken autenticado correctamente, cambiado estado");
                    }
                    req.removeAttribute("flagEntrustToken");
                    session.cambiaEstadoMetodoAutenticacion(ID_PRODENTRUSTTOKEN, true);
                    
                    String autentPagoTransfer = (String)session.getAttrib("autentEntrustToken");
                    session.removeAttrib("autentEntrustToken");
                    if(getLogger().isInfoEnabled()){
                        getLogger().info("[service] rut["+Rut+"] autentPagoTransfer ["+autentPagoTransfer+"]");
                    }
                    if(autentPagoTransfer!=null && autentPagoTransfer.equals("true"))
                        session.setAttrib("entrustTokenAutorizadoPagoTransfer", "true");
                    
                    despacho = (String)session.getAttrib("despacho");
                    if(getLogger().isInfoEnabled()){
                        getLogger().info("[service] despacho ="+despacho!=null?despacho:"NO_EXISTE");
                    }
                    
                    if(despacho==null ||(despacho!=null && despacho.trim().equals(""))){
                        if(getLogger().isInfoEnabled()){getLogger().info("[service] El despacho no puede ser null o blanco cuando el canal es EntrustToken");}
                        ss.invalidate();
                        invalidSession=true;
                        throw new SeguridadException("USERLOGIN");
                    }
                    
                    session.removeAttrib("despacho");
                    if(canalId == null || (canalId != null && !canalId.trim().equals("215"))){
                        if (session.getAttrib("origenLogin")!=null){
                            if(getLogger().isInfoEnabled()){
                                getLogger().info("[service] journalizo ClaveInternet OK CON entrustToken");
                            }
                            session.removeAttrib("origenLogin");
                            journalizacion(session,EVE_LOGIN,SUB_COD_OK,userIdrut,ID_PRODINTERNET,null);
                        }
                    }
                    if(getLogger().isInfoEnabled()){
                        getLogger().info("[service] rut["+Rut+"] despachando a (" + despacho + ") desde login (entrustToken)");
                    }
                    /* Mejoras Seguridad Fase 3, journalizacion de eventos claves*/
                    if(canalId == null || (canalId != null && !canalId.trim().equals("215"))){
                        journalizacion(session,EVE_LOGIN,SUB_COD_OK,String.valueOf(session.getCliente().getRut()), ID_PRODENTRUSTTOKEN, null, null, EST_EVENTO_NEGOCIO_PRO);
                    }
                    
                    this.getServletContext().getRequestDispatcher(res.encodeURL(despacho)).forward(req,res);
                    if(getLogger().isInfoEnabled()){
                        getLogger().info("[service] rut["+Rut+"] vuelve de (" + despacho + ")");
                    }
                    return;
                }
            }
            
			if(getLogger().isInfoEnabled()){
			    getLogger().info("[service] rut["+Rut+"] BCI - Ver. 11.11.2005");
			    getLogger().info("[service] rut["+Rut+"] El canal no es micromatico");
			}

            if (canalId == null | (canalId != null && canalId.trim().equals(""))) {
                if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[service][BCI_FINEX][GeneralException] rut["+Rut+"] Canal inv�lido");}
                throw new wcorp.util.GeneralException("ESPECIAL","Canal inv�lido");
            }

            try {
                canal = new Canal(canalId);
            }
            catch (Exception e) {
                if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[service][BCI_FINEX][GeneralException] rut["+Rut+"] Canal inv�lido, al instanciar Canal("+canalId+")");}
                throw new wcorp.util.GeneralException("ESPECIAL","Canal inv�lido");
            }

            serv_param = req.getParameter("serv");
            int pid = Integer.parseInt(canal.getPinID());
            switch(pid){
            case PINID_PINCORP_BCI:         // PIN Corporativo BCINet
            case PINID_PINCORP_TBANC:       // PIN Corporativo TBanc
            case PINID_PINCORP_WAPTBANC:    // PIN Corporativo WAP TBANC
            case PINID_PINCORP_BCIEXPRESS:  // PIN Corporativo BCIExpress
            case PINID_CONOSUR:  // PIN Conosur

                if(userIdrut != null && userIdAux == null){
                    userIdAux = userIdrut.substring(userIdrut.length() - 1);
                    userIdrut = userIdrut.substring(0, userIdrut.length() - 1);
                    if (userIdrut.endsWith("-"))
                        userIdrut = userIdrut.substring(0, userIdrut.length() - 1);
                }
                //se quitan los blancos de las variables del rut
                userIdrut = userIdrut != null? userIdrut.trim() : userIdrut;
                userIdAux = userIdAux != null? userIdAux.trim() : userIdAux;

                if(userIdrut != null && !userIdrut.equals("") &&  userIdAux != null && !userIdAux.equals("")){
                    if(getLogger().isInfoEnabled()){getLogger().info("[service] rut["+userIdrut+"] Validar Rut");}
                    try {//se agrega catch para validar que el rut sea valido y no arroje NumberFormatException, cuando en el cuerpo del rut viene una letra
                        if (!RUTUtil.validaDigitoVerificador(userIdrut.trim(), userIdAux.trim().toUpperCase())){
                            if(getLogger().isInfoEnabled()){getLogger().info("[service] Rut Inv�lido.");}
                            throw new wcorp.util.GeneralException("ESPECIAL", "Rut Inv�lido.");
                        }
                    } 
                    catch (Exception e) {
                        if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[service][BCI_FINEX][Exception] Error Rut Inv�lido.");}
                        throw new wcorp.util.GeneralException("ESPECIAL", "Rut Inv�lido.");
                    }
                    if(getLogger().isInfoEnabled()){getLogger().info("[service] rut["+userIdrut+"-"+userIdAux+"] Validar Rut - 2");}
                }

				if (canal.getNombre().equals("paguedirecto")){
					HttpSession ses   = null;
					try{
			   			String paso = req.getParameter("paso");
			   			if(getLogger().isInfoEnabled()){getLogger().info("[service] rut["+userIdrut+"-"+userIdAux+"] paso ["+paso+"]");}
						if (paso == null) {
							canalId = canalId + req.getParameter("bco");
							if (req.getParameter("compra").trim().equals("") || req.getParameter("trx").trim().equals("")) {
								throw new PagueDirectoException("PD-SINPARAM-" + req.getParameter("bco"));
							}
							PagueDirectoPaso0(ses, req, res, canalId);
                            return;
						}
						if (paso.equals("1")){
							ses     = req.getSession(false);
							canalId = canalId+((PagueDirecto)ses.getAttribute("PagueDirecto")).getBco();
							canal   = new Canal(canalId);
							pid     = Integer.parseInt(canal.getPinID());

							if (PagueDirectoPaso1(ses, req, res, canal) == false)
								return;
							pdir = (PagueDirecto)ses.getAttribute("PagueDirecto");
						}
						if (paso.equals("2")){
							ses = req.getSession(false);
							PagueDirectoPaso2(req);
							pdir = (PagueDirecto)ses.getAttribute("PagueDirecto");
						}
					}
					catch(Exception e){
					    if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[service]["+userIdrut+"][BCI_FINEX][Exception] Error ["+e.getMessage()+"]", e);}
						if (ses!=null) {
							pdir = (PagueDirecto)ses.getAttribute("PagueDirecto");
							if (pdir != null)
								enviarRespuesta(pdir.getCnvnum(), pdir.getTrx());
							if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[Login] rut["+userIdrut+"-"+userIdAux+"] ERROR : " + e.toString());}
						}
						throw e;
					}
				}

                if (canalId.equals("230") || canalId.equals("231") ||canalId.equals("232") || canalId.equals("906")){
                    try{
                        String paso=null;
                        if(getLogger().isInfoEnabled()){getLogger().info("[service] vieneDeSeleccionConvenio["+vieneDeSeleccionConvenio+"]");}
                        if (vieneDeSeleccionConvenio != null && vieneDeSeleccionConvenio.equals("true")){
                            paso = String.valueOf(req.getAttribute("paso"));
                        }
                        else{
                            paso = req.getParameter("paso");
                        }
                        
                        ic          = JNDIConfig.getInitialContext();
                        segHome     = (ServiciosSeguridadHome)ic.lookup("wcorp.serv.seguridad.ServiciosSeguridad");
                        segBean     = (ServiciosSeguridad)segHome.create();
                        if(getLogger().isInfoEnabled()){getLogger().info("[service] Canal [" + canalId + "] userIdrut [" + userIdrut + "] userIdAux [" + userIdAux + "] Paso [" + paso + "]");}

                        if (canalId.equals("230") || canalId.equals("906")){
                            try{
                                if (paso == null) {  // viene del ingreso inicial de clave del operador
                                    if(getLogger().isInfoEnabled()){getLogger().info("[service] Obtiene lista de convenios BEL del operador");}
                                    long userRUT = (new Long(userIdrut)).longValue();
                                    ListaConvenios[] listaConv = segBean.consultaListaConvenios(userRUT, "1");

                                    if(getLogger().isInfoEnabled()){getLogger().info("[service] Crea sesion de operador Empresa.");}
                                    session = new SessionBCI(canalId, userIdrut, userIdAux);
                                    if(session!=null)
                                        session.setIdSession(ss.getId());

                                    session.setIP(req.getRemoteAddr());
                                    String auxCanalId = "";
                                    boolean sw_prov = false;
                                    if(getLogger().isInfoEnabled()){getLogger().info("[service] " + userIdrut + "-" + userIdAux + " "+session.indCambioClave);}
                                    if (listaConv == null || listaConv.length == 0) { // Caso Banco en Linea (CASO 1)
                                        if(getLogger().isInfoEnabled()){getLogger().info("[service] Autentica usando ping corporativo para canal bci.cl.");}
                                        auxCanalId = String.valueOf(PINID_PINCORP_BCI);
                                        if(getLogger().isInfoEnabled()){getLogger().info("[service] auxCanalId [" + auxCanalId + "]");}
                                        req.setAttribute("ValidaClaveCanal", auxCanalId);

                                        //Captura de excepci�n para clientes empresa sin convenio ( Banco en L�nea)							  
                                        try{
                                            session.autentica(pin_param, auxCanalId);
                                        }
                                        catch (SeguridadException ex) {
                                            if (logger.isEnabledFor(Level.ERROR)) {logger.error("Excepci�n capturada por Clave Expirada para Banco en l�nea ex.getCodigo()=[" + ex.getCodigo() + "]");}
                                            if (ex.getCodigo().equals("0433")) {
                                                getLogger().info("[service] indCambioClave1 true");
                                                session.indCambioClave = true;
                                                session.primeraVez=false;
                                                session.activa();    
                                            }
                                            else{
                                                throw ex;
                                            }
                                        }
                                        sw_prov = true;
                                        session.setAttrib("clinocnv", "1");
                                    }
                                    else {
                                        //Captura de excepci�n para clientes empresa con convenio ( Express e Epyme)
                                        try{
                                            session.autentica(pin_param);
                                        }
                                        catch (SeguridadException ex) {
                                            if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[service][BCI_FINEX][SeguridadException] Clave Expirada para clientes con convenio ex.getCodigo()=[" + ex.getCodigo() + "]");}
                                            if ("0433".equals(ex.getCodigo())) {
												getLogger().info("[service] indCambioClave2 true");
                                                session.indCambioClave = true;
                                                session.primeraVez=false;
                                                session.activa();    
                                            }
                                            else{
                                                throw ex;
                                            }												
                                        }
                                        auxCanalId = canalId;
                                    }
                                    session.setAttrib("claveWLS", pin_param); // para autenticar posteriormente en MOPA
                                    session.setAttrib("convWLS" , auxCanalId);

                                    HttpSession hs = req.getSession(true);
                                    int tu = canal.getTimeOutUso();
                                    if (tu > 0)
                                        hs.setMaxInactiveInterval(tu * 60);
                                    hs.setAttribute("sessionBci", session);										

                                    session.setAttrib("listaConvenios", listaConv);
                                    if(getLogger().isInfoEnabled()){
                                        getLogger().info("[service] Cantidad de convenios BEL de [" + userIdrut + "] es: " + listaConv.length);
                                        getLogger().info("[service]" + userIdrut + "-" + userIdAux + " "+session.indCambioClave+ " " + session.primeraVez);
                                    }

                                    if(getLogger().isInfoEnabled()){getLogger().info("[service] cambiandoClave[" + cambiandoClave + "]");}

                                    if (cambiandoClave || session.indCambioClave || session.primeraVez){
                                        if(getLogger().isInfoEnabled()){getLogger().info("[service] Cambiando Clave");}
                                        if(canalId.equals("906")){
                                            despacho = "/bciexmovilwls/common/advertenciaCambioClave.jsp";
                                            this.getServletContext().getRequestDispatcher(res.encodeURL(despacho)).forward(req,res);
                                            return;
                                        }
                                        session.indCambioClave=true;
                                        hs.setAttribute("sessionBci", session);
                                        despacho = "/seguridadwls/RealizaCambioClave?cambio=0&paso=0";
                                        this.getServletContext().getRequestDispatcher(res.encodeURL(despacho)).forward(req,res);
                                        return;
                                    }

                                    if (listaConv.length == 0){ // Caso Banco en Linea (CASO 1)
                                        String rutUsuario        = userIdrut;
                                        String dvUsuario         = userIdAux;
                                        if(getLogger().isInfoEnabled()){
                                            getLogger().info("[service] BcoLinea NoCnv - rut = " + rutUsuario + "-" + dvUsuario);
                                            getLogger().info("[service] BcoLinea NoCnv - auxCanalId = " + auxCanalId);
                                        }
                                        session.setAttrib("parametros","usunumrut=" + ((rutUsuario.length() < 8)? "0" : "") + rutUsuario + "&usuvrtrut=" + dvUsuario);
                                        session.setAttrib("convWLS", auxCanalId);
                                        session.setAttrib("claveWLS", pin_param);
                                        if (!sw_prov) {
                                            session.autoriza();
                                            session.activa();
                                            if(canalId.equals("230")){
                                                if(getLogger().isInfoEnabled()){getLogger().info("[service] despachando hacia '/belwls/bciexpress/enrolamiento/Enrolamiento.do?metodo=inicio&despachoFinal=perfilUCEP_op00'");}
                                                this.getServletContext().getRequestDispatcher(res.encodeURL("/belwls/bciexpress/enrolamiento/Enrolamiento.do?metodo=inicio&despachoFinal=perfilUCEP_op00")).forward(req,res);
                                            }
                                            else if(canalId.equals("906")){
                                                if(getLogger().isInfoEnabled()){getLogger().info("[service] despachando hacia '/bciexmovilwls/consultaPerfilesUCEP?opcion=00'");}
                                                this.getServletContext().getRequestDispatcher(res.encodeURL("/bciexmovilwls/consultaPerfilesUCEP?opcion=00")).forward(req,res);
                                            }
                                        }
                                        else  {
                                            session.autoriza();
                                            session.activa();

                                            if(getLogger().isInfoEnabled()){getLogger().info("[service] serv_param..: " + serv_param);}

	                                        String numeroEventoJournal = 
                                            journalizacionIngresoSitioEmpresas(session, 
                                                EVE_LOGIN, 
                                                SUB_COD_OK, 
                                                "Intento de conexi&oacute;n exitoso Banco en L�nea, R.U.T.: " 
                                                + session.getCliente().getRut() 
                                                + "-" + session.getCliente().getDigito() );
                                
	                                        if(getLogger().isInfoEnabled()){getLogger().info("[service] termino journalizacion empresas banco en linea, " 
                                                        + numeroEventoJournal 
                                                        + " RUT : " + session.getCliente().getRut());
                                            }

                                            if (serv_param!=null){
                                                if (serv_param.equals("comprasWLS")){
                                                    if(getLogger().isInfoEnabled()){getLogger().info("[service] /belwls/bciexpress/enrolamiento/Enrolamiento.do?metodo=inicio&despachoFinal=defineAmbiente");}
                                                    session.setAttrib("servicioInicial", serv_param);
                                                    if(canalId.equals("906")) {
                                                        throw new GeneralException("ESPECIAL","Servicio No Habilitado para el Canal Movil");
                                                    }
                                                    this.getServletContext().getRequestDispatcher(res.encodeURL("/belwls/bciexpress/enrolamiento/Enrolamiento.do?metodo=inicio&despachoFinal=defineAmbiente")).forward(req,res);
                                                }                                                 
                                            }
                                            if(canalId.equals("230")){                                          
                                                if(getLogger().isInfoEnabled()){getLogger().info("[service] despachando hacia '/belwls/bciexpress/enrolamiento/Enrolamiento.do?metodo=inicio&despachoFinal=perfilUCEP_op01'");}
                                                this.getServletContext().getRequestDispatcher(res.encodeURL("/belwls/bciexpress/enrolamiento/Enrolamiento.do?metodo=inicio&despachoFinal=perfilUCEP_op01")).forward(req,res);
                                            }
                                            else if(canalId.equals("906")){
                                                if(getLogger().isInfoEnabled()){getLogger().info("[service] despachando hacia '/bciexmovilwls/consultaPerfilesUCEP?opcion=01'");}
                                                this.getServletContext().getRequestDispatcher(res.encodeURL("/bciexmovilwls/consultaPerfilesUCEP?opcion=01")).forward(req,res);
                                            }
                                        }
                                    }
                                    else if (listaConv.length == 1){	// Caso Monoconvenio (CASO 2)
                                        session = new SessionBCI(canalId,
                                                String.valueOf(listaConv[0].getRutEmpresa()),
                                                String.valueOf(listaConv[0].getDigitoVerificador()),
                                                userIdrut, userIdAux,
                                                listaConv[0].getNumConvenio(),
                                                listaConv[0].getNombreRazonSocial(),
                                                listaConv[0].getNombre().trim() + " " + listaConv[0].getApellidoPaterno().trim() + " " + listaConv[0].getApellidoMaterno().trim());
                                        
                                        session.setAttrib("parametros", "usunumrut=" + (userIdrut.length() >= 8 ? "" : "0") + userIdrut + "&usuvrtrut=" + userIdAux);
                                        session.setAttrib("tipoUser", listaConv[0].getTipoUsuario());
                                        session.setAttrib("nomEmp", listaConv[0].getNombreRazonSocial());
                                        session.setAttrib("listaConvenios", listaConv);

                                        if(getLogger().isInfoEnabled()){getLogger().info("[service] antes de PerfilUsuario .. Caso Monoconvenio (CASO 2)" + String.valueOf(userRUT) + " " +  String.valueOf(listaConv[0].getNumConvenio()) + " " + String.valueOf(listaConv[0].getRutEmpresa()));}
                                        PerfilUsuario(req, String.valueOf(userRUT),String.valueOf(listaConv[0].getNumConvenio()), String.valueOf(listaConv[0].getRutEmpresa()));

                                        // establecemos el perfil y tipo de usuario 
                                        if(getLogger().isInfoEnabled()){
                                            getLogger().info("[service] setPerfil: [" + hs.getAttribute("perfil") + "]");
                                            getLogger().info("[service] setTipoUsuario: [" + hs.getAttribute("tipoUsuario") + "]");
                                        }

                                        session.setPerfil((String) hs.getAttribute("perfil"));
                                        session.setTipoUsuario((String) hs.getAttribute("tipoUsuario"));

                                        if(getLogger().isInfoEnabled()){getLogger().info("[service] despues de PerfilUsuario .. Caso Monoconvenio (CASO 2)" + String.valueOf(userRUT) + " " +  String.valueOf(listaConv[0].getNumConvenio()) + " " + String.valueOf(listaConv[0].getRutEmpresa()));}

                                        session.tipoUser = 'T';
                                        session.setIP(req.getRemoteAddr());
                                        session.setAttrib("claveWLS", pin_param);
                                        session.autoriza();
                                        session.activa();

                                        session = setServiciosEmpresas(session, userRUT, listaConv[0].getNumConvenio(), listaConv[0].getRutEmpresa());
                                        hs.setAttribute("sessionBci", session);

                                        if(getLogger().isInfoEnabled()){getLogger().info("[service] serv_param: " + serv_param);}

                                        String numeroEventoJournal = journalizacionIngresoSitioEmpresas(session, 
                                                EVE_LOGIN, 
                                                SUB_COD_OK, 
                                                "Intento de conexi&oacute;n exitoso monoconvenio, R.U.T.: " 
                                                + session.getCliente().getRut() 
                                                + "-" + session.getCliente().getDigito());
                    
                                        if(getLogger().isInfoEnabled()){getLogger().info("[service] termino journalizacion empresas monoconvenio : " 
                                                    + session.getCliente().getRut() 
                                                    + " monoconvenio : " + numeroEventoJournal);
                                        }

                                        if (serv_param!=null){
                                            if (serv_param.equals("comprasWLS")){
                                                if(getLogger().isInfoEnabled()){getLogger().info("[service] /belwls/bciexpress/enrolamiento/Enrolamiento.do?metodo=inicio&despachoFinal=defineAmbiente");}
                                                session.setAttrib("servicioInicial", serv_param);
                                                if(canalId.equals("906")) {
                                                    if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[service][BCI_FINEX][GeneralException] Servicio No Habilitado para el Canal Movil");}
                                                    throw new GeneralException("ESPECIAL","Servicio No Habilitado para el Canal Movil");
                                                }
                                                this.getServletContext().getRequestDispatcher(res.encodeURL("/belwls/bciexpress/enrolamiento/Enrolamiento.do?metodo=inicio&despachoFinal=defineAmbiente")).forward(req,res);
                                            }                                               
                                        }

                                        if ("SUP".equals(listaConv[0].getTipoUsuario())) {
                                            hs.setAttribute("listaConvenio", listaConv[0]);
                                            if(getLogger().isInfoEnabled()){getLogger().info("[service] despachando hacia /belwls/bciexpress/enrolamiento/Enrolamiento.do?metodo=inicio&despachoFinal=usuario");}
                                            if(canalId.equals("906")) {
                                                if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[service][BCI_FINEX][GeneralException] Servicio No Habilitado para el Canal Movil");}
                                                throw new GeneralException("ESPECIAL","Servicio No Habilitado para el Canal Movil");
                                            }
                                            this.getServletContext().getRequestDispatcher(res.encodeURL("/belwls/bciexpress/enrolamiento/Enrolamiento.do?metodo=inicio&despachoFinal=usuario")).forward(req,res);
                                        }
                                        else {
                                            if(canalId.equals("230")){
                                                if(getLogger().isInfoEnabled()){getLogger().info("[service] despachando hacia '/belwls/bciexpress/enrolamiento/Enrolamiento.do?metodo=inicio&despachoFinal=perfilUCEP_op00'");}
                                                this.getServletContext().getRequestDispatcher(res.encodeURL("/belwls/bciexpress/enrolamiento/Enrolamiento.do?metodo=inicio&despachoFinal=perfilUCEP_op00")).forward(req,res);
                                            }
                                            else if(canalId.equals("906")){
                                                if(getLogger().isInfoEnabled()){getLogger().info("[service] despachando hacia '/bciexmovilwls/consultaPerfilesUCEP?opcion=00'");}
                                                this.getServletContext().getRequestDispatcher(res.encodeURL("/bciexmovilwls/consultaPerfilesUCEP?opcion=00")).forward(req,res);
                                            }
                                        }
                                    }
                                    else {	// Caso Multiconvenio (CASO 3)
                                        session.autoriza();
                                        session.activa();

                                        if(getLogger().isInfoEnabled()){getLogger().info("[service] serv_param..: " + serv_param);}

                                        String numeroEventoJournal = journalizacionIngresoSitioEmpresas(session, 
                                                EVE_LOGIN, SUB_COD_OK, 
                                                "Intento de conexi&oacute;n exitoso MultiConvenio, R.U.T.: " 
                                                + session.getCliente().getRut() 
                                                + "-" + session.getCliente().getDigito());
                        
                                        if(getLogger().isInfoEnabled()){getLogger().info("[service] termino journalizacion empresas multiconvenio: "+numeroEventoJournal+", RUT: "+session.getCliente().getRut());}

                                        if (serv_param!=null){
                                            if (serv_param.equals("comprasWLS")){
                                                if(getLogger().isInfoEnabled()){getLogger().info("[service] /belwls/bciexpress/enrolamiento/Enrolamiento.do?metodo=inicio&despachoFinal=defineAmbiente");}
                                                session.setAttrib("servicioInicial", serv_param);
                                                if(canalId.equals("906")) {
                                                    if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[service][BCI_FINEX][GeneralException] Servicio No Habilitado para el Canal Movil");}
                                                    throw new GeneralException("ESPECIAL","Servicio No Habilitado para el Canal Movil");
                                                }
                                                this.getServletContext().getRequestDispatcher(res.encodeURL("/belwls/bciexpress/enrolamiento/Enrolamiento.do?metodo=inicio&despachoFinal=defineAmbiente")).forward(req,res);
                                            }                                                
                                        }
                                        if(canalId.equals("230")){
                                            if(getLogger().isInfoEnabled()){getLogger().info("[service] despachando hacia '/belwls/bciexpress/enrolamiento/Enrolamiento.do?metodo=inicio&despachoFinal=convenio'");}
                                            this.getServletContext().getRequestDispatcher(res.encodeURL("/belwls/bciexpress/enrolamiento/Enrolamiento.do?metodo=inicio&despachoFinal=convenio")).forward(req,res);
                                        }
                                        else if(canalId.equals("906")){
                                            if(getLogger().isInfoEnabled()){getLogger().info("[service] despachando hacia '/bciexmovilwls/bciexpress/convenios.jsp'");}
                                            this.getServletContext().getRequestDispatcher(res.encodeURL("/bciexmovilwls/bciexpress/convenios.jsp")).forward(req,res);
                                        }
                                    }
                                }
                                else if (paso.equals("1")) {  // viene de la seleci�n de convenio BEL
                                    HttpSession hs = req.getSession(false);
                                    session = (SessionBCI) hs.getAttribute("sessionBci");
                                    
                                    String rutEmpresa="";
                                    String digitoVerificador="";
                                    String rutUsuario="";
                                    String dvUsuario="";
                                    String idConvenio="";
                                    String nombreCliente="";
                                    String tipoUser="";
                                    
                                    if(req.getAttribute("redirectDesdeOtroLogin")!=null){
                                        if(getLogger().isInfoEnabled()){
                                            getLogger().info("[service][LoginRedirect] rutEmpresa["+String.valueOf(req.getAttribute("rutEmpresa"))+"]");
                                            getLogger().info("[service][LoginRedirect] digitoVerificador["+String.valueOf(req.getAttribute("digitoVerificador"))+"]");
                                            getLogger().info("[service][LoginRedirect] rutUsuario["+String.valueOf(req.getAttribute("rutUsuario"))+"]");
                                            getLogger().info("[service][LoginRedirect] digUsuario["+String.valueOf(req.getAttribute("digUsuario"))+"]");
                                            getLogger().info("[service][LoginRedirect] idConvenio["+String.valueOf(req.getAttribute("idConvenio"))+"]");
                                            getLogger().info("[service][LoginRedirect] nombreEmpresa["+String.valueOf(req.getAttribute("nombreEmpresa"))+"]");
                                            getLogger().info("[service][LoginRedirect] tipoUsuario["+String.valueOf(req.getAttribute("tipoUsuario"))+"]");
                                        }
                                        
                                        rutEmpresa          = String.valueOf(req.getAttribute("rutEmpresa"));
                                        digitoVerificador   = String.valueOf(req.getAttribute("digitoVerificador"));
                                        rutUsuario          = String.valueOf(req.getAttribute("rutUsuario"));
                                        dvUsuario           = String.valueOf(req.getAttribute("digUsuario"));
                                        idConvenio          = String.valueOf(req.getAttribute("idConvenio"));
                                        nombreCliente       = String.valueOf(req.getAttribute("nombreEmpresa"));
                                        tipoUser            = String.valueOf(req.getAttribute("tipoUsuario"));
                                         
                                    }
                                    else{
                                        if(getLogger().isInfoEnabled()){
                                            getLogger().info("[service] rutEmpresa["+req.getParameter("rutEmpresa")+"]");
                                            getLogger().info("[service] digitoVerificador["+req.getParameter("digitoVerificador")+"]");
                                            getLogger().info("[service] rutUsuario["+req.getParameter("rutUsuario")+"]");
                                            getLogger().info("[service] digUsuario["+req.getParameter("digUsuario")+"]");
                                            getLogger().info("[service] idConvenio["+req.getParameter("idConvenio")+"]");
                                            getLogger().info("[service] nombreEmpresa["+req.getParameter("nombreEmpresa")+"]");
                                            getLogger().info("[service] tipoUsuario["+req.getParameter("tipoUsuario")+"]");
                                        }
                                        rutEmpresa          = req.getParameter("rutEmpresa");
                                        digitoVerificador   = req.getParameter("digitoVerificador");
                                        rutUsuario          = req.getParameter("rutUsuario");
                                        dvUsuario           = req.getParameter("digUsuario");
                                        idConvenio          = req.getParameter("idConvenio");
                                        nombreCliente       = req.getParameter("nombreEmpresa");
                                        tipoUser            = req.getParameter("tipoUsuario");
                                    }
                                    // Rescata datos anteriores de la Sesion
                                    
                                    String nombreOperador = session.getNomOperador();
                                    if (nombreOperador == null) {
                                        nombreOperador = session.getCliente().getFullName();
                                    }
                                    
                                    String clavePaso = (String) session.getAttrib("claveWLS");
                                    ListaConvenios[] listaConv = (ListaConvenios[]) session.getAttrib("listaConvenios");
                                    if(getLogger().isInfoEnabled()){getLogger().info("[service] rutEmp[" + rutEmpresa + "][" + digitoVerificador + "] rutUsu[" + rutUsuario + "][" + dvUsuario + "] cnv[" + idConvenio + "] nomCli[" + nombreCliente + "] nomOpe[" + nombreOperador + "]");}

                                    // Crea nueva sesion y le setea valores
                                    session = new SessionBCI(canalId,rutEmpresa,digitoVerificador,rutUsuario,dvUsuario,idConvenio,nombreCliente,nombreOperador);
                                    if(session!=null)
                                        session.setIdSession(ss.getId());
                                    session.setAttrib("parametros","usunumrut=" + ((rutUsuario.length() < 8)? "0" : "") + rutUsuario + "&usuvrtrut=" + dvUsuario);
                                    session.setAttrib("tipoUser",tipoUser);
                                    session.setAttrib("nomEmp",nombreCliente);
                                    session.setAttrib("Referer","");
                                    session.setAttrib("listaConvenios", listaConv);
                                    session.setAttrib("claveWLS", clavePaso);
                                    session.tipoUser = 'T';
                                    session.setIP(req.getRemoteAddr());

                                    if(getLogger().isInfoEnabled()){getLogger().info("[service] antes de PerfilUsuario" + rutUsuario + " " +  idConvenio + " " + rutEmpresa);}
                                    PerfilUsuario(req, rutUsuario,idConvenio,rutEmpresa);

                                    // establecemos el perfil y tipo de usuario 
                                    if(getLogger().isInfoEnabled()){
                                        getLogger().info("[service] antes de setPerfil y setTipoUsuario ..:: rut usuario..:[" + rutUsuario + "] idconvenio..:[" +  idConvenio + "] rut empresa..:[" + rutEmpresa + "]");
                                        getLogger().info("[service] setPerfil..::[" + hs.getAttribute("perfil") + "]");
                                        getLogger().info("[service] setTipoUsuario..::[" + hs.getAttribute("tipoUsuario") + "]");
                                    }
                                    session.setPerfil((String) hs.getAttribute("perfil"));
                                    session.setTipoUsuario((String) hs.getAttribute("tipoUsuario"));

                                    if(getLogger().isInfoEnabled()){getLogger().info("[service] fin de setPerfil y setTipoUsuario ..:: rut usuario..:[" + rutUsuario + "] idconvenio..:[" +  idConvenio + "] rut empresa..:[" + rutEmpresa + "]");}

                                    session.autoriza();
                                    session.activa();

                                    if(!tipoUser.equals("SUP"))
                                        session = setServiciosEmpresas(session, new Long(rutUsuario).longValue(), idConvenio, new Long(rutEmpresa).longValue());

                                    hs.setAttribute("sessionBci", session);

                                    // Busca el convenio para ver el tipo de usuario
                                    int nCnv = -1;
                                    String rutEmpA = String.valueOf(Long.valueOf(rutEmpresa.trim()));
                                    String numCnvA = idConvenio.trim();
                                    for (int i=0; i < listaConv.length && nCnv < 0; i++) {
                                        String rutEmpB = String.valueOf(listaConv[i].getRutEmpresa());
                                        String numCnvB  = listaConv[i].getNumConvenio().trim();
                                        if (rutEmpA.equals(rutEmpB)) {
                                            if (numCnvB.equals(numCnvA)) {
                                                nCnv = i;
                                            }
                                        }
                                    }

                                    if ("SUP".equals(listaConv[nCnv].getTipoUsuario())) {
                                        hs.setAttribute("listaConvenio", listaConv[nCnv]);
                                        if(getLogger().isInfoEnabled()){getLogger().info("[service] /belwls/bciexpress/supervisores/usuarios.do?metodo=cargarUsuarios");}
                                        if(canalId.equals("906")) {
                                            if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[service][BCI_FINEX][GeneralException] Servicio No Habilitado para el Canal Movil");}
                                            throw new GeneralException("ESPECIAL","Servicio No Habilitado para el Canal Movil");
                                        }
                                        this.getServletContext().getRequestDispatcher(res.encodeURL("/belwls/bciexpress/supervisores/usuarios.do?metodo=cargarUsuarios")).forward(req,res);
                                    }
                                    else {
                                        if(canalId.equals("230")){									
                                            if(getLogger().isInfoEnabled()){getLogger().info("[service] despachando hacia '/belwls/consultaPerfilesUCEP?opcion=02'");}
                                            this.getServletContext().getRequestDispatcher(res.encodeURL("/belwls/consultaPerfilesUCEP?opcion=02")).forward(req,res);
                                        }
                                        else if(canalId.equals("906")){
                                            if(getLogger().isInfoEnabled()){getLogger().info("[service] despachando hacia '/bciexmovilwls/consultaPerfilesUCEP?opcion=02'");}
                                            this.getServletContext().getRequestDispatcher(res.encodeURL("/bciexmovilwls/consultaPerfilesUCEP?opcion=02")).forward(req,res);
                                        }
                                    }
                                }
                            }
                            catch (GeneralException ex){
                                if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[service][BCI_FINEX][GeneralException] Error[" + ex.getMessage() + "]", ex);}
                                if (session != null){
                                    //Agregado para proyecto Mejoras Seguridad FASE 1
                                    ex = (GeneralException)ControlErrorMsg.modificaCodigo((Exception)ex, session);
                                    /* Mejoras Seguridad Fase 3, journalizacion de eventos claves*/
                                    if(canalId == null || (canalId != null && !canalId.trim().equals("215"))){
                                        journalizacion(session,EVE_LOGIN,SUB_COD_ERROR,userIdrut,ID_PRODINTERNET,ex.getCodigo());
                                    }
                                }
                                throw ex;
                            }
                            catch (Exception ex){
                                if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[service][BCI_FINEX][GeneralException] Error[" + ex.getMessage() + "]", ex);}
                                if (session != null){
                                    //Agregado para proyecto Mejoras Seguridad FASE 1
                                    ex = ControlErrorMsg.modificaCodigo(ex, session);
                                    /* Mejoras Seguridad Fase 3, journalizacion de eventos claves*/
                                    if(canalId == null || (canalId != null && !canalId.trim().equals("215"))){
                                        journalizacion(session,EVE_LOGIN,SUB_COD_ERROR,userIdrut,ID_PRODINTERNET,ex.getMessage());
                                    }
                                }
                                throw ex;
                            }
                        }
                    }
                    catch (ClientesException ex){
                        if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[service][BCI_FINEX][ClientesException] Error[" + ex.getMessage() + "]", ex);}
                        if (ex.getFullMessage().toUpperCase().indexOf("CLIENTE NO EXISTE") != -1)
                            req.setAttribute("javax.servlet.jsp.jspException", new SeguridadException("USERLOGIN"));
                        else
                            req.setAttribute("javax.servlet.jsp.jspException", ex);
                        if(!canalId.equals("906")){
                            this.getServletContext().getRequestDispatcher(res.encodeURL("/belwls/common/Error.jsp")).forward(req,res);
                        }
                        else {
                            this.getServletContext().getRequestDispatcher(res.encodeURL("/bciexmovilwls/common/Error.jsp")).forward(req,res);
                        }
                        session.cerrar();
                    }
                    catch (SeguridadException ex){
                        if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[service][BCI_FINEX][SeguridadException] Error[" + ex.getMessage() + "]", ex);}
                        //Agregado para proyecto Mejoras Seguridad FASE 1
                        ex = (SeguridadException)ControlErrorMsg.modificaCodigo((Exception)ex, session);
                        if (ex.getCodigo().equalsIgnoreCase("USERLOGIN"))
                            req.setAttribute("javax.servlet.jsp.jspException", new SeguridadException("USERLOGIN"));
                        else
                            req.setAttribute("javax.servlet.jsp.jspException", ex);
                        if(!canalId.equals("906")){
                            this.getServletContext().getRequestDispatcher(res.encodeURL("/belwls/common/Error.jsp")).forward(req,res);
                        }
                        else {
                            this.getServletContext().getRequestDispatcher(res.encodeURL("/bciexmovilwls/common/Error.jsp")).forward(req,res);
                        }
                        session.cerrar();
                    }
                    catch(Exception e){
                        if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[service][BCI_FINEX][Exception] Error[" + e.getMessage() + "]", e);}
                        //Agregado para proyecto Mejoras Seguridad FASE 1
                        e = ControlErrorMsg.modificaCodigo(e, session);
                        req.setAttribute("javax.servlet.jsp.jspException", e);
                        if(!canalId.equals("906")){
                            this.getServletContext().getRequestDispatcher(res.encodeURL("/belwls/common/Error.jsp")).forward(req,res);
                        }
                        else {
                            this.getServletContext().getRequestDispatcher(res.encodeURL("/bciexmovilwls/common/Error.jsp")).forward(req,res);
                        }
                        session.cerrar();
                    }
                    return;
                }
                break;
            case PINID_INTRA: // Usuarios Internos UYP
                userIdusu = req.getParameter("usuario");
                pin_param = req.getParameter("clave");
                if (logger.isDebugEnabled()) {getLogger().info("[service] el usuario UYP es: " +  userIdusu);}
                break;
            case PINID_CALLCENTER:// Txs Para CallCenter BCINET y/o TBanc
                if (canalId.equals("160")){
                    if(getLogger().isInfoEnabled()){getLogger().info("[service] TeleCanal 1: " + canalId);}
                    try{
                        ic      = JNDIConfig.getInitialContext();
                        segHome = (ServiciosSeguridadHome)ic.lookup("wcorp.serv.seguridad.ServiciosSeguridad");
                        segBean = (ServiciosSeguridad)segHome.create();

						String paso     = req.getParameter("paso");
                        HttpSession ses = null;

                        if (paso == null){
                            long rut = new Integer(req.getParameter("rut")).longValue();
                            char dv  = req.getParameter("dv").charAt(0);
                            ses      = req.getSession(true);
                            ConsultaClave cc     = segBean.consultaClave(rut);

                            if (cc.getFlagSybaseTandem() == 0) {
                                EstadoPin estadoPin = segBean.estadoPin("  ", rut, dv, "16");
                                if(getLogger().isInfoEnabled()){getLogger().info("[service] estadoPin: " + estadoPin.estado);}
                                if (estadoPin.estado.equals("01")) {
                                    cc.codEstado = "BLQ";
                                }
                            }
                            if (cc.estadoCliente == null){
                                if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[service][BCI_FINEX][GeneralException] Estimado cliente, usted no posee clave para este canal.");}
                                throw new wcorp.util.GeneralException("ESPECIAL", "Estimado cliente, usted no posee clave para este canal.||");
                            }
                            else if (cc.estadoCliente.equals("VIG") && !cc.codEstado.equals("BLQ")){
                                ses.setAttribute("CallCenterRut"          , String.valueOf(rut));
                                ses.setAttribute("CallCenterDv"           , String.valueOf(dv));
                                ses.setAttribute("CallCenterClave"        , cc.clave);
                                ses.setAttribute("CallCenterCodRespuesta" , String.valueOf(cc.codRespuesta));
                                ses.setAttribute("flagSybaseTandem", new Integer(cc.getFlagSybaseTandem()));

                                this.getServletContext().getRequestDispatcher(res.encodeURL(canal.getPathJSP() + "/seguridad/login_clave.jsp")).forward(req,res);
                                return;
                            }
                            else if ((cc.estadoCliente.equals("VIG") && cc.codEstado.equals("BLQ")) || (cc.estadoCliente.equals("SEG"))){
                                if(getLogger().isInfoEnabled()){getLogger().info("[service][BCI_FINOK][ERROR] Estimado cliente, su cuenta est� bloqueada o presenta restricciones.");}
                                wcorp.util.GeneralException se = new wcorp.util.GeneralException("ESPECIAL", "Estimado cliente, su cuenta est� bloqueada o presenta restricciones.||");
                                req.setAttribute("javax.servlet.jsp.jspException", se);
                                this.getServletContext().getRequestDispatcher(res.encodeURL("/portalwls/seguridad/error.jsp?rut=" + String.valueOf(rut) + "&dv=" + String.valueOf(dv))).forward(req,res);
                                return;
                            }
                            else if (!((cc.estadoCliente.equals("VIG")) || (cc.estadoCliente.equals("SEG")))){
                                if(getLogger().isInfoEnabled()){getLogger().info("[service][BCI_FINOK][ERROR] Estimado cliente, su cuenta est� en proceso de apertura.");}
                                throw new wcorp.util.GeneralException("ESPECIAL", "Estimado cliente, su cuenta est� en proceso de apertura.||");
                            }
                            else{
                                if(getLogger().isInfoEnabled()){getLogger().info("[service][BCI_FINOK][ERROR] Condici�n no esperada.");}
                                throw new wcorp.util.GeneralException("ESPECIAL", "Condici�n no esperada.||");
                            }
                        }
                        
                        if (paso.equals("1")){
                            if(getLogger().isInfoEnabled()){getLogger().info("[service] PASO 1");}
                            String codResp = null;

                            try{
                                ses = req.getSession(false);
                                session = (SessionBCI)ses.getAttribute("sessionBci");

                                if (ses.getAttribute("CallCenterRut") != null){
                                    codResp = (String)ses.getAttribute("CallCenterCodRespuesta");
                                    Integer flagSybaseTandem = (Integer) ses.getAttribute("flagSybaseTandem");
                                    if(getLogger().isInfoEnabled()){getLogger().info("[service] tvl (PASO 1): flagSybaseTandem" + flagSybaseTandem);}

                                    int    dig1 = new Integer(req.getParameter("dig1")).intValue();
                                    int    dig2 = new Integer(req.getParameter("dig2")).intValue();
                                    char   clv1 = req.getParameter("clave1").toUpperCase().charAt(0);
                                    char   clv2 = req.getParameter("clave2").toUpperCase().charAt(0);

                                    boolean validaClaveTB = false;

                                    if (flagSybaseTandem.intValue() == -1) {
                                        validaClaveTB = segBean.validaClaveTB(dig1, dig2, clv1, clv2, (String) ses.getAttribute("CallCenterClave"));
                                    }
                                    else {
                                        DatosParaValidacionDeClaveDTO datosParaValidacionDeClaveDTO = new DatosParaValidacionDeClaveDTO();
                                        datosParaValidacionDeClaveDTO.setCanal("101");
                                        datosParaValidacionDeClaveDTO.setRutDelCliente(Long.parseLong((String)ses.getAttribute("CallCenterRut")));
                                        datosParaValidacionDeClaveDTO.setDigitoVerificador(RUTUtil.calculaDigitoVerificador(datosParaValidacionDeClaveDTO.getRutDelCliente()));
                                        datosParaValidacionDeClaveDTO.setIndicadorDeValidacion(DatosParaValidacionDeClaveDTO.TIPO_VALIDACION_PARCIAL);
                                        datosParaValidacionDeClaveDTO.setCantidadDeCaracteres("02");
                                        datosParaValidacionDeClaveDTO.setCantidadDeGrupos("01");
                                        datosParaValidacionDeClaveDTO.setCampoVariable(StringUtil.rellenaConCeros(String.valueOf(dig1),2)
                                                + StringUtil.rellenaConCeros(String.valueOf(dig2),2)
                                                + String.valueOf(clv1)
                                                + String.valueOf(clv2));
                                        validaClaveTB = segBean.validarClaveTbanc(datosParaValidacionDeClaveDTO);
                                    }

                                    if (validaClaveTB) {
                                        if (((String)ses.getAttribute("CallCenterCodRespuesta")).equals("S")){
                                            int num = 0;
                                            while(num < 1 || num > 5){
                                                num = (int)Math.abs(Math.round((Math.random() * 10)));
                                            }
                                            ConsultaPreguntas cp = segBean.consultaPreguntas(new Integer((String)ses.getAttribute("CallCenterRut")).longValue(), num);
                                            ses.setAttribute("CallCenterPreg" , cp.pregunta1);
                                            ses.setAttribute("CallCenterResp" , cp.respuesta1);
                                            this.getServletContext().getRequestDispatcher(res.encodeURL(canal.getPathJSP() + "/seguridad/login_pregunta.jsp")).forward(req,res);
                                            return;
                                        }
                                        else{
                                            if(getLogger().isInfoEnabled()){getLogger().info("[service] IP : " + req.getRemoteAddr());}
                                            String tipoTrato = (String)session.getAttrib("TipoTratoClienteBCITBANC");

                                            if (tipoTrato.equals("TBANC"))
                                                tipoTrato = "TBN";

                                            CtiCommand cti = new CtiCommand( req.getRemoteAddr() );
                                            boolean ok = cti.activaTCorp( (String)ses.getAttribute("CallCenterRut"), ((String)ses.getAttribute("CallCenterDv")).charAt(0), tipoTrato );
                                            if(ok){
                                                String nuevoEst = segBean.modificaEstadoClave(new Integer((String)ses.getAttribute("CallCenterRut")).longValue(), 'S');
                                                if(canalId == null || (canalId != null && !canalId.trim().equals("215"))){
                                                    journalizacion(session,EVE_LOGIN,SUB_COD_AUTBAN,userIdrut,ID_PRODINTERNET,null);
                                                }
                                                this.getServletContext().getRequestDispatcher(res.encodeURL("/portalwls/portal/Escritorio?opcion=52&rut="+(String)ses.getAttribute("CallCenterRut")+"&dv="+(String)ses.getAttribute("CallCenterDv")+"&cliente=' '")).forward(req,res);
                                                return;
                                            }
                                            else {
                                                if (logger.isEnabledFor(Level.ERROR)) {logger.debug("Error al enviar mensaje activaTCorp(" + (String)ses.getAttribute("CallCenterRut") + ((String)ses.getAttribute("CallCenterDv")).charAt(0) + "," + tipoTrato + ")");}
                                                req.setAttribute("javax.servlet.jsp.jspException", new GeneralException("ESPECIAL","Ha ocurrido un error de comunicaci&oacute;n por recarga en llamadas a servicios. Por favor reintente"));
                                                this.getServletContext().getRequestDispatcher(res.encodeURL(session.getCanal().getPathJSP() + "/common/Error.jsp")).forward(req,res);
                                                return;
                                            }
                                        }
                                    }
                                    else{
                                        String nuevoEst = segBean.modificaEstadoClave(new Integer((String)ses.getAttribute("CallCenterRut")).longValue(), 'N');
                                        if (nuevoEst.equals("BLQ")){
                                            if (flagSybaseTandem.intValue() == 0) {
                                                long rut = new Long( (String) ses.getAttribute("CallCenterRut")).longValue();
                                                segBean.bloquearClaveInternetTbanc(rut, RUTUtil.calculaDigitoVerificador(rut), (String) ses.getAttribute("CallCenterClave"));
                                            }
                                            if(getLogger().isInfoEnabled()){getLogger().info("[service][BCI_FINOK][ERROR] Estimado cliente, su cuenta est� bloqueada.");}
                                            wcorp.util.GeneralException se = new wcorp.util.GeneralException("ESPECIAL", "Estimado cliente, su cuenta est� bloqueada.");
                                            req.setAttribute("javax.servlet.jsp.jspException", se);
                                            this.getServletContext().getRequestDispatcher(res.encodeURL("/portalwls/seguridad/error.jsp?rut=" + (String)ses.getAttribute("CallCenterRut") + "&dv=" + (String)ses.getAttribute("CallCenterDv"))).forward(req,res);
                                            return;
                                        }
                                        else{
                                            if(getLogger().isInfoEnabled()){getLogger().info("[service][BCI_FINOK][ERROR] USERLOGIN.");}
                                            throw new wcorp.util.GeneralException("USERLOGIN");
                                        }
                                    }
                                }
                                else{
                                    if(getLogger().isInfoEnabled()){getLogger().info("[service][BCI_FINOK][ERROR] Estimado cliente, usted no posee clave para este canal.");}
                                    throw new wcorp.util.GeneralException("ESPECIAL", "Estimado cliente, usted no posee clave para este canal.||");
                                }
                            }
                            catch(Exception e){
                                if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[service]["+userId+"][BCI_FINEX][Exception] Error ["+e.getMessage()+"]", e);}
                                req.setAttribute("javax.servlet.jsp.jspException", e);
                                if (codResp == null || codResp.equals("N"))
                                    this.getServletContext().getRequestDispatcher(res.encodeURL("/portalwls/seguridad/Error2.jsp")).forward(req,res);
                                else
                                    this.getServletContext().getRequestDispatcher(res.encodeURL("/portalwls/common/Error.jsp")).forward(req,res);
                            }
                        }

                        if (paso.equals("2")){
                            try{
                                ses = req.getSession(false);
                                session = (SessionBCI)ses.getAttribute("sessionBci");

                                if (ses.getAttribute("CallCenterRut") != null){

                                    Integer flagSybaseTandem = (Integer) ses.getAttribute("flagSybaseTandem");
                                    if(getLogger().isInfoEnabled()){getLogger().info("[service] tvl (PASO 2): " + flagSybaseTandem);}

                                    String resp = req.getParameter("respuesta");

                                    if (segBean.validaRespuestaTB((String)ses.getAttribute("CallCenterResp"), resp)){
                                        if(getLogger().isInfoEnabled()){getLogger().info("[service] IP : " + req.getRemoteAddr());}

                                        String tipoTrato = (String)session.getAttrib("TipoTratoClienteBCITBANC");
                                        if(getLogger().isInfoEnabled()){getLogger().info("[service] TIPO TRATO:[" + tipoTrato + "]");}
                                        if (tipoTrato.equals("TBANC"))
                                            tipoTrato = "TBN";

                                        CtiCommand cti = new CtiCommand( req.getRemoteAddr() );
                                        boolean ok = cti.activaTCorp( (String)ses.getAttribute("CallCenterRut"), ((String)ses.getAttribute("CallCenterDv")).charAt(0), tipoTrato );
                                        if(ok){
                                            String nuevoEst = segBean.modificaEstadoClave(new Integer((String)ses.getAttribute("CallCenterRut")).longValue(), 'S');
                                            this.getServletContext().getRequestDispatcher(res.encodeURL("/portalwls/portal/Escritorio?opcion=52&rut="+(String)ses.getAttribute("CallCenterRut")+"&dv="+(String)ses.getAttribute("CallCenterDv")+"&cliente=' '")).forward(req,res);
                                            return;
                                        }
                                        else {
                                            if(getLogger().isInfoEnabled()){getLogger().info("[service] Error al enviar mensaje activaTCorp(" + (String)ses.getAttribute("CallCenterRut") + ((String)ses.getAttribute("CallCenterDv")).charAt(0) + "," + tipoTrato + ")");}
                                            req.setAttribute("javax.servlet.jsp.jspException", new GeneralException("ESPECIAL","Ha ocurrido un error de comunicaci&oacute;n por recarga en llamadas a servicios. Por favor reintente"));
                                            this.getServletContext().getRequestDispatcher(res.encodeURL(session.getCanal().getPathJSP() + "/common/Error.jsp")).forward(req,res);
                                            return;
                                        }
                                    }
                                    else {
                                        String nuevoEst = segBean.modificaEstadoClave(new Integer((String)ses.getAttribute("CallCenterRut")).longValue(), 'N');
                                        if (nuevoEst.equals("BLQ")){
                                            if (flagSybaseTandem.intValue() == 0) {
                                                long rut = new Long( (String) ses.getAttribute("CallCenterRut")).longValue();
                                                segBean.bloquearClaveInternetTbanc(rut, RUTUtil.calculaDigitoVerificador(rut), (String) ses.getAttribute("CallCenterClave"));
                                            }

                                            wcorp.util.GeneralException se = new wcorp.util.GeneralException("ESPECIAL", "Estimado cliente, su cuenta est� bloqueada.");
                                            req.setAttribute("javax.servlet.jsp.jspException", se);
                                            this.getServletContext().getRequestDispatcher(res.encodeURL("/portalwls/seguridad/error.jsp?rut=" + (String)ses.getAttribute("CallCenterRut") + "&dv=" + (String)ses.getAttribute("CallCenterDv"))).forward(req,res);
                                            return;
                                        }
                                        else
                                            throw new wcorp.util.GeneralException("USERLOGIN");
                                    }
                                }
                                else{
                                    if(getLogger().isInfoEnabled()){getLogger().info("[service][BCI_FINOK][ERROR] Estimado cliente, usted no posee clave para este canal.");}
                                    throw new wcorp.util.GeneralException("ESPECIAL", "Estimado cliente, usted no posee clave para este canal.||");
                                }
                            }
                            catch(Exception e){
                                if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[service][BCI_FINEX][Exception] Error ["+e.getMessage()+"]", e);}
                                req.setAttribute("javax.servlet.jsp.jspException", e);
                                this.getServletContext().getRequestDispatcher(res.encodeURL("/portalwls/seguridad/Error3.jsp")).forward(req,res);
                            }
                        }

                        if (paso.equals("3")){
                            ses = req.getSession(false);
                            session = (SessionBCI)ses.getAttribute("sessionBci");

                            long rut = new Integer(req.getParameter("rut")).intValue();
                            char dv  = (req.getParameter("dv")).charAt(0);

                            Cliente cliente = new Cliente(rut,dv);
                            cliente.setDatosBasicos();
                            RetornoTipCli retorno = cliente.getDatosBasicosGenerales();
                            Datos dat = retorno.getDatosComun();
                            String bco = null;

                            if(getLogger().isInfoEnabled()){getLogger().info("[service] Oficina : " + dat.CodOficina);}

                            if (dat.CodOficina.equals("247"))
                                bco = "TBN";
                            else
                                bco = "BCI";

                            this.getServletContext().getRequestDispatcher(res.encodeURL("/portalwls/common/blanco.html")).forward(req,res);
                            CtiCommand cti = new CtiCommand( req.getRemoteAddr() );
                            boolean ok = cti.traspasaASeguridad(String.valueOf(rut), dv, ((String)session.getAttrib("TipoTratoClienteBCITBANC")), bco);
                            if(!ok){
                                if(getLogger().isInfoEnabled()){getLogger().info("[service] Error al enviar mensaje traspasaASeguridad("+rut+"-"+dv+ ","+session.getAttrib("TipoTratoClienteBCITBANC") + "," + bco);}
                                req.setAttribute("javax.servlet.jsp.jspException", new GeneralException("ESPECIAL","Ha ocurrido un error de comunicaci&oacute;n por recarga en llamadas a servicios. Por favor reintente"));
                                this.getServletContext().getRequestDispatcher(res.encodeURL(session.getCanal().getPathJSP() + "/common/Error.jsp")).forward(req,res);
                                return;
                            }
                            /*Journaliza Transferencia a seguridad*/
                            if(canalId == null || (canalId != null && !canalId.trim().equals("215"))){
                                journalizacion(session,EVE_LOGIN,SUB_COD_TRASEG,userIdrut,ID_PRODINTERNET,null);
                            }
                        }

                        if (paso.equals("4")){
                            ses = req.getSession(false);
                            session = (SessionBCI)ses.getAttribute("sessionBci");

                            long rut = new Integer(req.getParameter("rut")).intValue();
                            char dv  = (req.getParameter("dv")).charAt(0);
                            if(getLogger().isInfoEnabled()){getLogger().info("[service] Autenticando en el IVR a [" + rut + "-" + dv + "]");}
                            String men = "Autenticando en el IVR ...";
                            session.setAttrib("message", men);
                            this.getServletContext().getRequestDispatcher(res.encodeURL("/portalwls/common/CommonEsperar.jsp")).forward(req,res);
                            String ip = req.getRemoteAddr();
                            if(getLogger().isInfoEnabled()){getLogger().info("[service] LOGIN PASO 4 IP = [" + ip + "]");}
                            CtiCommand cti = new CtiCommand( ip );
                            boolean ok = cti.activaIVR(String.valueOf(rut), dv);
                            if(!ok){
                                if(getLogger().isInfoEnabled()){getLogger().info("[service] Error al enviar mensaje activaIVR(" + rut + "-" + dv + ")");}
                                req.setAttribute("javax.servlet.jsp.jspException", new GeneralException("ESPECIAL","Ha ocurrido un error de comunicaci&oacute;n por recarga en llamadas a servicios. Por favor reintente"));
                                this.getServletContext().getRequestDispatcher(res.encodeURL(session.getCanal().getPathJSP() + "/common/Error.jsp")).forward(req,res);
                                return;
                            }
                            if(canalId == null || (canalId != null && !canalId.trim().equals("215"))){
                                journalizacion(session,EVE_LOGIN,SUB_COD_TRAIVR,userIdrut,ID_PRODINTERNET,null);
                            }
                        }

                        if (paso.equals("5")){
                            ses = req.getSession(false);
                            session = (SessionBCI)ses.getAttribute("sessionBci");

                            long rut = new Integer(req.getParameter("rut")).intValue();
                            if(getLogger().isInfoEnabled()){getLogger().info("[service] Rut : " + rut);}
                            char dv  = (req.getParameter("dv")).charAt(0);
                            if(getLogger().isInfoEnabled()){getLogger().info("[service] Dv  : " + dv);}

                            Cliente cliente = new Cliente(rut,dv);
                            if(getLogger().isInfoEnabled()){getLogger().info("[service] new Cliente.");}
                            cliente.setDatosBasicos();
                            if(getLogger().isInfoEnabled()){getLogger().info("[service] datos basicos.");}
                            RetornoTipCli retorno = cliente.getDatosBasicosGenerales();
                            if(getLogger().isInfoEnabled()){getLogger().info("[service] datos basicos generales.");}
                            Datos dat = retorno.getDatosComun();
                            if(getLogger().isInfoEnabled()){getLogger().info("[service] datos comun.");}

                            String eje = dat.CodigoEjecutivo;

                            if(getLogger().isInfoEnabled()){getLogger().info("[service] CodigoEjecutivo : " + eje);}

                            ic 		 = JNDIConfig.getInitialContext();
                            miscHome = (ServiciosMiscelaneosHome)ic.lookup("wcorp.serv.misc.ServiciosMiscelaneos");
                            miscBean = (ServiciosMiscelaneos)miscHome.create();

                            String anexo = null;

                            anexo = miscBean.getAnexoColaborador(eje);
                            if(getLogger().isInfoEnabled()){getLogger().info("[service] Anexo : " + anexo);}

                            String men = "Transfiriendo al Ejecutivo ...";
                            session.setAttrib("message", men);

                            this.getServletContext().getRequestDispatcher(res.encodeURL("/portalwls/common/CommonEsperar.jsp")).forward(req,res);
                            CtiCommand cti = new CtiCommand( req.getRemoteAddr() );
                            boolean ok = cti.transferirEjecutivo(String.valueOf(rut), dv, anexo);
                            if(!ok){
                                if(getLogger().isInfoEnabled()){getLogger().info("[service] Error al enviar mensaje transferirEjecutivo(" + rut + "-" + dv + "," + anexo + ")");}
                                req.setAttribute("javax.servlet.jsp.jspException", new GeneralException("ESPECIAL","Ha ocurrido un error de comunicaci&oacute;n por recarga en llamadas a servicios. Por favor reintente"));
                                this.getServletContext().getRequestDispatcher(res.encodeURL(session.getCanal().getPathJSP() + "/common/Error.jsp")).forward(req,res);
                                return;
                            }
                        }

                        if (paso.equals("6")){
                            ses = req.getSession(false);
                            session = (SessionBCI)ses.getAttribute("sessionBci");

                            long   rut      = new Integer(req.getParameter("rut")).intValue();
                            char   dv       = (req.getParameter("dv")).charAt(0);
                            String telefono = req.getParameter("TelefonoParaTransferir");

                            String men = "Llamando a Cliente ...";
                            session.setAttrib("message", men);

                            this.getServletContext().getRequestDispatcher(res.encodeURL("/portalwls/common/CommonEsperar.jsp")).forward(req,res);
                            CtiCommand cti = new CtiCommand( req.getRemoteAddr() );
                            boolean ok =cti.transferirCliente(String.valueOf(rut), dv, telefono);
                            if(!ok){
                                if(getLogger().isInfoEnabled()){getLogger().info("[service] Error al enviar mensaje transferirCliente(" + rut + "-" + dv + "," + telefono + ")");}
                                req.setAttribute("javax.servlet.jsp.jspException", new GeneralException("ESPECIAL","Ha ocurrido un error de comunicaci&oacute;n por recarga en llamadas a servicios. Por favor reintente"));
                                this.getServletContext().getRequestDispatcher(res.encodeURL(session.getCanal().getPathJSP() + "/common/Error.jsp")).forward(req,res);
                                return;
                            }
                        }
                    }
                    catch(Exception e){
                        if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[service][BCI_FINEX][Exception] Error ["+e.getMessage()+"]", e);}
                        //Agregado para proyecto Mejoras Seguridad FASE 1
                        e = ControlErrorMsg.modificaCodigo(e, session);
                        req.setAttribute("javax.servlet.jsp.jspException", e);
                        this.getServletContext().getRequestDispatcher(res.encodeURL("/portalwls/common/Error.jsp")).forward(req,res);
                    }
                }
                else{
                    if(getLogger().isInfoEnabled()){getLogger().info("[service] TeleCanal 2: " + canalId);}
                    msg = req.getParameter("msg");
                    pin_param = String.valueOf(PINID_CALLCENTER);
                    String msgN = Encriptacion.desnormaliza(msg);
                    msgN = Encriptacion.desencriptar(msgN);
                    userIdrut = msgN;
                    String msgs[] = StringUtil.divide(msgN, ';');
                    for (int i = 0; i < msgs.length; i++){
                        if (msgs[i].toUpperCase().startsWith("SERVER=")){
                            int idx = msgs[i].indexOf('=');
                            serv_param = msgs[i].substring(idx + 1);
                        }
                    }
                }

                break;
            default: // SIN DEFINICION DE CANAL
                if(getLogger().isInfoEnabled()){getLogger().info("[service][BCI_FINOK][ERROR] Canal sin definici�n");}
                throw new wcorp.util.GeneralException("ESPECIAL", "Canal sin definici�n");
            }

            userId = userIdrut == null ? userIdusu.trim() : userIdrut.trim();
            if (userId.compareTo("") == 0 | pin_param.compareTo("")==0){
                if(getLogger().isInfoEnabled()){getLogger().info("[service][BCI_FINOK][ERROR] Valores Insuficientes");}
                throw new wcorp.util.GeneralException("PARAM", "Valores Insuficientes");
            }
            pin = pin_param;
            session = new SessionBCI(canalId, userId, userIdAux);       
            session.setIP(req.getRemoteAddr());
            if(session!=null)
                session.setIdSession(ss.getId());
            if(getLogger().isInfoEnabled()){getLogger().info("[service] La sesion fue creada: " +  canalId + " " + userId  +  " " + userIdAux);}

            if (pid==PINID_CALLCENTER) {
                // si hacemos login via callcenter, no hay informacion relevante
                // que guardar en la clave principal ==> la dejamos en "blanco".
                userId="0000000000000000";
            }

            if (serv_param == null){
                session.setAttrib("servicioInicial", "NoService");
            }
            else{
                if (!serv_param.trim().equals(""))
                    session.setAttrib("servicioInicial", serv_param);
                else
                    session.setAttrib("servicioInicial", "NoService");
            }

            try {
                String verificaFecha = TablaValores.getValor("Seguridad.parametros", "verificaFecha"+session.getCanal().getCanalID(), "valor") != null 
                		? TablaValores.getValor("Seguridad.parametros", "verificaFecha"+session.getCanal().getCanalID(), "valor") : "false";
                if(getLogger().isInfoEnabled()){getLogger().info("[service] Valida sin considerar la fecha expiraci�n :"+verificaFecha );}
                boolean verifica =  new Boolean(verificaFecha).booleanValue();
                if (verifica) {
                    try {
                        if(getLogger().isInfoEnabled()){getLogger().info("[service] Si la clave esta expirada redirecciona a RealizaCambioClave?paso=0");}
                        session.autentica(pin);
                    }
                    catch (SeguridadException se) {
                        if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[service][BCI_FINEX][SeguridadException] Error[" + se.getMessage() + "]", se);}
                        if(se.getCodigo().equals("0433")) {
                            if(getLogger().isEnabledFor(Level.ERROR)){
                                getLogger().error("[service] [" + session.getCliente().getRut() + "]" + " Cliente canal " + canal.getCanalID() + " con clave expirada");
                                getLogger().error("[service] Debe cambiar la contrase�a por estar expirada");
                            }
                            session.activa();
                            HttpSession hs = req.getSession(true);
                            hs.removeAttribute("sessionBci");
                            hs.setAttribute("sessionBci", session);
                            this.getServletContext().getRequestDispatcher(res.encodeURL("/seguridadwls/RealizaCambioClave?paso=0")).forward(req, res);
                            return;
                        }
                        else {
                            throw se;
                        }
                    }
                } 
                else {
                    try{
                    	if(canalId.equalsIgnoreCase(PINID_PINCORP_PORTAL) || canalId.equalsIgnoreCase(PINID_PINCORP_EVEREST)){
	                        String autenticacionLDAP = TablaValores.getValor(TABLA_PARAMETROS, "irLDAP", "Desc");
	                        if(autenticacionLDAP != null && autenticacionLDAP.equalsIgnoreCase("no")){
	                            if(getLogger().isInfoEnabled()){getLogger().info("[service] Autentica clave en Active Directory");}
	                            autenticarActiveDirectory(session, userId, pin, canalId);
	                        }
	                        else{
	                        	if(getLogger().isInfoEnabled()){getLogger().info("[service] Autentica clave en LDAP");}
	                            session.autentica(pin); // Autentifica la Sesion
	                        }
	                    }
                        else{
                            if(getLogger().isInfoEnabled()){getLogger().info("[service] Autentica clave en LDAP");}
                            session.autentica(pin); // Autentifica la Sesion
                        }
                    }
                    catch (SeguridadException ex) {
                        if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[service][BCI_FINEX][SeguridadException] Clave Expirada para clientes con convenio ex.getCodigo()=[" + ex.getCodigo() + "]");}
                        if ("0433".equals(ex.getCodigo())) {
                            getLogger().info("[service] indCambioClave2 true");
                            session.indCambioClave = true;
                            session.primeraVez=false;
                            session.activa();
                        }
                        else{
                            throw ex;
                        }
                    }
                }
                if(canal.getCanalID().equals("665")){
                    if(session!=null)
                        session.setIdSession(ss.getId());
                    ss.setAttribute("sessionBci", session);
                    if(getLogger().isInfoEnabled()){getLogger().info("[service] Login realizado desde PDA redirecciona :"+verificaFecha );}
                    this.getServletContext().getRequestDispatcher(res.encodeURL(session.getCanal().getPathJSP()+"/menu/menu.jsp")).forward(req, res);
                    return;
                }
            }
            catch (wcorp.serv.clientes.ClientesException ex){
                if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[service][BCI_FINEX][ClientesException] Error["+ex.getMessage()+"]", ex);}
                wcorp.serv.clientes.ClientesException ce = (wcorp.serv.clientes.ClientesException)ex;

                /* Mejoras Seguridad Fase 3, journalizacion de eventos claves*/
                if(canalId == null || (canalId != null && !canalId.trim().equals("215"))){
                    journalizacion(session,EVE_LOGIN,SUB_COD_ERROR,userIdrut,ID_PRODINTERNET,ex.getCodigo());
                }
                if (ce.getInfoAdic().startsWith("Cliente no existe")){
                    if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[service][BCI_FINEX][SeguridadException] Cliente no existe.");}
                    throw new wcorp.serv.seguridad.SeguridadException("USERLOGIN");
                }
                session.cerrar(); // Cierra la Sesion
                throw ex;
            }
            catch (wcorp.serv.seguridad.SeguridadException ex){
                if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[service][BCI_FINEX][SeguridadException] Error["+ex.getMessage()+"]", ex);}
                wcorp.serv.seguridad.SeguridadException se = (wcorp.serv.seguridad.SeguridadException)ex;
                //Agregado para proyecto Mejoras Seguridad FASE 1
                se = (SeguridadException)ControlErrorMsg.modificaCodigo((Exception)se, session);

                /* Mejoras Seguridad Fase 3, journalizacion de eventos claves*/
                if(canalId == null || (canalId != null && !canalId.trim().equals("215"))){
                    journalizacion(session,EVE_LOGIN,SUB_COD_ERROR,userIdrut,ID_PRODINTERNET,ex.getCodigo());
                }
                if (se.getCodigo().equalsIgnoreCase("USERLOGIN")){
                    if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[service][BCI_FINEX][SeguridadException] USERLOGIN.");}
                    throw new wcorp.serv.seguridad.SeguridadException("USERLOGIN");
                }
                session.cerrar(); // Cierra la Sesion
                throw ex;
            }
            catch (wcorp.util.GeneralException ex){
                if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[service][BCI_FINEX][GeneralException] Error["+ex.getMessage()+"]", ex);}
                //				Agregado para proyecto Mejoras Seguridad FASE 1
                ex = (GeneralException)ControlErrorMsg.modificaCodigo((Exception)ex, session);
                wcorp.util.GeneralException ge = (wcorp.util.GeneralException)ex;
                /* Mejoras Seguridad Fase 3, journalizacion de eventos claves*/
                if(canalId == null || (canalId != null && !canalId.trim().equals("215"))){
                    journalizacion(session,EVE_LOGIN,SUB_COD_ERROR,userIdrut,ID_PRODINTERNET,ex.getCodigo());
                }
                session.cerrar(); // Cierra la Sesion
                req.setAttribute("mensaje", ex.getSimpleMessage());
                this.getServletContext().getRequestDispatcher(res.encodeURL(session.getCanal().getPathJSP() + "/common/ErrorPostSession.jsp")).forward(req,res);
            }
            catch (Exception ex){
                if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[service][BCI_FINEX][Exception] Error["+ex.getMessage()+"]", ex);}
                /* Mejoras Seguridad Fase 3, journalizacion de eventos claves*/
                if(canalId == null || (canalId != null && !canalId.trim().equals("215"))){
                    journalizacion(session,EVE_LOGIN,SUB_COD_ERROR,userIdrut,ID_PRODINTERNET,ex.getMessage());
                }
                session.cerrar(); // Cierra la Sesion
                throw ex;
            }

            if(PINID_PINCORP_WAPTBANC == pid){
                if(getLogger().isInfoEnabled()){getLogger().info("[service] " + userId + "-" + userIdAux + " " + req.getParameter("HTTP_X_UP_SUBNO"));}
            }
            if(getLogger().isInfoEnabled()){getLogger().info("[service] rut " +userId + "-" + userIdAux +"cambia Clave: " +  session.indCambioClave);}

            /**
             * validaci�n de morosidad del cliente
             * se solicita la lista de morosidades y se calcula la esad de mora
             * si supera el limite, se impide el login y se le despacha a mensaje ad-hoc
             * esta funcionalidad se maneja en try-catch para que, ante problemas, no impedir el login normal
             * aituarte (schema): 23/05/2006
             */
            try {
                // Solo para canales elegidos
                if (session!=null && (session.getCanal().getCanalID().equals("100") || session.getCanal().getCanalID().equals("110"))){
                    // setea alarma de morosidad (encapsulado)
                    int edadMora = wcorp.informaciondemorosidad.ui.actions.ShowAlarmaAction.
                    setAlarmaMorosidadCliente(req, session.getCliente().getRut(), session.getCanal().getCanalID());

                    // revisa si hay que impedir acceso
                    if (edadMora >= wcorp.bprocess.informaciondemorosidad.CondicionMorosidad.LIM_FUERTE) {
                        if(getLogger().isInfoEnabled()){getLogger().info("[service] cliente: "+session.getCliente().getFullRut()+" impedido de ingresar a sesi�n por morosidad");}

                        // registra que muestra banner grave
                        HistoriaComplementoDVO complemento = (HistoriaComplementoDVO) ss.
                        getAttribute("morosidad.complemento");
                        if (complemento != null) {
                            complemento.setVistoBanner(true);
                            complemento.setTipoMensaje(wcorp.bprocess.informaciondemorosidad.CondicionMorosidad.MSG_GRAVE);
                            InformacionDeMorosidadProcessDelegate delegate = new
                            InformacionDeMorosidadProcessDelegate();
                            delegate.actualizarHistoriaComplemento(complemento);
                            ss.setAttribute("morosidad.complemento", complemento);
                        }

                        session.cerrar(); // Cierra la Sesion y despacha al error
                        if(getLogger().isInfoEnabled()){getLogger().info("[service] Despacho a Morosidad Grave");}
                        this.getServletContext().getRequestDispatcher(res.encodeURL(session.getCanal().getPathJSP() + "/common/morosidadGrave.html")).forward(req,res);
                        return;
                    }
                }
            }
            catch (Exception ex) {
                if(getLogger().isEnabledFor(Level.ERROR)){
                    getLogger().error("[service][BCI_FINEX][Exception] Error al tratar de validar morosidad del cliente: " + ex.toString());
                    getLogger().error("[service] Se contin�a ejecuci�n normal");
                }
            }
            /** fin zona de morosidad */
            if (session.indCambioClave){
                session.setAttrib("Escritorio","browser", "nav");
                session.setAttrib("Escritorio","resolucion", "640");
                session.setAttrib("Escritorio","colores", "8");
                session.setAttrib("Escritorio","idColaborador", userId);

                despacho = "/seguridadwls/RealizaCambioClave";
            }
            else{
                // Valida si tiene restriccion de clave por 24 Horas
                if (session.restriccion){
                    despacho = session.getCanal().getPathJSP() + "/seguridad/PrimeraVez.html";
                }
                else{
                    if (session.primeraVez)
                        despacho = session.getCanal().getPathJSP() + "/seguridad/PrimeraVez.html";
                    else { // Finalmente, se fuerza que solo actue como Titular
						if (session.tipoUser == 'M' || session.tipoUser == 'A'){
                                despacho = "/seguridadwls/DefineAmbiente?tipoUser=T";
                        }
                        else{
                            despacho = "/seguridadwls/DefineAmbiente";
                        }
                    }
                }
            }

            // Registra la Sesion en ambiente HTTP
            HttpSession hs = req.getSession(true);

            // Ajustes de los valores para timeOutTotal y timeOutUso GOJ
            int tu = canal.getTimeOutUso();
            if (tu > 0)
                hs.setMaxInactiveInterval((tu+3) * 60); // Retoma

            /* Mejoras Seguridad Fase 3, journalizacion de eventos claves*/
            if(getLogger().isInfoEnabled()){getLogger().info("[service] rut ["+userId+"-"+userIdAux+"]");}

            if(userIdrut==null)
                userIdrut = "";
            if(canalId == null || (canalId != null && !canalId.trim().equals("215"))){
                if ((!canalId.equals("110")) && (!canalId.equals("100"))){
                    if(getLogger().isInfoEnabled()){getLogger().info("[service] Journaliz� ingreso Ok clave internet");}
                    journalizacion(session,EVE_LOGIN,SUB_COD_OK,userIdrut,ID_PRODINTERNET,null);
                }
                else {	
                    session.setAttrib("origenLogin", "SI");
                    if(getLogger().isInfoEnabled()){getLogger().info("[service] No journaliz� por ser canal " + canalId);}
                }
            }
            if (TablaValores.getValor("RealizaCambioClave.parametros", canalId, "valida") != null && TablaValores.getValor("RealizaCambioClave.parametros", canalId, "valida").equals("Si")){
                if ((new Integer(TablaValores.getValor("RealizaCambioClave.parametros", canalId, "min")).intValue() > pin_param.length()) || (new Integer(TablaValores.getValor("RealizaCambioClave.parametros", canalId, "max")).intValue() < pin_param.length())){
                    session.indCambioClave = true;
                    despacho = "/seguridadwls/RealizaCambioClave";
                    if(getLogger().isInfoEnabled()){getLogger().info("[service] rut ["+userId+"-"+userIdAux+"] despacho ["+despacho+"]");}
                }
            }

            if (canalId.equals("540") || canalId.equals("540bci") || canalId.equals("540tbc")){
                session.setAttrib("PagueDirecto", pdir);
                if ((despacho.indexOf("/seguridadwls/RealizaCambioClave") != -1) || (despacho.indexOf("/seguridad/PrimeraVez.html") != -1))
                    throw new wcorp.util.GeneralException("ESPECIAL", "Estimado cliente, debe cambiar su clave");
                if(getLogger().isInfoEnabled()){getLogger().info("[service] Pago.jsp : " + pdir.getCnvnum() + "-" + pdir.getTrx()+ "-" + pdir.getRutEmpresa() + "-" + userIdrut);}
            }
            //Asigna m�todos de autenticaci�n
            if (!(canalId.equals("150")) && !(canalId.equals("151")) && !(canalId.equals("152"))) {
                if (canalId.equals("540") || canalId.equals("540bci") || canalId.equals("540tbc")){
                    String servicio="PagueDirecto";
                    HttpSession hsess = req.getSession(false);
                    if(getLogger().isInfoEnabled()){getLogger().info("[service] hsess: " + hsess);}
                    SessionBCI sesionPpal = (SessionBCI) hsess.getAttribute(IDNAME);
                    if(getLogger().isInfoEnabled()){getLogger().info("[service] sesionPpal:" + sesionPpal);}
                    hs.setAttribute(IDNAME, session);
                    hsess = req.getSession(false);
                    sesionPpal = (SessionBCI) hsess.getAttribute(IDNAME);
                    if(getLogger().isInfoEnabled()){getLogger().info("[service] sesionPpal:" + sesionPpal);}
                    
                    ParametrosEstrategiaSegundaClaveTO parametrosEstrategiaSegundaClaveTO = 
                        new ParametrosEstrategiaSegundaClaveTO(servicio,
                            session.getCliente(),  canal,  session);
                    
                    if(getLogger().isInfoEnabled()){getLogger().info("[service] parametrosEstrategiaSegundaClaveTO: " + parametrosEstrategiaSegundaClaveTO);}
                    session.setParametrosEstrategiaSegundaClaveTO(parametrosEstrategiaSegundaClaveTO);
                }
                session.asignaMetodosAutenticacion();
                if(getLogger().isInfoEnabled()){getLogger().info("[service] MetodosAutenticacion: " + StringUtil.contenidoDe(session.getMetodosAutenticacion()));}
            }

            hs.removeAttribute("sessionBci");
            session.setParametrosEstrategiaSegundaClaveTO(null);
            hs.setAttribute("sessionBci", session);
            if(getLogger().isInfoEnabled()){getLogger().info("[service] Despacho finalmente es: [" + despacho + "]" );}

            this.getServletContext().getRequestDispatcher(res.encodeURL(despacho)).forward(req, res);
            /* se debe agregar el return para evitar problema cuando el despacho tiene problemas*/
            return;
        }
        catch(wcorp.serv.clientes.ClientesException ex){
            if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[service][BCI_FINEX][ClientesException] Error: [" + ex.getMessage() + "]", ex);}

            /*Agregar en cada caso hacia donde se despacha el error*/
            if (session != null){
                req.setAttribute("javax.servlet.jsp.jspException", ex);
                if (canalId.equals("540") || canalId.equals("540bci")){
                    enviarRespuesta(pdir.getCnvnum(), pdir.getTrx());
                    if(req.getAttribute("estadoPopUp") == null)
                        req.setAttribute("estadoPopUp", req.getParameter("estadoPopUp")==null?"true": String.valueOf(req.getParameter("estadoPopUp")));
                    if(getLogger().isInfoEnabled()){getLogger().info("[service] ClientesException: despachando a -->/bcinetwls/paguedirecto/error.jsp");}
                    this.getServletContext().getRequestDispatcher(res.encodeURL("/bcinetwls/paguedirecto/error.jsp")).forward(req,res);
                }
                else if (canalId.equals("540tbc")){
                    enviarRespuesta(pdir.getCnvnum(), pdir.getTrx());
                    if(getLogger().isInfoEnabled()){getLogger().info("[service] ClientesException: despachando a -->/tbancwls/paguedirecto/error.jsp");}
                    if(req.getAttribute("estadoPopUp") == null)
                        req.setAttribute("estadoPopUp", req.getParameter("estadoPopUp")==null?"true": String.valueOf(req.getParameter("estadoPopUp")));
                    this.getServletContext().getRequestDispatcher(res.encodeURL("/tbancwls/paguedirecto/error.jsp")).forward(req,res);
                }
                else{
                    if(getLogger().isInfoEnabled()){getLogger().info("[service] ClientesException: despachando a -->"+session.getCanal().getPathJSP() + "/common/Error.jsp");}
                    this.getServletContext().getRequestDispatcher(res.encodeURL(session.getCanal().getPathJSP() + "/common/Error.jsp")).forward(req,res);
                }
            }
            else {
                try {
                    wcorp.serv.clientes.ClientesException ce = (wcorp.serv.clientes.ClientesException)ex;
                    if (ce.getCodigo().equals("0058"))
                        throw new wcorp.serv.seguridad.SeguridadException("USERLOGIN");

                    req.setAttribute("javax.servlet.jsp.jspException", ce);
                    if (canalId.equals("540") || canalId.equals("540bci")){
                        enviarRespuesta(pdir.getCnvnum(), pdir.getTrx());
                        if(getLogger().isInfoEnabled()){getLogger().info("[service] ClientesException: despachando a -->/bcinetwls/paguedirecto/error.jsp");}
                        if(req.getAttribute("estadoPopUp") == null)
                            req.setAttribute("estadoPopUp", req.getParameter("estadoPopUp")==null?"true": String.valueOf(req.getParameter("estadoPopUp")));
                        this.getServletContext().getRequestDispatcher(res.encodeURL("/bcinetwls/paguedirecto/error.jsp")).forward(req,res);
                    }
                    else if (canalId.equals("540tbc")){
                        enviarRespuesta(pdir.getCnvnum(), pdir.getTrx());
                        if(getLogger().isInfoEnabled()){getLogger().info("[service] ClientesException: despachando a -->/tbancwls/paguedirecto/error.jsp");}
                        if(req.getAttribute("estadoPopUp") == null)
                            req.setAttribute("estadoPopUp", req.getParameter("estadoPopUp")==null?"true": String.valueOf(req.getParameter("estadoPopUp")));
                        this.getServletContext().getRequestDispatcher(res.encodeURL("/tbancwls/paguedirecto/error.jsp")).forward(req,res);
                    }
                    else{
                        if(getLogger().isInfoEnabled()){getLogger().info("[service] ClientesException: despachando a -->/common/Error.jsp");}
                        if(req.getAttribute("estadoPopUp") == null)
                            req.setAttribute("estadoPopUp", req.getParameter("estadoPopUp")==null?"true": String.valueOf(req.getParameter("estadoPopUp")));
                        this.getServletContext().getRequestDispatcher(res.encodeURL("/common/Error.jsp")).forward(req,res);
                    }
                }
                catch (Exception e) {
                    if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[service][BCI_FINEX][Exception] Error[" + e.getMessage() + "]", e);}
                    req.setAttribute("javax.servlet.jsp.jspException", e);
                    if (canalId.equals("540") || canalId.equals("540bci")){
                        enviarRespuesta(pdir.getCnvnum(), pdir.getTrx());
                        if(getLogger().isInfoEnabled()){getLogger().info("[service] lientesException: despachando a -->/bcinetwls/paguedirecto/error.jsp");}
                        if (req.getAttribute("estadoPopUp") == null)
                            req.setAttribute("estadoPopUp", req.getParameter("estadoPopUp")==null?"true": String.valueOf(req.getParameter("estadoPopUp")));
                        this.getServletContext().getRequestDispatcher(res.encodeURL("/bcinetwls/paguedirecto/error.jsp")).forward(req,res);
                    }
                    else if (canalId.equals("540tbc")){
                        enviarRespuesta(pdir.getCnvnum(), pdir.getTrx());
                        if(getLogger().isInfoEnabled()){getLogger().info("[service] ClientesException: despachando a -->/tbancwls/paguedirecto/error.jsp");}
                        if (req.getAttribute("estadoPopUp") == null)
                            req.setAttribute("estadoPopUp", req.getParameter("estadoPopUp")==null?"true": String.valueOf(req.getParameter("estadoPopUp")));
                        this.getServletContext().getRequestDispatcher(res.encodeURL("/tbancwls/paguedirecto/error.jsp")).forward(req,res);
                    }
                    else{
                        if(getLogger().isInfoEnabled()){getLogger().info("[service] ClientesException: despachando a -->/common/Error.jsp");}
                        this.getServletContext().getRequestDispatcher(res.encodeURL("/common/Error.jsp")).forward(req,res);
                    }
                }
            }
        }
        catch(wcorp.serv.seguridad.SeguridadException ex){
            if(getLogger().isEnabledFor(Level.ERROR)){
                getLogger().error("[service][BCI_FINEX][SeguridadException] Error [" + ex.getMessage() + "]", ex);
                getLogger().error("rut["+Rut+"] Por aqui se va la excepcion de seguridad" );
                getLogger().error("rut["+Rut+"] El canal es : " + canalId);
                getLogger().error("rut["+Rut+"] El canal de la sesion es : " + ((session!=null)?session.getCanal().getCanalID():"null"));
                getLogger().error("rut["+Rut+"] ex.getCodigo() : " + ex.getCodigo());
            }
            /*Agregar en cada caso hacia donde se despacha el error*/
            //Agregado para proyecto Mejoras Seguridad FASE 1
            ex = (SeguridadException)ControlErrorMsg.modificaCodigo((Exception)ex, session);

            if (canalId.equals("540") && ex.getCodigo().equals("0433")) {
                String mensaje = "";
                mensaje = TablaValores.getValor("mensajePagueDirecto.parametros", "ClaveExpiradaEmp", "msg");
                ex.setInfoAdic(mensaje);
            }

            if(getLogger().isInfoEnabled()){getLogger().info("[service] ex.getCodigo() : " + ex.getCodigo());}
            if (session != null){
                req.setAttribute("javax.servlet.jsp.jspException", ex);
                if (canalId.equals("540") || canalId.equals("540bci") || session.getCanal().getCanalID().equals("540") || session.getCanal().getCanalID().equals("540bci") ){
                    enviarRespuesta(pdir.getCnvnum(), pdir.getTrx());
                    if(getLogger().isInfoEnabled()){getLogger().info("[service] SeguridadException: despachando a -->/bcinetwls/paguedirecto/error.jsp");}
                    if (req.getAttribute("estadoPopUp") == null)
                        req.setAttribute("estadoPopUp", req.getParameter("estadoPopUp")==null?"true": String.valueOf(req.getParameter("estadoPopUp")));
                    this.getServletContext().getRequestDispatcher(res.encodeURL("/bcinetwls/paguedirecto/error.jsp")).forward(req,res);
                }
                else if (canalId.equals("540tbc") || session.getCanal().getCanalID().equals("540tbc")){
                    enviarRespuesta(pdir.getCnvnum(), pdir.getTrx());
                    if(getLogger().isInfoEnabled()){getLogger().info("[service] SeguridadException: despachando a -->/tbancwls/paguedirecto/error.jsp");}
                    if (req.getAttribute("estadoPopUp") == null)
                        req.setAttribute("estadoPopUp", req.getParameter("estadoPopUp")==null?"true": String.valueOf(req.getParameter("estadoPopUp")));
                    this.getServletContext().getRequestDispatcher(res.encodeURL("/tbancwls/paguedirecto/error.jsp")).forward(req,res);
                }
                else if(invalidSession){
                    if(getLogger().isInfoEnabled()){getLogger().info("[service] SeguridadException: despachando a -->/common/Error.html");}
                    if (req.getAttribute("estadoPopUp") == null)
                        req.setAttribute("estadoPopUp", req.getParameter("estadoPopUp")==null?"true": String.valueOf(req.getParameter("estadoPopUp")));
                    this.getServletContext().getRequestDispatcher(res.encodeURL("/common/Error.html")).forward(req,res);
                }
                else{
                    if(getLogger().isInfoEnabled()){getLogger().info("[service] SeguridadException: despachando a -->"+session.getCanal().getPathJSP() + "/common/Error.jsp");}
                    if(req.getAttribute("estadoPopUp") == null)
                        req.setAttribute("estadoPopUp", req.getParameter("estadoPopUp")==null?"true": String.valueOf(req.getParameter("estadoPopUp")));
                    this.getServletContext().getRequestDispatcher(res.encodeURL(session.getCanal().getPathJSP() + "/common/Error.jsp")).forward(req,res);
                }
            }
            else{
                wcorp.serv.seguridad.SeguridadException se = (wcorp.serv.seguridad.SeguridadException)ex;
                req.setAttribute("javax.servlet.jsp.jspException", se);
                if (canalId.equals("540") || canalId.equals("540bci")){
                    enviarRespuesta(pdir.getCnvnum(), pdir.getTrx());
                    if(getLogger().isInfoEnabled()){getLogger().info("[service] SeguridadException: despachando a -->/bcinetwls/paguedirecto/error.jsp");}
                    if (req.getAttribute("estadoPopUp") == null)
                        req.setAttribute("estadoPopUp", req.getParameter("estadoPopUp")==null?"true": String.valueOf(req.getParameter("estadoPopUp")));
                    this.getServletContext().getRequestDispatcher(res.encodeURL("/bcinetwls/paguedirecto/error.jsp")).forward(req,res);
                }
                else if (canalId.equals("540tbc")){
                    enviarRespuesta(pdir.getCnvnum(), pdir.getTrx());
                    if(getLogger().isInfoEnabled()){getLogger().info("[service] SeguridadException: despachando a -->/tbancwls/paguedirecto/error.jsp");}
                    if (req.getAttribute("estadoPopUp") == null)
                        req.setAttribute("estadoPopUp", req.getParameter("estadoPopUp")==null?"true": String.valueOf(req.getParameter("estadoPopUp")));
                    this.getServletContext().getRequestDispatcher(res.encodeURL("/tbancwls/paguedirecto/error.jsp")).forward(req,res);
                }
                else if(invalidSession){
                    if(getLogger().isInfoEnabled()){getLogger().info("[service] SeguridadException: despachando a -->/common/Error.html");}
                    this.getServletContext().getRequestDispatcher(res.encodeURL("/common/Error.html")).forward(req,res);
                }
                else {
                    if(getLogger().isInfoEnabled()){getLogger().info("[service] SeguridadException: despachando a -->/common/Error.jsp");}
                    this.getServletContext().getRequestDispatcher(res.encodeURL("/common/Error.jsp")).forward(req,res);
                }
            }
        }
        catch(wcorp.util.GeneralException ex){
            if(getLogger().isInfoEnabled()){getLogger().info("[service][BCI_FINEX][GeneralException] Error ["+ex.getMessage()+"]", ex);}
            /*Agregar en cada caso hacia donde se despacha el error*/
            if (session != null){
                //Agregado para proyecto Mejoras Seguridad FASE 1
                ex = (GeneralException)ControlErrorMsg.modificaCodigo((Exception)ex, session);
                req.setAttribute("javax.servlet.jsp.jspException", ex);
                if (canalId.equals("540") || canalId.equals("540bci")){
                    enviarRespuesta(pdir.getCnvnum(), pdir.getTrx());
                    if(getLogger().isInfoEnabled()){getLogger().info("[service] GeneralException: despachando a -->/bcinetwls/paguedirecto/error.jsp");}
                    if (req.getAttribute("estadoPopUp") == null)
                        req.setAttribute("estadoPopUp", req.getParameter("estadoPopUp")==null?"true": String.valueOf(req.getParameter("estadoPopUp")));
                    this.getServletContext().getRequestDispatcher(res.encodeURL("/bcinetwls/paguedirecto/error.jsp")).forward(req,res);
                }
                else if (canalId.equals("540tbc")){
                    enviarRespuesta(pdir.getCnvnum(), pdir.getTrx());
                    if(getLogger().isInfoEnabled()){getLogger().info("[service] GeneralException: despachando a -->/tbancwls/paguedirecto/error.jsp");}
                    if (req.getAttribute("estadoPopUp") == null)
                        req.setAttribute("estadoPopUp", req.getParameter("estadoPopUp")==null?"true": String.valueOf(req.getParameter("estadoPopUp")));
                    this.getServletContext().getRequestDispatcher(res.encodeURL("/tbancwls/paguedirecto/error.jsp")).forward(req,res);
                }
                else{
                    if(getLogger().isInfoEnabled()){getLogger().info("[service] GeneralException: despachando a -->"+session.getCanal().getPathJSP() + "/common/Error.jsp");}
                    req.setAttribute("restriccionClave",new Boolean(session.restriccion));
                    this.getServletContext().getRequestDispatcher(res.encodeURL(session.getCanal().getPathJSP() + "/common/Error.jsp")).forward(req,res);
                }
            }
            else{
                wcorp.util.GeneralException se = (wcorp.util.GeneralException)ex;
                req.setAttribute("javax.servlet.jsp.jspException", se);
                if (canalId.equals("540") || canalId.equals("540bci")){
                    enviarRespuesta(pdir.getCnvnum(), pdir.getTrx());
                    if(getLogger().isInfoEnabled()){getLogger().info("[service] GeneralException: despachando a -->/bcinetwls/paguedirecto/error.jsp");}
                    if (req.getAttribute("estadoPopUp") == null)
                        req.setAttribute("estadoPopUp", req.getParameter("estadoPopUp")==null?"true": String.valueOf(req.getParameter("estadoPopUp")));
                    this.getServletContext().getRequestDispatcher(res.encodeURL("/bcinetwls/paguedirecto/error.jsp")).forward(req,res);
                }
                else if (canalId.equals("540tbc")){
                    enviarRespuesta(pdir.getCnvnum(), pdir.getTrx());
                    if(getLogger().isInfoEnabled()){getLogger().info("[service] GeneralException: despachando a -->/tbancwls/paguedirecto/error.jsp");}
                    if (req.getAttribute("estadoPopUp") == null)
                        req.setAttribute("estadoPopUp", req.getParameter("estadoPopUp")==null?"true": String.valueOf(req.getParameter("estadoPopUp")));
                    this.getServletContext().getRequestDispatcher(res.encodeURL("/tbancwls/paguedirecto/error.jsp")).forward(req,res);
                }
                else{
                    if(getLogger().isInfoEnabled()){getLogger().info("[service] GeneralException: despachando a -->/common/Error.jsp");}
					req.setAttribute("restriccionClave", new Boolean(session.restriccion));
                    this.getServletContext().getRequestDispatcher(res.encodeURL("/common/Error.jsp")).forward(req,res);
                }
            }
        }
        catch (Exception ex){
            if(getLogger().isEnabledFor(Level.ERROR)) {
                getLogger().error("[service] Sesion HTTP Expirada");
                getLogger().error("[service][BCI_FINEX][Exception] Error["+ ex.getMessage() + "]", ex);
            }

            /*Agregar en cada caso hacia donde se despacha el error*/
            if (session != null) {
                //Agregado para proyecto Mejoras Seguridad FASE 1
                ex = ControlErrorMsg.modificaCodigo(ex, session);
                req.setAttribute("javax.servlet.jsp.jspException", ex);

                if (canalId != null && (canalId.equals("540") || canalId.equals("540bci"))) {
                    enviarRespuesta(pdir.getCnvnum(), pdir.getTrx());
                    if(getLogger().isInfoEnabled()){getLogger().info("[service] Exception: despachando a -->/bcinetwls/paguedirecto/error.jsp");}
                    if (req.getAttribute("estadoPopUp") == null)
                        req.setAttribute("estadoPopUp", req.getParameter("estadoPopUp")==null?"true": String.valueOf(req.getParameter("estadoPopUp")));
                    this.getServletContext().getRequestDispatcher(res.encodeURL("/bcinetwls/paguedirecto/error.jsp")).forward(req, res);
                }
                else if (canalId != null && canalId.equals("540tbc")) {
                    enviarRespuesta(pdir.getCnvnum(), pdir.getTrx());
                    if(getLogger().isInfoEnabled()){getLogger().info("[service] Exception: despachando a -->/tbancwls/paguedirecto/error.jsp");}
                    if (req.getAttribute("estadoPopUp") == null)
                        req.setAttribute("estadoPopUp", req.getParameter("estadoPopUp")==null?"true": String.valueOf(req.getParameter("estadoPopUp")));
                    this.getServletContext().getRequestDispatcher(res.encodeURL("/tbancwls/paguedirecto/error.jsp")).forward(req, res);
                }
                else {
                    if(getLogger().isInfoEnabled()){getLogger().info("[service] SessionBCI Expirada, se envia a login");}
                    session.cerrar();
                    ((HttpSession)req.getSession(false)).removeAttribute("sessionBci");
                    if(getLogger().isInfoEnabled()){getLogger().info("[service] Exception: despachando a -->/common/Error.html");}
                    req.setAttribute("restriccionClave", new Boolean(session.restriccion));
                    this.getServletContext().getRequestDispatcher(res.encodeURL("/common/Error.html")).forward(req, res);
                }
            }
            else {
                req.setAttribute("javax.servlet.jsp.jspException", ex);
                if (canalId != null && (canalId.equals("540") || canalId.equals("540bci"))) {
                    enviarRespuesta(pdir.getCnvnum(), pdir.getTrx());
                    if (req.getAttribute("estadoPopUp") == null)
                        req.setAttribute("estadoPopUp", req.getParameter("estadoPopUp")==null?"true": String.valueOf(req.getParameter("estadoPopUp")));
                    if(getLogger().isInfoEnabled()){getLogger().info("[service] Exception: despachando a -->/bcinetwls/paguedirecto/error.jsp");}
                    this.getServletContext().getRequestDispatcher(res.encodeURL("/bcinetwls/paguedirecto/error.jsp")).forward(req, res);
                }
                else if (canalId != null && canalId.equals("540tbc")) {
                    enviarRespuesta(pdir.getCnvnum(), pdir.getTrx());
                    if(getLogger().isInfoEnabled()){getLogger().info("[service] Exception: despachando a -->/tbancwls/paguedirecto/error.jsp");}
                    if (req.getAttribute("estadoPopUp") == null)
                        req.setAttribute("estadoPopUp", req.getParameter("estadoPopUp")==null?"true": String.valueOf(req.getParameter("estadoPopUp")));
                    this.getServletContext().getRequestDispatcher(res.encodeURL("/tbancwls/paguedirecto/error.jsp")).forward(req, res);
                }
                else {
                    if(getLogger().isInfoEnabled()){getLogger().info("[service] Exception: despachando a --> http://www.bci.cl");}
                    res.sendRedirect("http://www.bci.cl");
                }
            }
        }
    }


    private Vector getSecuencia(){

        String[] letras = new String[10];
        letras[0] = "A";letras[1] = "B";letras[2] = "C";letras[3] = "D";letras[4] = "E";
        letras[5] = "F";letras[6] = "G";letras[7] = "H";letras[8] = "I";letras[9] = "J";
        String[] orden  = new String[1001];
        int i       = 0;
        int numero  = 0;
        while(i < 10){
            numero = (int)Math.abs(Math.round((Math.random() * 1000)));
            if (orden[numero] == null){
                orden[numero] = letras[i];
                i++;
            }
        }
        Vector vec = new Vector();
        for (int j = 0; j < 1001; j++){
            if (orden[j] != null)
                vec.add(orden[j]);
        }
        return vec;
    }
    
    /**
     * Asigna a la variable de sesi�n los m�todos de autenticaci�n disponibles
     * para el cliente y el estado en que est� cada uno:
     * '0': No autenticado en el login con el m�todo
     * '1': Autneticado en el login con el m�todo
     * Ejemplo:<br>
     * <p> ("TOK", '1'), significa que el cliente est� autenticado con Token
     * correctamente
     * Registro de versiones:<ul>
     * <li> 1.0 xx/xx/xxxx, Desconocido (?): Version inicial</li>
     * <li> 2.4 08/11/2015, Diego Urrutia Guerra(Imagemaker) - Mario Lorca (BCI): Se normaliza Logs.</li>
     * </ul>
     * @param req request.
     * @param session sessionBCI.
     * @return
     * @throws GeneralException GeneralException.
     * @throws Exception Exception.
     */
    private void asignaMetodosAutenticacion(HttpServletRequest req, SessionBCI session)
            throws GeneralException, Exception {
        wcorp.serv.seguridad.EstadoToken estadoToken = null;
        Hashtable seguridad = new Hashtable();
        long rut;
        char digito;

        rut = Long.parseLong(req.getParameter("rut"));
        digito = req.getParameter("dig").charAt(0);

        if(getLogger().isInfoEnabled()){getLogger().info("[asignaMetodosAutenticacion] Inicia 'asignaMetodosAutenticacion()'");}

        //Asigna por defecto m�todo de autenticaci�n "Clave Internet"
        if(getLogger().isInfoEnabled()){getLogger().info("[asignaMetodosAutenticacion] Asigna 'Clave Internet' como medio de autenticaci�n");}
        seguridad.put("CLI", new Boolean(true));

        //Verifica por Token
        if(getLogger().isInfoEnabled()){getLogger().info("[asignaMetodosAutenticacion] verificar� Token");}
        try{
            ic = JNDIConfig.getInitialContext();
            segHome      = (ServiciosSeguridadHome)ic.lookup("wcorp.serv.seguridad.ServiciosSeguridad");
            segBean      = (ServiciosSeguridad)segHome.create();

            estadoToken = segBean.obtieneEstadoToken(rut, digito);

        }
        catch(GeneralException ge){
            if(getLogger().isInfoEnabled()){getLogger().info("[asignaMetodosAutenticacion] Catch 1 Token1");}
            if (ge.getCodigo().equals("TK05")){
                if(getLogger().isInfoEnabled()){getLogger().info("[asignaMetodosAutenticacion] Usuario " + rut + "-" + digito + " no tiene Token");}
            }
            else{
                if(getLogger().isInfoEnabled()){getLogger().info("[asignaMetodosAutenticacion] Excepci�n Token : " + ge.toString());}
                throw ge;
            }
            //Agregado para proyecto Mejoras Seguridad FASE 1
            ge = (GeneralException)ControlErrorMsg.modificaCodigo((Exception)ge, session);

        }
        catch(Exception se){
            if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[asignaMetodosAutenticacion][BCI_FINEX][Exception] Error["+se.getMessage() + "]", se);}
            //Agregado para proyecto Mejoras Seguridad FASE 1
            se = ControlErrorMsg.modificaCodigo(se, session);
            throw se;
        }
        if(getLogger().isInfoEnabled()){
            getLogger().info("[asignaMetodosAutenticacion] verificar� valores del estado del Token");
            getLogger().info("[asignaMetodosAutenticacion] estadoToken.habilitado:" + estadoToken.habilitado);
            getLogger().info("[asignaMetodosAutenticacion] estadoToken.newPin:" + estadoToken.newPin);
        }
        if (estadoToken != null && estadoToken.habilitado && !estadoToken.newPin){
            if(getLogger().isInfoEnabled()){getLogger().info("[asignaMetodosAutenticacion] Tiene Token activo");}
            seguridad.put("TOK", new Boolean(false));
        } 
        else {
            if(getLogger().isInfoEnabled()){getLogger().info("[asignaMetodosAutenticacion] No tiene token activo");}
        }

        //Verifica por Passcode
        if(getLogger().isInfoEnabled()){getLogger().info("[asignaMetodosAutenticacion] verificar� si posee Passcode");}
        if (!session.getCanal().getCanalID().equals("150")) {
            PassCode passCode = new PassCode(session.getCanal().getCanalID());
            if (passCode.tienePassCode(rut, digito)) {
                if(getLogger().isInfoEnabled()){getLogger().info("[asignaMetodosAutenticacion] Tiene Passcode");}
                seguridad.put("PAS", new Boolean(false));
            } 
            else {
                if(getLogger().isInfoEnabled()){getLogger().info("[asignaMetodosAutenticacion] No tiene Passcode");}
            }
        }

        session.setAttrib("MetodosAutenticacion", seguridad);
        return;
    }

    /**
     *
     * Registro de versiones:<ul>
     * <li> 1.0 xx/xx/xxxx, Desconocido (?): Version inicial
     * 
     * <li> 2.0 08/09/2006, Paola Parra (SEnTRA): Se agrega log para debug de consulta de clave dos.
     * 
     * <li> 2.1 03/10/2006, Paola Parra (SEnTRA): Se setea en la sesi�n la fecha de expiraci�n de la clave dos del cliente.
     * 
     * <li> 2.2 30/05/2007, Pedro Carmona (SEnTRA): Se condiciona la verificaci�n de fecha de expiraci�n s�lo a los casos en 
     * 						que el estado de la ClaveDos sea distinto de "  " (inactiva).
     * <li> 2.3 05/07/2011, Angelo Vel�squez V (SEnTRA)): Se reemplaza LogFile por Log4j.
     * <li> 2.4 08/11/2015, Diego Urrutia Guerra(Imagemaker) - Mario Lorca (BCI): Se normaliza Logs.
     *</ul>
     * @param session session.
     * @returnb boolean PassCodePaso1.
     * @throws GeneralException GeneralException.
     * @throws Exception Exception.
     * @return boolean PassCodePaso1.
     * @since 2.2
     */
    private boolean PassCodePaso1(SessionBCI session) throws GeneralException, Exception{

        if(getLogger().isInfoEnabled()){getLogger().info("[Passcodepaso1] - session.getRutUsuario [" + session.getRutUsuario() + "-"+session.getDvUsuario()+ "] entro a PassCodePaso1");}
        Vector letras = new Vector();
        letras        = getSecuencia();
        session.setAttrib("passcode", letras);

        EstadoPin respuestaEstado = new EstadoPin();

        try{
            ic = JNDIConfig.getInitialContext();
            miscHome     = (ServiciosMiscelaneosHome)ic.lookup("wcorp.serv.misc.ServiciosMiscelaneos");
            miscBean     = (ServiciosMiscelaneos)miscHome.create();
            segHome      = (ServiciosSeguridadHome)ic.lookup("wcorp.serv.seguridad.ServiciosSeguridad");
            segBean      = (ServiciosSeguridad)segHome.create();

            String periodoTransicion = TablaValores.getValor("Seguridad.parametros", "periodoTransicion", "fecha");

            if(getLogger().isDebugEnabled()){getLogger().debug("[Passcodepaso1] - session.getRutUsuario [" + session.getRutUsuario() + "-"+session.getDvUsuario()+ "] " +"periodoTransicion ["+periodoTransicion+"]");}
            Date finTransicion = java.sql.Date.valueOf(periodoTransicion);
            Date fechaActual   = new Date();
            Date fechaFinal    = new Date();
            fechaFinal.setTime(finTransicion.getTime());

            if (fechaActual.compareTo(fechaFinal) < 0){
                session.setAttrib("transicionPassCode", "1"); // No se ha cumplido fecha de transicion
                if (getLogger().isDebugEnabled()) {getLogger().debug("[Passcodepaso1] - session.getRutUsuario [" + session.getRutUsuario() + "-"+session.getDvUsuario()+ "] " +"No se ha cunplido fecha transicion");}
            }
            else{
                session.setAttrib("transicionPassCode", "0"); // Se cumplio fecha de transicion
                if(getLogger().isDebugEnabled()){getLogger().debug("[Passcodepaso1] - session.getRutUsuario [" + session.getRutUsuario() + "-"+session.getDvUsuario()+ "] " +"Se cunplio fecha transicion");}
            }

            CodyDescTabla[] sucursales = (CodyDescTabla[])miscBean.getConsTabla("OFI");
            session.setAttrib("CodyDescT", sucursales);

            if(session.getCanal().getCanalID().equals("230")) {
                if (getLogger().isDebugEnabled()){getLogger().debug("[Passcodepaso1] - session.getRutUsuario [" + session.getRutUsuario() + "-"+session.getDvUsuario()+ "] " +"canal 230");}
                respuestaEstado = segBean.estadoPin("01", Long.parseLong(session.getRutUsuario()), session.getDvUsuario().charAt(0), "12");
            } 
            else {
                respuestaEstado = segBean.estadoPin("01", session.getCliente().getRut(), session.getCliente().getDigito(), "12");
            }

            if (getLogger().isDebugEnabled()) {getLogger().debug("[Passcodepaso1] - session.getRutUsuario [" + session.getRutUsuario() + "-"+session.getDvUsuario()+ "] " +"estado passcode ["+respuestaEstado.estado+"]");}
            session.setAttrib("estadoPassCode",    respuestaEstado.estado);
            session.setAttrib("motivoPassCode",    respuestaEstado.motivo);
            session.setAttrib("ultModPassCode",    respuestaEstado.fechaModEstado);
            session.setAttrib("estadoAntPassCode", respuestaEstado.estado2);
            session.setAttrib("modalidadPassCode", String.valueOf(respuestaEstado.modalidad));
            session.setAttrib("fechaExpPassCode",  respuestaEstado.fecExpiracion);

            //Secci�n agregada para "mejoras Clave Dos" - M�s informaci�n de solicitud de clave dos
            RetornoTipCli datosBasicosBP = null;
            /* Se determina si el cliente pertenece a Banca Preferencial */
            datosBasicosBP = session.getCliente().getDatosBasicosGenerales();

            /*
            Si el tipo de cliente es igual a "P", el tipo de banca es
            obtenido desde la variable DatPerson de los datosBasicos,
            sino, el tipo de banca se obtiene desde DatEmpresa
             */
            String tipoBcaBP = datosBasicosBP.TipoCliente.equalsIgnoreCase("P")? datosBasicosBP.DatPersona.TipoBca: datosBasicosBP.DatEmpresa.TipoBca;

            /* "PBP" es banca preferencial */
            boolean bancaPrefBP = tipoBcaBP.equalsIgnoreCase("PBP");

            session.setAttrib("BancaPreferencial",new Boolean(bancaPrefBP));

            SolicitudPassCode solicitudPasscode = new SolicitudPassCode();
            if (respuestaEstado.estado.equals("  ")) {
                solicitudPasscode = segBean.consultaSolicitudPassCode("12", session.getCliente().getRut());
            }
            session.setAttrib("estadoSolicitudPasscode", solicitudPasscode );
            //Fin nueva secci�n para Mejoras Clave Dos"

            if(getLogger().isDebugEnabled()){
                getLogger().debug("[Passcodepaso1] - session.getRutUsuario [" + session.getRutUsuario() + "-"+session.getDvUsuario()+ "] " + "fechaActual ["+fechaActual+"]");
                getLogger().debug("[Passcodepaso1] - session.getRutUsuario [" + session.getRutUsuario() + "-"+session.getDvUsuario()+ "] " + "fecExpiracion ["+respuestaEstado.fecExpiracion+"]");
            }
            if ((!respuestaEstado.estado.equals("  "))&& (fechaActual.after(respuestaEstado.fecExpiracion)))
                return true;
        }
        catch(GeneralException ge){
            if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[PassCodePaso1][BCI_FINEX][Exception] Error["+ge.getMessage()+"]", ge);}
            //Agregado para proyecto Mejoras Seguridad FASE 1
            ge = (GeneralException)ControlErrorMsg.modificaCodigo((Exception)ge, session);
            if (ge.getCodigo().equals("0438")){
                if (getLogger().isDebugEnabled()) {getLogger().debug("[Passcodepaso1] - session.getRutUsuario [" + session.getRutUsuario() + "-"+session.getDvUsuario()+ "] error 0438 ");}
                session.setAttrib("estadoPassCode",    "04");
                session.setAttrib("motivoPassCode",    "  ");
                session.setAttrib("estadoAntPassCode", "04");
                session.setAttrib("modalidadPassCode", "N");
            }
            else{
                if (getLogger().isDebugEnabled()) {getLogger().debug("[Passcodepaso1] - session.getRutUsuario [" + session.getRutUsuario() + "-"+session.getDvUsuario()+ "] " +"Exception11 : " + ge.toString());}
                throw ge;
            }
        }
        catch(Exception se){
            if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[PassCodePaso1][BCI_FINEX][Exception] Error["+se.getMessage()+"]", se);}
            //Agregado para proyecto Mejoras Seguridad FASE 1
            se = ControlErrorMsg.modificaCodigo(se, session);
            throw se;
        }
        return false;
    }


    /**
     * M�todo que autentica la sesi�n para canal passcode (120).
     * <p>
     * Nota: Si tiene indicador de cambio de clave, se despacha al JSP de cambio
     * de clave, de lo contrario, se despacha a la p�gina de TTFF.
     * Registro de versiones:<ul>
     *
     * <li>1.0 (??/??/????, Desconocido): Versi�n inicial
     *
     * <li>1.1 (06/12/2004, Victor Urra (Novared)): Modificaciones para proyecto mejoras
     *          de seguridad fase 3. Journalizaci�n de eventos de configuraci�n token.
     * <li>1.2 (05/07/2011, Angelo Vel�squez V (SEnTRA)): Se reemplaza LogFile por Log4j.
     * <li>1.3 02/11/2015, Diego Urrutia Guerra(Imagemaker) - Mario Lorca (BCI): Se normaliza logs.</li>
     *
     * </ul>
     * <p>
     *
     * @param session Sesi�n del cliente
     * @param req El request
     * @param res El response
     * @param pin_param Clave passcode del cliente.
     * @exception GeneralException GeneralException.
     * @exception Exception Exception.
     * @since 1.0
     */
    private void PassCodePaso2(SessionBCI session, HttpServletRequest req, HttpServletResponse res, String pin_param) 
            throws GeneralException, Exception{

        Journal journal = null;
        String userId   = null;
        String canalId = req.getParameter("canal");
        try{
            Vector letras   = new Vector();
            letras          = (Vector)session.getAttrib("passcode");
            userId          = String.valueOf(session.getCliente().getRut());
            String cl       = "";
            String despacho = null;

            for (int k =0; k < pin_param.length(); k++){
                cl = cl + letras.get(new Integer(pin_param.substring(k,k+1)).intValue());
            }

            session.autentica(cl, "120"); // Autentifica la Sesion

            if (session.indCambioClave)
                despacho = "/seguridadwls/RealizaCambioClave?cambio=0&paso=2";

            else {
                session.setAttrib("autenticado", "Si");
                despacho = (String)session.getAttrib("dispatcherPassCode");
            }

            /* Mejoras Seguridad Fase 3, journalizacion de eventos claves*/
            if(canalId == null || (canalId != null && !canalId.trim().equals("215"))){
                journalizacion(session,EVE_LOGIN,SUB_COD_OK,String.valueOf(session.getCliente().getRut()),ID_PRODPASSCODE,null);
            }

            this.getServletContext().getRequestDispatcher(res.encodeURL(despacho)).forward(req, res);
            /* 20050314: L�nea siguiente agregada por MMC */
            session.removeAttrib("dispatcherPassCode");
            return;

        }
        catch (wcorp.serv.clientes.ClientesException ex){
            wcorp.serv.clientes.ClientesException ce = (wcorp.serv.clientes.ClientesException)ex;

            /* Mejoras Seguridad Fase 3, journalizacion de eventos claves*/
            if(canalId == null || (canalId != null && !canalId.trim().equals("215"))){
                journalizacion(session,EVE_LOGIN,SUB_COD_ERROR,String.valueOf(session.getCliente().getRut()),ID_PRODPASSCODE,ce.getCodigo());
            }
            if (getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[PassCodePaso2][BCI_FINEX][ClientesException] Error: ["+ce.getMessage()+"]", ce);}

            if (ce.getInfoAdic().startsWith("Cliente no existe"))
                throw new wcorp.serv.seguridad.SeguridadException("USERLOGIN");

            session.cerrar(); // Cierra la Sesion
            throw ex;
        }
        catch (wcorp.serv.seguridad.SeguridadException ex){
            wcorp.serv.seguridad.SeguridadException se = (wcorp.serv.seguridad.SeguridadException)ex;

            /* Mejoras Seguridad Fase 3, journalizacion de eventos claves*/
            if(canalId == null || (canalId != null && !canalId.trim().equals("215"))){
                journalizacion(session,EVE_LOGIN,SUB_COD_ERROR,String.valueOf(session.getCliente().getRut()),ID_PRODPASSCODE,ex.getCodigo());
            }
            if (getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[PassCodePaso2][BCI_FINEX][SeguridadException] Error: ["+se.getMessage()+"]", se);}

            if (se.getCodigo().equalsIgnoreCase("USERLOGIN"))
                throw new wcorp.serv.seguridad.SeguridadException("USERLOGIN");
            else if (se.getCodigo().equals("0432")){
                this.getServletContext().getRequestDispatcher(res.encodeURL("/seguridadwls/PassCode?paso=0")).forward(req, res);
                return;
            }
            else if (se.getCodigo().equals("0433")){
                this.getServletContext().getRequestDispatcher(res.encodeURL("/seguridadwls/RealizaCambioClave?cambio=0&paso=2")).forward(req, res);
                return;
            }
            else if ((se.getCodigo().compareTo("0430")==0) || (se.getCodigo().compareTo("0431")==0) || (se.getCodigo().compareTo("0434")==0) || (se.getCodigo().compareTo("0436")==0) || (se.getCodigo().compareTo("0437")==0) || (se.getCodigo().compareTo("0438")==0) || (se.getCodigo().compareTo("0439")==0))
                throw new wcorp.serv.seguridad.SeguridadException("PC" + se.getCodigo());
            else{
                session.cerrar(); // Cierra la Sesion
                throw ex;
            }
        }
        catch (wcorp.util.GeneralException ex){
            wcorp.util.GeneralException ge = (wcorp.util.GeneralException)ex;
            /* Mejoras Seguridad Fase 3, journalizacion de eventos claves*/
            if(canalId == null || (canalId != null && !canalId.trim().equals("215"))){
                journalizacion(session,EVE_LOGIN,SUB_COD_ERROR,String.valueOf(session.getCliente().getRut()),ID_PRODPASSCODE,ex.getCodigo());
            }
            session.cerrar(); // Cierra la Sesion
            if (getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[PassCodePaso2][BCI_FINEX][GeneralException] Error: ["+ge.getMessage()+"]", ge);}
            throw ex;
        }
        catch (Exception ex){
            /* Mejoras Seguridad Fase 3, journalizacion de eventos claves*/
            if(canalId == null || (canalId != null && !canalId.trim().equals("215"))){
                journalizacion(session,EVE_LOGIN,SUB_COD_ERROR,String.valueOf(session.getCliente().getRut()),ID_PRODPASSCODE,ex.getMessage());
            }
            session.cerrar(); // Cierra la Sesion
            if (getLogger().isEnabledFor(Level.ERROR)) {getLogger().error("[PassCodePaso2][BCI_FINEX][Exception] Error: ["+ex.getMessage()+"]", ex);}
            throw ex;
        }
    }

    /**
     * M�todo que autentica la sesi�n para canal supuser (150: BCIDEMP).
     * <p>
     * Registro de versiones:<ul>
     *
     * <li>1.0 (??/??/????, Desconocido): Versi�n inicial
     *
     * <li>1.1 (06/12/2004, Victor Urra (Novared)): Modificaciones para proyecto mejoras
     *          de seguridad fase 3. Journalizaci�n de eventos de configuraci�n token.
     * <li>1.2 (05/07/2011, Angelo Vel�squez V (SEnTRA)): Se reemplaza LogFile por Log4j.
     * <li>1.3 02/11/2015, Diego Urrutia Guerra(Imagemaker) - Mario Lorca(BCI): Se realiza normalizacion de logs.</li>
     *
     * </ul>
     * <p>
     *
     * @param session Sesi�n del cliente
     * @param req El request
     * @param res El response
     * @param pin_param Clave internet del cliente
     * @exception GeneralException GeneralException
     * @exception Exception Exception
     * @since 1.0
     */
    private void supUserPaso1(SessionBCI session, HttpServletRequest req, HttpServletResponse res, String pin_param) throws GeneralException, Exception{

        Journal journal       = null;
        String userId         = null;
        SessionBCI sessionTmp = null;
        String canalId = req.getParameter("canal");
        try{

            String userIdusu = req.getParameter("usuario");
            pin_param = req.getParameter("clave");
            if (getLogger().isDebugEnabled()){getLogger().debug("[supUserPaso1] el super usuario UYP es: " +  userIdusu);}

            userId = userIdusu.trim();
            if (userId.compareTo("") == 0 | pin_param.compareTo("")==0)
                throw new wcorp.util.GeneralException("PARAM", "Valores Insuficientes");

            String pin = pin_param;
            sessionTmp = new SessionBCI("150", userId, null);
            if(session!=null)
                session.setIdSession(req.getSession().getId());
            sessionTmp.setIP(req.getRemoteAddr());
            if (getLogger().isDebugEnabled()){getLogger().debug("[supUserPaso1] La sesion temporal fue creada: " +  "150" + " " + userId  +  " " );}

            try{
                sessionTmp.autentica(pin); // Autentifica la Sesion
            }
            catch (wcorp.serv.clientes.ClientesException ex){
                if (getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[supUserPaso1][BCI_FINEX][ClientesException] Error: ["+ex.toString()+"]", ex);}
                wcorp.serv.clientes.ClientesException ce = (wcorp.serv.clientes.ClientesException)ex;

                /* Mejoras Seguridad Fase 3, journalizacion de eventos claves*/
                if(canalId == null || (canalId != null && !canalId.trim().equals("215"))){
                    journalizacion(sessionTmp,EVE_LOGINSUP,SUB_COD_ERROR,userId,ID_PRODINTERNET,ce.getCodigo());
                }

                if (ce.getInfoAdic().startsWith("Cliente no existe"))
                    throw new wcorp.serv.seguridad.SeguridadException("USERLOGIN");

                sessionTmp.cerrar(); // Cierra la Sesion
            }
            catch (wcorp.serv.seguridad.SeguridadException ex){
                //Agregado para proyecto Mejoras Seguridad FASE 1
                ex = (SeguridadException)ControlErrorMsg.modificaCodigo((Exception)ex, session);
                if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[supUserPaso1][BCI_FINEX][SeguridadException] Error: ["+ex.toString()+"]", ex);}
                wcorp.serv.seguridad.SeguridadException se = (wcorp.serv.seguridad.SeguridadException)ex;

                /* Mejoras Seguridad Fase 3, journalizacion de eventos claves*/
                if(canalId == null || (canalId != null && !canalId.trim().equals("215"))){
                    journalizacion(sessionTmp,EVE_LOGINSUP,SUB_COD_ERROR,userId,ID_PRODINTERNET,ex.getCodigo());
                }

                if (se.getCodigo().equalsIgnoreCase("USERLOGIN"))
                    throw new wcorp.serv.seguridad.SeguridadException("USERLOGIN");
                sessionTmp.cerrar(); // Cierra la Sesion
            }
            catch (wcorp.util.GeneralException ex){
                if(getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[supUserPaso1][BCI_FINEX][GeneralException] Error: ["+ex.toString()+"]", ex);}
                wcorp.util.GeneralException ge = (wcorp.util.GeneralException)ex;

                /* Mejoras Seguridad Fase 3, journalizacion de eventos claves*/
                if(canalId == null || (canalId != null && !canalId.trim().equals("215"))){
                    journalizacion(sessionTmp,EVE_LOGINSUP,SUB_COD_ERROR,userId,ID_PRODINTERNET,ge.getCodigo());
                }
                sessionTmp.cerrar(); // Cierra la Sesion
                throw ge;
            }
            catch (Exception ex){
                if (getLogger().isEnabledFor(Level.ERROR)) {getLogger().error("[supUserPaso1][BCI_FINEX][Exception] Error: ["+ex.toString()+"]", ex);}
                /* Mejoras Seguridad Fase 3, journalizacion de eventos claves*/
                if(canalId == null || (canalId != null && !canalId.trim().equals("215"))){
                    journalizacion(sessionTmp,EVE_LOGINSUP,SUB_COD_ERROR,userId,ID_PRODINTERNET,ex.getMessage());
                }
                sessionTmp.cerrar(); // Cierra la Sesion
                throw ex;

            }

            session.setAttrib("supUser", "Si");
            String despacho = (String)session.getAttrib("dispatcherSupUser");
            if (getLogger().isDebugEnabled()){getLogger().debug("[supUserPaso1] despacha" + despacho );}

            /* Mejoras Seguridad Fase 3, journalizacion de eventos claves*/
            if(canalId == null || (canalId != null && !canalId.trim().equals("215"))){
                journalizacion(session,EVE_LOGINSUP,SUB_COD_OK,userId,ID_PRODINTERNET,null);
            }

            sessionTmp.cerrar();			
            this.getServletContext().getRequestDispatcher(res.encodeURL(despacho)).forward(req, res);
            return;
        } 
        catch (Exception ex){
            if (getLogger().isEnabledFor(Level.ERROR)) {getLogger().error("[supUserPaso1][BCI_FINEX][Exception] Error: ["+ex.toString()+"]", ex);}
        }
    }

    /**
     * 
     * <p>
     * Registro de versiones:<ul>
     *
     * <li>1.0 (??/??/????, Desconocido): Versi�n inicial
     * <li>1.1 (24/07/2007, Luis Ibarra (Progesys)): se reemplazan mensajes en duro, por tabla de valores
     * <li>1.2 (12/08/2009, Marcelo Avendano (Bci)): Se modifica el orden en que se setea el atributo pd en sesion.
     * <li>1.3 (24/06/2011, Angelo Vel�squez Vel�squez (SEnTRA)): Se agrega instancia para recuperar el valor del estadoPopUp, esta 
     *         variable indica la forma en que trabajan las p�ginas de pague directo
     *         (Pop-up o No pop-up), seg�n el convenio con que se entra al servlet Login.java
     *         Se reemplaza LogFile por Log4j.
     * <li>1.4 02/11/2015, Diego Urrutia Guerra(Imagemaker) - Mario Lorca (BCI): Se normaliza Logs.
     *                                                            
     * </ul>
     * <p>
     *
     * @param ses ses.
     * @param res res.
     * @param req req.
     * @param canalId canalId.
     * @throws Exception Exception.
     * @since 1.1
     */
    private void PagueDirectoPaso0(HttpSession ses, HttpServletRequest req, HttpServletResponse res, String canalId) throws Exception{

        ConvenioPDir convenio = new ConvenioPDir();
        ServiciosPagosDelegate businessDelegate = ServiciosPagosDelegate.getInstance();
        int cantidad = 0;

        ses = req.getSession(true);
        PagueDirecto pd = new PagueDirecto();
        pd.setBco(req.getParameter("bco"));
        pd.setAction(req.getParameter("action"));
        pd.setIp(req.getParameter("ip"));
        pd.setCnvnum(req.getParameter("cnvnum"));
        pd.setProto(req.getParameter("proto"));
        pd.setRmiserver(req.getParameter("rmiserver"));
        String cpr = URLDecoder.decode(req.getParameter("compra"));
        pd.setCompra(cpr);
        pd.setCstenv(req.getParameter("cstenv"));
        pd.setPagret(req.getParameter("pagret"));
        pd.setTrx(req.getParameter("trx"));

        if (getLogger().isDebugEnabled()) {
            getLogger().debug("[PagueDirectoPaso0] bco       : " + pd.getBco());
            getLogger().debug("[PagueDirectoPaso0] action    : " + pd.getAction());
            getLogger().debug("[PagueDirectoPaso0] ip        : " + pd.getIp());
            getLogger().debug("[PagueDirectoPaso0] cnvnum    : " + pd.getCnvnum());
            getLogger().debug("[PagueDirectoPaso0] proto     : " + pd.getProto());
            getLogger().debug("[PagueDirectoPaso0] rmiserver : " + pd.getRmiserver());
            getLogger().debug("[PagueDirectoPaso0] compra    : " + pd.getCompra());
            getLogger().debug("[PagueDirectoPaso0] cstenv    : " + pd.getCstenv());
            getLogger().debug("[PagueDirectoPaso0] pagret    : " + pd.getPagret());
            getLogger().debug("[PagueDirectoPaso0] trx       : " + pd.getTrx());
        }

        ic = JNDIConfig.getInitialContext();
        pagoshome    = (ServiciosPagosHome)ic.lookup("wcorp.serv.pagos.ServiciosPagos");
        pagosbean    = (ServiciosPagos)pagoshome.create();

        convenio = pagosbean.convenioPagueDirecto(pd.getCnvnum(),pd.getIp(),pd.getRmiserver());

        if (convenio.getCodigoError() != null)
            throw new wcorp.serv.seguridad.SeguridadException(convenio.getCodigoError().substring(1,convenio.getCodigoError().length()));

        pd.setImagen(convenio.getNombre().trim());

        try{
			boolean estadoPopUp = businessDelegate.obtenerEstadoPopup(pd.getCnvnum());
            req.setAttribute("estadoPopUp", new Boolean(estadoPopUp));
        }
        catch(Exception ex){}

        String numTrx      = StringUtil.completaPorLaIzquierda(pd.getTrx(), 20, '0');


        if (pagosbean.existeSerialPagueDirecto(numTrx, pd.getCnvnum())) {
            if (getLogger().isDebugEnabled()){getLogger().debug("[PagueDirectoPaso0] Login.jsp : Serial Repetido" + pd.getCnvnum() + "-" + pd.getTrx());}
            throw new wcorp.util.GeneralException("ESPECIAL",TablaValores.getValor("mensajePagueDirecto.parametros", "TrxRegistrada", "mensaje"));
        }

        ses.setAttribute("PagueDirecto", pd);
        cantidad = pagosbean.validarExistCompra(pd.getCnvnum(),numTrx);

        if (cantidad > 0)
            throw new wcorp.util.GeneralException("ESPECIAL", "Pago ya efectuado");

        else{
            Canal canal = new Canal(canalId);
            if (getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[PagueDirectoPaso0] Login.jsp : " + pd.getCnvnum() + "-" + pd.getTrx());}
            this.getServletContext().getRequestDispatcher(res.encodeURL(canal.getPathJSP() + "/paguedirecto/login.jsp")).forward(req,res);
            return;
        }
    }

    /**
     *
     * Registro de versiones:<ul>
     * <li> 1.0 (xx/xx/xxxx, Desconocido : Version inicial)
     * <li> 2.0 (08/09/2006, Paola Parra (SEnTRA)): Se agrega log para debug de consulta de clave dos.
     * <li> 2.1 (24/07/2007, Luis Ibarra (Progesys)): Se cambian los mensajes enduros por una tabla de valor
     * <li> 2.2 (02/10/2007, Jorge Garcia(BCI)): Determinar tipo de cliente seg�n existencia de convenio empresa.
     * <li> 2.3 27/05/2010, Etson Mora (Imagemaker IT): Se cambia la clase Integer por la clase Long en la validaci�n
     * del monto m�ximo permitido, para soportar m�s altas y se elimina c�digo comentado.
     * <li> 2.4 (24/06/2011, Angelo Vel�squez Vel�squez (SEnTRA)): Se setea en el Request la variable estadoPopUp, para ocuparla 
     * en el JSP login_apo.jsp. Se reemplaza LogFile por Log4j.
     * <li> 2.5 02/11/2015, Diego Urrutia Guerra(Imagemaker) - Mario Lorca (BCI): Normalizacion Logs.
     * </ul>
     * @param ses Sesion
     * @param req request
     * @param res response
     * @param canal canal
     * @return boolean PagueDirectoPaso1
     * @throws Exception Exception
     * @since 2.2
     */
    private boolean PagueDirectoPaso1(HttpSession ses, HttpServletRequest req, HttpServletResponse res, Canal canal) throws Exception{
        int cantidad      = 0;
        int numCompras    = 0;
        PagueDirecto pd   = (PagueDirecto)ses.getAttribute("PagueDirecto");
        String compra     = pd.getCompra()+"|";
        String fecha      = new SimpleDateFormat("yyyyMMdd").format(new Date());

        String userIdrut  = req.getParameter("rut");
        String userIdAux  = req.getParameter("dig");

        req.setAttribute("estadoPopUp", req.getParameter("estadoPopUp")==null?"true": String.valueOf(req.getParameter("estadoPopUp")));

        if (getLogger().isDebugEnabled()){getLogger().debug("[PagueDirectoPaso1] rut["+userIdrut+"-"+userIdAux+"]");}

        while(compra.indexOf("|") != -1){
            compra   = compra.substring(compra.indexOf("|")+1, compra.length());
            compra   = compra.substring(compra.indexOf("|")+1, compra.length());
            compra   = compra.substring(compra.indexOf("|")+1, compra.length());
            numCompras++;
        }

        ic = JNDIConfig.getInitialContext();
        clientesHome = (ServiciosClienteHome)ic.lookup("wcorp.serv.clientes.ServiciosCliente");
        clientesBean = (ServiciosCliente)clientesHome.create();
        pagoshome    = (ServiciosPagosHome)ic.lookup("wcorp.serv.pagos.ServiciosPagos");
        pagosbean    = (ServiciosPagos)pagoshome.create();

        String[] desCompra = new String[numCompras];
        int[]    unidades  = new int[numCompras];
        long[]   precio    = new long[numCompras];
        int      i         = 0;
        compra             = pd.getCompra()+"|";
        int     costoEnvio = new Integer(pd.getCstenv()).intValue();

        if (getLogger().isDebugEnabled()) {getLogger().debug("[PagueDirectoPaso1] rut["+userIdrut+"-"+userIdAux+"] costoEnvio ["+costoEnvio+"]");}
        if (costoEnvio < 0)
            throw new wcorp.util.GeneralException("ESPECIAL", TablaValores.getValor("mensajePagueDirecto.parametros", "MontoNegativo", "mensajeEnvioNegativo"));
        else
            pd.setMontoCompra(costoEnvio);

        while(compra.indexOf("|") != -1){
            desCompra[i] = (compra.substring(0,compra.indexOf("|"))).length() > 20 ? (compra.substring(0,compra.indexOf("|"))).substring(0, 20) : (compra.substring(0,compra.indexOf("|")));
                    compra = compra.substring(compra.indexOf("|")+1, compra.length());
                    precio[i] = new Long(compra.substring(0,compra.indexOf("|"))).longValue();

                    if (precio[i] < 0)
                        throw new wcorp.util.GeneralException("ESPECIAL", TablaValores.getValor("mensajePagueDirecto.parametros", "MontoNegativo", "mensaje"));

                    compra         = compra.substring(compra.indexOf("|")+1, compra.length());
                    unidades[i]    = new Integer(compra.substring(0,compra.indexOf("|"))).intValue();
                    compra         = compra.substring(compra.indexOf("|")+1, compra.length());
                    pd.setMontoCompra(pd.getMontoCompra() + precio[i] * unidades[i]);
                    i++;
        }

        pd.setGlosaCompra(desCompra);
        pd.setPrecio(precio);
        pd.setUnidades(unidades);

        cantidad = pagosbean.compraSimilar(pd.getCnvnum(), new Integer(userIdrut).longValue(), pd.getMontoCompra(), fecha);

        if (getLogger().isDebugEnabled()) {getLogger().debug("[PagueDirectoPaso1] rut["+userIdrut+"-"+userIdAux+"] cantidad ["+cantidad+"]");}
        if (cantidad > 0)
            pd.setCompraSimilar('S');

        else
            pd.setCompraSimilar('N');

        Operacion[] ope = clientesBean.consultaOperaciones(new Integer(userIdrut).longValue(), userIdAux.charAt(0), 'T');
        pd.setOperaciones(ope);

        ses.setAttribute("PagueDirecto", pd);

        Cliente cliente = new Cliente(new Integer(userIdrut).longValue(),userIdAux.charAt(0));
        cliente.setDatosBasicos();
        TipoConvPDir tipoConv = pagosbean.tipoConvPagueDirecto(pd.getCnvnum());

        if (getLogger().isDebugEnabled()) {getLogger().debug("[PagueDirectoPaso1] rut["+userIdrut+"-"+userIdAux+"] tipoUsuario["+cliente.tipoUsuario+"]");}

        //Obtener convenios empresa vigentes
        String[] convenio = (String[])pagosbean.convenioInternet(userIdrut);
        if (getLogger().isDebugEnabled()) {getLogger().debug("[PagueDirectoPaso1] rut["+userIdrut+"-"+userIdAux+"] CantidadConvenios["+convenio.length+"]");}

        if (cliente.tipoUsuario == null)
            throw new wcorp.serv.seguridad.SeguridadException("USERLOGIN");

        else if (cliente.tipoUsuario.equals("P") && convenio.length == 0){
            if (getLogger().isDebugEnabled()) {getLogger().debug("[PagueDirectoPaso1] rut["+userIdrut+"-"+userIdAux+"] PERSONA");}
            if ((tipoConv.getTipoConvenio().equals("BYS") == true) && ((new Long(tipoConv.getMontoMaxAut())).longValue() < pd.getMontoCompra()) &&  (new Long(tipoConv.getMontoMaxAut())).longValue() > 0)
                throw new wcorp.util.GeneralException("ESPECIAL", TablaValores.getValor("mensajePagueDirecto.parametros", "MontoMaximo", "maximosuperado"));

            return true;
        }

        else if (cliente.tipoUsuario.equals("E") || cliente.tipoUsuario.equals("P")){

            getLogger().debug("EMPRESA");
            pd.setRutEmpresa(new Integer(userIdrut).longValue());
            ses.setAttribute("PagueDirecto", pd);
            if (convenio.length == 0){
                getLogger().debug("Sin Convenio");
                if (tipoConv.getTipoConvenio().equals("BYS") == true){
                    throw new wcorp.util.GeneralException("ESPECIAL", TablaValores.getValor("mensajePagueDirecto.parametros", "NoAutorizado", "sitio"));
                }
                /*Validaci�n para saber si la empresa sin convenio tiene o no pagos realizados
                por PagueDirecto anteriores*/
                String tienePagos = pagosbean.tienePagosPorRut(userIdrut);
                if (getLogger().isDebugEnabled()) {getLogger().debug("[PagueDirectoPaso1] El tiene Pagos dice "+ tienePagos );}
                if (tienePagos.equals("0")){
                    throw new wcorp.util.GeneralException("ESPECIAL", TablaValores.getValor("mensajePagueDirecto.parametros", "NoConvExpress", "mensaje"));
                }
                return true;
            }
            else{
                getLogger().debug("Con Convenio");

                //en vez de IMF debe ser BYS
                if ((tipoConv.getTipoConvenio().equals("BYS") == true) && ((new Long(tipoConv.getMontoMaxAut())).longValue() < pd.getMontoCompra()) &&  (new Long(tipoConv.getMontoMaxAut())).longValue() > 0){
                    throw new wcorp.util.GeneralException("ESPECIAL",TablaValores.getValor("mensajePagueDirecto.parametros", "MontoMaximo", "maximosuperado"));
                }
                ses.setAttribute("rutEmpresa", userIdrut);
                if (getLogger().isDebugEnabled()) {getLogger().debug("Login_apo.jsp : " + pd.getCnvnum() + "-" + pd.getTrx() + "-" + userIdrut);}				
                this.getServletContext().getRequestDispatcher(res.encodeURL(canal.getPathJSP() + "/paguedirecto/login_apo.jsp")).forward(req,res);
                return false;
            }
        }
        else
            throw new wcorp.serv.seguridad.SeguridadException("USERLOGIN");
    }

    /**
     * 
     * <p>
     * Registro de versiones:<ul>
     *
     * <li>1.0 (??/??/????, Desconocido): Versi�n inicial
     *
     * <li>1.1 (24/07/2007, Luis Ibarra (Progesys)): se reemplazan mensajes en duro, por tabla de valores
     * <li>1.2 (05/07/2011, Angelo Vel�squez V (SEnTRA)): Se reemplaza LogFile por Log4j.
     * <li>1.3 02/11/2015, Diego Urrutia Guerra(Imagemaker) - Mario Lorca (BCI): Se normaliza logs.
     *
     * </ul>
     * <p>
     * @param req request.
     * @throws Exception Exception.
     * @since 1.1
     */
    private void PagueDirectoPaso2(HttpServletRequest req) throws Exception{

        String userIdrut   = req.getParameter("rut");
        HttpSession ses    = req.getSession(false);
        long rutEmpresa    = new Integer((String)ses.getAttribute("rutEmpresa")).longValue();
        long rutApoderado  = new Integer(userIdrut).longValue();

        int cantidad       = 0;
        int precio         = 0;
        int unidades       = 0;
        PagueDirecto pd    = (PagueDirecto)ses.getAttribute("PagueDirecto");
        String fecha       = new SimpleDateFormat("yyyyMMdd").format(new Date());
        long monto         = pd.getMontoCompra();

        ic = JNDIConfig.getInitialContext();
        pagoshome    = (ServiciosPagosHome)ic.lookup("wcorp.serv.pagos.ServiciosPagos");
        pagosbean    = (ServiciosPagos)pagoshome.create();

        req.setAttribute("estadoPopUp", req.getParameter("estadoPopUp")==null?"true": String.valueOf(req.getParameter("estadoPopUp")));

        cantidad = pagosbean.compraSimilar(pd.getCnvnum(), rutEmpresa, monto, fecha);

        if (cantidad > 0)
            pd.setCompraSimilar('S');

        else
            pd.setCompraSimilar('N');

        TipoConvPDir tipoConv = pagosbean.tipoConvPagueDirecto(pd.getCnvnum());
        if (getLogger().isDebugEnabled()){getLogger().debug("[PagueDirectoPaso2] Tipo Convenio : " + tipoConv.getTipoConvenio());}
        String autorizaImpFa = null;

        if (tipoConv.getTipoConvenio().equals("IMF") == true){
            autorizaImpFa = pagosbean.validaPerImpFa(rutApoderado, rutEmpresa);
            if (getLogger().isDebugEnabled()) {getLogger().debug("[PagueDirectoPaso2] autorizaImpFa : " + autorizaImpFa);}

            if(autorizaImpFa == null){
                tipoConv.setTipoConvenio("IMP");
                if (getLogger().isDebugEnabled()){getLogger().debug("[PagueDirectoPaso2] Tipo Convenio : " + tipoConv.getTipoConvenio());}
            }
            else{
                String autorizaFirPo = pagosbean.validaCnvFirPo(autorizaImpFa, rutEmpresa);
                if (getLogger().isDebugEnabled()){getLogger().debug("[PagueDirectoPaso2] autorizaFirPo : " + autorizaFirPo);}
                if (autorizaFirPo == null){
                    autorizaFirPo = "N";
                }
                pd.setFirmaPod(autorizaFirPo);
                pd.setConvenioBEL(autorizaImpFa);
            }
        }

        ses.setAttribute("PagueDirecto", pd);

        ApoderadoEmpresa[] apoderadoEmpresa = (ApoderadoEmpresa[])pagosbean.apoderadoEmpresa(rutEmpresa, rutApoderado);

        if (getLogger().isDebugEnabled()){getLogger().debug("[PagueDirectoPaso2] valPagueDirecto : " + rutApoderado + "-" + rutEmpresa + "- [" +tipoConv.getTipoConvenio() + "]");}
        if (apoderadoEmpresa == null || apoderadoEmpresa.length == 0){
            throw new wcorp.util.GeneralException("ESPECIAL", TablaValores.getValor("mensajePagueDirecto.parametros", "Apoderaro", "noPertenece"));
        }

        if ((tipoConv.getTipoConvenio().equals("IMP") == true) && (pagosbean.valPagoPagueDirecto(rutApoderado, rutEmpresa, "Prod01","XPDIR01") == 'N') && (pagosbean.valPagoPagueDirecto(rutApoderado, rutEmpresa,"Prod21","XPDIR01") == 'N')) {
            throw new wcorp.util.GeneralException("ESPECIAL", TablaValores.getValor("mensajePagueDirecto.parametros", "NoAutorizado", "sitio"));
        }

        if ((tipoConv.getTipoConvenio().equals("IMF") == true) && (pagosbean.valPagoPagueDirecto(rutApoderado, rutEmpresa, "Prod01","XPDIR03") == 'N') && (autorizaImpFa == null) && (pagosbean.valPagoPagueDirecto(rutApoderado, rutEmpresa,"Prod21","XPDIR03") == 'N')) {
            throw new wcorp.util.GeneralException("ESPECIAL", TablaValores.getValor("mensajePagueDirecto.parametros", "NoAutorizado", "sitio"));
        }

        if ((tipoConv.getTipoConvenio().equals("BYS") == true) && (pagosbean.valPagoPagueDirecto(rutApoderado, rutEmpresa, "Prod01","XPDIR02") == 'N') && (pagosbean.valPagoPagueDirecto(rutApoderado, rutEmpresa,"Prod21","XPDIR02") == 'N')) {
            throw new wcorp.util.GeneralException("ESPECIAL", TablaValores.getValor("mensajePagueDirecto.parametros", "NoAutorizado", "sitio"));
        }
    }

    /**
     *
     * Registro de versiones:<ul>
     * <li> 1.0 (xx/xx/xxxx, Desconocido : Version inicial)
     * <li> 2.0 (08/09/2006, Paola Parra (SEnTRA)): Se agrega log para debug de consulta de clave dos.
     * <li> 2.1 (05/07/2011, Angelo Vel�squez V (SEnTRA)): Se reemplaza LogFile por Log4j.
     * <li> 2.2 02/11/2015, Diego Urrutia Guerra(Imagemaker) - Mario Lorca (BCI): Se normaliza Logs.
     *</ul>
     * @param convenio convenio.
     * @param trx trx.
     * @since 2.2
     */
    private void enviarRespuesta(String convenio, String trx){
        if (convenio == null  ||  trx == null )
            return;

        URL url = null;
        HttpURLConnection conn = null;
        String respuesta  = "rtabco=";

        try {
            respuesta += StringUtil.completaPorLaIzquierda(trx, 20, '0');
            respuesta += "0434";
            if (getLogger().isDebugEnabled()) {
                getLogger().debug("[enviarRespuesta] Host to Host : " + convenio + "-" + trx + "-" + respuesta);
                getLogger().debug("[enviarRespuesta] Host to Host : "+ convenio + "-" + trx + "-" + respuesta);
            }
            String urlcgi       = TablaValores.getValor("PagueDirecto.parametros","urlcgi", "desc");
            String urlRespuesta = urlcgi + convenio;
            url = new URL(urlRespuesta);
            conn = (HttpURLConnection)url.openConnection();
            conn.setDoInput(true);
            conn.setDoOutput(true);
            conn.setRequestMethod("POST");
            OutputStream out = conn.getOutputStream();
            for (int i=0; i<respuesta.length(); i++)
                out.write((int)respuesta.charAt(i));

            out.flush();
            out.close();

            if (getLogger().isDebugEnabled()) {getLogger().debug("[enviarRespuesta] Respuesta  : " + conn.getResponseCode());}

        }
        catch (Exception e) {
            if (getLogger().isEnabledFor(Level.ERROR)){getLogger().error("[enviarRespuesta] Exception : " + e);}
        }
    }

    /**
     * M�todo setServiciosEmpresas
     * 
     * Registro de versiones:<ul>
     *
     * <li>1.0 ??/??/????, Desconocido: Versi�n inicial.
     * <li> 1.1 02/11/2015, Diego Urrutia Guerra(Imagemaker) - Mario Lorca (BCI): Se normaliza Logs.
     * </ul>
     * <p>
     *
     * @param session session
     * @param userIdrut userIdrut
     * @param idConvenio id convenio
     * @param rutEmpresa Rut empresa
     * @throws Exception Retorna una excepci�n si ocurre algun problema.
     * @return SessionBCI
     * @since 1.0
     */
    private SessionBCI setServiciosEmpresas(SessionBCI session, long userIdrut, String idConvenio, long rutEmpresa)
    throws Exception{

        ic          = JNDIConfig.getInitialContext();
        cuentashome = (ServiciosCuentasHome)ic.lookup("wcorp.serv.cuentas.ServiciosCuentas");
        cuentasbean = (ServiciosCuentas)cuentashome.create();


        CtasCtesEmpresas[] ctasCtesEmp = cuentasbean.getCtasCtesEmpresas(rutEmpresa, idConvenio);
        if (getLogger().isDebugEnabled()) {getLogger().debug("ctasCtesEmp : " + ctasCtesEmp.length);}

        if (ctasCtesEmp.length == 0)
            throw new SeguridadException("ESPECIAL", "Esta Empresa no posee Cuenta Corriente o Cuenta Prima.");

        int cantServVigBL 		   = segBean.cantidadServiciosVigentesBL(userIdrut, idConvenio, rutEmpresa);
        if (getLogger().isDebugEnabled()) {getLogger().debug("cantServVigBL      : " + cantServVigBL);}

        String estadoVigProd01     = segBean.estadoVigenciaProd01(idConvenio);
        if (getLogger().isDebugEnabled()) {getLogger().debug("estadoVigProd01    : " + estadoVigProd01);}

        int cantServDistProd01		= segBean.cantidadServiciosDistintosProd01(userIdrut, idConvenio, rutEmpresa);
        if (getLogger().isDebugEnabled()) {getLogger().debug("cantServDistProd01 : " + cantServDistProd01);}

        int cantServVig             = segBean.cantidadServiciosVigentes(userIdrut, idConvenio, rutEmpresa);
        if (getLogger().isDebugEnabled()) {getLogger().debug("cantServVig        : " + cantServVig);}

        String[] perfUsr    = segBean.perfilesUsuario(userIdrut, idConvenio, rutEmpresa);
        if (getLogger().isDebugEnabled()) {getLogger().debug("perfUsr            : " + perfUsr.length);}

        CodigoValorBEL[] servAsoc   = segBean.serviciosAsociados(userIdrut, idConvenio, rutEmpresa);
        if (getLogger().isDebugEnabled()) {getLogger().debug("servAsoc           : " + servAsoc.length);}

        String[] listaProductos     = segBean.listaProductos(userIdrut, idConvenio, rutEmpresa);
        if (getLogger().isDebugEnabled()) {getLogger().debug("listaProductos     : " + listaProductos.length);}

        session.setAttrib("ctasCtesEmpresas", ctasCtesEmp);

        return session;
    }


    /**
     * M�todo que realiza la journalizaci�n.
     * <p>
     * Nota: M�todo realizado por modificaciones para proyecto mejoras
     * de seguridad fase 3. Journalizaci�n de eventos de configuraci�n token.
     * <p>
     * Registro de versiones:<ul>
     *
     * <li>1.0 (06/12/2004, Victor Urra (Novared)): Versi�n inicial.
     * <li>1.1 (05/07/2011, Angelo Vel�squez V (SEnTRA)): Se reemplaza LogFile por Log4j.
     * <li> 1.2 02/11/2015, Diego Urrutia Guerra(Imagemaker) - Mario Lorca (BCI): Se normaliza Logs.
     * <li>1.3 02/08/2017  Marcelo Fuentes (SEnTRA) - Ricardo Carrasco (Ing. Soft. BCI): Se sobrecarga metodo moviendo
     * toda la logica al metodo nuevo.</li>
     *
     * </ul>
     * <p>
     *
     * @param session La sessi�n del cliente
     * @param codEvento C�digo del evento de negocio
     * @param subCodNeg Sub c�digo del evento de negocio
     * @param clavePrincipal Clave principal
     * @param ipProducto C�digo del producto
     * @param codigo C�digo o mensaje de la excepci�n
     * @since 1.2
     */
    private void journalizacion(SessionBCI session, String codEvento, String subCodNeg, String clavePrincipal, String ipProducto, String codigo) {
        journalizacion(session, codEvento, subCodNeg, clavePrincipal, ipProducto, codigo, null, null);
    }
    
    /**
     * M�todo que realiza la journalizaci�n.
     * <p>
     *
     * Registro de versiones:<ul>
     * <li> 1.0 02/08/2017,  Marcelo Fuentes (SEnTRA) - Ricardo Carrasco (Ing. Soft. BCI): Versi�n Inicial.</li>
     * </ul>
     * <p>
     * @param session La sessi�n del cliente
     * @param codEvento C�digo del evento de negocio
     * @param subCodNeg Sub c�digo del evento de negocio
     * @param clavePrincipal Clave principal
     * @param ipProducto C�digo del producto
     * @param codigo C�digo o mensaje de la excepci�n
     * @param campoVariable un campo variable.
     * @param estadoEventoNegocio el estado de la operacion a journalizar.
     * 
     * @since 5.1
     */
    private void journalizacion(SessionBCI session, String codEvento, String subCodNeg, String clavePrincipal, String ipProducto, 
            String codigo, String campoVariable, String estadoEventoNegocio) {
        
        Journal journal = null;
        try {
            journal = new Journal(session);
            journal.getNroOperacionEvento();
            journal.setCodEventoNegocio(codEvento);
            journal.setSubCodEventoNegocio(subCodNeg);
            journal.setClavePrincipal(clavePrincipal);
            journal.setIdProducto(ipProducto);
            
            if(campoVariable != null) {
                journal.setCampoVariable(campoVariable);
            }
            
            if(estadoEventoNegocio != null) {
                journal.setEstadoEventoNegocio(estadoEventoNegocio);
            }
            
            if(codigo!=null) {
                journal.setAtributo("codigo", codigo);
            }
            
            journal.journalizarEvento();
            
            getLogger().debug("Journalizacion OK");
        }
        catch(Exception e) {
            if (getLogger().isEnabledFor(Level.ERROR)) {
                getLogger().error("No Jounralizo ["+e.getMessage()+"]");
            }
        }

    }
    
    /**
     * M�todo PerfilUsuario
     * 
     * Registro de versiones:<ul>
     *
     * <li>1.0 ??/??/????, Desconocido: Versi�n inicial.
     * <li> 1.1 02/11/2015, Diego Urrutia Guerra(Imagemaker) - Mario Lorca (BCI): Se normaliza Logs.
     * </ul>
     * <p>
     *
     * @param req request
     * @param rutUsuario Rut usuario
     * @param idConvenio id convenio
     * @param rutEmp Rut empresa
     * @throws Exception Retorna una excepci�n si ocurre algun problema.
     * @since 1.0
     */
    
    private void PerfilUsuario(HttpServletRequest req, String rutUsuario, String idConvenio, String rutEmp) throws Exception{

        getLogger().debug( "entrando a PerfilUsuario");

        HttpSession ses    = req.getSession(false);
        getLogger().debug("0");
        getLogger().debug("A");


        BELPerfilUsu pd    = (BELPerfilUsu)ses.getAttribute("PerfilUsuario");
        getLogger().debug("1");
        String fecha       = new SimpleDateFormat("yyyyMMdd").format(new Date());
        getLogger().debug("2");

        ic = JNDIConfig.getInitialContext();
        getLogger().debug("3");
        bciexpresshome    = (BCIExpressHome)ic.lookup("wcorp.serv.bciexpress.BCIExpress");
        getLogger().debug("4");
        bciexpress    = (BCIExpress)bciexpresshome.create();
        getLogger().debug("5");

        BELPerfilUsu tipoConv = bciexpress.consultaBELPerfil(rutUsuario, idConvenio, rutEmp).getBELPerfilUsu()[0];
        getLogger().debug( "6");
        if (getLogger().isDebugEnabled()) {
            getLogger().debug("Tipo Usuario : " + tipoConv.getTipoUsuario());
            getLogger().debug("Perfil Usuario : " + tipoConv.getPerfil());
        }

        String autorizaImpFa = null;

        ses.setAttribute("perfil", tipoConv.getPerfil());
        ses.setAttribute("tipoUsuario", tipoConv.getTipoUsuario());

    }

    /**
     * <p>
     *   M�todo encargado de journalizar el evento de ingreso (OK) de usuarios (banca empresas) 
     *   al sitio bci.cl/empresas. <br><br>
     *   
     *   Este m�todo se realiza debido al requerimiento <b>SBIF</b> de registro de 
     *   estad�sticas de ingreso de clientes a sus respectivos sitios bancarios.
     *   <br><br>
     *   La journalizaci�n se realiza tanto en las bases de datos journal 
     *   <code>(journal..jen)</code> y banele <code>(banele..LOG)</code> 
     * </p> 
     *
     * <b>Registro de versiones:</b>
     * <ul>
     *    <li> 1.0 22/02/2012, Jaime Suazo D�az (Imagemaker IT): versi�n inicial</li>
     * </ul>
     *
     * 
     * @param session objeto de tipo SessionBCI
     * @param codigoEvento C�digo de evento de negocio.
     * @param codigoSubEvento C�digo sub evento de negocio.
     * @param glosaLog Glosa de loggeo de evento.
     * @return N�mero de evento Journal.
     * @since 4.8
     */
    private String journalizacionIngresoSitioEmpresas(SessionBCI session, 
            String codigoEvento, 
            String codigoSubEvento, 
            String glosaLog) {
        
        try {
            Journal journalEmpresa = new Journal(session);
            Eventos evento = new Eventos(session);
            
            evento.setRutOperadorCliente(String.valueOf(session.getCliente().getRut()));
            evento.setDvOperadorCliente(String.valueOf(session.getCliente().getDigito()));
            evento.setCodEventoNegocio(codigoEvento);
            evento.setEstadoEventoNegocio("P");
            evento.setSubCodEventoNegocio(codigoSubEvento);
            evento.setCodsubtipoEventoNegocio(codigoEvento + " " + codigoSubEvento);
            evento.setFueraLinea("N");
            evento.setModo("N");       
            evento.setIdProducto("INT");
            evento.setAtributo("convenio", " ");
            evento.setAtributo("codProducto", " ");
            evento.setAtributo("codServicio", " ");
            evento.setAtributo("tipoLog", "C");
            evento.setAtributo("glosaLog", glosaLog);
            
            journalEmpresa.journalizar(evento);
            String numeroEventoJournal = evento.getNroOperacionEvento();
            
            return numeroEventoJournal;
            
        } 
        catch (Exception e) {
            if (getLogger().isEnabledFor(Level.ERROR)) {
                getLogger().error("journalizacionLoginmpresas error [" 
                        + e.getMessage() + "] " 
                        + "clase : [" + e.getClass() + "] "
                        + "[" + session.getCliente().getRut() + "]", e);
            }
            return "";
        }
    }
    
    /**
     * Metodo que realiza la autenticaci�n de usuario en Active Directory.
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 29/10/2015 Diego Urrutia Guerra(Imagemaker) - Mario Lorca(BCI): Versi�n Inicial.</li>
     * </ul>
     * </p>
     * 
     * @param nombreUsuario Id de Usuario
     * @param clave clave de Usuario.
     * @param canal canal de usuario.
     * @param session session.
     * @throws Exception Retorna una excepci�n si ocurre algun problema con la comunicaci�n a Active Directory.
     * @since 1.3
     */
    private void autenticarActiveDirectory(SessionBCI session, String nombreUsuario, String clave, String canal) 
            throws Exception{

        if(getLogger().isInfoEnabled()){getLogger().info("[autenticarActiveDirectory]["+nombreUsuario+"][BCI_INI]");}

        AutenticaActiveDirectory autenticaActiveDirectory = new AutenticaActiveDirectory();
        ResultadoAutenticacionActiveDirectoryTO resultadoActiveDirectoryTO = new ResultadoAutenticacionActiveDirectoryTO();
        autenticaActiveDirectory.autentica(nombreUsuario, clave, canal);
        resultadoActiveDirectoryTO = autenticaActiveDirectory.getResultado();        
        if (!resultadoActiveDirectoryTO.isAutenticado()) {
            if (resultadoActiveDirectoryTO.isUsuarioBloqueado()) {
                if(getLogger().isInfoEnabled()){getLogger().info("[autenticarActiveDirectory]["+nombreUsuario+"][BCI_FINOK][ERROR][Usuario Bloqueado]");}
                throw new SeguridadException("0432");
            }
            else if (resultadoActiveDirectoryTO.isUsuarioNoExiste()) {
                if(getLogger().isInfoEnabled()){getLogger().info("[autenticarActiveDirectory]["+nombreUsuario+"][BCI_FINOK][ERROR][Usuario invalido]");}
                throw new SeguridadException("0063");
            }
            else {
                if(getLogger().isInfoEnabled()){getLogger().info("[autenticarActiveDirectory]["+nombreUsuario+"][BCI_FINOK][ERROR][Acceso no permitido] Usuario/clave incorrecta");}
                throw new GeneralException("0434");
            }
        }
        session.tipoUser = 'E';
        if(getLogger().isInfoEnabled()){getLogger().info("[autenticarActiveDirectory]["+nombreUsuario+"][BCI_FINOK] resultadoActiveDirectoryTO ["+resultadoActiveDirectoryTO+"]");}
    }
    
    /**
     * <p>
     * M�todo que permite obtener una instancia del logger para la clase.
     * 
     * </p>
     * Registro de Versiones:
     * <ul>
     * <li>1.0 29/10/2015 Diego Urrutia Guerra(Imagemaker) - Mario Lorca (BCI) versi�n inicial</li>
     * </ul>
     * @return instancia de objeto getLogger().
     * @since 1.3
     */ 
    public Logger getLogger() {
        if (logger == null) {
            logger = Logger.getLogger(this.getClass());
        }
        return logger;
    }
    
    /**
     * <p>
     * M�todo que genera la llave QR y redirecciona a la pagina para presentacion al usuario.
     * </p>
     * 
     * Registro de versiones:
     * <ul>
     * <li>1.0 02/08/2017, Marcelo Fuentes (SEnTRA) - Ricardo Carrasco (Ing. Soft. BCI): Versi�n Inicial.</li>
     * </ul>
     * <p>
     * 
     * @param req la solicitud HTTP que se esta procesando.
     * @param res la respuesta entregada.
     * @param canalId el Id del canal.
     * @return con el resultado de la generacion de la llave.
     * @since 5.1
     */
    private boolean generarLlaveQR(HttpServletRequest req, HttpServletResponse res, String canalId) {
        
        getLogger().info("[generarLlaveQR][BCI_INI] iniciado.");
        
        HttpSession hs = req.getSession(false);
        SessionBCI session = (SessionBCI)hs.getAttribute("sessionBci");
        
        try {
            String usuarioId = "";
            if (session.getCanal().getCanalID().equals(String.valueOf(PINID_EMPRESAS_PYME))) {
                usuarioId = session.getRutUsuario() + String.valueOf(session.getDvUsuario().charAt(0));
            }
            else {
                usuarioId = String.valueOf(session.getCliente().getRut()) + String.valueOf(session.getCliente().getDigito());
            }
            UsuarioSegundaClaveTO usuarioSegundaClave = new UsuarioSegundaClaveTO(usuarioId, 
                                                                                  ControllerBCI.obtieneDominio(session.getCanal().getCanalID()), 
                                                                                  session.getCanal().getCanalID());
            
            CamposDeLlaveTO campoDeLlaves = obtenerCamposDellaves(hs);
            LlaveSegundaClaveTO llavesQREntrust = ControllerBCI.generarLlave(ENTRUST_SOFTTOKEN, SERVICIO_QR, usuarioSegundaClave, campoDeLlaves, OFFLINE);
            
            logger.debug("[generarLlaveQR] cantidad de llaves: " + (llavesQREntrust.getLlaves() != null ? llavesQREntrust.getLlaves().length : 0));
            
            req.setAttribute("servicioQR", "PagueDirecto"); 
            hs.setAttribute("imagenesqr", llavesQREntrust.getLlaves());
            
            logger.debug("[generarLlaveQR] [BCI_FINOK]");
            
            return true;
            
        }
        catch (Exception e) {
            if(getLogger().isEnabledFor(Level.ERROR)){
                getLogger().error("[generarLlaveQR][BCI_FINEX] Exception ", e);
            }
            return false;
        }
        
    }
    
    /**
     * <p>
     * M�todo que se encarga de validar la clave ingresada por el usuario contra el servicio de Entrust.
     * </p>
     * 
     * Registro de versiones:
     * <ul>
     * <li>1.0 02/08/2017, Marcelo Fuentes (SEnTRA) - Ricardo Carrasco (Ing. Soft. BCI): Versi�n Inicial.</li>
     * </ul>
     * <p>
     * 
     * @param session la sesion HTTP.
     * @param request la solicitud HTTP que se esta procesando.
     * @param response la respuesta entregada.
     * @param modoAutenticacion El modo de autenticacion: ONLINE u OFFLINE.
     * @param codeConfirmation La clave a validar.
     * @return Env�a los datos a la p�gina de respuesta.
     * @since 5.1
     */
    private boolean validaEntrustSoftToken(HttpSession session, HttpServletRequest request, 
            HttpServletResponse response, String modoAutenticacion, String codeConfirmation) {
        
        if (getLogger().isEnabledFor(Level.INFO)){
            getLogger().info("[validaEntrustSoftToken][BCI_INI] iniciado.");
        }
        HttpSession hs = request.getSession(false);
        SessionBCI sessionBci = (SessionBCI)hs.getAttribute("sessionBci");
        
        try {
            String usuarioId = "";
            if (sessionBci.getCanal().getCanalID().equals(String.valueOf(PINID_EMPRESAS_PYME))) {
                usuarioId = sessionBci.getRutUsuario() + String.valueOf(sessionBci.getDvUsuario().charAt(0));
            }
            else {
                usuarioId = String.valueOf(sessionBci.getCliente().getRut()) + String.valueOf(sessionBci.getCliente().getDigito());
            }
            UsuarioSegundaClaveTO usuarioSegundaClave = new UsuarioSegundaClaveTO(usuarioId, 
                                                                                  ControllerBCI.obtieneDominio(sessionBci.getCanal().getCanalID()), 
                                                                                  sessionBci.getCanal().getCanalID());
            
            logger.debug("[validaEntrustSoftToken] Se agrega parametro para EntrustSoftToken");
            
            CamposDeLlaveTO campoDeLlaves = (CamposDeLlaveTO)sessionBci.getAttrib("camposDeLLavePD");
            sessionBci.removeAttrib("camposDeLLavePD");
            
            boolean resultado = ControllerBCI.verificarAutenticacion(ENTRUST_SOFTTOKEN, modoAutenticacion, 
                    usuarioSegundaClave, campoDeLlaves, SERVICIO_QR, codeConfirmation);
            
            if(getLogger().isDebugEnabled()){
                getLogger().debug("[validaEntrustSoftToken][BCI_FINOK] resultado: " + resultado);
            }
            if(!resultado) {
                sessionBci.cerrar();
                return false;
            }
            return true;
        } 
        catch (SeguridadException e) {
            if(getLogger().isEnabledFor(Level.ERROR)){
                getLogger().error("[validaEntrustSoftToken][BCI_FINEX] SeguridadException", e);
            }
            sessionBci.cerrar();
            return false;
            
        }
        catch (Exception e) {
            if(getLogger().isEnabledFor(Level.ERROR)){
                getLogger().error("[validaEntrustSoftToken][BCI_FINEX] SeguridadException", e);
            }
            sessionBci.cerrar();
            return false;
            
        }
    }
    
    /**
     * <p>
     * M�todo que obtiene los datos para las transacciones bancarias que operan con segunda clave.
     * </p>
     * 
     * Registro de versiones:
     * <ul>
     * <li>1.0 02/08/2017, Marcelo Fuentes (SEnTRA) - Ricardo Carrasco (Ing. Soft. BCI): Versi�n Inicial.</li>
     * </ul>
     * <p>
     * 
     * @param hs la sesion HTTP.
     * @return Objeto con los datos para las transacciones bancarias que operan con segunda clave.
     * @throws PagueDirectoException en caso de error.
     * @since 5.1
     */
    private CamposDeLlaveTO obtenerCamposDellaves(HttpSession hs) throws PagueDirectoException {
        
        SessionBCI session = (SessionBCI) hs.getAttribute("sessionBci");
        SimpleDateFormat formateador = new SimpleDateFormat("yyyyMMddHHmm", new Locale("es","CL"));
        
        Date fechaDate = new Date();
        String fecha = formateador.format(fechaDate);
        String nombresDeLLave = TablaValores.getValor(TABLA_SEGURIDAD, "Llave_" + SERVICIO_QR, "datosLlave");
        
        if(getLogger().isInfoEnabled()){getLogger().info("[service] nombresDeLLave : " + nombresDeLLave);}
        PagueDirecto pd = (PagueDirecto)hs.getAttribute("PagueDirecto");
        
        String montoCompra = String.valueOf(pd.getMontoCompra()); 
        String[] valoresDeLlave = { session.getCanal().getNombre(), fecha, montoCompra, SERVICIO_QR };
        
        CamposDeLlaveTO campoDeLlaves = new CamposDeLlaveTO();
        SafeSignerUtil safeSignerUtil = new SafeSignerUtil();
        
        campoDeLlaves = safeSignerUtil.seteaCamposDeLlave(nombresDeLLave, valoresDeLlave);
        return campoDeLlaves;
    }
    
}