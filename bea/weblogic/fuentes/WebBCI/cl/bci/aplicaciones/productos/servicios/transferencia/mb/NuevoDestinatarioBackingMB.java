package cl.bci.aplicaciones.productos.servicios.transferencia.mb;

import java.io.Serializable;
import java.util.Calendar;
import java.util.HashMap;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import wcorp.bprocess.cuentas.vo.AliasTTFFDVO;
import wcorp.gestores.validador.utils.ValidaCuentasUtility;
import wcorp.serv.seguridad.SeguridadException;
import wcorp.util.ErroresUtil;
import wcorp.util.RUTUtil;
import wcorp.util.StringUtil;
import wcorp.util.TablaValores;

import cl.bci.aplicaciones.productos.servicios.transferencia.to.AliasTTFFTO;
import cl.bci.aplicaciones.utilitarios.transferencias.mb.PagosYTransferenciasUtilityMB;
import cl.bci.infraestructura.web.seguridad.segundaclave.SegundaClaveUIInput;
import cl.bci.infraestructura.web.seguridad.segundaclave.SegundaClaveUtil;
import cl.bci.infraestructura.web.seguridad.segundaclave.mb.JerarquiaClavesModelMB;

/**
 * Backing ManagedBean que proporciona los atributos y acciones para la Vista de
 * nuevo destinatario de transferencia.
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 (17/07/2013 Guillermo Marambio. (Sermaluc)): versi�n inicial.</li>
 * <li>1.1 (18/10/2013 Guillermo Marambio. (Sermaluc)): Actualizaci�n de formato
 * de rut del destinatario y bandera de correo electronico.</li>
 * <li>1.2 (21/10/2013 Guillermo Marambio. (Sermaluc)): Actualizaci�n de
 * validaci�n al eliminar un destinatario dentro de 24 horas.</li>
 * <li>1.3 19/11/2013 Guillermo Marambio (Sermaluc): Se actualiza autorizacion
 * para fecha de ingreso.</li>
 * <li>1.4 21/01/2014 Guillermo Marambio (Sermaluc): Se actualiza metodo
 * getTiposDeCuentaTransferencia().</li>
 * <li>1.5 18/02/2013 Guillermo Marambio (Sermaluc): Se actualizan logs del
 * fuente.</li>
 * <li>1.6 10/07/2015 Nelson Alvarado(SEnTRA)-Claudia Lopez(Ing. Soft. BCI)Se agrega el siguiente metodo {@link #limpiarVariablesVista()}
 * <li>1.7 09/08/2016 Claudio Marambio C. (Imagemaker) - Pamela Inostroza(Ing. Soft. BCI): Se agrega validacion a codigo de verificacion 
 * al autorizar alias de transferencia</li>
 * <li>1.8 21/04/2017 Jaime Collao(Imagemaker)-Pamela Inostroza(Ing. Soft. BCI) Se crea el siguiente m�todo
 * {@link #obtieneMontoMaximoAlias()} para obtener el tipo de banca y monto maximos de alias.
 * Se agrega al metodo {@link #isAutorizaConSafeSigner()} el llamado al metodo obtieneMontoMaximoAlias para obtener el tipo de banca y 
 * monto maximos de alias.
 * Se agregan las variables {@link #montoMaximoAlias} y {@link #maximo24HorasAlias}, para manejar el valor monto maximo y su valor sin formato. </li> 
 * <li>1.9 26/04/2017, Mauricio Am�stica G. (Kubos) - Marcelo Arias (ing. Soft. BCI): Se modifica el m�todo 
 * {@link #crearNuevoDestinatario()} para agregar validaci�n si la cuenta de destino es Mach.</li> 
 * <li>2.0 05/09/2017 Luis Lopez Alamos (SEnTRA) - Ricardo Carrasco C�ceres (Ing. Soft. BCI): Se agregan constantes: {@link #CHAR_VACIO} y {@link #TIENE_ENTRUST_STK},
 * 	Se agrega atributo {@link #autorizaConEntrust} con sus respectivos conmuntadores, Se modifican m�todos {@link #crearNuevoDestinatario()}, {@link #autorizarDestinatario()}}.
 *  Se crea m�todo {@link #validaTamanioConsecutivo()}</li> 
* </ul>
 * </p>
 * <p>
 * <b>Todos los derechos reservados por Banco de Cr�dito e Inversiones.</b>
 * </p>
 */

@ManagedBean
@RequestScoped
public class NuevoDestinatarioBackingMB implements Serializable {

    /**
     * Serial de la clase.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Tabla que contiene configuraci�n de transferencias a terceros.
     */
    private static final String TTFF_CONFIG = "TransferenciaTerceros.parametros";

    /**
     * C�digo banco BCI.
     */
    private static final int CODIGOBCI = Integer.parseInt(TablaValores.getValor(TTFF_CONFIG,
        "codigoBCI", "valor"));

    /**
     * Bandera para envio de correo.
     */
    private static final boolean BANDERA_ENVIOCORREO = Boolean.valueOf(TablaValores.getValor(
        TTFF_CONFIG, "banderaMisDestinatarios", "envioCorreo"));

    /**
     * Cadena vacio.
     */
    private static final String VACIO = "";

    /**
     * Primitivo char vac�o.
     */
    private static final char CHAR_VACIO = '\u0000';
    
    /**
     * Determina la cantidad m�zima de caracteres consecutivos que se pueden agregar para nombre y alias.
     */
    private static final int MAXIMO_CARACTERES_CONSECUTIVOS = 30;
    
    /**
     * Constante que representa existencia de autenticaci�n de Tipo De SegundaClave Entrust SoftToken.
     */
    private static final char TIENE_ENTRUST_STK = 'S';

    /**
     * Log de la clase.
     */
    private transient Logger log = (Logger) Logger.getLogger(NuevoDestinatarioBackingMB.class);

    /**
     * Atributo que representa el rut del destinatario.
     */
    private String rut = VACIO;

    /**
     * Atributo que representa el nombre del destinatario.
     */
    private String nombre = VACIO;

    /**
     * Atributo que representa el email del destinatario.
     */
    private String email;

    /**
     * Atributo que representa el c�digo de alias del destinatario.
     */
    private long codigoAlias;

    /**
     * Atributo que representa el alias del destinatario.
     */
    private String alias;

    /**
     * Atributo que representa el banco del destinatario.
     */
    private int banco;

    /**
     * Atributo que representa el nombre del banco del destinatario.
     */
    private String nombreBanco;

    /**
     * Atributo que representa la cuenta del destinatario.
     */
    private String cuenta = VACIO;

    /**
     * Atributo que representa el tipo de cuenta del destinatario.
     */
    private String tipoCuenta;

    /**
     * Atributo que representa el nombre del tipo de cuenta del destinatario.
     */
    private String nombreTipoCuenta;

    /**
     * Atributo que representa la bandera de favorito del destinatario.
     */
    private boolean favorito;

    /**
     * Atributo que representa el c�digo de verficaci�n del destinatario.
     */
    private String codigoVerificacion;
    
    /**
     * Atributo utilizado para conteer el monto maximo de transferencias de fondos.
     */
    private String montoMaximoAlias;
    
    /**
     * monto maximo permitido para una transferencia a otro banco.
     */
    private Double maximo24HorasAlias;
    
    /**
     * Atributo que representa la bandera de safe signer.
     */
    private boolean autorizaConSafeSigner;

    /**
     * Atributo que representa la bandera de Entrust.
     */
    private char autorizaConEntrust;

    /**
     * Referencia a la componente que maneja la segunda Clave.
     */
    private SegundaClaveUIInput segundaClave;

    /**
     * Inyecci�n ManagedBean de misDestinatariosView.
     */
    @ManagedProperty(value = "#{misDestinatariosViewMB}")
    private MisDestinatariosViewMB misDestinatariosViewMB;

    /**
     * Inyecci�n ManagedBean de misDestinatariosTTFFUtility.
     */
    @ManagedProperty(value = "#{misDestinatariosTTFFUtilityMB}")
    private MisDestinatariosTTFFUtilityMB misDestinatariosTTFFUtilityMB;

    /**
     * Propiedad inyectada desde el MB pagos y transferencias utility.
     */
    @ManagedProperty(value = "#{pagosYTransferenciasUtilityMB}")
    private PagosYTransferenciasUtilityMB pagosYTransferenciasUtilityMB;

    /**
     * Propiedad inyectada desde el MB jerarquia claves model.
     */
    @ManagedProperty(value = "#{jerarquiaClavesModelMB}")
    private JerarquiaClavesModelMB jerarquiaClavesModelMB;

    /**
     * Constructor de la clase.
     */
    public NuevoDestinatarioBackingMB() {
        if (getLog().isDebugEnabled()) {
            getLog().debug("[NuevoDestinatarioBackingMB] Inicia Constructor");
        }
    }

    /**
     * <p>
     * Limpia variables para la ejecucion del panel crear mis destinatarios
     * </p>
     * 
     * Registro de versiones:
     * <ul>
     * <li>1.010/07/2015 Nelson Alvarado(SEnTRA)-Claudia Lopez(Ing. Soft. BCI): versi�n inicial.</li>
     * </ul>
     * 
     * @since 1.0
     */
    public void limpiarVariablesVista(){
        if(getLog().isEnabledFor(Level.INFO)){
            getLog().info("[limpiarVariablesVista][BCI_INI]");
        }
        misDestinatariosViewMB.setRenderedMisDestinatarios(true);
        misDestinatariosViewMB.setOperacion(null);
        
        if(getLog().isEnabledFor(Level.INFO)){
            getLog().info("[limpiarVariablesVista][BCI_FIN]");
        }
    }
    /**
     * Constructor del log.
     * 
     * @return Logger instancia de logger.
     */
    public Logger getLog() {
        if (log == null) {
            log = Logger.getLogger(this.getClass());
        }
        return log;
    }

    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public long getCodigoAlias() {
        return codigoAlias;
    }

    public void setCodigoAlias(long codigoAlias) {
        this.codigoAlias = codigoAlias;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public int getBanco() {
        return banco;
    }

    public void setBanco(int banco) {
        this.banco = banco;
    }

    public String getNombreBanco() {
        return nombreBanco;
    }

    public void setNombreBanco(String nombreBanco) {
        this.nombreBanco = nombreBanco;
    }

    public String getCuenta() {
        return cuenta;
    }

    public void setCuenta(String cuenta) {
        this.cuenta = cuenta;
    }

    public String getTipoCuenta() {
        return tipoCuenta;
    }

    public void setTipoCuenta(String tipoCuenta) {
        this.tipoCuenta = tipoCuenta;
    }

    public String getNombreTipoCuenta() {
        return nombreTipoCuenta;
    }

    public void setNombreTipoCuenta(String nombreTipoCuenta) {
        this.nombreTipoCuenta = nombreTipoCuenta;
    }

    public boolean isFavorito() {
        return favorito;
    }

    public void setFavorito(boolean favorito) {
        this.favorito = favorito;
    }

    public String getCodigoVerificacion() {
        return codigoVerificacion;
    }

    public void setCodigoVerificacion(String codigoVerificacion) {
        this.codigoVerificacion = codigoVerificacion;
    }


	/**
     * <p>Metodo isAutorizaConSafeSigner</p>
     * 
     * Registro de versiones:
     * <ul>
     * <li>1.0 ??/??/???? ???? (????) - ???? (????): Version Inicial.</li>
     * <li>1.1 21/04/2017 Jaime Collao R. (ImageMaker) - Pamela Inostroza (Ing. Soft. BCI): Se agrega el metodo obtieneMontoMaximoAlias para 
     * obtener el monto maximo de la primera transferencia.</li>
     * </ul>
     * <p>
     * 
     * @return boolean Atributo que representa la bandera de safe signer.
     * @since 1.0
     */
    public boolean isAutorizaConSafeSigner() {
        try {
            autorizaConSafeSigner = jerarquiaClavesModelMB.tieneSafeSigner(
                "GestorAliasTransferencias", SegundaClaveUtil.getUsuarioSegundaClave());
            this.obtieneMontoMaximoAlias();
            if (getLog().isDebugEnabled()) {
                getLog().debug(
                    "[isAutorizaConSafeSigner]["
                        + misDestinatariosTTFFUtilityMB.getClienteMB().getRut()
                        + "] autorizaConSafeSigner: " + autorizaConSafeSigner);
            }
        }
        catch (Exception e) {
            if (getLog().isEnabledFor(Level.ERROR)) {
                getLog().error(
                    "[isAutorizaConSafeSigner]["
                        + misDestinatariosTTFFUtilityMB.getClienteMB().getRut()
                        + "] error al validar tieneSafeSigner:" + ErroresUtil.extraeStackTrace(e));
            }
        }
        return autorizaConSafeSigner;
    }

    public void setAutorizaConSafeSigner(boolean autorizaConSafeSigner) {
        this.autorizaConSafeSigner = autorizaConSafeSigner;
    }

    public SegundaClaveUIInput getSegundaClave() {
        return segundaClave;
    }

    public void setSegundaClave(SegundaClaveUIInput segundaClave) {
        this.segundaClave = segundaClave;
    }

    public MisDestinatariosViewMB getMisDestinatariosViewMB() {
        return misDestinatariosViewMB;
    }

    public void setMisDestinatariosViewMB(MisDestinatariosViewMB misDestinatariosViewMB) {
        this.misDestinatariosViewMB = misDestinatariosViewMB;
    }

    public MisDestinatariosTTFFUtilityMB getMisDestinatariosTTFFUtilityMB() {
        return misDestinatariosTTFFUtilityMB;
    }

    public void setMisDestinatariosTTFFUtilityMB(
        MisDestinatariosTTFFUtilityMB misDestinatariosTTFFUtilityMB) {
        this.misDestinatariosTTFFUtilityMB = misDestinatariosTTFFUtilityMB;
    }

    public PagosYTransferenciasUtilityMB getPagosYTransferenciasUtilityMB() {
        return pagosYTransferenciasUtilityMB;
    }

    public void setPagosYTransferenciasUtilityMB(
        PagosYTransferenciasUtilityMB pagosYTransferenciasUtilityMB) {
        this.pagosYTransferenciasUtilityMB = pagosYTransferenciasUtilityMB;
    }

    public JerarquiaClavesModelMB getJerarquiaClavesModelMB() {
        return jerarquiaClavesModelMB;
    }

    public void setJerarquiaClavesModelMB(JerarquiaClavesModelMB jerarquiaClavesModelMB) {
        this.jerarquiaClavesModelMB = jerarquiaClavesModelMB;
    }

    public String getMontoMaximoAlias() {
		return montoMaximoAlias;
	}

	public void setMontoMaximoAlias(String montoMaximoAlias) {
		this.montoMaximoAlias = montoMaximoAlias;
	}

	/**
     * <p>
     * Obtiene los bancos autorizados para realizar transferencias.
     * </p>
     * 
     * Registro de versiones:
     * <ul>
     * <li>1.0 17/07/2013 Guillermo Marambio (Sermaluc): versi�n inicial.</li>
     * <li>1.1 23/10/2013 Guillermo Marambio (Sermaluc): Se actualiza lista para
     * dejar banco BCI primero.</li>
     * </ul>
     * 
     * @return arreglo de SelectItem para despliegue jsf.
     * @since 1.0
     */
    public SelectItem[] getBancosAutorizados() {
        if (getLog().isDebugEnabled()) {
            getLog().debug(
                "[getBancosAutorizados][" + misDestinatariosTTFFUtilityMB.getClienteMB().getRut()
                    + "] Inicio getBancosAutorizados");
        }
        SelectItem[] bancos = misDestinatariosViewMB.getBancosAutorizados();
        if (bancos == null) {
            try {
                getLog().debug(
                    "[getBancosAutorizados]["
                        + misDestinatariosTTFFUtilityMB.getClienteMB().getRut()
                        + "] Inicio getBancosAutorizadosTransferencia");
                bancos = pagosYTransferenciasUtilityMB.getBancosAutorizadosTransferencia();
                SelectItem[] bancosAutorizados= new SelectItem[bancos.length];
                for (int i = 0; i < bancos.length; i++) {
                    if (CODIGOBCI == Integer.parseInt(bancos[i].getValue().toString())) {
                        bancosAutorizados[0] = bancos[i];
                        break;
                    }
                }
                for (int i = 0, j = 1; i < bancos.length; i++) {
                    if (CODIGOBCI != Integer.parseInt(bancos[i].getValue().toString())) {
                        bancosAutorizados[j] = bancos[i];
                        j++;
                    }
                }
                misDestinatariosViewMB.setBancosAutorizados(bancosAutorizados);
            }
            catch (Exception e) {
                if (getLog().isEnabledFor(Level.ERROR)) {
                    getLog().error(
                        "[getBancosAutorizados]["
                            + misDestinatariosTTFFUtilityMB.getClienteMB().getRut()
                            + "] excepcion: " + ErroresUtil.extraeStackTrace(e));
                }
            }
        }
        if (getLog().isDebugEnabled()) {
            getLog().debug(
                "[getBancosAutorizados][" + misDestinatariosTTFFUtilityMB.getClienteMB().getRut()
                    + "] Fin getBancosAutorizados");
        }
        return misDestinatariosViewMB.getBancosAutorizados();
    }

    /**
     * <p>
     * Obtiene los tipos de cuentas autorizadas para realizar transferencias.
     * </p>
     * 
     * Registro de versiones:
     * <ul>
     * <li>1.0 17/07/2013 Guillermo Marambio (Sermaluc): versi�n inicial.</li>
     * <li>1.1 21/01/2014 Guillermo Marambio (Sermaluc): Se actualiza llamado a
     * getTiposDeCuentaTransferencia con parametro en true.</li>
     * </ul>
     * 
     * @return arreglo de SelectItem para despliegue jsf.
     * @since 1.0
     */
    public SelectItem[] getTiposDeCuentaTransferencia() {
        if (getLog().isDebugEnabled()) {
            getLog().debug(
                "[getTiposDeCuentaTransferencia]["
                    + misDestinatariosTTFFUtilityMB.getClienteMB().getRut()
                    + "] Inicio getTiposDeCuentaTransferencia");
        }
        try {
            getLog().debug(
                "[getTiposDeCuentaTransferencia]["
                    + misDestinatariosTTFFUtilityMB.getClienteMB().getRut() + "] codigo Banco: "
                    + banco);
            if(banco!=0){
                boolean ahorro = banco == CODIGOBCI;
                SelectItem[] tiposDeCuentaTransferencia = pagosYTransferenciasUtilityMB
                    .getTiposDeCuentaTransferencia(true);
                misDestinatariosViewMB.setTiposDeCuentaTransferencia(tiposDeCuentaTransferencia);
            }
        }
        catch (Exception e) {
            if (getLog().isEnabledFor(Level.ERROR)) {
                getLog().error(
                    "[getTiposDeCuentaTransferencia]["
                        + misDestinatariosTTFFUtilityMB.getClienteMB().getRut() + "] excepcion: "
                            + ErroresUtil.extraeStackTrace(e));
            }
        }
        if (getLog().isDebugEnabled()) {
            getLog().debug(
                "[getTiposDeCuentaTransferencia]["
                    + misDestinatariosTTFFUtilityMB.getClienteMB().getRut()
                    + "] Fin getTiposDeCuentaTransferencia");
        }
        return misDestinatariosViewMB.getTiposDeCuentaTransferencia();
    }

    /**
     * <p>
     * Crear el nuevo destinatario ingresado por el cliente.
     * </p>
     * 
     * Registro de versiones:
     * <ul>
     * <li>1.0 20/07/2013 Guillermo Marambio (Sermaluc): versi�n inicial.</li>
     * <li>1.1 18/10/2013 Guillermo Marambio (Sermaluc): Se actualiza formato
     * del rut para nuevos destinatarios.</li>
     * <li>1.2 (21/10/2013 Guillermo Marambio. (Sermaluc)): Actualizaci�n de
     * validaci�n al eliminar un destinatario dentro de 24 horas.</li>
     * <li>1.3 28/02/2016 Juan Jose Buendia (BCI): Se implenta uso de validacion de cuentas de seguridad.</li> 
	 * <li>1.4 26/04/2017 Mauricio Am�stica G. (Kubos) - Marcelo Arias (ing. Soft. BCI): Se agrega validaci�n de 
	 * cuentas Mach </li>
	 * <li>1.5 05/09/2017 Luis Lopez Alamos (SEnTRA) - Ricardo Carrasco C�ceres (Ing. Soft. BCI): Se agrega l�gica propia de nueva Autenticaci�n Entrust,
	 * eliminaci�n de codigo de verificaci�n por correo para Entrust SoftToken y eliminaci�n de restricci�n de 24 horas para Entrust SoftToken.</li>
     * </ul>
     * 
     * @since 1.0
     */
    public void crearNuevoDestinatario() {
        if (getLog().isDebugEnabled()) {
            getLog().debug(
                "[crearNuevoDestinatario][" + misDestinatariosTTFFUtilityMB.getClienteMB().getRut()
                    + "][" + rut + "] Inicio");
        }
        boolean cuentaBCI = true;
        try {
            int rutAlias = (int) RUTUtil.extraeRUT(rut);
            char dv = RUTUtil.calculaDigitoVerificador(rutAlias);
            AliasTTFFDVO aliasTTFFDVO = misDestinatariosTTFFUtilityMB.validarAlias(0, banco,
                tipoCuenta, email, alias, rutAlias + "" + dv, nombre, cuenta);
			if (CODIGOBCI == banco && !misDestinatariosTTFFUtilityMB.isMach(cuenta)) {
                if (getLog().isDebugEnabled()) {
                    getLog().debug(
                        "[crearNuevoDestinatario]["
                            + misDestinatariosTTFFUtilityMB.getClienteMB().getRut() + "][" + rut
                            + "] validarCuentaBCI: " + cuenta);
                }
                
                
                cuentaBCI = ValidaCuentasUtility.validarCuentaBCI(cuenta, tipoCuenta, rutAlias, dv);
            }
            isAutorizaConSafeSigner();
            boolean enviaCorreoPorTipoAutenticacion = true;
            
            if (autorizaConSafeSigner || getAutorizaConEntrust()==TIENE_ENTRUST_STK) {
            	enviaCorreoPorTipoAutenticacion = false;
            }
            
            
            if (cuentaBCI) {
                if (getLog().isDebugEnabled()) {
                    getLog().debug(
                        "[crearNuevoDestinatario]["
                            + misDestinatariosTTFFUtilityMB.getClienteMB().getRut() + "][" + rut
                            + "] crearAliasTTFFDVO: " + aliasTTFFDVO.getAlias());
                }
                if (getLog().isDebugEnabled()) {
                    getLog().debug(
                        "[crearNuevoDestinatario]["
                            + misDestinatariosTTFFUtilityMB.getClienteMB().getRut() + "][" + rut
                            + "] envioCorreo: " + BANDERA_ENVIOCORREO + ", autorizaConSafeSigner: "
                            + autorizaConSafeSigner);
                }
               
                String puedeCrearAliasRecienEliminados = TablaValores.getValor(TTFF_CONFIG,"puedeCrearAliasRecienEliminados", "desc")==null ? "NO" : TablaValores.getValor(TTFF_CONFIG,"puedeCrearAliasRecienEliminados", "desc") ;
 
                
                if (getAutorizaConEntrust()==TIENE_ENTRUST_STK && "SI".equalsIgnoreCase(puedeCrearAliasRecienEliminados)) {
                	aliasTTFFDVO.setRestriccionRegla24Horas(false);
                	getLog().debug("[crearNuevoDestinatario] Se omite regla de restricci�n para este destinatario");
                }
                
                aliasTTFFDVO = misDestinatariosTTFFUtilityMB.crearAliasTTFFDVO(aliasTTFFDVO,
                		enviaCorreoPorTipoAutenticacion && BANDERA_ENVIOCORREO);

                codigoAlias = aliasTTFFDVO.getCodigo();
                nombreBanco = aliasTTFFDVO.getNombreBanco();
                nombreTipoCuenta = TablaValores
                    .getValor(TTFF_CONFIG, "glosaTipoCuenta", tipoCuenta);
                misDestinatariosViewMB.setOperacion("autorizarDestinatario");
                AliasTTFFTO aliasTTFFTO = new AliasTTFFTO();
                aliasTTFFTO.setCodigo(codigoAlias);
                aliasTTFFTO.setNombre(nombre);
                aliasTTFFTO.setNombreAlias(alias);
                aliasTTFFTO.setRut(rutAlias);
                aliasTTFFTO.setDv(dv);
                aliasTTFFTO.setEmail(email);
                aliasTTFFTO.setTipoCuenta(tipoCuenta);
                aliasTTFFTO.setNombreTipoCuenta(nombreTipoCuenta);
                aliasTTFFTO.setCodigoBanco(banco);
                aliasTTFFTO.setNombreBanco(nombreBanco);
                aliasTTFFTO.setCuentaDestino(cuenta);
                aliasTTFFTO.setFechaIngreso(Calendar.getInstance().getTime());
                misDestinatariosViewMB.setAliasTTFFTO(aliasTTFFTO);
            }
            else {
                misDestinatariosViewMB.setOperacion("cuentaInvalida");
            }
        }
        catch (wcorp.util.GeneralException e) {
            if (getLog().isEnabledFor(Level.ERROR)) {
                getLog().error(
                    "[crearNuevoDestinatario]["
                        + misDestinatariosTTFFUtilityMB.getClienteMB().getRut() + "][" + rut
                        + "] excepcion: " + ErroresUtil.extraeStackTrace(e));
            }
            if(e.getCodigo().equals("TRF058")){
                misDestinatariosViewMB.setOperacion("eliminado24Horas");
            }
            else if(e.getCodigo().contains("TRF")){
                misDestinatariosViewMB.setOperacion("errorDesconocidoNuevoDestinatario");
                FacesContext.getCurrentInstance().addMessage(
                    "errorNuevoDestinatario",
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, "errorNuevoDestinatario", e
                        .getInfoAdic()));
            }
            else{
            misDestinatariosViewMB.setOperacion(e.getInfoAdic());
        }
        }
        catch (Exception e) {
            if (getLog().isEnabledFor(Level.ERROR)) {
                getLog().error(
                    "[crearNuevoDestinatario]["
                        + misDestinatariosTTFFUtilityMB.getClienteMB().getRut() + "][" + rut
                        + "] excepcion: " + ErroresUtil.extraeStackTrace(e));
            }
            misDestinatariosViewMB.setOperacion("errorNuevoDestinatario");
        }
        if (getLog().isDebugEnabled()) {
            getLog().debug(
                "[crearNuevoDestinatario][" + misDestinatariosTTFFUtilityMB.getClienteMB().getRut()
                    + "][" + rut + "] Fin");
        }
    }
    /**
     * <p>
     * Autorizar el nuevo destinatario ingresado por el cliente.
     * </p>
     * 
     * Registro de versiones:
     * <ul>
     * <li>1.0 20/07/2013 Guillermo Marambio (Sermaluc): versi�n inicial.</li>
     * <li>1.1 19/11/2013 Guillermo Marambio (Sermaluc): Se actualiza
     * autorizacion para fecha de ingreso.</li>
     * <li>1.2 09/08/2016 Claudio Marambio C. (Imagemaker) - Pamela Inostroza(Ing. Soft. BCI): Se agrega validacion a codigo de verificacion 
     * al autorizar alias de transferencia</li>
     * <li>1.3 05/09/2017 Luis Lopez Alamos (SEnTRA) - Ricardo Carrasco C�ceres (Ing. Soft. BCI): Se agrega l�gica propia de nueva Autenticaci�n Entrust,
	 * Se omite la validaci�n de codigo de verificaci�n para cuando es Entrust SoftToken.</li>
     * </ul>
     * 
     * @throws Exception error al autorizar el destinatario.
     * @since 1.0
     */
    public void autorizarDestinatario() throws Exception {
        if (getLog().isDebugEnabled()) {
            getLog().debug(
                "[autorizarDestinatario][" + misDestinatariosTTFFUtilityMB.getClienteMB().getRut()
                    + "][" + codigoAlias + "] Inicio");
        }
        try {
            codigoAlias = misDestinatariosViewMB.getAliasTTFFTO().getCodigo();
            if (segundaClave.verificarAutenticacion()) {
                if (isAutorizaConSafeSigner() || getAutorizaConEntrust()==TIENE_ENTRUST_STK) {
                    try {
                        if (getLog().isDebugEnabled()) {
                            getLog().debug(
                                "[autorizarDestinatario]["
                                    + misDestinatariosTTFFUtilityMB.getClienteMB().getRut() + "]["
                                    + codigoAlias + "] autorizaConSafeSigner");
                        }
                        pagosYTransferenciasUtilityMB.obtenerCtasFrecuentes(false);
                        HashMap<Long, AliasTTFFDVO> map = pagosYTransferenciasUtilityMB
                            .getCuentasFrecuentesHashMap();
                        AliasTTFFDVO aliasTTFFDVO = map.get(codigoAlias);
                        codigoVerificacion = aliasTTFFDVO.getIdValidacion();
                    }
                    catch (Exception e) {
                        if (getLog().isEnabledFor(Level.ERROR)) {
                            getLog().error(
                                "[autorizarDestinatario]["
                                    + misDestinatariosTTFFUtilityMB.getClienteMB().getRut() + "]["
                                    + codigoAlias
                                    + "] error al obtener autorizaci�n con safeSigner, Error: "
                                    + ErroresUtil.extraeStackTrace(e));
                        }
                    }
                }
                try {
                    if (getLog().isDebugEnabled()) {
                        getLog().debug(
                            "[autorizarDestinatario]["
                                + misDestinatariosTTFFUtilityMB.getClienteMB().getRut() + "]["
                                + codigoAlias + "] autorizarAlias");
                    }
                    if(codigoVerificacion != null && !codigoVerificacion.isEmpty()){
	                    misDestinatariosTTFFUtilityMB.autorizarAlias(codigoAlias, codigoVerificacion,
	                        misDestinatariosViewMB.getAliasTTFFTO().getFechaIngreso());
	                    misDestinatariosViewMB.setOperacion("autorizadoDestinatario");
                    }
                    else{
                    	throw new Exception();
                    }
                }
                catch (Exception e) {
                    if (getLog().isEnabledFor(Level.ERROR)) {
                        getLog().error(
                            "[autorizarDestinatario]["
                                + misDestinatariosTTFFUtilityMB.getClienteMB().getRut() + "]["
                                + codigoAlias + "] error al autorizar el alias, Error: "
                                + ErroresUtil.extraeStackTrace(e));
                    }
                    misDestinatariosViewMB.setOperacion("errorAutorizarDestinatario");
                }
            }
        }
        catch (SeguridadException se) {
            if (getLog().isDebugEnabled()) {
                getLog().debug(
                    "[autorizarDestinatario]["
                        + misDestinatariosTTFFUtilityMB.getClienteMB().getRut() + "]["
                        + codigoAlias + "] Fallo verificarAutenticacion");
            }
            misDestinatariosViewMB.setOperacion("errorSafeSignerAutorizarDestinatario");
        }
        catch (Exception e) {
            if (getLog().isEnabledFor(Level.ERROR)) {
                getLog().error(
                    "[autorizarDestinatario]["
                        + misDestinatariosTTFFUtilityMB.getClienteMB().getRut() + "]["
                        + codigoAlias + "] error al autorizar el alias, Error: "
                        + ErroresUtil.extraeStackTrace(e));
            }
            misDestinatariosViewMB.setOperacion("errorAutorizarDestinatario");
        }
        if (getLog().isDebugEnabled()) {
            getLog().debug(
                "[autorizarDestinatario][" + misDestinatariosTTFFUtilityMB.getClienteMB().getRut()
                    + "][" + codigoAlias + "] Fin");
        }
    }
    
	/**
     * <p>Metodo encargado de obtener el monto m�ximo primera transferencia para un alias seg�n la banca a la cual
     *  pertenece el cliente.</p>
     * 
     * Registro de versiones:
     * <ul>
     * <li>1.0 21/04/2017 Jaime Collao R. (ImageMaker) - Pamela Inostroza (Ing. Soft. BCI): Versi�n inicial.</li>
     * </ul>
     * <p>
     * @since 1.8
     */
    private void obtieneMontoMaximoAlias(){ 
	    long rutMaxi = misDestinatariosTTFFUtilityMB.getClienteMB().getRut();
        if(getLog().isInfoEnabled()){
            getLog().info("[obtieneMontoMaximoAlias][" + rutMaxi + "][BCI_INI] Inicio ");
        }
         
        try{
          String segmento = misDestinatariosTTFFUtilityMB.getSesionMB().getSegmento();
          String[] tipoBancas = StringUtil.divide(TablaValores.getValor(TTFF_CONFIG, "TiposBancas", "desc"), "-");
          String valorMonto = "";
          
          if(getLog().isInfoEnabled()){
                getLog().info("[obtieneMontoMaximoAlias][" + rutMaxi + "] Antes del if (tipoBancas != null && tipoBancas.length > 0) " 
                    + "tipoBancas[" + StringUtil.contenidoDe(tipoBancas) + "]");
            }
          
          if(tipoBancas != null && tipoBancas.length > 0){
            for(int i=0; i < tipoBancas.length; i++){
              if(tipoBancas[i].equalsIgnoreCase(segmento)){
                valorMonto = TablaValores.getValor(TTFF_CONFIG, "montoMaximoAlias-" + segmento,"valorMensaje");
                this.maximo24HorasAlias = Double.parseDouble(TablaValores.getValor(TTFF_CONFIG, "montoMaximoAlias-" + segmento,"valor"));
              }
            }
            
            if(valorMonto.equalsIgnoreCase("")){
              valorMonto = TablaValores.getValor(TTFF_CONFIG, "montoMaximoAlias", "valorMensaje");
              this.maximo24HorasAlias = Double.parseDouble(TablaValores.getValor(TTFF_CONFIG, "montoMaximoAlias","valor"));
            }
            this.montoMaximoAlias = valorMonto;
          }
          
          if(getLog().isInfoEnabled()){
            getLog().info("[obtieneMontoMaximoAlias][" + rutMaxi + "] montoMaximoAlias["+montoMaximoAlias+"] [BCI_FIN] fin del metodo");
          }
        }
        catch (Exception e) {
          if (getLog().isEnabledFor(Level.ERROR)) {
                getLog().error("[obtieneMontoMaximoAlias][" + rutMaxi + "] montoMaximoAlias["+montoMaximoAlias+"] maximo24HorasAlias["+maximo24HorasAlias+"][BCI_FINEX][Exception] Error: " + e.getMessage(), e);
            }
          this.montoMaximoAlias = TablaValores.getValor(TTFF_CONFIG, "montoMaximoAlias", "valorMensaje");
          this.maximo24HorasAlias = Double.parseDouble(TablaValores.getValor(TTFF_CONFIG, "montoMaximoAlias","valor"));
    }
  }

    
    /**
     * <p>Metodo autorizaConEntrust, se consulta primeramente si atributo de isntancia es nulo con la finalidad de no generar multiples llamadas al componente 
     * de segundas claves, se entender�a que el cambio se da posterior al destroy de objetos, lo que no debiese generar inconsitencias por cambios en caliente
     * de algun tipo.</p>
     * 
     * Registro de versiones:
     * <ul>
     * <li>1.0 05/09/2017 Luis Lopez Alamos (SEnTRA) - Ricardo Carrasco C�ceres (Ing. Soft. BCI): Version Inicial.</li>
     * </ul>
     * <p>
     * 
     * @return autorizaConEntrust Atributo que representa la bandera de safe signer.
     * @since 2.0
     */
    public char getAutorizaConEntrust() {
    	if(getLog().isEnabledFor(Level.INFO)){
			getLog().info("[getAutorizaConEntrust][BCI_INI][Rut = " + misDestinatariosTTFFUtilityMB.getClienteMB().getRut()+"]");
        }
    	if (CHAR_VACIO==autorizaConEntrust) {
	    	try {
	    		autorizaConEntrust = jerarquiaClavesModelMB.tieneEntrust("GestorAliasTransferencias", SegundaClaveUtil.getUsuarioSegundaClave());
	            this.obtieneMontoMaximoAlias();
	        }
	        catch (Exception e) {
	            if (getLog().isEnabledFor(Level.WARN)) {
	                getLog().warn("[getAutorizaConEntrust]["+ misDestinatariosTTFFUtilityMB.getClienteMB().getRut()+ "] error al validar EntrustSoftToken:" + ErroresUtil.extraeStackTrace(e));
	            }
	        }
    	}
    	if(getLog().isEnabledFor(Level.INFO)){
			getLog().info("[getAutorizaConEntrust][BCI_FINOK]["+ misDestinatariosTTFFUtilityMB.getClienteMB().getRut()+ "] Tipo de EntrustSoftToken que posee: " + autorizaConEntrust);
        }
        return autorizaConEntrust;
    }

    /**
     * <p>M�todo que impide ingresar caracteres consecutivos para nombre y alias, puesto que desencadena un error visual en la aplicaci�n web que no puede
     * ser controlado desde ese �mbito</p>
     * 
     * Registro de versiones:
     * <ul>
     * <li>1.0 05/09/2017 Luis Lopez Alamos (SEnTRA) - Ricardo Carrasco C�ceres (Ing. Soft. BCI): Version Inicial.</li>
     * </ul>
     * <p>
     * 
     * @since 2.0
     */
    private void validaTamanioConsecutivo() {
    	if(getLog().isEnabledFor(Level.INFO)){
			getLog().info("[validaTamanioConsecutivo][BCI_INI][Nombre = " + nombre + "][Alias = " + alias);
        }
    	if (nombre.length()>MAXIMO_CARACTERES_CONSECUTIVOS) {
    		if (nombre.indexOf(" ")==-1) {
    			nombre = nombre.substring(0, MAXIMO_CARACTERES_CONSECUTIVOS).concat(" ") + nombre.substring(MAXIMO_CARACTERES_CONSECUTIVOS, nombre.length());
    		}
    	}
    	if (alias.length()>MAXIMO_CARACTERES_CONSECUTIVOS) {
    		if (alias.indexOf(" ")==-1) {
    			alias = alias.substring(0, MAXIMO_CARACTERES_CONSECUTIVOS).concat(" ") + alias.substring(MAXIMO_CARACTERES_CONSECUTIVOS, alias.length());
    		}
    	}
    	
    	if(getLog().isEnabledFor(Level.INFO)){
			getLog().info("[validaTamanioConsecutivo][BCI_FINOK][Nombre = " + nombre + "][Alias = " + alias);
        }
    }

}
