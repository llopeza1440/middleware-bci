package wcorp.transantiago.ui.action;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Hashtable;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import wcorp.bprocess.seguridad.token.TokenDelegate;
import wcorp.env.WCorpConfig;
import wcorp.model.seguridad.CamposDeLlaveTO;
import wcorp.model.seguridad.ControllerBCI;
import wcorp.model.seguridad.EstadoSegundaClaveTO;
import wcorp.model.seguridad.ParametrosEstrategiaSegundaClaveTO;
import wcorp.model.seguridad.RepresentacionNumericaSegundaClaveTO;
import wcorp.model.seguridad.RepresentacionSegundaClave;
import wcorp.model.seguridad.ResultadoOperacionSegundaClaveTO;
import wcorp.model.seguridad.SessionBCI;
import wcorp.model.seguridad.UsuarioSegundaClaveTO;
import wcorp.serv.seguridad.SeguridadException;
import wcorp.transantiago.Constantes;
import wcorp.transantiago.adapter.AdaptadorGenericoFactory;
import wcorp.transantiago.adapter.IAdaptadorWeb;
import wcorp.transantiago.exception.DelegateTarjetaBipException;
import wcorp.transantiago.exception.TarjetaBipException;
import wcorp.transantiago.ui.form.RecargaTarjetaBipForm;
import wcorp.transantiago.vo.DatosCargaVO;
import wcorp.transantiago.vo.PeriodoVO;
import wcorp.transantiago.vo.TarjetaVO;
import wcorp.util.ErroresUtil;
import wcorp.util.GeneralException;
import wcorp.util.TablaValores;
import wcorp.util.TextosUtil;
import wcorp.util.journal.Eventos;
import wcorp.util.journal.Journal;
/**
 * <b>GENERAR UNA RECARGA DE UNA TARJETA BIP!</b>
 * <p>
 * Clase que representa la accion de generar la recarga de un tarjeta Bip
 * <p>
 * Se espera que sea usado en los canales: BCInet, TBanc y Nova.
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 07/12/2006, Enrique Villar (Neoris Chile)- versi�n inicial
 * <li>1.1 25/01/2007, Enrique Villar (Neoris Chile)- se agrega la funcionalidad para programar una recarga
 * automatica
 * <li> 1.2 03/07/2007, Denisse Ca�ete (Neoris Chile)- Se habilita la segunda clave a trav�s de
 * ControllerBCI.validarAcceso(idName,idService)
 * <li> 2.0 20/10/2007, Enrique Villar (Neoris Chile)- se agrega metodo getTipoAutenticacion() responsable de
 * determinar el tipo de autenticacion utilizado para hacer transferencias.
 * <li> 2.1 19/12/2007, Mart�n Maturana (BCI)- Se usa metodo checkServicioObjeto() para controlar
 * los servicios no autorizados.
 * <li> 2.2 16/12/2008, Italo Morales (SEnTRA)- Se setea fecha en formato (dd/MM/yy HH:mm:ss) para desplegar
 * en comprobante de recarga. 
 * <li>2.3 04/12/2012, Diego Urrutia Guerra(Imagemaker IT). Se agrega l�gica para enviar mensaje a 
 *         p�gina de respuesta</li>
 * <li>2.4 23/04/2013, Eduardo Villagr�n Morales (Imagemaker IT) - Se agrega journalizaci�n para eventos de
 *          recarga bip.
 *          Se modifican llamadas a logger para respetar reglas del banco.</li>
 * <li> 2.5 23/04/2013, Victor Hugo Enero (Orand)- Se agrega la validacion de segunda clave.
 * <li> 2.6 30/08/2017 Marcelo Fuentes (SEnTRA) - Ricardo Carrasco (Ing. Soft. BCI): Se modifican m�todos 
 * {@link #execute(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)} y {@link #getTipoAutenticacion(SessionBCI, String)}.
 * Se agregan metodos {@link #journalizar(SessionBCI, String, String, String, String, String, String)} y {@link #getLogger()}.
 * Ademas se agregan las constantes {@link #TABLA_PARAMETROS_ENTRUST}, {@link #ENTRUST_TOKEN}, {@link #ENTRUST_SOFTTOKEN}, 
 * {@link #ID_PRODENTRUSTTOKEN}, {@link #ID_PRODENTRUSTSOFTTOKEN}, {@link #ID_EVENTO_SEGCLAVE}, {@link #SUB_EVENTO_OK}
 *  y {@link #SUB_EVENTO_NOK}.</li> 
 * </ul>
 * <p>
 * <B>Todos los derechos reservados por Banco de Cr�dito e Inversiones.</B>
 * <p>
 */
public class GenerarRecargaTarjetaBipAction extends Action {


    /** 
     * Tabla de par�metros de la aplicaci�n. 
     */
    private static final String TABLA_PARAMETROS_TRANSANTIAGO = "transantiago.parametros";
    /**
     * Clave de journal para el producto TARJETA BIP.
     */
    private static final String RECBIP_PRODUCTO = "TRJBIP";
    /**
     * Identificador de evento RECARGA BIP.
     */
    private static final String ID_EVENTO = "RECARGA";
    /**
     * Identificador de evento para journalizacion de segunda clave.
     */
    private static final String ID_EVENTO_SEGCLAVE = "LOGIN";
    /**
     * Identificador para cuando evento es OK.
     */
    private static final String EVENTO_OK = "P";
    /**
     * Identificador para cuando evento es NOK.
     */
    private static final String EVENTO_NOK = "R";
    
    /**
     * Identificador para cuando subEvento es OK.
     */
    private static final String SUB_EVENTO_OK = "OK";
    
    /**
     * Identificador para cuando subEvento es NOK.
     */
    private static final String SUB_EVENTO_NOK = "NOK";
    
    /**
     * tabla de par�metros de entrust.
     */
    private static final String TABLA_PARAMETROS_ENTRUST = "entrust.parametros";
    
    /**
     * Tipo de Segunda Clave Softtoken Entrust.
     */
    private static final String ENTRUST_TOKEN = TablaValores.getValor(TABLA_PARAMETROS_ENTRUST, "nombreToken", "desc");
    
    /**
     * Tipo de Segunda Clave Softtoken Entrust.
     */
    private static final String ENTRUST_SOFTTOKEN = TablaValores.getValor(TABLA_PARAMETROS_ENTRUST, "nombreSoftToken", "desc");
    
    /**
     * Identificador del producto Entrust Token.
     */
    private static final String ID_PRODENTRUSTTOKEN       = "HTK";
    
    /**
     * Identificador del producto Entrust SoftToken.
     */
    private static final String ID_PRODENTRUSTSOFTTOKEN   = "STK";
    
    /** 
     * Logger del action.
     */
    private transient Logger logger = (Logger) Logger.getLogger(GenerarRecargaTarjetaBipAction.class);

    private IAdaptadorWeb servicioCargaTransantiago;

    public GenerarRecargaTarjetaBipAction() {
        servicioCargaTransantiago = (IAdaptadorWeb) AdaptadorGenericoFactory.getInstance(Constantes.CANAL_WEB);

    }
    
    /**
     * <b>Recarga Tarjeta Bip</b><br>
     * 
     * <br>
     * Registro de Versiones:<ul>
     * <li>1.0 xx/xx/xxxx, Desconocido. (Desconocido): versi�n inicial.</li>
     * <li>1.1 30/08/2017  Marcelo Fuentes (SEnTRA) - Ricardo Carrasco (Ing. Soft. BCI): Se agrega logica que revisa 
     * si se requiere autenticaci�n Entrust Token o Entrust SoftToken y en caso de ser asi se valida. Ademas se normaliza log.<li>
     * </ul>
     * <br>
     * 
     * @param mapping Mapeo de forwards para la acci�n.
     * @param form    ActionForm con las variables de acci�n si corresponde.
     * @param request Petici�n HTTP que se est� procesando.
     * @param response Respuesta HTTP que se esta generando.
     * @return El forward correspondiente al formulario por defecto.
     * @throws Exception En caso de producirse un error.
     * @since 1.0
     * <P>
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
            HttpServletResponse response) throws Exception {
        getLogger().info("[execute] Inicio");
        
        RecargaTarjetaBipForm recargaForm = (RecargaTarjetaBipForm) form;
        Long comprobante = null;
        
        HttpSession sesionHttp = request.getSession(false);
        SessionBCI sesionBci = (SessionBCI) sesionHttp.getAttribute(WCorpConfig.SESION_BCI);
        
        try {
            
            String codigoConfirmacion = recargaForm.getConfirmationCode();
            if (codigoConfirmacion==null)
                codigoConfirmacion="";
            
            String rut = String.valueOf(sesionBci.getCliente().getRut());
            
            sesionBci.checkServicioObjeto(Constantes.IDSERVICE, recargaForm.getCuenta());
            
            int estatusOK = Integer.parseInt( 
                TablaValores.getValor(Constantes.PARAMSAFESIGNER, "estatusok", "valor"));
            int estatusExpirado = Integer.parseInt( 
                TablaValores.getValor(Constantes.PARAMSAFESIGNER, "estatusexpirado", "valor"));
            int estatusClaveIncorrecta = Integer.parseInt( 
                TablaValores.getValor(Constantes.PARAMSAFESIGNER, "estatusclaveincorrecta", "valor"));
            int estatuserrorlargo = Integer.parseInt( 
                TablaValores.getValor(Constantes.PARAMSAFESIGNER, "estatuserrorlargo", "valor"));
            int estatusfallido = Integer.parseInt( 
                TablaValores.getValor(Constantes.PARAMSAFESIGNER, "estatusintentofallido", "valor"));
            String sactive = TablaValores.getValor(Constantes.PARAMSAFESIGNER, "active", "valor");
            String segundaClaveValor = TablaValores.getValor(Constantes.PARAMSAFESIGNER, "segundaclave", "valor");
            
            ParametrosEstrategiaSegundaClaveTO parametrosEstrategiaSegundaClaveTO;
            parametrosEstrategiaSegundaClaveTO = new ParametrosEstrategiaSegundaClaveTO(Constantes.IDSERVICE, 
                                                                                        sesionBci.getCliente(), 
                                                                                        sesionBci.getCanal(), 
                                                                                        sesionBci);
            
            EstadoSegundaClaveTO estadoSegundaClaveTO = ControllerBCI.estadoSegundaClave(parametrosEstrategiaSegundaClaveTO);
            
            String segundaClaveSS = "";
            if (estadoSegundaClaveTO != null && estadoSegundaClaveTO.getIdSegundaClave() != null) {
                segundaClaveSS = estadoSegundaClaveTO.getIdSegundaClave().getNombre();
                segundaClaveSS = segundaClaveSS != null ? segundaClaveSS : "";
                
                if (getLogger().isEnabledFor(Level.DEBUG)) {
                    getLogger().debug("[execute] " + segundaClaveSS + ", status:[" + estadoSegundaClaveTO.isEstatus() + "]");
                }
                
                if (!estadoSegundaClaveTO.isEstatus() && (segundaClaveSS.equals(ENTRUST_SOFTTOKEN) || segundaClaveSS.equals(ENTRUST_TOKEN))) {
                    return mapping.findForward("fracaso");
                }
            }
            
            if (getLogger().isEnabledFor(Level.DEBUG)) {
                getLogger().debug("[execute] Se verifica autenticacion para " + segundaClaveSS);
            }
            if (segundaClaveSS.equals(segundaClaveValor) && codigoConfirmacion.length() > 0) {
                RepresentacionSegundaClave segundaClave = new RepresentacionNumericaSegundaClaveTO();
                segundaClave.setTipoDeClave(RepresentacionSegundaClave.CLAVE_ALFANUMERICA);
                segundaClave.setClaveAlfanumerica(codigoConfirmacion);
                
                ResultadoOperacionSegundaClaveTO resultado = ControllerBCI
                    .validarSegundaClave(request, Constantes.IDNAME,
                    Constantes.IDSERVICE, segundaClave);
                if (resultado == null) {
                    this.grabaError(request, "errors.noTransaccion", 
                        TablaValores.getValor("errores.codigos","SAFESIGNER05", "Desc"));
                    getLogger().error("[execute] [" + sesionBci.getCliente().getRut() 
                      + "] La confirmacion de la segunda clave es nula");
                  throw new SeguridadException("SAFESIGNER05");
                }
                else if (!resultado.isEstatus()) {

                    if (getLogger().isInfoEnabled()) {
                        getLogger().info("[" + sesionBci.getCliente().getRut() + "] Resultado de validacion: "
                            + resultado.getIdCodigoEstado() + " Descripcion:" + resultado.getGlosa());
                    }

                    if (resultado.getIdCodigoEstado() == estatusClaveIncorrecta) {
                        this.grabaError(request, "errors.noTransaccion",
                            TablaValores.getValor("errores.codigos", "SAFESIGNER01", "Desc"));
                        throw new SeguridadException("SAFESIGNER01");
                    }
                    else if (resultado.getIdCodigoEstado() == estatuserrorlargo) {
                        this.grabaError(request, "errors.noTransaccion",
                            TablaValores.getValor("errores.codigos", "SAFESIGNER02", "Desc"));
                        throw new SeguridadException("SAFESIGNER02");
                    }
                    else if (resultado.getIdCodigoEstado() == estatusfallido) {
                        this.grabaError(request, "errors.noTransaccion",
                            TablaValores.getValor("errores.codigos", "SAFESIGNER03", "Desc"));
                        throw new SeguridadException("SAFESIGNER03");
                    }
                    else if (resultado.getIdCodigoEstado() == estatusExpirado) {
                        this.grabaError(request, "errors.noTransaccion",
                            TablaValores.getValor("errores.codigos", "SAFESIGNER04", "Desc"));
                        throw new SeguridadException("SAFESIGNER04");
                    }
                    else {
                        this.grabaError(request, "errors.noTransaccion",
                            TablaValores.getValor("errores.codigos", "SAFESIGNER05", "Desc"));
                        throw new SeguridadException("SAFESIGNER05");
                    }

                }



            
            }
            else if (segundaClaveSS.equals(ENTRUST_SOFTTOKEN)) {
                String modoTransaccion = "";
                if(codigoConfirmacion.length() > 0) {
                    modoTransaccion = "OFFLINE";
                }
                else {
                    modoTransaccion = "ONLINE";
                    codigoConfirmacion = (String)sesionBci.getAttrib("transactionId");
                    sesionBci.removeAttrib("transactionId");
                }
                
                if(codigoConfirmacion.length() == 0){
                    this.grabaError(request, "errors.noTransaccion", TablaValores.getValor("errores.codigos","ENTRUSTFRASE03", "Desc"));
                    if(getLogger().isEnabledFor(Level.ERROR)){
                        getLogger().error("[execute] [" + sesionBci.getCliente().getRut() + "] La confirmacion de la segunda clave es nula");
                    }
                    throw new SeguridadException("ENTRUSTFRASE03");
                }
                
                try {
                    CamposDeLlaveTO campoDeLlaves = sesionBci.getParametrosEstrategiaSegundaClaveTO().getCamposDeLlave();
                    
                    boolean resultado = ControllerBCI.verificarAutenticacion(ENTRUST_SOFTTOKEN, 
                                                                              modoTransaccion, 
                                                                              sesionBci.getParametrosEstrategiaSegundaClaveTO().getUsuarioSegundaClave(), 
                                                                              campoDeLlaves, 
                                                                              Constantes.IDSERVICE, 
                                                                              codigoConfirmacion);
                    sesionBci.setParametrosEstrategiaSegundaClaveTO(null);
                    
                    if(getLogger().isDebugEnabled()){
                        getLogger().debug("[execute] resultado de autenticacion: " + resultado);
                    }
                    
                    journalizar(sesionBci, ID_PRODENTRUSTSOFTTOKEN, ID_EVENTO_SEGCLAVE, SUB_EVENTO_OK, rut, modoTransaccion, EVENTO_OK);
                }
                catch (SeguridadException se) {
                    journalizar(sesionBci, ID_PRODENTRUSTSOFTTOKEN, ID_EVENTO_SEGCLAVE, SUB_EVENTO_NOK, rut, modoTransaccion, EVENTO_NOK);
                    this.grabaError(request, "errors.noTransaccion", se.getInfoAdic());
                    if(getLogger().isEnabledFor(Level.ERROR)){
                        getLogger().error("[execute] [" + sesionBci.getCliente().getRut() + "] SeguridadException:" + ErroresUtil.extraeStackTrace(se));
                    }
                    throw se;
                }
            }
            else if (segundaClaveSS.equals(ENTRUST_TOKEN)) {
                if(codigoConfirmacion.length() == 0){
                    this.grabaError(request, "errors.noTransaccion", TablaValores.getValor("errores.codigos","ENTRUSTFRASE01", "Desc"));
                    if(getLogger().isEnabledFor(Level.ERROR)){
                        getLogger().error("[execute] [" + sesionBci.getCliente().getRut() + "] La confirmacion de la segunda clave es nula");
                    }
                    throw new SeguridadException("ENTRUSTFRASE01");
                }
                
                try {
                    String usuarioId = String.valueOf(sesionBci.getCliente().getRut()) + String.valueOf(sesionBci.getCliente().getDigito());
                    UsuarioSegundaClaveTO usuarioSegundaClave = new UsuarioSegundaClaveTO(usuarioId, 
                                                                                          ControllerBCI.obtieneDominio(sesionBci.getCanal().getCanalID()), 
                                                                                          sesionBci.getCanal().getCanalID());
                    
                    boolean resultado = ControllerBCI.verificarAutenticacion(ENTRUST_TOKEN, null, usuarioSegundaClave, null, null, codigoConfirmacion);
                    
                    if(getLogger().isDebugEnabled()){
                        getLogger().debug("[execute] resultado de autenticacion: " + resultado);
                    }
                    
                    journalizar(sesionBci, ID_PRODENTRUSTTOKEN, ID_EVENTO_SEGCLAVE, SUB_EVENTO_OK, rut, null, EVENTO_OK);
                }
                catch (SeguridadException se) {
                    journalizar(sesionBci, ID_PRODENTRUSTTOKEN, ID_EVENTO_SEGCLAVE, SUB_EVENTO_NOK, rut, null, EVENTO_NOK);
                    this.grabaError(request, "errors.noTransaccion", se.getInfoAdic());
                    if(getLogger().isEnabledFor(Level.ERROR)){
                        getLogger().error("[execute] [" + sesionBci.getCliente().getRut() + "] SeguridadException:" + ErroresUtil.extraeStackTrace(se));
                    }
                    throw se;
                }
            }
            else {
                ControllerBCI.validarAcceso(request, Constantes.IDNAME, Constantes.IDSERVICE);
            }
            
            
            sesionBci.checkServicioObjeto(Constantes.IDSERVICE, recargaForm.getCuenta());
            
            DatosCargaVO datosCargaEnvio = new DatosCargaVO();
            datosCargaEnvio.setCanal(Constantes.CANAL_WEB);
            datosCargaEnvio.setMonto(new Long(recargaForm.getMontoCarga()));
            datosCargaEnvio.setNumeroCuenta(recargaForm.getCuenta());
            datosCargaEnvio.setTipoCuenta(recargaForm.getTipoCuenta());
            datosCargaEnvio.setNueva(recargaForm.getNueva().equals("1"));
            datosCargaEnvio.setMensajeMail(recargaForm.getMsgDestino());
            TarjetaVO tarjeta = new TarjetaVO();
            tarjeta.setAlias(recargaForm.getAliasTarjeta());
            tarjeta.setNumeroTarjeta(Long.valueOf(recargaForm.getNumeroTarjeta()));
            tarjeta.setMail(recargaForm.getEmail());
            tarjeta.setRut(Long.valueOf(recargaForm.getRutCliente()));
            tarjeta.setDigitoVerificador(recargaForm.getDigitoVerificadorCliente().charAt(0));
            datosCargaEnvio.setTarjeta(tarjeta);
            datosCargaEnvio.setBanco(Constantes.getBancoSegunCanalWeb(recargaForm.getBanco()));
            datosCargaEnvio.setMailCliente(recargaForm.getMailClienteCuenta());
            datosCargaEnvio.setTipoAutenticacion(getTipoAutenticacion(sesionBci, recargaForm.getTipoCuenta()));
            if (recargaForm.getProgramaSN().equals("1")) {
                PeriodoVO periodo = new PeriodoVO();
                periodo.setRangoPeriodo(new Long(recargaForm.getPeriodoPrograma()));
                periodo.setTipoPeriodo(new Long(recargaForm.getTipoPrograma()));
                datosCargaEnvio.setPeriodo(periodo);
                comprobante = (Long) servicioCargaTransantiago.registraConvenioPrePago(datosCargaEnvio);
            }
            else {
                comprobante = (Long) servicioCargaTransantiago.recargaRemota(datosCargaEnvio);

                // journalizo el evento
                try{
                    Eventos eventos = new Eventos(sesionBci);
                    Journal journal = new Journal(sesionBci);
                    eventos.setIdProducto(RECBIP_PRODUCTO);
                    eventos.setCodEventoNegocio(ID_EVENTO);
                    eventos.setEstadoEventoNegocio(EVENTO_OK);
                    eventos.setClavePrincipal(datosCargaEnvio.getNumeroCuenta());
                    eventos.setClaveSecundaria(comprobante.toString());
                    eventos.setMonto(datosCargaEnvio.getMonto().doubleValue());
                    eventos.setCampoVariable("BIP: " + datosCargaEnvio.getTarjeta().getNumeroTarjeta());
                    journal.journalizar(eventos);
                }
                catch (Exception exp){
                    if (getLogger().isEnabledFor(Level.WARN)){
                        getLogger().warn("Error al journalizar evento recarga bip exitoso", exp);
                    }
                }
            }
            
            String canal = sesionBci.getCanal().getCanalID();
            
            String mensaje1 = null;
            String mensaje2 = null;
            
            mensaje1 = TablaValores.getValor(TABLA_PARAMETROS_TRANSANTIAGO, "MSG_REC_INFO_1", canal);
            mensaje2 = TablaValores.getValor(TABLA_PARAMETROS_TRANSANTIAGO, "MSG_REC_INFO_2", "Desc");
            request.setAttribute("mensaje_info_1", mensaje1);
            request.setAttribute("mensaje_info_2", mensaje2);
            
        } 
        catch (TarjetaBipException ex) {
            if (getLogger().isDebugEnabled()){
                getLogger().debug(ErroresUtil.extraeStackTrace(ex));
            }
            String msg = ex.getSimpleMessage();
            String mto = "";
            // Si el error se produce por que sobrepas� el monto permitido
            if (ex.getCodigo().equals("TSG-O029")) {
                mto = ex.getMtoDisponibleRecarga();
                msg = msg + " M�ximo permitido es $" + mto + ".";
            }
            // si el error se produce por saldo insuficiente
            if (ex.getCodigo().equals("TSG-O003")) {
                mto = ex.getInformacionSaldoCtaCte();
                msg = msg + " El saldo de su cuenta corriente es $" + mto + ".";
            }

            // journalizo el evento
            try{
                Eventos eventos = new Eventos(sesionBci);
                Journal journal = new Journal(sesionBci);
                eventos.setIdProducto(RECBIP_PRODUCTO);
                eventos.setCodEventoNegocio(ID_EVENTO);
                eventos.setEstadoEventoNegocio(EVENTO_NOK);
                eventos.setClavePrincipal(recargaForm.getCuenta());
                eventos.setMonto(Double.valueOf(recargaForm.getMontoCarga()).doubleValue());
                eventos.setCampoVariable("BIP: " + recargaForm.getNumeroTarjeta());
                journal.journalizar(eventos);
            }
            catch (Exception exp){
                if (getLogger().isEnabledFor(Level.WARN)){
                    getLogger().warn("Error al journalizar evento recarga bip fallido", exp);
                }
            }
            this.grabaError(request, "errors.tarjeta_bip", msg);
            return mapping.findForward("error");
        } 
        catch (DelegateTarjetaBipException ex) {
            if (getLogger().isDebugEnabled()){
                getLogger().debug(ErroresUtil.extraeStackTrace(ex));
            }
            // journalizo el evento
            try{
                Eventos eventos = new Eventos(sesionBci);
                Journal journal = new Journal(sesionBci);
                eventos.setIdProducto(RECBIP_PRODUCTO);
                eventos.setCodEventoNegocio(ID_EVENTO);
                eventos.setEstadoEventoNegocio(EVENTO_NOK);
                eventos.setClavePrincipal(recargaForm.getCuenta());
                eventos.setMonto(Double.valueOf(recargaForm.getMontoCarga()).doubleValue());
                eventos.setCampoVariable("BIP: " + recargaForm.getNumeroTarjeta());
                journal.journalizar(eventos);
            }
            catch (Exception exp){
                if (getLogger().isEnabledFor(Level.WARN)){
                    getLogger().warn("Error al journalizar evento recarga bip fallido", exp);
                }
            }
            this.grabaError(request, "errors.noTransaccion", "");
            return mapping.findForward("error");

        } 
        catch (wcorp.serv.seguridad.NoSessionException nse) {
            if (getLogger().isDebugEnabled()){
                getLogger().debug("ControllerBCI : Sesion Expiro...");
            }
            String urlNeto = mapping.findForward("login").getPath();
            request.setAttribute("url", urlNeto);
            // journalizo el evento
            try{
                Eventos eventos = new Eventos(sesionBci);
                Journal journal = new Journal(sesionBci);
                eventos.setIdProducto(RECBIP_PRODUCTO);
                eventos.setCodEventoNegocio(ID_EVENTO);
                eventos.setEstadoEventoNegocio(EVENTO_NOK);
                eventos.setClavePrincipal(recargaForm.getCuenta());
                eventos.setMonto(Double.valueOf(recargaForm.getMontoCarga()).doubleValue());
                eventos.setCampoVariable("BIP: " + recargaForm.getNumeroTarjeta());
                journal.journalizar(eventos);
            }
            catch (Exception exp){
                if (getLogger().isEnabledFor(Level.WARN)){
                    getLogger().warn("Error al journalizar evento recarga bip fallido", exp);
                }
            }
            return mapping.findForward("error");
        } 
        catch (SeguridadException se) {
            if (getLogger().isDebugEnabled()){
                getLogger().debug("Error en el controller BCI");
                getLogger().debug(ErroresUtil.extraeStackTrace(se));
            }
            request.setAttribute("javax.servlet.jsp.jspException", se);
            // journalizo el evento
            try{
                Eventos eventos = new Eventos(sesionBci);
                Journal journal = new Journal(sesionBci);
                eventos.setIdProducto(RECBIP_PRODUCTO);
                eventos.setCodEventoNegocio(ID_EVENTO);
                eventos.setEstadoEventoNegocio(EVENTO_NOK);
                eventos.setClavePrincipal(recargaForm.getCuenta());
                eventos.setMonto(Double.valueOf(recargaForm.getMontoCarga()).doubleValue());
                eventos.setCampoVariable("BIP: " + recargaForm.getNumeroTarjeta());
                journal.journalizar(eventos);
            }
            catch (Exception exp){
                if (getLogger().isEnabledFor(Level.WARN)){
                    getLogger().warn("Error al journalizar evento recarga bip fallido", exp);
                }
            }
            return mapping.findForward("fracaso");
        } 
        catch (Exception e) {
            if (getLogger().isDebugEnabled()){
                getLogger().debug("ControllerBCI : Error en Controller BCI... Exception...");
            }
            String urlNeto = mapping.findForward("login").getPath();
            ActionMessages messages = new ActionMessages();
            messages.add("conAutentica", new ActionMessage("error.conAutentica"));
            saveMessages(request, messages);
            request.setAttribute("urlTagGoogle", "/personas/invertir_ffmm/error");
            request.setAttribute("descripcionNoTransaccion", "");
            request.setAttribute("url", urlNeto);
            // journalizo el evento
            try{
                Eventos eventos = new Eventos(sesionBci);
                Journal journal = new Journal(sesionBci);
                eventos.setIdProducto(RECBIP_PRODUCTO);
                eventos.setCodEventoNegocio(ID_EVENTO);
                eventos.setEstadoEventoNegocio(EVENTO_NOK);
                eventos.setClavePrincipal(recargaForm.getCuenta());
                eventos.setMonto(Double.valueOf(recargaForm.getMontoCarga()).doubleValue());
                eventos.setCampoVariable("BIP: " + recargaForm.getNumeroTarjeta());
                journal.journalizar(eventos);
            }
            catch (Exception exp){
                if (getLogger().isEnabledFor(Level.WARN)){
                    getLogger().warn("Error al journalizar evento recarga bip fallido", exp);
                }
            }
            return mapping.findForward("error");
        }

        Date fecha = new Date();
    String fechaPalabras = TextosUtil.fechaEnPalabras(fecha);
    SimpleDateFormat dateSimpleFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    String fechaFormat =  dateSimpleFormat.format(fecha);
        
        request.setAttribute("fechaFormat", fechaFormat);
        request.setAttribute("fecha", fechaPalabras);
        request.setAttribute("comprobante", comprobante);
        
        return mapping.getInputForward();

    }

    /**
     * M�todo de agregar un error de acuerdo al tipo de error (keyError)
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 31-01-2007, Enrique Villar (Neoris Chile)- versi�n inicial
     * </ul>
     * </p>
     * 
     * @param request
     * @param keyError key del error correspondiente al error del property de la aplicaccion(ver strut-config)
     * @param valueError un mensaje con el error si es necesario o "" si no se desea enviar nada.d
     */
    private void grabaError(HttpServletRequest request, String keyError, String valueError) {
        ActionErrors errors = new ActionErrors();
        ActionError error = new ActionError(keyError, valueError);
        errors.add("errorGlobal", error);
        super.saveErrors(request, errors);

    }

    /**
     * Metodo responsable de determinar el tipo de autenticacion utilizado para hacer transferencias.
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 20/10/2007 Enrique Villar G.(Neoris Chile) - versi�n inicial
     * <li>1.1 30/08/2017  Marcelo Fuentes (SEnTRA) - Ricardo Carrasco (Ing. Soft. BCI): Se agrega obtenci�n de montos tope 
     * para segunda clave Entrust.<li>
     * </ul>
     * </p>
     * 
     * @param session La session Bci.
     * @param tipoCuenta El tipo de cuenta.
     * @return String con el tipo de autenticacion.
     * @throws GeneralException en caso de error.
     * @throws Exception en caso de error.
     * @since version 2.0
     */
    public String getTipoAutenticacion(SessionBCI session, String tipoCuenta) throws GeneralException, Exception {
        TokenDelegate delegate = new TokenDelegate();

        String topeCCT = "";
        String topeCPR = "";
        String tope = "";
        String tipoAutenticacion = null;
        
       
        String identificadorPrioridadSegundaClave = (String) session
            .getAttrib("identificadorPrioridadSegundaClave");
        String identificadorEstadoSegundaClave = (String) session.getAttrib("identificadorEstadoSegundaClave");
        String sactive = TablaValores.getValor(Constantes.PARAMSAFESIGNER, "active", "valor");
        String segundaClaveValor = TablaValores.getValor(Constantes.PARAMSAFESIGNER, "segundaclave", "valor");
        
        if (segundaClaveValor.equalsIgnoreCase(identificadorPrioridadSegundaClave)
            && sactive.equalsIgnoreCase(identificadorEstadoSegundaClave)) {
            topeCCT = "topeCCTSAFESIGNER";
            topeCPR = "topeCPRSAFESIGNER";
            tipoAutenticacion = segundaClaveValor;
            getLogger().debug("setMontosMaximosAutorizados   : Tiene SafeSigner");
        }
        else if (ENTRUST_SOFTTOKEN.equalsIgnoreCase(identificadorPrioridadSegundaClave)
            && sactive.equalsIgnoreCase(identificadorEstadoSegundaClave)) {
        	//topeCCT = "topeCCTENTRUSTSOFTTOKEN";
            //topeCPR = "topeCPRENTRUSTSOFTTOKEN";
            topeCCT = "topeCCTSAFESIGNER";
            topeCPR = "topeCPRSAFESIGNER";
            tipoAutenticacion = segundaClaveValor;
            getLogger().debug("setMontosMaximosAutorizados   : Tiene EntrustSoftToken");
        }
        else if (ENTRUST_TOKEN.equalsIgnoreCase(identificadorPrioridadSegundaClave)
            && sactive.equalsIgnoreCase(identificadorEstadoSegundaClave)) {
        	//topeCCT = "topeCCTENTRUSTTOKEN";
        	//topeCPR = "topeCPRENTRUSTTOKEN";
            topeCCT = "topeCCTSAFESIGNER";
            topeCPR = "topeCPRSAFESIGNER";
            tipoAutenticacion = segundaClaveValor;
            getLogger().debug("setMontosMaximosAutorizados   : Tiene EntrustToken");
        }
        else {
        Hashtable seguridad = session.getMetodosAutenticacion();

        if (seguridad != null) {
            getLogger().debug("setMontosMaximosAutorizados   : seguridad no null");
            Boolean estadoMetodo = (Boolean) seguridad.get("TOK");
            if (estadoMetodo != null) {
                getLogger().debug("setMontosMaximosAutorizados   :Posee Token");
                if (estadoMetodo.booleanValue()) {
                    getLogger().debug("setMontosMaximosAutorizados   :Adem�s, SI est� autenticado con token");
                    tipoAutenticacion = "Token";
                } 
                else if (session.getAttrib("permitirAutentificacion") == null) {
                    getLogger().debug("setMontosMaximosAutorizados   :Adem�s, token est� opcional");
                    tipoAutenticacion = "Token";
                }
            }
        }
        if (tipoAutenticacion != null && tipoAutenticacion.equalsIgnoreCase("Token")) {
            getLogger().debug("setMontosMaximosAutorizados   : multipass");
            topeCCT = "topeCCTTOKEN";
            topeCPR = "topeCPRTOKEN";
        } 
        else if (delegate.estadoClaveDos(session.getCliente().getRut(), session.getCliente().getDigito())) {
            getLogger().debug("setMontosMaximosAutorizados   : tiene segunda clave");
            topeCCT = "topeCCTPASS";
            topeCPR = "topeCPRPASS";
        } 
        else {
            getLogger().debug("setMontosMaximosAutorizados  : tipo de autenticacion clave internet");
            topeCCT = "topeCCT";
            topeCPR = "topeCPR";
        }

        }
        
        if (tipoCuenta.equalsIgnoreCase("CCT"))
            tope = topeCCT;
        if (tipoCuenta.equals("CPR"))
            tope = topeCPR;
        
        if (getLogger().isInfoEnabled()) {
            getLogger().debug("tope [" + tope + "]");
        }
        return tope;
    }
    
    /**
     * M�todo que realiza la journalizaci�n.
     * <p>
     * Registro de versiones:<ul>
     * <li> 1.0 30/08/2017, Marcelo Fuentes (SEnTRA) - Ricardo Carrasco (Ing. Soft. BCI): Versi�n Inicial.</li>
     * </ul>
     * <p>
     *
     * @param sesionBci La sessi�n del cliente.
     * @param idProducto C�digo del producto.
     * @param idEvento C�digo del evento de negocio.
     * @param subCodNeg Sub c�digo del evento de negocio.
     * @param clavePrincipal Clave principal.
     * @param campoVariable un campo variable.
     * @param estadoEventoNegocio el estado de la operacion a journalizar.
     * @since 2.6
     */
    private void journalizar(SessionBCI sesionBci, String idProducto, String idEvento, String subCodNeg, 
            String clavePrincipal, String campoVariable, String estadoEventoNegocio) {
        try {
            Eventos eventos = new Eventos(sesionBci);
            Journal journal = new Journal(sesionBci);
            eventos.setIdProducto(idProducto);
            eventos.setCodEventoNegocio(idEvento);
            eventos.setSubCodEventoNegocio(subCodNeg);
            eventos.setClavePrincipal(clavePrincipal);
            if (campoVariable != null) {
                eventos.setCampoVariable(campoVariable);
            }
            if (estadoEventoNegocio != null) {
                eventos.setEstadoEventoNegocio(estadoEventoNegocio);
            }
            journal.journalizar(eventos);
        }
        catch (Exception exp) {
            if (logger.isEnabledFor(Level.WARN)) {
                logger.warn("Error al journalizar evento ", exp);
            }
        }
    }
    
    /**
     * Metodo encargado de obtener el logger().
     * <p>
     *
     * Registro de versiones:<ul>
     * <li> 1.0 30/08/2017,  Marcelo Fuentes (SEnTRA) - Ricardo Carrasco (Ing. Soft. BCI): Versi�n Inicial.</li>
     * </ul>
     * <p>
     * @return el logger.
     * 
     * @since 2.6
     */
    private Logger getLogger() {
        if (logger == null) {
            logger = Logger.getLogger(this.getClass());
        }
        return logger;
    }
    
}
