<%@taglib prefix="html"  uri="/WEB-INF/struts-html.tld"%>
<%@taglib prefix="bean"  uri="/WEB-INF/struts-bean.tld"%>
<%@taglib prefix="logic" uri="/WEB-INF/struts-logic.tld"%>
<%@taglib prefix="c"     uri="http://java.sun.com/jstl/core"%>
<%@taglib prefix="fmt"   uri="http://java.sun.com/jstl/fmt"%>
<%@page import="wcorp.util.Formatting" %>
<%@page import="java.util.Locale"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html:html>
<head>
<title>Recarga de Celular</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link href="/bcinetwls/recargacelulares/style/estiloBcinuevo.css" rel="stylesheet" type="text/css" />
<script src="/bcinetwls/recargacelulares/js/recargas.js" type="text/javascript" LANGUAGE="JavaScript1.5"></script>
<script src="/bcinetwls/recargacelulares/js/common.js" type="text/javascript"></script>
<link href="/bcinetwls/common/css/frames.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="/bcinetwls/common/js/bcipassOnline.js"></script>
<script type="text/javascript" src="/bcinetwls/common/js/multipass.js"></script>
<script type="text/javascript" src="/bcinetwls/common/js/jquery.js"></script>
<script>  

var numerosAlias = new Array();
var contador=0;
<c:forEach items="${recargas}" var="alias">
	numerosAlias[contador] = '<c:out value="${alias.aliasRecarga}"/>';
	contador++;
</c:forEach>

  
var confirmo=false;
function mOver(which){ which.style.background='#FFFFD2' } 
function mOut(which){ which.style.background='#FFFFFF' } 
function mOut2(which){ which.style.background='#F5F7FC' } 
function carga_celular()
{
    if (document.RecargaCelularesForm.codigo_alias.value==0){
        if (document.RecargaCelularesForm.alias_recarga.value!="" && numerosAlias.length>0){
            for (i=0;i<numerosAlias.length;i++){
                if (numerosAlias[i]==document.RecargaCelularesForm.alias_recarga.value){
                    alert("El nombre de alias ya existe");
                    return false;
                }
            }
        }
    }
    
<c:choose>
    <c:when test="${!empty EntrustSoftToken}">
        var codigoConfQR = document.getElementById("confirmationCode");
        if (codigoConfQR != null) {
            try {
                var codConfOffline = window.frames["iframe_offline"].document.getElementById("codigoConfirmacionQR");
                if (codConfOffline != null) {
                  codigoConfQR.value = codConfOffline.value;
                  if (codigoConfQR.value == "") {
                      alert("Debe ingresar 8 d\u00edgitos para la confirmaci\u00F3n de Bci Pass");
                      return false;
                  }
                }else{
                  return false;
                }
            } catch(Err) {
                alert(Err.description);
                return false;
            }
        }
        
    </c:when>
    
    <c:when test="${!empty EntrustToken}">
        if (document.RecargaCelularesForm.confirmationCode.value.length!=6) {
            alert("Debe ingresar 6 d\u00edgitos para la confirmaci\u00F3n de Bci Pass");
            return false;
        }
    </c:when>
    
    <c:when test="${!empty data}">
				if (document.RecargaCelularesForm.confirmationCode.value.length!=7) {
					 alert("Debe ingresar 7 d\u00edgitos para la confirmaci\u00F3n de Multipass M\u00F3vil");
					 return false;
				}
    </c:when>
</c:choose>
    
    if (!confirmo) {
        confirmo=true;
        document.RecargaCelularesForm.submit();
    }

}

function mostrarBotonRecarga(){
    var btn1 = document.getElementById('btn1');
    var btn2 = document.getElementById('btn2');
    btn1.style.display="";
    btn2.style.display="none";
}

function ocultarBotonRecarga(){
    var btn1 = document.getElementById('btn1');
    var btn2 = document.getElementById('btn2');
    btn1.style.display='none';
    btn2.style.display='none';
}
</script>

<logic:present name="data"> 
<!--[if lt IE 9]>
<script type="text/javascript">
		function getImageSrc(base64Src) {
		        return "/segundaClave/ImagenQR";
		}
</script>
<![endif]-->

<!--[if (gte IE 9)|!(IE)]><!-->
<script type="text/javascript">
			function getImageSrc(base64Src) {
			return base64Src;
			}
</script>
<!--<![endif]-->
</logic:present>

<script type="text/javascript">

function showClientId() {
	o = document.getElementById('client-id-help');
	a = document.getElementById('client-id-help2');
	if ( o && a ) {
		o.style.display='block';
		a.style.display='none';
	}
}
function hideClientId() {
	o = document.getElementById('client-id-help');
	a = document.getElementById('client-id-help2');
	if ( o && a ) {
		o.style.display='none';
		a.style.display='block';
	}
}

function switchEmailEdit() {
	o = document.getElementById( 'edit-email-field' );
	b = document.getElementById( 'edit-email-button' );
	if ( o && b ) {
		if ( o.disabled ) {
			b.value='Deshacer';
			b.className = 'task undo-button';
			b.disabled = true;
			ge('edit-email-field-instruction').style.display='block';
		} else {
			b.value='Modificar email';
			o.value='pepe@hotmail.com';
			b.className = 'task';
			ge('edit-email-field-instruction').style.display='none';
		}
		o.disabled = !o.disabled;
	}
}

function isNumberKey(evt)
{
   var charCode = (evt.which) ? evt.which : event.keyCode
   if (charCode > 31 && (charCode < 48 || charCode > 57))
      return false;

   return true;
} 

function iniciar(image) {
	  var qr = document.getElementById("qr");
	  qr.src = getImageSrc(image);
}

</script>
</head>
<c:choose>
	<c:when test="${!empty data}">
		<body onLoad="iniciar('data:image/png;base64,<bean:write name="data"/>');">
	</c:when>
	<c:when test="${!empty EntrustSoftToken}">
		<body onLoad="inicializaValoresGenerico(<bean:write name="timeOutEntrust"/>, <bean:write name="frecuenciaEntrust"/>, this.document.RecargaCelularesForm);ocultarBotonRecarga();">
	</c:when>
	<c:when test="${!empty EntrustToken}">
		<body onLoad="inicializarGenerico(this.document.RecargaCelularesForm);">
	</c:when>
	<c:otherwise>
		<body onload="btn1.style.display='none'; btn2.style.display='';">
	</c:otherwise>
</c:choose>
<html:form action="/cargaCelular" method="post"> 
    <html:hidden property="numeroCelular"/>
    <html:hidden property="reNumeroCelular"/>
    <html:hidden property="monto_carga"/>
    <html:hidden property="cuenta_origen"/>
    <html:hidden property="disp_recarga"/>
    <html:hidden property="nombre_operador"/>
    <html:hidden property="nombre_cuenta"/>
    <html:hidden property="saldoCuentaCte"/>
    <html:hidden property="saldoSobreGiroCte"/>
    <html:hidden property="saldoDisponibleCte"/>
    <html:hidden property="nombreCuentaCte"/>
    <html:hidden property="rut_cliente"/>
    <html:hidden property="operador_movil"/>
    <html:hidden property="codigo_alias"/>
    <html:hidden property="tipo_recarga"/>
    <html:hidden property="tipoOperadora"/>
    <input type="hidden" name="paso" value="3"/>
<table width="100%" border="0" cellspacing="0" cellpadding="0" id="contenidos">
  <tr>
    <td width="1030" id="tdcontenido"><h1>Recarga <span class="titulo">de Celular/TV</span></h1>
      <table width="350" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="200"><h3>Paso <strong>2</strong> de 3</h3> </td>
        </tr>
      </table>      
	<table border="0" cellpadding="0" cellspacing="0" id="borde">
      <tr>
        <th width="6"><img src="images/curvaizquierdaarriba.gif" width="6" height="6" /></th>
        <td id="bordearribatabla"></td>
        <td width="6" height="6" align="right"><img src="images/curvaderechaarriba.gif" width="6" height="6" /></td>
      </tr>
      <tr>
        <td width="6" height="6" id="bordeizqmorado">&nbsp;</td>
            <th class="morado"><h1>CONFIRMACI&oacute;N</h1>            </th>
        <td id="bordederechomorado">&nbsp;</td>
      </tr>
      <tr>
        <td id="tdbordeizq">&nbsp;</td>
            <td class="centrotablaformularios"><h2><span class="comprobantestandar">Tu
                recarga a&uacute;n no ha sido realizada</span>, por favor <strong>revisa
                los datos</strong>. Si los datos est&aacute;n correctos, presiona
                el bot&oacute;n<strong> Recargar.</strong></h2>
 <table width="100%" border="0" cellpadding="0" cellspacing="0" id="tablacontenedora" >  
    <tr>
    <td>
        <table border="0" cellpadding="0" cellspacing="0" id="datostabuladosformularios" >
          <tr>
            <td colspan="2" id="titulosdatos">Informaci&oacute;n de recarga </td>
          </tr>
          <tr >
            <td width="22%" id="nombredato">Monto a cargar : </td>
            <td width="45%" class="importante"><script>document.write(cambiaComaPunto('<fmt:formatNumber value="${monto_carga}" pattern="$#,##0"/>'));</script></td>
          </tr>
          <logic:greaterThan name="montoAdicional" value="0">
          <tr  >
            <td width="22%" id="nombredato">Monto Abono Adicional: </td>
            <td width="45%" class="importante"><script>document.write('$<%=Formatting.number(Integer.parseInt((String)session.getAttribute("montoAdicional")),"#,##0",new Locale ("ES","CL")) %>');</script></td>
          </tr>
          </logic:greaterThan>
          <logic:present name="promociones"> 
             <logic:notEqual name="promociones" value="">
          	<tr >
	            <td width="22%" id="nombredato">Adicionales: </td>
            	<td width="45%" class="importante"><script>document.write('<bean:write name="promociones" />');</script></td>
          	</tr>
          	 </logic:notEqual>
          </logic:present>
                <tr >
            <td id="nombredato" >Compa&ntilde;&iacute;a : </td>
            <td><bean:write name="RecargaCelularesForm" property="nombre_operador"/></td>
          </tr>
          <tr >
		    <td id="nombredato">
			<logic:equal name="tipoOperadora" value="TV">
				Rut de Recarga :
			</logic:equal>
			<logic:notEqual name="tipoOperadora" value="TV">
				N&uacute;mero de celular :
			</logic:notEqual>
			</td>
		    
			<td>
			<bean:write name="RecargaCelularesForm" property="numeroCelular"/>
			</td>
          </tr>
          <tr >
            
            <td id="nombredato">
				<logic:equal name="existeAlias" value="no">
               		Esta recarga se guardar&aacute como:
               	</logic:equal>
               	<logic:notEqual name="existeAlias" value="no">
               		El alias a recargar es:
               	</logic:notEqual>
			</td>
            <td>
            	<logic:equal name="existeAlias" value="si">
               		<html:text property="alias_recarga" size="10" maxlength="10" readonly="true"/>
               	</logic:equal>
               	<logic:notEqual name="existeAlias" value="si">
               <html:text property="alias_recarga" size="10" maxlength="10"/>
               	</logic:notEqual>
            </td>
            
          </tr>
		  </table>
          </td>
          </tr>
        </table>
          
    
          <table border="0" cellpadding="0" cellspacing="0" id="datostabuladosformularios" >
            <tr>
              <td colspan="2" id="titulosdatos">Cargo a Cuenta</td>
            </tr>
            <tr >
              <td width="22%" id="nombredato">Recargar con Cargo a : </td>
              <td width="45%"><bean:write name="RecargaCelularesForm" property="nombreCuentaCte"/></td>
            </tr>
                  <tr >
              <td id="nombredato">Disponible para recargas : </td>
              <td class="importante"><script>document.write(cambiaComaPunto('<fmt:formatNumber value="${montoDisponible}" pattern="$#,##0"/>')); </script></td>
            </tr>
          <logic:present name="vigenciaRecarga"> 
          	 <tr >
	           <td width="22%" id="nombredato">Vigencia de Recarga: </td>
               <td ><script>document.write('<bean:write name="vigenciaRecarga" />');</script></td>
          	 </tr>
          </logic:present>
	          </table>

	    	  <logic:present name="data"> 
			  	<table border="0" cellpadding="0" cellspacing="0" id="datostabuladosformularios" >
			            <tr>
			              <td colspan="2" id="titulosdatos">Segunda Clave</td>
			            </tr>
						<tr>
							<td  valign="top" width="22%" align="center">
									<img id="qr" width="160" height="160" />				
							</td>
							<td valign="top" width="45%">
								<table border="0" cellpadding="0" cellspacing="0"  id="impresionCodigo" >
							 		<tr>
							 			<td style="font-size:11px;font-family:Arial,Helvetica,sans-serif;padding-right:5px" align="justify"><br/>La recarga a&uacute;n no se ha completado, revise la informaci&oacute;n y confirme que corresponde a la 
							 			recarga que usted desea realizar. <b>Escanee el c&oacute;digo QR con su aplicaci&oacute;n MultiPass M&oacute;vil</b>, si los datos concuerdan con los de la transferencia, 
							 			ingrese el c&oacute;digo num&eacute;rico en el campo de texto y presione el bot&oacute;n Recargar.</td>
							 		<tr/>
							 		<tr>
							 			<td style="font-size:11px;font-family:Arial,Helvetica,sans-serif"  align="justify">
							 			    <br/>
							 				C&oacute;digo de Confirmaci&oacute;n&nbsp;
							 				<input id="confirmationCode" maxlength="7" type="password" name="confirmationCode" onkeypress="return isNumberKey(event)" >
	
										<br/>
										&nbsp;
							 			</td>
							 		</tr>
							 	</table>
							 </td>
						</tr>
					</table> 
			</logic:present>
			
			<logic:present name="EntrustSoftToken"> 
				<table border="0" cellpadding="0" cellspacing="0" id="datostabuladosformularios" >
					<tr>
						<td id="titulosdatos">Segunda Clave</td>
					</tr>
					<tr>
						<td valign="top">
							<table border="0" cellpadding="0" cellspacing="0" class="ancho_completo">
								<tr><td>
									<div id="layerOnline">
										<div class="entrust_container uno">
											<div class="col img"></div>
											<div class="col texto1"><bean:message key="entrustSoftToken.contenido.textoPrincipalOnline"/></div>
											<div class="mensaje"><bean:message key="entrustSoftToken.contenido.sinInternet"/> <a href="javascript:limpiaIntervalo();generarQRActionGenerico();mostrarBotonRecarga();"><bean:message key="entrustSoftToken.contenido.clickAqui"/></a></div>
										</div>
									</div>
									<div id="errorLayerOnline" class="error_layer_online"><strong><bean:message key="entrustSoftToken.contenido.transaccionCancelada"/></strong><br/></div>
									<div id="layerOffline" style="display:none">
										<div class="entrust_container uno">
											<div id="loading_gen"></div>
											<iframe name="iframe_offline" id="iframe_offline" width="100%" height="100%" frameborder="0" marginwidth="0" marginheight="0" onload="ocultaImgLoading();"> </iframe>
											<input type="hidden" id="confirmationCode" name="confirmationCode">
										</div>
									</div>
								</td></tr>
							</table>
						</td>
					</tr>
				</table> 
			</logic:present>
			
			<logic:present name="EntrustToken"> 
				<table border="0" cellpadding="0" cellspacing="0" id="datostabuladosformularios" >
					<tr>
						<td id="titulosdatos">Segunda Clave</td>
					</tr>
					<tr>
						<td valign="top">
							<table border="0" cellpadding="0" cellspacing="0" class="ancho_completo" >
								<tr><td>
									<div class="entrust_container opciones">
										<p class="titMultipass"><bean:message key="entrustToken.contenido.tituloMultipass"/></p>
										
										<div class="multipass">
											<div class="img"></div>
											<div class="ingreso">
												<div class="label" id="label"><bean:message key="entrustToken.contenido.subtitulo"/></div>
												<input type="password" id="pass" name="confirmationCode" placeholder="<bean:message key="entrustToken.contenido.placeholder"/>" maxlength="6">
												<div id="ver"></div>
											</div>
											<div class="paso"><bean:message key="entrustToken.contenido.cantCaracteres"/></div>
										</div>
										
										<div id="loading"></div>
									</div>
								</td></tr>
							</table>
						</td>
					</tr>
				</table> 
			</logic:present>
     	
      </td>
          	<!-- Fin integracion SafeSigner -->
        <td id="tdbordeder">&nbsp;</td>
      </tr>
      <tr>
        <td><img src="images/curvaabajoizquierda.gif" width="6" height="6" /></td>
        <td id="bordebajotabla"><img src="images/bordeblancoabajo.gif" width="6" height="6" /></td>
        <td><img src="images/curvaabajoderecha.gif" width="6" height="6" /></td>
      </tr>
    </table> <!-- Fin table id="borde" -->
    
	<table border="0" cellspacing="0" cellpadding="0" id="botonera">
    <tr>
          <td><c:if test="${tieneSaldo == '1'}" >
                  <a href="/bcinetwls/recargacelulares/obtieneOperadoras.do"><img src="images/btn_volver.gif" title="volver" alt="volver" width="70" height="21" border="0" /></a>
                  <img src="images/btn_recarga.gif" id="btn1" border="0" title="Continuar" alt="Continuar" class="boton_sin_accion">
                  <img src="images/btn_recarga.gif" id="btn2" border="0" title="Continuar" alt="Continuar" style="display:none;margin-left:10px;cursor:hand;color: #BBC5CA;" onClick="javascript:carga_celular();" ondblclick="javascript:;"/>
                </c:if>
                  <c:if test="${tieneSaldo == '2'}" > &nbsp;&nbsp;<a href="/bcinetwls/recargacelulares/obtieneOperadoras.do"><img src="images/btn_volver.gif" title="volver" alt="volver" width="70" height="21" border="0" /></a>
                      <div class="importante Estilo1"> Ud. no tiene Saldo disponible para realizar la recarga.</div>
             </c:if>
		</td>
        </tr>
  </table>    
  </td>
  </tr>
</table>
 </html:form>
 </body>
 </html:html>
<%--
/*****************************************************************************************************************************************************
* Archivo:          confirmaRecarga.jsp
* Descripci�n:      Muestra los datos con los cuales se realizar� 
*                   la recarga del celular, para que le cliente los revise y 
*                   confirme que desea hacer la recarga con estos datos.
* Path:             /bcinetwls/recargacelulares
* " Todos los derechos reservados por Banco de Cr�dito e Inversiones."
*
* Historia de cambios
* -------------------
*  versi�n   fecha       autor                                 cambios
* =======   ==========  =================================      ======================================================================================
*  1.0       ??/??/????  desconocido                            Versi�n inicial.
*  1.1       11/12/2006  Kendru Estrada R. (TINet)              Se agrega el par�metro paso para que el 
*  1.2       21/04/2008  Andr�s Mor�n O. (SEnTRA)               Se agrega despliegue del monto abono adicional y otras promociones 
*                                                               sistema solicite la segunda clave cuando corresponda.
*  1.3       20/04/2009  Jessica Carroza E. (SEnTRA)            Se elimina mensaje (saldo + l�nea de sobregiro) de 
*                                                               Disponible para recargas.
*  1.4       26/07/2011  Rodrigo Gonz�lez M. (Imagemaker IT)    Se eliminan los input ocultos 'celular11', 'celular12',
*                                                               'celular21' y 'celular22', y todas las referencias
*                                                               a ellos en la presente JSP. Estas referencias e
*                                                               imputs se reemplazan por 'numeroCelular' y
*                                                               'reNumeroCelular' para tener el n�mero celular del
*                                                               cliente en un s�lo campo y no en 2 (c�digo celular
*                                                               y resto del n�mero).          
*   1.5      28/02/2013  Victor Hugo Enero (Orand)                   Integraci�n con SafeSigner                                          
*   1.6      18/04/2013  Victor Hugo Enero (Orand)              Se aumenta en 160 el QR, se hace ajuste en la mensajeria
*   1.7      06/02/2017  Luis Lopez Alamos (SEnTRA) - Christian Moraga Roldan (Ing. Soft. BCI) Se parametrizan valores en duro, se cambia orden de documentacion del jsp
*                                                            se agrega logica para cambio visual en caso de modificar el tipo de operadora de recarga.
*   1.8      30/08/2017  Marcelo Fuentes (SEnTRA) - Ricardo Carrasco (Ing. Soft. BCI) Se integra la autenticacion con Entrust.
****************************************************************************************************************************************************/
--%>
