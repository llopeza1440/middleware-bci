package cl.bci.aplicaciones.seguridad.autenticacion.mb;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import wcorp.infraestructura.seguridad.autenticacion.to.OpcionalidadSegundaClaveTO;
import wcorp.serv.autenticacion.ServiciosAutenticacion;
import wcorp.serv.autenticacion.ServiciosAutenticacionHome;
import wcorp.serv.seguridad.SeguridadException;
import wcorp.serv.seguridad.ValidaPinCorp;
import wcorp.util.EnhancedServiceLocator;
import wcorp.util.GeneralException;
import wcorp.util.StringUtil;
import wcorp.util.mensajeria.ControlErrorMsg;

import cl.bci.aplicaciones.cliente.mb.ClienteMB;
import cl.bci.aplicaciones.seguridad.autenticacion.utils.DefineDespacho;
import cl.bci.aplicaciones.seguridad.autenticacion.utils.SegundaClave;
import cl.bci.infraestructura.web.portal.mb.PortalMB;
import cl.bci.infraestructura.web.seguridad.login.AbstractLoginController;
import cl.bci.infraestructura.web.seguridad.mb.SesionMB;
import cl.bci.infraestructura.web.seguridad.segundaclave.SegundaClaveUtil;
import cl.bci.infraestructura.web.seguridad.segundaclave.mb.JerarquiaClavesModelMB;
import cl.bci.infraestructura.web.seguridad.segundaclave.to.CamposDeLlaveTO;
import cl.bci.infraestructura.web.seguridad.segundaclave.to.EstadoSegundaClaveTO;
import cl.bci.infraestructura.web.seguridad.segundaclave.to.ItemDeLlaveTO;
import cl.bci.infraestructura.web.seguridad.segundaclave.to.LlaveSegundaClaveTO;
import cl.bci.infraestructura.web.seguridad.to.CanalTO;
import cl.bci.infraestructura.web.seguridad.to.DatosAutenticacionTO;
import cl.bci.infraestructura.web.seguridad.to.UsuarioConectadoTO;
import cl.bci.infraestructura.web.seguridad.util.ConectorStruts;

/**
 * Support ManagedBean
 * 
 * <pre><b>Login Personas</b>
 *
 * Componente encargado de autenticar un cliente en banca M�vil.
 *
 * </pre>
 *
 * Registro de versiones:
 * <ul>
 * <li>1.0 15/04/2014 Sergio Cuevas D�az. (SEnTRA): Versi�n inicial.</li>
 * <li>1.1 23/03/2015 Gonzalo Villagr�n (SEnTRA): Se modifica el m�todo {@link #getCamposLlave()} para que 
 *                    obtenga la fechaHora desde sesi�n.
 * </li>
 * <li>1.2 06/10/2015 Pedro Carmona Escobar (SEnTRA) - Rodrigo Briones (Ing. Soft. BCI): Se modifican los m�todos
                       {@link #debeSolicitarMultipass(String, String, CanalTO, EstadoSegundaClaveTO) y 
 					  {@link #despachaValidacionSegundaClave(String, String, CanalTO)}. Adem�s, se normalizan los
					  logs de todos los m�todos de la clase. </li>
 * <li>2.0 10/07/2017 Luis L�pez Alamos (SEnTRA) - Ricardo Carrasco C�ceres (Ing. Soft. BCI): Se modifica m�todo {@link #despachaValidacionSegundaClave(String, String, CanalTO)}}</li>
 * </ul>
 * <b>Todos los derechos reservados por Banco de Cr�dito e Inversiones.</b>
 */

@ManagedBean
@RequestScoped
public class LoginMovilWebControllerMB extends AbstractLoginController implements Serializable {

    /**
     * Atributo SerialVersion.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Constante que identifica el servicio para login. 
     */
    private static final String ID_SAFESIGNER = "safesigner";
    
    /**
     * Constante que identifica el servicio para login. 
     */
    private static final String ID_TOKEN = "multipass";

    /**
     * Constante que identifica el servicio para login. 
     */
    private static final String SERVICIO_LOGIN = "Login";
    
    /**
    /**
     * Constante que identifica el catch user login. 
     */
    private static final String CATCH_USER_LOGIN = "USERLOGIN";

    /**
     * Constante que identifica el catch user login 901. 
     */
    private static final String CATCH_USER_LOGIN_901 = "USERLOGIN-901";
    
    /**
     * Constante que identifica el  catch user login 905. 
     */
    private static final String CATCH_USER_LOGIN_905 = "USERLOGIN-905";
    
    /**
     * Constante que identifica el  catch user login 907. 
     */
    private static final String CATCH_USER_LOGIN_907 = "USERLOGIN-907";
    
    /**
     * Constante que identifica el jsp a retornar. 
     */
    private static final String RETURN_ERROR = "/movilwls/common/Error.jsp";

    /**
     * Constante que identifica el jsp a retornar. 
     */
    private static final String RETURN_CON_QR = "/movilwls/token/autenticacionToken.jsp?qr=true";
    
    /**
     * Constante que identifica el jsp a retornar. 
     */
    private static final String RETURN_SIN_QR = "/movilwls/token/autenticacionToken.jsp?qr=false";
    
    /**
     * Constante que identifica el jsp a retornar. 
     */
    private static final String RETURN_CONFIGURAR_TOK = "/seguridadwls/ConfigurarToken?paso=inicioActivacionWeb";
    
    /**
     * Constante que identifica el  catch mensaje definici�n. 
     */
    private static final String CATCH_MENSAJE_DEF = "Canal sin definici�n";
    
    /**
     * Constante que identifica el  catch mensaje definici�n. 
     */
    private static final String CATCH_MENSAJE_VAL = "Valores Insuficientes";
    
    /**
     * Constante que identifica el  catch mensaje tipo. 
     */
    private static final String CATCH_MENSAJE_TIPO = "Tipo de Usuario Mal definido";

    /**
     * Constante que identifica canal string. 
     */
    private static final String CANAL_STRING = "canal";

    /**
     * Constante que identifica nombre string. 
     */
    private static final String NOMBRE_STRING = "nombre";

    /**
     * Constante que identifica la fecha hora. 
     */
    private static final String FECHA_STRING = "fechaHora";

    /**
     * Constante que identifica el formato de fecha string. 
     */
    private static final String FORMATO_FECHA_STRING = "yyyyMMddHHmm";
    
    /**
     * Pin Id de Canal BCIMovil.    
     */
    private static final int PIN_ID_BCI_MOVIL = 110;

    /**
     * Pin Id de Canal TBANCMovil.
     */
    private static final int PIN_ID_TBANC_MOVIL = 100;

    /**
     * Atributo Logger.
     */
    private transient Logger logger = (Logger)Logger.getLogger(LoginMovilWebControllerMB.class);

    /**
     * Atributo que representa la url de despacho, tras los procesos de autenticaci�n.
     */
    private String despacho;

    /**
     * Atributo que representa el mensaje de error, en caso de ocurra alguno.
     */
    private String mensajeError;

    /**
     * Atributo que representa el rut del usuario que est� ingresando a BCI.
     */
    private long rutUsuario;

    /**
     * Atributo que representa el dv del usuario que est� ingresando a BCI.
     */
    private char dvUsuario;

    /**
     * Ejb servicios de autenticaci�n.
     */
    private ServiciosAutenticacion serviciosAutenticacion;

    /**
     * Ejb servicios de autenticaci�n.
     */
    private String idTipoSegundaClave;

    /**
     * Atributo representa Managed Bean inyectado PortalMB.
     */
    @ManagedProperty(value = "#{portalMB}")
    private PortalMB portalMB;

    /**
     * Atributo representa Managed Bean inyectado SesionMB.
     */
    @ManagedProperty(value = "#{sesionMB}")
    private SesionMB sesionMB;

    /**
     * Atributo representa Managed Bean inyectado ClienteMB.
     */
    @ManagedProperty(value = "#{clienteMB}")
    private ClienteMB clienteMB;

    /**
     * Atributo representa Managed Bean inyectado JerarquiaClavesModelMB.
     */
    @ManagedProperty(value = "#{jerarquiaClavesModelMB}")
    private JerarquiaClavesModelMB jerarquiaClavesModelMB;

    /**
     * M�todo que realiza la validaci�n de ingreso de clave del cliente, en caso de
     * que el identificador de pin sea de tipo MOVIL.
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 15/04/2014 Sergio Cuevas D�az. (SEnTRA): Versi�n inicial.</li>
     * <li>1.1 06/10/2015 Pedro Carmona Escobar (SEnTRA) - Rodrigo Briones (Ing. Soft. BCI): Se normalizan log.</li>
     * </ul>
     * @param datosAutenticacion datos de autenticaci�n.
     * @return despacho url con la vista a despachar.
     * @since 1.0
     */
    public String validaClaveInternet(DatosAutenticacionTO datosAutenticacion) {
        if(getLogger().isEnabledFor(Level.INFO)){
            getLogger().info("[validaClaveInternet][BCI_INI]");
        }
        this.rutUsuario = datosAutenticacion.getRut();
        this.dvUsuario =  datosAutenticacion.getDv();
        String canalId = datosAutenticacion.getCanal();
        String clave = datosAutenticacion.getClave();
        String servicioInicial = datosAutenticacion.getServicioInicial();
        if(getLogger().isDebugEnabled()){
            getLogger().debug("[validaClaveInternet] [" + rutUsuario + "] dvUsuario[" + dvUsuario
                    + "] canalID[" + canalId + "] clave[" + clave + "] servicioInicial[" + servicioInicial);
        }
        String pin          = null;
        String userIdrut    = null;
        String userIdAux    = null;
        CanalTO canalCliente = null;
        try{
            this.registraLogIngreso(String.valueOf(rutUsuario), canalId, servicioInicial);
            canalCliente = this.iniciaInfraestructuraCanal(canalId);
            if(getLogger().isDebugEnabled()){
                getLogger().debug("[validaClaveInternet] [" + rutUsuario + "] canalCliente " + canalCliente );
            }
            sesionMB.setIdServicio(servicioInicial);
            int pid = Integer.parseInt(canalCliente.getPinID());
            if(getLogger().isDebugEnabled()){
                getLogger().debug("[validaClaveInternet] [" + rutUsuario + "] pid " + pid );
            }
            switch(pid){
            case PIN_ID_BCI_MOVIL: 
            case PIN_ID_TBANC_MOVIL:
                userIdrut = String.valueOf(rutUsuario);
                userIdAux = String.valueOf(dvUsuario);
                pin = clave;
                this.validaRut(userIdrut, userIdAux);
                break;
            default:
                throw new wcorp.util.GeneralException("ESPECIAL", CATCH_MENSAJE_DEF);
            }
            if ("".compareTo(userIdrut) == 0 || "".compareTo(pin) == 0){
                throw new wcorp.util.GeneralException("PARAM", CATCH_MENSAJE_VAL);
            }
            
            if(getLogger().isDebugEnabled()){
                getLogger().debug("[validaClaveInternet] [" + rutUsuario + "] Inicio iniciaSesionBciPersonas");
            }
            this.iniciaSesionBciPersonas(canalId, userIdrut, userIdAux);
            if(getLogger().isDebugEnabled()){
                getLogger().debug("[validaClaveInternet] [" + rutUsuario + "] Retorno iniciaSesionBciPersonas");
                getLogger().debug("[validaClaveInternet] [" + rutUsuario + "] Inicio registraServicioInicial");
            }
            this.registraServicioInicial(servicioInicial);
            if(getLogger().isDebugEnabled()){
                getLogger().debug("[validaClaveInternet] [" + rutUsuario + "] Retorno registraServicioInicial");
            }

            clienteMB.setDatosBasicos();
            UsuarioConectadoTO usuario = new UsuarioConectadoTO();
            if(getLogger().isDebugEnabled()){
                getLogger().debug("[validaClaveInternet] [" + rutUsuario + "] idUsuarioConectado["
                        +userIdrut + userIdAux+"]");
            }
            usuario.setIdUsuarioConectado(userIdrut + userIdAux);
            sesionMB.setUsuarioConectado(usuario);
            if(getLogger().isDebugEnabled()){
                getLogger().debug("[validaClaveInternet] [" + rutUsuario + "] antes del validaPinCorporativo");
            }
            ValidaPinCorp vp = this.validaPinCorporativo(canalCliente, Long.parseLong(userIdrut), userIdAux, pin);
            if(getLogger().isDebugEnabled()){
                getLogger().debug("[validaClaveInternet] [" + rutUsuario + "] vp ["+vp+"]");
            }
            
            if(vp == null){
                if(getLogger().isEnabledFor(Level.INFO)){
                    getLogger().info("[validaClaveInternet][BCI_FINOK][" + rutUsuario + "] NOK return["
                            + despacho + "]");
                }
                return despacho;
            }
            if(getLogger().isDebugEnabled()){
                getLogger().debug("[validaClaveInternet] [" + rutUsuario + "] Inicio seteaEnSessionBCI");
            }
            this.seteaEnSessionBCI(vp);
            if(getLogger().isDebugEnabled()){
                getLogger().debug("[validaClaveInternet] [" + rutUsuario + "] Retorno seteaEnSessionBCI");
            }
            if (vp.tipoUsuario == 'M' || vp.tipoUsuario == 'A'){
                vp.tipoUsuario = 'T';
            }

            if (!this.despachaCambiarClave(vp, pin)){
                if(getLogger().isDebugEnabled()){
                    getLogger().debug("[validaClaveInternet] [" + rutUsuario + "] Debe validar Segunda clave");
                }
                if (this.despachaValidacionSegundaClave(userIdrut, userIdAux, canalCliente)){
                    if(getLogger().isEnabledFor(Level.INFO)){
                        getLogger().info("[validaClaveInternet][BCI_FINOK][" + rutUsuario + "] pide segunda clave ["+ despacho + "]");
                    }
                    return despacho;
                } 
                else{
                    if(getLogger().isDebugEnabled()){
                        getLogger().debug("[validaClaveInternet] [" + rutUsuario + "] despachaCambiarClave TRUE");
                    }
                    String tipoUserParam = String.valueOf(vp.tipoUsuario);
                    if (tipoUserParam == null || (tipoUserParam.compareTo("T")!=0 
                            && tipoUserParam.compareTo("A")!=0 && tipoUserParam.compareTo("E")!=0)) {
                        throw new wcorp.util.GeneralException("PARAM", CATCH_MENSAJE_TIPO);
                    }
                    DefineDespacho defineDespacho = new DefineDespacho();
                    if(getLogger().isDebugEnabled()){
                        getLogger().debug("[validaClaveInternet] [" + rutUsuario 
                                + "] Invocando a define despacho!");
                    }
                    despacho = defineDespacho.defineDespacho(String.valueOf(rutUsuario), canalCliente);
                    if (idTipoSegundaClave!=null){
                        despacho = despacho + "?idTipoSegundaClave=" + idTipoSegundaClave;
                    }
                }
                ConectorStruts.setAttribHttp("sessionBci", ConectorStruts.getSessionBCI());
                journalizaLogin(Long.parseLong(userIdrut), userIdAux, Long.parseLong(userIdrut), 
                        userIdAux, SUB_COD_OK, null, canalCliente.getNombre(), ConectorStruts.getRemoteAddr());
                sesionMB.activar();
                if(getLogger().isEnabledFor(Level.INFO)){
                    getLogger().info("[validaClaveInternet][BCI_FINOK] [" + rutUsuario + "]return[" 
                            + despacho + "]");
                }
                return despacho;
            }
        }
        catch(wcorp.serv.clientes.ClientesException ex){
            if(getLogger().isEnabledFor(Level.ERROR)){
                getLogger().error("[validaClaveInternet][BCI_FINEX][ClientesException][" + rutUsuario + "]" 
                        + ex.getMessage(), ex);
            }
            return this.controlLoginClientesException(ex);
        }
        catch(SeguridadException ex){ 
            if ((ex.getCodigo().equals(CATCH_USER_LOGIN_901)) || (ex.getCodigo().equals(CATCH_USER_LOGIN_905))
            		|| (ex.getCodigo().equals(CATCH_USER_LOGIN_907))){
                ex = new wcorp.serv.seguridad.SeguridadException(CATCH_USER_LOGIN);
            }
            if(getLogger().isEnabledFor(Level.ERROR)){
                getLogger().error("[validaClaveInternet][SeguridadException][" + rutUsuario + "]" 
                        + ex.getMessage(), ex);
            }
            ex = (SeguridadException)ControlErrorMsg.modificaCodigoError((Exception)ex,canalCliente.getCanalID());
            if(getLogger().isEnabledFor(Level.ERROR)){
                getLogger().error("[validaClaveInternet][SeguridadException][" + rutUsuario + "]" 
                        + ex.getMessage(), ex);
            }
            FacesContext context = FacesContext.getCurrentInstance();
            HttpServletRequest request= (HttpServletRequest)context.getExternalContext().getRequest();
            request.setAttribute("javax.servlet.jsp.jspException", ex);
            
            if(getLogger().isEnabledFor(Level.ERROR)){
                getLogger().error("[validaClaveInternet][BCI_FINEX][SeguridadException][" + rutUsuario + "]" 
                        + RETURN_ERROR);
            }
            return RETURN_ERROR;
        }
        catch(GeneralException ex){
            if(getLogger().isEnabledFor(Level.ERROR)){
                getLogger().error("[validaClaveInternet][GeneralException][" + rutUsuario + "]" 
                        + ex.getMessage(), ex);
            }
            ex = (GeneralException)ControlErrorMsg.modificaCodigoError(
                    (Exception)ex, canalCliente.getCanalID());
            if(getLogger().isEnabledFor(Level.ERROR)){
                getLogger().error("[validaClaveInternet][GeneralException][" + rutUsuario + "]" 
                        + ex.getMessage(), ex);
            }
            FacesContext context = FacesContext.getCurrentInstance();
            HttpServletRequest request= (HttpServletRequest)context.getExternalContext().getRequest();
            request.setAttribute("javax.servlet.jsp.jspException", ex);
            
            if(getLogger().isEnabledFor(Level.ERROR)){
                getLogger().error("[validaClaveInternet][BCI_FINEX][GeneralException][" + rutUsuario + "]" 
                        + RETURN_ERROR);
            }
            return RETURN_ERROR;

        }
        catch (Exception ex){
            if(getLogger().isEnabledFor(Level.ERROR)){
                getLogger().error("[validaClaveInternet][Exception][" + rutUsuario + "]" 
                        + ex.getMessage(), ex);
            }
            ex = ControlErrorMsg.modificaCodigoError(ex, canalCliente.getCanalID());
            if(getLogger().isEnabledFor(Level.ERROR)){
                getLogger().error("[validaClaveInternet][Exception][" + rutUsuario + "]" 
                        + ex.getMessage(), ex);
            }
            FacesContext context = FacesContext.getCurrentInstance();
            HttpServletRequest request= (HttpServletRequest)context.getExternalContext().getRequest();
            request.setAttribute("javax.servlet.jsp.jspException", ex);
            
            if(getLogger().isEnabledFor(Level.ERROR)){
                getLogger().error("[validaClaveInternet][BCI_FINEX][Exception][" + rutUsuario + "]" 
                        + RETURN_ERROR);
            }
            return RETURN_ERROR;
        }
        if(getLogger().isEnabledFor(Level.INFO)){
            getLogger().info("[validaClaveInternet][BCI_FINOK][" + rutUsuario + "]return["+ despacho + "]");
        }
        return despacho;
    }

    /**
     * M�todo que permite despachar la validaci�n de segunda clave, ya sea SafeSigner o Multipass.
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 15/04/2014 Sergio Cuevas D�az. (SEnTRA): Versi�n inicial.</li>
     * <li>1.1 06/10/2015 Pedro Carmona Escobar (SEnTRA) - Rodrigo Briones (Ing. Soft. BCI): Se normalizan log y se corrige
     *                               obtenci�n de los MetodosAutenticacion. Por �ltimo, se incorpora el par�metros 'estadoSegundaClave'
     *                               a la llamada del m�todo {@link #debeSolicitarMultipass(String, String, CanalTO, EstadoSegundaClaveTO)}.</li>
     * <li>2.0 10/07/2017 Luis L�pez Alamos (SEnTRA) - Ricardo Carrasco C�ceres (Ing. Soft. BCI): Se agrega seteo de valor null a llamada de metodo generarLLave, puesto que
     * la implementaci�n actual no contempla modificaciones sobre sevicios llamados desde REST.</li>
     * </ul>
     * @param userIdrut rut usuario.
     * @param userIdAux dv usuario.
     * @param canalCliente canal.
     * @throws SeguridadException en caso de error.
     * @throws Exception en caso de error.
     * @return boolean determina si solicita 2da clave.
     * @since 1.0
     */
    protected boolean despachaValidacionSegundaClave(String userIdrut,
            String userIdAux, CanalTO canalCliente) throws SeguridadException,
            Exception{
        if(getLogger().isEnabledFor(Level.INFO)){
            getLogger().info("[despachaValidacionSegundaClave][BCI_INI] ["+userIdrut+"]");
        }
        try{
            EstadoSegundaClaveTO estadoSegundaClave = jerarquiaClavesModelMB.estadoSegundaClave(SERVICIO_LOGIN, 
                    SegundaClaveUtil.getUsuarioSegundaClave());
            if(getLogger().isDebugEnabled()){
                getLogger().debug("[despachaValidacionSegundaClave] ["+userIdrut 
                        +"] estadoSegundaClave.getSegundaClaveEnum(). " 
                        + estadoSegundaClave.getSegundaClaveEnum());
            }
            if(estadoSegundaClave.getSegundaClaveEnum() == null){
                if(getLogger().isEnabledFor(Level.INFO)){
                    getLogger().info("[despachaValidacionSegundaClave][BCI_FINOK]["+userIdrut
                            +"] No hay segunda clave v�lida.");
                }
                return false;
            }
            else{
                switch (estadoSegundaClave.getSegundaClaveEnum()) {
                case SAFESIGNER:
                    if(getLogger().isDebugEnabled()){
                        getLogger().debug("[despachaValidacionSegundaClave] ["+userIdrut+"] SafeSigner.");
                    }
                    
                    if(getLogger().isEnabledFor(Level.INFO)){ 
                        getLogger().info("[despachaValidacionSegundaClave] metodosAutenticacion: "
                                + StringUtil.contenidoDe(estadoSegundaClave.getMetodosAutenticacion())); 
                    }
                    
                    ConectorStruts.getSessionBCI().setMetodosAutenticacion(estadoSegundaClave.
                            getMetodosAutenticacion());
                    OpcionalidadSegundaClaveTO opcionalidadSegundaClaveTO = new OpcionalidadSegundaClaveTO();
                    opcionalidadSegundaClaveTO.setCanal(canalCliente.getCanalID());
                    opcionalidadSegundaClaveTO.setRut(Long.valueOf(userIdrut));
                    opcionalidadSegundaClaveTO.setDv(userIdAux.charAt(0));
                    opcionalidadSegundaClaveTO.setIdSegundaClave(
                            String.valueOf(estadoSegundaClave.getSegundaClaveEnum()));
                    boolean safeSignerOpciona = serviciosAutenticacion().consultaOpcionalidadSegundaClave(
                            opcionalidadSegundaClaveTO);
                    if(getLogger().isDebugEnabled()){
                        getLogger().debug("[despachaValidacionSegundaClave] ["+userIdrut+"] safeSignerOpciona. "
                                +safeSignerOpciona);
                    }
                    if (safeSignerOpciona) {
                        if(getLogger().isDebugEnabled()){
                            getLogger().debug("[despachaValidacionSegundaClave] ["+userIdrut
                                    +"] estadoSegundaClave.isEstatus(). " +estadoSegundaClave.isEstatus());
                            getLogger().debug("[despachaValidacionSegundaClave] ["+userIdrut
                                    +"]estadoSegundaClave.getSegundaClaveEnum().isRequiereLlave(). "
                                    + estadoSegundaClave.getSegundaClaveEnum().isRequiereLlave());
                        }
                        if (estadoSegundaClave.isEstatus() 
                                && estadoSegundaClave.getSegundaClaveEnum().isRequiereLlave()) {
                            CamposDeLlaveTO camposDeLlave = getCamposLlave();
                            LlaveSegundaClaveTO llave = jerarquiaClavesModelMB.generarLlave(
                                    estadoSegundaClave.getSegundaClaveEnum(), SERVICIO_LOGIN, 
                                    SegundaClaveUtil.getUsuarioSegundaClave(), camposDeLlave, null);
                            if(getLogger().isDebugEnabled()){
                                getLogger().debug("[despachaValidacionSegundaClave] ["+userIdrut+"] llave. "
                                        +llave.toString());
                            }
                            String valorLlave = llave.getLlave().getClaveAlfanumerica();
                            if (getLogger().isDebugEnabled()) {
                                getLogger().debug("[despachaValidacionSegundaClave] ["+userIdrut+"] valorLlave: " 
                                        + valorLlave);
                            }
                            if (llave.isEstatus()) {
                                this.setDespacho(RETURN_CON_QR);
                            }
                            else {
                                estadoSegundaClave.setEstatus(false);
                            }
                        }
                    }
                    else{
                        idTipoSegundaClave = ID_SAFESIGNER;
                    }
                    if (getLogger().isEnabledFor(Level.INFO)){
                        getLogger().info("[despachaValidacionSegundaClave] ["+userIdrut+"] [BCI_FINOK] Return "
                                + safeSignerOpciona);
                    }
                    return safeSignerOpciona;
                case MULTIPASS:
                    if(getLogger().isEnabledFor(Level.INFO)){
                        getLogger().info("[despachaValidacionSegundaClave] ["+userIdrut+"][BCI_FINOK] return this.debeSolicitarMultipass.");
                    }
                    return this.debeSolicitarMultipass(userIdrut, userIdAux, canalCliente, estadoSegundaClave);
                default:
                    if (getLogger().isEnabledFor(Level.INFO)){
                        getLogger().info("[despachaValidacionSegundaClave] ["+userIdrut+"][BCI_FINOK] Return false");
                    }
                    return false;
                }
            }
        }
        catch (SeguridadException ex) {
            if(getLogger().isEnabledFor(Level.ERROR)){
                getLogger().error("[despachaValidacionSegundaClave][BCI_FINEX][SeguridadException]["+userIdrut+"]" 
                        + ex.getMessage(), ex);
            }
            throw ex;
        }
        catch (Exception e) {
            if(getLogger().isEnabledFor(Level.ERROR)){
                getLogger().error("[despachaValidacionSegundaClave][BCI_FINEX][Exception]["+userIdrut+"]" 
                        + e.getMessage(), e);
            }
            throw e;
        }
    }

    /**
     * M�todo que permite obtener una instancia del EJB ServiciosCliente.
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 15/04/2014 Sergio Cuevas D�az. (SEnTRA): Versi�n inicial.</li>
     * <li>1.1 06/10/2015 Pedro Carmona Escobar (SEnTRA) - Rodrigo Briones (Ing. Soft. BCI): Se normalizan log.</li>
     * </ul>
     * <p>
     * @return instancia del ejb ServiciosCliente.
     * @throws Exception en caso de error.
     * @since 1.0
     */
    private ServiciosAutenticacion serviciosAutenticacion() throws Exception {
        if(getLogger().isEnabledFor(Level.INFO)){
            getLogger().info("[serviciosAutenticacion][BCI_INI]");
        }
        if(serviciosAutenticacion == null){
            if(getLogger().isDebugEnabled()){
                getLogger().debug("[serviciosAutenticacion] Se creara instancia");
            }
            EnhancedServiceLocator locator = EnhancedServiceLocator.getInstance();
            ServiciosAutenticacionHome serviciosAutenticacionHome = 
                    (ServiciosAutenticacionHome) locator.getGenericService("wcorp.serv.autenticacion"
                            + ".ServiciosAutenticacion", ServiciosAutenticacionHome.class);
            serviciosAutenticacion = serviciosAutenticacionHome.create();
            if(getLogger().isDebugEnabled()){
                getLogger().debug("[serviciosAutenticacion] instancia creada");
            }
        }
        if(getLogger().isEnabledFor(Level.INFO)){
            getLogger().info("[serviciosAutenticacion][BCI_FINOK]");
        }
        return serviciosAutenticacion;
    }

    /**
     * M�todo que redirecciona a la vista autenticacionSegundaClave, para 
     * que el cliente ingrese segunda clave MultiPass*, previa validaci�n
     * de despacho hacia la segunda clave. 
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 15/04/2014 Sergio Cuevas D�az. (SEnTRA): Versi�n inicial.</li>
     * <li>1.1 06/10/2015 Pedro Carmona Escobar (SEnTRA) - Rodrigo Briones (Ing. Soft. BCI): Se normalizan log. Se incorpora el
     *                       par�metros estadoSegundaClave. Por �ltimo se corrige la forma de obtener MetodosAutenticacion.</li>
     * </ul>
     * </p>
     * @param userIdrut rut usuario.
     * @param userIdAux dv usuario.
     * @param canalCliente canal de ingreso del usuario.
     * @param estadoSegundaClave con el estado de segunda clave.
     * @return booleano en que caso que requiera multipass.
     * @since 1.0
     */
    public boolean debeSolicitarMultipass(String userIdrut, String userIdAux, CanalTO canalCliente, EstadoSegundaClaveTO estadoSegundaClave){
        if(getLogger().isEnabledFor(Level.INFO)){
            getLogger().info("[debeSolicitarMultipass][BCI_INI] ["+userIdrut+"]");
        }
        SegundaClave segundaClave = new SegundaClave();
        try {
            
            if(getLogger().isEnabledFor(Level.INFO)){ 
                getLogger().info("[debeSolicitarMultipass] metodosAutenticacion: "
                        + StringUtil.contenidoDe(estadoSegundaClave.getMetodosAutenticacion())); 
            }
            
            ConectorStruts.getSessionBCI().setMetodosAutenticacion(estadoSegundaClave.getMetodosAutenticacion());
            segundaClave.asignarOpcionalidadDeTokenEnSesion(userIdrut, userIdAux, canalCliente.getCanalID(),
                    estadoSegundaClave.getGlosa());
            
            if(getLogger().isEnabledFor(Level.DEBUG)){
                getLogger().debug("[debeSolicitarMultipass] [" + this.obtenerRutUsuario()
                        + "] Verifica si debe solicitar Token");
            }
            if (segundaClave.debeValidarToken(this.obtenerRutUsuario(), canalCliente.getCanalID(), 
            		estadoSegundaClave.getMetodosAutenticacion()) 
            		&& (ConectorStruts.getAttribBCI("permitirAutentificacion") != null)) {
                if(getLogger().isEnabledFor(Level.DEBUG)){
                    getLogger().debug("[debeSolicitarMultipass] [" + this.obtenerRutUsuario() 
                            + "] despachando a autenticacionSegundaClave");
                }
                this.setDespacho(RETURN_SIN_QR);
                if (getLogger().isEnabledFor(Level.INFO)){
                    getLogger().info("[debeSolicitarMultipass] [BCI_FINOK] Return true a "+ this.getDespacho());
                }
                return true;
            }
            else if (segundaClave.validarTokenAutorizado(this.obtenerRutUsuario(), 
            		estadoSegundaClave.getMetodosAutenticacion())) {
                if(getLogger().isEnabledFor(Level.DEBUG)){
                    getLogger().debug("[debeSolicitarMultipass] [" + this.obtenerRutUsuario() 
                            + "] Validara Permiso Activacion Web");
                }
                if(segundaClave.validarPermisoActivacionWeb(canalCliente.getCanalID())){
                    if(getLogger().isEnabledFor(Level.DEBUG)){
                        getLogger().debug("[debeSolicitarMultipass] [" + this.obtenerRutUsuario() 
                                + "] Antes de activar por web");
                    }
                    ConectorStruts.getSessionBCI().autoriza();
                    this.getSesion().activar();
                    String urlDespacho = RETURN_CONFIGURAR_TOK;
                    if(getLogger().isEnabledFor(Level.DEBUG)){
                        getLogger().debug("[debeSolicitarMultipass] [" + this.obtenerRutUsuario() 
                                + "] despachando a ConfigurarToken " + urlDespacho);
                    }
                    this.setDespacho(urlDespacho);
                    if (getLogger().isEnabledFor(Level.INFO)){
                        getLogger().info("[debeSolicitarMultipass] [BCI_FINOK] Return true a "+ this.getDespacho());
                    }
                    return true;
                }
            }
        } 
        catch (NumberFormatException e) {
            if(getLogger().isEnabledFor(Level.ERROR)){
                getLogger().error("[debeSolicitarMultipass][NumberFormatException]" + e.getMessage(), e);
            }
        } 
        catch (SeguridadException e) {
            if(getLogger().isEnabledFor(Level.ERROR)){
                getLogger().error("[debeSolicitarMultipass][SeguridadException]" + e.getMessage(), e);
            }
        } 
        catch (GeneralException e) {
            if(getLogger().isEnabledFor(Level.ERROR)){
                getLogger().error("[debeSolicitarMultipass][GeneralException]" + e.getMessage(), e);
            }
        } 
        catch (Exception e) {
            if(getLogger().isEnabledFor(Level.ERROR)){
                getLogger().error("[debeSolicitarMultipass][Exception]" + e.getMessage(), e);
            }
        }
        if (getLogger().isEnabledFor(Level.INFO)) {
            getLogger().info("[debeSolicitarMultipass] [BCI_FINOK] ]Return false");
        }
        idTipoSegundaClave = ID_TOKEN;
        return false;
    }

    /**
     * M�todo que inserta datos al objeto CamposDeLlaveTO.
     *
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 15/04/2014 Sergio Cuevas D�az. (SEnTRA): Versi�n inicial.</li>
     * <li>1.1 23/03/2015 Gonzalo Villagr�n (SEnTRA): Se modifica el m�todo para que obtenga el valor del campo 
     *                     fechaHora, a env�ar al servicio SafeSigner, desde la sesi�n.
     * <li>1.1 06/10/2015 Pedro Carmona Escobar (SEnTRA) - Rodrigo Briones (Ing. Soft. BCI): Se normalizan log.</li>
     * </li>
     * </ul>
     * </p>
     * @return CamposDeLlaveTO objeto campos de llave.
     * @since 1.0
     */
    private CamposDeLlaveTO getCamposLlave(){
        if(getLogger().isEnabledFor(Level.INFO)){
            getLogger().info("[getCamposLlave][BCI_INI]");
        }
        CamposDeLlaveTO camposDeLlave = new CamposDeLlaveTO();
        int posicion = 0;

        String nombre = CANAL_STRING;
        String valor = sesionMB.getCanalTO().getNombre();
        ItemDeLlaveTO item = new ItemDeLlaveTO();
        item.setPosicion(posicion);
        item.setNombre(nombre);
        item.setValorString(valor);
        camposDeLlave.agregarCampo(item);
        posicion++;

        nombre = FECHA_STRING;
        DateFormat dateFormat = new SimpleDateFormat(FORMATO_FECHA_STRING);
        Date date = sesionMB.getFechaCreacion();
        valor = (dateFormat.format(date));
        item = new ItemDeLlaveTO();
        item.setPosicion(posicion);
        item.setNombre(nombre);
        item.setValorString(valor);
        camposDeLlave.agregarCampo(item);
        posicion++;

        nombre = NOMBRE_STRING;
        valor = clienteMB.getNombres() + " " + clienteMB.getApellidoPaterno() 
                + " " + clienteMB.getApellidoMaterno();
        item = new ItemDeLlaveTO();
        item.setPosicion(posicion);
        item.setNombre(nombre);
        item.setValorString(valor);
        camposDeLlave.agregarCampo(item);
        if(getLogger().isEnabledFor(Level.INFO)){
            getLogger().info("[getCamposLlave][BCI_FINOK]camposDeLlave["+camposDeLlave+"]");
        }
        return camposDeLlave;
    }

    protected void setMensajeError(String mensajeError) {
        this.mensajeError = mensajeError;

    }

    public long getRutUsuario() {
        return rutUsuario;
    }

    public void setRutUsuario(long rutUsuario) {
        this.rutUsuario = rutUsuario;
    }

    public char getDvUsuario() {
        return dvUsuario;
    }

    public void setDvUsuario(char dvUsuario) {
        this.dvUsuario = dvUsuario;
    }

    public String getDespacho() {
        return despacho;
    }

    public void setDespacho(String despacho) {
        this.despacho = despacho;
    }

    public String getMensajeError() {
        return mensajeError;
    }

    public PortalMB getPortalMB() {
        return portalMB;
    }

    public void setPortalMB(PortalMB portalMB) {
        this.portalMB = portalMB;
    }

    public SesionMB getSesionMB() {
        return sesionMB;
    }

    public void setSesionMB(SesionMB sesionMB) {
        this.sesionMB = sesionMB;
    }

    public ClienteMB getClienteMB() {
        return clienteMB;
    }

    public void setClienteMB(ClienteMB clienteMB) {
        this.clienteMB = clienteMB;
    }

    public JerarquiaClavesModelMB getJerarquiaClavesModelMB() {
        return jerarquiaClavesModelMB;
    }

    public void setJerarquiaClavesModelMB(JerarquiaClavesModelMB jerarquiaClavesModelMB) {
        this.jerarquiaClavesModelMB = jerarquiaClavesModelMB;
    }

    @Override
    protected PortalMB getPortal() {
        return portalMB;
    }


    @Override
    protected SesionMB getSesion() {
        return sesionMB;
    }

    /**
     * M�todo retorna String con rut completo del usuario.
     *
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 15/04/2014 Sergio Cuevas D�az. (SEnTRA): Versi�n inicial.</li>
     * </ul>
     * </p>
     * @return String con el rut del ususario.
     * @since 1.0
     */
    protected String obtenerRutUsuario() {
        return String.valueOf(rutUsuario);
    }

    /**
     * M�todo para loggear la clase.
     *
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 15/04/2014 Sergio Cuevas D�az. (SEnTRA): Versi�n inicial.</li>
     * </ul>
     * </p>
     * @return logger retorna el log.
     * @since 1.0
     */
    public Logger getLogger() {
        if (logger == null){
            logger = Logger.getLogger(this.getClass());
        }
        return logger;
    }

}