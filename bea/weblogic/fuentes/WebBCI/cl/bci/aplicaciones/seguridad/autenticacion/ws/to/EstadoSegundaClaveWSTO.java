package cl.bci.aplicaciones.seguridad.autenticacion.ws.to;

import java.io.Serializable;
import java.util.Arrays;

import cl.bci.infraestructura.web.seguridad.segundaclave.ws.to.TokenEntrustTOWSTO;

/**
 * WSTO
 * <br>
 * <b>EstadoSegundaClaveWSTO</b>
 * <br>
 * WSTO que representa el resultado de la consulta del estado de segunda clave de un cliente.
 * <br>
 * Registro de versiones:<ul>
 * <li> 1.0 18/06/2013  Yon Sing Sius (ExperimentoSocial): versi�n inicial.</li>
 * <li> 2.0 10/07/2017 Sergio Flores. (SEnTRA) - Ricardo Carrasco (Ing. Soft. BCI) : Por proyecto migracion
 *          plataforma autenticacion se agrega el atributo lista de dispositivos y se actualiza el metodo
 *          {@link #toString()}.</li>
 * </ul>
 * 
 * <p><b>Todos los derechos reservados por Banco de Cr�dito e Inversiones.</b></p>
 */
public class EstadoSegundaClaveWSTO implements Serializable {

	/** Numero Version. **/
	private static final long serialVersionUID = 1L;
	
	/**
	 * Estado de la segunda clave.
	 */
	private boolean status;
	
	/**
	 * Nombre del m�todo de segunda clave utilizado por el cliente. 
	 */
    private String nombre;
    
    /**
     * Indica si para una transacci�n es necesario generar llave. 
     */
    private boolean requiereLlave;
    
    /**
     * Descripci�n del resultado de la operaci�n.
     */
    private String glosa;
    
    /**
     * Atributo que almacena una lista de dispositivos del tipo TokenEntrustTO.
     */
    private TokenEntrustTOWSTO[] listaDispositivos;    
    
	public String getGlosa() {
		return glosa;
	}
	
	public void setGlosa(String glosa) {
		this.glosa = glosa;
	}
	
	public boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public boolean isRequiereLlave() {
		return requiereLlave;
	}
	public void setRequiereLlave(boolean requiereLlave) {
		this.requiereLlave = requiereLlave;
	}
	
    public TokenEntrustTOWSTO[] getListaDispositivos() {
        return listaDispositivos;
    }

    public void setListaDispositivos(TokenEntrustTOWSTO[] listaDispositivos) {
        this.listaDispositivos = listaDispositivos;
    }

	@Override
	public String toString() {
		StringBuffer builder = new StringBuffer();
		builder.append("EstadoSegundaClaveWSTO [status=");
		builder.append(status);
		builder.append(", nombre=");
		builder.append(nombre);
		builder.append(", requiereLlave=");
		builder.append(requiereLlave);
		builder.append(", glosa=");
		builder.append(glosa);
		builder.append(", listaDispositivos=");
		builder.append(Arrays.toString(listaDispositivos));
		builder.append("]");
		return builder.toString();
	}

}
