package cl.bci.infraestructura.web.seguridad.segundaclave.ws.to;

import java.io.Serializable;


/**
 * 
 * <b>EstadoActivacionTokenWSTO</b>
 * <p>
 * Representa estado del usuario si existe o no.
 * </p>
 * 
 * <p>
 * Registro de versiones:
 * </p>
 * 
 * <ul>
 * <li>1.0 10/07/2017 Sergio Flores (Sentra) - Ricardo Carrasco (Ing. Soft. BCI) : versi�n inicial.</li>
 * </ul>
 * 
 * <p>
 * <b>Todos los derechos reservados por Banco de Cr�dito e Inversiones.</b>
 * </p>
 * 
 */
public class EstadoActivacionTokenWSTO implements Serializable {

	/**
	 * N�mero de versi�n utilizado durante la serializaci�n.
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * Campo codigo de activacion.
	 */
	private String activationCode;
	/**
	 * Campo numero de serial.
	 */
	private String serialNumber;

	/**
	 * estado del resultado.
	 */
	private ResponseStatusWSTO resultado;
    
	
	public String getActivationCode() {
		return activationCode;
	}

	public void setActivationCode(String activationCode) {
		this.activationCode = activationCode;
	}

	public String getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

	public ResponseStatusWSTO getResultado() {
		return resultado;
	}

	public void setResultado(ResponseStatusWSTO resultado) {
		this.resultado = resultado;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		buffer.append("EstadoActivacionTokenWSTO [activationCode=");
		buffer.append(activationCode);
		buffer.append(", serialNumber=");
		buffer.append(serialNumber);
		buffer.append(", resultado=");
		buffer.append(resultado);
		buffer.append("]");
		return buffer.toString();
	}
}
