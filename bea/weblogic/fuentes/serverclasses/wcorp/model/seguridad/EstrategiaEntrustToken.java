package wcorp.model.seguridad;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import wcorp.serv.seguridad.SeguridadException;
import wcorp.util.TablaValores;

/**
 * <b>Clase que contiene la l�gica de Estrategia de Autenticaci�n Entrust Token F�sico</b>
 * <p>
 * Registro de versiones:
 * <ul>
 *  <li>1.0 10/07/2017 Luis L�pez Alamos (SEnTRA) - Ricardo Carrasco C�ceres (Ing. Soft. BCI): Versi�n Inicial.</li>
 * </ul>
 * <b>Todos los derechos reservados por Banco de Cr�dito e Inversiones.</b>
 * <p>
 */
public class EstrategiaEntrustToken extends EstrategiaAbstractaEntrust  {
     
    /**
     * Log de la clase.
     */
    private transient Logger logger = (Logger) Logger.getLogger(EstrategiaEntrustToken.class);

    /**
     * Metodo para consultarEstadoSegundaClave.
     * <p>
     * Registro de versiones: 
     * <ul>
     * <li>1.0 10/07/2017 Luis L�pez Alamos (SEnTRA) - Ricardo Carrasco (Ing. Soft. BCI): Version inicial.</li>
     * </ul>
     * <p> 
     * @param parametroEstrategiaSegundaClave parametros para ejecutar la consulta de estado.
     * @return EstadoSegundaClaveTO[] lista de token usuario.
     * @throws SeguridadException error lanzado por el metodo.
     * @since 1.0
     */
    public EstadoSegundaClaveTO consultarEstadoSegundaClave(ParametrosEstrategiaSegundaClaveTO parametroEstrategiaSegundaClave) throws SeguridadException {
        if (getLogger().isEnabledFor(Level.INFO)){
            getLogger().info("[consultarEstadoSegundaClave][BCI_INI] " + parametroEstrategiaSegundaClave);
        }
            EstadoSegundaClaveTO resultadoEstado = consultaEstadoDeSegundaClave(parametroEstrategiaSegundaClave, PrioridadSegundaClaveIdTO.ENTRUST_TOKEN);
            if (getLogger().isEnabledFor(Level.INFO)){
                getLogger().info("[consultarEstadoSegundaClave][BCI_FINOK] " + resultadoEstado);
            }
            return resultadoEstado;
        }

    
    /**
     * Implementaci�n nula de m�todo #{@link #generarLlave(ParametrosEstrategiaSegundaClaveTO)}, se realiza de esta forma ya que se decidi� no dejar
     * en clase abstracta para ser sobreescrito.
     * <p>
     * 
     * Registro de Versiones:
     * <ul>
     * <li>1.0 10/07/2017 Luis L�pez Alamos (SEnTRA) - Ricardo Carrasco C�ceres (Ing. Soft. BCI): Versi�n Inicial.</li>
     * </ul>
     * <P>
     * @param parametroEstrategiaSegundaClave sin implementaci�n concreta.
     * @return null sin implementaci�n concreta.
     * @throws SeguridadException error lanzado por el metodo.
     * @since 1.0
     */
    public LlaveSegundaClaveTO generarLlave(ParametrosEstrategiaSegundaClaveTO parametroEstrategiaSegundaClave) throws SeguridadException {
        return null;
    }

    /**
     * Metodo encargado de autenticar una transacci�n realizada mediante dispositivo Entrust F�sico. 'HARDTOKEN'
     * <p>
     * 
     * Registro de Versiones:
     * <ul>
     * <li>1.0 10/07/2017 Luis L�pez Alamos (SEnTRA) - Ricardo Carrasco Caceres (Ing. Soft. BCI): Version Inicial.</li>
     * </ul>
     * <P>
     * @param parametroEstrategiaSegundaClave clave ingresada por el cliente para autenticar.
     * @return String con el resultado
     * @throws SeguridadException excepcion lanzada por el metodo.
     * @since 1.0
     */
    public ResultadoOperacionSegundaClaveTO autenticar(ParametrosEstrategiaSegundaClaveTO parametroEstrategiaSegundaClave) throws SeguridadException {

        if (getLogger().isEnabledFor(Level.INFO)){
            getLogger().info("[autenticar][BCI_INI] " + parametroEstrategiaSegundaClave);
            }
            ResultadoOperacionSegundaClaveTO response = new ResultadoOperacionSegundaClaveTO();
            try {
            String grupoEntrustBCI = TablaValores.getValor(TABLA_DE_PARAMETROS, "pinToken", "desc") != null ? TablaValores.getValor(TABLA_DE_PARAMETROS, "pinToken", "desc") : "";
            String tipoSegundaClave = TablaValores.getValor(TABLA_DE_PARAMETROS, "modoEntrustFisico", "desc") != null ? TablaValores.getValor(TABLA_DE_PARAMETROS, "modoEntrustFisico", "desc") : "";
            if (grupoEntrustBCI.equalsIgnoreCase("") || tipoSegundaClave.equalsIgnoreCase("")){
                getLogger().info("[autenticar] Hubo un problema al leer datos de tabla de parametros");
                throw new IllegalArgumentException("ENTRUSTFRASE02");
            }
            parametroEstrategiaSegundaClave.getUsuarioSegundaClave().setGrupoClienteEntrust(grupoEntrustBCI);
            parametroEstrategiaSegundaClave.setTipoSegundaClave(tipoSegundaClave);
            serviciosSegundaClave = obtenerServiciosSegundaClave();
            response = serviciosSegundaClave.autenticarDesafioGenerico(parametroEstrategiaSegundaClave);
        } 
        catch(Exception e){
            if (getLogger().isEnabledFor(Level.ERROR)){
                getLogger().error("[autenticar][BCI_FINEX] Error al autenticar transacci�n:" + e.getMessage(), e);
            } 
                throw new SeguridadException(PREFIJO_ENTRUST + e.getMessage());
                }
                
        if (getLogger().isEnabledFor(Level.INFO)){
            getLogger().info("[autenticar][BCI_FINOK]");
            }
            return response;
        }

    /**
     * <p>
     * M�todo que obtiene el objeto getLogger().
     * </p>
     * 
     * Registro de versiones:
     * <ul>
     * <li>1.0 10/07/2017 Luis L�pez Alamos (SEnTRA) - Ricardo Carrasco C�ceres (Ing. Soft. BCI): Versi�n Inicial.</li>
     * </ul>
     * <p>
     * 
     * @return Logger Objeto de log
     * @since 1.0
     */
    private Logger getLogger() {
        if ( logger == null ) {
            logger = Logger.getLogger( this.getClass() );
        }
        return logger;
    }

}

