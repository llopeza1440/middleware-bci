package wcorp.recargacelulares.ui.helpers;

import java.util.Hashtable;

import org.apache.log4j.Logger;

import wcorp.bprocess.recargacelulares.RecargaCelularDelegate;
import wcorp.bprocess.seguridad.token.TokenDelegate;
import wcorp.model.seguridad.SessionBCI;
import wcorp.serv.cuentas.ListaCuentas;
import wcorp.util.GeneralException;
import wcorp.util.TablaValores;

/**
 * Clase con m�todos que permiten reutilizar funcionalidades
 * entre los actions de "recargacelulares".
 * <p>
 * Registro de versiones:<ul>
 * <li>1.0 16/04/2007 Pedro Carmona E. (SEnTRA): Versi�n Inicial
 * <li>1.1 30/01/2013, Darlyn Delgado (SEnTRA): Se estandarizan logs
 * <li>1.2 22/04/2013 Victor Hugo Enero. (ORAND): Se agrega m�todo de autentificacion SafeSigner en metodo
 * setMontosMaximosAutorizados
 * <li>1.3 30/08/2017 Marcelo Fuentes (SEnTRA) - Ricardo Carrasco (Ing. Soft. BCI): Se modifica m�todo 
 * {@link #setMontosMaximosAutorizados(SessionBCI, String)}. Se agrega metodo {@link #getLogger()} y 
 * las constantes {@link #TABLA_PARAMETROS_ENTRUST}, {@link #ENTRUST_TOKEN} y {@link #ENTRUST_SOFTTOKEN}.</li> 
 * </ul> <p>
 * 
 * <B>Todos los derechos reservados por Banco de Cr�dito e Inversiones.</B>
 * <P>
 */
public class RecargaCelularesHelper {

    /**
     * Tabla de parametros de Recargas.
     */
    private static final String PARAMSAFESIGNER = "safesigner.parametros";
    
    /**
     * tabla de par�metros de entrust.
     */
    private static final String TABLA_PARAMETROS_ENTRUST = "entrust.parametros";
    
    /**
     * Tipo de Segunda Clave Softtoken Entrust.
     */
    private static final String ENTRUST_TOKEN = TablaValores.getValor(TABLA_PARAMETROS_ENTRUST, "nombreToken", "desc");
    
    /**
     * Tipo de Segunda Clave Softtoken Entrust.
     */
    private static final String ENTRUST_SOFTTOKEN = TablaValores.getValor(TABLA_PARAMETROS_ENTRUST, "nombreSoftToken", "desc");
    
    /**
     * Objeto que registra datos durante el flujo de ejecuci�n de la aplicaci�n.
     */
    private transient Logger log = (Logger) Logger.getLogger(this.getClass());
    
    private RecargaCelularDelegate delegate = new RecargaCelularDelegate();
    private static final String paramFile = "recargaCelular.parametros";

    /**
     * Constructor por defecto.
     */
    public RecargaCelularesHelper() {
    }

    /**
	 * M�todo que asigna el monto m�ximo que que se puede recargar un celular dependiendo del tipo de clave que tenga el cliente
     * <p>
     * Registro de versiones:<ul>
     * <li>1.0 (13/04/2007, Pedro Carmona E. (SEnTRA): versi�n inicial.
     * Se copia este m�todo de la clase wcorp/servletcorp/pagos/PortalPagos.java.
     * <li>1.1 30/08/2017  Marcelo Fuentes (SEnTRA) - Ricardo Carrasco (Ing. Soft. BCI): Se agrega obtenci�n de montos tope 
     * para segunda clave Entrust.<li>
     * 
     * </ul>
     * <p>
	 * @param session La sesion Bci.
	 * @param paramFile archivo parametros con los tokens de montos.
	 * @return El monto maximo autorizado.
     * @exception GeneralException en caso de error.
     * @exception Exception en caso de error.
     * @since 1.0
     */
	  public double setMontosMaximosAutorizados(SessionBCI session, String paramFile)
	        throws GeneralException, Exception{
        long rutLog = session.getCliente().getRut();
        TokenDelegate delegate = new TokenDelegate();

        String topeCCT = "";
        String topeCPR = "";
        String tope = "";
        String tipoAutenticacion = null;

        String identificadorPrioridadSegundaClave = (String) session
            .getAttrib("identificadorPrioridadSegundaClave");
        String identificadorEstadoSegundaClave = (String) session.getAttrib("identificadorEstadoSegundaClave");

        String sactive = TablaValores.getValor(PARAMSAFESIGNER, "active", "valor");
        String segundaClaveValor = TablaValores.getValor(PARAMSAFESIGNER, "segundaclave", "valor");
        getLogger().debug("setMontosMaximosAutorizados   : Se verifica si tiene SafeSigner");
        if (segundaClaveValor.equalsIgnoreCase(identificadorPrioridadSegundaClave)
            && sactive.equalsIgnoreCase(identificadorEstadoSegundaClave)) {
            topeCCT = "topeCCTSAFESIGNER";
            topeCPR = "topeCPRSAFESIGNER";
            tipoAutenticacion = segundaClaveValor;
            getLogger().debug("setMontosMaximosAutorizados   : Tiene SafeSigner");
        }
        else if (ENTRUST_SOFTTOKEN.equalsIgnoreCase(identificadorPrioridadSegundaClave)
            && sactive.equalsIgnoreCase(identificadorEstadoSegundaClave)) {
            //topeCCT = "topeCCTENTRUSTSOFTTOKEN";
        	//topeCPR = "topeCPRENTRUSTSOFTTOKEN";
            topeCCT = "topeCCTSAFESIGNER";
            topeCPR = "topeCPRSAFESIGNER";
            tipoAutenticacion = identificadorEstadoSegundaClave;
            getLogger().debug("setMontosMaximosAutorizados   : Tiene EntrustSoftToken");
        }
        else if (ENTRUST_TOKEN.equalsIgnoreCase(identificadorPrioridadSegundaClave)
            && sactive.equalsIgnoreCase(identificadorEstadoSegundaClave)) {
        	//topeCCT = "topeCCTENTRUSTTOKEN";
        	//topeCPR = "topeCPRENTRUSTTOKEN";
            topeCCT = "topeCCTSAFESIGNER";
            topeCPR = "topeCPRSAFESIGNER";
            tipoAutenticacion = identificadorEstadoSegundaClave;
            getLogger().debug("setMontosMaximosAutorizados   : Tiene EntrustToken");
        }
        else {
            Hashtable seguridad = session.getMetodosAutenticacion();
            if (getLogger().isDebugEnabled()) {
                getLogger().debug("[setMontosMaximosAutorizados][" + rutLog + "] seguridad [" + seguridad + "]");
            }
            if (seguridad != null) {
                Boolean estadoMetodo = (Boolean) seguridad.get("TOK");
                if (getLogger().isDebugEnabled()) {
                    getLogger().debug("[setMontosMaximosAutorizados][" + rutLog + "] estadoMetodo [" + estadoMetodo + "]");
                }
                if (estadoMetodo != null) {
                    if (estadoMetodo.booleanValue()) {
                        tipoAutenticacion = "Token";
	                } else if (session.getAttrib("permitirAutentificacion") == null){
                        tipoAutenticacion = "Token";
                    }
                }
            }
            if (getLogger().isInfoEnabled()) {
                getLogger().info("[setMontosMaximosAutorizados]["+rutLog+"] tipoAutenticacion ["+tipoAutenticacion+"]");
            }
            if (tipoAutenticacion != null && tipoAutenticacion.equalsIgnoreCase("Token")) {
                topeCCT = "topeCCTTOKEN";
                topeCPR = "topeCPRTOKEN";
	        } else if (delegate.estadoClaveDos(session.getCliente().getRut(),session.getCliente().getDigito())){
                topeCCT = "topeCCTPASS";
                topeCPR = "topeCPRPASS";
	        	} else {
                topeCCT = "topeCCT";
                topeCPR = "topeCPR";
            }
        }
        // Cuentas y Saldos
        ListaCuentas cct = session.getCliente().getCuentasCtes();
        ListaCuentas cpr = session.getCliente().getCuentasPrimas();
        if (cct != null && cct.cuentas != null) {
            if (cct.cuentas.length > 0)
                tope = topeCCT;
	        } else if(cpr != null && cpr.cuentas != null){
            if (cpr.cuentas.length > 0)
                tope = topeCPR;
        }
        String monto = TablaValores.getValor(paramFile, session.getCanal().getCanalID(), tope);
        if (getLogger().isInfoEnabled()) {
            getLogger().info("[setMontosMaximosAutorizados][BCI_FINOK]["+ rutLog+"] Retorna (new Double(monto)).doubleValue(); monto ["+monto+"]");
        }
        return (new Double(monto)).doubleValue();
    }
    
    /**
     * Metodo encargado de obtener el logger().
     * <p>
     *
     * Registro de versiones:<ul>
     * <li> 1.0 30/08/2017,  Marcelo Fuentes (SEnTRA) - Ricardo Carrasco (Ing. Soft. BCI): Versi�n Inicial.</li>
     * </ul>
     * <p>
     * @return el logger.
     * 
     * @since 1.3
     */
    private Logger getLogger() {
        if (log == null) {
            log = Logger.getLogger(this.getClass());
        }
        return log;
    }
    
}
