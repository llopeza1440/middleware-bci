var form;
var input;
var btnAceptar;
var ndigit;
var loading;

/*Se utiliza cuando se carga la pagina de softoken-offline-qr*/
function inicializar(){
  form = document.getElementById("form");
  input = document.getElementById("codigoConfirmacionQR");
  btnAceptar = document.getElementById("aceptar");
  ndigit = document.getElementById("ndigit");
  loading = document.getElementById("loading");
  
  input.onkeyup = function(event){
    this.value=this.value.replace(/[^0-9\.]/g,'');
    ndigit.innerHTML = this.value.length;
    if(this.value.length==8){
      btnAceptar.classList.remove("disabled");
      btnAceptar.disabled = false;
    }else{
      btnAceptar.classList.add("disabled");
      btnAceptar.disabled = true;
    }
  }
  
  form.onsubmit = function(){
    loading.style.display='block';
    setTimeout(function(){
      loading.style.display='none';
      //parent.resize();
      }, 2000);
    return aceptar(this);
  }
}

function getImageSrc(base64Src) {
  return base64Src;
}

/*Se usa en boton aceptar de pagina softoken-offline-qr, 
 en cambio cuando es incrustado en iframe se usa el boton de la pagina que lo contiene*/
function aceptar() {
  form = document.getElementById("form");
  salir = false;
  var clave = form.codigoConfirmacionQR.value;
  
  if(isNaN(clave) || (clave == '')){
    alert("Debe ingresar el valor que aparece en el dispositivo");
    form.codigoConfirmacionQR.value = '';
    form.codigoConfirmacionQR.focus();
    return false;
  }
  
  btnAceptar = document.getElementById("aceptar");
  if(btnAceptar != null){
    btnAceptar.classList.add("disabled");
    btnAceptar.disabled = true;
  }
  return true;
}

function isNumberKey(evt){
  var charCode = (evt.which) ? evt.which : event.keyCode
  if (charCode > 31 && (charCode < 48 || charCode > 57))
    return false;
  
  return true;
}

function mostrarImagenQr(image) {
  var imgQr = document.getElementById("imgQr");
  imgQr.src = getImageSrc(image);
}

/*Se utiliza cuando se carga la pagina de softoken-offline-qr*/
function cargaQRInicial(){
  form = document.getElementById("form");
  mostrarImagenQr(form.codigosqr[0].value);
}

/*Se utiliza cuando se carga la pagina de softoken-offline-qr en un iframe*/
function inicializarGenerico(pForm){
  if(pForm == null){
    form = document.getElementById("formSegClave");
  }else{
    form = pForm;
  }
  
  input = document.getElementById("codigoConfirmacionQR");
  btnAceptar = document.getElementById("aceptar");
  ndigit = document.getElementById("ndigit");
  loading = document.getElementById("loading");
  
  var btn1 = parent.document.getElementById('btn1');
  var btn2 = parent.document.getElementById('btn2');
  var ckbx_acepto = parent.document.getElementById('ckbx_acepto');
  
  input.onkeyup = function(event){
    this.value=this.value.replace(/[^0-9\.]/g,'');
    ndigit.innerHTML = this.value.length;
    
    if(this.value.length == 8){
      if(btn2 != null){
        if(ckbx_acepto == null || ckbx_acepto.checked){
          btn1.style.display = "none";
          btn2.style.display = "";
        }
      }else{
        btnAceptar.classList.remove("disabled");
        btnAceptar.disabled = false;
      }
    }else{
      if(btn2 != null){
        btn1.style.display = "";
        btn2.style.display = "none";
      }else{
        btnAceptar.classList.add("disabled");
        btnAceptar.disabled = true;
      }
    }
  }
  /*
  form.onsubmit = function(){
    loading.style.display='block';
    setTimeout(function(){
      loading.style.display='none';
      //parent.resize();
      }, 2000);
    return aceptar(this);
  }*/
}

/*Se utiliza cuando se carga la pagina de softoken-offline-qr en un iframe*/
function cargaQRInicialGenerico(){
  form = document.getElementById("formSegClave");
  mostrarImagenQr(form.codigosqr[0].value);
}
