package cl.bci.infraestructura.web.seguridad.segundaclave.validator;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import wcorp.util.TablaValores;


/**
 * Validacion del codigo de autorizacion para Entrust Token F�sico.
 * 
 * Registro de versiones:
 * <ul>
 * <li>1.0 10/07/2017 Luis L�pez Alamos (SEnTRA) - Ricardo Carrasco C�ceres (Ing. Soft. BCI): Versi�n Inicial.</li>
 * </ul>
 * <p>
 * <b>Todos los derechos reservados por Banco de Cr�dito e Inversiones.</b>
 * </p>
 */
@FacesValidator("cl.bci.infraestructura.web.seguridad.segundaclave.validator.SegundaClaveEntrustTokenValidator")
public class SegundaClaveEntrustTokenValidator implements Validator {

    /**
     * Logger de la clase.
     */
    private static Logger logger = (Logger) Logger.getLogger(SegundaClaveEntrustTokenValidator.class);

    /**
     * Tabla de codigos de errores.
     */
    private static final String CODIGOS_ERRORES = "errores.codigos";
    
    /**
     * tabla de par�metros de entrust.
     */
    private static final String TABLA_PARAMETROS_ENTRUST = "entrust.parametros";

    /**
     * Largo clave Entrust Fisica.
     */
    private static final int LARGO_CLAVE_FISICA = Integer.parseInt(TablaValores.getValor(TABLA_PARAMETROS_ENTRUST, "largoLlaveFisica", "desc")!=null ? TablaValores.getValor(TABLA_PARAMETROS_ENTRUST, "largoLlaveFisica", "desc") : "0");

    
    /**
     * M�todo que valida la segunda clave Entrust Token.
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 10/07/2017 Luis L�pez Alamos (SEnTRA) - Ricardo Carrasco C�ceres (Ing. Soft. BCI): Versi�n Inicial.</li>
     * </ul>
     * </p>
     * 
     * @param context contexto.
     * @param component componente.
     * @param value valor.
     * @throws ValidatorException excepcion de validacion.
     * @since 1.0
     */
    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
        if(logger.isEnabledFor(Level.INFO)){
            logger.info("[validate][BCI_INI]");
        }

        if (value == null) {
            logger.error("[validate]  Autenticacion de segunda clave fallida, clave nula");
            throw new ValidatorException(
                new FacesMessage(FacesMessage.SEVERITY_ERROR, "segundaClave", TablaValores.getValor(
                CODIGOS_ERRORES, "MENSAJESINCLAVEENTRUST", "Desc")));
        }
        String clave = value.toString();
        
        if (clave.equals("")) {
            logger.error("[validate]  Autenticacion de segunda clave fallida, clave no tiene valor");
            throw new ValidatorException(
                new FacesMessage(FacesMessage.SEVERITY_ERROR, "segundaClave", TablaValores.getValor(
                CODIGOS_ERRORES, "MENSAJESINCLAVEENTRUST", "Desc")));
        }

        if (LARGO_CLAVE_FISICA != 0 && clave.length() != LARGO_CLAVE_FISICA) {
            logger.error("[validate]  Autenticacion de segunda clave fallida, clave no tiene el largo necesario");

            throw new ValidatorException(
                new FacesMessage(FacesMessage.SEVERITY_ERROR, "segundaClave", TablaValores.getValor(
                CODIGOS_ERRORES, "ENTRUSTFRASE01", "Desc")));
        }
        
        if(logger.isEnabledFor(Level.INFO)){
            logger.info("[validate][BCI_FINOK]");
        }
    }

}
