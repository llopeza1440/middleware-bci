package cl.bci.aplicaciones.seguridad.autenticacion.mb;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Hashtable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.oltu.oauth2.common.exception.OAuthProblemException;

import cl.bci.aplicaciones.cliente.mb.ClienteMB;
import cl.bci.aplicaciones.cliente.mb.UsuarioModelMB;
import cl.bci.aplicaciones.seguridad.autenticacion.utils.DefineDespacho;
import cl.bci.aplicaciones.seguridad.autenticacion.utils.SegundaClave;
import cl.bci.infraestructura.web.portal.mb.PortalMB;
import cl.bci.infraestructura.web.seguridad.mb.SesionMB;
import cl.bci.infraestructura.web.seguridad.segundaclave.SegundaClaveUtil;
import cl.bci.infraestructura.web.seguridad.segundaclave.mb.JerarquiaClavesModelMB;
import cl.bci.infraestructura.web.seguridad.segundaclave.to.CamposDeLlaveTO;
import cl.bci.infraestructura.web.seguridad.segundaclave.to.EstadoSegundaClaveTO;
import cl.bci.infraestructura.web.seguridad.segundaclave.to.LlaveSegundaClaveTO;
import cl.bci.infraestructura.web.seguridad.segundaclave.to.UsuarioSegundaClaveTO;
import cl.bci.infraestructura.web.seguridad.to.CanalTO;
import cl.bci.infraestructura.web.seguridad.util.ConectorStruts;
import cl.bci.infraestructura.ws.excepcion.SistemaException;
import wcorp.model.seguridad.EstrategiaAutenticacionFactory;
import wcorp.model.seguridad.EstrategiaSafeSigner;
import wcorp.model.seguridad.EstrategiaSegundaClave;
import wcorp.model.seguridad.ParametrosEstrategiaSegundaClaveTO;
import wcorp.model.seguridad.RepresentacionAlfanumericaSegundaClaveTO;
import wcorp.model.seguridad.RepresentacionSegundaClave;
import wcorp.model.seguridad.ResultadoOperacionSegundaClaveTO;
import wcorp.model.seguridad.SessionBCI;
import wcorp.serv.seguridad.SeguridadException;
import wcorp.util.GeneralException;
import wcorp.util.TablaValores;
import wcorp.util.journal.Eventos;
import wcorp.util.journal.Journal;
import wcorp.util.mensajeria.ControlErrorMsg;

/**
 * Support ManagedBean
 *
 * <pre><b>SegundaClaveMB</b>
 *
 * Componente encargado de la autenticaci�n de un cliente con su segunda clave.
 *
 * </pre>
 *
 * Registro de versiones:<ul>
 *
 * <LI>1.0 21/06/2011, Jorge G�mez O. (SEnTRA) - Versi�n inicial
 * <LI>1.1 10/11/2011, Hctor Hernndez Orrego (SEnTRA) - Se agrega modifica el fuente para permitir el despacho a un
 * <li>1.2 27/04/2012, Eduardo Villagr�n M. (Imagemaker) - Se agrega journalizaci�n de NOK en login con token</li>
 * <LI>1.3 19/05/2012 Marco Aic�n D.(SEnTRA): se agrega el metodo validaSegundaClaveTBanc.
 * <li>1.1 25/05/2012, Eduardo Villagr�n M. (Imagemaker) - Se modifican las l�neas de logging
 * 			para mejorar la calidad de �stas.</li>
 * <li>1.2 12/02/2013 Ricardo Treumun, Eduardo Mascayano (TInet): Se agregar flag {@link #vieneDelLogin}
 * para determinar si el cliente viene del login personas. Se modifica el m�todo {@link #validaSegundaClave()}
 * para journalizar el evento LOGIN OK del producto INT s�lo cuando el flag posea valor TRUE.
 * <li>1.3 05/06/2013 Yon Sing Sius. (ExperimentoSocial): Se agregan m�todos para la integraci�n
 * de la nueva arquitectura definida para la validaci�n de segunda clave.
 * <li>1.3 28/08/2013, Robinson Mu�oz(SEnTRA) - Se agrega validaci�n multipass movil </li>
 * <li>1.4 07/11/2013, Agust�n Fuentes(ExperimentoSocial): Se modifica firma del m�todo
 * {@link #validarSegundaClave(String, wcorp.model.seguridad.CamposDeLlaveTO)} para env�o de campos llave
 * para validaci�n de Segunda Clave.</li>
 * <li>1.5 21/07/2014, Ignacio Gonz�lez (TINet): Se agrega m�todo:
 * {@link #validarSegundaClave(String, CamposDeLlaveTO)} utiliza como parametro una instancia de
 * cl.bci.infraestructura.web.seguridad.segundaclave.to.CamposDeLlaveTO
 * Se corrigen alertas seg�n normas checkstyle.</li>
 * <li>1.6 30/01/2015 Rodrigo Pino Galleguillos (SEnTRA): Se incorpora el m�todo
 * 						{@link #determinarDespachoLogin(String)}. Adem�s, se agregan los atributos
 * 						{@link #ID_PRODSAFESIGNER} y {@link #PARAMSAFESIGNER}.
 * </li>
 * <li>1.7 06/07/2016, Luis Lopez Alamos (SEnTRA) - Pamela Inostroza Mu�oz (Ing. Soft. BCI): Se agregan constante y atributos a la clase, se modifica el metodo {@link #determinarDespachoLogin(String)}.</li>
 * <li>1.8 27/04/2016, Victor Enero (Orand) - Pablo Rompentin (Ing. Soft. BCI) : Se modifican los siguientes metodos:
 *                      {@link #generarLlave(CamposDeLlaveTO)}
 *                      {@link #validarSegundaClave(String, CamposDeLlaveTO)}
 * Se agrega m�todo:    {@link #estadoSegundaClave(HttpServletRequest)} para traspasar el parametro HttpServletRequest 
 * para obtener el usuario desde sessionBCI o desde el token Oauth.
 * </li>
 * <li>1.9 10/07/2017 Luis L�pez Alamos (SEnTRA) - Ricardo Carrasco C�ceres (Ing. Soft. BCI): Se modifica m�todo {@link #generarLlave()}}</li>
 * </ul>
 * <b>Todos los derechos reservados por Banco de Cr�dito e Inversiones.</b>
 */
@ManagedBean
@RequestScoped
public class SegundaClaveMB implements Serializable {

	/** Valor "LOGIN" de la constante EVE_LOGIN.  */
	public static final String EVE_LOGIN                 = "LOGIN";

	/** Valor "OK" de la constante SUB_COD_OK. */
	public static final String SUB_COD_OK                = "OK";

	/** Valor "NOK" de la constante SUB_COD_NOK. */
	public static final String SUB_COD_NOK               = "NOK";

	/** Valor "INCORR" de la constante SUB_COD_ERROR. */
	public static final String SUB_COD_ERROR             = "INCORR";

	/** Valor "INT" de la constante ID_PRODINTERNET. */
	public static final String ID_PRODINTERNET           = "INT";

	/** Valor "TOK" de la constante ID_PRODTOKEN. */
	public static final String ID_PRODTOKEN              = "TOK";

	/**
	 * Variable que representa en el id del producto Multipass Movil.
	 */
	private static final String ID_PRODSAFESIGNER  = "MPM";

	/**
	 * Identificador para safesigner.parametros.
	 */
	private static final String PARAMSAFESIGNER = "safesigner.parametros";

	/** Serial de la clase. */
	private static final long serialVersionUID = 1L;

	/** Variable para asignar log de ejecuci�n. */
	private static Logger logger = (Logger)Logger.getLogger(SegundaClaveMB.class);

	/** Estado ok de autenticacion. */
	private static final int AUTENTICACION_OK = 0;

	/**
	 * Tabla banca privada.
	 */
	private static final String TABLA_BPR = "bancaPrivada.parametros";

	/**
	 * Tabla banca privada.
	 */
	private static final String ID_BANCA_PRIVADA = "117";

	/** Atributo que representa clave token. */
	private String claveToken;

	/** Atributo que representa monto m�ximo. */
	private long montoMaximo;

	/** Atributo que representa valor del despacho. */
	private String despacho;

	/** Atributo que representa mensajes de error. */
	private String mensajeError;

	/** Variable que representa rut del cliente. */
	private String rutCliente;

	/**
	 * atributo utilizado para desplegar mensaje
	 * de error en vista de solicitud de multipass
	 * de Tbanc.
	 */
	private boolean mostrarError = false;

	/** Atributo que representa c�digo de error. */
	private String codigoError;

	/**
	 *  Parametro contiene url a despachar despues de realizar validacion de segunda clave.
	 */
	private String despachoDespuesValidar;

	/**
	 * Bandera que indica si el cliente viene del Login Personas. Valores posibles:<br/>
	 * True: Cliente viene del login personas para validar su multipass. False: Valor por defecto cuando cliente
	 * viene desde otro m�dulo a validar su multipass.
	 */
	private boolean vieneDelLogin;

	/**
	 * Nombre de servicio para consulta y generaci�n de llave para una transai�n
	 * Ver tabla de parametros "SegundaClave.parametros" para ver los nombres de servicios
	 * disponibles.
	 */
	private String servicio;

	/**
	 * Clase que se usa como pivote para manejar el valor de redirectWeb en el caso que sea banca privada y entre por selector de rut.
	 * Para otro tipo de logica no se isntancia este atributo.
	 */
	private CanalTO portalMask;

	/**
	 * Inyecci�n managed bean de sesi�n.
	 */
	@ManagedProperty(value = "#{sesionMB}")
	private SesionMB sesion;

	/**
	 * Inyecci�n managed bean de portal.
	 */
	@ManagedProperty(value = "#{portalMB}")
	private PortalMB portal;

	/**
	 * Inyecci�n managed bean de cliente.
	 */
	@ManagedProperty(value = "#{clienteMB}")
	private ClienteMB cliente;

	/**
	 * Atributo representa Managed bean inyectado UsuarioModelMB.
	 */
	@ManagedProperty(value = "#{usuarioModelMB}")
	private UsuarioModelMB usuario;

	/**
	 * Atributo representa Managed bean inyectado JerarquiaClavesModelMB.
	 */
	@ManagedProperty(value = "#{jerarquiaClavesModelMB}")
	private JerarquiaClavesModelMB jerarquiaClaves;

	/**
	 * Servlet.
	 */
	private HttpServletRequest servletRequest;

	public String getClaveToken() {
		return claveToken;
	}
	public void setClaveToken(String claveToken) {
		this.claveToken = claveToken;
	}

	public HttpServletRequest getServletRequest() {
		return servletRequest;
	}
	public void setServletRequest(HttpServletRequest servletRequest) {
		this.servletRequest = servletRequest;
	}
	/**
	 * <p>
	 * M�todo que obtiene monto m�ximo.
	 * </p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 ??/??/???? ?: Version inicial.</li>
	 * </ul>
	 *
	 * @since ?
	 * @return montoMaximo.
	 **/
	public long getMontoMaximo() {
		String canal = portal.getCanalPersonalizado().getCanalID();
		if(logger.isEnabledFor(Level.DEBUG)){
			logger.debug("[getMontoMaximo] canal=<" + canal + ">");
		}
		String montoMaxParam;
		if (TablaValores.getValor("TransferenciaTerceros.parametros", canal, "topeCCTTOKEN")!=null)
			montoMaxParam=TablaValores.getValor("TransferenciaTerceros.parametros", canal, "topeCCTTOKEN");
		else
			montoMaxParam="0";

		montoMaximo = Long.parseLong(montoMaxParam);
		if(logger.isEnabledFor(Level.DEBUG)){
			logger.debug("[getMontoMaximo] montoMaximo=<" + montoMaximo + ">");
		}
		return montoMaximo;
	}
	public void setMontoMaximo(long montoMaximo) {
		this.montoMaximo = montoMaximo;
	}

	public String getDespacho() {
		return despacho;
	}
	public void setDespacho(String despacho) {
		this.despacho = despacho;
	}
	public String getMensajeError() {
		return mensajeError;
	}
	public void setMensajeError(String mensajeError) {
		this.mensajeError = mensajeError;
	}

	public SesionMB getSesion() {
		return sesion;
	}
	public void setSesion(SesionMB sesion) {
		this.sesion = sesion;
	}
	public PortalMB getPortal() {
		return portal;
	}
	public void setPortal(PortalMB portal) {
		this.portal = portal;
	}
	public ClienteMB getCliente() {
		return cliente;
	}
	public void setCliente(ClienteMB cliente) {
		this.cliente = cliente;
	}
	public String getDespachoDespuesValidar() {
		return despachoDespuesValidar;
	}
	public void setDespachoDespuesValidar(String despachoDespuesValidar) {
		this.despachoDespuesValidar = despachoDespuesValidar;
	}
	public boolean isMostrarError() {
		return mostrarError;
	}
	public void setMostrarError(boolean mostrarError) {
		this.mostrarError = mostrarError;
	}

	public String getServicio() {
		return servicio;
	}

	public void setServicio(String servicio) {
		this.servicio = servicio;
	}

	/**
	 * M�todo que realiza la validaci�n de la segunda clave de un cliente.
	 * <p>
	 * Registro de versiones:<ul>
	 *
	 * <li>1.0 21/06/2011 Jorge G�mez (Sentra): Versi�n Inicial
	 * <li>1.1 27/04/2012 Eduardo Villagr�n M. (Imagemaker): Se agrega journalizaci�n para
	 * 			NOK en login con token.</li>
	 * <li>1.2 25/05/2012, Eduardo Villagr�n M. (Imagemaker): Se modifican las l�neas de logging
	 * 			para mejorar la calidad de �stas.</li>
	 * <li>1.3 12/02/2013 Ricardo Treumun, Eduardo Mascayano (TInet): Se eval�a el flag {@link #vieneDelLogin}
	 * para determinar si el cliente viene del login personas y journalizar el evento LOGIN OK del producto
	 * INT s�lo cuando el flag posea valor TRUE.
	 * </ul>
	 * <p>
	 * @return pagina de despacho
	 * @since 1.0
	 */
	public String validaSegundaClave(){
		CanalTO canalCliente = null;
		try{
			canalCliente = portal.getCanalPersonalizado();
			String nombreCanal = canalCliente.getNombre();
			String canal = canalCliente.getCanalID();
			rutCliente = String.valueOf(cliente.getRut());
			String dvCliente = String.valueOf(cliente.getDigitoVerif());
			String rutCompleto = cliente.getFullRut();


			if(logger.isEnabledFor(Level.INFO)){
				logger.info("[validaSegundaClave] [" + rutCliente + "] [SPK_INI] canalCliente=<"
						+ canalCliente +">, " +	"nombreCanal=<" + nombreCanal + ">, "
						+ "canal=<" + canal + ">, " + "dvCliente=<" + dvCliente + ">, "
						+ "rutCompleto=<" + rutCompleto + ">");
			}

			if (claveToken == null) {
				throw new SeguridadException("USERLOGIN");
			} else {
				if(logger.isEnabledFor(Level.DEBUG)){
					logger.debug("[validaSegundaClave] [" + rutCliente
							+ "] Ejecutar autenticaSegundaClave");
				}

				SegundaClave segundaClave = new SegundaClave();
				try{
					segundaClave.autenticaSegundaClave(claveToken, canal, Long.parseLong(rutCliente),
							dvCliente.charAt(0));
				}
				catch (SeguridadException errorSeguridad){
					logger.debug("journalizo ClaveInternet NOK CON TOKEN");
					journalizacion(EVE_LOGIN, SUB_COD_NOK, rutCliente, ID_PRODTOKEN, null, dvCliente,
							nombreCanal, ConectorStruts.getRemoteAddr());
					throw errorSeguridad;
				}
				if(logger.isEnabledFor(Level.DEBUG)){
					logger.debug("[validaSegundaClave] [" + rutCliente
							+ "] Token autenticado correctamente");
				}

				if ((canal == null || (canal != null && !canal.trim().equals("215"))) && vieneDelLogin) {
					if(logger.isEnabledFor(Level.DEBUG)){
						logger.debug("[validaSegundaClave] [" + rutCliente
								+ "] journalizo LOGIN OK CON TOKEN");
					}
					journalizacion(EVE_LOGIN, SUB_COD_OK, rutCliente, ID_PRODINTERNET, null, dvCliente,
							nombreCanal, ConectorStruts.getRemoteAddr());
				}

				if(canal == null || (canal != null && !canal.trim().equals("215"))){
					if(logger.isEnabledFor(Level.DEBUG)){
						logger.debug("[validaSegundaClave] [" + rutCliente
								+ "] journalizo TOKEN OK");
					}
					journalizacion(EVE_LOGIN, SUB_COD_OK, rutCliente, ID_PRODTOKEN, null, dvCliente, nombreCanal,
							ConectorStruts.getRemoteAddr());
				}

				if(logger.isEnabledFor(Level.DEBUG)){
					logger.debug("[validaSegundaClave] [" + rutCliente + "] autenticacion token ok");
				}

				if (despachoDespuesValidar!=null && !despachoDespuesValidar.equals("")){
					this.setDespachoDespuesValidar(despachoDespuesValidar);
				}

				if ( canalCliente.getUsaStruts()!=null && canalCliente.getUsaStruts().equalsIgnoreCase("NO")){
					if(logger.isEnabledFor(Level.DEBUG)){
						logger.debug("[validaSegundaClave] [" + rutCliente + "] usaStruts=<"
								+ canalCliente.getUsaStruts() + ">");
					}
					despacho=canalCliente.getRedirectWebServer();
				}else{
					DefineDespacho defineDespacho = new DefineDespacho();
					despacho = defineDespacho.defineDespacho(rutCliente, canalCliente);
				}

				if(logger.isEnabledFor(Level.DEBUG)){
					logger.debug("[validaSegundaClave] [" + rutCliente + "] despacho: " + despacho);
				}

				Hashtable metodosAutenticacion = ConectorStruts.getSessionBCI().getMetodosAutenticacion();
				if (metodosAutenticacion.get(ID_PRODTOKEN) != null) {
					if(logger.isEnabledFor(Level.DEBUG)){
						logger.debug("[validaSegundaClave] [" + rutCliente + "] Cambiando Estado Token");
					}
					metodosAutenticacion.remove(ID_PRODTOKEN);
					metodosAutenticacion.put(ID_PRODTOKEN, new Boolean(true));
				}
				ConectorStruts.getSessionBCI().setMetodosAutenticacion(metodosAutenticacion);

				sesion.activar();
				String destinoDespacho;
				if (despachoDespuesValidar!=null && !despachoDespuesValidar.equals("")){
					this.setDespachoDespuesValidar(despachoDespuesValidar);
					destinoDespacho=despachoDespuesValidar;
				}else{
					destinoDespacho="cargaDespacho";
				}
				if(logger.isEnabledFor(Level.DEBUG)){
					logger.debug("[validaSegundaClave] [" + rutCliente + "] [SPK_FINOK] despachando a "
							+ destinoDespacho);
				}
				return destinoDespacho;
			}
		}catch(wcorp.serv.seguridad.SeguridadException ex){
			if(logger.isEnabledFor(Level.ERROR)){
				logger.error("[validaSegundaClave] [" + rutCliente
						+ "] [SeguridadException] El canal es : " + canalCliente.getCanalID());
				logger.error("[validaSegundaClave] [" + rutCliente
						+ "] [SeguridadException] El canal de la sesion es : "
						+ ((ConectorStruts.getSessionBCI()!=null)?canalCliente.getCanalID():"null"));
				logger.error("[validaSegundaClave] [" + rutCliente
						+ "] [SeguridadException] ex.getCodigo() : " + ex.getCodigo() + ", mensaje=<"
						+ ex.getMessage() + "> ", ex);
			}
			ex = (SeguridadException)ControlErrorMsg.modificaCodigoError((Exception)ex, canalCliente.getCanalID());
			if(logger.isEnabledFor(Level.ERROR)){
				logger.error("[validaSegundaClave] [" + rutCliente
						+ "] [SeguridadException] ex.getCodigo() : " + ex.getCodigo() + ", mensaje=<"
						+ ex.getMessage() + "> " , ex);
			}
			if (ConectorStruts.getSessionBCI() != null)
			{
				if(logger.isEnabledFor(Level.ERROR)){
					logger.error("[validaSegundaClave] [" + rutCliente
							+ "] [SPK_FINEX] [SeguridadException] despachando a error.xhtml, mensaje=<"
							+ ex.getMessage() + "> " , ex); }
				mensajeError = ex.getSimpleMessage();
				codigoError = ex.getCodigo();
				return "error";
			}
			else
			{
				wcorp.serv.seguridad.SeguridadException se = (wcorp.serv.seguridad.SeguridadException)ex;
				if(logger.isEnabledFor(Level.ERROR)){
					logger.error("[validaSegundaClave] [" + rutCliente
							+ "] [SPK_FINEX] [SeguridadException] despachando a error.xhtml, mensaje=<"
							+ se.getMessage() + "> " , se);
				}
				mensajeError = se.getSimpleMessage();
				codigoError = ex.getCodigo();
				return "error";
			}
		}
		catch(wcorp.util.GeneralException ex){
			if (ConectorStruts.getSessionBCI() != null)
			{
				ex = (GeneralException) ControlErrorMsg.modificaCodigoError((Exception) ex,
						canalCliente.getCanalID());
				if(logger.isEnabledFor(Level.ERROR)){
					logger.error("[validaSegundaClave] [" + rutCliente
							+ "] [SPK_FINEX] [GeneralException] despachando a error.xhtml, mensaje=<"
							+ ex.getMessage() + "> " , ex);
				}
				mensajeError = ex.getMessage();
				return "error";
			}
			else
			{
				wcorp.util.GeneralException se = (wcorp.util.GeneralException)ex;
				if (logger.isEnabledFor(Level.ERROR)) {
					logger.error(
							"[validaSegundaClave] [" + rutCliente
							+ "] [SPK_FINEX] [GeneralException] despachando a error.xhtml, mensaje=<"
							+ se.getMessage() + "> ", se);
				}
				mensajeError = se.getMessage();
				return "error";
			}
		}
		catch (Exception ex) {
			if(logger.isEnabledFor(Level.ERROR)){
				logger.error("[validaSegundaClave] [" + rutCliente + "] [Exception] Sesion HTTP Expirada");
			}
			if (ConectorStruts.getSessionBCI() != null) {
				ex = ControlErrorMsg.modificaCodigoError(ex, canalCliente.getCanalID());
				if(logger.isEnabledFor(Level.ERROR)){
					logger.error("[validaSegundaClave] [" + rutCliente
							+ "] [Exception] SessionBCI Expirada, se envia a login");
				}
				sesion.desactivar();
				ConectorStruts.removeAttribHttp("sessionBci");
				if(logger.isEnabledFor(Level.ERROR)){
					logger.error("[validaSegundaClave] [" + rutCliente
							+ "] [SPK_FINEX] [Exception] despachando a /common/Error.html, mensaje=<"
							+ ex.getMessage() + "> " , ex);
				}
				despacho = "/common/Error.html";
				return "cargaDespacho";
			}
			else {
				if(logger.isEnabledFor(Level.ERROR)){
					logger.error("[validaSegundaClave] [" + rutCliente
							+ "] [SPK_FINEX] [Exception] despachando a http://www.bci.cl, mensaje=<"
							+ ex.getMessage() + "> " , ex);
				}
				despacho = "http://www.bci.cl";
				return "cargaDespacho";
			}
		}

	}

	/**
	 * M�todo que determina el despacho tras la autenticaci�n de la segunda clave en el proceso de login.
	 * Adem�s, si la segunda clave autenticada no es Token (por ende es safeSigner), se setean valores
	 * particulares en la sessionBci que ser� usandos dentro del portal.
	 * <p>
	 * Registro de versiones:<ul>
	 *
	 * <li>1.0 30/01/2015 Rodrigo Pino Galleguillos (SEnTRA): Versi�n Inicial
	 * <li>1.1 06/07/2016 Luis Lopez Alamos (SEnTRA) - Pamela Inostroza (Ing. Soft. BCI): Se agrega logica especial para el despacho de un cliente banca privada
	 * que ingrese por seleccion de rut.</li>
	 *
	 * </ul>
	 * <p>
	 * @param codigoProducto con el codigo de la clave que se acaba de autenticar.
	 * @return cargaDespacho con la url de despacho.
	 * @since 1.6
	 */
	public String determinarDespachoLogin(String codigoProducto) {
		if(getLogger().isDebugEnabled()){
			getLogger().debug("[determinarDespachoLogin] entra a determinarDespachoLogin");
			getLogger().debug("[determinarDespachoLogin] codClave: " + codigoProducto);
		}
		SessionBCI sesionBci = ConectorStruts.getSessionBCI();
		if (sesionBci == null) {
			if(getLogger().isDebugEnabled())
				getLogger().debug("[determinarDespachoLogin] sesionBCI : NULL ");
			return "error";
		}
		else{
			if (codigoProducto.equalsIgnoreCase(ID_PRODSAFESIGNER)){
				String segundaClaveActiva = TablaValores.getValor(PARAMSAFESIGNER, "active", "valor");
				String tipoSegundaClave = TablaValores.getValor(PARAMSAFESIGNER, "segundaclave", "valor");
				if(getLogger().isDebugEnabled()){
					getLogger().debug("[determinarDespachoLogin] segundaClaveActiva : " + segundaClaveActiva);
					getLogger().debug("[determinarDespachoLogin] tipoSegundaClave : " + tipoSegundaClave);
				}
				if(getLogger().isDebugEnabled())
					getLogger().debug("[determinarDespachoLogin] sesionBCI : "
							+ sesionBci.toString());
				sesionBci.setAttrib("identificadorPrioridadSegundaClave",
						tipoSegundaClave);
				sesionBci.setAttrib("identificadorEstadoSegundaClave",
						segundaClaveActiva);
			}
			sesion.activar();
			try{
				DefineDespacho defineDespacho = new DefineDespacho();
				if ((ID_BANCA_PRIVADA.equalsIgnoreCase(portal.getCanalPersonalizado().getPinID())) && (usuario.isPaginaRedirect())){
					if (getLogger().isDebugEnabled()){
						getLogger().debug("[determinarDespachoLogin] Se cargar� pagina de seleccion de clientes para banca privada");
					}
					portalMask= portal.getCanalPersonalizado();
					String paginaRedirectBP = TablaValores.getValor(TABLA_BPR, "paginaRedirect", "desc");
					if (paginaRedirectBP!=null){
						portalMask.setRedirectWebServer(paginaRedirectBP);
					}
					despacho = defineDespacho.defineDespacho(rutCliente, portalMask);

				}
				else
					despacho = defineDespacho.defineDespacho(rutCliente, portal.getCanalPersonalizado());
			}
			catch (Exception e){
				if(getLogger().isDebugEnabled())
					getLogger().debug("[determinarDespachoLogin] Exception : "
							+ wcorp.util.ErroresUtil.extraeStackTrace(e));
			}
			if(getLogger().isDebugEnabled())
				getLogger().debug("[determinarDespachoLogin] despacho: " + despacho);
			return despacho;
		}
	}

	/**
	 * M�todo que realiza la journalizaci�n.
	 * <p>
	 * Registro de versiones:<ul>
	 *
	 * <li>1.0 21/06/2011 Jorge G�mez (Sentra): Versi�n Inicial
	 *
	 * </ul>
	 * <p>
	 * @param CodEvento C�digo del evento de negocio
	 * @param SubCodNeg Sub c�digo del evento de negocio
	 * @param ClavePrincipal Clave principal
	 * @param ipProducto C�digo del producto
	 * @param codigo C�digo o mensaje de la excepci�n
	 * @param dvCliente D�gito verificador
	 * @param nombreCanal Nombre del canal
	 * @param idMedio IP de la m�quina
	 * @since 1.0
	 */
	private void journalizacion(String CodEvento, String SubCodNeg, String ClavePrincipal, String ipProducto,
			String codigo, String dvCliente, String nombreCanal, String idMedio) {

		if(logger.isEnabledFor(Level.INFO)){
			logger.info("[journalizacion] [" + rutCliente + "] Inicio journalizacion CodEvento=<"
					+ CodEvento + ">, " + "SubCodNeg=<" + SubCodNeg + ">, ClavePrincipal=<"
					+ ClavePrincipal + ">, ipProducto=<" + ipProducto + ">, "
					+ "codigo=<" + codigo + ">, dvCliente=<" + dvCliente
					+ ">, nombreCanal=<" + nombreCanal + ">, "
					+ "idMedio=<" + idMedio + ">");
		}

		Journal journal     = null;
		try{
			journal = new Journal();

			Eventos evento = new Eventos();
			evento.setCodEventoNegocio(CodEvento);
			evento.setSubCodEventoNegocio(SubCodNeg);
			evento.setIdProducto(ipProducto);
			evento.setNroOperacionEvento(evento.getNroOperacionEvento());
			evento.setClavePrincipal(ClavePrincipal);
			evento.setRutCliente(ClavePrincipal);
			evento.setDvCliente(dvCliente);
			evento.setRutOperadorCliente(ClavePrincipal);
			evento.setDvOperadorCliente(dvCliente);
			evento.setIdCanal((nombreCanal.length() > 10)? nombreCanal.substring(0,10) : nombreCanal);
			evento.setIdMedio((idMedio.length() > 15)? idMedio.substring(0,15) : idMedio);

			if(codigo!=null)
				evento.setAtributo("codigo", codigo);

			journal.journalizar(evento);

			if(logger.isEnabledFor(Level.DEBUG)){
				logger.debug("[journalizacion][" + rutCliente + "] Journalizacion OK");
			}
		}catch(Exception e){
			if(logger.isEnabledFor(Level.ERROR)){
				logger.error("[journalizacion] [" + rutCliente + "] [Exception], mensaje=<"
						+ e.getMessage() + "> " ,e);
			}
		}
	}

	/**
	 * Metodo que realiza la validacion de la segunda clave de un cliente Tbanc.
	 * Se encarga de redireccionar a la vista de solicitud de multipass en caso
	 * de producirse un error, ademas de indicar el despliegue del mensaje
	 * de error correspondiente.
	 *
	 * <p>
	 * Registro de versiones:<ul>
	 *
	 * <LI>1.0 19/05/2012 Marco Aic�n D.(SEnTRA): Versi�n inicial.
	 *
	 * </ul>
	 * <p>
	 * @return pagina de despacho
	 * @since 1.3
	 */
	public String validaSegundaClaveTBanc() {
		String resultado = validaSegundaClave();
		if (resultado.equalsIgnoreCase("error")) {
			this.mostrarError = true;
			return TablaValores.getValor("TBancParametros.parametros",
					"segunda_clave", "desc");
		}
		else {
			return resultado;
		}
	}
	public String getCodigoError() {
		return codigoError;
	}
	public void setCodigoError(String codigoError) {
		this.codigoError = codigoError;
	}

	public boolean isVieneDelLogin() {
		return vieneDelLogin;
	}

	public void setVieneDelLogin(boolean vieneDelLogin) {
		this.vieneDelLogin = vieneDelLogin;
	}

	public UsuarioModelMB getUsuario() {
		return usuario;
	}

	public void setUsuario(UsuarioModelMB usuario) {
		this.usuario = usuario;
	}
	public JerarquiaClavesModelMB getJerarquiaClaves() {
		return jerarquiaClaves;
	}
	public void setJerarquiaClaves(JerarquiaClavesModelMB jerarquiaClaves) {
		this.jerarquiaClaves = jerarquiaClaves;
	}

	/**
	 * Consulta el estado de segunda clave independiente de la implementaci�n,
	 * puede ser multipass, safesigner, etc.
	 * Antes de llamar al m�todo se debe setear el nombre del servicio
	 * para el cual se consultar� por la segunda clave.
	 * <p>
	 * Registro de versiones:<ul>
	 *
	 * <LI>1.0 05/06/2013 Yon Sing Sius.(ExperimentoSocial): Versi�n inicial.
	 *
	 * </ul>
	 * <p>
	 * @throws SeguridadException excepci�n de seguridad.
	 * @return estado de la segunda clave
	 * @since 1.3
	 */
	public EstadoSegundaClaveTO estadoSegundaClave() throws SeguridadException {

		UsuarioSegundaClaveTO usuarioSegundaClave = SegundaClaveUtil.getUsuarioSegundaClave();
		EstadoSegundaClaveTO estadoSegundaClave = jerarquiaClaves
				.estadoSegundaClave(servicio, usuarioSegundaClave);
		return estadoSegundaClave;
	}


	/**
	 * Consulta el estado de segunda clave independiente de la implementaci�n,
	 * puede ser multipass, safesigner, etc.
	 * Antes de llamar al m�todo se debe setear el nombre del servicio
	 * para el cual se consultar� por la segunda clave.
	 * <p>
	 * Registro de versiones:<ul>
	 *
	 * <LI>1.0 07/04/2016 Victor Enero.(Orand S.A.) - Pablo Rompentin (Ing. Soft. BCI): Versi�n inicial.
	 *
	 * </ul>
	 * <p>
	 * @param httpServletRequest HttpServletRequest
	 * @return estado de la segunda clave
	 * @throws SeguridadException excepci�n de seguridad.
	 * @throws OAuthProblemException OAuth
	 * @since 1.3
	 */
	public EstadoSegundaClaveTO estadoSegundaClave(HttpServletRequest httpServletRequest) throws SeguridadException, 
	OAuthProblemException {

		UsuarioSegundaClaveTO usuarioSegundaClave = SegundaClaveUtil.getUsuarioSegundaClave(httpServletRequest);
		EstadoSegundaClaveTO estadoSegundaClave = jerarquiaClaves
				.estadoSegundaClave(servicio, usuarioSegundaClave);
		return estadoSegundaClave;
	}

	/**
	 * Consulta el estado de segunda clave para una lista de servicios.
	 * Por cada servicio se verifica el estado de segunda clave.
	 * <p>
	 * Registro de versiones:<ul>
	 *
	 * <LI>1.0 05/06/2013 Yon Sing Sius.(ExperimentoSocial): Versi�n inicial.
	 *
	 * </ul>
	 * <p>
	 * @param servicios por los que se consultara el estado segunda clave
	 * @throws SeguridadException excepci�n de seguridad.
	 * @return Tabla Hash con los estado se de segunda para cada servicio
	 * @since 1.3
	 */
	public HashMap<String, EstadoSegundaClaveTO> estadosSegundaClaveParaServicios(String[] servicios)
			throws SeguridadException {

		UsuarioSegundaClaveTO usuarioSegundaClave = SegundaClaveUtil.getUsuarioSegundaClave();
		HashMap<String, EstadoSegundaClaveTO> dict = new HashMap<String, EstadoSegundaClaveTO>();

		for (int i = 0; i < servicios.length; i++) {
			try {
				EstadoSegundaClaveTO estadoSegundaClave = jerarquiaClaves.estadoSegundaClave(
						servicios[i], usuarioSegundaClave);
				dict.put(servicios[i], estadoSegundaClave);
			} catch (SeguridadException e) {
				if (e.getCodigo() == "SAFESIGNER05") {
					logger.error("[estadosSegundaClaveParaServicios] error SAFESIGNER05 para servicio: " +
							servicios[i]);
					EstadoSegundaClaveTO estadoSegundaClave = new EstadoSegundaClaveTO();
					estadoSegundaClave.setEstatus(false);
					estadoSegundaClave.setGlosa("Sin segunda clave para este servicio");
					dict.put(servicios[i], estadoSegundaClave);
				}
				else {
					logger.error("[estadosSegundaClaveParaServicios] SeguridadException: " + e);
					throw e;
				}
			} catch (Exception e) {
				logger.error("[estadosSegundaClaveParaServicios] Exception: " + e);
				throw new SeguridadException("SAFESIGNER_ERROR_GENERAL");
			}
		}
		return dict;
	}

	/**
	 * Genera una llave de seguridad dados los datos de una transacci�n.
	 * Esta llave luego es utilizada, por ejemplo, en la aplicaci�n de safesigner para
	 * generar la segunda clave.
	 * <p>
	 * Registro de versiones:<ul>
	 *
	 * <li>1.0 05/06/2013 Yon Sing Sius.(ExperimentoSocial): Versi�n inicial.
	 * <li>1.1 27/04/2016 Victor Enero (Orand) - Pablo Rompentin (Ing. Soft. BCI) : En el metodo SegundaClaveUtil.getUsuarioSegundaClave
	 * se traspasa el parametro HttpServletRequest para obtener el usuario desde sessionBCI o desde el token Oauth.
	 * <li>1.2 10/07/2017 Luis L�pez Alamos (SEnTRA) - Ricardo Carrasco C�ceres (Ing. Soft. BCI): Se agrega seteo de valor null a llamada de metodo generarLLave, puesto que
     * la implementaci�n actual no contempla modificaciones sobre sevicios llamados desde REST.</li>
     * 
	 * </ul>
	 * <p>
	 * @param camposDeLlave para firmar la transacci�n.
	 * @throws SeguridadException excepci�n de seguridad.
	 * @return Llave generada
	 * @since 1.3
	 */
	public RepresentacionSegundaClave generarLlave(CamposDeLlaveTO camposDeLlave) throws SeguridadException {

		UsuarioSegundaClaveTO usuarioSegundaClave;

		if (logger.isEnabledFor(Level.DEBUG)) {
			logger.debug("[generarLlave][BCI_INI]");
		}

		try {
			usuarioSegundaClave = SegundaClaveUtil.getUsuarioSegundaClave(servletRequest);
		}
		catch (OAuthProblemException e) {
			if (logger.isEnabledFor(Level.ERROR)) {
				logger.error("[generarLlave][BCI_FINEX] Error al tratar de obtener usuario", e);
			}
			throw new SistemaException("Error servicio");
		}


		EstadoSegundaClaveTO estadoSegundaClave = jerarquiaClaves.estadoSegundaClave(servicio,
				usuarioSegundaClave);
		LlaveSegundaClaveTO llave = null;

		if (estadoSegundaClave.getSegundaClaveEnum().isRequiereLlave()) {
			llave = jerarquiaClaves.generarLlave(estadoSegundaClave.getSegundaClaveEnum(),
					servicio, usuarioSegundaClave, camposDeLlave,null);
			if (logger.isDebugEnabled()) {
				logger.debug("[generarLlave][BCI_FINOK] valorLlave: " + llave);
			}
		}

		if (llave == null) {
			if (logger.isEnabledFor(Level.ERROR))
				logger.error("[generarLlave][BCI_FINEX] llave= null");
			throw new SistemaException("Error servicio");
		}

		if (!llave.isEstatus()) {
			throw new SeguridadException(String.valueOf(llave.getIdCodigoEstado()), llave.getGlosa());
		}

		return llave.getLlave();
	}

	/**
	 * Valida segunda clave de usuario, independiente si es Multipass o Safesigner.
	 * Si la segunda clave no es valida, se lanza excepci�n SeguridadExcepcion.
	 * <p>
	 * Registro de versiones:<ul>
	 *
	 * <li>1.0 05/06/2013 Yon Sing Sius.(ExperimentoSocial): Versi�n inicial.</li>
	 * <li>1.1 07/11/2013 Agustin Fuentes (ExperimentoSocial): Se modifica firma para recibir los campos
	 * llave cuando se trata de segunda clave SafeSigner.</li>
	 * <li>1.2 27/04/2016 Victor Enero (Orand) - Pablo Rompentin (Ing. Soft. BCI) : En el metodo SegundaClaveUtil.getUsuarioSegundaClave
	 * se traspasa el parametro HttpServletRequest para obtener el usuario desde sessionBCI o desde el token Oauth
	 * </ul>
	 * <p>
	 * @param clave a validar.
	 * @param camposDeLlave campos de llave.
	 * @throws SeguridadException excepci�n de seguridad.
	 * @since 1.3
	 */
	public void validarSegundaClave(String clave, wcorp.model.seguridad.CamposDeLlaveTO camposDeLlave)
			throws SeguridadException {

		UsuarioSegundaClaveTO usuarioSegundaClave;

		if (logger.isEnabledFor(Level.DEBUG)) {
			logger.debug("[validarSegundaClave][BCI_INI]");
		}

		try {
			usuarioSegundaClave = SegundaClaveUtil.getUsuarioSegundaClave(servletRequest);
		}
		catch (OAuthProblemException e) {
			if (logger.isEnabledFor(Level.ERROR)) {
				logger.error("[validarSegundaClave][BCI_FINEX] Error al tratar de obtener usuario", e);
			}
			throw new SistemaException("Error servicio");
		}

		EstadoSegundaClaveTO estadoSegundaClave = jerarquiaClaves.estadoSegundaClave(servicio,
				usuarioSegundaClave);
		EstrategiaSegundaClave estrategiaSegundaClave = EstrategiaAutenticacionFactory
				.obtenerEstrategiaSegundaClave(SegundaClaveUtil.
						toPrioridadSegundaClaveIdTO(estadoSegundaClave.getSegundaClaveEnum()));

		RepresentacionAlfanumericaSegundaClaveTO representacionAlfanumericaSegundaClave =
				new RepresentacionAlfanumericaSegundaClaveTO(clave);

		ParametrosEstrategiaSegundaClaveTO parametrosEstrategiaSegundaClaveTO =
				new ParametrosEstrategiaSegundaClaveTO(SegundaClaveUtil.toUsuarioSegundaClaveTO(usuarioSegundaClave),
						representacionAlfanumericaSegundaClave, ConectorStruts.getSessionBCI());

		parametrosEstrategiaSegundaClaveTO.setCanal(usuarioSegundaClave.getCanalId());

		// Se agrega debido cambio en EstrategiaSafesigner
		if((camposDeLlave != null) && (estrategiaSegundaClave instanceof EstrategiaSafeSigner)){
			parametrosEstrategiaSegundaClaveTO.setCamposDeLlave(camposDeLlave);

			// Se setea el servicio del que se est� validando la segunda clave.
			parametrosEstrategiaSegundaClaveTO.setServicio(servicio);
		}

		ResultadoOperacionSegundaClaveTO resultado = estrategiaSegundaClave.
				autenticar(parametrosEstrategiaSegundaClaveTO);

		if (logger.isEnabledFor(Level.DEBUG)) {
			logger.debug("[validarSegundaClave][BCI_FINOK]");
		}

		if (resultado.getIdCodigoEstado() != AUTENTICACION_OK){
			if (logger.isDebugEnabled()) {
				logger.debug("[validarSegundaClave][BCI_FINEX] Validacion erronea " + resultado.getGlosa());
			}
			throw new SeguridadException(String.valueOf(resultado.getIdCodigoEstado()), resultado.getGlosa());
		}
	}

	/**
	 * <p>
	 * Constructor del log.
	 * </p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 ??/??/???? ?: Version inicial.</li>
	 * </ul>
	 *
	 * @since ?
	 * @return logger instancia de logger.
	 **/
	public Logger getLogger() {
		if (logger == null) {
			logger = Logger.getLogger(this.getClass());
		}
		return logger;
	}


	/**
	 * Sobrecarga del m�todo validarSegundaClave() utiliza como parametro una
	 * instancia de cl.bci.infraestructura.web.seguridad.segundaclave.to.CamposDeLlaveTO
	 * <p>
	 * Registro de versiones:<ul>
	 *
	 * <li>1.0 21/07/2014 Ignacio Gonz�lez.(TINet): Versi�n inicial.</li>
	 *
	 * </ul>
	 * <p>
	 * @param clave valor clave a validar.
	 * @param camposDeLlave campos de llave.
	 * @throws SeguridadException excepci�n de seguridad.
	 * @since 1.5
	 */
	public void validarSegundaClave(String clave, CamposDeLlaveTO camposDeLlave) throws SeguridadException {
		if(logger.isEnabledFor(Level.INFO)){
			logger.info("[validarSegundaClave]["+cliente.getRut()+"][BCI_INI] Inicio.");
		}
		wcorp.model.seguridad.CamposDeLlaveTO camposDeLlaveModel = null;

		if (camposDeLlave != null) {
			camposDeLlaveModel = new wcorp.model.seguridad.CamposDeLlaveTO();
			camposDeLlaveModel.setHash(camposDeLlave.toHashMap());
			validarSegundaClave(clave, camposDeLlaveModel);
		}
		else {
			validarSegundaClave(clave, camposDeLlaveModel);
		}

		if(logger.isEnabledFor(Level.INFO)){
			logger.info("[validarSegundaClave]["+cliente.getRut()+"][BCI_FINOK]");
		}
	}
	public void setPortalMask(CanalTO portalMask) {
		this.portalMask = portalMask;
	}
	public CanalTO getPortalMask() {
		return portalMask;
	}

}