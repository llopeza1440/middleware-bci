package wcorp.aplicaciones.productos.servicios.seguridad.segundaclave.ui.actions;

import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

import wcorp.aplicaciones.productos.servicios.pagos.paguedirecto.struts.helper.ValidaSesion;
import wcorp.model.seguridad.ControllerBCI;
import wcorp.model.seguridad.EstrategiaEntrustSoftToken;
import wcorp.model.seguridad.LlaveSegundaClaveTO;
import wcorp.model.seguridad.SessionBCI;
import wcorp.serv.seguridad.NoSessionException;
import wcorp.util.ErroresUtil;
import wcorp.util.TablaValores;

/**
 * <b>Clase que contiene la l�gica de Estrategia de Autenticaci�n Entrust Token F�sico</b>
 * <p>
 * Registro de versiones:
 * <ul>
 *  <li>1.0 02/08/2017 Marcelo Fuentes (SEnTRA) - Ricardo Carrasco (Ing. Soft. BCI): Versi�n Inicial.</li>
 * </ul>
 * <b>Todos los derechos reservados por Banco de Cr�dito e Inversiones.</b>
 * <p>
 */
public class ConsultarEstadoDesafioSegundaClave extends DispatchAction {
    
    /**
     * tabla de par�metros de entrust.
     */
    private static final String TABLA_PARAMETROS_ENTRUST = "entrust.parametros";
    
    /**
     * Tipo de Segunda Clave Softtoken Entrust.
     */
    private static final String ENTRUST_SOFTTOKEN = TablaValores.getValor(TABLA_PARAMETROS_ENTRUST, "nombreSoftToken", "desc");
    
    /**
     * Log de la clase.
     */
    private transient Logger logger = (Logger) Logger.getLogger(ConsultarEstadoDesafioSegundaClave.class);
    
    
    /**
     * M�todo que consulta el estado de un desafio Entrust SofToken Online.
     * <p>
     * Registro de versiones: 
     * <ul>
     * <li>1.0 02/08/2017 Marcelo Fuentes (SEnTRA) - Ricardo Carrasco (Ing. Soft. BCI): Version inicial.</li>
     * </ul>
     * <p> 
     * @param mapping representa el mapeo de una instancia del formulario.
     * @param form ActionForm asociado al ActionMapping.
     * @param request la solicitud HTTP que se esta procesando.
     * @param response la respuesta entregada.
     * @since 1.0
     */
    public void consultarEstadoDesafioOperacion(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response) {
        getLogger().info("[consultarEstadoDesafioOperacion][BCI_INI]");
        PrintWriter out = null;
        try {
            if (request == null) {
                throw new NoSessionException("NOSESSION");
            }
            
            HttpSession hs = request.getSession(false);
            if (hs == null) {
                throw new NoSessionException("NOSESSION");
            }
            
            SessionBCI sessionBci = (SessionBCI)hs.getAttribute("sessionBci");
            if (sessionBci == null) {
                throw new NoSessionException("NOSESSION");
            }
            
            sessionBci.checkAlive();
            
            response.setContentType("text/plain;charset=utf-8");
            out = response.getWriter();
            
            String id = (String)sessionBci.getAttrib("transactionId");
            EstrategiaEntrustSoftToken estrategiaConsultada = new EstrategiaEntrustSoftToken();
            String resultado = estrategiaConsultada.consultarEstadoDesafio(id);
            
            if (getLogger().isEnabledFor(Level.INFO)) {
                getLogger().info("[consultarEstadoDesafioOperacion][BCI_FINOK] resultado:[" + resultado + "]");
            }
            out.print(resultado);
        }
        catch (Exception e) {
            if (getLogger().isEnabledFor(Level.ERROR)) {
                getLogger().error("[consultarEstadoDesafioOperacion][BCI_FINEX] Exception: " + ErroresUtil.extraeStackTrace(e));
            }
        }
        finally {
            if (out != null) {
                out.close();
            }
        }
    }
    
    /**
     * <p>
     * M�todo que genera la llave QR y redirecciona a la pagina para presentacion al usuario.
     * </p>
     * 
     * Registro de versiones:
     * <ul>
     * <li>1.0 02/08/2017, Marcelo Fuentes (SEnTRA) - Ricardo Carrasco (Ing. Soft. BCI): Versi�n Inicial.</li>
     * </ul>
     * <p>
     * 
     * @param mapping representa el mapeo de una instancia del formulario.
     * @param actionForm ActionForm asociado al ActionMapping.
     * @param request la solicitud HTTP que se esta procesando.
     * @param response la respuesta entregada.
     * @return Env�a los datos a la p�gina de respuesta.
     * @since 1.0
     */
    public ActionForward generarLlaveQR(ActionMapping mapping, ActionForm actionForm, HttpServletRequest request,
            HttpServletResponse response) {
        getLogger().info("[generarLlaveQR][BCI_INI] iniciado.");
        
        try {
            
            HttpSession hs = request.getSession(false);
            SessionBCI sessionBci = (SessionBCI)hs.getAttribute(ValidaSesion.SESSION_BCI);
            
            if (sessionBci == null) {
                throw new NoSessionException("NOSESSION");
            }
            
            sessionBci.checkAlive();
            
            String servicio = sessionBci.getParametrosEstrategiaSegundaClaveTO().getServicio();
            
            LlaveSegundaClaveTO llavesQREntrust = ControllerBCI.generarLlave(ENTRUST_SOFTTOKEN, 
                                                                            servicio, 
                                                                            sessionBci.getParametrosEstrategiaSegundaClaveTO().getUsuarioSegundaClave(), 
                                                                            sessionBci.getParametrosEstrategiaSegundaClaveTO().getCamposDeLlave(), 
                                                                            "OFFLINE");
            
            if (getLogger().isInfoEnabled()) {
                getLogger().info("[generarLlaveQR] vuelta largo total  " + llavesQREntrust.getLlaves().length);
            }
            
            getLogger().debug("[generarLlaveQR] Seteando la llave");
            
            request.setAttribute("servicioQR", servicio); 
            request.setAttribute("imagenesqr", llavesQREntrust.getLlaves());
           
           getLogger().info("[generarLlaveQR] [BCI_FINOK]");
           
           return mapping.findForward("segclaveOffline");
           
        }
        catch (Exception e) {
            if (getLogger().isEnabledFor(Level.ERROR)) {
                getLogger().error("[generarLlaveQR][BCI_FINEX] PagueDirectoException", e);
            }
            return mapping.findForward("error");
        }
        
    }
    
    /**
     * <p>
     * M�todo que obtiene un desafio Entrust SofToken Online y guarda la llave en la sesi�n.
     * </p>
     * 
     * Registro de versiones:
     * <ul>
     * <li>1.0 02/08/2017, Marcelo Fuentes (SEnTRA) - Ricardo Carrasco (Ing. Soft. BCI): Versi�n Inicial.</li>
     * </ul>
     * <p>
     * 
     * @param mapping representa el mapeo de una instancia del formulario.
     * @param actionForm ActionForm asociado al ActionMapping.
     * @param request la solicitud HTTP que se esta procesando.
     * @param response la respuesta entregada.
     * @since 1.0
     */
    public void obtenerDesafioOnline(ActionMapping mapping, ActionForm actionForm, HttpServletRequest request,
            HttpServletResponse response) {
        getLogger().info("[obtenerDesafioOnline][BCI_INI]");
        PrintWriter out = null;
        try {
            if (request == null) {
                throw new NoSessionException("NOSESSION");
            }
            
            HttpSession hs = request.getSession(false);
            if (hs == null) {
                throw new NoSessionException("NOSESSION");
            }
            
            SessionBCI sessionBci = (SessionBCI)hs.getAttribute("sessionBci");
            if (sessionBci == null) {
                throw new NoSessionException("NOSESSION");
            }
            
            sessionBci.checkAlive();
            
            response.setContentType("text/plain;charset=utf-8");
            out = response.getWriter();
            
            LlaveSegundaClaveTO llave = ControllerBCI.generarLlave(ENTRUST_SOFTTOKEN, 
                                                                   sessionBci.getParametrosEstrategiaSegundaClaveTO().getServicio(), 
                                                                   sessionBci.getParametrosEstrategiaSegundaClaveTO().getUsuarioSegundaClave(), 
                                                                   sessionBci.getParametrosEstrategiaSegundaClaveTO().getCamposDeLlave(), 
                                                                   "ONLINE");
            
            if (getLogger().isEnabledFor(Level.DEBUG)) {
                getLogger().debug("[obtenerDesafioOnline] Estado de la llave : " + llave.isEstatus());
            }
            
            getLogger().debug("[obtenerDesafioOnline] Seteando la llave");
            
            sessionBci.setAttrib("transactionId", llave.getSerial());
           
            getLogger().info("[obtenerDesafioOnline] [BCI_FINOK]");
            
            out.print("OK");
        }
        catch (Exception e) {
            if (getLogger().isEnabledFor(Level.ERROR)) {
                getLogger().error("[obtenerDesafioOnline][BCI_FINEX] Exception: " + ErroresUtil.extraeStackTrace(e));
            }
        }
        finally {
            if (out != null) {
                out.close();
            }
        }
    }
    
    /**
     * <p>
     * M�todo que obtiene el objeto getLogger().
     * </p>
     * 
     * Registro de versiones:
     * <ul>
     * <li>1.0 02/08/2017,  Marcelo Fuentes (SEnTRA) - Ricardo Carrasco (Ing. Soft. BCI): Versi�n Inicial.</li>
     * </ul>
     * <p>
     * 
     * @return el logger.
     * @since 1.0
     */
    private Logger getLogger() {
        if ( logger == null ) {
            logger = Logger.getLogger( this.getClass() );
        }
        return logger;
    }
    
}
