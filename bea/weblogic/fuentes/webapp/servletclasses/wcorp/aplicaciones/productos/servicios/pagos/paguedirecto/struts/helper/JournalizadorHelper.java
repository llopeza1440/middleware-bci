package wcorp.aplicaciones.productos.servicios.pagos.paguedirecto.struts.helper;

import java.util.Date;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import wcorp.model.seguridad.SessionBCI;
import wcorp.util.ErroresUtil;
import wcorp.util.RUTUtil;
import wcorp.util.journal.Eventos;
import wcorp.util.journal.Journal;

/**
 * Clase que se encarga de Journalizar la aplicaci�n.
 * 
 * <p>
 * Registro de Versiones:
 * <ul>
 * <li>1.0 08/11/2007, Robson Watt (Neoris Chile): versi�n inicial
 * <li>1.0 04/04/2014, Pablo Salazar (Imagemaker IT): Se corrige la clase para que este reciba la sesi�n
 *                                                   y obtenga el canal de la Journalizacion.
 *                                                   
 * <li>1.2 03/09/2014, Mauricio Hernandez (Imagemaker IT): Se modifica el metodo journaliza para que registre
     * 															 el rut del cliente en pague directo.                                                 
 * <li>1.3 02/08/2017, Marcelo Fuentes (SEnTRA) - Ricardo Carrasco (Ing. Soft. BCI): Se sobrecarga m�todo
 * {@link #journaliza(SessionBCI, String, String, String, String, String)}.
 * Se agrega metodo {@link #getLogger()}. Se elimina modificador "final" de la variable {@link #LOGGER}.</li>
 * </ul>
 * </p>
 * 
 * <p>
 * <b>Todos los derechos reservados por Banco de Cr�dito e Inversiones.</b>
 * </p>
 */
public class JournalizadorHelper {

    /** LOGGER de la clase. */
    private static Logger LOGGER = (Logger) Logger.getLogger(JournalizadorHelper.class);
            
    /**
     * M�todo que Journaliza la aplicaci�n de Pague Directo Monex.
     * 
     * <p>
     * Registro de Versiones:
     * <ul>
     * <li>1.0 08/11/2007, Robson Watt (Neoris Chile): versi�n inicial
     * <li>1.0 04/04/2014, Pablo Salazar (Imagemaker IT): Se corrige la clase para que este reciba la sesi�n
     *                                                   y obtenga el canal de la Journalizacion.
     * <li>1.2 03/09/2014, Mauricio Hernandez (Imagemaker IT): Se modifica el metodo journaliza para que registre
     * 															 el rut del cliente en pague directo.
     * <li>1.3 02/08/2017  Marcelo Fuentes (SEnTRA) - Ricardo Carrasco (Ing. Soft. BCI): Se sobrecarga metodo moviendo
     * toda la logica al metodo nuevo.</li>
     * </ul>
     * </p>
     * 
     * @param session la session bci.
     * @param codEvento el codigo de evento.
     * @param subCodNeg el subcodigo de negocio.
     * @param clavePrincipal la clave principal.
     * @param idProducto el id del producto.
     * @param codigo C�digo o mensaje de la excepci�n.
     * @since 1.0
     */
    public static void journaliza(SessionBCI session, String codEvento, String subCodNeg, String clavePrincipal,
            String idProducto, String codigo) {
        journaliza(session, codEvento, subCodNeg, clavePrincipal, idProducto, codigo, null, null);
    }
    
    /**
     * M�todo que Journaliza la aplicaci�n de Pague Directo Monex.
     * <p>
     *
     * Registro de versiones:<ul>
     * <li> 1.0 02/08/2017,  Marcelo Fuentes (SEnTRA) - Ricardo Carrasco (Ing. Soft. BCI): Versi�n Inicial.</li>
     * </ul>
     * <p>
     * @param session la session bci.
     * @param codEvento el codigo de evento.
     * @param subCodNeg el subcodigo de negocio.
     * @param clavePrincipal la clave principal.
     * @param idProducto el id del producto.
     * @param codigo C�digo o mensaje de la excepci�n.
     * @param campoVariable un campo variable.
     * @param estadoEventoNegocio el estado de la operacion a journalizar.
     * 
     * @since 1.3
     */
    public static void journaliza(SessionBCI session, String codEvento, String subCodNeg, String clavePrincipal,
            String idProducto, String codigo, String campoVariable, String estadoEventoNegocio) {
        
        char dvRut = '0';
        
        getLogger().debug("[PagueDirectoMonex][Ingreso al proceso de Journalizaci�n]");
        
        try {

            Journal journal = new Journal(session);
            Eventos evento = new Eventos();
            evento.setCodEventoNegocio(codEvento);
            evento.setSubCodEventoNegocio(subCodNeg);
            evento.setClavePrincipal(clavePrincipal);
            evento.setIdProducto(idProducto);
            evento.setFechaHora(new Date());
            
            if(clavePrincipal != null && !"".equals(clavePrincipal)){
                dvRut = RUTUtil.calculaDigitoVerificador(clavePrincipal);
                evento.setRutCliente(clavePrincipal);
            }
            evento.setDvCliente(String.valueOf(dvRut));

            if (session!=null && session.getCanal() !=null && session.getCanal().getNombre()!= null) {
                evento.setIdCanal(session.getCanal().getNombre());

                getLogger().debug("[PagueDirectoMonex][Se ingresa canal]");
            }

            if (codigo != null) {
                evento.setAtributo("codigo", codigo);
            }

            if(campoVariable != null) {
                evento.setCampoVariable(campoVariable);
            }
            
            if(estadoEventoNegocio != null) {
                evento.setEstadoEventoNegocio(estadoEventoNegocio);
            }
            
            journal.journalizar(evento);
        }
        catch (Exception e) {
            if (getLogger().isEnabledFor(Level.ERROR)) { 
                getLogger().error(ErroresUtil.extraeStackTrace(e));
                getLogger().error("No se pudo registrar en el Journalizador de Movimientos, numero de Evento :"
                        + codEvento);
            }
        }
    }
    
    /**
     * Metodo encargado de obtener el logger().
     * <p>
     *
     * Registro de versiones:<ul>
     * <li> 1.0 02/08/2017,  Marcelo Fuentes (SEnTRA) - Ricardo Carrasco (Ing. Soft. BCI): Versi�n Inicial.</li>
     * </ul>
     * <p>
     * @return el logger.
     * 
     * @since 1.3
     */
    private static Logger getLogger() {
        if (LOGGER == null) {
            LOGGER = Logger.getLogger(JournalizadorHelper.class);
        }
        return LOGGER;
    }
}
