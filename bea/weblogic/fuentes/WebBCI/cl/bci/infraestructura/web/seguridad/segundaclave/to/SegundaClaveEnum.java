package cl.bci.infraestructura.web.seguridad.segundaclave.to;

/**
 * 
 * <b>SegundaClaveEnum</b>
 * <p>
 * Enum de prioridad de segunda clave.
 * </p>
 * 
 * <p>Registro de versiones:</p>
 * 
 * <ul>
 * <li> 1.0 05/06/2013, Victor Hugo Enero (Orand): version inicial</li>
 * <li> 1.1 08/10/2013, Rodrigo Briones O (Bci),
 * 						Victor Hugo Enero (Orand); Se agrega m�todo atributo
 * SINCLAVE para considerar cliente sin segunda clave.
 * <li> 1.2 12/12/2016 Darlyn Delgado P�rez (SEnTRA) - Daniel Araya Ureta (Ing. Soft. BCI): Se agrega a m�todo ETOKENMULTIBROWSER como llave para nueva estrategia a incluir.</li>
 * <li> sin doc yet: Se agrega softtoken </li>
 * <li>1.3 10/07/2017, Luis L�pez Alamos (SEnTRA) - Ricardo Carrasco C�ceres (Ing. Soft. BCI): Se agregan constantes predefinidas ENTRUST_SOFTTOKEN y ENTRUST_TOKEN. Adem�s, se modifica el m�todo {@link #obtieneSegundaClaveEnum(String)}}</li> 
 * </ul>
 * 
 * <p><b>Todos los derechos reservados por Banco de Cr�dito e Inversiones.</b></p>
 * 
 */
public enum SegundaClaveEnum {
    SAFESIGNER {
        public String getNombre() {
            return "SafeSigner";
        }

        public boolean isRequiereLlave() {
            return true;
        }
    },
    MULTIPASS {
        public String getNombre() {
            return "Multipass";
        }

        public boolean isRequiereLlave() {
            return false;
        }
    },
    INTERNET {
        public String getNombre() {
            return "Internet";
        }

        public boolean isRequiereLlave() {
            return false;
        }
    },
    ETOKEN {
        public String getNombre() {
            return "EToken";
        }

        public boolean isRequiereLlave() {
            return false;
        }
    },
    ETOKENMULTIBROWSER {
        public String getNombre() {
            return "ETokenMultiBrowser";
        }

        public boolean isRequiereLlave() {
            return false;
        }
    },
    SINCLAVE {
        public String getNombre() {
            return "SinClave";
        }

        public boolean isRequiereLlave() {
            return false;
        }
    },
    ENTRUST_SOFTTOKEN {
        public String getNombre() {
            return "EntrustSoftToken";
        }

        public boolean isRequiereLlave() {
            return true;
        }
    },
    ENTRUST_TOKEN {
        public String getNombre() {
            return "EntrustToken";
        }
        public boolean isRequiereLlave() {
            return false;
        }
    };

    
    /**
     * Declaraci�n de m�todo public para ser implementado por cada instancia. Retorna metodo 
     * autenticacion.
     * <P>
     * Registro de versiones:<UL>
     * <LI>1.0 05-06-2013 Victor Hugo Enero (Orand): versi�n inicial.</LI>
     * </UL>
     * <P>
     * @return String El metodo de autenticacion.
     */
    public abstract String getNombre();
    
    /**
     * Declaraci�n de m�todo public para ser implementado por cada instancia. Verifica si el metodo 
     * autenticacion genera una llave previa.
     * <P>
     * Registro de versiones:<UL>
     * <LI>1.0 05-06-2013 Victor Hugo Enero (Orand): versi�n inicial.</LI>
     * </UL>
     * <P>
     * @return boolean true si el canal est� dentro del dominio.
     */
    public abstract boolean isRequiereLlave();
    
    
    /**
     * M�todo utilizado para obtener el metodo de autenticacion, mediante el nombre.
     * <P>
     * Registro de versiones:<UL>
     * <LI>1.0 05/06/2013, Victor Hugo Enero (Orand): versi�n inicial.</LI>
     * <LI>1.1 08/10/2013, Rodrigo Briones O. (Bci),
     * 					   Victor Hugo Enero (Orand): Se agrega opcion SINCLAVE.</LI>
     * <li>1.2 20/03/2017, Darlyn Delgado - Daniel Araya Ureta  (Ing. Soft. BCI): Se agrega opcion ETOKENMULTIBROWSER.</li>
     * <li>1.3 10/07/2017, Luis L�pez Alamos (SEnTRA) - Ricardo Carrasco C�ceres (Ing. Soft. BCI): Se agrega opci�n ENTRUST_SOFTOKEN Y ENTRUST_TOKEN.</li>
     * </UL>
     * <P>
     * @param metodoAutenticacion metodo de autenticacion.
     * @return objeto SegundaClaveEnum.
     */
    public static SegundaClaveEnum obtieneSegundaClaveEnum(String metodoAutenticacion) {
        if (SegundaClaveEnum.ENTRUST_SOFTTOKEN.getNombre().equalsIgnoreCase(metodoAutenticacion))
            return SegundaClaveEnum.ENTRUST_SOFTTOKEN;
        else if (SegundaClaveEnum.ENTRUST_TOKEN.getNombre().equalsIgnoreCase(metodoAutenticacion))
            return SegundaClaveEnum.ENTRUST_TOKEN;
        else if (SegundaClaveEnum.SAFESIGNER.getNombre().equalsIgnoreCase(metodoAutenticacion))
            return SegundaClaveEnum.SAFESIGNER;
        else if (SegundaClaveEnum.MULTIPASS.getNombre().equalsIgnoreCase(metodoAutenticacion))
            return SegundaClaveEnum.MULTIPASS;
        else if (SegundaClaveEnum.INTERNET.getNombre().equalsIgnoreCase(metodoAutenticacion))
            return SegundaClaveEnum.INTERNET;
        else if (SegundaClaveEnum.ETOKEN.getNombre().equalsIgnoreCase(metodoAutenticacion))
            return SegundaClaveEnum.ETOKEN;
        else if (SegundaClaveEnum.ETOKENMULTIBROWSER.getNombre().equalsIgnoreCase(metodoAutenticacion))
            return SegundaClaveEnum.ETOKENMULTIBROWSER;
        else if (SegundaClaveEnum.SINCLAVE.getNombre().equalsIgnoreCase(metodoAutenticacion))
            return SegundaClaveEnum.SINCLAVE;
        else 
        	return null;
    }

}

