package cl.bci.infraestructura.web.seguridad.segundaclave;

import java.io.IOException;

import javax.faces.context.FacesContext;

import org.apache.log4j.Logger;

import wcorp.model.seguridad.ParametrosEstrategiaSegundaClaveTO;
import wcorp.model.seguridad.RepresentacionSegundaClave;
import wcorp.serv.seguridad.SeguridadException;
import wcorp.util.SistemaException;
import wcorp.util.StringUtil;

import cl.bci.infraestructura.web.seguridad.segundaclave.mb.FirmaDigitalModelMB;
import cl.bci.infraestructura.web.seguridad.segundaclave.mb.JerarquiaClavesModelMB;
import cl.bci.infraestructura.web.seguridad.segundaclave.to.CamposDeLlaveTO;
import cl.bci.infraestructura.web.seguridad.segundaclave.to.EstadoSegundaClaveTO;
import cl.bci.infraestructura.web.seguridad.segundaclave.to.ItemDeLlaveTO;
import cl.bci.infraestructura.web.seguridad.segundaclave.to.LlaveSegundaClaveTO;
import cl.bci.infraestructura.web.seguridad.segundaclave.to.SegundaClaveEnum;
import cl.bci.infraestructura.web.seguridad.segundaclave.to.UsuarioSegundaClaveTO;
import cl.bci.infraestructura.web.seguridad.util.ConectorStruts;
import cl.bci.infraestructura.ws.to.RespuestaSegundaClaveWSTO;

/**
 * Componente cuya funci�n es la de realizar operaciones de generaci�n y validaci�n de segunda clave que, a
 * diferencia de {@link SegundaClaveUIInput} la cual extiende, puede ser usada bajo un contexto que no es el
 * de JSF (por ejemplo servicios REST).
 * <p>
 * Registro de versiones:<ul>
 * <li>1.0 11/08/2015 Luis Silva (TINet): Versi�n inicial.</li>
 * <li>2.0 10/07/2017 Luis L�pez Alamos (SEnTRA) - Ricardo Carrasco C�ceres (Ing. Soft. BCI): Se modifica m�todo {@link #generarLlave()}}</li>
 * </ul>
 * <p>
 * <b>Todos los derechos reservados por Banco de Cr�dito e Inversiones.</b>
 * <p>
 */
public class SegundaClaveInput extends SegundaClaveUIInput {

    /**
     * Largo que debe tener una clave multipass.
     */
    private static final int LARGO_CLAVE_MULTIPASS_1 = 6;

    /**
     * Largo que debe tener una clave multipass.
     */
    private static final int LARGO_CLAVE_MULTIPASS_2 = 8;

    /**
     * Largo que debe tener una clave safesigner.
     */
    private static final int LARGO_CLAVE_SAFESIGNER = 7;

    /**
     * Largo m�nimo que debe tener una clave internet.
     */
    private static final int LARGO_MINIMO_CLAVE_INTERNET = 6;

    /**
     * Largo m�ximo que debe tener una clave internet.
     */
    private static final int LARGO_MAXIMO_CLAVE_INTERNET = 8;
    
    /**
     * Logger de la clase.
     */
    private static Logger logger = Logger.getLogger(SegundaClaveInput.class);

    /**
     * Variable puntero para ir agregando los campos de llave.
     */
    private int posicionItemsLlave;

    /**
     * Data a Firmar (para efectos de clave EToken).
     */
    private String dataToSign;

    /**
     * Data firmada (para efectos de clave EToken).
     */
    private String dataSignature;

    /**
     * Identificador del usuario (rut) con el que se est� operando para efectos de segunda clave.
     */
    private String idUsuario;

    /**
     * Constructor de la clase. Determina el m�todo de la segunda clave a utilizar tomando como base el
     * identificador de servicio pasado como par�metro.
     * <p>
     * Registro de versiones:<ul>
     * 
     * <li>1.0 11/08/2015 Luis Silva (TINet): Versi�n inicial.
     * </ul><p>
     * @param servicio Identificador del servicio.
     * @throws SeguridadException En caso de ocurrir alg�n error al moemnto de consultar el estado de segunda
     * clave para el servicio
     * @since 1.0
     */
    public SegundaClaveInput(String servicio) throws SeguridadException {
        this.servicio = servicio;
        if (logger.isInfoEnabled()) {
            logger.info("[SegundaClaveInput][" + this.servicio + "][BCI_INI] Inicio");
        }
        this.posicionItemsLlave = 0;
        this.camposDeLlave = new CamposDeLlaveTO();
        try {
            usuarioSegundaClave = SegundaClaveUtil.getUsuarioSegundaClave();
            idUsuario = usuarioSegundaClave.getUsuarioId();
            if (logger.isDebugEnabled()) {
                logger.debug("[SegundaClaveInput][" + this.servicio + "] Usuario segunda clave ["
                    + usuarioSegundaClave.getUsuarioId() + "]");
            }
            jerarquiaClaves = (JerarquiaClavesModelMB) ConectorStruts.getAttribHttp("jerarquiaClavesModelMB");
            if (jerarquiaClaves == null) {
                jerarquiaClaves = new JerarquiaClavesModelMB();
            }
            estadoSegundaClave = jerarquiaClaves.estadoSegundaClave(this.servicio, usuarioSegundaClave);
            if (logger.isDebugEnabled()) {
                logger.debug("[SegundaClaveInput][" + this.idUsuario + "_" + this.servicio
                    + "] estado segunda clave [" + estadoSegundaClave + "]");
            }
            if (estadoSegundaClave.getIdCodigoEstado() == -1 || !estadoSegundaClave.isEstatus()) {
                logger.error("[SegundaClaveInput][" + this.idUsuario + "_" + this.servicio
                    + "] Estado de segunda clave no valido");
                throw new SeguridadException("SERV");
            }
        }
        catch (SeguridadException e) {
            logger.error("[SegundaClaveInput][" + this.idUsuario + "_" + this.servicio
                + "][BCI_FINEX][SeguridadException] Error al obtener estado de segunda clave, mensaje=<"
                + e.getMessage() + ">", e);
            throw e;
        }
        catch (Exception e) {
            logger.error("[SegundaClaveInput][" + this.idUsuario + "_" + this.servicio
                + "][BCI_FINEX][Exception] Error al obtener estado de segunda clave, mensaje=<"
                + e.getMessage() + ">", e);
            throw new SistemaException(e);
        }
        if (logger.isInfoEnabled()) {
            logger.info("[SegundaClaveInput][" + this.idUsuario + "_" + this.servicio
                + "][BCI_FINOK] Estado de segunda clave obtenido");
        }
    }

    @Override
    public void encodeBegin(FacesContext context) throws IOException {
        throw new UnsupportedOperationException();
    }

    @Override
    public void decode(FacesContext context) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Object saveState(FacesContext context) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void restoreState(FacesContext context, Object state) {
        throw new UnsupportedOperationException();
    }

    /**
     * Establece el valor de segunda clave para su posterior validaci�n.
     * <p>
     * Registro de versiones:<ul>
     * <li>1.0 11/08/2015 Luis Silva (TINet): Versi�n inicial.
     * </ul><p>
     * @param segundaClave Valor de segunda clave
     * @since 1.0
     */
    public void setSegundaClave(String segundaClave) {
        this.codeConfirmation = segundaClave;
    }

    /**
     * Agrega un par clave/valor necesario para la generaci�n de una llave.
     * <p>
     * Registro de versiones:<ul>
     * 
     * <li>1.0 11/08/2015 Luis Silva (TINet): Versi�n inicial.
     * </ul><p>
     * @param campo Nombre del campo
     * @param valor Valor asociado al campo
     * @since 1.0
     */
    public void agregarItemLlave(String campo, String valor) {
        ItemDeLlaveTO itemLlave = new ItemDeLlaveTO();
        itemLlave.setPosicion(posicionItemsLlave);
        itemLlave.setNombre(campo);
        itemLlave.setValorString(valor);
        camposDeLlave.agregarCampo(itemLlave);
        posicionItemsLlave++;
    }

    /**
     * Retorna valor de llave, en caso de que el m�todo se segunda clave asociado a un servicio as�
     * lo especifique.
     * <p>
     * Registro de versiones:<ul>
     * 
     * <li>1.0 11/08/2015 Luis Silva (TINet): Versi�n inicial.
     * <li>2.0 10/07/2017 Luis L�pez Alamos (SEnTRA) - Ricardo Carrasco C�ceres (Ing. Soft. BCI): Se agrega seteo de valor null a llamada de metodo generarLLave, puesto que
     * la implementaci�n actual no contempla modificaciones sobre sevicios llamados desde REST.</li>
     * </ul><p>
     * @return Valor de llave. <code>null</code> indica que el m�todo de segunda clave asociado al servicio no
     * requiere de llave.
     * @throws SeguridadException En caso de haber alg�n error en la generaci�n de la llave
     * @since 1.0
     */
    public RepresentacionSegundaClave generarLlave() throws SeguridadException {
        if (logger.isInfoEnabled()) {
            logger.info("[generarLlave][" + this.idUsuario + "_" + this.servicio + "][BCI_INI] Inicio");
        }
        try {
            if (!estadoSegundaClave.isEstatus() || estadoSegundaClave.getIdCodigoEstado() == -1) {
                logger.error("[generarLlave][" + this.idUsuario + "_" + this.servicio
                    + "] Estado de segunda clave no valido");
                throw new SeguridadException("SERV");
            }
            SegundaClaveEnum metodoSegundaClave =  estadoSegundaClave.getSegundaClaveEnum();
            if (!metodoSegundaClave.isRequiereLlave()) {
                logger.info("[generarLlave][" + this.idUsuario + "_" + this.servicio
                    + "][BCI_FINOK] No se genera llave porque no se requiere");
                return null;
            }
            else {
                if (logger.isDebugEnabled()) {
                    logger.debug("[generarLlave][" + this.idUsuario + "_" + this.servicio
                        + "] Campos de llave [" + camposDeLlave + "]");
                }
                LlaveSegundaClaveTO llave = jerarquiaClaves.generarLlave(metodoSegundaClave, servicio,
                    usuarioSegundaClave, camposDeLlave,null);
                if (logger.isInfoEnabled()) {
                    logger.info("[generarLlave][" + this.idUsuario + "_" + this.servicio
                        + "][BCI_FINOK] llave generada [" + llave + "]");
                }
                return llave.getLlave();
            }
        }
        catch (SeguridadException e) {
            logger.error("[generarLlave][" + this.idUsuario + "_" + this.servicio
                + "] Error al generar llave, mensaje=<" + e.getMessage() + ">", e);
            throw e;
        }
    }

    /**
     * Genera la llave agregandola a una instancia de {@link RespuestaSegundaClaveWSTO} junto con la informaci�n
     * del m�todo de segunda clave a utilizar.<br>
     * <strong>IMPORTANTE</strong>: Este m�todo s�lo deber�a utilizarse bajo el contexto de servicios REST.
     * Bajo otras ciscunstancias se debe utilizar {@link #generarLlave()}.
     * <p>
     * Registro de versiones:<ul>
     * 
     * <li>1.0 11/08/2015 Luis Silva (TINet): Versi�n inicial.
     * </ul><p>
     * @return Datos de m�todo de segunda clave (con llave incluida) en formato WSTO
     * @throws SeguridadException En caso de haber alg�n error en la generaci�n de la llave
     * @since 1.0
     */
    public RespuestaSegundaClaveWSTO generarLlaveRespuestaSegundaClaveWSTO() throws SeguridadException {
        if (logger.isInfoEnabled()) {
            logger.info("[generarLlaveRespuestaSegundaClaveWSTO][" + this.idUsuario + "_" + this.servicio
                + "][BCI_INI] Inicio");
        }
        RepresentacionSegundaClave llave = this.generarLlave();
        SegundaClaveEnum metodoSegundaClave = this.estadoSegundaClave.getSegundaClaveEnum();
        if (metodoSegundaClave.isRequiereLlave()
            && (llave == null || StringUtil.esVacio(llave.getClaveAlfanumerica()))) {
            logger.error("[generarLlaveRespuestaSegundaClaveWSTO][" + this.idUsuario + "_" + this.servicio
                + "][BCI_FINEX] No se genero llave");
            throw new SeguridadException("SERV");
        }
        else {
            RespuestaSegundaClaveWSTO respuesta = new RespuestaSegundaClaveWSTO();
            respuesta.setMetodoSegundaClave(metodoSegundaClave.getNombre());
            respuesta.setRequiereLlave(metodoSegundaClave.isRequiereLlave());
            if (respuesta.isRequiereLlave()) {
                respuesta.setLlave(llave.getClaveAlfanumerica());
            }
            if (logger.isInfoEnabled()) {
                logger.info("[generarLlaveRespuestaSegundaClaveWSTO][" + this.idUsuario + "_" + this.servicio
                    + "][BCI_FINOK] respuesta [" + respuesta + "]");
            }
            return respuesta;
        }
    }

    /**
     * Verifica que el valor establecido por medio de {@link #setSegundaClave(String)} sea v�lido considerando
     * el m�todo de segunda clave.
     * <p>
     * Registro de versiones:<ul>
     * 
     * <li>1.0 11/08/2015 Luis Silva (TINet): Versi�n inicial.
     * </ul><p>
     * @return <code>true</code> si la clave es v�lida, <code>false</code> en caso contrario
     * @throws SeguridadException En caso de haber alg�n error en la generaci�n de la clave
     * @since 1.0
     */
    @Override
    public boolean verificarAutenticacion() throws SeguridadException {
        if (logger.isInfoEnabled()) {
            logger.info("[verificarAutenticacion][" + this.idUsuario + "_" + this.servicio + "][BCI_INI] Inicio");
        }
        switch (estadoSegundaClave.getSegundaClaveEnum()) {
            case MULTIPASS:
                validarClaveMultipass();
                break;
            case SAFESIGNER:
                validarClaveSafeSigner();
                break;
            case INTERNET:
                validarClaveInternet();
                break;
            default:
                break;
        }
        boolean resultadoVerificacion = super.verificarAutenticacion();
        if (logger.isInfoEnabled()) {
            logger.info("[verificarAutenticacion][" + this.idUsuario + "_" + this.servicio
                + "][BCI_FINOK] Resultado verificacion [" + resultadoVerificacion + "]");
        }
        return resultadoVerificacion;
    }

    /**
     * Valida que una clave ingresada usando multipass sea v�lida.
     * <p>
     * Registro de versiones:<ul>
     * 
     * <li>1.0 11/08/2015 Luis Silva (TINet): Versi�n inicial.
     * </ul><p>
     * @throws SeguridadException En caso de que la clave no sea v�lida
     * @since 1.0
     */
    private void validarClaveMultipass() throws SeguridadException {
        if (StringUtil.esVacio(codeConfirmation)) {
            logger.error("[validarClaveMultipass][" + this.idUsuario + "_" + this.servicio
                + "][BCI_FINEX] Segunda clave vacia");
            throw new SeguridadException("MENSAJESINCLAVE");
        }
        else if (codeConfirmation.length() != LARGO_CLAVE_MULTIPASS_1
            && codeConfirmation.length() != LARGO_CLAVE_MULTIPASS_2) {
            logger.error("[validarClaveMultipass][" + this.idUsuario + "_" + this.servicio
                + "][BCI_FINEX] Clave no cumple con los largos especificados");
            throw new SeguridadException("TOKENFRASE07");
        }
    }

    /**
     * Valida que una clave ingresada usando safesigner sea v�lida.
     * <p>
     * Registro de versiones:<ul>
     * 
     * <li>1.0 11/08/2015 Luis Silva (TINet): Versi�n inicial.
     * </ul><p>
     * @throws SeguridadException En caso de que la clave no sea v�lida
     * @since 1.0
     */
    private void validarClaveSafeSigner() throws SeguridadException {
        if (StringUtil.esVacio(codeConfirmation)) {
            logger.error("[validarClaveSafeSigner][" + this.idUsuario + "_" + this.servicio
                + "][BCI_FINEX] Segunda clave vacia");
            throw new SeguridadException("MENSAJESINCLAVE");
        }
        else if (codeConfirmation.length() != LARGO_CLAVE_SAFESIGNER) {
            logger.error("[validarClaveSafeSigner][" + this.idUsuario + "_" + this.servicio
                + "][BCI_FINEX] Segunda clave no cumple con el largo valido");
            throw new SeguridadException("SAFESIGNER02");
        }
    }

    /**
     * Valida que una clave internet ingresada por un cliente sea v�lida.
     * <p>
     * Registro de versiones:<ul>
     * 
     * <li>1.0 11/08/2015 Luis Silva (TINet): Versi�n inicial.
     * </ul><p>
     * @throws SeguridadException En caso de que la clave no sea v�lida
     * @since 1.0
     */
    private void validarClaveInternet() throws SeguridadException {
        if (StringUtil.esVacio(codeConfirmation)) {
            logger.error("[validarClaveSafeSigner][" + this.idUsuario + "_" + this.servicio
                + "][BCI_FINEX] Segunda clave vacia");
            throw new SeguridadException("INGRESECLAVEINTERNET");
        }
        else if (codeConfirmation.length() < LARGO_MINIMO_CLAVE_INTERNET
            || codeConfirmation.length() > LARGO_MAXIMO_CLAVE_INTERNET) {
            logger.error("[validarClaveInternet][" + this.idUsuario + "_" + this.servicio
                + "][BCI_FINEX] Segunda clave no cumple con el largo valido");
            throw new SeguridadException("ERRORLARGOCLAVEINTERNET");
        }
    }

    /**
     * Obtiene los datos de las firma para cuando el m�todo de segunda clave sea E-TOKEN
     * <p>
     * Registro de versiones:<ul>
     * <li>1.0 11/08/2015 Luis Silva (TINet): Versi�n inicial.
     * </ul><p>
     * @param parametrosEstrategiaSegundaClaveTO Objeto que contiene los datos con los cuales se validar� la clave
     * @since 1.0
     */
    @Override
    protected void agregarDatosFirmaEToken(ParametrosEstrategiaSegundaClaveTO parametrosEstrategiaSegundaClaveTO) {
        firmaDigitalModelMB = (FirmaDigitalModelMB) ConectorStruts
            .getAttribHttp("firmaDigitalModelMB");
        if (firmaDigitalModelMB == null) {
            firmaDigitalModelMB = new FirmaDigitalModelMB();
        }
        String mensajeOriginal = firmaDigitalModelMB.getMensaje();
        parametrosEstrategiaSegundaClaveTO.setDataToSign(dataToSign);
        parametrosEstrategiaSegundaClaveTO.setDataSignature(dataSignature);
        parametrosEstrategiaSegundaClaveTO.setMensajeOriginal(mensajeOriginal);
    }

    @Override
    public String getFamily() {
        throw new UnsupportedOperationException();
    }

    @Override
    public EstadoSegundaClaveTO getEstadoSegundaClave() {
        return this.estadoSegundaClave;
    }

    @Override
    public UsuarioSegundaClaveTO getUsuarioSegundaClave() {
        return this.usuarioSegundaClave;
    }

    @Override
    public SegundaClaveEnum getSegundaClaveId() {
        return this.estadoSegundaClave.getSegundaClaveEnum();
    }

    public void setDataToSign(String dataToSign) {
        this.dataToSign = dataToSign;
    }

    public void setDataSignature(String dataSignature) {
        this.dataSignature = dataSignature;
    }

    @Override
    public String getTipoBotonDestino() {
        throw new UnsupportedOperationException();
    }

    @Override
    public void setTipoBotonDestino(String tipoBotonDestino) {
        throw new UnsupportedOperationException();
    }

    @Override
    public String getTipoBotonRetorno() {
        throw new UnsupportedOperationException();
    }

    @Override
    public void setTipoBotonRetorno(String tipoBotonRetorno) {
        throw new UnsupportedOperationException();
    }

    @Override
    public String getDestinoButtonOnclick() {
        throw new UnsupportedOperationException();
    }

    @Override
    public void setDestinoButtonOnclick(String destinoButtonOnclick) {
        throw new UnsupportedOperationException();
    }

    @Override
    public String getDestinoButtonRetornoOnclick() {
        throw new UnsupportedOperationException();
    }

    @Override
    public void setDestinoButtonRetornoOnclick(String destinoButtonRetornoOnclick) {
        throw new UnsupportedOperationException();
    }
}
