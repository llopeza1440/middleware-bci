package wcorp.bprocess.cuentas.vo;

import java.io.Serializable;
import java.sql.Timestamp;


/**
 * <pre>Data Value Object (DVO) de Alias para las Transferencias de Fondos</pre>
 *
 * Registro de Versiones:<ul>
 * <li>1.0 (12/08/2005, Ivo Willemsen (schema)): versi�n inicial
 * <li>1.1 (22/02/2007, Alejandro Ituarte (schema)): Aplica trim a nroCuenta
 * <li>1.2 (04/07/2007, Alejandro Ituarte (schema)): Aplica trim a resto de los campos
 * <li>1.3 (23/05/2008, David Salazar (SEnTRA)): se Agrega Tipo de Cuenta
 * <li>1.4 25/03/2009, Carlos Cerda I. (SEnTRA): se a�ade m�todo {@link #toString()}.
 * <li>1.5 09/06/2010, Carlos Cerda Iglesias (Orand Innovaci�n): Se a�aden los atributos 'estado', 'idValidacion' 
 * <li>1.6 09/03/2011   Ma. Magdalena D�az R. (Imagemaker IT): Se agregan los atributos fechaIngreso y fechaAutorizacion
 *												y sus respectivos get y set.												y sus respectivos m�todos getter y setter.
 * <li>1.7 13/04/2012 Eduardo Villagr�n Morales (Imagemaker IT): Se agrega atributo 'favorito' y 'nombreBanco'
 * 					en el marco de la gesti�n de alias de TTFF favoritos o no. 
 * <li>1.8 20/06/2012 Diego urrutia Guerra (Imagemaker IT): se agrga atributo "bancoBloqueado" para distinguir
 *                  cuando un banco se encuentre bloqueado.
 * <li>1.9 05/09/2017 Luis Lopez Alamos (SEnTRA) - Ricardo Carrasco C�ceres (Ing. Soft. BCI): Se agrega atributo {@link #restriccionRegla24Horas} con sus respectivos m�todos
 * 	conmutadores, adem�s se corrige m�todo {@link #toString()} de acuerdo a normativa vigente a la fecha.</li>                  
 * </ul>
 * <B>Todos los derechos reservados por Banco de Cr�dito e Inversiones.</B>
 * <P>
 */
public class AliasTTFFDVO implements Serializable {

    /** 
    * N�mero de versi�n utilizado durante la serializaci�n. 
    */
   private static final long serialVersionUID = 2L;


  /**
   * Clave Primaria de la tabla ali
  **/
  private long codigo;
  /**
   * El rut del cliente
  **/
  private String rutCliente;
  /**
   * El alias
  **/
  private String alias;
  /**
   * El nombre
  **/
  private String nombre;
  /**
   * El rut
  **/
  private String rut;
  /**
   * El numero de la cuenta
  **/
  private String numeroCuenta;
  /**
   * El codigo del tipo de Cuenta
  **/
	private int tipoCuentaCodigo;
  /**
   * El codigo del banco.
  **/
  private int bancoCodigo;
  /**
   * El email del cliente
  **/
  private String email;
	
	/**
   * Estado del alias en la base de datos. 
   * 
   * <p>Actualmente los valores aceptados son:<ul>
   * <li>'P': pendiente</li>
   * <li>'N': nuevo</li>
   * <li>'A': activo</li>
   * </ul>
   * @see {@link #isAutorizado()}.
	 **/
	private char estado;
	
	/**
	 * C�digo de validaci�n con el cual se autoriza el alias.
	 **/
	private String idValidacion;

    /**
     * Fecha de ingreso del alias de TTFF.
     **/
    private Timestamp fechaIngreso;

    /**
     * Fecha de autorizaci�n del alias de TTFF.
     **/
    private Timestamp fechaAutorizacion;

	/**
	 * Indica si el alias es favorito.
	 */
	private boolean favorito;
	
	/**
	 * Nombre del banco.
	 */
	private String nombreBanco;

	/**
	 * Indica si el banco se encuentra bloqueado.
	 */
	private boolean bancoBloqueado;

	/**
	 * Flag que indica si el alias de transferencia puede volver a crearse inmediatamente una vez eliminado.
	 */
	private boolean restriccionRegla24Horas = true;


  public AliasTTFFDVO() {
  }

  public long getCodigo() {
     return codigo;
   }

  public void setCodigo(long codigo) {
     this.codigo = codigo;
  }

  public String getRutCliente() {
    return rutCliente == null ? null : rutCliente.trim();
   }

  public void setRutCliente(String rutCliente) {
    this.rutCliente = rutCliente;
  }

  public String getAlias() {
    return alias == null ? null : alias.trim();
   }

  public void setAlias(String alias) {
     this.alias = alias;
  }

  public String getNombre() {
    return nombre == null ? null : nombre.trim();
   }

  public void setNombre(String nombre) {
     this.nombre = nombre;
  }

  public String getRut() {
    return rut == null ? null : rut.trim();
   }

  public void setRut(String rut) {
    this.rut = rut;
  }

  public String getNumeroCuenta() {
     return numeroCuenta == null ? null : numeroCuenta.trim();
   }

  public void setNumeroCuenta(String numeroCuenta) {
     this.numeroCuenta = numeroCuenta;
  }

  public int getBancoCodigo() {
     return bancoCodigo;
   }

  public void setBancoCodigo(int bancoCodigo) {
     this.bancoCodigo = bancoCodigo;
  }

  public String getEmail() {
    return email == null ? null : email.trim();
   }

  public void setEmail(String email) {
     this.email = email;
  }

	public int getTipoCuentaCodigo() {
	return tipoCuentaCodigo;
  }

	public void setTipoCuentaCodigo(int tipoCuentaCodigo) {
	this.tipoCuentaCodigo = tipoCuentaCodigo;
  }

	public char getEstado() {
		return estado;
	}

	public void setEstado(char estado) {
		this.estado = estado;
	}

	public String getIdValidacion() {
		return idValidacion;
	}

	public void setIdValidacion(String idValidacion) {
		this.idValidacion = idValidacion;
	}

	public Timestamp getFechaIngreso() {
        return fechaIngreso;
    }

	public void setFechaIngreso(Timestamp fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }
    public Timestamp getFechaAutorizacion() {
        return fechaAutorizacion;
    }

    public void setFechaAutorizacion(Timestamp fechaAutorizacion) {
        this.fechaAutorizacion = fechaAutorizacion;
    }

	
	public String toString(){
        StringBuffer sb = new StringBuffer();
        sb.append("AliasTTFFDVO: codigo[").append(codigo);
        sb.append("], rutCliente[").append(rutCliente);
        sb.append("], alias[").append(alias);
        sb.append("], nombre[").append(nombre);
        sb.append("], rut[").append(rut);
        sb.append("], numeroCuenta[").append(numeroCuenta);
        sb.append("], tipoCuentaCodigo[").append(tipoCuentaCodigo);
        sb.append("], bancoCodigo[").append(bancoCodigo);
        sb.append("], email[").append(email);
        sb.append("], estado[").append(estado);
        sb.append("], idValidacion[").append(idValidacion);
        sb.append("], fechaIngreso[").append(fechaIngreso);
        sb.append("], fechaAutorizacion[").append(fechaAutorizacion);
        sb.append("], favorito[").append(favorito);
        sb.append("], restriccionRegla24Horas[").append(restriccionRegla24Horas);
        sb.append("]");
        return sb.toString();
	}
	
	/**
	 * Setter para favorito.
	 * @param favorito Indica si el alias es favorito (true) o normal (false).
	 * @since 1.7
	 */
	public void setFavorito(boolean favorito){
		this.favorito = favorito;
	}
	
	/**
	 * Getter para favorito.
	 * @return True para alias favorito y false para alias normal
	 * @since 1.7
	 */
	public boolean isFavorito(){
		return this.favorito;
	}
	
	/**
	 * Setter para nombreBanco.
	 * @param banco El nombre del banco para la cuenta del alias
	 * @since 1.7
	 */
	public void setNombreBanco(String banco){
		this.nombreBanco = banco;
	}
	
	/**
	 * Getter para nombreBanco.
	 * @return El nombre del banco para la cuenta del alias
	 * @since 1.7
	 */
	public String getNombreBanco(){
		return this.nombreBanco;
	}
	
	/**
	 * Indica si el alias est� o no autorizado.
	 * 
	 * <p>Actualmente se emplea el atributo {@link AliasTTFFDVO#estado} para determinar si est� autorizado.
	 * Este valor debe ser 'A' o 'N' para ser considerado autorizado.</p>
	 * @return True si el alias est� autorizado y false en caso contrario.
	 * @since 1.7
	 */
	public boolean isAutorizado(){
		return (this.estado == 'A' || this.estado == 'N');
	}
	
	/**
	 * Indica si un banco se encuentra Bloqueado.
	 * @param bancoBloqueado indicador de si un banco se encuentra bloqueado
	 * @since 1.8
	 */
	public void setBancoBloqueado(boolean bancoBloqueado) {
			this.bancoBloqueado = bancoBloqueado;
		}
	
	/**
	 * Indica si un banco se encuentra Bloqueado.
	 * @return True si el alias est� autorizado y false en caso contrario.
	 * @since 1.8
	 */
	public boolean isBancoBloqueado(){
	    return this.bancoBloqueado; 
	}
	
	public boolean isRestriccionRegla24Horas() {
		return restriccionRegla24Horas;
	}

	public void setRestriccionRegla24Horas(boolean restriccionRegla24Horas) {
		this.restriccionRegla24Horas = restriccionRegla24Horas;
	}
	
	
}
