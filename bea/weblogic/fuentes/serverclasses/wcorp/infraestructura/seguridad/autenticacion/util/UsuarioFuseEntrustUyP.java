package wcorp.infraestructura.seguridad.autenticacion.util;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import wcorp.model.seguridad.UyPCache;
import wcorp.model.seguridad.UyPDatos;
import wcorp.util.TablaValores;


/**
 * Clase de acceso al usuario aplicativo UyP que permite acceso a los bundles de FUSE - Entrust.
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 07/19/2017 Jaime Suazo D. (Sentra) - Ricardo Carrasco (Ing. Soft. BCI.): versi�n inicial.</li>
 * </ul>
 * <p>
 * 
 *
 * <P>
 * <B>Todos los derechos reservados por Banco de Cr�dito e Inversiones.</B>
 */
public class UsuarioFuseEntrustUyP {
	
	/**
	 * tabla de par�metros de entrust.
	 */
	private static final String TABLA_PARAMETROS = "entrust.parametros";
	
    /**
     * Instancia �nica.
     */
    private static final UsuarioFuseEntrustUyP INSTANCIA = new UsuarioFuseEntrustUyP();
			
    /**
     * Logger.
     */
    private transient Logger logger = Logger.getLogger( UsuarioFuseEntrustUyP.class );
        
    /**
     * Usuario aplicativo de fuse UyP.
     */
    private String usuarioAplicativoId;

    /**
     * Contrase�a del usuario de fuse Uyp.
     */
    private String claveUsuarioAplicativo;    
        
    /**
     * 
     * Constructror de la clase, inicializa el usuario y contrase�a obtenido desde UyP.
     *
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 07/19/2017 Jaime Suazo D. (Sentra) - Ricardo Carrasco (Ing. Soft. BCI.): versi�n inicial.</li>
     * </ul>
     * <p>
     *
     * @since 1.0 
     *
     */
    private UsuarioFuseEntrustUyP() {
    	obtenerSistemaFuseUyP();
    }

    /**
     * Inicializa usuario aplicativo de la instancia UyP para fuse - entrust.
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 07/19/2017 Jaime Suazo D. (Sentra) - Ricardo Carrasco (Ing. Soft. BCI.): versi�n inicial.</li>
     * </ul>
     * <p>
     *
     * @since 1.0
     *
     */
    public void obtenerSistemaFuseUyP() {
        try {
        	if (getLogger().isEnabledFor(Level.INFO)) {
        		getLogger().debug( "[obtenerSistemaFuseUyP][BCI_INI]" );	
        	}
            
            String sistemaUyP = TablaValores.getValor( TABLA_PARAMETROS, "WSAdminParametros", "sistema" );
            if( getLogger().isEnabledFor(Level.DEBUG) ) {
                getLogger().debug( "[obtenerSistemaFuseUyP][sistemaUyP][" + sistemaUyP + "]" );
            }

            UyPDatos uypDatos = UyPCache.getInstance().getUyPSybUser(sistemaUyP);
            
            usuarioAplicativoId    = uypDatos.clave;
            claveUsuarioAplicativo = uypDatos.usuario;
            
            if (getLogger().isEnabledFor(Level.INFO)) {
            	getLogger().debug( "[obtenerSistemaFuseUyP][BCI_FIN][usuario y clave obtenidos OK]" );	
            }            
        }
        catch ( Throwable e ) {
        	if (getLogger().isEnabledFor(Level.WARN)) {
        		getLogger().warn( "[obtenerSistemaFuseUyP][Throwable][" + e.getMessage() + "]", e );	
        	}
        	
        	try {
            	if (usuarioAplicativoId == null) {
            		usuarioAplicativoId = TablaValores.getValor( TABLA_PARAMETROS, "WSAdminParametros", "usuario" );
            	}
			} 
        	catch (Exception e2) {
	        	if (getLogger().isEnabledFor(Level.WARN)) {
	        		getLogger().warn( "[obtenerSistemaFuseUyP][Exception][usuarioAplicativoId][" + e2.getMessage() + "]", e2 );	
	        	}
			}
        	
        	try {
            	if (claveUsuarioAplicativo == null) {
            		claveUsuarioAplicativo = TablaValores.getValor( TABLA_PARAMETROS, "WSAdminParametros", "password" );
            	}
			} catch (Exception e3) {
	        	if (getLogger().isEnabledFor(Level.WARN)) {
	        		getLogger().warn( "[obtenerSistemaFuseUyP][Exception][claveUsuarioAplicativo][" + e3.getMessage() + "]", e3 );	
	        	}
			}
        }    	
    }
        
    /**
     * 
     * M�todo que obtiene el usuario UyP de fuse - entrust.
     *
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 07/19/2017 Jaime Suazo D. (Sentra) - Ricardo Carrasco (Ing. Soft. BCI.): versi�n inicial.</li>
     * </ul>
     * <p>
     *
     * @return Usuario aplicativo de fuse - entrust.
     * @since 1.0
     *
     */    
    public String getUsuarioAplicativoId() {
        if(  usuarioAplicativoId == null ) {
        	obtenerSistemaFuseUyP();
        }
        return usuarioAplicativoId;
    }
        
    /**
     * 
     * M�todo que obtiene la contrase�a UyP de fuse - entrust.
     *
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 07/19/2017 Jaime Suazo D. (Sentra) - Ricardo Carrasco (Ing. Soft. BCI.): versi�n inicial.</li>
     * </ul>
     * <p>
     *
     * @return Contrase�a de usuario aplicativo fuse - entrust.
     * @since 1.0
     *
     */
    public String getClaveUsuarioAplicativo() {
        if ( claveUsuarioAplicativo == null ) {
        	obtenerSistemaFuseUyP();
        }
        return claveUsuarioAplicativo;
    }
        
    /**
     * 
     * M�todo de acceso a la instancia del componente.
     *
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 07/19/2017 Jaime Suazo D. (Sentra) - Ricardo Carrasco (Ing. Soft. BCI.): versi�n inicial.</li>
     * </ul>
     * <p>
     *
     * @return Instancia �nica.
     * @since 1.0
     *
     */
    public static UsuarioFuseEntrustUyP getInstancia() {
        return INSTANCIA;
    }    
    
	/**
	 * Metodo para obtener el log de la clase.
	 * 
	 * Registro de versiones:
	 * <ul>
     * <li>1.0 07/19/2017 Jaime Suazo D. (Sentra) - Ricardo Carrasco (Ing. Soft. BCI.): versi�n inicial.</li>
	 * </ul>
	 * 
	 * @return Logger log de la clase.
	 * @since 1.0
	 */
	private Logger getLogger() {
		if (logger == null) {
			logger = Logger.getLogger(UsuarioFuseEntrustUyP.class);
		}
		return logger;
	}    
}
