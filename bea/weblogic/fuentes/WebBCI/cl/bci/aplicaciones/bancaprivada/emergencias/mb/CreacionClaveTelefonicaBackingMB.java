package cl.bci.aplicaciones.bancaprivada.emergencias.mb;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.component.html.HtmlInputSecret;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import cl.bci.aplicaciones.cliente.mb.ClienteMB;
import cl.bci.infraestructura.web.seguridad.mb.SesionMB;
import cl.bci.infraestructura.web.seguridad.segundaclave.SegundaClaveUIInput;
import cl.bci.infraestructura.web.seguridad.util.ConectorStruts;
import wcorp.bprocess.tcreditoonline.TCreditoOnline;
import wcorp.bprocess.tcreditoonline.TCreditoOnlineHome;
import wcorp.bprocess.tcreditoonline.vo.ResultadoServicioCuentasVO;
import wcorp.model.seguridad.SessionBCI;
import wcorp.serv.clientes.Operacion;
import wcorp.serv.clientes.ServiciosCliente;
import wcorp.serv.clientes.ServiciosClienteHome;
import wcorp.serv.seguridad.SeguridadException;
import wcorp.serv.seguridad.ServiciosSeguridad;
import wcorp.serv.seguridad.ServiciosSeguridadHome;
import wcorp.serv.tarjetas.ServiciosTarjetas;
import wcorp.serv.tarjetas.ServiciosTarjetasHome;
import wcorp.serv.tarjetas.TarjetaCliente;
import wcorp.util.EnhancedServiceLocator;
import wcorp.util.FechasUtil;
import wcorp.util.GeneralException;
import wcorp.util.StringUtil;
import wcorp.util.TablaValores;
import wcorp.util.journal.Eventos;
import wcorp.util.journal.Journal;


/**
 * Support ManagedBean
 * <br>
 * <b>CreacionClaveTelefonicaBackingMB</b>
 * <br>
 * Managed bean que participa en las vistas para las opciones de clave telefonica.
 * <br>
 * Registro de versiones:<ul>
 * <li>1.0 (16/11/2011 Javier Aguirre A. (Imagemaker IT)): versi�n inicial.</li>
 * <li>1.3 (22/05/2012 Victor Agurto. (Imagemaker IT)): Se corrige obtenci�n de clave multipass 
 * y nueva clave en crearClaveTelefonica. Corrigen niveles de logs en try-catch</li>
 * <li>1.4 (11/07/2012 Gonzalo Bustamante V. (Sermaluc)): Se modifica mensaje de error enviado al validar
 *  campos de claves. Se incluye un indicador para saber si la excepcion es 
 * causada por validacion de multipass. Se modifica el m�todo validaTokenCliente()</li>
 * <li>1.5 (10/09/2013 Yon Sing Sius. (ExperimentoSocial)): Se quita valicaci�n con multipass y se deja validaci�n
 * de segunda clave gen�rica</li>
 * <li>1.6 (16/05/2014 Luis Lopez Alamos (SEnTRA)): Se modifica el atributo LARGO_MINIMO por 4 y se reemplaza
 * la parametrizaci�n de la nueva clave por una clave editada de 4 d�gitos y concatenada con 2 espacios.</li>
 * <li>1.7 19/12/2014 Eduardo Villagr�n Morales (Imagemaker): Se agrega constante {@link #TIPO_USUARIO} y se
 *      modifican llamadas al m�todo {@link #validarAcceso(long, char, String, char)}. Se normalizan logs.
 *      Se crea el m�todo {@link #getLogger()}.</li>
 * <li>2.0 05/09/2017, Marcelo Fuentes (SEnTRA) - Ricardo Carrasco (Ing. Soft. BCI): Se cambia alcance del ManagedBean
 * de RequestScoped a ViewScoped debido a que se agrego un nuevo paso en la vista de ingreso de clave para 
 * separar el ingreso de la validacion de la segunda clave. Se modifica m�todo
 * {@link #validate(FacesContext, UIComponent, Object)}. Se agrega atributo {@link #claveTelValidada}.</li>
 * </ul>
 *
 * <p><b>Todos los derechos reservados por Banco de Cr�dito e Inversiones.</b></p>
 */

@ManagedBean
@ViewScoped
public class CreacionClaveTelefonicaBackingMB implements Serializable{

	/**
	 * Version serial.
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Ruta al comprobante.
	 */
	private static final String COMPROBANTE = "comprobanteCreacionTelefonica";

	/**
     * C�digo del estado del evento utilizado para journalizar.
     */
    private static final String ESTADOEVENTOJOURNAL = "NOK";

	/**
     * Cantidad de meses que la clave estar� activa.
     */
    private static final int MESESVENCIMIENTO = 12;
    
    /**
     * C�digo de error que indica que ya existe la clave.
     */
    private static final String ERROR_CLAVE_YA_EXISTE = "0437";
    
    /**
     * Estado de una tarjeta de d�bito activa.
     */
    private static final String ESTADO_TARJERA_DEBITO_ACTIVADA = "003";
    
    /**
     * Archivo de Par�metros de Errores.
     */
    private static final String ARCH_PARAM_ERROR = "errores.codigos";
    
    /**
     * C�digo de error cliente sin tarjetas de d�bito.
     */
    private static final String ERROR_SIN_TARJETAS_ACTIVAS = "CT_0002";
    
    /**
     * C�digo de error al verificar las tarjetas de d�bito.
     */
    private static final String ERROR_VERIFICANDO_TARJETAS = "CT_0001";
    
    /**
     * C�digo de error general.
     */
    private static final String ERROR_GENERAL = "8364";

	 /**
     * Clave grupo canal para registrar una clave telef�nica en el banco.
     */
    private static final String CLAVE_GRUPO_CANAL = "14";

	 /**
     * Clave que corresponde al canal de una clave telef�nica en el banco.
     */
    private static final String CANAL_CLAVE_TELEFONICA = "220";
    
    /**
     * C�digo del estado del evento OK utilizado para journalizar.
     */
    private static final String JOURNAL_ESTADOEVENTO = " ";

	    /**
     * Identificador de producto.
     */
    private static final String JOURNAL_PRODUCTO = "SEGURI";
    
    /**
     * Nombre del evento para journalizaci�n de solicitud de clave telef�nica.
     */
    private static final String JOURNAL_EVENTO_SOLICITA_CLAVE = "SOLICITA";
    
    /**
     * C�digo de subevento.
     */
    private static final String JOURNAL_SUBEVENTO = "CLVTEL";

    /**
     * C�digo de error al generar la clave telef�nica.
     */
    private static final String ERROR_GENERAR_CLAVE = "CT_0004";

    /**
     * C�digo indicador de operacion vigente.
     */
    private static final String OPERACION_VIGENTE = "VIG";

    /**
     * C�digo indicador de operacion del tipo cuenta corriente.
     */
    private static final String OPERACION_CUENTA_CORRIENTE = "CCT";

    /**
     * C�digo indicador de operacion del tipo cuenta prima.
     */
    private static final String OPERACION_CUENTA_PRIMA = "CPR";
    
    /**
     * Mensaje error de validaci�n 1.
     */
	private static final String MSJE_ERROR_VALIDACION1 = "Claves no coinciden";
	
	/**
     * Mensaje error de validaci�n 2.
     */
	private static final String MSJE_ERROR_VALIDACION2 ="Debe ser de 4 d�gitos";
	
	/**
	 * Largo m�nimo.
	 */
	private static final int LARGO_MINIMO = 4;
	
	/**
     * Atributo que declara valor maximo de caracteres que se envian.
     */
    private static final int LARGO_CLAVE = 6;
    /**
     * Constante para tipo usuario.
     * @since 1.7
     */
    private static final char TIPO_USUARIO = 'T';
	
	/**
	 * Variable para asignar log de ejecuci�n.
	 */
	private static transient Logger logger = Logger.getLogger(CreacionClaveTelefonicaBackingMB.class);

	/**
	 * Nueva clave Telefonica.
	 */
	private String nuevaClaveTelefonica;
	
	/**
	 * Clave Repetida.
	 */
	private String repetidaClaveTelefonica;
	
	/**
	 * Para validar la autenticaci�n con Segunda Clave.
	 */
	private SegundaClaveUIInput segundaClave;
	
	/**
	 * Error general.
	 */
	private boolean errorGeneral;
	
	/**
	 * Mensaje de Error.
	 */
	private String mensajeError;
	
	/**
	 * Servicios de Seguridad.
	 */
    private ServiciosSeguridad serviciosSeguridad;
    
    /**
     * Servico de tarjetas.
     */
    private ServiciosTarjetas serviciosTarjetas;
    
    /**
     * Servicio de clientes.
     */
    private ServiciosCliente serviciosCliente;
    
    /**
     * Servicio de TCredito.
     */
    private TCreditoOnline tCreditoOnline;
	
     /**
     * Inyeccion dependencia Managed bean sesionMB.
     */
    @ManagedProperty(value = "#{sesionMB}")
    private SesionMB sesionMB;
    
     /**
     * Inyeccion dependencia Managed bean clienteMB.
     */
    @ManagedProperty(value = "#{clienteMB}")
    private ClienteMB clienteMB;
    
    /**
     * Confirmar.
     */
    private HtmlInputSecret confirmarUIInput;
    
    // Variables locales
    /**
     * Objeto utilizado para journalizar.
     */
    private Eventos eventos;
    
    /**
     * Indicador de error multipass.
     */
    private boolean multipassError;
    
    /**
     * Indica si la clave telefonica esta validada.
     */
    private boolean claveTelValidada;
    
	/**
	 * Construye un nuevo objeto request CreacionClaveTelefonicaBackingMB.
	 */
	public CreacionClaveTelefonicaBackingMB() 	{
		logger.debug("[Constructor]");
		
	}
	
	public String getNuevaClaveTelefonica() {
		return "";
	}

	public void setNuevaClaveTelefonica(String nuevaClaveTelefonica) {
		this.nuevaClaveTelefonica = nuevaClaveTelefonica;
	}

	public String getRepetidaClaveTelefonica() {
		return repetidaClaveTelefonica;
	}

	public void setRepetidaClaveTelefonica(String repetidaClaveTelefonica) {
		this.repetidaClaveTelefonica = repetidaClaveTelefonica;
	}

	public SegundaClaveUIInput getSegundaClave() {
		return segundaClave;
	}

	public void setSegundaClave(SegundaClaveUIInput segundaClave) {
		this.segundaClave = segundaClave;
	}

	public ClienteMB getClienteMB() {
		return clienteMB;
	}

	public void setClienteMB(ClienteMB clienteMB) {
		this.clienteMB = clienteMB;
	}

	public SesionMB getSesionMB() {
		return sesionMB;
	}

	public void setSesionMB(SesionMB sesionMB) {
		this.sesionMB = sesionMB;
	}
	
    /**
     * M�todo que obtiene indicador que informa de algun error al crear la clave telefonica.
     *<p>
     * Registro de versiones:
     * <UL>
     * <li>1.0 (16/11/2011 Javier Aguirre A. (Imagemaker IT)): versi�n inicial.</li>
     * <li>1.1 05/09/2017, Marcelo Fuentes (SEnTRA) - Ricardo Carrasco (Ing. Soft. BCI): Se obtiene indicador de error 
     * desde atributo en el request.</li>
     * </UL></p>
     * @return boolean indicando si hubo algun error.
     * @since 1.0
     */
    public boolean isErrorGeneral() {
        HttpServletRequest req = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        String error = (String)req.getAttribute("errorGeneral");
        return (error != null && error.equals("true") ? true : false);
    }

    /**
     * M�todo que setea indicador que informa de algun error al crear la clave telefonica.
     *<p>
     * Registro de versiones:
     * <UL>
     * <li>1.0 (16/11/2011 Javier Aguirre A. (Imagemaker IT)): versi�n inicial.</li>
     * <li>1.1 05/09/2017, Marcelo Fuentes (SEnTRA) - Ricardo Carrasco (Ing. Soft. BCI): Se setea indicador de error 
     * en atributo en el request.</li>
     * </UL></p>
     * @param errorGeneral Indica si hubo algun error.
     * @since 1.0
     */
    public void setErrorGeneral(boolean errorGeneral) {
        HttpServletRequest req = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        req.setAttribute("errorGeneral", String.valueOf(errorGeneral));
        this.errorGeneral = errorGeneral;
    }

	public String getMensajeError() {
		return mensajeError;
	}

	public void setMensajeError(String mensajeError) {
		this.mensajeError = mensajeError;
	}
	
	public HtmlInputSecret getConfirmarUIInput() {
		return confirmarUIInput;
	}

	public void setConfirmarUIInput(HtmlInputSecret confirmarUIInput) {
		this.confirmarUIInput = confirmarUIInput;
	}
	
	public boolean isMultipassError() {
        return multipassError;
    }

    public void setMultipassError(boolean multipassError) {
        this.multipassError = multipassError;
    }

	/**
	 * M�todo que entrega el estado del token.
	 *<p>
	 * Registro de versiones:
	 * <UL>
	 * <LI>1.0 (07/01/2012, Javier Aguirre (Imagemaker)): version inicial</LI>
	 * <li>1.1 (10/09/2013, Yon Sing Sius (ExperimentoSocial)): m�todo se deja en desuso por cambio
	 * a Segunda Clave</li>
	 * </UL></p>
	 * @param rut del cliente
	 * @param dv verificador del cliente
	 * @return codigo de estado
	 * @since 1.3
	 * @deprecated
	 */
	private String dameEstadoToken(long rut, char dv){
		String estado = null;
		try{
			estado = getServiciosSeguridad().determinaEstadoToken(rut, dv);
		}
		catch(Exception e){
			estado="SIN_INFO";
		}
		return estado;
	}
	
	/**
	 *  Valida si cliente posee tarjetas activas y/o si posee token habilitado.
	 *<p>
	 * Registro de versiones:
	 * <UL>
	 * <LI>1.0 (10/01/2012, Javier Aguirre (ImageMaker)): version inicial</LI>
	 * <li>1.3  22/05/2012, Victor Agurto. (ImageMaker): corrige nivel de log-error en try-catch</li>
     * <li>1.4  10/09/2013, Yon Sing Sius. (ExperimentoSocial): Se quita validaci�n de si tiene token,
     *  de est� funcionalidad se encargar� el componente de Segunda Clave</li>
     * <li>1.5 22/12/2014 Eduardo Villagr�n Morales (Imagemaker): se normaliza log.</li> 
	 * </UL></p>
	 * @param rut rut del cliente
	 * @param dv digito verificador cliente
	 * @param canal canal asociado
	 * @param tipoUsuario el tipo de usuario
	 * @throws Exception En caso de un error en la validaci�n
	 * @since 1.0
	 */
	private void validarAcceso(long rut, char dv, String canal, char tipoUsuario)
			throws Exception {
	    if (getLogger().isInfoEnabled()) {
            getLogger().info("[validarAcceso] [" + rut + "] [BCI_INI] rut=<" + rut + ">, dv=<" + dv 
                    + ">, canal=<" + canal + ">, tipoUsuario=<" + tipoUsuario + ">");
        }
		boolean hayTarjetasActivas = false;
		final int caracteresLibres = 4;
		try {
			TarjetaCliente tarjetasATM = null;

			tarjetasATM = this.getServiciosTarjeta().getTarjetasATMXInd(rut,
					tipoUsuario);
			if (getLogger().isDebugEnabled()) {
			    if (tarjetasATM==null){
			        getLogger().debug("[validarAcceso] [" + rut + "] tajetasATM=<null>");
			    }
			    else{
			        getLogger().debug("[validarAcceso] [" + rut + "] tajetasATM=<" + tarjetasATM 
			                + ">, tarjetaATM.getNumero()=<" + tarjetasATM.getNumero() 
			                + ">, tarjetaATM.getTarjeta()=<" + tarjetasATM.getTarjeta() + ">");
			    }
            }

			if (tarjetasATM != null && tarjetasATM.tarjeta != null) {
				for (int i = 0; i < tarjetasATM.tarjeta.length; i++) {
				    if (getLogger().isDebugEnabled()) {
                        getLogger().debug("[validarAcceso] [" + rut + "] tarjetasATM.tarjeta[" + i 
                                + "].numeroTarjeta=<" + StringUtil.censura(tarjetasATM.tarjeta[i].numeroTarjeta,
                                ' ', caracteresLibres, false, false) + ">, tarjetasATM.tarjeta[" + i 
                                + "].estado=<" + tarjetasATM.tarjeta[i].estado + ">");
					}
					if (tarjetasATM.tarjeta[i].estado.equals(ESTADO_TARJERA_DEBITO_ACTIVADA)) {
						hayTarjetasActivas = true;
						break;
					}
				}
			} 
			else {
			    if (getLogger().isEnabledFor(Level.WARN)) {
                    getLogger().warn("[validarAcceso] [" + rut + "] No se obtuvieron tarjetas del cliente.");
                }
			}

		} 
		catch (Exception e) {
		    if (getLogger().isEnabledFor(Level.ERROR)) {
                getLogger().error("[validarAcceso] [" + rut 
                        + "] [Exception] [BCI_FINEX[ Lanzando nueva Exception. " + e.getMessage(), e);
            }
			throw new Exception(TablaValores.getValor(ARCH_PARAM_ERROR, ERROR_VERIFICANDO_TARJETAS, "Desc"), e);
		}

		if (!hayTarjetasActivas) {
		    if (getLogger().isEnabledFor(Level.ERROR)) {
                getLogger().error("[validarAcceso] [" + rut 
                        + "] [BCI_FINEX] El cliente no tiene tarjetas activas. Lanzo nueva Exception.");
		}
			throw new Exception(TablaValores.getValor(ARCH_PARAM_ERROR, ERROR_SIN_TARJETAS_ACTIVAS, "Desc"));
		}
		if (getLogger().isInfoEnabled()) {
            getLogger().info("[validarAcceso] [" + rut + "] [BCI_FINOK]");
        }
		return;

	}
	

    /**
     * <p>M�todo para validar si cliente corresponde a MonoProducto (Tarjeta de Cr�dito).</p>
     *
     * <p>Registro de versiones:</p><ul>
     * <li>1.0 10/01/2012, Javier Aguirre (Imagemaker)): versi�n inicial.</li>
     * <li>1.3 22/05/2012, Victor Agurto. (ImageMaker): corrige nivel de log-error en try-catch</li>
     * <li>1.4 22/12/2014 Eduardo Villagr�n Morales (Imagemaker): se normaliza log.</li> 
     * </ul>
     *
     * <B>Todos los derechos reservados por Banco de Cr�dito e Inversiones.</B>
     *
     * @param rut Rut del Cliente MonoProducto.
     * @param dv Digito Verificador del Cliente MonoProducto.
     * @throws Exception si ocurre un error en la validaci�n
     * @return boolean verdadero si es cliente monoProducto
     */
	private boolean validarClienteMonoproducto(long rut, char dv)
			throws Exception {
	    if (getLogger().isInfoEnabled()) {
            getLogger().info("[validarClienteMonoproducto] [BCI_INI] [" + rut + "] rut=<" + rut 
                    + ">, dv=<" + dv + ">");
		}
		char tipoCliente = clienteMB.getTipoUsuario().charAt(0);
		if (getLogger().isDebugEnabled()) {
            getLogger().debug("[validarClienteMonoproducto] [" + rut + "] tipoCliente=<" + tipoCliente + ">");
        }
		boolean esTDC = true;
		boolean esCCT = false;
		boolean esCPR = false;

		try {
			Operacion[] operacionesCliente = this.getServiciosCliente().consultaOperaciones(rut, dv, tipoCliente);
			if (getLogger().isDebugEnabled()) {
			    int largo = operacionesCliente!=null?operacionesCliente.length:0;
                getLogger().debug("[validarClienteMonoproducto] [" + rut + "] operacionesCliente=<"
                        + operacionesCliente + ">, largo=<" + largo+ ">");
			}
			for (int i = 0; i < operacionesCliente.length; i++) {
				if (OPERACION_VIGENTE.equals(operacionesCliente[i].codEstado)
						&& OPERACION_CUENTA_CORRIENTE.equals(operacionesCliente[i].tipoOperacion)) {
					esCCT = true;
					esTDC = false;
					break;
				} 
				else if (OPERACION_VIGENTE.equals(operacionesCliente[i].codEstado)
						&& OPERACION_CUENTA_PRIMA.equals(operacionesCliente[i].tipoOperacion)) {
					esCPR = true;
					esTDC = false;
					break;
				}
			}
			if (getLogger().isDebugEnabled()) {
                getLogger().debug("[validarClienteMonoproducto] [" + rut + "] esCPR=<" + esCPR + ">, esCCT=<"
                        + esCCT + ">");
			}

			if (!esCCT && !esCPR) {
				ResultadoServicioCuentasVO cuentasCliente = this.getTCreditoOnline().getCuentasPorRut(rut, dv);
				if (cuentasCliente != null && cuentasCliente.getCuentas() != null 
						&& cuentasCliente.getCuentas().length > 0) {
					if (getLogger().isDebugEnabled()) {
                        getLogger().debug("[validarClienteMonoproducto] [" + rut + "] obtiendo cuenta TDC.");
					}
					esTDC = true;
				} 
				else {
					if (getLogger().isEnabledFor(Level.WARN)) {
                        getLogger().warn("[validarClienteMonoproducto] [" + rut + "] cliente sin TDC.");
					}
					esTDC = false;
				}
			} 
			else {
				if (getLogger().isEnabledFor(Level.WARN)) {
                    getLogger().warn("[validarClienteMonoproducto] [" + rut + "] cliente no monoproducto tdc");
				}
				esTDC = false;
			}
		} 
		catch (GeneralException e) {
		    if (getLogger().isEnabledFor(Level.ERROR)) {
                getLogger().error("[validarClienteMonoproducto] [" + rut + "] [GeneralException] [BCI_FINEX] "
                        + e.getMessage(), e);
            }
			throw e;
		} 
		catch (Exception ex) {
		    if (getLogger().isEnabledFor(Level.ERROR)) {
                getLogger().error("[validarClienteMonoproducto] [" + rut + "] [Exception] [BCI_FINEX] Lanzando nueva Exception. "
                        + ex.getMessage(), ex);
            }
			throw new Exception(TablaValores.getValor("errores.codigos", "EDDA-015", "Desc"), ex);
		}
		if (getLogger().isInfoEnabled()) {
            getLogger().info("[validarClienteMonoproducto] [" + rut + "] [BCI_FINOK] esTDC=<" + esTDC + ">");
		}
		return esTDC;
    }

	
	/**
	 * <p>M�todo  para verificar los tipos de accesos del usuario.</p>
	 * 
     * <p>Registro de versiones:</p><ul>
     * <li>1.0 10/01/2012, Javier Aguirre (Imagemaker)): versi�n inicial.</li>
     * <li>1.2  10/04/2012, Javier Aguirre. (ImageMaker): corrije la obtenci�n del canal a PinID</li>
     * <li>1.3  22/05/2012, Victor Agurto. (ImageMaker): corrige nivel de log-error en try-catch</li>
     * <li>1.4 19/12/2014 Eduardo Villagr�n Morales (Imagemaker): Se modifica llamada al m�todo
     *      {@link #validarAcceso(long, char, String, char)} para hacer uso de constante {@link #TIPO_USUARIO}
     *      como cuarto par�metro. Se normaliza log.</li>
     * </ul>
     *
     * <B>Todos los derechos reservados por Banco de Cr�dito e Inversiones.</B>
	 * @throws Exception 
	 */
	private void validacionInicial() throws Exception {
		long rut = clienteMB.getRut();
		char dv = clienteMB.getDigitoVerif();
		String canal = sesionMB.getCanalTO().getPinID();
		if (getLogger().isInfoEnabled()) {
            getLogger().info("[validacionInicial] [" + rut + "] [BCI_INI] canal=<" + canal + ">");
        }
		FacesContext fc = FacesContext.getCurrentInstance();
		SessionBCI sessionBci = ConectorStruts.getSessionBCI();
	    if(sessionBci == null) {
	            if (getLogger().isEnabledFor(Level.ERROR)) {
                    getLogger().error("[validacionInicial] [" + rut + "] [BCI_FINEX] sin sesi�n BCI. Lanzando GeneralException");
                }
	            throw new GeneralException("No hay Sesion BCI");
	    }
		try {
			if (!sesionMB.isActiva()) {
			    if (getLogger().isEnabledFor(Level.ERROR)) {
                    getLogger().error("[validacionInicial] [" + rut + "] [BCI_FINEX] No hay sesi�n activa. Lanzo GeneralException");
                }
				throw new GeneralException(TablaValores.getValor(ARCH_PARAM_ERROR, "8366", "Desc"));
			}
			eventos = new Eventos();
			if (this.validarClienteMonoproducto(rut, dv)) {
			    if (getLogger().isDebugEnabled()) {
                    getLogger().debug("[validacionInicial] [" + rut + "] validaci�n monoproducto exitosa.");
				}
				fc.getExternalContext().redirect("solicitudClaveMonoproducto.do");
			} 
			else {
			    if (getLogger().isDebugEnabled()) {
                    getLogger().debug("[validacionInicial] [" + rut + "] validaci�n monoproducto no exitosa.");
				}
				this.validarAcceso(rut, dv, canal, TIPO_USUARIO);
				if (getLogger().isDebugEnabled()) {
                    getLogger().debug("[validacionInicial] [" + rut + "] validaci�n acceso exitosa.");
				}
			}
		}
		catch (Exception e) {
		    if (getLogger().isEnabledFor(Level.ERROR)) {
                getLogger().error("[validacionInicial] [" + rut + "] [Exception] [BCI_FINEX] Lanza exception. "
                        + e.getMessage(), e);
			}
			throw e;
		}
		if (getLogger().isInfoEnabled()) {
            getLogger().info("[validacionInicial] [" + rut + "] [BCI_FINOK]");
        }
	}
	
	/**
	 * <p>M�todo para verificar si cliente posee token.</p>
	 * 
     * <p>Registro de versiones:</p><ul>
     * <li>1.0 10/01/2012, Javier Aguirre (Imagemaker)): versi�n inicial.</li>
     * <li>1.3 22/05/2012, Victor Agurto (Imagemaker)): cambian nivel de log en try-catch.</li>
     * <li>1.4 17/07/2012 Gonzalo Bustamante(Sermaluc): Se incluye un indicador para saber si la excepcion es 
     * causada por validacion de multipass. </li>
     * <li>1.5 06/09/2013 Yon Sing Sius(ExperimentoSocial): Se deja obsoleto por cambio de validaci�n a 
     * Segunda Clave. </li>
     * </ul>
     *
     * <B>Todos los derechos reservados por Banco de Cr�dito e Inversiones.</B>
	 * @param rut rut de cliente
	 * @param dv digito verificador
	 * @param canal canal suado
	 * @param codigoMultipass n�mero de su dispositivo token
	 * @throws Exception error al validar token
	 * @deprecated Se cambia por validacion de SegundaClave
	 */
	private void validaTokenCliente(long rut, char dv, String canal, String codigoMultipass) throws Exception {
		String pin = "";

		pin = TablaValores.getValor("Seguridad.parametros", "pinToken", canal);
		if (pin == null) {
			logger.debug("[validaTokenCliente]:: no se ha podido rescatar el pin de la tabla");
			this.setMultipassError(true);
			throw new GeneralException("ESPECIAL"
					, TablaValores.getValor( ARCH_PARAM_ERROR, ERROR_GENERAL, "Desc"));
		}

		if (logger.isDebugEnabled()) {
			logger.debug("[validaTokenCliente] Largo Multipass:" + codigoMultipass.length());
		}

		try {
			if (logger.isDebugEnabled()) {
				logger.debug("[validaTokenCliente]: rut:[" + rut + "] dv:[" + dv + "]");
			}
			this.getServiciosSeguridad().autenticaToken(rut, dv, pin + codigoMultipass);
		}
		catch (SeguridadException e) {
			if (logger.isEnabledFor(Level.ERROR)) {
				logger.error("[validaTokenCliente] Buscando error: " + canal + "-"
						+ e.getCodigo(), e);
			}
			this.setMultipassError(true);
			throw new Exception(TablaValores.getValor("errores.codigos", canal
					+ "-" + e.getCodigo(), "desc"), e);
		}
		catch (Exception e) {
			if (logger.isEnabledFor(Level.ERROR)) {
				logger.error("[validaTokenCliente]:Error al validar token", e);
			}
			logger.error("[validaTokenCliente]:: Clave multipass incorrecta");

			if (logger.isEnabledFor(Level.ERROR)) {
				logger.error("[validaTokenCliente] Sistema no disponible", e);
			}
			this.setMultipassError(true);
			throw new Exception("Sistema no disponible", e);
		}
		logger.debug("[validaTokenCliente] fin...");
	}
	
	/**
	 * <p>Valida segunda clave. Cliente puede utilizar cualquier m�todo de autenticaci�n (Multipass, SafeSigner) 
	 * </p>
	 * 
     * <p>Registro de versiones:</p><ul>
     * <li>1.0 06/09/2013, Yon Sing Sius (ExperimentoSocial): versi�n inicial.</li>
     * <li>1.1 22/12/2014 Eduardo Villagr�n Morales (Imagemaker): se normaliza log.</li> 
     * </ul>
     *
     * <B>Todos los derechos reservados por Banco de Cr�dito e Inversiones.</B>
	 * @throws Exception error al validar token
	 * @return true si la validaci�n es correcta, false si no.
	 * @since 1.5
	 */
	private boolean validaSegundaClave() throws Exception {
	    if (getLogger().isInfoEnabled()) {
            getLogger().info("[validaSegundaClave] [" + getClienteMB().getRut() + "] [BCI_INI]");
        }
		boolean validacion = false;
        try{
            segundaClave.verificarAutenticacion();
            if (getLogger().isDebugEnabled()) {
                getLogger().debug("[validaSegundaClave] [" + getClienteMB().getRut() + "] validaci�n OK");
            }
            validacion = true;
        }
        catch(SeguridadException ex){
            if (getLogger().isEnabledFor(Level.WARN)) {
                getLogger().warn("[validaSegundaClave] [" + getClienteMB().getRut() + "] [SeguridadException] "
                        + ex.getMessage(), ex);
            }
            FacesMessage mensaje = new FacesMessage(ex.getInfoAdic());
            mensaje.setSeverity(FacesMessage.SEVERITY_ERROR);
            FacesContext.getCurrentInstance().addMessage("segundaClave", mensaje);
            if (getLogger().isEnabledFor(Level.ERROR)) {
                getLogger().error("[validaSegundaClave] [" + getClienteMB().getRut() + "] [BCI_FINEX] validacion=<"
                        + validacion + ">");
            }
            return validacion;
        }
        if (getLogger().isInfoEnabled()) {
            getLogger().info("[validaSegundaClave] [" + getClienteMB().getRut() + "] [BCI_FINOK] validacion=<"
                    + validacion + ">");
        }
        return validacion;
	}
	
	
	/**
	 * <p> Journaliza el evento.</p>
	 * <p><ul>
	 * <li>1.0 10/01/2012, Javier Aguirre (Imagemaker)): versi�n inicial. </li>
	 * <li>1.3 22/05/2012, Victor Agurto (Imagemaker)): modifica nivel de log en try-catch </li>
	 * <li>1.4 22/12/2014 Eduardo Villagr�n Morales (Imagemaker): se normaliza log.</li> 
	 * </ul></p>
	 * @param rut rut del cliente
	 * @param dv digito verificador del cliente
	 * @param canal canal usado para la creacion de clave
	 * @param codProducto codigo del producto
	 * @param codEvento codigo del evento
	 * @param codSubEvento codigo del subevento
	 * @param estadoEvento estado del evento
	 * @return string con datos de respuesta
	 */
	private String journalizarEvento( long rut, char dv, String canal, String codProducto, String codEvento,
	        String codSubEvento, String estadoEvento ) {
	        if (getLogger().isInfoEnabled()) {
                getLogger().info("[journalizarEvento] [" + rut + "] [BCI_INI] dv=<"+ dv + ">, canal=<"
                        + canal  +">, codProducto=<" + codProducto + ">, codEvento=<" + codEvento 
                        + ">, codSubEvento=<" + codSubEvento + ">, estadoEvento=<" + estadoEvento + ">");
            }
	        try {
	            Journal journal = new Journal();
	            Eventos evento;
	            Date fechaHoraActual = null;
	            String fechaActual = null;
	            String horaActual = null;
	            evento = eventos;
	            evento.setRutCliente( String.valueOf( rut ) );
	            evento.setDvCliente( String.valueOf( dv ) );
	            evento.setIdProducto( codProducto );
	            evento.setCodEventoNegocio( codEvento );
	            evento.setSubCodEventoNegocio( codSubEvento );
	            evento.setMedio( canal );
	            evento.setEstadoEventoNegocio( estadoEvento );
	            journal.journalizar( evento );
	            if (getLogger().isDebugEnabled()) {
                    getLogger().debug("[journalizarEvento] [" + rut + "] journalizaci�n OK");
                }

	            fechaHoraActual = evento.getFechaHora();
	            fechaActual = FechasUtil.convierteDateAString( fechaHoraActual, "dd/MM/yyyy" );
	            horaActual = FechasUtil.convierteDateAString( fechaHoraActual, "HH:mm" );
	            if (getLogger().isInfoEnabled()) {
	                getLogger().info("[journalizarEvento] [" + rut + "] [BCI_FINOK] Journalizaci�n OK.");
	            }
	            return evento.getNumeroOperacion() + "," + fechaActual + "," + horaActual;

	        }
	        catch ( Exception e ) {
	            if (getLogger().isEnabledFor(Level.ERROR)) {
                    getLogger().error("[journalizarEvento] [" + rut + "] [Exception] [BCI_FINEX] "
                            + e.getMessage(), e);
	            }
	        }
	        return " , , ";
	    }
	
	/**
	 * 
	 * Realiza la creaci�n de la clave telef�nica.
     * <p>
     * Registro de versiones:<ul>
     * <li>1.0| 10/01/2012, Javier Aguirre. (Imagemaker): versi�n inicial.</li>
     * <li>1.3 22/05/2012, Victor Agurto. (Imagemaker): modifica nivel de log en try-catch.</li>
     * <li>1.4 09/09/2013, Yon Sing Sius. (ExperimentoSocial): Se quita la llamada al m�todo validaTokenCliente. 
     *  La validaci�n de la segunda clave se deja en "crearClaveTelefonica". Se quita par�metro multipass</li>
     * <li>1.5 22/12/2014 Eduardo Villagr�n Morales (Imagemaker): normalizar log.</li> 
	 * </ul></p>
     * @param rut Rut del cliente.
     * @param dv  D�gito verificador del cliente.
     * @param canal  Canal por el cual se realiza la autenticaci�n.
     * @param clave La clave telef�nica ingresada por el cliente.
	 * @return verdadero si la clave se creo con exito
	 * @throws Exception en caso de error en la creaci�n
	 */
	private boolean crearClave(long rut, char dv, String canal, String clave) throws Exception {
	    if (getLogger().isInfoEnabled()) {
            getLogger().info("[crearClave] [" + rut + "] [BCI_INI] dv=<" + dv + ">, canal=<" + canal + ">");
		}
		boolean respuesta = false;
		try {
			ServiciosSeguridad serviciosSeguiridad = this.getServiciosSeguridad();
			Date fechaInicio = new Date();
			Calendar calendario = Calendar.getInstance();
			calendario.setTime(fechaInicio);
			calendario.add(Calendar.MONTH, MESESVENCIMIENTO);
			respuesta = serviciosSeguiridad.crearPin(CLAVE_GRUPO_CANAL, rut, dv, "", fechaInicio, 
			        calendario.getTime(), clave);
			if (getLogger().isDebugEnabled()) {
                getLogger().debug("[crearClave] [" + rut + "] respuesta=<" + respuesta + ">");
            }
			serviciosSeguiridad.cambiaPinCorporativo(CANAL_CLAVE_TELEFONICA, rut, dv, clave, clave);
			if (getLogger().isDebugEnabled()) {
                getLogger().debug("[crearClave] [" + rut + "] luego de llamar a cambio pin corporativo");
            }
			this.journalizarEvento(rut, dv, canal, JOURNAL_PRODUCTO, JOURNAL_EVENTO_SOLICITA_CLAVE, 
			        JOURNAL_SUBEVENTO, JOURNAL_ESTADOEVENTO);
		} 
		catch (SeguridadException e) {
		    if (getLogger().isEnabledFor(Level.WARN)) {
                getLogger().warn("[crearClave] [" + rut + "] [SeguridadException] " + e.getMessage(), e);
            }
			respuesta = false;
			this.journalizarEvento(rut, dv, canal, JOURNAL_PRODUCTO, JOURNAL_EVENTO_SOLICITA_CLAVE, 
			        JOURNAL_SUBEVENTO, ESTADOEVENTOJOURNAL);
			if (e.getCodigo().equals(ERROR_CLAVE_YA_EXISTE)) {
			    if (getLogger().isEnabledFor(Level.ERROR)) {
                    getLogger().error("[crearClave] [" + rut + "] [SeguridadException] [BCI_FINEX] Clave ya existe. Lanzo nueva Exception. "
                            + e.getMessage(), e);
			}
				throw new Exception(e.getSimpleMessage(), e);
			} 
			else {
			    if (getLogger().isEnabledFor(Level.ERROR)) {
                    getLogger().error("[crearClave] [" + rut + "] [SeguridadException] [BCI_FINEX] Error al generar clave. Lanzo nueva Exception. "
                            + e.getMessage(), e);
				}
				throw new Exception(TablaValores.getValor(ARCH_PARAM_ERROR, ERROR_GENERAR_CLAVE, "Desc"), e);
			}
		} 
		catch (GeneralException e) {
		    if (getLogger().isEnabledFor(Level.WARN)) {
                getLogger().warn("[crearClave] [" + rut + "] [GeneralException] " + e.getMessage(), e);
			}
			this.journalizarEvento(rut, dv, canal, JOURNAL_PRODUCTO, JOURNAL_EVENTO_SOLICITA_CLAVE, 
			        JOURNAL_SUBEVENTO, ESTADOEVENTOJOURNAL);
			if (getLogger().isEnabledFor(Level.ERROR)) {
                getLogger().error("[crearClave] [" + rut + "] [GeneralException] [BCI_FINEX] Error al generar clave. Lanzo nueva Exception. "
                        + e.getMessage(), e);
            }
			throw new Exception(e.getInfoAdic(), e);

		} 
		catch (Exception e) {
		    if (getLogger().isEnabledFor(Level.WARN)) {
                getLogger().warn("[crearClave] [" + rut + "] [Exception] " + e.getMessage(), e);
			}
			this.journalizarEvento(rut, dv, canal, JOURNAL_PRODUCTO, JOURNAL_EVENTO_SOLICITA_CLAVE, 
			        JOURNAL_SUBEVENTO, ESTADOEVENTOJOURNAL);
			if (getLogger().isEnabledFor(Level.ERROR)) {
                getLogger().error("[crearClave] [" + rut + "] [GeneralException] [BCI_FINEX] Error al generar clave. Lanzo nueva Exception. "
                        + e.getMessage(), e);
            }
			throw new Exception(TablaValores.getValor(ARCH_PARAM_ERROR, ERROR_GENERAR_CLAVE, "Desc"), e);
		}

		if (getLogger().isInfoEnabled()) {
            getLogger().info("[crearClave] [" + rut + "] [GeneralException] [BCI_FINOK] respuesta=<"
                    + respuesta + ">");
		}
		return respuesta;
	}
	
	/**
	 * <p>M�todo <i>Action</i> que permite crear la clave para acceder a servicios telef�nicos.</p>
     * <p>
     * Registro de versiones:<ul>
     * <li>1.0 10/01/2012, Javier Aguirre. (Imagemaker): versi�n inicial.</li>
     * <li>1.2  10/04/2012, Javier Aguirre. (ImageMaker): corrije la obtenci�n del canal a PinID</li>
     * <li>1.3  22/05/2012, Victor Agurto. (ImageMaker): corrige la obtenci�n de la clave multipass y
     *  nueva clave telef�nica. Corrigen niveles de log error en try-catch  </li>
     * <li>1.4  10/09/2013, Yon Sing Sius. (ExperimentoSocial): Se agrega validaci�n de segunda clave, si 
     * la validaci�n es incorrecta, se muestan los mensajes de error en la misma p�gina.
     * Se quita par�metro a la llamada del m�todo crearClave </li>
     * <li>1.5 22/12/2014 Eduardo Villagr�n Morales (Imagemaker): se normalizan logs.</li> 
	 * </ul></p>
	 * 
	 * @return el <i>outcome</i> a la siguiente p�gina
	 */
	public String crearClaveTelefonica() {
		long rutCliente = clienteMB.getRut();
		if (getLogger().isInfoEnabled()) {
            getLogger().info("[crearClaveTelefonica] [" + rutCliente + "] [BCI_INI]");
        }
		char dv = clienteMB.getDigitoVerif();
		String canalId = sesionMB.getCanalTO().getPinID();
        
		try {
			this.validacionInicial();
			if (!sesionMB.isActiva()) {
			    if (getLogger().isEnabledFor(Level.ERROR)) {
                    getLogger().error("[crearClaveTelefonica] [" + rutCliente 
                            + "] [BCI_FINEX] no hay sesi�n. Lanzo nueva GeneralException.");
                }
				throw new GeneralException(TablaValores.getValor(ARCH_PARAM_ERROR, "8366", "Desc"));
			}

			if (!validaSegundaClave()) {
			    if (getLogger().isInfoEnabled()) {
                    getLogger().info("[crearClaveTelefonica] [" + rutCliente 
                            + "] [BCI_FINEX] No valida segunda clave. Retorno=<>");
                }
				return "";
			}

			String claveEdit = StringUtil.completaPorLaDerecha(this.nuevaClaveTelefonica, LARGO_CLAVE, ' ');
			
			this.eventos = new Eventos();
			this.crearClave(rutCliente, dv, canalId, claveEdit);
			if (getLogger().isDebugEnabled()) {
                getLogger().debug("[crearClaveTelefonica] [" + rutCliente 
                        + "] [BCI_FINOK] redireccionando a p�gina comprobante.");
            }
			this.setErrorGeneral(false);
		} 
		catch (Exception e) {
			this.setErrorGeneral(true);
			this.setMensajeError(e.getMessage());
			if (getLogger().isEnabledFor(Level.ERROR)) {
                getLogger().error("[crearClaveTelefonica] [" + rutCliente + "] [Exception] [BCI_FINEX] "
                        + e.getMessage(), e);
            }
		}
		return CreacionClaveTelefonicaBackingMB.COMPROBANTE; 
	}
	
	/**
	 * <p> Valida que el valor ingresado como clave nueva coincida
	 *  con el valor ingresado como clave de confirmaci�n. </p>
	 *  
	 * Registro de versiones:<ul>
	 * <li>1.0 (20/01/2012 Javier Aguirre A. (Imagemaker IT)): versi�n inicial.</li>
	 * <li>1.1 22/12/2014 Eduardo Villagr�n Morales (Imagemaker): normalizando logs.</li> 
     * <li>1.2 05/09/2017  Marcelo Fuentes (SEnTRA) - Ricardo Carrasco (Ing. Soft. BCI): Se setea indicador que 
     * permite determinar si la clave telefonica ingresada por el cliente es valida.</li>
	 * </ul>
	 *  
	 * @param contexto contiene informaci�n del <i>request</i>
	 * @param componente referencia del <i>tag</i> jsf
	 * @param valor contenido del campo de texto
	 * @throws ValidatorException cuando no se cumple la validaci�n
	 */
	public void validate(FacesContext contexto, UIComponent componente, Object valor) throws ValidatorException {
	    long rutCliente = clienteMB.getRut();
		if (getLogger().isInfoEnabled()) {
            getLogger().info("[validate] [" + rutCliente + "] [BCI_INI] contexto=<" + contexto + ">, componente=<"
                    + componente + ">, valor=<" + valor + ">");
        }
		this.claveTelValidada = false;
		String claveNueva = (String) valor;
		UIInput confirmComponent = (UIInput) this.getConfirmarUIInput();
		String claveConfirma = (String) confirmComponent.getSubmittedValue();

		if (claveNueva == null || claveNueva.isEmpty() || claveConfirma == null || claveConfirma.isEmpty()) {
			if (getLogger().isInfoEnabled()) {
                getLogger().info("[validate] [" + rutCliente + "] [BCI_FINOK] nada que validar");
            }
			return; // Let required="true" do its job.
		}

		if (!claveNueva.equals(claveConfirma)) {
			confirmComponent.setValid(false);
			if (getLogger().isEnabledFor(Level.ERROR)) {
                getLogger().error("[validate] [" + rutCliente 
                        + "] [BCI_FINEX] Claves no coinciden. Lanzo ValidatorException.");
            }
			throw new ValidatorException(new FacesMessage(MSJE_ERROR_VALIDACION1));
		}

		if (claveNueva.length() < LARGO_MINIMO || claveConfirma.length() < LARGO_MINIMO) {
			confirmComponent.setValid(false);
			if (getLogger().isEnabledFor(Level.ERROR)) {
                getLogger().error("[validate] [" + rutCliente + "] [BCI_FINEX] No cumple largo m�nimo de " 
                        + LARGO_MINIMO + ". Lanzo ValidatorException.");
		}
			throw new ValidatorException(new FacesMessage(MSJE_ERROR_VALIDACION2));
		}
		
		this.nuevaClaveTelefonica = claveNueva;
		this.claveTelValidada = true;
		if (getLogger().isInfoEnabled()) {
            getLogger().info("[validate] [" + rutCliente + "] [BCI_FINOK]");
        }
	}
	
	/**
	 * <p>M�todo para obtener instancia de ejb de ServiciosSeguridad.</p>
	 * <br>
	 * Registro de versiones:<ul>
	 * <li>1.0 18/11/2011 Javier Aguirre A.(Imagemaker IT): versi�n inicial.</li>
	 * <li>1.1 23/12/2014 Eduardo Villagr�n Morales (Imagemaker): se normalizan logs</li> 
	 * </ul>
	 * @return instancia del ejb
	 * @throws Exception en caso de error de la creacion del servicio
	 * @since 1.0
	 */
	private ServiciosSeguridad getServiciosSeguridad() throws Exception {
	    long rutCliente = clienteMB.getRut();
	    if (getLogger().isInfoEnabled()) {
            getLogger().info("[getServiciosSeguridad] [" + rutCliente + "] [BCI_INI]");
        }
		if (serviciosSeguridad == null) {
		    if (getLogger().isDebugEnabled()) {
                getLogger().debug("[getServiciosSeguridad] [" + rutCliente + "] servicioSeguridad es null.");
            }
			try {
				Class<ServiciosSeguridad> homeClass = ServiciosSeguridad.class;
				EnhancedServiceLocator locator = EnhancedServiceLocator.getInstance();
				ServiciosSeguridadHome serviciosSeguridadHome = (ServiciosSeguridadHome) locator
						.getGenericService("wcorp.serv.seguridad.ServiciosSeguridad", homeClass);
				serviciosSeguridad = serviciosSeguridadHome.create();
				if (getLogger().isDebugEnabled()) {
                    getLogger().debug("[getServiciosSeguridad] [" + rutCliente + "] servicio instanciado");
                }
			} 
			catch (Exception ex) {
			    if (getLogger().isEnabledFor(Level.ERROR)) {
                    getLogger().error("[getServiciosSeguridad] [" + rutCliente 
                            + "] [Exception] [BCI_FINEX] Se lanza nueva Exception. " + ex.getMessage(), ex);
				}
				throw new Exception("Error al crear instancia de ejb ServiciosSeguridad",ex);
			}
		} 
		if (getLogger().isInfoEnabled()) {
            getLogger().info("[getServiciosSeguridad] [" + rutCliente + "] [BCI_FINOK] retornando instancia");
        }
			return serviciosSeguridad;
	}

	/**
	 * <p>M�todo para obtener instancia de ejb de ServiciosTarjetas.</p>
	 * <br>
	 * Registro de versiones:<ul>
	 * <li>1.0 18/11/2011 Javier Aguirre A.(Imagemaker IT): versi�n inicial.</li>
	 * <li>1.1 23/12/2014 Eduardo Villagr�n Morales (Imagemaker): se normaliza log.</li> 
	 * </ul>
	 * @return instancia del ejb
	 * @throws Exception en caso de error de la creacion del servicio
	 * @since 1.0
	 */
	private ServiciosTarjetas getServiciosTarjeta() throws Exception {
	    long rutCliente = clienteMB.getRut();
        if (getLogger().isInfoEnabled()) {
            getLogger().info("[getServiciosTarjeta] [" + rutCliente + "] [BCI_INI]");
        }
		if (serviciosTarjetas == null) {
		    if (getLogger().isDebugEnabled()) {
                getLogger().debug("[getServiciosTarjeta] [" + rutCliente + "] servicioTarjetas es null");
            }
			try {
				Class<ServiciosTarjetas> homeClass = ServiciosTarjetas.class;
				EnhancedServiceLocator locator = EnhancedServiceLocator.getInstance();
				ServiciosTarjetasHome serviciosTarjetasHome = (ServiciosTarjetasHome) locator
						.getGenericService("wcorp.serv.tarjetas.ServiciosTarjetas", homeClass);
				serviciosTarjetas = serviciosTarjetasHome.create();
				if (getLogger().isDebugEnabled()) {
                    getLogger().debug("[getServiciosTarjeta] [" + rutCliente + "] servicio instanciado");
                }
			} 
			catch (Exception ex) {
			    if (getLogger().isEnabledFor(Level.ERROR)) {
                    getLogger().error("[getServiciosTarjeta] [" + rutCliente 
                            + "] [Exception] [BCI_FINEX] lanzo nueva Exception. " + ex.getMessage(), ex);
				}
				throw new Exception("Error al crear instancia de ejb ServiciosTarjetas",ex);
			}
		}
		if (getLogger().isInfoEnabled()) {
            getLogger().info("[getServiciosTarjeta] [" + rutCliente + "] [BCI_FINOK] retornando instancia");
        }
			return serviciosTarjetas;
	}
	
	/**
	 * <p>M�todo para obtener instancia de ejb de ServiciosCliente.</p>
	 * <br>
	 * Registro de versiones:<ul>
	 * <li>1.0 18/11/2011 Javier Aguirre A.(Imagemaker IT): versi�n inicial.</li>
	 * <li>1.1 23/12/2014 Eduardo Villagr�n Morales (Imagemaker): se normaliza log.</li> 
	 * </ul>
	 * @return instancia del ejb
	 * @throws Exception en caso de error de la creacion del servicio
	 * @since 1.0
	 */
	private ServiciosCliente getServiciosCliente() throws Exception {
	    long rut = clienteMB.getRut();
	    if (getLogger().isInfoEnabled()) {
            getLogger().info("[getServiciosCliente] [" + rut + "] [BCI_INI]");
        }
		if (serviciosCliente == null) {
		    if (getLogger().isDebugEnabled()) {
                getLogger().debug("[getServiciosCliente] [" + rut + "] servicioCliente es null.");
            }
			try {
				Class<ServiciosCliente> homeClass = ServiciosCliente.class;
				EnhancedServiceLocator locator = EnhancedServiceLocator.getInstance();
				ServiciosClienteHome serviciosClienteHome = (ServiciosClienteHome) locator.getGenericService(
								"wcorp.serv.clientes.ServiciosCliente", homeClass);
				serviciosCliente = serviciosClienteHome.create();
				if (getLogger().isDebugEnabled()) {
                    getLogger().debug("[getServiciosCliente] [" + rut + "] servicio instanciado");
                }
			} 
			catch (Exception ex) {
			    if (getLogger().isEnabledFor(Level.ERROR)) {
                    getLogger().error("[getServiciosCliente] [" + rut 
                            + "] [Exception] [BCI_FINEX] lanzo nueva Exception. " + ex.getMessage(), ex);
				}
				throw new Exception("Error al crear instancia de ejb ServiciosCliente", ex);
			}
		} 
		if (getLogger().isInfoEnabled()) {
            getLogger().info("[getServiciosCliente] [" + rut + "] [BCI_FINOK] retorno instancia.");
        }
			return serviciosCliente;
	}
	
	/**
	 * <p>M�todo para obtener instancia de ejb TCreditoOnline.</p>
	 * <br>
	 * Registro de versiones:<ul>
	 * <li>1.0 18/11/2011 Javier Aguirre A.(Imagemaker IT): versi�n inicial.</li>
	 * <li>1.1 23/12/2014 Eduardo Villagr�n Morales (Imagemaker): se normaliza log.</li> 
	 * </ul>
	 * @return instancia del ejb
	 * @throws Exception en caso de error de la creacion del servicio
	 * @since 1.0
	 */
	private TCreditoOnline getTCreditoOnline() throws Exception {
	    long rut = clienteMB.getRut();
	    if (getLogger().isInfoEnabled()) {
            getLogger().info("[getTCreditoOnline] [" + rut + "] [BCI_INI]");
        }
		if (tCreditoOnline == null) {
		    if (getLogger().isDebugEnabled()) {
                getLogger().debug("[getTCreditoOnline] [" + rut + "] tCreditoOnline es null.");
            }
			try {
				Class<TCreditoOnline> homeClass = TCreditoOnline.class;
				EnhancedServiceLocator locator = EnhancedServiceLocator.getInstance();
				TCreditoOnlineHome tCreditoOnlineHome = (TCreditoOnlineHome) locator.getGenericService(
				        "wcorp.bprocess.tcreditoonline.TCreditoOnline", homeClass);
				tCreditoOnline = tCreditoOnlineHome.create();
				if (getLogger().isDebugEnabled()) {
                    getLogger().debug("[getTCreditoOnline] [" + rut + "] servicio instanciado");
                }
			} 
			catch (Exception ex) {
				if (getLogger().isEnabledFor(Level.ERROR)) {
                    getLogger().error("[getTCreditoOnline] [" + rut 
                            + "] [Exception] [BCI_FINEX] lanzo nueva Exception. " + ex.getMessage(), ex);
				}
				throw new Exception("Error al crear instancia de ejb TCreditoOnline",ex);
			}
		}
		if (getLogger().isInfoEnabled()) {
            getLogger().info("[getTCreditoOnline] [" + rut + "] [BCI_FINOK] retorno instancia");
        }
			return tCreditoOnline;
	}
	
	/**
	 * <p>M�todo que retorna el formato de fecha para el tag de segunda clave.</p>
	 * <br>
	 * Registro de versiones:<ul>
	 * <li>1.0 10/09/2013 Yon Sing Sius.(ExperimentoSocial): versi�n inicial.</li>
	 * <li>1.1 23/12/2014 Eduardo Villagr�n Morales (Imagemaker): normalizaci�n log.</li>
	 * </ul>
	 * @return String con la fecha formateada
	 * @since 1.5
	 */
	public String getFechaCreacion() {
	    if (getLogger().isDebugEnabled()) {
            getLogger().debug("[getFechaCreacion] [BCI_INI]");
        }
		Date hoy = new Date();
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmss");
		String date = format.format(hoy);
		if (getLogger().isDebugEnabled()) {
            getLogger().debug("[getFechaCreacion] [BCI_FINOK] date=<" + date + ">");
        }
		return date;
	}
	
	/**
	 * Retorna la instancia Logger.
	 * 
	 * <p>Registro de versiones:<ul>
	 * <li>1.0 22/12/2014 Eduardo Villagr�n Morales (Imagemaker): versi�n inicial. 
     * </ul>
     * 
	 * @return el logger de la clase.
	 * @since 1.7
	 */
	private Logger getLogger(){
	    if (logger==null){
	        logger = Logger.getLogger(CreacionClaveTelefonicaBackingMB.class);
	    }
	    return logger;
	}

	public boolean isClaveTelValidada() {
		return claveTelValidada;
	}

	public void setClaveTelValidada(boolean claveTelValidada) {
		this.claveTelValidada = claveTelValidada;
	}
	

}
