package cl.bci.aplicaciones.bancaprivada.emergencias.mb;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import cl.bci.aplicaciones.cliente.mb.ClienteMB;
import cl.bci.infraestructura.web.seguridad.mb.SesionMB;
import cl.bci.infraestructura.web.seguridad.segundaclave.SegundaClaveUIInput;
import cl.bci.infraestructura.web.seguridad.util.ConectorStruts;
import wcorp.bprocess.administraciondeclaves.AdmClaveException;
import wcorp.bprocess.tcreditoonline.TCreditoOnline;
import wcorp.bprocess.tcreditoonline.TCreditoOnlineHome;
import wcorp.bprocess.tcreditoonline.vo.ResultadoServicioCuentasVO;
import wcorp.model.seguridad.SessionBCI;
import wcorp.serv.clientes.Operacion;
import wcorp.serv.clientes.ServiciosCliente;
import wcorp.serv.clientes.ServiciosClienteHome;
import wcorp.serv.seguridad.EstadoPin;
import wcorp.serv.seguridad.SeguridadException;
import wcorp.serv.seguridad.ServiciosSeguridad;
import wcorp.serv.seguridad.ServiciosSeguridadHome;
import wcorp.serv.tarjetas.ServiciosTarjetas;
import wcorp.serv.tarjetas.ServiciosTarjetasHome;
import wcorp.serv.tarjetas.TarjetaCliente;
import wcorp.util.EnhancedServiceLocator;
import wcorp.util.FechasUtil;
import wcorp.util.GeneralException;
import wcorp.util.StringUtil;
import wcorp.util.TablaValores;
import wcorp.util.journal.Eventos;
import wcorp.util.journal.Journal;

/**
 * Support ManagedBean
 * <br>
 * <b>DesbloqueoClaveTelefonicaBackingMB</b>
 * <br>
 * Managed bean que participa en las vistas para la opcion desbloqueo de clave telefonica.
 * <br>
 * Registro de versiones:<ul>
 * <li>1.0 (20/11/2011 Javier Aguirre A. (Imagemaker IT)): versi�n inicial.</li>
 * <li>1.3 22/05/2011 Victor Agurto. (Imagemaker IT): cambian niveles de logs en try-catch.</li>
 * <li>1.4 17/07/2012 Gonzalo Bustamante(Sermaluc): Se incluye un indicador para saber si la excepcion es 
 * causada por validacion de multipass. Se modifica el m�todo validaTokenCliente()</li>
 * <li>2.0 22/05/2013 Pablo Sanhueza (TINet): <ul>
 *      <li>Se elimina metodos referente a la validaci�n y estado del token del cliente 
 *      (validaTokenCliente, dameEstadoToken respectivamente), ya que ahora la validaci�n de la
 *      segunda clave se realiza mediante el atributo {@link #segundaClave}, el cual fue agregado en 
 *      esta versi�n con sus respectivos get y set.</li>
 * <li>Se eliminan atributos ERROR_VERIFICANDO_TOKEN, TOKEN_SOLICITAR_TOKEN,
 *      ERROR_MULTIPASS_NO_HABILITADO, ERROR_GENERAL utilizados en los codigos de las
 *      validaciones eliminadas.</li>
 * <li>Se agrega la constante {@link #FORMATO_FECHA} y {@link #RECURSO_MENSAJE}.</li>
 * <li>Se crea el m�todo {@link #getFechaActualFormateada()}.</li>
 * <li>Se elimina atributo repetidaClaveTelefonica ya que no es utilizado.</li>
 *      </ul></li>
 * <li>2.1 (16/05/2014 Luis Lopez Alamos (SEnTRA)): Se modifica el atributo LARGO_MINIMO por 4 y se reemplaza
 * la parametrizaci�n de la nueva clave por una clave editada de 4 d�gitos y concatenada con 2 espacios.</li>
 * <li>2.2 (29/10/2014 V�ctor Ortiz Rivero (BCI)): Se modifica {@link #desbloquearClave(long, 
 * char, String, String, String)} para que clientes que olviden su clave o se encuentre 
 * expirada puedan cambiar su clave telefonica, adem�s se normaliza log de estos metodos
 * seg�n la normativa vigente</li>
 * <li>2.3 19/12/2014 Eduardo Villagr�n Morales (Imagemaker): Se agrega constante {@link #TIPO_USUARIO} y se
 *      modifican llamadas al m�todo {@link #validarAcceso(long, char, String, char)}. Se normalizan logs.</li> 
 * <li>3.0 05/09/2017, Marcelo Fuentes (SEnTRA) - Ricardo Carrasco (Ing. Soft. BCI): Se cambia alcance del ManagedBean
 * de RequestScoped a ViewScoped debido a que se agrego un nuevo paso en la vista de desbloqueo/modificacion de clave  
 * para separar el ingreso de la validacion de la segunda clave. Se modifica m�todo
 * {@link #validate(FacesContext, UIComponent, Object)}. Se agrega atributo {@link #claveTelValidada}.</li>
 * </ul>
 *
 * <p><b>Todos los derechos reservados por Banco de Cr�dito e Inversiones.</b></p>
 */

@ManagedBean
@ViewScoped
public class DesbloqueoClaveTelefonicaBackingMB implements Serializable {

    /**
     * Serial de versi�n.
     */
    private static final long serialVersionUID = -4208878013560772593L;
        
    /**
     *  <i>outcome</i> de la pagina de comprobante.
     */
    private static final String COMPROBANTE = "comprobanteDesbloqueoClaveTelefonica";
    
     /**
     * Clave que corresponde al canal de una clave telef�nica en el banco.
     */
    private static final String CANAL_CLAVE_TELEFONICA = "220";
    
     /**
     * Clave que corresponde al canal de una clave telef�nica activa en el banco.
     */
    private static final String ESTADO_CLAVE_ACTIVA = "00";

	/**
	 * Estado de una clave bloqueada.
	 */
	private static final String ESTADO_CLAVE_BLOQUEADA = "01";

	/**
	 * Estado de una clave restringida.
	 */
	
	private static final String ESTADO_CLAVE_RESTRINGIDA = "02";

	/**
	 * Motivo de estado bloqueado por Decision BCI.
	 */
	private static final String MOTIVO_DECISION_BCI= "03";
   

   /**
    * C�digo de error al desbloquear la clave telef�nica.
    */
   private static final String ERROR_DESBLOQUEAR_CLAVE = "CT_0005";
   
   /**
    * C�digo de error al desbloquear la clave telef�nica, bloqueada por BCI.
    */
   private static final String ERROR_RESTRICCION_CLAVE = "CT_0008";

   /**
    *  Atributo que identifica al tipo de clave Telef�nica.
    */
   private static final String CLAVE_TELEFONICA = "  ";

   /**
    * Identificador de producto.
    */
   private static final String JOURNAL_PRODUCTO = "SEGURI";

   /**
    * Nombre del evento para journalizaci�n de desbloqueo de clave telef�nica.
    */
   private static final String JOURNAL_EVENTO_DESBLOQUEA_CLAVE = "DESBLOQUEA";

   /**
    * C�digo de subevento.
    */
   private static final String JOURNAL_SUBEVENTO = "CLVTEL";
   
   /**
    * C�digo del estado del evento OK utilizado para journalizar.
    */
   private static final String JOURNAL_ESTADOEVENTO = " ";
    
    /**
     * Cantidad de meses que la clave estar� activa.
     */
    private static final int MESES_VENCIMIENTO = 12;
    
    /**
     * Clave grupo canal para registrar una clave telef�nica en el banco.
     */
    private static final String CLAVE_GRUPO_CANAL = "14";
           
    /**
     * Estado de una tarjeta de d�bito activa.
     */
    private static final String ESTADO_TARJERA_DEBITO_ACTIVADA = "003";
    
    /**
     * Archivo de Par�metros de Errores.
     */
    private static final String ARCH_PARAM_ERROR = "errores.codigos";
    
    /**
     * C�digo de error cliente sin tarjetas de d�bito.
     */
    private static final String ERROR_SIN_TARJETAS_ACTIVAS = "CT_0002";
    
    /**
     * C�digo de error al verificar las tarjetas de d�bito.
     */
    private static final String ERROR_VERIFICANDO_TARJETAS = "CT_0001";
    
    /**
     * C�digo indicador de operacion vigente.
     */
    private static final String OPERACION_VIGENTE = "VIG";

    /**
     * C�digo indicador de operacion del tipo cuenta corriente.
     */
    private static final String OPERACION_CUENTA_CORRIENTE = "CCT";

    /**
     * C�digo indicador de operacion del tipo cuenta prima.
     */
    private static final String OPERACION_CUENTA_PRIMA = "CPR";
    
    /**
     * C�digo de error que indica que la clave no est� bloqueada.
     */
    private static final String ERROR_CLAVE_NO_BLOQUEADA = "CT_0006";

    /**
     * C�digo de error al verificar el estado de la clave telef�nica.
     */
    private static final String ERROR_VERIFICANDO_ESTADO_CLAVE = "CT_0007";
    
    /**
     * C�digo del estado del evento utilizado para journalizar.
     */
    private static final String ESTADOEVENTOJOURNAL = "NOK";
    
    /**
     * Mensaje validaci�n.
     */
    private static final String MSJE_ERROR_VALIDACION1 = "Claves no coinciden";
    
    /**
     * Mensaje validaci�n 2.
     */
    private static final String MSJE_ERROR_VALIDACION2 ="Debe ser de 4 d�gitos";
    
    /**
     * Largo m�nimo.
     */
    private static final int LARGO_MINIMO = 4;

    /**
     * Formato de fecha utilizado para efectos de determinaci�n de segunda clave.
     */
    private static final String FORMATO_FECHA = "yyyyMMddHHmm";

    /**
     * Ruta en donde se encuentra el archivo con la mensajer�a de la aplicaci�n.
     */
    private static final String RECURSO_MENSAJE = "cl.bci.aplicaciones.emergencias.emergencias";
    /**
     * Constante para tipo usuario.
     * @since 2.3
     */
    private static final char TIPO_USUARIO = 'T';

    /**
     * Atributo que declara valor maximo de caracteres que se envian.
     */
    private static final int LARGO_CLAVE = 6;
    
    /**
     * Variable para asignar log de ejecuci�n.
     */
    private static transient Logger logger = (Logger) Logger.getLogger(DesbloqueoClaveTelefonicaBackingMB.class);
    
     /**
     * Inyeccion dependencia Managed bean sesionMB.
     */
    @ManagedProperty(value = "#{sesionMB}")
    private SesionMB sesionMB;
    
     /**
     * Inyeccion dependencia Managed bean clienteMB.
     */
    @ManagedProperty(value = "#{clienteMB}")
    private ClienteMB clienteMB;
    
    /**
     * Nueva clave telef�nica.
     */
    private String nuevaClaveTelefonica;
    
    /**
     * Clave Multipass.
     */
    private String claveMultipass;
    
    /**
     * Error general.
     */
    private boolean errorGeneral;
    /**
     * Mensaje de error.
     */
    private String mensajeError;   

    /**
     * Servicio seguridad.
     */
    private ServiciosSeguridad serviciosSeguridad;
    /**
     * Servicio Tarjetas.
     */
    private ServiciosTarjetas serviciosTarjetas;
    /**
     * Servicio de cliente.
     */
    private ServiciosCliente serviciosCliente;
    
    /**
    * Eventos.
    */
    private TCreditoOnline tCreditoOnline;

    /**
     * Eventos.
     */
    private Eventos eventos;
    
    /**
     * Indicador de error multipass.
     */
    private boolean multipassError;

    /**
     * Componentes usado para despliegue y validaci�n de segunda clave.
     */
    private transient SegundaClaveUIInput segundaClave;
    
    /**
     * Indica si la clave telefonica esta validada.
     */
    private boolean claveTelValidada;
    
    /** 
     *  Construye un nuevo DesbloqueoClaveTelefonicaBackingMB.
     */
    public DesbloqueoClaveTelefonicaBackingMB(){
        logger.debug("[DesbloqueoClaveTelefonicaBackingMB]:Constructor");
    }
    
    public String getNuevaClaveTelefonica() {
        return "";
    }

    public void setNuevaClaveTelefonica(String nuevaClaveTelefonica) {
        this.nuevaClaveTelefonica = nuevaClaveTelefonica;
    }

    public String getRepetidaClaveTelefonica() {
        return "";
    }

    public String getClaveMultipass() {
        return "";
    }

    public void setClaveMultipass(String claveMultipass) {
        this.claveMultipass = claveMultipass;
    }

    public ClienteMB getClienteMB() {
        return clienteMB;
    }

    public void setClienteMB(ClienteMB clienteMB) {
        this.clienteMB = clienteMB;
    }

    public SesionMB getSesionMB() {
        return sesionMB;
    }

    public void setSesionMB(SesionMB sesionMB) {
        this.sesionMB = sesionMB;
    }

    /**
     * M�todo que obtiene indicador que informa de algun error al desbloquear la clave telefonica.
     *<p>
     * Registro de versiones:
     * <UL>
     * <li>1.0 (16/11/2011 Javier Aguirre A. (Imagemaker IT)): versi�n inicial.</li>
     * <li>1.1 05/09/2017, Marcelo Fuentes (SEnTRA) - Ricardo Carrasco (Ing. Soft. BCI): Se obtiene indicador de error 
     * desde atributo en el request.</li>
     * </UL></p>
     * @return boolean indicando si hubo algun error.
     * @since 1.0
     */
    public boolean isErrorGeneral() {
        HttpServletRequest req = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        String error = (String)req.getAttribute("errorGeneral");
        return (error != null && error.equals("true") ? true : false);
    }

    /**
     * M�todo que setea indicador que informa de algun error al desbloquear la clave telefonica.
     *<p>
     * Registro de versiones:
     * <UL>
     * <li>1.0 (16/11/2011 Javier Aguirre A. (Imagemaker IT)): versi�n inicial.</li>
     * <li>1.1 05/09/2017, Marcelo Fuentes (SEnTRA) - Ricardo Carrasco (Ing. Soft. BCI): Se setea indicador de error 
     * en atributo en el request.</li>
     * </UL></p>
     * @param errorGeneral Indica si hubo algun error.
     * @since 1.0
     */
    public void setErrorGeneral(boolean errorGeneral) {
        HttpServletRequest req = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        req.setAttribute("errorGeneral", String.valueOf(errorGeneral));
        this.errorGeneral = errorGeneral;
    }

    public String getMensajeError() {
        return mensajeError;
    }

    public void setMensajeError(String mensajeError) {
        this.mensajeError = mensajeError;
    }
    
    public boolean isMultipassError() {
        return multipassError;
    }

    public void setMultipassError(boolean multipassError) {
        this.multipassError = multipassError;
    }

    public SegundaClaveUIInput getSegundaClave() {
        return segundaClave;
    }

    public void setSegundaClave(SegundaClaveUIInput segundaClave) {
        this.segundaClave = segundaClave;
    }

    /**
     * <p>M�todo para validar si cliente corresponde a MonoProducto (Tarjeta de Cr�dito).</p>
     *
     * <p>Registro de versiones:</p><ul>
     * <li>1.0 10/01/2012, Javier Aguirre (Imagemaker)): versi�n inicial.</li>
     * <li>1.3 22/05/2012, Victor Agurto (Imagemaker)): cambian niveles de logs en try-catch.</li>
     * <li>1.4 23/12/2014 Eduardo Villagr�n Morales (Imagemaker): se normaliza log.</li> 
     * </ul>
     *
     * <B>Todos los derechos reservados por Banco de Cr�dito e Inversiones.</B>
     *
     * @param rut Rut del Cliente MonoProducto.
     * @param dv Digito Verificador del Cliente MonoProducto.
     * @throws Exception si ocurre un error en la validaci�n
     * @return boolean verdadero si es cliente monoProducto
     */
    private boolean validarClienteMonoproducto(long rut, char dv) throws Exception {
        if (getLogger().isInfoEnabled()) {
            getLogger().info("[validarClienteMonoproducto] [" + rut + "] [BCI_INI]");
        }
        char tipoCliente = clienteMB.getTipoUsuario().charAt(0);
        boolean esTDC = true;
        boolean esCCT = false;
        boolean esCPR = false;
        try {
            Operacion[] operacionesCliente = this.getServiciosCliente().consultaOperaciones(rut, dv, tipoCliente);
            if (getLogger().isDebugEnabled()) {
                int largo = operacionesCliente!=null?operacionesCliente.length:0;
                getLogger().debug("[validarClienteMonoproducto] [" + rut + "] operacionesCliente=<"
                        + operacionesCliente + ">, largo=<" + largo + ">");
            }

                if (logger.isDebugEnabled()) {
                logger.debug("[validarClienteMonoproducto] Llamada a ServicioCliente Exitosa!!! ");
            }

            for (int i = 0; i < operacionesCliente.length; i++) {
                if (OPERACION_VIGENTE.equals(operacionesCliente[i].codEstado)
                        && OPERACION_CUENTA_CORRIENTE.equals(operacionesCliente[i].tipoOperacion)) {
                    esCCT = true;
                    esTDC = false;
                    break;
                }
                else if (OPERACION_VIGENTE.equals(operacionesCliente[i].codEstado)
                        && OPERACION_CUENTA_PRIMA.equals(operacionesCliente[i].tipoOperacion)) {
                    esCPR = true;
                    esTDC = false;
                    break;
                }
            }
            if (getLogger().isDebugEnabled()) {
                getLogger().debug("[validarClienteMonoproducto] [" + rut + "] esCPR=<" + esCPR + ">, esCCT=<"
                        + esCCT + ">");
            }

            if (!esCCT && !esCPR) {
                ResultadoServicioCuentasVO cuentasCliente = this.getTCreditoOnline().getCuentasPorRut(rut, dv);
                if (getLogger().isDebugEnabled()) {
                    getLogger().debug("[validarClienteMonoproducto] [" + rut + "] cuentaCliente=<" 
                            + cuentasCliente + ">" );
                }
                if (cuentasCliente != null && cuentasCliente.getCuentas() != null 
                        && cuentasCliente.getCuentas().length > 0) {
                    if (getLogger().isDebugEnabled()) {
                        getLogger().debug("[validarClienteMonoproducto] [" + rut + "] obteniendo ctas TDC");
                    }
                    esTDC = true;
                } 
                else {
                    if (getLogger().isEnabledFor(Level.WARN)) {
                        getLogger().warn("[validarClienteMonoproducto] [" + rut + "] cliente sin TDC");
                    }
                    esTDC = false;
                }
            } 
            else {
                if (getLogger().isDebugEnabled()) {
                    getLogger().debug("[validarClienteMonoproducto] [" + rut + "] cliente no monoproducto tdc");
                }
                esTDC = false;
            }
        }
        catch (GeneralException e) {
            if (getLogger().isEnabledFor(Level.ERROR)) {
                getLogger().error("[validarClienteMonoproducto] [" + rut 
                        + "] [GeneralException] [BCI_FINEX] relanzo GeneralException. " + e.getMessage(), e);
            }
            throw e;
        } 
        catch (Exception ex) {
            if (getLogger().isEnabledFor(Level.ERROR)) {
                getLogger().error("[validarClienteMonoproducto] [" + rut 
                        + "] [Exception] [BCI_FINEX] Lanzo nueva Exception");
            }
            throw new Exception(TablaValores.getValor("errores.codigos", "EDDA-015", "Desc"), ex);
        }
        if (getLogger().isInfoEnabled()) {
            getLogger().info("[validarClienteMonoproducto] [" + rut + "] [BCI_FINOK] esTDC=<" + esTDC + ">");
        }
        return esTDC;
    }
    
    /**
     *  Valida si cliente posee tarjetas activas y/o si posee token habilitado.
     *<p>
     * Registro de versiones:
     * <UL>
     * <LI>1.0 (10/01/2012, Javier Aguirre (ImageMaker)): version inicial</LI>
     * <li>1.3 22/05/2012, Victor Agurto (Imagemaker)): cambian niveles de logs en try-catch.</li>
     * <li>2.0 22/05/2013, Pablo Sanhueza (TINet): Se elimina validaci�n de habilitaci�n token del cliente.</li>
     * <li>2.1 23/12/2014 Eduardo Villagr�n Morales (Imagemaker): se normaliza log.</li> 
     * </UL></p>
     * @param rut rut del cliente
     * @param dv digito verificador cliente
     * @param canal canal asociado
     * @param tipoUsuario el tipo de usuario
     * @throws Exception En caso de un error en la validaci�n
     * @since 1.0
     */
    private void validarAcceso(long rut, char dv, String canal, char tipoUsuario) throws Exception {
        if (getLogger().isInfoEnabled()) {
            getLogger().info("[validarAcceso] [" + rut + "] [BCI_INI] dv=<" + dv + ">, canal=<" + canal 
                    + ">, tipoUsuario=<" + tipoUsuario + ">");
        }
        boolean hayTarjetasActivas = false;
        final int numeroCaracteres = 4;
        try {
            TarjetaCliente tarjetasATM = null;
            tarjetasATM = this.getServiciosTarjeta().getTarjetasATMXInd(rut,tipoUsuario);
            if (getLogger().isDebugEnabled()) {
                getLogger().debug("[validarAcceso] [" + rut + "] tarjetasATM=<" + tarjetasATM + ">");
            }
            
            tarjetasATM = this.getServiciosTarjeta().getTarjetasATMXInd(rut,
                    tipoUsuario);

            if (tarjetasATM != null && tarjetasATM.tarjeta != null) {
                for (int i = 0; i < tarjetasATM.tarjeta.length; i++) {
                    if (getLogger().isDebugEnabled()) {
                        getLogger().debug("[validarAcceso] [" + rut + "] tarjetasATM.tarjeta[" + i 
                                + "].numeroTarjeta=<" + StringUtil.censura(tarjetasATM.tarjeta[i].numeroTarjeta,
                                ' ', numeroCaracteres, false, false) + ">, tarjetasATM.tarjeta[" + i 
                                + "].estado=<" + tarjetasATM.tarjeta[i].estado + ">");
                    }
                    if (tarjetasATM.tarjeta[i].estado.equals(ESTADO_TARJERA_DEBITO_ACTIVADA)) {
                        if (getLogger().isInfoEnabled()) {
                            getLogger().info("[validarAcceso] [" + rut + "] hay tarjeta activa");
                        }
                        hayTarjetasActivas = true;
                        break;
                    }
                }
            } 
            else {
                if (getLogger().isEnabledFor(Level.WARN)) {
                    getLogger().warn("[validarAcceso] [" + rut + "] no se obtuvieron tarjetas del cliente");
                }
            }

        } 
        catch (Exception e) {
            if (getLogger().isEnabledFor(Level.ERROR)) {
                getLogger().error("[validarAcceso] [" + rut 
                        + "] [Exception] [BCI_FINEX] No se pudo analizar si tiene tarjetas activas. Lanzo nueva Exception. "
                        + e.getMessage(), e);
            }
            throw new Exception(TablaValores.getValor(ARCH_PARAM_ERROR, ERROR_VERIFICANDO_TARJETAS, "Desc"), e);
        }

        if (!hayTarjetasActivas) {
            if (getLogger().isEnabledFor(Level.ERROR)) {
                getLogger().error("[validarAcceso] [" + rut + "] no tiene tarjetas activas. Lanzo nueva Exception.");
            }
            throw new Exception(TablaValores.getValor(ARCH_PARAM_ERROR, ERROR_SIN_TARJETAS_ACTIVAS, "Desc"));
        }
        if (getLogger().isInfoEnabled()) {
            getLogger().info("[validarAcceso] [" + rut + "] [BCI_FINOK] hayTarjetasActivas=<" + hayTarjetasActivas + ">");
        }
        return;

    }
    
    /**
     * <p>M�todo  para verificar los tipos de accesos del usuario.</p>
     * 
     * <p>Registro de versiones:</p><ul>
     * <li>1.0 10/01/2012, Javier Aguirre (Imagemaker)): versi�n inicial.</li>
     * <li>1.2 10/04/2012, Javier Aguirre (ImageMaker)): corrije la obtenci�n canal a PinID 
     *         y cambia la obtenci�n de <i>tipousuario</i> desde clienteMB a sessionBci. 
     *      Corrije mensajes de log</li>
     * <li>1.3 22/05/2012, Victor Agurto (Imagemaker)): cambian niveles de logs en try-catch.</li>
     * <li>1.4 19/12/2014 Eduardo Villagr�n Morales (Imagemaker): Se modifica llamada al m�todo
     *      {@link #validarAcceso(long, char, String, char)} para hacer uso de constante 
     *      {@link #TIPO_USUARIO} como cuarto par�metro.</li>
     * <li>1.5 23/12/2014 Eduardo Villagr�n Morales (Imagemaker): se normalizan logs.</li>   
     * </ul>
     *
     * <B>Todos los derechos reservados por Banco de Cr�dito e Inversiones.</B>
     * @throws Exception 
     */
    public void validacionInicial() throws Exception {
        logger.debug("[validacionInicial] inicio m�todo");

        long rut = clienteMB.getRut();
        if (getLogger().isInfoEnabled()) {
            getLogger().info("[validacionInicial] [" + rut + "] [BCI_INI]");
        }
        char dv = clienteMB.getDigitoVerif();
        String canal = sesionMB.getCanalTO().getPinID();
        if (getLogger().isDebugEnabled()) {
            getLogger().debug("[validacionInicial] [" + rut + "] canal=<" + canal + ">");
        }
        FacesContext fc = FacesContext.getCurrentInstance();
        SessionBCI sessionBci = ConectorStruts.getSessionBCI();
        
        if(sessionBci == null) {
            if (getLogger().isEnabledFor(Level.WARN)) {
                getLogger().warn("[validacionInicial] [" + rut 
                        + "] [BCI_FINEX] sin sesi�n BCI. Lanzo GeneralException.");
            }
                throw new GeneralException("No hay Sesion BCI");
        }
        char tipoUsuario = sessionBci.tipoUser;

        try {

            if (!sesionMB.isActiva()) {
                if (getLogger().isEnabledFor(Level.WARN)) {
                    getLogger().warn("[validacionInicial] [" + rut 
                            + "] [BCI_FINEX] no hay sesi�n activa. Lanzo GeneralException.");
                }
                throw new GeneralException(TablaValores.getValor(ARCH_PARAM_ERROR, "8366", "Desc"));
            }

            eventos = new Eventos();
            if (this.validarClienteMonoproducto(rut, dv)) {
                if (getLogger().isDebugEnabled()) {
                    getLogger().debug("[validacionInicial] [" + rut + "] validaci�n monoproducto exitosa");
                }
                fc.getExternalContext().redirect("solicitudClaveMonoproducto.do");
                if (getLogger().isDebugEnabled()) {
                    getLogger().debug("[validacionInicial] [" + rut + "] defino redireccionado.");
                }
            } 
            else {
                this.validarAcceso(rut, dv, canal, TIPO_USUARIO);
                if (getLogger().isDebugEnabled()) {
                    getLogger().debug("[validacionInicial] [" + rut + "] validaci�n acceso exitosa");
                }
                }
            }
        catch (Exception e) {
            if (getLogger().isEnabledFor(Level.ERROR)) {
                getLogger().error("[validacionInicial] [" + rut + "] [Exception] [BCI_FINEX] relanzo Exception. " 
                        + e.getMessage(), e);
            }
            throw e;

        }
        if (getLogger().isInfoEnabled()) {
            getLogger().info("[validacionInicial] [" + rut + "] [BCI_FINOK]");
        }
    }
    
    /**
     * Resetea clave en tandem
     * <P>
     * Registro de versiones:
     * <UL>
     * <li>1.0 13-01-2012, Javier Aguirre (ImageMaker): versi�n inicial.
     * <li>1.1 29-10-2014, V�ctor Ortiz (BCI): Se modifica log 
     * <li>1.2 23/12/2014 Eduardo Villagr�n Morales (Imagemaker): se normaliza log.</li> 
     * </UL>
     * </P>
     * 
     * @param canal Codigo canal
     * @param rut Rut del cliente
     * @param dv Digito verificador
     * @param motivo Motivo por el cual se bloquea.
     * @param usuario Usuario responsable del cambio de estado
     * @param clave clave telefonica 
     * @throws Exception En caso de producirse algun error en el proceso.
     * @since 1.0
     */
    private void reseteaClave(String canal, long rut, char dv, String motivo, String usuario, String clave) 
            throws Exception {
        if (getLogger().isInfoEnabled()) {
            getLogger().info("[reseteaClave] [" + rut + "] [BCI_INI] canal=<" + canal + ">, rut=<" + rut 
                    + ">, dv=<" + dv + ">, motivo=<" + motivo + ">, usuario=<" + usuario + ">");
		}
        try {
            this.getServiciosSeguridad().cambiaEstadoPin(canal, rut, dv, "0", "00", motivo, clave, "1");
            if (getLogger().isInfoEnabled()) {
                getLogger().info("[reseteaClave] [" + rut + "] [BCI_FINOK] cambio de pin exitoso.");
			}			
			}
        catch (Exception e) {
            if (getLogger().isEnabledFor(Level.ERROR)) {
                getLogger().error("[reseteaClave] [" + rut 
                        + "] [BCI_FINEX] [Exception] Error en cambio pin. Lanzo nueva AdmClaveException. "
                        + e.getMessage(), e);
        } 
            throw new AdmClaveException("Error al bloquear Clave");
        }
    }
    
    /**
     * <p> Journaliza el evento.</p>
     * <p><ul>
     * <li>1.0 10/01/2012, Javier Aguirre (Imagemaker)): versi�n inicial. </li>
     * <li>1.3 22/05/2012, Victor Agurto (Imagemaker)): cambian niveles de logs en try-catch.</li>
     * <li>1.4 23/12/2014 Eduardo Villagr�n Morales (Imagemaker): se normaliza log.</li> 
     * </ul></p>
     * @param rut rut del cliente
     * @param dv digito verificador del cliente
     * @param canal canal usado para la creacion de clave
     * @param codProducto codigo del producto
     * @param codEvento codigo del evento
     * @param codSubEvento codigo del subevento
     * @param estadoEvento estado del evento
     * @return string con datos de respuesta
     */
    private String journalizarEvento(long rut, char dv, String canal, String codProducto, String codEvento, 
            String codSubEvento, String estadoEvento) {
        if (getLogger().isInfoEnabled()) {
            getLogger().info("[journalizarEvento] [" + rut + "] [BCI_INI] canal=<" + canal + ">, codProducto=<"
                    + codProducto + ">, codEvento=<" + codEvento + ">, codSubEvento=<" + codSubEvento 
                    + ">, estadoEvento=<" + estadoEvento + ">");
        }
        try {
            Journal journal = new Journal();
            Eventos evento;
            Date fechaHoraActual = null;
            String fechaActual = null;
            String horaActual = null;

            evento = eventos;
            evento.setRutCliente(String.valueOf(rut));
            evento.setDvCliente(String.valueOf(dv));
            evento.setIdProducto(codProducto);
            evento.setCodEventoNegocio(codEvento);
            evento.setSubCodEventoNegocio(codSubEvento);
            evento.setMedio(canal);
            evento.setEstadoEventoNegocio(estadoEvento);
            journal.journalizar(evento);
            if (getLogger().isDebugEnabled()) {
                getLogger().debug("[journalizarEvento] [" + rut + "] journalizaci�n OK");
            }

            fechaHoraActual = evento.getFechaHora();
            fechaActual = FechasUtil.convierteDateAString(fechaHoraActual, "dd/MM/yyyy");
            horaActual = FechasUtil.convierteDateAString(fechaHoraActual, "HH:mm");
            if (getLogger().isDebugEnabled()) {
                getLogger().debug("[journalizarEvento] [" + rut + "] fechaHoraActual=<" + fechaHoraActual
                        + ">, fechaActual=<" + fechaActual + ">, horaActual=<" + horaActual + ">");
            }
            if (getLogger().isInfoEnabled()) {
                getLogger().info("[journalizarEvento] [" + rut + "] [BCI_FINOK]");
            }
            return evento.getNumeroOperacion() + "," + fechaActual + "," + horaActual;
        } 
        catch (Exception e) {
            if (getLogger().isEnabledFor(Level.ERROR)) {
                getLogger().error("[journalizarEvento] [" + rut + "] [Exception] [BCI_FINEX] " + e.getMessage(), e);
        }
        return " , , ";
        }
    }

    /**
     * Realiza el desbloqueo de la clave telef�nica.
     * <p>
     * Registro de versiones:<ul>
     * <li>1.0 10/01/2012, Javier Aguirre A. (Imagemaker): versi�n inicial.</li>
     * <li>1.3 22/05/2012, Victor Agurto (Imagemaker)): cambian niveles de logs en try-catch.</li>
     * <li>2.0 22/05/2013, Pablo Sanhueza(TINet): Se elimina llamada al m�todo que valida el token del
     * cliente. Adem�s se mejora manejo de excepciones</li>
     * <li>3.0 29/10/2014, V�ctor Ortiz (BCI): Se modifica metodo para que clientes 
	 * que olviden su clave o se encuentre expirada puedan cambiar su clave telefonica, 
	 * adem�s se normaliza log seg�n la normativa log4j vigente</li>
	 * <li>3.1 23/12/2014 Eduardo Villagr�n Morales (Imagemaker): se mejora log.</li> 
     * </ul>
     * <p>
     * 
     * @param rut Rut del cliente.
     * @param dv  D�gito verificador del cliente.
     * @param canal  Canal por el cual se realiza la autenticaci�n.
     * @param codigoMultipass La clave multipass ingresada por el cliente.
     * @param clave La clave telef�nica ingresada por el cliente.
     * @return boolean que indica si cliente posee o no token.
     * @throws GeneralException al prodocirse un error en el desbloqueo.
     * @since 1.0
     */
    private boolean desbloquearClave(long rut, char dv, String canal, String codigoMultipass, String clave) 
            throws GeneralException {
        if (getLogger().isInfoEnabled()) {
            getLogger().info("[desbloquearClave] [" + rut + "] [BCI_INI] canal=<" + canal + ">");
		}
        boolean respuesta = false;
        String claveTemporal = "000000";
        try {        
            ServiciosSeguridad serviciosSeguiridad = this.getServiciosSeguridad();
            if (getLogger().isDebugEnabled()) {
                getLogger().debug("[desbloquearClave] [" + rut + "] serviciosSeguridad=<" 
                        + serviciosSeguiridad + ">");
            }

            Date fechaInicio = new Date();
            Calendar calendario = Calendar.getInstance();
            calendario.setTime(fechaInicio);
            calendario.add(Calendar.MONTH, MESES_VENCIMIENTO);
            if (getLogger().isDebugEnabled()) {
                getLogger().debug("[desbloquearClave] [" + rut + "] fechaInicio=<" + fechaInicio 
                        + ">, calendario=<" + calendario + ">");
			}
            EstadoPin estadoPin = serviciosSeguiridad.estadoPin(CLAVE_TELEFONICA, rut, dv, CLAVE_GRUPO_CANAL);
            if (getLogger().isDebugEnabled()) {
                getLogger().debug("[desbloquearClave] [" + rut + "] estadoPin=<" + estadoPin + ">");
            }
            if (estadoPin != null) {
            	if ((estadoPin.estado.equals(ESTADO_CLAVE_BLOQUEADA)
						|| estadoPin.estado.equals(ESTADO_CLAVE_ACTIVA)
						|| estadoPin.estado.equals(ESTADO_CLAVE_RESTRINGIDA))
						&&!estadoPin.motivo.equals(MOTIVO_DECISION_BCI)) {
            	    if (getLogger().isDebugEnabled()) {
                        getLogger().debug("[desbloquearClave] [" + rut + "] se proceder� al desbloqueo de clave");
					}					
                    this.reseteaClave(CLAVE_GRUPO_CANAL, rut, dv, "04", "", claveTemporal);
                    if (getLogger().isDebugEnabled()) {
                        getLogger().debug("[desbloquearClave] [" + rut + "] luego del desbloqueo");
					}	
                    serviciosSeguiridad.cambiaPinCorporativo(CANAL_CLAVE_TELEFONICA, rut, dv, 
                            claveTemporal,clave);
                    if (getLogger().isDebugEnabled()) {
                        getLogger().debug("[desbloquearClave] [" + rut + "] luego del cambio de clave");
					}
                } 
                else {
                	if(estadoPin.motivo.equals(MOTIVO_DECISION_BCI)){
                	    if (getLogger().isEnabledFor(Level.WARN)) {
                            getLogger().warn("[desbloquearClave] [" + rut + "] estado=<" + estadoPin.estado 
                                    + ">, motivo=<" + estadoPin.motivo + ">");
                        }
                		if (getLogger().isEnabledFor(Level.ERROR)) {
                            getLogger().error("[desbloquearClave] [" + rut 
                                    + "] [BCI_FINEX] La clave no cumple condiciones para ser cambiada. "
                                    + "Lanzo nueva GeneralException.");
                        }
                        throw new GeneralException(ERROR_RESTRICCION_CLAVE);                		
                	}
                	else{
                	    if (getLogger().isEnabledFor(Level.ERROR)) {
                            getLogger().error("[desbloquearClave] [" + rut 
                                    + "] [BCI_FINEX] La clave no est� bloqueada. Lanzo nueva GeneralException.");
                        }
                         throw new GeneralException(ERROR_CLAVE_NO_BLOQUEADA);
                	}
                	
                }
            } 
            else {
                if (getLogger().isEnabledFor(Level.ERROR)) {
                    getLogger().error("[desbloquearClave] [" + rut 
                            + "] [BCI_FINEX] No se pudo obtener estado de clave. Lanzo nueva GeneralException.");
                }
                throw new GeneralException(ERROR_VERIFICANDO_ESTADO_CLAVE);
            }

            if (getLogger().isDebugEnabled()) {
                getLogger().debug("[desbloquearClave] [" + rut + "] se llama al journal");
			}
            this.journalizarEvento(rut, dv, canal, JOURNAL_PRODUCTO, JOURNAL_EVENTO_DESBLOQUEA_CLAVE, 
                    JOURNAL_SUBEVENTO, JOURNAL_ESTADOEVENTO);
            if (getLogger().isDebugEnabled()) {
                getLogger().debug("[desbloquearClave] [" + rut + "] journal OK");
            }
            respuesta = true;
        }
        catch (GeneralException e) {
            if (getLogger().isEnabledFor(Level.WARN)) {
                getLogger().warn("[desbloquearClave] [" + rut 
                        + "] [GeneralException] Error al desbloquear la clave" + e.getMessage(), e);
            }
			if (logger.isDebugEnabled()) {
				logger.debug("[desbloquearClave] [" + rut + "] Llamada al Journalizador");
			}
            this.journalizarEvento(rut, dv, canal, JOURNAL_PRODUCTO, JOURNAL_EVENTO_DESBLOQUEA_CLAVE, 
                    JOURNAL_SUBEVENTO, ESTADOEVENTOJOURNAL);
            if (getLogger().isEnabledFor(Level.ERROR)) {
                getLogger().error("[desbloquearClave] [" + rut 
                        + "] [GeneralException] [BCI_FINEX] relanzo exception. " + e.getMessage(), e);
            }
            throw e;
        } 
        catch (Exception e) {
            if (getLogger().isEnabledFor(Level.WARN)) {
                getLogger().warn("[desbloquearClave] [" + rut + "] [Exception] Error al desbloquear clave. "
                        + e.getMessage(), e);
            }
			if (logger.isDebugEnabled()) {
				logger.debug("[desbloquearClave] [" + rut + "] Llamada al Journalizador");
			}
            this.journalizarEvento(rut, dv, canal, JOURNAL_PRODUCTO, JOURNAL_EVENTO_DESBLOQUEA_CLAVE, 
                    JOURNAL_SUBEVENTO, ESTADOEVENTOJOURNAL);
            if (getLogger().isEnabledFor(Level.ERROR)) {
                getLogger().error("[desbloquearClave] [" + rut 
                        + "] [Exception] [BCI_FINEX] Lanzo nueva GenerealException. " + e.getMessage(), e);
            }
            throw new GeneralException(ERROR_DESBLOQUEAR_CLAVE);
        }
        if (getLogger().isInfoEnabled()) {
            getLogger().info("[desbloquearClave] [" + rut + "] [BCI_FINOK] respuesta=<" + respuesta + ">");
		}
        return respuesta;

    }


    /**
     * <p>M�todo <i>Action</i> que permite desbloquear la clave telef�nica.</p>
     * <p>
     * Registro de versiones:<ul>
     * <li>1.0  10/01/2012, Javier Aguirre. (Imagemaker): versi�n inicial.</li>
     * <li>1.2  10/04/2012, Javier Aguirre. (ImageMaker): corrije la obtenci�n del canal a PinID
     * <li>1.3 22/05/2012, Victor Agurto (Imagemaker)): cambian niveles de logs en try-catch.</li>
     * <li>1.4 22/05/2013, Pablo Sanhueza (TINet): Se agrega validaci�n de segunda clave por medio del
     * atributo {@link #segundaClave} y se consideran modificaciones en el mensaje de error dependiendo
     * de qu� tipo de error ocurri�.</li>
     * <li>1.5 23/12/2014 Eduardo Villagr�n Morales (Imagemaker): se normaliza log.</li> 
     * </li>
     * </ul></p>
     * 
     * @return el <i>outcome</i> a la siguiente p�gina
     */
    public String desbloquearClaveTelefonica() {
        long rut = clienteMB.getRut();
        if (getLogger().isInfoEnabled()) {
            getLogger().info("[desbloquearClaveTelefonica] [" + rut + "] [BCI_INI]");
        }
        char dv = clienteMB.getDigitoVerif();
        String canal = sesionMB.getCanalTO().getPinID();
        if (getLogger().isDebugEnabled()) {
            getLogger().debug("[desbloquearClaveTelefonica] [" + rut + "] canal=<" + canal + ">");
        }
        try {
            this.validacionInicial();
            if (getLogger().isDebugEnabled()) {
                getLogger().debug("[desbloquearClaveTelefonica] [" + rut + "] luego de validacionInicial");
            }
            if (!sesionMB.isActiva()) {
                if (getLogger().isEnabledFor(Level.WARN)) {
                    getLogger().warn("[desbloquearClaveTelefonica] [" + rut 
                            + "] [BCI_FINEX] no hay sesi�n activa.Lanzo nueva GeneralException.");
            }
                throw new GeneralException(TablaValores.getValor(ARCH_PARAM_ERROR, "8366", "Desc"));
            }
            if (segundaClave != null && segundaClave.verificarAutenticacion()) {
                if (getLogger().isDebugEnabled()) {
                    getLogger().debug("[desbloquearClaveTelefonica] [" + rut + "] autenticaci�n OK");
                }
                eventos = new Eventos();
                String claveEdit = StringUtil.completaPorLaDerecha(nuevaClaveTelefonica, LARGO_CLAVE, ' '); 
                this.desbloquearClave(rut, dv, canal, claveMultipass,claveEdit);
                if (getLogger().isDebugEnabled()) {
                    getLogger().debug("[desbloquearClaveTelefonica] [" + rut 
                            + "] luego del desbloqueo. Redirecciono a p�gina de comprobante");
                }
                this.setErrorGeneral(false);
                if (getLogger().isInfoEnabled()) {
                    getLogger().info("[desbloquearClaveTelefonica] [" + rut + "] [BCI_FINOK]");
                }
                return DesbloqueoClaveTelefonicaBackingMB.COMPROBANTE;
            }
            else {
                if (getLogger().isEnabledFor(Level.ERROR)) {
                    getLogger().error("[desbloquearClaveTelefonica] [" + rut 
                            + "] autenticaci�n incorrecta. Lanzo nueva SeguridadException (SAFESIGNER01).");
                }
                throw new SeguridadException("SAFESIGNER01");
            }
        }
        catch (GeneralException e) {
            if (getLogger().isEnabledFor(Level.WARN)) {
                getLogger().warn("[desbloquearClaveTelefonica] [" + rut 
                        + "] [GeneralException] Error al generar clave" + e.getMessage(), e);
            }
            this.setErrorGeneral(true);
            this.setMensajeError(null);
            String codigoError = e.getCodigo();
            ResourceBundle mensajes = PropertyResourceBundle.getBundle(RECURSO_MENSAJE);
            if (getLogger().isDebugEnabled()) {
                getLogger().debug("[desbloquearClaveTelefonica] [" + rut + "] mensajes=<" + mensajes + ">");
            }
            if (codigoError.startsWith("SAFESIGNER") && !StringUtil.esVacio(e.getInfoAdic())) {
                this.setMensajeError(e.getInfoAdic());
            }
            else if (codigoError.equals(ERROR_CLAVE_NO_BLOQUEADA)) {               
                this.setMensajeError(mensajes.getString("mensajeClaveTelefonicaNoBloqueada"));
            }else if (codigoError.equals(ERROR_RESTRICCION_CLAVE)) {               
                this.setMensajeError(mensajes.getString("mensajeClaveTelefonicaConRestriccion"));
            }
        }
        catch (Exception e) {
            if (getLogger().isEnabledFor(Level.WARN)) {
                getLogger().warn("[desbloquearClaveTelefonica] [" + rut + "] [Exception] Error al general clave. "
                        + e.getMessage(), e);
            }
            this.setErrorGeneral(true);
        }
        if (getLogger().isEnabledFor(Level.ERROR)) {
            getLogger().error("[desbloquearClaveTelefonica] [" + rut + "] [BCI_FINEX]");
        }
        return DesbloqueoClaveTelefonicaBackingMB.COMPROBANTE;
    }

    /**
     * <p> Valida que el valor ingresado como clave nueva coincida
     *  con el valor ingresado como clave de confirmaci�n. </p>
     *  
     * Registro de versiones:<ul>
     * <li>1.0 (20/01/2012 Javier Aguirre A. (Imagemaker IT)): versi�n inicial.</li>
     * <li>1.1 23/12/2014 Eduardo Villagr�n Morales (Imagemaker): se normalizan logs.</li>  
     * <li>1.2 05/09/2017  Marcelo Fuentes (SEnTRA) - Ricardo Carrasco (Ing. Soft. BCI): Se setea indicador que 
     * permite determinar si la clave telefonica ingresada por el cliente es valida.</li>
     * </ul>
     *  
     * @param contexto contiene informaci�n del <i>request</i>
     * @param componente referencia del <i>tag</i> jsf
     * @param valor contenido del campo de texto
     * @throws ValidatorException cuando no se cumple la validaci�n
     */
    public void validate(FacesContext contexto, UIComponent componente, Object valor) throws ValidatorException {
        long rut = clienteMB.getRut();
        if (getLogger().isInfoEnabled()) {
            getLogger().info("[validate] [" + rut + "] [BCI_INI] contexto=<" + contexto + ">, componente=<"
                    + componente + ">, valor=<" + valor + ">");
        }
        this.claveTelValidada = false;
        String claveNueva = (String) valor; 
        UIInput confirmComponent = (UIInput) componente.getAttributes().get("pass2");
        String claveConfirma = (String) confirmComponent.getSubmittedValue();
        if (claveNueva == null || claveNueva.isEmpty() || claveConfirma == null || claveConfirma.isEmpty()) {
            if (getLogger().isDebugEnabled()) {
                getLogger().debug("[validate] [" + rut + "] [BCI_FINOK] nada que validar");
            }
            return; // Let required="true" do its job.
        }
        if (!claveNueva.equals(claveConfirma)) {
            confirmComponent.setValid(false);
            if (getLogger().isDebugEnabled()) {
                getLogger().debug("[validate] [" + rut 
                        + "] [BCI_FINEX] Claves no coinciden. Lanzo ValidatorException.");
        }
            throw new ValidatorException(new FacesMessage(MSJE_ERROR_VALIDACION1));
        }
        if (claveNueva.length() < LARGO_MINIMO || claveConfirma.length() < LARGO_MINIMO) {
            confirmComponent.setValid(false);
            if (getLogger().isEnabledFor(Level.ERROR)) {
                getLogger().error("[validate] [" + rut + "] No cumple largo m�nimo de " + LARGO_MINIMO
                        + ". Lanzo ValidatorException.");
        }
            throw new ValidatorException(new FacesMessage(MSJE_ERROR_VALIDACION2));
        }
        
        this.claveTelValidada = true;
        this.nuevaClaveTelefonica = claveNueva;
        if (getLogger().isInfoEnabled()) {
            getLogger().info("[validate] [" + rut + "] [BCI_FINOK]");
        }
    }

    /**
     * <p>M�todo para obtener instancia de ejb de ServiciosSeguridad.</p>
     * <br>
     * Registro de versiones:<ul>
     * <li>1.0 18/11/2011 Javier Aguirre A.(Imagemaker IT): versi�n inicial.</li>
     * <li>1.1 22/05/2013, Pablo Sanhueza(TINet): Se corrige instanciaci�n del home asociado al EJB.</li>
     * <li>1.2 23/12/2014 Eduardo Villagr�n Morales (Imagemaker): se normaliza log. </li> 
     * </ul>
     * @return instancia del ejb
     * @throws Exception en caso de error de la creacion del servicio
     * @since 1.0
     */
    private ServiciosSeguridad getServiciosSeguridad() throws Exception {
        long rut = clienteMB.getRut();
        if (getLogger().isInfoEnabled()) {
            getLogger().info("[getServiciosSeguridad] [" + rut + "] [BCI_INI]");
        }
        if (serviciosSeguridad == null)     {
            if (getLogger().isDebugEnabled()) {
                getLogger().debug("[getServiciosSeguridad] [" + rut + "] servicioSeguridad es null");
            }
            try {
                Class<ServiciosSeguridadHome> homeClass = ServiciosSeguridadHome.class;
                EnhancedServiceLocator locator = EnhancedServiceLocator.getInstance();
                ServiciosSeguridadHome serviciosSeguridadHome = (ServiciosSeguridadHome) locator.getGenericService(
                        "wcorp.serv.seguridad.ServiciosSeguridad", homeClass);
                serviciosSeguridad = serviciosSeguridadHome.create();
                if (getLogger().isDebugEnabled()) {
                    getLogger().debug("[getServiciosSeguridad] [" + rut + "] servicio instanciado");
                }
            } 
            catch (Exception ex) {
                if (getLogger().isEnabledFor(Level.ERROR)) {
                    getLogger().error("[getServiciosSeguridad] [" + rut 
                            + "] [Exception] [BCI_FINEX] Lanzo nueva Exception. " + ex.getMessage(), ex);
                }
                throw new Exception( "Error al crear instancia de ejb ServiciosSeguridad", ex);
            }
        } 
        if (getLogger().isInfoEnabled()) {
            getLogger().info("[getServiciosSeguridad] [" + rut + "] [BCI_FINOK] retorno instancia");
        }
            return serviciosSeguridad;
    }
    
    /**
     * <p>M�todo para obtener instancia de ejb de ServiciosTarjetas.</p>
     * <br>
     * Registro de versiones:<ul>
     * <li>1.0 18/11/2011 Javier Aguirre A.(Imagemaker IT): versi�n inicial.</li>
     * <li>1.1 22/05/2013, Pablo Sanhueza(TINet): Se corrige instanciaci�n del home asociado al EJB.</li>
     * <li>1.2 23/12/2014 Eduardo Villagr�n Morales (Imagemaker): normalizaci�n de logs.</li> 
     * </ul>
     * @return instancia del ejb
     * @throws Exception en caso de error de la creacion del servicio
     * @since 1.0
     */
    public ServiciosTarjetas getServiciosTarjeta() throws Exception {
        long rut = clienteMB.getRut();
        if (getLogger().isInfoEnabled()) {
            getLogger().info("[getServiciosTarjeta] [" + rut + "] [BCI_INI]");
        }
        if (serviciosTarjetas == null) {
            if (getLogger().isDebugEnabled()) {
                getLogger().debug("[getServiciosTarjeta] [" + rut + "] serviciosTarjetas es null");
            }
            try {
                Class<ServiciosTarjetasHome> homeClass = ServiciosTarjetasHome.class;
                EnhancedServiceLocator locator = EnhancedServiceLocator.getInstance();
                ServiciosTarjetasHome serviciosTarjetasHome = (ServiciosTarjetasHome) locator.getGenericService(
                        "wcorp.serv.tarjetas.ServiciosTarjetas", homeClass);
                serviciosTarjetas = serviciosTarjetasHome.create();
                if (getLogger().isDebugEnabled()) {
                    getLogger().debug("[getServiciosTarjeta] [" + rut + "] servicio instanciado");
                }
            }
            catch (Exception ex) {
                if (getLogger().isEnabledFor(Level.ERROR)) {
                    getLogger().error("[getServiciosTarjeta] [" + rut 
                            + "] [Exception] [BCI_FINEX] Lanzo nueva Exception. "
                            + ex.getMessage(), ex);
                }
                throw new Exception("Error al crear instancia de ejb ServiciosTarjetas",ex);
            }
        }
        if (getLogger().isInfoEnabled()) {
            getLogger().info("[getServiciosTarjeta] [" + rut + "] [BCI_FINOK] retorno instancia");
        }
            return serviciosTarjetas;
    }
    
    /**
     * <p>M�todo para obtener instancia de ejb de ServiciosCliente.</p>
     * <br>
     * Registro de versiones:<ul>
     * <li>1.0 18/11/2011 Javier Aguirre A.(Imagemaker IT): versi�n inicial.</li>
     * <li>1.1 22/05/2013, Pablo Sanhueza(TINet): Se corrige instanciaci�n del home asociado al EJB.</li>
     * <li>1.2 23/12/2014 Eduardo Villagr�n Morales (Imagemaker): normalizaci�n de logs 
     * </ul>
     * @return instancia del ejb
     * @throws Exception en caso de error de la creacion del servicio
     * @since 1.0
     */
    private ServiciosCliente getServiciosCliente() throws Exception {
        long rut = clienteMB.getRut();
        if (getLogger().isInfoEnabled()) {
            getLogger().info("[getServiciosCliente] [" + rut + "] [BCI_INI]");
        }
        if (serviciosCliente == null) {
            if (getLogger().isDebugEnabled()) {
                getLogger().debug("[getServiciosCliente] [" + rut + "] serviciosCliente es null");
            }
            try {
                Class<ServiciosClienteHome> homeClass = ServiciosClienteHome.class;
                EnhancedServiceLocator locator = EnhancedServiceLocator.getInstance();
                ServiciosClienteHome serviciosClienteHome = (ServiciosClienteHome) locator.getGenericService(
                        "wcorp.serv.clientes.ServiciosCliente", homeClass);
                serviciosCliente = serviciosClienteHome.create();
                if (getLogger().isDebugEnabled()) {
                    getLogger().debug("[getServiciosCliente] [" + rut + "] servicio instanciado");
                }
            } 
            catch (Exception ex) {
                if (getLogger().isEnabledFor(Level.ERROR)) {
                    getLogger().error("[getServiciosCliente] [" + rut 
                            + "] [Exception] [BCI_FINEX] Lanzo nueva Exception. " + ex.getMessage(), ex);
                }
                throw new Exception("Error al crear instancia de ejb ServiciosCliente",ex);
            }
        }
        if (getLogger().isInfoEnabled()) {
            getLogger().info("[getServiciosCliente] [" + rut + "] [BCI_FINOK] retorno instancia.");
        }
            return serviciosCliente;
    }
    
    /**
     * <p>M�todo para obtener instancia de ejb TCreditoOnline.</p>
     * <br>
     * Registro de versiones:<ul>
     * <li>1.0 18/11/2011 Javier Aguirre A.(Imagemaker IT): versi�n inicial.</li>
     * <li>1.1 22/05/2013, Pablo Sanhueza(TINet): Se corrige instanciaci�n del home asociado al EJB.</li>
     * <li>1.2 23/12/2014 Eduardo Villagr�n Morales (Imagemaker): se normaliza log</li> 
     * </ul>
     * @return instancia del ejb
     * @throws Exception en caso de error de la creacion del servicio
     * @since 1.0
     */
    private TCreditoOnline getTCreditoOnline() throws Exception    {
        long rut = clienteMB.getRut();
        if (getLogger().isInfoEnabled()) {
            getLogger().info("[getTCreditoOnline] [" + rut + "] [BCI_INI]");
        }
        if (tCreditoOnline == null) {
            if (getLogger().isDebugEnabled()) {
                getLogger().debug("[getTCreditoOnline] [" + rut + "] tCreditoOnline es null");
            }
            try {
                Class<TCreditoOnlineHome> homeClass = TCreditoOnlineHome.class;
                EnhancedServiceLocator locator = EnhancedServiceLocator.getInstance();
                TCreditoOnlineHome tCreditoOnlineHome = (TCreditoOnlineHome) locator.getGenericService(
                        "wcorp.bprocess.tcreditoonline.TCreditoOnline", homeClass);
                tCreditoOnline = tCreditoOnlineHome.create();
                if (getLogger().isDebugEnabled()) {
                    getLogger().debug("[getTCreditoOnline] [" + rut + "] servicio instanciado");
                }
            } 
            catch (Exception ex) {
                if (getLogger().isEnabledFor(Level.ERROR)) {
                    getLogger().error("[getTCreditoOnline] [" + rut 
                            + "] [Exception] [BCI_FINEX] Lanzo nueva Exception. " + ex.getMessage(), ex);
                }
                throw new Exception("Error al crear instancia de ejb TCreditoOnline",ex);
            }
        }
        if (getLogger().isInfoEnabled()) {
            getLogger().info("[getTCreditoOnline] [" + rut + "] [BCI_FINOK] retorno instancia");
        }
            return tCreditoOnline;
    }

    /**
     * Retorna la fecha de proceso formateada para efectos de determinaci�n de segunda clave.
     * <p>
     * Registro de versiones:<ul>
     * 
     * <li>1.0 22/05/2013 Pablo Sanhueza (TINet): Versi�n inicial.
     * </ul><p>
     * @return Fecha actual con formato {@link #FORMATO_FECHA}
     * @since 2.0
     */
    public String getFechaActualFormateada() {
        return FechasUtil.convierteDateAString(new Date(), FORMATO_FECHA);
    }
    
    /**
     * Retorna la instancia Logger.
     * 
     * <p>Registro de versiones:<ul>
     * <li>1.0 23/12/2014 Eduardo Villagr�n Morales (Imagemaker): versi�n inicial. 
     * </ul>
     * 
     * @return el logger de la clase.
     * @since 2.3
     */
    private Logger getLogger(){
        if (logger==null){
            logger = Logger.getLogger(DesbloqueoClaveTelefonicaBackingMB.class);
        }
        return logger;
    }
    
    public boolean isClaveTelValidada() {
        return claveTelValidada;
    }
    
    public void setClaveTelValidada(boolean claveTelValidada) {
        this.claveTelValidada = claveTelValidada;
    }
    
}