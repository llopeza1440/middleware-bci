package wcorp.model.seguridad.to;

import java.io.Serializable;
import java.util.Date;


/**
 * TokenEntrustTO
 * <br>
 * Clase transportadora que representa la informacion de un token entrust, ya sea hard o softoken. 
 * Y tambi�n representa la informaci�n de la tabla TANDEM TBPSC02. Adem�s, representa la salida
 * del servicio Entrust IDG UserTokenList.
 * 
 * <br>
 * Registro de versiones:
 * 
 * <ul>
 *   <li>1.0 10/07/2017 Jaime Suazo (Sentra) - Ricardo Carrasco (Ing. Soft. BCI) : versi�n inicial.</li>
 *   <li>2.0 20/10/2017 Jaime Suazo (Sentra) - Ricardo Carrasco (Ing. Soft. BCI) : Se agregan los siguientes atributos a la clase
 *           {@link #estadoActivacion}, {@link #plataformaMovil}, {@link #fechaDeRegistro}. Se agregan los respectivos getters, setters 
 *           y se actualiza el m�todo {@link #toString()}.</li>
 * </ul>
 * <p>
 * <b>Todos los derechos reservados por Banco de Cr�dito e Inversiones.</b>
 * </p>
 */
public class TokenEntrustTO implements Serializable {

	/**
	 * numero serial de la clase. 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * codigo respuesta WS entrust.
	 */
	private int codigoRespuestaWs = 0;
	
	/**
	 * descripcion respuesta WS entrust.
	 */
	private String descripcionRespuestaWs = null;
	
	/**
	 * estado token - softoken. Importante, para entrust, dicho estado debe venir con mayusculas.
	 * ej: CURRENT, PENDING, HOLD, CANCEL. 
	 */
	private String estado = null;
		
	/**
	 * numero serial token - softoken.
	 */
	private String serialNumber = null;
	
	/**
	 * Atributo relacionado a campo PSC_FEC_MOD de tabla TANDEM.
	 */
	private Date fechaModificacion;
	
	/**
	 * tipo de dispositivo token - softoken. (HTK, SFT).
	 */
	private String tipoDispositivo = null;
	
	/**
	 * nombre dispositivo softoken.
	 */
	private String nombreDispositivo = null;
	
	/**
	 * Grupo del token.
	 */
	private String grupoToken = null;

	/**
	 * Estado de activaci�n.
	 */
	private String estadoActivacion = null;
	
	/**
	 * nombre de la plataforma m�vil.
	 */
	private String plataformaMovil = null;

	/**
	 * Fecha de registro del dispositivo.
	 */
	private Date fechaDeRegistro = null;

	public int getCodigoRespuestaWs() {
		return codigoRespuestaWs;
	}

	public void setCodigoRespuestaWs(int codigoRespuestaWs) {
		this.codigoRespuestaWs = codigoRespuestaWs;
	}

	public String getDescripcionRespuestaWs() {
		return descripcionRespuestaWs;
	}

	public void setDescripcionRespuestaWs(String descripcionRespuestaWs) {
		this.descripcionRespuestaWs = descripcionRespuestaWs;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

	public String getTipoDispositivo() {
		return tipoDispositivo;
	}

	public void setTipoDispositivo(String tipoDispositivo) {
		this.tipoDispositivo = tipoDispositivo;
	}
	
	public Date getFechaModificacion() {
		return fechaModificacion;
	}

	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	public String getNombreDispositivo() {
		return nombreDispositivo;
	}

	public void setNombreDispositivo(String nombreDispositivo) {
		this.nombreDispositivo = nombreDispositivo;
	}

	public String getGrupoToken() {
		return grupoToken;
	}

	public void setGrupoToken(String grupoToken) {
		this.grupoToken = grupoToken;
	}

	public String getEstadoActivacion() {
		return estadoActivacion;
	}

	public void setEstadoActivacion(String estadoActivacion) {
		this.estadoActivacion = estadoActivacion;
	}

	public String getPlataformaMovil() {
		return plataformaMovil;
	}

	public void setPlataformaMovil(String plataformaMovil) {
		this.plataformaMovil = plataformaMovil;
	}

	public Date getFechaDeRegistro() {
		return fechaDeRegistro;
	}

	public void setFechaDeRegistro(Date fechaDeRegistro) {
		this.fechaDeRegistro = fechaDeRegistro;
	}
	
	public String toString() {
		StringBuffer builder = new StringBuffer();
		builder.append("TokenEntrustTO [codigoRespuestaWs=");
		builder.append(codigoRespuestaWs);
		builder.append(", descripcionRespuestaWs=");
		builder.append(descripcionRespuestaWs);
		builder.append(", estado=");
		builder.append(estado);
		builder.append(", serialNumber=");
		builder.append(serialNumber);
		builder.append(", fechaModificacion=");
		builder.append(fechaModificacion);
		builder.append(", tipoDispositivo=");
		builder.append(tipoDispositivo);
		builder.append(", nombreDispositivo=");
		builder.append(nombreDispositivo);
		builder.append(", grupoToken=");
		builder.append(grupoToken);		
		builder.append(", estadoActivacion=");
		builder.append(estadoActivacion);
		builder.append(", plataformaMovil=");
		builder.append(plataformaMovil);
		builder.append(", fechaDeRegistro=");
		builder.append(fechaDeRegistro);
		builder.append("]");
		return builder.toString();
	}
}
