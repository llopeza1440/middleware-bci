package wcorp.serv.seguridad.dao;

import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

import javax.naming.NamingException;
import javax.xml.rpc.soap.SOAPFaultException;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import wcorp.infraestructura.seguridad.autenticacion.util.UsuarioFuseEntrustUyP;
import wcorp.model.seguridad.EstadoSegundaClaveTO;
import wcorp.model.seguridad.LlaveSegundaClaveTO;
import wcorp.model.seguridad.ParametrosEstrategiaSegundaClaveTO;
import wcorp.model.seguridad.RepresentacionNumericaSegundaClaveTO;
import wcorp.model.seguridad.RepresentacionSegundaClave;
import wcorp.model.seguridad.ResultadoOperacionSegundaClaveTO;
import wcorp.model.seguridad.UsuarioSegundaClaveTO;
import wcorp.model.seguridad.to.EstadoActivacionTokenTO;
import wcorp.model.seguridad.to.OtpEntrustTO;
import wcorp.model.seguridad.to.ResponseStatusTO;
import wcorp.model.seguridad.to.TokenEntrustTO;
import wcorp.util.FechasUtil;
import wcorp.util.GeneralException;
import wcorp.util.RUTUtil;
import wcorp.util.StringUtil;
import wcorp.util.TablaValores;
import wcorp.util.com.JOLTPoolConnection;
import wcorp.util.com.TuxedoException;

import ws.bci.cl.bci.esb.service.seguridad.administracion.entrust.wscliente.ServicioAdministracionBCI;
import ws.bci.cl.bci.esb.service.seguridad.administracion.entrust.wscliente.ServicioAdministracionBCI_Impl;
import ws.bci.cl.bci.esb.service.seguridad.administracion.entrust.wscliente.ServicioAdministracionPT;
import ws.bci.cl.bci.esb.service.seguridad.administracion.entrust.wscliente.ServicioAdministracionPT_Stub;
import ws.bci.cl.bci.esb.service.seguridad.autenticacion.segundaclave.wscliente.ServicioAutenticacionBCI;
import ws.bci.cl.bci.esb.service.seguridad.autenticacion.segundaclave.wscliente.ServicioAutenticacionBCI_Impl;
import ws.bci.cl.bci.esb.service.seguridad.autenticacion.segundaclave.wscliente.ServicioAutenticacionPT;
import ws.bci.cl.bci.esb.service.seguridad.autenticacion.segundaclave.wscliente.ServicioAutenticacionPT_Stub;

import bea.jolt.pool.DataSet;
import bea.jolt.pool.Result;
import bea.jolt.pool.servlet.ServletSessionPool;

import cl.bci.www.esb.service.seguridad.administracion.entrust.types.AuthorizationPolicy;
import cl.bci.www.esb.service.seguridad.administracion.entrust.types.UserTokenActivationRequest;
import cl.bci.www.esb.service.seguridad.administracion.entrust.types.UserTokenActivationResponse;
import cl.bci.www.esb.service.seguridad.administracion.entrust.types.UserTokenListRequest;
import cl.bci.www.esb.service.seguridad.administracion.entrust.types.UserTokenListResponse;
import cl.bci.www.esb.service.seguridad.administracion.entrust.types.UserTokenUpdateRequest;
import cl.bci.www.esb.service.seguridad.administracion.entrust.types.UserTokenUpdateResponse;
import cl.bci.www.esb.service.seguridad.autenticacion.segundaclave.types.AuthenticateGenericChallengeCallParms;
import cl.bci.www.esb.service.seguridad.autenticacion.segundaclave.types.GenericAuthenticateParms;
import cl.bci.www.esb.service.seguridad.autenticacion.segundaclave.types.GenericAuthenticateResponse;
import cl.bci.www.esb.service.seguridad.autenticacion.segundaclave.types.GenericChallenge;
import cl.bci.www.esb.service.seguridad.autenticacion.segundaclave.types.GenericChallengeParms;
import cl.bci.www.esb.service.seguridad.autenticacion.segundaclave.types.GetGenericChallengeCallParms;
import cl.bci.www.esb.service.seguridad.autenticacion.segundaclave.types.GetOTPChallengeRequest;
import cl.bci.www.esb.service.seguridad.autenticacion.segundaclave.types.GetOTPChallengeResponse;
import cl.bci.www.esb.service.seguridad.autenticacion.segundaclave.types.NameValue;
import cl.bci.www.esb.service.seguridad.autenticacion.segundaclave.types.Response;
import cl.bci.www.esb.service.seguridad.autenticacion.segundaclave.types.TransactionRequest;
import cl.bci.www.esb.service.seguridad.autenticacion.segundaclave.types.TransactionResponse;


/**
 * Clase Dao que contiene los metodos necesarios para el uso de una Segunda Clave.
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 22/07/2016 Felipe Briones M. (SEnTRA) - Victor Ortiz (Ing. Soft. BCI): versi�n inicial.</li>
 * <li>2.0 10/07/2017 Luis L�pez Alamos - Jaime Suazo D�az - Sergio Flores (Sentra) - Ricardo Carrasco C�ceres (Ing. Soft. BCI): 
 *  Se agregan los m�todos {@link #autenticarDesafioGenerico(ParametrosEstrategiaSegundaClaveTO)}, {@link #obtenerDesafioGenerico(ParametrosEstrategiaSegundaClaveTO)}
 *                         {@link #getService()} , {@link #getServiceADM()}, {@link #actualizarTokenUsuario(ParametrosEstrategiaSegundaClaveTO)}, {@link #obtenerDescripcionErrorEntrust(int)}
 *                         {@link #consultarEstadoDesafio(String)}, {@link #listarTokenUsuario(ParametrosEstrategiaSegundaClaveTO)}, {@link #obtenerRutClienteEntrust(ParametrosEstrategiaSegundaClaveTO)}
 *                          {@link #activarSoftTokenUsuario(ParametrosEstrategiaSegundaClaveTO)}, {@link #obtenerOTP(UsuarioSegundaClaveTO)} y {@link #convierteByteArrayAQr(byte[])}}</li>
 * <li>3.0 10/10/2017 Sergio Flores (Sentra) - Ricardo Carrasco C�ceres (Ing. Soft. BCI): Se modifica el m�todo {@link #obtenerOTP(UsuarioSegundaClaveTO)}</li>                          
 *                          
 * <li>4.0 30/10/2017 Jaime Suazo D�az - Ricardo Carrasco C�ceres (Ing. Soft. BCI): Se crean los m�todos         
 *         {@link #obtenerCamposLlaveEntrust(ParametrosEstrategiaSegundaClaveTO, Object)}, {@link #obtenerMapaDeCampos(String)}.
 *         Adem�s, se crean las constantes {@link #CARACTERES_PARA_DESTACAR_CAMPOS_ENTRUST}, {@link #SEPARADOR_CAMPO_TIPO_FORMATO}, {@link #POSICION_ARREGLO_UNO},
 *         {@link #POSICION_ARREGLO_CERO}, {@link #LARGO_ARREGLO_PAR}, {@link #CANTIDAD_DIGITOS_MILES}, {@link #TITULO_CAMPOS_LLAVE_ENTRUST},
 *         {@link #LARGO_CERO}, {@link #SEPARADOR_DE_ETIQUETA_VALOR}, {@link #SUB_TITULO_PUSH_LLAVE_ENTRUST}.<br><br>
 *         
 *         Se modifica el m�todo {@link #listarTokenUsuario(ParametrosEstrategiaSegundaClaveTO)} </li>
 *                 
 * </ul>
 * <p>
 * 
 * <p>
 * <B>Todos los derechos reservados por Banco de Cr�dito e Inversiones.</B>
 */
public class SegundaClaveDAO {
    
/**
	 * Maxima cantidad de campos para la llave por defecto. 
	 */
	private static final int MAXIMA_CANTIDAD_CAMPOS = 8;

	/**
	 * Grupo de caracteres que sirven para destacar campos para entrust.
	 */
	private static final String CARACTERES_PARA_DESTACAR_CAMPOS_ENTRUST = "#B#";

	/**
	 * Caracter separador de campo y formato.
	 */
	private static final String SEPARADOR_CAMPO_TIPO_FORMATO = ",";
	
	/**
	 * Caracter separador de etiqueta.
	 */
	private static final String SEPARADOR_DE_ETIQUETA_VALOR = ":";
	

	/**
	 * Posición uno de arreglo. 
	 */
    private static final int POSICION_ARREGLO_UNO = 1;

	/**
	 * Posición cero de arreglo. 
	 */
	private static final int POSICION_ARREGLO_CERO = 0;

	/**
	 * Largo dos de arreglo. 
	 */
	private static final int LARGO_ARREGLO_PAR = 2;

	/**
     * Constante que define la cantidad de dígitos antes del punto de separación de miles.
     */
    private static final int CANTIDAD_DIGITOS_MILES = 3;
	
   /**
	* Nombre de etiqueta titulo para campos de llave a enviar a entrust.
	*/
    private static final String TITULO_CAMPOS_LLAVE_ENTRUST = "titulo";
     
    /**
  	* Nombre de etiqueta subt�tulo para el push de app entrus kim-it.
  	*/
    private static final String SUB_TITULO_PUSH_LLAVE_ENTRUST = "subtitulo";
     
     
   /**
	* Largo cero de arreglo.
	*/
	private static final int LARGO_CERO = 0;

	/**
	 * codigo de error generico del producto entrust.
	 */
    private static final int CODIGO_ERROR_GENERICO_ENTRUST = 600;

    /**
     * Valor que indica el formato de las fechas que devuelven los servicios.
     */
    private static final String FORMATO_FECHA = "yyyy-mm-dd";
    
    /**
     * Indica el valor por defecto a tomar cuando los codigos de la consulta vienen en blanco.
     */
    private static final String VALOR_DEFECTO_EST_SEG_CLA = "0";
    
    /**
     * tabla de par�metros de entrust.
     */
    private static final String TABLADEPARAMETROS = "entrust.parametros";
    
    /**
     * Constante utilizada para definir descripci�n de valores de llaves desde tablas de parametros. 
     */
    private static final String DESC = "desc";
    
    /**
     * Constante que representa el modo ONLINE de Entrust.
     */
    private static final String MODO_ONLINE = TablaValores.getValor(TABLADEPARAMETROS, "modoOnline", DESC);
    
    /**
     * Constante que representa el modo OFFLINE de Entrust.
     */
    private static final String MODO_OFFLINE = TablaValores.getValor(TABLADEPARAMETROS, "modoOffline", DESC);
    
    /**
     * Constante que representa el modo OFFLINE de Entrust.
     */
    private static final String ENTRUST_FISICO = TablaValores.getValor(TABLADEPARAMETROS, "modoEntrustFisico", DESC);
    
    /**
     * Constante que representa el modo OTP para enrolamiento Entrust.
     */
    private static final String OTP = TablaValores.getValor(TABLADEPARAMETROS, "modoOtp", DESC);
    
    /**
     * Constante que indica un error de tipo Usuario no tiene TOkens Activos.
     */
    private static final int SIN_TOKEN_ACTIVO = 432;
    
    /**
     * Constante que indica la cantidad de caracteres que deben ser utilizados para obtener correctamente un ERROR_CODE.
     */
    private static final int CORTE_PARA_ERROR_CODE = 11;
    
    /**
     * Valor Cero.
     */
    private static final int VALOR_CERO = 0;
    
    /**
     * Valor Uno.
     */
    private static final int VALOR_UNO = 1;
        
    /**
     * Valor Uno.
     */
    private static final int VALOR_MENOS_UNO = -1;
    
    
    /**
     * Pool de conexion para Jolt.
     */
    private JOLTPoolConnection joltPool;
    
    /**
     * Log de la clase.
     */
    private transient Logger logger = (Logger)Logger.getLogger(SegundaClaveDAO.class);
    
    /**
     * Metodo para obtener el log de la clase.
     * 
     * Registro de versiones:
     * <ul>
     * <li>1.0 22/07/2016 Felipe Briones M. (SEnTRA) - Victor Ortiz (Ing. Soft. BCI): versi�n inicial.</li>
     * </ul>
     * 
     * @return Logger log de la clase.
     * @since 1.0
     */
    private Logger getLogger() {
        if (logger == null) {
            logger = Logger.getLogger(getClass());
        }
        return logger;
    }
  
    /**
     * Metodo que consulta el estado de la segunda clave con el servicio Tuxedo "SegConEstSegCla".
     * Registro de versiones:
     * <ul>
     * <li>1.0 22/07/2016 Felipe Briones M. (SEnTRA) - Victor Ortiz (Ing. Soft. BCI): versi�n inicial.</li>
     * </ul>
     * @param rut rut del cliente.
     * @param digitoVerificador digito verificador del cliente.
     * @param codigoTipoProd codigo del tipo de dispositivo.
     * @return EstadoSegundaClaveTO[] arreglo de objetos con los datos del estado de las segundas claves.
     * @throws NamingException en caso de error.
     * @throws TuxedoException en caso de error.
     * @since 1.0
     */
    public EstadoSegundaClaveTO[] consultarEstadoSegundaClave(String rut, String digitoVerificador, String codigoTipoProd) 
            throws NamingException, TuxedoException{
        if(getLogger().isEnabledFor(Level.INFO)){
            getLogger().info("[consultarEstadoSegundaClave][BCI_INI]["+rut+"]");
        }
        EstadoSegundaClaveTO[] resultadosEstadoSegundaClave = null;
        DataSet parametros = new DataSet();
        parametros.setValue("rut", rut);
        parametros.setValue("digitoVerificador", digitoVerificador);
        parametros.setValue("codigoTipoProd", codigoTipoProd);
        
        Result resultado = invocarServicioTuxedo( "SegConEstSegCla", parametros );
       
        if (getLogger().isEnabledFor(Level.DEBUG)){
            getLogger().debug("[consultarEstadoSegundaClave]["+rut+"] invocarServicioTuxedo resultado " +resultado);
        }
 
        if (resultado != null && !resultado.isEmpty()){
            int cantidad = Integer.parseInt( (String) resultado.getValue("cantidadMov", 0, null));
            if (getLogger().isEnabledFor(Level.DEBUG)){
                getLogger().debug("[consultarEstadoSegundaClave]["+rut+"] cantidad " + cantidad);
            }
            resultadosEstadoSegundaClave = new EstadoSegundaClaveTO[cantidad];
            SimpleDateFormat sdf = new SimpleDateFormat(FORMATO_FECHA);
            for (int i = 0; i < cantidad; i++) {
                EstadoSegundaClaveTO estadoSegundaClave = new EstadoSegundaClaveTO();
                String codEstado = ( String ) resultado.getValue("codEstado", i, null);
                estadoSegundaClave.setIdCodigoEstado( Integer.parseInt(codEstado) );
                estadoSegundaClave.setEstatus( estadoSegundaClave.getIdCodigoEstado() == 1 );
                estadoSegundaClave.setTipoSegundaClave( ( String ) resultado.getValue("codigoAuxiliar", i, null) );
                estadoSegundaClave.setSerial(( String ) resultado.getValue("serial", i, null) );
                String fechaInicio = ( String ) resultado.getValue("fechaInicio", i, null);
                estadoSegundaClave.setFechaInicio(FechasUtil.convierteStringADate(fechaInicio, sdf));
                if (resultado.getValue("modo", i, null) != null){
                    estadoSegundaClave.setModo((( String ) resultado.getValue("modo", i, null)).charAt(0) == '1' );
                }
                String claveNuevaStr = ( String ) resultado.getValue("claveNueva", i, "0");
                if (claveNuevaStr == null || "".equals(claveNuevaStr.trim())){
                    claveNuevaStr = VALOR_DEFECTO_EST_SEG_CLA;
                }
                estadoSegundaClave.setClaveNueva(Integer.parseInt(claveNuevaStr));
                String codigoCambioStr = ( String ) resultado.getValue("codigoCambio", i, null);
                if (codigoCambioStr == null || "".equals(codigoCambioStr.trim())){
                    codigoCambioStr = VALOR_DEFECTO_EST_SEG_CLA;
                }
                estadoSegundaClave.setCodigoCambio(Integer.parseInt(codigoCambioStr) );
                String fechaMod = ( String ) resultado.getValue("fechaFin", i, null);
                estadoSegundaClave.setFechaModificacion(FechasUtil.convierteStringADate(fechaMod, sdf));
                estadoSegundaClave.setGlosa(( String ) resultado.getValue("glosaMov", i, null) );
                resultadosEstadoSegundaClave[i] = estadoSegundaClave;
            }
            
        }

        if(getLogger().isEnabledFor(Level.INFO)){
            getLogger().info("[consultarEstadoSegundaClave][BCI_FINOK]["+rut+"] retornando "
                + StringUtil.contenidoDe(resultadosEstadoSegundaClave));
        }
        return resultadosEstadoSegundaClave;
    }
    
    /**
     * Metodo que consulta el estado de la segunda clave con el servicio Tuxedo "SegConEstSegCla".
     * Registro de versiones:
     * <ul>
     * <li>1.0 22/07/2016 Felipe Briones M. (SEnTRA) - Victor Ortiz (Ing. Soft. BCI): versi�n inicial.</li>
     * </ul>
     * @param rut rut del cliente.
     * @param serial serial del dispositivo si es que es necesario.
     * @param codigoTipoProd codigo del tipo de dispositivo.
     * @return String con el codigo de respuesta.
     * @throws NamingException en caso de error.
     * @throws TuxedoException en caso de error.
     * @since 1.0
     */
    public String eliminarRegistroSegundaClave(String rut, String serial, String codigoTipoProd) throws TuxedoException, NamingException{
        if(getLogger().isEnabledFor(Level.INFO)){
            getLogger().info("[eliminarRegistroSegundaClave][BCI_INI]["+rut+"]["+serial+"]["+codigoTipoProd+"]");
        }
        DataSet parametros = new DataSet();
        parametros.setValue("rut", rut);
        parametros.setValue("codigoTipoProd", codigoTipoProd);
        if (serial != null && !"".equals( serial )){
            parametros.setValue("serial", serial);
        }
        Result resultado = invocarServicioTuxedo( "SegEliRegSegCla", parametros );
        String estadoSolicitud = null;
        if (resultado != null){
            estadoSolicitud = (String) resultado.getValue("indicadorStatus", 0, null);
        }
        if(getLogger().isEnabledFor(Level.INFO)){
            getLogger().info("[eliminarRegistroSegundaClave][BCI_FINOK]["+rut+"] retornando estado solicitud "+estadoSolicitud);
        }
        return estadoSolicitud;
    }
    
    /**
     * Metodo que ingresa un nuevo registro de segunda clave para TANDEM.
     * Registro de versiones:
     * <ul>
     * <li>1.0 22/07/2016 Felipe Briones M. (SEnTRA) - Victor Ortiz (Ing. Soft. BCI): versi�n inicial.</li>
     * </ul>
     * @param rut rut del cliente.
     * @param digitoVerificador digito verificador del cliente.
     * @param codigoTipoProd codigo del tipo de dispositivo.
     * @param serial serial del dispositivo.
     * @param estado estado que se registrara.
     * @param fechaInicio fecha de inicio.
     * @param codEstado codigo del estado.
     * @param modo modo del registro.
     * @param claveNueva nueva clave.
     * @param codigoCambio codigo de cambio.
     * @param glosaMov glosa.
     * @return String con el resultado de la ejecucion.
     * @throws TuxedoException en caso de error.
     * @throws NamingException en caso de error.
     * @since 1.0
     */
    public String ingresarRegistroSegundaClave(String rut, String digitoVerificador, String codigoTipoProd, String serial, String estado, String fechaInicio,
            String codEstado, String modo, String claveNueva, String codigoCambio, String glosaMov) throws TuxedoException, NamingException{
        if(getLogger().isEnabledFor(Level.INFO)){
            getLogger().info("[ingresarRegistroSegundaClave][BCI_INI]["+rut+"]["+serial+"]["+codigoTipoProd+"]["+serial+"]["+estado+"]["
                +fechaInicio+"]["+codEstado+"]["+modo+"]["+(claveNueva!=null)+"]["+codigoCambio+"]["+glosaMov+"]");
        }
        DataSet parametros = new DataSet();
        parametros.setValue("rut", rut );
        parametros.setValue("digitoVerificador", digitoVerificador );
        parametros.setValue("codigoTipoProd", codigoTipoProd);
        parametros.setValue("serial", serial);
        parametros.setValue("estado", estado);
        parametros.setValue("fechaInicio", fechaInicio);
        parametros.setValue("codEstado", codEstado);
        parametros.setValue("modo", modo);
        parametros.setValue("claveNueva", claveNueva);
        parametros.setValue("codigoCambio", codigoCambio);
        if (glosaMov != null && !"".equals( glosaMov )){
            parametros.setValue("glosaMov", glosaMov);
        }
        Result resultado = invocarServicioTuxedo( "SegInsRegSegCla", parametros );
        String respuetaObtenida = null;
        if (resultado != null){
            
            respuetaObtenida = (String) resultado.getValue("indicadorStatus", 0, null);
        }
        if(getLogger().isEnabledFor(Level.INFO)){
            getLogger().info("[ingresarRegistroSegundaClave][BCI_FINOK]["+rut+"] respuetaObtenida " + respuetaObtenida);
        }
        return respuetaObtenida;
    }
    
    /**
     * Metodo que modifica un registro especifico de la segunda clave.
     * Registro de versiones:
     * <ul>
     * <li>1.0 22/07/2016 Felipe Briones M. (SEnTRA) - Victor Ortiz (Ing. Soft. BCI): versi�n inicial.</li>
     * </ul>
     * @param rut rut del cliente.
     * @param codigoTipoProd codigo del tipo de dispositivo.
     * @param serial serial del dispositivo si es que es necesario.
     * @param ubicacion ubicacion dentro del registro TANDEM.
     * @param parametro parametro para el registro TANDEM.
     * @return String con el codigo de respuesta.
     * @throws NamingException en caso de error.
     * @throws TuxedoException en caso de error.
     * @since 1.0
     */
    public String modificarRegistroSegundaClave(String rut, String codigoTipoProd, String serial, String ubicacion, String parametro) throws TuxedoException, NamingException{
        if(getLogger().isEnabledFor(Level.INFO)){
            getLogger().info("[modificarRegistroSegundaClave][BCI_INI]["+rut+"]["+serial+"]["+codigoTipoProd+"]["+ubicacion+"]");
        }
        String respuesta = null;
        DataSet parametros = new DataSet();
        parametros.setValue("rut", rut );
        parametros.setValue("codigoTipoProd", codigoTipoProd);
        if (serial != null && !"".equals( serial )){
            parametros.setValue("serial", serial);
        }
        parametros.setValue("ubicacion", ubicacion);
        parametros.setValue("parametro1", parametro);
        Result resultado = invocarServicioTuxedo( "SegModOneSegCla", parametros );
        if (resultado != null){
            respuesta = (String) resultado.getValue("indicadorStatus", 0, null);
        }
        if(getLogger().isEnabledFor(Level.INFO)){
            getLogger().info("[modificarRegistroSegundaClave][BCI_FINOK] resultado ejecucion " + respuesta);
        }
        return respuesta;
    }
    
    /**
     * Metodo que genera la conexion con el pool para invocar Tuxedos..
     * Registro de versiones:
     * <ul>
     * <li>1.0 22/07/2016 Felipe Briones M. (SEnTRA) - Victor Ortiz (Ing. Soft. BCI): versi�n inicial.</li>
     * </ul>
     * @param nombreServicio nombre del servicio tuxedo.
     * @param parametros parametros para el servicio.
     * @return Result objeto con el resultado de la ejecucion del tuxedo.
     * @throws TuxedoException en caso de error.
     * @throws NamingException en caso de error.
     * @since 1.0
     */
    private Result invocarServicioTuxedo(String nombreServicio, DataSet parametros) throws TuxedoException, NamingException{
        if(getLogger().isEnabledFor(Level.INFO)){
            getLogger().info("[invocarServicioTuxedo][BCI_INI] nombreServicio["+nombreServicio+"] parametros["+parametros+"]");
        }
        joltPool = new JOLTPoolConnection();
        ServletSessionPool sesion = joltPool.getSesion(getClass().getName());
        Result resultado =  sesion.call(nombreServicio, parametros,null);
        if(getLogger().isEnabledFor(Level.INFO)){
            getLogger().info("[invocarServicioTuxedo][BCI_FINOK] resultado ejecucion " + resultado);
        }
        return resultado;
    }
    
    /**
     * Metodo que autentica un desafio generico(Challenge)
     * Registro de versiones:
     * <ul>
     * <li>1.0 10/07/2017 Luis L�pez Alamos. (SEnTRA) - Ricardo Carrasco C�ceres (Ing. Soft. BCI): versi�n inicial.</li>
     * </ul>
     * @param parametroEstrategiaSegundaClave datos para la consulta.
     * @return ResultadoOperacionSegundaClaveTO objeto con el resultado de la ejecucion de la consulta.
     * @throws Exception en caso de error.
     * @since 2.0
     */
    public ResultadoOperacionSegundaClaveTO autenticarDesafioGenerico(ParametrosEstrategiaSegundaClaveTO parametroEstrategiaSegundaClave) throws Exception{
        if(getLogger().isEnabledFor(Level.INFO)){
            getLogger().info("[autenticarDesafioGenerico][BCI_INI] [" + parametroEstrategiaSegundaClave + "]");
        }
        ResultadoOperacionSegundaClaveTO resultado = new ResultadoOperacionSegundaClaveTO();
        int estatusNOK = Integer.parseInt(TablaValores.getValor(TABLADEPARAMETROS, "estatusnok", "valor"));
        int estatusOK = Integer.parseInt(TablaValores.getValor(TABLADEPARAMETROS, "estatusok", "valor"));
        String glosa = TablaValores.getValor(TABLADEPARAMETROS, "estatusnok", "glosa");

        resultado.setEstatus(false);
        resultado.setIdCodigoEstado(estatusNOK);
        resultado.setGlosa(glosa);
        
        String codeConfirmation = "";

        if(parametroEstrategiaSegundaClave.getSegundaClave() != null){
        		codeConfirmation = parametroEstrategiaSegundaClave.getSegundaClave().getClaveAlfanumerica();        	
        }
        else{
        	if(parametroEstrategiaSegundaClave.getTipoSegundaClave().equalsIgnoreCase(OTP)){
        		codeConfirmation = parametroEstrategiaSegundaClave.getCodigoConfirmacion();
        }
        }
        
        if(getLogger().isEnabledFor(Level.INFO)){
            getLogger().info("[autenticarDesafioGenerico][codeConfirmation] ["+codeConfirmation+"]");
        }
        
        
        String userID =  obtenerRutClienteEntrust(parametroEstrategiaSegundaClave);
        
        try {
            String tipoAutenticacion = TablaValores.getValor(TABLADEPARAMETROS, "tipoAutenticacion", DESC);
            String modoTransaccion = parametroEstrategiaSegundaClave.getTipoSegundaClave();
            
            ServicioAutenticacionPT postType = this.getService();            
            AuthenticateGenericChallengeCallParms parametros = new AuthenticateGenericChallengeCallParms();
            
            parametros.setUserId(userID);
            
            if (MODO_OFFLINE.equalsIgnoreCase(modoTransaccion) || ENTRUST_FISICO.equalsIgnoreCase(modoTransaccion) || OTP.equalsIgnoreCase(modoTransaccion)){
                Response paramInterno = new Response();
        		String[] parametrosString = new String[VALOR_UNO];
        		parametrosString[VALOR_CERO] = codeConfirmation;
                paramInterno.setResponse(parametrosString);
                parametros.setResponse(paramInterno);
            }
                
            if (OTP.equalsIgnoreCase(modoTransaccion)){
                tipoAutenticacion = modoTransaccion;
            }
            
            GenericAuthenticateParms parms = new GenericAuthenticateParms();
            parms.setAuthenticationType(tipoAutenticacion);
            
        	if (MODO_ONLINE.equalsIgnoreCase(modoTransaccion)) {
                parms.setTransactionId(codeConfirmation);
        	}
        		
        	if (MODO_OFFLINE.equalsIgnoreCase(modoTransaccion) || MODO_ONLINE.equalsIgnoreCase(modoTransaccion)) {
            
                String switchPlantillas = TablaValores.getValor(TABLADEPARAMETROS, "switchPlantillas", DESC);
            
                if (getLogger().isEnabledFor(Level.DEBUG)) {
                    getLogger().debug("[obtenerDesafioGenerico][" + userID + "][switchPlantillas][" + switchPlantillas + "]");
                }

                if (switchPlantillas != null && switchPlantillas.trim().equalsIgnoreCase("OK")) {
                	parms = (GenericAuthenticateParms) obtenerCamposLlaveEntrust(parametroEstrategiaSegundaClave, parms);	
                }
                else {
                int largoArreglo = parametroEstrategiaSegundaClave.getCamposDeLlave().getCamposDeLlave().length;
                    String valor = null;
                    String descripcion = null;
                    int indice = 0;
                
                NameValue[] paramArrayOfNameValue = new NameValue[largoArreglo];
                    for (; indice < largoArreglo; indice++) {
                        valor = parametroEstrategiaSegundaClave.getCamposDeLlave().getCamposDeLlave()[indice].getNombre();
                        descripcion = parametroEstrategiaSegundaClave.getCamposDeLlave().getCamposDeLlave()[indice].getValorString(); 
                        paramArrayOfNameValue[indice] = new NameValue(valor,descripcion);
                }
                parms.setTransactionDetails(paramArrayOfNameValue);
                }
                       	
                parms.setPerformDeliveryAndSignature(Boolean.TRUE);
            }
            
            if (getLogger().isEnabledFor(Level.DEBUG)) {
                getLogger().debug("[obtenerDesafioGenerico][" + userID + "][parms][" + parms + "]");
            }
        	
            parametros.setParms(parms);
            
            if(getLogger().isEnabledFor(Level.INFO)){
                getLogger().info("[autenticarDesafioGenerico][" + userID + "]  parametros de request " + parametros.toString());
            }
            
            if (validarDisponibilidadDispositivo(parametroEstrategiaSegundaClave)){
            GenericAuthenticateResponse resultadoEntrust = new GenericAuthenticateResponse();
				
				cl.bci.www.esb.service.seguridad.autenticacion.segundaclave.types.AuthorizationPolicy politicaSeguridad = 
						new cl.bci.www.esb.service.seguridad.autenticacion.segundaclave.types.AuthorizationPolicy();
				
				politicaSeguridad.setUserName(UsuarioFuseEntrustUyP.getInstancia().getUsuarioAplicativoId());
				politicaSeguridad.setPassword(UsuarioFuseEntrustUyP.getInstancia().getClaveUsuarioAplicativo());
				
				resultadoEntrust = postType.autenticarDesafioGenerico(parametros, politicaSeguridad);
            
            if(getLogger().isEnabledFor(Level.INFO)){
            		getLogger().info("[autenticarDesafioGenerico][" + userID + "]  Respuesta del servicio :" + resultadoEntrust.toString());
            }
            
            if (OTP.equalsIgnoreCase(modoTransaccion) && resultadoEntrust!= null){
                resultado.setEstatus(true);
            }
			else {
                    resultado.setIdCodigoEstado(estatusOK);
            }
            
            }
            else {
                if(getLogger().isEnabledFor(Level.INFO)){
            	getLogger().info("[autenticarDesafioGenerico] cliente no tiene token asignado");
                	}
                throw new Exception(obtenerDescripcionErrorEntrust(SIN_TOKEN_ACTIVO));
            }
         
        }
        catch (RemoteException re){
            if(getLogger().isEnabledFor(Level.ERROR)){
                getLogger().error("[autenticarDesafioGenerico][BCI_FINEX][RemoteException][" + userID + "] Remota , mensaje=<"+ re.getMessage() + ">", re);
            }
            String detalleError = obtenerDescripcionErrorEntrust(CODIGO_ERROR_GENERICO_ENTRUST);
            try{
                SOAPFaultException ex = (SOAPFaultException) re.detail;
                detalleError = ex.getDetail().toString();
                detalleError = detalleError.substring(detalleError.indexOf("<ErrorCode>")+CORTE_PARA_ERROR_CODE,detalleError.indexOf("</ErrorCode>"));
            } 
            finally {
                if(getLogger().isEnabledFor(Level.INFO)){
                    getLogger().info("[autenticarDesafioGenerico][" + userID + "] Se lanza error = " + detalleError);
                }
                throw new Exception(detalleError);
                }
        
            }
        catch (Exception e) {
            if(getLogger().isEnabledFor(Level.ERROR)){
                getLogger().error("[autenticarDesafioGenerico][BCI_FINEX][Excepcion][" + userID + "] se relanza error generico , mensaje=<"+ e.getMessage() + ">", e);
            }
            throw new Exception(obtenerDescripcionErrorEntrust(CODIGO_ERROR_GENERICO_ENTRUST));
        }
        
        if(getLogger().isEnabledFor(Level.INFO)){
            getLogger().info("[autenticarDesafioGenerico][" + userID + "][BCI_FINOK]");
        }
        return resultado;
    }
    
    /**
     * Obtiene el desafo gen�rico, ya sea de modo ONLINE como OFFLINE.
     * <p>
     * 
     * Registro de Versiones:
     * <ul>
     * <li>1.0 10/07/2017 Luis L�pez Alamos (SEnTRA) - Ricardo Carrasco C�ceres (Ing. Soft. BCI): Versi�n Inicial.</li>
     * </ul>
     * <P>
     * 
     * @param parametroEstrategiaSegundaClave objeto con todos los datos necesarios. 
     * @return LlaveSegundaClaveTO.
     * @throws Exception en caso de error.
     * @since 2.0
     */
    public LlaveSegundaClaveTO obtenerDesafioGenerico(ParametrosEstrategiaSegundaClaveTO parametroEstrategiaSegundaClave) throws Exception{
        if(getLogger().isEnabledFor(Level.INFO)){
            getLogger().info("[obtenerDesafioGenerico][BCI_INI]");
        }
        String tipoAutenticacion = TablaValores.getValor(TABLADEPARAMETROS, "tipoAutenticacion", DESC);
        String modoTransaccion = parametroEstrategiaSegundaClave.getTipoSegundaClave();

        String userID =  obtenerRutClienteEntrust(parametroEstrategiaSegundaClave);
        
        LlaveSegundaClaveTO llaveGenerada = new LlaveSegundaClaveTO();
        
        if(getLogger().isEnabledFor(Level.INFO)){
            getLogger().info("[obtenerDesafioGenerico][" + userID + "] Datos cliente - autenticaci�n y modo [" 
            		+ userID + "][" + tipoAutenticacion + "][" + modoTransaccion);
        }
        
        try{
            ServicioAutenticacionPT postType = this.getService();
            GetGenericChallengeCallParms getGenericChallengeCallParms = new GetGenericChallengeCallParms();
            getGenericChallengeCallParms.setUserId(userID);
            
            GenericChallengeParms paramGenericChallengeParms = new GenericChallengeParms();
            paramGenericChallengeParms.setAuthenticationType(tipoAutenticacion);
            
            String switchPlantillas = TablaValores.getValor(TABLADEPARAMETROS, "switchPlantillas", DESC);
            
            if(getLogger().isEnabledFor(Level.DEBUG)){
                getLogger().debug("[obtenerDesafioGenerico][" + userID + "][switchPlantillas][" + switchPlantillas + "]");
            }
            
            if (switchPlantillas != null && switchPlantillas.trim().equalsIgnoreCase("OK")) {
            	paramGenericChallengeParms = (GenericChallengeParms) obtenerCamposLlaveEntrust(parametroEstrategiaSegundaClave, paramGenericChallengeParms);	
            }
            else {
            int largoArreglo = parametroEstrategiaSegundaClave.getCamposDeLlave().getCamposDeLlave().length;
            String descripcionChallenge = "";
            	String valor = null;
            	String descripcion = null;
            	int indice = 0;
            
            NameValue[] paramArrayOfNameValue = new NameValue[largoArreglo];
            	for (; indice < largoArreglo; indice++) {
            		
            		valor = parametroEstrategiaSegundaClave.getCamposDeLlave().getCamposDeLlave()[indice].getNombre();
            		descripcion = parametroEstrategiaSegundaClave.getCamposDeLlave().getCamposDeLlave()[indice].getValorString();
            		paramArrayOfNameValue[indice] = new NameValue(valor,descripcion);
            		
            		if (valor.equalsIgnoreCase(TablaValores.getValor(TABLADEPARAMETROS, "mensajeChalenge", DESC))) {
                          descripcionChallenge = descripcion; 
               }
               
            	}
            paramGenericChallengeParms.setTransactionDetails(paramArrayOfNameValue);
            	paramGenericChallengeParms.setTokenChallengeSummary(descripcionChallenge);
            }
				
            paramGenericChallengeParms.setPerformDeliveryAndSignature(Boolean.TRUE);
            paramGenericChallengeParms.setTokenTransactionMode(modoTransaccion);
            
            if (MODO_ONLINE.equalsIgnoreCase(modoTransaccion)){
                paramGenericChallengeParms.setTokenDeliveryCallback(TablaValores.getValor(TABLADEPARAMETROS, "tokenDeliveryCallBack", DESC));
            }
            
            if (MODO_OFFLINE.equalsIgnoreCase(modoTransaccion)){
                paramGenericChallengeParms.setTokenTransactionQRCodeSize(Integer.valueOf(TablaValores.getValor(TABLADEPARAMETROS, "qrSizeOffline", DESC)));
                paramGenericChallengeParms.setTokenTransactionReturnQRCode(Boolean.TRUE);
                
            }
                
            getGenericChallengeCallParms.setParms(paramGenericChallengeParms);
            
            if(getLogger().isEnabledFor(Level.INFO)){
                getLogger().info("[obtenerDesafioGenerico][" + userID + "]  parametros de request " + paramGenericChallengeParms.toString());
            }
            
            GenericChallenge respuestaServicio = new GenericChallenge();
			
			cl.bci.www.esb.service.seguridad.autenticacion.segundaclave.types.AuthorizationPolicy politicaSeguridad = 
					new cl.bci.www.esb.service.seguridad.autenticacion.segundaclave.types.AuthorizationPolicy();
			
			politicaSeguridad.setUserName(UsuarioFuseEntrustUyP.getInstancia().getUsuarioAplicativoId());
			politicaSeguridad.setPassword(UsuarioFuseEntrustUyP.getInstancia().getClaveUsuarioAplicativo());
			
			respuestaServicio = postType.obtenerDesafioGenerico(getGenericChallengeCallParms, politicaSeguridad);
            
            if(getLogger().isEnabledFor(Level.INFO)){
                getLogger().info("[obtenerDesafioGenerico][" + userID + "]  Respuesta del servicio :" + respuestaServicio.toString());
            }
            
            if (MODO_ONLINE.equalsIgnoreCase(modoTransaccion)){
                llaveGenerada.setSerial(respuestaServicio.getTransactionId());
                if(getLogger().isEnabledFor(Level.INFO)){
                    getLogger().info("[obtenerDesafioGenerico][" + userID + "]" + llaveGenerada.getSerial() + "[BCI_FINOK]");
                }
                llaveGenerada.setEstatus(false);
                return llaveGenerada;
            }
            
            
            if (MODO_OFFLINE.equalsIgnoreCase(modoTransaccion)){
                int arregloQr = respuestaServicio.getTokenChallenge().getTokens().length;
                RepresentacionSegundaClave[] llave = new RepresentacionNumericaSegundaClaveTO[arregloQr];
                for (int i = 0; i < respuestaServicio.getTokenChallenge().getTokens().length; i++ ){
                    RepresentacionNumericaSegundaClaveTO llaveInterna = new RepresentacionNumericaSegundaClaveTO();
                    llaveInterna.setNombreDispositivoAsociado(respuestaServicio.getTokenChallenge().getTokens()[i].getTokenSet());
                    llaveInterna.setClaveAlfanumericaMovil(respuestaServicio.getTokenChallenge().getTokens()[i].getOfflineChallenge());
                    llaveInterna.setClaveAlfanumerica(convierteByteArrayAQr(respuestaServicio.getTokenChallenge().getTokens()[i].getOfflineChallengeQRCode()));
                    llave[i] = llaveInterna;
                    }
                llaveGenerada.setLlaves(llave);
                llaveGenerada.setIdCodigoEstado(Integer.parseInt(TablaValores.getValor(TABLADEPARAMETROS, "estatusok", DESC)));
                llaveGenerada.setEstatus(true);
            }
           
        }
        catch (RemoteException re){
            if (getLogger().isEnabledFor(Level.ERROR)){
                getLogger().error("[obtenerDesafioGenerico][BCI_FINEX][Excepcion][" + userID + "]Remota , mensaje=<"+ re.getMessage() + ">", re);
            }
            String detalleError = obtenerDescripcionErrorEntrust(CODIGO_ERROR_GENERICO_ENTRUST);
            try{
                SOAPFaultException ex = (SOAPFaultException) re.detail;
                detalleError = ex.getDetail().toString();
                detalleError = detalleError.substring(detalleError.indexOf("<ErrorCode>")+CORTE_PARA_ERROR_CODE,detalleError.indexOf("</ErrorCode>"));
            } 
            finally {
                if(getLogger().isEnabledFor(Level.INFO)){
                    getLogger().info("[obtenerDesafioGenerico][" + userID + "] Se lanza error = " + detalleError);
        }
                throw new Exception(detalleError);
            }
            
        }
        catch (Exception e){
            if (getLogger().isEnabledFor(Level.ERROR)){
                getLogger().error("[obtenerDesafioGenerico][BCI_FINEX][Exception][" + userID + "] Error al obtener una respuesta del m�todo:" + e.getMessage(), e);
            }
            throw new Exception(obtenerDescripcionErrorEntrust(CODIGO_ERROR_GENERICO_ENTRUST));
        }
        
        if(getLogger().isEnabledFor(Level.INFO)){
            getLogger().info("[obtenerDesafioGenerico][BCI_FINOK][" + userID + "]");
        }
        return llaveGenerada;
    }

	/**
	 * M�todo que obtiene los campos llave que cada aplicaci�n envia para despu�s generar el desaf�o y postermente autenticarlo.<br>
	 * 
	 * Dichos desaf�os son ONLINE u OFFLINE dependiendo de lo que cada canal y/o aplicaci�n requiera.
	 * <br><br>
	 * Posteriomente, estos campos son tratados para destacar, eliminar y/o formatear sus etiquetas o valores seg�n corresponde. 
	 * 
	 * <p>
	 *    Registro de Versiones:
	 * <ul>
	 * <li>1.0 11/07/2017 Jaime Suazo (SEnTRA) - Ricardo Carrasco Caceres (Ing. Soft. BCI): Version Inicial.</li>
	 * </ul>
	 * <P>
	 * 
	 * @param parametroEstrategiaSegundaClave TO que contiene los campos llave de cada transaccion.
	 * @param paramChallengeParms objeto en cual se registraran y luego enviaran los valorea a los servicios entrust.
	 * @return objeto que contiene el arreglo de campos llave.
	 * @throws GeneralException error general de validaciones.
	 * @since 2.0
	 */    
	private Object obtenerCamposLlaveEntrust(ParametrosEstrategiaSegundaClaveTO parametroEstrategiaSegundaClave, 
			Object paramChallengeParms) throws GeneralException {
		
		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[obtenerCamposLlaveEntrust][BCI_INI][" + parametroEstrategiaSegundaClave 
					+ "][" + paramChallengeParms + "]");
		}

		String userID =  obtenerRutClienteEntrust(parametroEstrategiaSegundaClave);
		
		if (parametroEstrategiaSegundaClave == null) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger().error("[obtenerCamposLlaveEntrust][" + userID + "][BCI_FINEX] ParametrosEstrategiaSegundaClaveTO no puede ser null");
			}
			throw new GeneralException("ESPECIAL", "ParametrosEstrategiaSegundaClaveTO no puede ser null");
		}
		
		if (parametroEstrategiaSegundaClave.getServicio() == null || parametroEstrategiaSegundaClave.getServicio().trim().equals("")) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger().error("[obtenerCamposLlaveEntrust][" + userID + "][BCI_FINEX] El nombre del servicio no puede ser nulo o vacío");
			}
			throw new GeneralException("ESPECIAL", "El nombre del servicio no puede ser nulo o vac�o");
		}
		
		if (parametroEstrategiaSegundaClave.getCamposDeLlave() == null 
				|| parametroEstrategiaSegundaClave.getCamposDeLlave().getCamposDeLlave() == null
				|| parametroEstrategiaSegundaClave.getCamposDeLlave().getCamposDeLlave().length == LARGO_CERO) {
			
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger().error("[obtenerCamposLlaveEntrust][" + userID + "][BCI_FINEX] El arreglo de campos llave no puede ser nulo o vacío");
			}
			throw new GeneralException("ESPECIAL", "El arreglo de campos llave no puede ser nulo o vac�o");
		}
				
		String strMaximoCantidadCamposLLave = TablaValores.getValor(TABLADEPARAMETROS, "cantidadMaximaDeCamposLlave", DESC);
		int maximaCantidadCampos = VALOR_CERO;
		try {
			maximaCantidadCampos = Integer.parseInt(strMaximoCantidadCamposLLave);
		} 
		catch (Exception e) {
			if (getLogger().isEnabledFor(Level.WARN)) {
				getLogger().warn("[obtenerCamposLlaveEntrust][" + userID + "][" + strMaximoCantidadCamposLLave 
						+ "][Integer.parseInt(strMaximoCantidadCamposLLave)]" + e.getMessage());
			}
			maximaCantidadCampos = MAXIMA_CANTIDAD_CAMPOS; 
		}
		
		int largoArreglo = parametroEstrategiaSegundaClave.getCamposDeLlave().getCamposDeLlave().length;
		
		if (largoArreglo > maximaCantidadCampos) {
			if (getLogger().isEnabledFor(Level.ERROR)) {
				getLogger().error("[obtenerCamposLlaveEntrust][" + userID + "][BCI_FINEX] la llave no puede tener mas de " +  maximaCantidadCampos + " campos");
			}
			
			throw new GeneralException("ESPECIAL", "la llave no puede tener mas de " +  maximaCantidadCampos + " campos"); 
		}
				
		Map datosDestacados = obtenerMapaDeCampos(
				TablaValores.getValor(TABLADEPARAMETROS, parametroEstrategiaSegundaClave.getServicio(), "campoDestacado"));
		
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug("[obtenerCamposLlaveEntrust][" + userID + "][servicio][" + parametroEstrategiaSegundaClave.getServicio() 
				+ "][datosDestacados][" + datosDestacados + "]");
		}
		
		Map datosAEliminar = obtenerMapaDeCampos(
				TablaValores.getValor(TABLADEPARAMETROS, parametroEstrategiaSegundaClave.getServicio(), "campoAEliminar"));

		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug("[obtenerCamposLlaveEntrust][" + userID + "][servicio][" + parametroEstrategiaSegundaClave.getServicio() 
				+ "][campoAEliminar][" + datosAEliminar + "]");
		}
		
		Map datosAFormatear = obtenerMapaDeCampos(
				TablaValores.getValor(TABLADEPARAMETROS, parametroEstrategiaSegundaClave.getServicio(), "formato"));
		
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug("[obtenerCamposLlaveEntrust][" + userID + "][servicio][" + parametroEstrategiaSegundaClave.getServicio() 
				+ "][formato][" + datosAFormatear + "]");
		}
		
		Map datosAcambiarEtiqueta = obtenerMapaDeCampos(
				TablaValores.getValor(TABLADEPARAMETROS, parametroEstrategiaSegundaClave.getServicio(), "cambioEtiqueta"));
		
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug("[obtenerCamposLlaveEntrust][" + userID + "][servicio][" + parametroEstrategiaSegundaClave.getServicio() 
				+ "][cambioEtiqueta][" + datosAcambiarEtiqueta + "]");
		}		
		
		String templateSummaryEntrust = "";
		String llave = "";
		String valor = "";
		 
		String descripcionServicio = TablaValores.getValor(TABLADEPARAMETROS, parametroEstrategiaSegundaClave.getServicio(), TITULO_CAMPOS_LLAVE_ENTRUST);
		
		if (descripcionServicio == null) {
			descripcionServicio = TablaValores.getValor(TABLADEPARAMETROS, "tituloPorDefecto", "valor");
		}
		
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug("[obtenerCamposLlaveEntrust][" + userID + "][servicio][" + parametroEstrategiaSegundaClave.getServicio() 
				+ "][descripcionServicio][" + descripcionServicio + "]");
		}
		
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug("[obtenerCamposLlaveEntrust][" + userID + "][servicio][" + parametroEstrategiaSegundaClave.getServicio() 
				+ "][largoArreglo][" + largoArreglo + "][descripcionServicio][" + descripcionServicio + "]");
		}
		
		ArrayList listaOriginal = new ArrayList();
		HashMap mapaOrdenamiento = new HashMap();
		
		listaOriginal.add(new NameValue(TITULO_CAMPOS_LLAVE_ENTRUST, descripcionServicio));
		mapaOrdenamiento.put( String.valueOf(VALOR_MENOS_UNO), new NameValue(TITULO_CAMPOS_LLAVE_ENTRUST, descripcionServicio) );
		
		String subtitulo = TablaValores.getValor(TABLADEPARAMETROS, parametroEstrategiaSegundaClave.getServicio(), SUB_TITULO_PUSH_LLAVE_ENTRUST);
		
		if (subtitulo != null && !subtitulo.trim().equals("")) {
			listaOriginal.add(new NameValue(SUB_TITULO_PUSH_LLAVE_ENTRUST, subtitulo));
			mapaOrdenamiento.put( String.valueOf(VALOR_CERO), new NameValue(SUB_TITULO_PUSH_LLAVE_ENTRUST, subtitulo) );
		}
		
		int indice = VALOR_UNO;
		int indiceOrdenamiento = VALOR_CERO;
 
		for (; indice <= largoArreglo; indice++) {

			if (datosAEliminar != null && datosAEliminar.get(parametroEstrategiaSegundaClave.getCamposDeLlave().getCamposDeLlave()[indice - VALOR_UNO].getNombre()) != null) {
				continue;
			}

			if (datosDestacados != null 
					&& datosDestacados.get(parametroEstrategiaSegundaClave.getCamposDeLlave().getCamposDeLlave()[indice - VALOR_UNO].getNombre()) != null) {

				llave = CARACTERES_PARA_DESTACAR_CAMPOS_ENTRUST+TablaValores.getValor(TABLADEPARAMETROS, parametroEstrategiaSegundaClave.getServicio(), 
						parametroEstrategiaSegundaClave.getCamposDeLlave().getCamposDeLlave()[indice - VALOR_UNO].getNombre());
			}
			else if (datosAcambiarEtiqueta != null 
					&& datosAcambiarEtiqueta.get(parametroEstrategiaSegundaClave.getCamposDeLlave().getCamposDeLlave()[indice - VALOR_UNO].getNombre()) != null) {
				
				llave = TablaValores.getValor(TABLADEPARAMETROS, parametroEstrategiaSegundaClave.getServicio(), 
						parametroEstrategiaSegundaClave.getCamposDeLlave().getCamposDeLlave()[indice - VALOR_UNO].getNombre());
			}			
			else { 
				llave = parametroEstrategiaSegundaClave.getCamposDeLlave().getCamposDeLlave()[indice - VALOR_UNO].getNombre();
			}
			
			if (datosAFormatear!= null 
					&& datosAFormatear.get(parametroEstrategiaSegundaClave.getCamposDeLlave().getCamposDeLlave()[indice - VALOR_UNO].getNombre())  != null ) {
				
				valor = formatearDescripcion((String) datosAFormatear.get(parametroEstrategiaSegundaClave.getCamposDeLlave().getCamposDeLlave()[indice - VALOR_UNO].getNombre()),
						parametroEstrategiaSegundaClave.getCamposDeLlave().getCamposDeLlave()[indice - VALOR_UNO].getValorString());
			}
			else {
				
				valor = parametroEstrategiaSegundaClave.getCamposDeLlave().getCamposDeLlave()[indice - VALOR_UNO].getValorString();
			}
			
			listaOriginal.add(new NameValue(llave + SEPARADOR_DE_ETIQUETA_VALOR, valor));
			
			indiceOrdenamiento = obtenerIndiceOrdenamiento(parametroEstrategiaSegundaClave, 
										parametroEstrategiaSegundaClave.getCamposDeLlave().getCamposDeLlave()[indice - VALOR_UNO].getNombre());
			
			if (indiceOrdenamiento > VALOR_CERO) {
				mapaOrdenamiento.put(String.valueOf(indiceOrdenamiento), new NameValue(llave + SEPARADOR_DE_ETIQUETA_VALOR, valor));	
			}
		}
		
		ArrayList listaKeysOrden = new ArrayList();
		listaKeysOrden.addAll(mapaOrdenamiento.keySet());
		Collections.sort(listaKeysOrden);
		int index = VALOR_CERO;
		
		NameValue llaveValorPaso = null;
		
		if (mapaOrdenamiento.size() > VALOR_UNO) {
			ArrayList listaValuesOrden = new ArrayList();
			
			for (; index < listaKeysOrden.size(); index++) {
				llaveValorPaso = (NameValue) mapaOrdenamiento.get(listaKeysOrden.get(index).toString());
				listaValuesOrden.add(llaveValorPaso);
			}
			
			listaOriginal = listaValuesOrden;
		}
		
		if (paramChallengeParms instanceof GenericChallengeParms) {
			((GenericChallengeParms) paramChallengeParms).setTransactionDetails((NameValue[]) listaOriginal.toArray(new NameValue[LARGO_CERO]) );
			
			templateSummaryEntrust = TablaValores.getValor(TABLADEPARAMETROS, parametroEstrategiaSegundaClave.getServicio(), DESC);
			
			if (templateSummaryEntrust == null) {
				templateSummaryEntrust = TablaValores.getValor(TABLADEPARAMETROS, "templateGenericoServicios", DESC);
			}
			
			((GenericChallengeParms) paramChallengeParms).setTokenChallengeSummary(templateSummaryEntrust);
			
			if (getLogger().isEnabledFor(Level.DEBUG)) {
				getLogger().debug("[obtenerCamposLlaveEntrust][" + userID + "][servicio][" 
						+ parametroEstrategiaSegundaClave.getServicio() + "][paramChallengeParms][" + paramChallengeParms + "]");
			}			
		}
		else if (paramChallengeParms instanceof GenericAuthenticateParms ) {
			((GenericAuthenticateParms) paramChallengeParms).setTransactionDetails((NameValue[]) listaOriginal.toArray(new NameValue[LARGO_CERO]) );
		}
		
		if (getLogger().isEnabledFor(Level.DEBUG)) {
			getLogger().debug("[obtenerCamposLlaveEntrust][" + userID + "][servicio][" 
					+ parametroEstrategiaSegundaClave.getServicio() + "][descripcionChallenge][" + templateSummaryEntrust + "]");
		}

		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[obtenerCamposLlaveEntrust][BCI_FINOK][" + userID + "][" + listaOriginal + "]");
		}
    
		return paramChallengeParms;
	}

	/**
	 * Metodo que obtiene el indice de inserci�n para la lista de campos que son enviados a entrust para luego 
	 * ser desplegados en la app m�vil de auntenicaci�n.
	 * <br>
	 * En caso de que el campo no exista en entrust.parametros, el servicio retorna -1. Dado lo anterior,
	 * este valor no ser� enviado a Entrust, ya que significa que el campo no tiene un orden establecido.
	 * 
	 * <p>
	 *    Registro de Versiones:
	 * <ul>
	 * <li>1.0 11/07/2017 Jaime Suazo (SEnTRA) - Ricardo Carrasco Caceres (Ing. Soft. BCI): Version Inicial.</li>
	 * </ul>
	 * <P>
	 * 
	 * @param parametroEstrategiaSegundaClave parametros para generar desaf�o.
	 * @param llave el nombre del campo que ser� ordenado.
	 * @return indice de lista.
	 */
	private int obtenerIndiceOrdenamiento(ParametrosEstrategiaSegundaClaveTO parametroEstrategiaSegundaClave, String llave) {
		int resultado = VALOR_MENOS_UNO;
		
		try {
			String indiceStr = TablaValores.getValor(TABLADEPARAMETROS, parametroEstrategiaSegundaClave.getServicio(), "indice-" + llave);
			
			resultado = Integer.parseInt(indiceStr);
		} 
		catch (Exception e) {
			resultado = VALOR_MENOS_UNO;
            if (getLogger().isEnabledFor(Level.ERROR)) {
                getLogger().error("[obtenerIndiceOrdenamiento][Exception][" + parametroEstrategiaSegundaClave.getServicio() 
                	+ "][" + "indice-" + llave + "] Error :" + e.getMessage(), e);
            }
		}

		return resultado;
	}
	
	/** 
	 * M�todo que formatea el valor enviado seg�n el tipo de formato solicitado.<br><br>
	 * 
	 * Este valor se va hacia los servicios de generaci�n y autenticaci�n de desaf�o.
	 *  
	 * 
	 * <p>
	 *    Registro de Versiones:
	 * <ul>
	 * <li>1.0 11/07/2017 Jaime Suazo (SEnTRA) - Ricardo Carrasco Caceres (Ing. Soft. BCI): Versión Inicial.</li>
	 * </ul>
	 * <P>
	 *
	 * @param tipoFormato el nombre del tipo de formato (moneda, fecha, etc).
	 * @param valorDescripcion valor a formatear asociado al tipo de formato.
	 * @return valor formateado seg�n el tipo de formato.
	 * @since 2.0
	 */
	private String formatearDescripcion(String tipoFormato, String valorDescripcion) {
		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[formatearDescripcion][BCI_INI][" + tipoFormato 
					+ "][" + valorDescripcion + "]");
		}
		
		if (tipoFormato != null && valorDescripcion != null && !valorDescripcion.trim().equals("")) {

			if (tipoFormato.trim().equalsIgnoreCase("moneda")) {
				valorDescripcion = valorDescripcion.trim();
				valorDescripcion = StringUtil.eliminaCaracteres(valorDescripcion, ".");

				try {
					String simboloPesoCL = TablaValores.getValor(TABLADEPARAMETROS, "simboloPesoCL", DESC);
					valorDescripcion = simboloPesoCL + formatearMonto(Long.parseLong(valorDescripcion));
				} 
				catch (Exception ignored) {
					if (getLogger().isEnabledFor(Level.WARN)) {
						getLogger().warn("[formatearDescripcion][Exception][moneda][" + valorDescripcion + "]" + ignored.getMessage());
					}
				}
			}
			else if (tipoFormato.trim().equalsIgnoreCase("rut")) {
				try {

					valorDescripcion = RUTUtil.formatearRut(valorDescripcion);

				} catch (Exception ignored) {
					if (getLogger().isEnabledFor(Level.WARN)) {
						getLogger().warn("[formatearDescripcion][Exception][moneda][" + valorDescripcion + "]" + ignored.getMessage());
					}

				}
			}
			else if (tipoFormato.trim().equalsIgnoreCase("numeroTDCOrigen")) {
				valorDescripcion = StringUtil.censura(valorDescripcion);
			}

		}

		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[formatearDescripcion][BCI_FINOK][" + valorDescripcion + "]");
		}

		return valorDescripcion;
	}
    
    
    /**
     * Método que agrega los puntos de separación de miles al monto a ser presentado.
     * <p>
     * Ejemplo:
     * <p>
     * <code>"123456" --> "123.456" </code>
     * <p>
     * Registro de versiones:
     * <ul>
     *   <li>1.0 11/07/2017 Jaime Suazo (SEnTRA) - Ricardo Carrasco Cáceres (Ing. Soft. BCI): Versión Inicial.</li>
     * </ul>
     * @param monto Monto a convertir.
     * @return Monto con separación de miles.
     * @since 1.0
     */
    public String formatearMonto(long monto){
        return StringUtil.join(".", StringUtil.fracciona(String.valueOf(monto), CANTIDAD_DIGITOS_MILES,
                StringUtil.DE_DERECHA_A_IZQUIERDA));
    }	
    
    /**
     * Obtiene la conexi�n con servicio Autenticaci�n Entrust.
     * <p>
     * Registro de versiones: 
     * <ul>
     * <li>1.0 10/07/2017 Luis L�pez Alamos (SEnTRA) - Ricardo Carrasco (Ing. Soft. BCI): Version inicial.</li>
     * </ul>
     * <p> 
     * @return ServicioAutenticacionPT conexi�n con cliente ws.
     * @throws Exception en caso de error
     * @since 2.0
     */
    private ServicioAutenticacionPT getService() throws Exception {
        if (getLogger().isEnabledFor(Level.INFO)) {
            getLogger().info("[getService][BCI_INI]");
        }
        String wsdlUrl = TablaValores.getValor(TABLADEPARAMETROS, "WSParametros", "url");
        String timeOut = TablaValores.getValor(TABLADEPARAMETROS, "WSParametros", "timeoutSeconds");

        if(getLogger().isEnabledFor(Level.DEBUG)){
            getLogger().debug("[getService] wsdlUrl [" + wsdlUrl + "]");
        }
        if (wsdlUrl == null || wsdlUrl.trim().equals("")) {
            if (getLogger().isEnabledFor(Level.ERROR)) {
                getLogger().error("[getService][BCI_FINEX] Error al leer la url del wsdl");
            }
            throw new Exception("Error al leer la url del wsdl");
        }
        if(getLogger().isEnabledFor(Level.DEBUG)){
            getLogger().debug("[getService] timeOut [" + timeOut + "]");
        }
        if (timeOut == null || timeOut.trim().equals("")) {
            if (getLogger().isEnabledFor(Level.ERROR)) {
                getLogger().error("[getService][BCI_FINEX] Error al leer la timeOut del cliente");
            }
            throw new Exception("Error al leer la timeOut del cliente");
        }
        ServicioAutenticacionBCI servicio = new ServicioAutenticacionBCI_Impl(wsdlUrl);
        ServicioAutenticacionPT tipoServicio = servicio.getServicioAutenticacionPT();
        ServicioAutenticacionPT_Stub  stubDeServicio = (ServicioAutenticacionPT_Stub) tipoServicio;
        stubDeServicio._setProperty(javax.xml.rpc.Stub.USERNAME_PROPERTY, "");
        stubDeServicio._setProperty(javax.xml.rpc.Stub.PASSWORD_PROPERTY, "");
        
        if (getLogger().isEnabledFor(Level.INFO)) {
            getLogger().info("[getService][BCI_FINOK]");
        }
        return stubDeServicio;

    }
    /**
	 * Obtiene la conexi�n con servicio Administracion Entrust. Bundle fuse-service-administracionentrust.jar
	 * 
     * <p>
     * Registro de versiones: 
     * <ul>
     * <li>1.0 10/07/2017 Andr�s Silva H. (SEnTRA) - Ricardo Carrasco (Ing. Soft. BCI): Versi�n inicial.</li>
     * </ul>
     * <p> 
     * @return ServicioAutenticacionPT conexi�n con cliente ws.
     * @throws Exception en caso de error.
     * @since 2.0
     */
    private ServicioAdministracionPT getServiceADM() throws Exception {
        if (getLogger().isEnabledFor(Level.INFO)) {
            getLogger().info("[getService][BCI_INI]");
        }
        String wsdlUrl = TablaValores.getValor(TABLADEPARAMETROS, "WSAdminParametros", "url");
        String timeOut = TablaValores.getValor(TABLADEPARAMETROS, "WSAdminParametros", "timeoutSeconds");

        if(getLogger().isEnabledFor(Level.DEBUG)){
            getLogger().debug("[getService] wsdlUrl [" + wsdlUrl + "]");
        }
        if (wsdlUrl == null || wsdlUrl.trim().equals("")) {
            if (getLogger().isEnabledFor(Level.ERROR)) {
                getLogger().error("[getService][BCI_FINEX] Error al leer la url del wsdl");
            }
            throw new Exception("Error al leer la url del wsdl");
        }
        if(getLogger().isEnabledFor(Level.DEBUG)){
            getLogger().debug("[getService] timeOut [" + timeOut + "]");
        }
        if (timeOut == null || timeOut.trim().equals("")) {
            if (getLogger().isEnabledFor(Level.ERROR)) {
                getLogger().error("[getService][BCI_FINEX] Error al leer la timeOut del cliente");
            }
            throw new Exception("Error al leer la timeOut del cliente");
        }
        ServicioAdministracionBCI servicio = new ServicioAdministracionBCI_Impl(wsdlUrl);
        ServicioAdministracionPT tipoServicio = servicio.getServicioAdministracionPT();
        ServicioAdministracionPT_Stub  stubDeServicio = (ServicioAdministracionPT_Stub) tipoServicio;
		
        stubDeServicio._setProperty(javax.xml.rpc.Stub.USERNAME_PROPERTY, "");
        stubDeServicio._setProperty(javax.xml.rpc.Stub.PASSWORD_PROPERTY, "");
        
        if (getLogger().isEnabledFor(Level.INFO)) {
            getLogger().info("[getService][BCI_FINOK]");
        }
        return stubDeServicio;

    }
    
    /**
     * Metodo que actualiza (cambia el estado) de un Softoken (STK) de un cliente.<br>
     * 
     * Los servicios llamados desde aqui, son: fuse <code>actualizarTokenUsuario</code>, Entrust <code>userTokenUpdate</code>.
     * <br><br>
     * Los estados permitidos de ENTRUST, son: <code>CURRENT</code>, <code>PENDING</code>, <code>HOLD</code>, 
     * <code>CANCELED</code> (Este �ltimo es estado final, es decir, no se puede cambiar a otro estado un Softoken).
     * <br><br>
     * Los datos obligatorios del servicio, son: <code>UserId</code>(RUT formato Entrust (Grupo)/(RUT)-(DV)),
     *                                           <code>State</code>,
     *                                           <code>SerialNumber</code>                                           
     * 
     * Registro de versiones:
     * <ul>
     * <li>1.0 10/07/2017 Jaime Suazo. (SEnTRA) - Ricardo Carrasco (Ing. Soft. BCI): versi�n inicial.</li>
     * </ul>
     * 
     * @param parametroEstrategiaSegundaClave datos del cliente y del token a modificar su estado.
     * @return ResponseStatusTO objeto con el resultado de la ejecucion del servicio en fuse v�a entrust.  
     * @since 2.0
     */    
    public ResponseStatusTO actualizarTokenUsuario(ParametrosEstrategiaSegundaClaveTO parametroEstrategiaSegundaClave) {
        if (getLogger().isEnabledFor(Level.INFO)) {
            getLogger().info("[actualizarTokenUsuario][BCI_INI][" + parametroEstrategiaSegundaClave + "]");
        }
        String userId = obtenerRutClienteEntrust(parametroEstrategiaSegundaClave);
        
        if(getLogger().isEnabledFor(Level.DEBUG)) {
	       getLogger().debug("[actualizarTokenUsuario][userID][" + userId + "]");
        }
        
        if (parametroEstrategiaSegundaClave == null || parametroEstrategiaSegundaClave.getTokenEntrust() == null) {
        	throw new IllegalArgumentException("ParametrosEstrategiaSegundaClaveTO TokenEntrust invalido");
        }
        
        ResponseStatusTO respuesta = new ResponseStatusTO();
        
        try {
            UserTokenUpdateRequest requestFuse = new UserTokenUpdateRequest();
            requestFuse.setUserId(userId);
            requestFuse.setState(parametroEstrategiaSegundaClave.getTokenEntrust().getEstado().toUpperCase());
            requestFuse.setSerialNumber(parametroEstrategiaSegundaClave.getTokenEntrust().getSerialNumber());

            if(getLogger().isEnabledFor(Level.DEBUG)){
                getLogger().debug("[actualizarTokenUsuario] request seteado [" + userId + "]");
            }

            ServicioAdministracionPT portType = this.getServiceADM();
            
            if(getLogger().isEnabledFor(Level.DEBUG)){
                getLogger().debug("[actualizarTokenUsuario] antes de llamar a [portType.actualizarTokenUsuario][" 
                		+ requestFuse + "] [" + userId + "]");
            }
            
			AuthorizationPolicy politicaSeguridad = new AuthorizationPolicy();
			
			politicaSeguridad.setUserName(UsuarioFuseEntrustUyP.getInstancia().getUsuarioAplicativoId());
			politicaSeguridad.setPassword(UsuarioFuseEntrustUyP.getInstancia().getClaveUsuarioAplicativo());
			
			UserTokenUpdateResponse respuestaFuse = portType.actualizarTokenUsuario(requestFuse, politicaSeguridad);
            
            if(getLogger().isEnabledFor(Level.DEBUG)){
                getLogger().debug("[actualizarTokenUsuario] respuesta obtenida OK [respuestaFuse][" 
                		+ userId + "]["
                		+ respuestaFuse.getResponseStatus().getCode() + "]["
                		+ respuestaFuse.getResponseStatus().getDescription() + "]");                		
            }
            
            respuesta.setCode(respuestaFuse.getResponseStatus().getCode());
            respuesta.setDescription(this.obtenerDescripcionErrorEntrust(respuestaFuse.getResponseStatus().getCode()));
            
		} 
        catch (Exception e) {
            if (getLogger().isEnabledFor(Level.ERROR)) {
                getLogger().error("[actualizarTokenUsuario][BCI_FINEX][Exception][" + userId 
                	+ "]" + e.getMessage(), e);
        }
        
            respuesta.setCode(CODIGO_ERROR_GENERICO_ENTRUST);
            respuesta.setDescription(this.obtenerDescripcionErrorEntrust(CODIGO_ERROR_GENERICO_ENTRUST));
		}
                
        if (getLogger().isEnabledFor(Level.INFO)) {
            getLogger().info("[actualizarTokenUsuario][BCI_FINOK][" + userId + "][" + respuesta + "]");
        }
        
        return respuesta;
        
    }

    /**
     * Metodo que obtiene la descripcion de error que retorna IDG - Pooling - Entrust.
     * 
     * 
     * Registro de versiones:
     * <ul>
     * <li>1.0 10/07/2017 Jaime Suazo. (SEnTRA) - Ricardo Carrasco (Ing. Soft. BCI): versi�n inicial.</li>
     * </ul>
     * 
     * @param codigoErrorEntrust codigo de error entrust.
     * @return descripcion de codigo de error entrust. 
     * @since 2.0
     */        
    private String obtenerDescripcionErrorEntrust(int codigoErrorEntrust) {
        if (getLogger().isEnabledFor(Level.INFO)) {
            getLogger().info("[obtenerDescripcionErrorEntrust][BCI_INI][" + codigoErrorEntrust + "]");
        }
    	
        String descripcionErrorEntrust = TablaValores.getValor(TABLADEPARAMETROS, 
        		String.valueOf(codigoErrorEntrust), "desc");
        
        if (getLogger().isEnabledFor(Level.DEBUG)) {
            getLogger().debug("[obtenerDescripcionErrorEntrust][descripcionErrorEntrust][" 
            		+ descripcionErrorEntrust + "] [" + codigoErrorEntrust + "]");
        }
        
        if (getLogger().isEnabledFor(Level.INFO)) {
            getLogger().info("[obtenerDescripcionErrorEntrust][BCI_FINOK][" + codigoErrorEntrust + "]");
        }
        
        return descripcionErrorEntrust;
    }
    
    
    
    
    /**
     * Consulta si el cliente realiz� alguna acci�n al obtener un desafio de transacci�n en Entrust.
     * <p>
     * 
     * Registro de Versiones:
     * <ul>
     * <li>1.0 10/07/2017 Luis L�pez Alamos (SEnTRA) - Ricardo Carrasco C�ceres (Ing. Soft. BCI): Versi�n Inicial.</li>
     * </ul>
     * <P>
     * 
     * @param codigoTransaccion con el codigo enviado desde la web para consultar si se dio el OK por parte de entrust.
     * @return String con el resultado.
     * @since 2.0
     */
    public String consultarEstadoDesafio(String codigoTransaccion){
        if (getLogger().isEnabledFor(Level.INFO)) {
        	getLogger().info("[consultarEstadoDesafio][Codigo Transaccion = " + codigoTransaccion + "][BCI_INI]");
        }
            String respuesta = null;
        try {
            ServicioAutenticacionPT postType = this.getService();
            TransactionRequest peticionTransaccion = new TransactionRequest();
            TransactionResponse respuestaTransaccion = new TransactionResponse();
            peticionTransaccion.setTransactionId(codigoTransaccion);
			
			cl.bci.www.esb.service.seguridad.autenticacion.segundaclave.types.AuthorizationPolicy politicaSeguridad = 
					new cl.bci.www.esb.service.seguridad.autenticacion.segundaclave.types.AuthorizationPolicy();
			
			politicaSeguridad.setUserName(UsuarioFuseEntrustUyP.getInstancia().getUsuarioAplicativoId());
			politicaSeguridad.setPassword(UsuarioFuseEntrustUyP.getInstancia().getClaveUsuarioAplicativo());

			respuestaTransaccion = postType.consultarEstadoDesafio(peticionTransaccion, politicaSeguridad);
            respuesta = respuestaTransaccion.getTransactionStatus();
        }
        catch (Exception e){
            if (getLogger().isEnabledFor(Level.WARN)){
                getLogger().warn("[consultarEstadoDesafio][BCI_FINEX][Exception] Error de AutenticationSystem:" + e.getMessage(), e);
            }
        }
        
        if (getLogger().isEnabledFor(Level.INFO)) {
        	getLogger().info("[consultarEstadoDesafio][respuesta Servicio = " + respuesta + "][BCI_FINOK]");
        }
        return respuesta;
    }

    /**
     * Metodo que ejemplo:"recupera los datos iniciales."
     * <p>
     * Registro de versiones: 
     * <ul>
     * <li>1.0 10/07/2017 Sergio Flores (SEnTRA) - Ricardo Carrasco (Ing. Soft. BCI): Version inicial.</li>
     * <li>2.0 20/10/2017 Jaime Suazo D. (SEnTRA) - Ricardo Carrasco (Ing. Soft. BCI): Se agrega a TokenEntrustTO
     *         seteo de los campos estadoActivacion, plataformaMovil, fechaDeRegistro.</li>
     * </ul>
     * <p> 
     * @param parametroEstrategiaSegundaClave datos del cliente.
     * @return TokenEntrustTO[] objeto con el resultado de la ejecucion del servicio en fuse v�a entrust.
     * @since 2.0
     */
    public TokenEntrustTO[] listarTokenUsuario(ParametrosEstrategiaSegundaClaveTO parametroEstrategiaSegundaClave){
        if(getLogger().isEnabledFor(Level.INFO)){
            getLogger().info("[listarTokenUsuario][BCI_INI] [" + parametroEstrategiaSegundaClave +"]");
        }
        
        String userId = obtenerRutClienteEntrust(parametroEstrategiaSegundaClave);        

        if(getLogger().isEnabledFor(Level.DEBUG)){
	       getLogger().debug("[listarTokenUsuario][userID][" + userId + "]");
        }
        
        TokenEntrustTO[] tokenEntrustListTO = new TokenEntrustTO[1];
        try{
            ServicioAdministracionPT postType = this.getServiceADM();
			UserTokenListRequest userTokenListRequestR = new UserTokenListRequest();
			userTokenListRequestR.setUserId(userId);
            
            UserTokenListResponse respuestaServicio = new UserTokenListResponse();
			AuthorizationPolicy politicaSeguridad = new AuthorizationPolicy();
			
			String usuario = UsuarioFuseEntrustUyP.getInstancia().getUsuarioAplicativoId();
			String pass    = UsuarioFuseEntrustUyP.getInstancia().getClaveUsuarioAplicativo();
			
			politicaSeguridad.setUserName(usuario);
			politicaSeguridad.setPassword(pass);
			
			respuestaServicio = postType.listarTokenUsuario(userTokenListRequestR, politicaSeguridad);
			
            if(getLogger().isEnabledFor(Level.DEBUG)){
    			getLogger().debug("[listarTokenUsuario]["+respuestaServicio+"] respuesta servicio");
    		}
            
            tokenEntrustListTO = new TokenEntrustTO[respuestaServicio.getTokeList().length];
            
			if (respuestaServicio!= null && respuestaServicio.getResponseStatus().getCode() == 0) {
            	int i=0;
            	for(; i< respuestaServicio.getTokeList().length; i++) {
            		tokenEntrustListTO[i] = new TokenEntrustTO(); 
            		tokenEntrustListTO[i].setSerialNumber(String.valueOf(respuestaServicio.getTokeList()[i].getSerialNumber()));
            		tokenEntrustListTO[i].setTipoDispositivo(respuestaServicio.getTokeList()[i].getTokenType());
            		tokenEntrustListTO[i].setEstado(respuestaServicio.getTokeList()[i].getState());
            		tokenEntrustListTO[i].setGrupoToken(respuestaServicio.getTokeList()[i].getTokenGroup());
            		tokenEntrustListTO[i].setNombreDispositivo(respuestaServicio.getTokeList()[i].getName());
            		tokenEntrustListTO[i].setEstadoActivacion(respuestaServicio.getTokeList()[i].getActivationState());
            		tokenEntrustListTO[i].setPlataformaMovil(respuestaServicio.getTokeList()[i].getMobilePlatform());
            		tokenEntrustListTO[i].setFechaDeRegistro(respuestaServicio.getTokeList()[i].getRegistrationDate() != null 
            				? respuestaServicio.getTokeList()[i].getRegistrationDate().getTime() : null);
            	}
            }
            else{
            	tokenEntrustListTO[0].setCodigoRespuestaWs(respuestaServicio.getResponseStatus().getCode());
            	tokenEntrustListTO[0].setDescripcionRespuestaWs(String.valueOf(this.obtenerDescripcionErrorEntrust(respuestaServicio.getResponseStatus().getCode())));
            }
            
            
        } catch (Exception e){
        	 if(getLogger().isEnabledFor(Level.ERROR)){
                 getLogger().error("[listarTokenUsuario][Exception][BCI_FINEX]["+userId+"]" ,e);
             }
        }
        if(getLogger().isEnabledFor(Level.INFO)){
            getLogger().info("[listarTokenUsuario][BCI_FINOK]");
        }
        return tokenEntrustListTO;
    }
    
    
    /**
     * M�todo que obtiene el RUT de cliente BCI en formato Entrust.
     * <p>
     * Registro de versiones: 
     * <ul>
     * <li>1.0 10/07/2017 Sergio Flores (SEnTRA) - Ricardo Carrasco (Ing. Soft. BCI): Version inicial.</li>
     * </ul>
     * <p> 
     * @param parametroEstrategiaSegundaClave datos del cliente.
     * @return RUT de cliente BCI con formato Entrust.
     * @since 2.0
     */
	private String obtenerRutClienteEntrust(ParametrosEstrategiaSegundaClaveTO parametroEstrategiaSegundaClave) {
		String userId = null; 
    	String dv = null;
    	
    	if (parametroEstrategiaSegundaClave.getCliente() != null) {
    		userId = String.valueOf(parametroEstrategiaSegundaClave.getCliente().getRut());
    		dv = String.valueOf(parametroEstrategiaSegundaClave.getCliente().getDigito());
    		String grupoClienteEntrust = TablaValores.getValor(TABLADEPARAMETROS, "pinToken", "desc");
    		userId = grupoClienteEntrust + "/" + userId + "-" + dv;
    		
    	}
    	else if (parametroEstrategiaSegundaClave.getUsuarioSegundaClave() != null) {
    		
    		if (parametroEstrategiaSegundaClave.getUsuarioSegundaClave().getGrupoClienteEntrust() == null) {
    			String grupoClienteEntrust = TablaValores.getValor(TABLADEPARAMETROS, "pinToken", "desc");
    			parametroEstrategiaSegundaClave.getUsuarioSegundaClave().setGrupoClienteEntrust(grupoClienteEntrust);
    		}
    		
    		userId = parametroEstrategiaSegundaClave.getUsuarioSegundaClave().getRutEntrust();
    	}
    	else {
    		if (getLogger().isEnabledFor(Level.ERROR)) {
    			getLogger().error("[consultarEstadoSegundaClave][" + userId + "][BCI_FINEX] ");
    		}
    		throw new IllegalArgumentException("ParametrosEstrategiaSegundaClaveTO invalido");
    	}
		return userId;
	}
    
    /**
     *  Metodo para activar un nuevo soft token del cliente.
     *  <br>
     *  Este servicio llama a <code>userTokenActivation</code> de curus.
     * <p>
     * Registro de versiones: 
     * <ul>
     * <li>1.0 10/07/2017 Sergio Flores (SEnTRA) - Ricardo Carrasco (Ing. Soft. BCI): Version inicial.</li>
     * </ul>
     * <p> 
     * @param parametroEstrategiaSegundaClave datos del cliente.
     * @return EstadoActivacionTokenTO estado del token.
     * @since 1.0
     */
    public EstadoActivacionTokenTO activarSoftTokenUsuario(ParametrosEstrategiaSegundaClaveTO parametroEstrategiaSegundaClave){
         if(getLogger().isEnabledFor(Level.INFO)){
             getLogger().info("[activarSoftTokenUsuario][BCI_INI] [" + parametroEstrategiaSegundaClave +"]");
         }
         
         String userId = obtenerRutClienteEntrust(parametroEstrategiaSegundaClave);
         String qrSize = TablaValores.getValor(TABLADEPARAMETROS, "tamannoqr", "valor");
         
         if(getLogger().isEnabledFor(Level.DEBUG)){
 			getLogger().debug("[activarSoftTokenUsuario] userID: " + userId);
 			getLogger().debug("[activarSoftTokenUsuario] qrSize: " + qrSize);
 		}
         
         EstadoActivacionTokenTO estadoActivacion = new EstadoActivacionTokenTO();
         
         try{
        	 ServicioAdministracionPT postType = this.getServiceADM();
        	 UserTokenActivationRequest userTokenActivationRequest = new UserTokenActivationRequest();
             userTokenActivationRequest.setUserId(userId);
             userTokenActivationRequest.setQrSize(Integer.parseInt(qrSize));
             String nombreCliente = null;
             
             
             if (parametroEstrategiaSegundaClave.getUsuarioSegundaClave() != null 
            		 && parametroEstrategiaSegundaClave.getUsuarioSegundaClave().getNombreCliente() != null) {
            	 nombreCliente = parametroEstrategiaSegundaClave.getUsuarioSegundaClave().getNombreCliente();
            	 
                 if(getLogger().isEnabledFor(Level.DEBUG)) {
          			getLogger().debug("[activarSoftTokenUsuario][getUsuarioSegundaClave()][nombreCliente][" + nombreCliente + "][" + userId + "]");
          		 }
            	 
             }
             else if (parametroEstrategiaSegundaClave.getCliente() != null) {
                 if(getLogger().isEnabledFor(Level.DEBUG)) {
          			getLogger().debug("[activarSoftTokenUsuario][parametroEstrategiaSegundaClave.getCliente() != null][" + userId + "]");
          		 }
            	 
            	 nombreCliente = parametroEstrategiaSegundaClave.getCliente().getFullName();
            	 
                 if(getLogger().isEnabledFor(Level.DEBUG)) {
          			getLogger().debug("[activarSoftTokenUsuario][nombreCliente][parametroEstrategiaSegundaClave.getCliente()][" + nombreCliente + "][" + userId + "]");
          		 }
                 userTokenActivationRequest.setUserName(nombreCliente);
             }
             
             UserTokenActivationResponse respuestaServicio = new UserTokenActivationResponse();
			
			AuthorizationPolicy politicaSeguridad = new AuthorizationPolicy();
			
			politicaSeguridad.setUserName(UsuarioFuseEntrustUyP.getInstancia().getUsuarioAplicativoId());
			politicaSeguridad.setPassword(UsuarioFuseEntrustUyP.getInstancia().getClaveUsuarioAplicativo());
			
			respuestaServicio = postType.activarTokenUsuario(userTokenActivationRequest, politicaSeguridad);
             
             if(getLogger().isEnabledFor(Level.DEBUG)){
     			getLogger().debug("[activarSoftTokenUsuario][" + userId + "] despues de llamar a servicio [" + respuestaServicio + "]");
     		}
             
             if(respuestaServicio.getResponseStatus() != null && respuestaServicio.getResponseStatus().getCode() == 0 ){
             	estadoActivacion.setActivationCode(respuestaServicio.getActivationCode());
             	estadoActivacion.setSerialNumber(respuestaServicio.getSerialNumber());
             	
            	estadoActivacion.setResultado(new ResponseStatusTO());
             	estadoActivacion.getResultado().setCode(respuestaServicio.getResponseStatus().getCode());
             	estadoActivacion.getResultado().setDescription(String.valueOf(this.obtenerDescripcionErrorEntrust(respuestaServicio.getResponseStatus().getCode())));
             	
             }
             else{
            	estadoActivacion.setResultado(new ResponseStatusTO());
             	estadoActivacion.getResultado().setCode(respuestaServicio.getResponseStatus().getCode());
             	estadoActivacion.getResultado().setDescription(String.valueOf(this.obtenerDescripcionErrorEntrust(respuestaServicio.getResponseStatus().getCode())));
             }
             
         } 
         catch (Exception e) {
         	 if(getLogger().isEnabledFor(Level.ERROR)){
                  getLogger().error("[activarSoftTokenUsuario][Exception][BCI_FINEX][" + userId + "]" ,e);
              }

        	 estadoActivacion.setResultado(new ResponseStatusTO());
        	 estadoActivacion.getResultado().setCode(CODIGO_ERROR_GENERICO_ENTRUST);
        	 estadoActivacion.getResultado().setDescription(this.obtenerDescripcionErrorEntrust(CODIGO_ERROR_GENERICO_ENTRUST));
         	 
         }
         
         if(getLogger().isEnabledFor(Level.INFO)){
             getLogger().info("[activarSoftTokenUsuario][BCI_FINOK]");
         }
         return estadoActivacion;
     }
    
    /**
     * Obtiene el Otp gen�rico.
     * <p>
     * 
     * Registro de Versiones:
     * <ul>
     * <li>1.0 10/07/2017 Sergio Flores Leiva (SEnTRA) - Ricardo Carrasco C�ceres (Ing. Soft. BCI): Versi�n Inicial.</li>
     * <li>1.0 10/10/2017 Sergio Flores Leiva (SEnTRA) - Ricardo Carrasco C�ceres (Ing. Soft. BCI): Se agrega username como parametro de entrada
     * a la consulta.</li>
     * </ul>
     * <P>
     * 
     * @param usuarioSegundaClaveTO objeto con todos los datos necesarios. 
	 * @return OtpEntrustTO to con los datos de entrada.
	 * @throws Exception error de servicio.
     * @since 1.0
     */
    public OtpEntrustTO obtenerOTP(UsuarioSegundaClaveTO usuarioSegundaClaveTO) throws Exception{
        if(getLogger().isEnabledFor(Level.INFO)){
            getLogger().info("[obtenerOTP][BCI_INI] [" + usuarioSegundaClaveTO.getRut() + "][" + usuarioSegundaClaveTO.getRutEntrust() + "]");
        }
        
        String userId = usuarioSegundaClaveTO.getRutEntrust();
        OtpEntrustTO otpEntrustTO = new OtpEntrustTO();
        NameValue[] nv = new NameValue[0];
        try {
            ServicioAutenticacionPT postType = this.getService();
            GetOTPChallengeRequest request = new GetOTPChallengeRequest();
            
            GetOTPChallengeResponse respuesta= new GetOTPChallengeResponse();
            
            if (getLogger().isEnabledFor(Level.DEBUG)) {
     			getLogger().debug("[obtenerOTP] userId ["+userId+"], nombre cliente [" + usuarioSegundaClaveTO.getNombreCliente()+"]");
     		}
            
            request.setRepositoryId(null);
			request.setTransactionData(nv);
			request.setUserName(usuarioSegundaClaveTO.getNombreCliente());
            request.setUserId(userId);
            
			cl.bci.www.esb.service.seguridad.autenticacion.segundaclave.types.AuthorizationPolicy politicaSeguridad = 
					new cl.bci.www.esb.service.seguridad.autenticacion.segundaclave.types.AuthorizationPolicy();
            
			politicaSeguridad.setUserName(UsuarioFuseEntrustUyP.getInstancia().getUsuarioAplicativoId());
			politicaSeguridad.setPassword(UsuarioFuseEntrustUyP.getInstancia().getClaveUsuarioAplicativo());
            
			respuesta = postType.obtenerOTPEnrolamiento(request, politicaSeguridad);
            
            if (getLogger().isEnabledFor(Level.DEBUG)) {
     			getLogger().debug("[obtenerOTP][" + usuarioSegundaClaveTO.getRut() + "] respuesta [" + respuesta + "]");
     		}
            
            if (respuesta != null) {
                if (respuesta.getResponseStatus() != null 
                		&& respuesta.getResponseStatus().getCode() == 0) {
                    
                    otpEntrustTO.setUserId(respuesta.getUserId());
                    otpEntrustTO.setGroup(respuesta.getGroup());
                    otpEntrustTO.setOtp(respuesta.getOtp());
                    otpEntrustTO.setCreateDate(respuesta.getCreateDate().getTime());
                    otpEntrustTO.setExpireDate(respuesta.getExpireDate().getTime());
                    otpEntrustTO.setTransactionId(respuesta.getTransactionId());
                    otpEntrustTO.setCodigoRespuestaWs(respuesta.getResponseStatus().getCode());
                    otpEntrustTO.setDescripcionRespuestaWs(this.obtenerDescripcionErrorEntrust(respuesta.getResponseStatus().getCode()));
                }
                else {
                    otpEntrustTO.setCodigoRespuestaWs(CODIGO_ERROR_GENERICO_ENTRUST);
                    otpEntrustTO.setDescripcionRespuestaWs(this.obtenerDescripcionErrorEntrust(CODIGO_ERROR_GENERICO_ENTRUST));
                }
            }
     		}
        catch (Exception e) {
        	if(getLogger().isEnabledFor(Level.ERROR)){	
                getLogger().error("[obtenerOTP][Exception][BCI_FINEX][" + usuarioSegundaClaveTO.getRut() + "]" + e.getMessage(), e);
            }
            otpEntrustTO.setCodigoRespuestaWs(CODIGO_ERROR_GENERICO_ENTRUST);
            otpEntrustTO.setDescripcionRespuestaWs(this.obtenerDescripcionErrorEntrust(CODIGO_ERROR_GENERICO_ENTRUST));
        }
        
        if(getLogger().isEnabledFor(Level.INFO)){
            getLogger().info("[obtenerOTP][BCI_FINOK][" + usuarioSegundaClaveTO.getRut() + "]");
        }
        return otpEntrustTO;
    }

    /**
     * Genera un String con un c�digo QR, basandose en un arreglo de byte.
     * <p>
     * 
     * Registro de Versiones:
     * <ul>
     * <li>1.0 10/07/2017 Luis L�pez Alamos (SEnTRA) - Ricardo Carrasco C�ceres (Ing. Soft. BCI): Versi�n Inicial.</li>
     * </ul>
     * <P>
     * 
     * @param imagenQR objeto con todos los datos necesarios. 
     * @return sb en formato string decodificado 
     * @since 2.0
     */
    private String convierteByteArrayAQr(byte[] imagenQR){
        String valorLlave = new String(new Base64().encode(imagenQR));
        StringBuffer sb = new StringBuffer();
        sb.append(TablaValores.getValor(TABLADEPARAMETROS, "formatoQR", DESC));
        sb.append(';');
        sb.append(TablaValores.getValor(TABLADEPARAMETROS, "base64COD", DESC));
        sb.append(valorLlave);
        return sb.toString();
    }

    /**
     * Valida que el token usado para autenticar se encuentre en estado Activo 'CURRENT'.
     * <p>
     * 
     * Registro de Versiones:
     * <ul>
     * <li>1.0 10/07/2017 Luis L�pez Alamos (SEnTRA) - Ricardo Carrasco C�ceres (Ing. Soft. BCI): Versi�n Inicial.</li>
     * </ul>
     * <P>
     * @param parametroEstrategiaSegundaClave objeto con todos los datos necesarios para la estrategia de autenticaci�n utilizada. 
     * @return estadoDispositivo si se encuentra activo para realizar una autorizaci�n.
     * @throws Exception en caso de algun valor inv�lido. 
     * @since 2.0
     */
    private boolean validarDisponibilidadDispositivo(ParametrosEstrategiaSegundaClaveTO parametroEstrategiaSegundaClave) throws Exception{
        if(getLogger().isEnabledFor(Level.INFO)){
            getLogger().info("[validarDisponibilidadDispositivo][BCI_INI] tipoSegundaClave " + parametroEstrategiaSegundaClave.getTipoSegundaClave());
        }
        String tipoAConsultar = "";
        boolean estadoDispositivo = false; 
        
        
        if (OTP.equalsIgnoreCase(parametroEstrategiaSegundaClave.getTipoSegundaClave())){
            if(getLogger().isEnabledFor(Level.INFO)){
                getLogger().info("[validarDisponibilidadDispositivo][BCI_FINOK][No hay validacion para OTP, retorna true]");
            }
            return true;
        }
        
        String tipoDispositivo = parametroEstrategiaSegundaClave.getTipoSegundaClave();
        String estadoCurrent = TablaValores.getValor(TABLADEPARAMETROS, "CURRENT", "valor");

        if (tipoDispositivo.equalsIgnoreCase(TablaValores.getValor(TABLADEPARAMETROS, "modoEntrustFisico", "desc"))){
            tipoAConsultar = TablaValores.getValor(TABLADEPARAMETROS, "modoEntrustFisico", "corto");
        }
        else {
            tipoAConsultar = TablaValores.getValor(TABLADEPARAMETROS, "SOFTTOKEN", "valor");
        }
        
        if(getLogger().isEnabledFor(Level.DEBUG)){
            getLogger().debug("[validarDisponibilidadDispositivo] tipo a Consultar " + tipoAConsultar);
        }
        
        TokenEntrustTO[] tokensActivos = this.listarTokenUsuario(parametroEstrategiaSegundaClave);
        if (tokensActivos != null && tipoAConsultar != null && !tipoAConsultar.equalsIgnoreCase("")){
            for (int i=0; i < tokensActivos.length; i++){
                if (tokensActivos[i].getTipoDispositivo().equalsIgnoreCase(tipoAConsultar) && tokensActivos[i].getEstado().equalsIgnoreCase(estadoCurrent)){
                    if(getLogger().isEnabledFor(Level.INFO)){
                        getLogger().info("[validarDisponibilidadDispositivo] Se encuentra match con dispositivo " + tokensActivos[i].toString());
                    }
                    estadoDispositivo = true;
                    break;
                }
            }
        }
        else {
            getLogger().info("[validarDisponibilidadDispositivo] Hubo un problema al leer datos de tabla de parametros o al intentar leer tokens de usuario");
            throw new Exception(obtenerDescripcionErrorEntrust(CODIGO_ERROR_GENERICO_ENTRUST));
        }
        if(getLogger().isEnabledFor(Level.INFO)){
            getLogger().info("[validarDisponibilidadDispositivo][BCI_FINOK] Estado de dispositivo : " + estadoDispositivo);
        }
        return estadoDispositivo;
    }


	/**
	 * Método que obtiene el mapa de campos destacados, a eliminar y a formatear para ser enviados a entrust como desafio y
	 * autenticación de desafío.<br><br>
	 * 
	 * Estos String son obtenidos desde la tabla entrust.parametros.<br>
	 * 
	 * <p>
	 *    Registro de Versiones:
	 * <ul>
	 * <li>1.0 11/07/2017 Jaime Suazo (SEnTRA) - Ricardo Carrasco Cáceres (Ing. Soft. BCI): Versión Inicial.</li>
	 * </ul>
	 * <P>
	 * 
	 * @param tipoCamposRequerido String con formato monto|nombre;, monto,moneda|saldo,moneda;  
	 * @return Mapa con los que según la nacesidad se destaquen, eliminen o formateen.
	 */
	private Map obtenerMapaDeCampos(String tipoCamposRequerido) {
		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[obtenerMapaDeCampos][BCI_INI][tipoCampoRequerido][" + tipoCamposRequerido + "]");
		}
		Map resultado = null;
		
		if (tipoCamposRequerido != null && !tipoCamposRequerido.trim().equals("")) {
			StringTokenizer listaTokens = new StringTokenizer(tipoCamposRequerido, ":");
			String [] arregloFormato = null;
			
			if (listaTokens.countTokens() > LARGO_CERO) {
				
				String campoPaso = null;
				resultado = new HashMap();
				
				for (; listaTokens.hasMoreTokens(); ) {
					campoPaso = listaTokens.nextToken();
					
					if (campoPaso != null && campoPaso.indexOf(SEPARADOR_CAMPO_TIPO_FORMATO) > 0) {
						arregloFormato = StringUtil.divide(campoPaso, SEPARADOR_CAMPO_TIPO_FORMATO);
						
						if (arregloFormato != null && arregloFormato.length == LARGO_ARREGLO_PAR 
								&& arregloFormato[POSICION_ARREGLO_CERO] != null && arregloFormato[POSICION_ARREGLO_UNO] != null) {
							resultado.put(arregloFormato[0], arregloFormato[1]);	
						}
					}
					else {
						resultado.put(campoPaso, campoPaso);
					}
				}
			}
		}
		
		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[obtenerMapaDeCampos][BCI_FINOK]");
		}
		
		return resultado;
	}
}
