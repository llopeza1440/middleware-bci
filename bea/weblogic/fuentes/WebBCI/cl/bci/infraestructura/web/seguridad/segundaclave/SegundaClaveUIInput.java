package cl.bci.infraestructura.web.seguridad.segundaclave;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;
import java.util.Random;

import javax.el.ValueExpression;
import javax.faces.component.FacesComponent;
import javax.faces.component.NamingContainer;
import javax.faces.component.UIComponent;
import javax.faces.component.UIGraphic;
import javax.faces.component.UIInput;
import javax.faces.component.UINamingContainer;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import wcorp.model.seguridad.EstrategiaAutenticacionFactory;
import wcorp.model.seguridad.EstrategiaSegundaClave;
import wcorp.model.seguridad.ParametrosEstrategiaSegundaClaveTO;
import wcorp.model.seguridad.RepresentacionAlfanumericaSegundaClaveTO;
import wcorp.model.seguridad.ResultadoOperacionSegundaClaveTO;
import wcorp.serv.seguridad.SeguridadException;
import wcorp.util.ErroresUtil;
import wcorp.util.StringUtil;
import wcorp.util.TablaValores;
import wcorp.util.journal.Eventos;

import cl.bci.infraestructura.web.journal.mb.JournalUtilityMB;
import cl.bci.infraestructura.web.seguridad.segundaclave.mb.FirmaDigitalModelMB;
import cl.bci.infraestructura.web.seguridad.segundaclave.mb.JerarquiaClavesModelMB;
import cl.bci.infraestructura.web.seguridad.segundaclave.to.CamposDeLlaveTO;
import cl.bci.infraestructura.web.seguridad.segundaclave.to.EstadoSegundaClaveTO;
import cl.bci.infraestructura.web.seguridad.segundaclave.to.ItemDeLlaveTO;
import cl.bci.infraestructura.web.seguridad.segundaclave.to.LlaveSegundaClaveTO;
import cl.bci.infraestructura.web.seguridad.segundaclave.to.SegundaClaveEnum;
import cl.bci.infraestructura.web.seguridad.segundaclave.to.UsuarioSegundaClaveTO;
import cl.bci.infraestructura.web.seguridad.util.ConectorStruts;

/**
 * Este componente invoca SafeSigner para una transaccion dada, dibuja el QR y
 * acepta el codigo de confirmacion ingresado por el usuario, para invocar
 * nuevamente SafeSigner y validar el codigo.
 * 
 * Registro de versiones:
 * <ul>
 * <li>1.0 (04/05/2013 Mauricio Palma. (Orand S.A.)): versi�n inicial.</li>
 * <li>1.1 (24/09/2013 Victor Hugo Enero. (Orand S.A.)): Se agrega validacion de render para agregar los 
 * validators</li>
 * <li>1.2 (03/10/2013 Victor Hugo Enero. (Orand S.A.)): Se agrega al parametrosEstrategiaSegundaClaveTO el
 * atributo camposDellavesTO y el nombre del servicio antes de llamar al metodo de autenticar.
 * <li>1.3 (30/10/2013 Victor Hugo Enero. (Orand S.A.)): Se agrega validacion en {@link #decode()} para
 * manejar el caso que estado de segunda clave sea nulo y en el metodo {@link #getSegundaClaveId())} se
 * cambio la llamada del getEstadoSegundaClave por el atributo correspondiente (estadoSegundaClave)
 * <li>1.4 (22/11/2013 Victor Hugo Enero. (Orand S.A.)): Se corrige problema de doble click
 * <li>1.5 21/11/2014, Pablo Romero C�ceres (SEnTRA): Se corrige problema con onclick en el boton retorno 
 *                                                      cuando adem�s tiene action.
 * <li>1.6 11/08/2015 Luis Silva (TINet): Se realizan modificaciones con el fin de que se pueda crear una
 * subclase de esta clase, esto dentro del contexto de implementar safesigner en los servicios REST. Producto
 * de esto:<ul>
 * <li>Los atributos {@link #servicio}, {@link #usuarioSegundaClave}, {@link #estadoSegundaClave},
 * {@link #camposDeLlave}, {@link #jerarquiaClaves} y {@link #codeConfirmation} pasan de privados a
 * protegidos</li>
 * <li>Se crea m�todo {@link #agregarDatosFirmaEToken(ParametrosEstrategiaSegundaClaveTO)}</li>
 * <li>2.0 10/07/2017 Luis Lopez Alamos (SEnTRA) - Ricardo Carrasco C�ceres (Ing. Soft. BCI):
 *  Se modifican m�todos {@link #restoreState(FacesContext, Object)}, {@link #encodeBegin(FacesContext)}} y {@link #journalizar(ParametrosEstrategiaSegundaClaveTO, boolean, boolean)}
 *  Se agregan las constantes {@link #TABLA_PARAMETROS_JOURNAL}, {@link #TABLA_PARAMETROS_ENTRUST}, {@link #DESC}, {@link #ERROR_DE_ARGUMENTO} , {@link #FORWARD_IP}, {@link #TEXTO_DESCONOCIDO}, 
 *  {@link #ONLINE}, {@link #OFFLINE}, {@link #DESC}, {@link #TOKEN_ENTRUST} , {@link #SOFT_TOKEN_ENTRUST} y {@link #ERROR}.
 *  Se elimina la constante alusiva a safesigner.parametros y se reemplaza por la {@link #TABLA_PARAMETROS_JOURNAL} con la finalidad de centralizar codigos journal.
 *  Se agregan los atributos {@link #frecuenciaEntrust}, {@link #timeOutEntrust}, {@link #tipoOperacionEntrust}, {@link #llaveQR}, {@link #fragmentManejaError},
 *  {@link #llavesQREntrust}  y {@link #glosaErrorEntrust} 
 *  Se agregan los m�todos {@link #generarLlaveQR()}, {@link #getFrecuenciaEntrust()}, {@link #getTimeOutEntrust()}, {@link #resuelveIPEjecucion()}, {@link #resuelveResultado()()} y {@link #esEntrust()()} por proyecto Entrust.
 *  </li>
 *  <li>2.1 11/07/2017 Jaime Suazo D. (SEnTRA) - Ricardo Carrasco C�ceres (Ing. Soft. BCI): En m�todo {@link #resuelveIPEjecucion()} 
 *          Se corrige armado de URL REST para consulta del Pooling entrust en producci�n.</li> 
 *  <li>2.2 11/07/2017 Jaime Suazo D. (SEnTRA) - Ricardo Carrasco C�ceres (Ing. Soft. BCI): En m�todo {@link #resuelveResultado()}
 *          se agrega captura de parametro para despliegue de mensaje de estado de consulta de pooling.</li> 
 * </ul>
 * 
 * </li>
 * </ul>
 * <p>
 * <b>Todos los derechos reservados por Banco de Cr�dito e Inversiones.</b>
 * </p>
 */
@FacesComponent("cl.bci.infraestructura.web.seguridad.segundaclave.SegundaClaveUIInput")
public class SegundaClaveUIInput extends UIInput implements NamingContainer {

    /**
     * Logger de la clase.
     */
    private static Logger logger = (Logger) Logger.getLogger(SegundaClaveUIInput.class);

    /**
     * Cero.
     */
    private static final int ELEMENTO_CERO = 0;

    /**
     * Uno.
     */
    private static final int ELEMENTO_UNO = 1;

    /**
     * Dos.
     */
    private static final int ELEMENTO_DOS = 2;

    /**
     * Tres.
     */
    private static final int ELEMENTO_TRES = 3;

    /**
     * Cuatro.
     */
    private static final int ELEMENTO_CUATRO = 4;

    /**
     * Cinco.
     */
    private static final int ELEMENTO_CINCO = 5;

    /**
     * Seis.
     */
    private static final int ELEMENTO_SEIS = 6;

    /**
     * Siete.
     */
    private static final int ELEMENTO_SIETE = 7;

    /**
     * Ocho.
     */
    private static final int ELEMENTO_OCHO = 8;

    /**
     * Nueve.
     */
    private static final int ELEMENTO_NUEVE = 9;

    /**
     * Diez.
     */
    private static final int ELEMENTO_DIEZ = 10;

    /**
     * Once.
     */
    private static final int ELEMENTO_ONCE = 11;

    /**
     * Doce.
     */
    private static final int ELEMENTO_DOCE = 12;

    /**
     * Trece.
     */
    private static final int ELEMENTO_TRECE = 13;
    
    /**
     * Catorce.
     */
    private static final int ELEMENTO_CATORCE = 14;

    /**
     * Cantidad de paramatros.
     */
    private static final int SIZE = 15;

    /**
     * AUTENTICACION OK.
     */
    private static final int AUTENTICACION_OK = 0;

    /**
     * tabla de par�metros de seguridad.
     */
    private static final String TABLASEGURIDAD = "Seguridad.parametros";

    /**
     * tabla de par�metros a utilizar.
     */
    private static final String TABLA_PARAMETROS_JOURNAL = "SegundaClave.parametros";
    
    
    /**
     * tabla de par�metros de entrust.
     */
    private static final String TABLA_PARAMETROS_ENTRUST = "entrust.parametros";
    
    /**
     * Constante utilizada para definir descripci�n de valores de llaves desde tablas de parametros. 
     */
    private static final String DESC = "desc";
    
    /**
     * Constante utilizada para definir descripci�n de un error por argumento no v�lido. 
     */
    private static final String ERROR_DE_ARGUMENTO = "413";
    
    /**
     * tipo de segunda clave para SoftToken Entrust.
     */
    private static final String ONLINE = TablaValores.getValor(TABLA_PARAMETROS_ENTRUST, "modoOnline", DESC);
    
    /**
     * tipo de segunda clave para SoftToken Entrust.
     */
    private static final String OFFLINE = TablaValores.getValor(TABLA_PARAMETROS_ENTRUST, "modoOffline", DESC);
    
    /**
     * Tipo de Segunda Clave Softtoken Entrust.
     */
    private static final String SOFT_TOKEN_ENTRUST = TablaValores.getValor(TABLA_PARAMETROS_ENTRUST, "nombreSoftToken", DESC);
    
    /**
     * Tipo de Segunda Clave Token Entrust.
     */
    private static final String TOKEN_ENTRUST = TablaValores.getValor(TABLA_PARAMETROS_ENTRUST, "nombreToken", DESC);
    
    /**
     * Constante que representa el modo ERROR de entrust, se usa para manejar visualmente el componente de autenticaci�n.
     */
    private static final String ERROR = TablaValores.getValor(TABLA_PARAMETROS_ENTRUST, "modoEntrustError", DESC);
    
    /**
     * Identificador de posibles errores que se produzcan con entrust.
     */
    private String glosaErrorEntrust = TablaValores.getValor(TABLA_PARAMETROS_ENTRUST, "errorGenerico", DESC);
    
    /**
     * Variable para concatenar la url de un dominio.
     */
    private static final String FORWARD_IP = "://";
    
    /**
     * Atributo inmutable (sin seter) que sirve para tener una frecuencia de consulta desde la capa web hacia servicios entrust.
     */
    private String frecuenciaEntrust; 
    
    
    /**
     * Atriuto inmutable (sin seter) que sirve para obtener el tiempo maximo de espera para confirmar una transacci�n.
     */
    private String timeOutEntrust;
    

    /**
     * Servicio especificado en la pagina.
     */
    protected String servicio;
    
    /**
     * Identificador de la transaccion para entrust.
     */
    private String tipoOperacionEntrust;
    
    /**
     * Identificador de la llave qr por defecto para un desafio OFFLINE.
     */
    private String llaveQR;
    
    /**
     * Identificador de la transaccion para entrust.
     */
    private boolean fragmentManejaError;
    
    /**
     * Atributo inmutable con identificador del Host donde se ejecuta la aplicaci�n web.
     */
    private String urlConsultaPoolingEntrust;
    
    public String getUrlConsultaPoolingEntrust(){
        return this.urlConsultaPoolingEntrust;
    }
    
    /**
     * LLaves Entrust QR.
     */
    
    private LlaveSegundaClaveTO llavesQREntrust;
    
    
    public LlaveSegundaClaveTO getLlaveQREntrust(){
        return llavesQREntrust;
    }

    /**
     * Usuario conectado a un dominio.
     */
    protected UsuarioSegundaClaveTO usuarioSegundaClave;

    /**
     * El estado de segunda clave para este usuario, canal y servicio.
     */
    protected EstadoSegundaClaveTO estadoSegundaClave;

    /**
     * Los campos de llave especificados en la pagina.
     */
    protected CamposDeLlaveTO camposDeLlave;

    /**
     * Codigo de validacion de Segunda Clave.
     */
    protected String codeConfirmation = null;

    /**
     * Jerarquia de claves.
     */
    protected JerarquiaClavesModelMB jerarquiaClaves = null;

    /**
     * Jerarquia de claves.
     */
    protected FirmaDigitalModelMB firmaDigitalModelMB = null;

    /**
     * Permite inicializar una vez durante el ciclo de vida del componente.
     */
    private boolean isInitialized = false;

    /**
     * Layout especificado en la pagina.
     */
    private String layout;

    /**
     * True si el browser soporta imagenes inlined en base64.
     */
    private boolean renderImageToBinary = true;

    /**
     * True si se pinta la segunda clave.
     */
    private Boolean render = true;

    /**
     * Se guarda para remover antes de saveState.
     */
    private Validator segundaClaveValidator = null;

    /**
     * Tipo de boton de confirmar.
     */
    private String tipoBotonDestino;

    /**
     * Tipo de boton de retorno.
     */
    private String tipoBotonRetorno;

    /**
     * Parametro Onclick destinoButtonOnclick.
     */
    private String destinoButtonOnclick;
    
    /**
     * Parametro Onclick destinoButtonRetornoOnclick.
     */
    private String destinoButtonRetornoOnclick;
    
    /**
     * Constructor de la clase.
     */
    public SegundaClaveUIInput() {
        logger.debug("[SegundaClaveUIInput] Constructor de la clase");
        /*
         * Inicializaciones estaticas.
         */
        setRendererType(null); // this component renders itself
        setRequired(false);
    }

    /**
     * Se debe invocar desde encodeBegin. getAttributes no funciona desde el
     * constructor. Se necesitan los valores de los atributos para invocar
     * factories.
     * 
     * @param context
     * contexto.
     * @throws SeguridadException
     * excepci�n de seguridad.
     */
    private void initialize(FacesContext context) throws SeguridadException {
        logger.debug("[initialize] Inicio");

        /*
         * Inicializaciones dinamicas.
         */

        // habilitar Component Binding
        // ver seccion 3.1.5 "Component Bindings", JSF 2.1 Spec
        String binding = "#{" + getAttributes().get("bindingComponent").toString() + "}";
        logger.debug("[initialize] binding ");
        ValueExpression bindingValueExpression = context.getApplication().getExpressionFactory()
        .createValueExpression(context.getELContext(), binding, SegundaClaveUIInput.class);
        logger.debug("[initialize] Antes de setValueExpression ");
        setValueExpression("binding", bindingValueExpression);


        if (getAttributes().get("destinoButtonAction") == null
                && getAttributes().get("destinoButtonOnclick") == null) {
            logger.error("[initialize] destinoButtonAction y  destinoButtonOnclick no pueden ser ambos nulos");
            throw new IllegalArgumentException(
            "destinoButtonAction y  destinoButtonOnclick no pueden ser ambos nulos");
        }
        
        if (getAttributes().get("textoButtonRetorno") != null
                && getAttributes().get("destinoButtonRetorno") == null
                && getAttributes().get("destinoButtonRetornoOnclick") == null) {
            logger.error("[initialize] destinoButtonRetorno y destinoButtonRetornoOnclick"
                    +" no pueden ser ambos nulos");
            throw new IllegalArgumentException(
            "destinoButtonRetorno y  destinoButtonRetornoOnclick no pueden ser ambos nulos");
        }
        
        //Se cheque el tipo de boton de confirmacion
        if (getAttributes().get("destinoButtonAction") != null){
            this.tipoBotonDestino = "submit";
            destinoButtonOnclick = (String) getAttributes().get("destinoButtonOnclick");
            if (destinoButtonOnclick==null)
                destinoButtonOnclick ="";
            destinoButtonOnclick += ";setTimeout('document.getElementById(\\'' + this.id + '\\').disabled=true;', 50);";
        }
        else {
            this.tipoBotonDestino = "button";
            destinoButtonOnclick = (String) getAttributes().get("destinoButtonOnclick");
            destinoButtonOnclick += ";this.disabled=true";
        }

        //Se cheque el tipo de boton de retorno
        if (getAttributes().get("textoButtonRetorno") != null){
            if (getAttributes().get("destinoButtonRetorno") != null){
                this.tipoBotonRetorno = "submit";
                destinoButtonRetornoOnclick = (String) getAttributes().get("destinoButtonRetornoOnclick");
                if (destinoButtonRetornoOnclick==null)
                    destinoButtonRetornoOnclick ="";
                destinoButtonRetornoOnclick+=";setTimeout('document.getElementById(\\'' + this.id + '\\').disabled=true;', 50);";
            }
            else {
                this.tipoBotonRetorno = "button";
                destinoButtonRetornoOnclick = (String) getAttributes().get("destinoButtonRetornoOnclick");
                destinoButtonRetornoOnclick += ";this.disabled=true";
            }
        }

        // Se obtiene el render
        Object oRender = getAttributes().get("render");
        if (oRender == null) {
            render = new Boolean(true);
        }
        else {
            render = (new Boolean(oRender.toString()));
        }

        if (!render.booleanValue()) {
            if (logger.isDebugEnabled()) {
                logger.debug("[initialize] No se tiene que pedir segunda clave");
                logger.debug("[initialize] Fin");
            }
            return;
        }

        isInitialized = true;
        jerarquiaClaves = (JerarquiaClavesModelMB) ConectorStruts.getAttribHttp("jerarquiaClavesModelMB");
        if (jerarquiaClaves == null) {
            jerarquiaClaves = new JerarquiaClavesModelMB();
        }
        
        // obtener cliente y canal del ambiente BCI
        usuarioSegundaClave = SegundaClaveUtil.getUsuarioSegundaClave();
        // obtiene el servicio (operacion) que se firmara
        servicio = getAttributes().get("servicio").toString();
        // obtener estado segunda clave, ver diseño seccion 5.4.1
        // se agregan a EstadoSegundaClaveTO el cliente, canal y servicio como atributos
        estadoSegundaClave = jerarquiaClaves.estadoSegundaClave(servicio, usuarioSegundaClave);

        Object oLayout = getAttributes().get("layout");
        if (oLayout == null)
            layout = "default";
        else layout = oLayout.toString();

        // obtiene los datos para generar llave

        if (estadoSegundaClave.isEstatus() && estadoSegundaClave.getSegundaClaveEnum().isRequiereLlave()) {
            camposDeLlave = getCamposDeLlave();
            
            // verificar browser porque IE 7 y 8 no soportan imagenes inlined en base 64
            ExternalContext externalContext = context.getExternalContext();
            String userAgent = externalContext.getRequestHeaderMap().get("User-Agent");
            renderImageToBinary = userAgent.contains("; MSIE 4") || userAgent.contains("; MSIE 5")
            || userAgent.contains("; MSIE 6") || userAgent.contains("; MSIE 7")
            || userAgent.contains("; MSIE 8");
            /*
             * Obtener validator para la segunda clave que corresponda
             */
            logger.debug("[initialize] Se obtiene el segundaClaveValidator");
            segundaClaveValidator = SegundaClaveValidatorFactory.crear(estadoSegundaClave);
            addValidator(segundaClaveValidator);

            if (logger.isDebugEnabled()) {
                logger.debug("[initialize] Tipo de segunda clave: "
                        + estadoSegundaClave.getSegundaClaveEnum().getNombre());
                logger.debug("[initialize] Servicio: " + servicio + " Layout: " + layout
                        + " renderImageToBinary: " + renderImageToBinary);
            }
        }

        logger.debug("[initialize] Fin");
    }

    /**
     * Este metodo y valor de retorno es exigido por la spec JSF para usar
     * composite components backed por UIComponent en java.
     * 
     * @return String.
     */
    @Override
    public String getFamily() {
        return UINamingContainer.COMPONENT_FAMILY;
    }

    /**
     * Los validators requieren acceso al objeto estadoSegundaClave.
     * 
     * @return objeto estadoSegundaClave asociado a este usuario, dominio, servicio.
     */
    public EstadoSegundaClaveTO getEstadoSegundaClave() {
        try {
            if (!isInitialized) {
                initialize(this.getFacesContext());
            }
            return estadoSegundaClave;
        }
        catch (SeguridadException seguridadException) {
            if (logger.isEnabledFor(Level.ERROR)) {
                logger.error("[initialize] No es posible inicializar SegundaClaveUIInput: "
                        + ErroresUtil.extraeStackTrace(seguridadException));
            }
            throw new IllegalStateException("No es posible inicializar SegundaClaveUIInput: "
                    + seguridadException.getMessage(), seguridadException);
        }
    }

    /**
     * Obtiene usuario de segunda clave.
     * 
     * @return UsuarioSegundaClaveTO usuario.
     */
    public UsuarioSegundaClaveTO getUsuarioSegundaClave() {
        try {
            if (!isInitialized) {
                initialize(this.getFacesContext());
            }
            return usuarioSegundaClave;
        }
        catch (SeguridadException seguridadException) {
            if (logger.isEnabledFor(Level.ERROR)) {
                logger.error("[initialize] No es posible inicializar SegundaClaveUIInput: "
                        + ErroresUtil.extraeStackTrace(seguridadException));
            }
            throw new IllegalStateException("No es posible inicializar SegundaClaveUIInput: "
                    + seguridadException.getMessage(), seguridadException);
        }
    }

    /**
     * El composite component requiere acceso a la segunda clave que se debe
     * aplicar para seleccionar el layout. .
     * 
     * @return el tipo de segunda clave que se debe requerir al usuario.
     */
    public SegundaClaveEnum getSegundaClaveId() {
        if (estadoSegundaClave != null)
            return estadoSegundaClave.getSegundaClaveEnum();

        return null;
    }

    /**
     * Lee los datos genericos de la operacion, los cuales deben ser firmados.
     * 
     * @return los datos en un objeto CamposDeLlaveTO
     */
    private CamposDeLlaveTO getCamposDeLlave() {

        if (logger.isDebugEnabled()) {
            logger.debug("[getCamposDeLlave] Inicio");
        }

        CamposDeLlaveTO campos = new CamposDeLlaveTO();
        int posicion = 0;

        List<UIComponent> children = getChildren();

        for (UIComponent child : children) {
            if (child instanceof SegundaClaveUIData) {
                SegundaClaveUIData dato = (SegundaClaveUIData) child;

                String nombre = dato.getAttributes().get("nombre").toString();
                String valor = dato.getAttributes().get("valor").toString().trim();
                ItemDeLlaveTO item = new ItemDeLlaveTO();
                item.setPosicion(posicion);
                item.setNombre(nombre);
                item.setValorString(valor);
                campos.agregarCampo(item);
                posicion++;
                if (logger.isDebugEnabled()) {
                    logger.debug("[getCamposDeLlave] addMember(" + nombre + ", " + valor + ")");
                }

            }
        }

        if (logger.isDebugEnabled()) {
            logger.debug("[getCamposDeLlave] Fin");
        }

        return campos;
    }

    /**
     * El encoding esta definido en la implementacion del composite component.
     * Este metodo solo debe dibujar la llave cuando corresponda.
     * 
     * <p>
     * Registro de Versiones:
     * <ul>
     * <li>1.0 ??/??/?? Desconocido : Versi�n Inicial.</li>
     * <li>2.0 10/07/2017 Luis L�pez Alamos (SEnTRA) - Ricardo Carrasco C�ceres (Ing. Soft. BCI): Se modifica llamada a jerarquia de Claves,
     * cuando la segunda clave que entra requiere llave se setea por defecto la constante ONLINE, la cual solamente es utilizada por EntrustSoftToken,
     * internamente en el componente de jerarquia se hace la validaci�n pertinente para los demas tipos de claves.
     * Adem�s, se modifica el atributo local valorLLave. El cual primeramente se crea con un valor vac�o y se omite para cualquier jerarquia ENTRUST
     * La l�gica particular de EntrustSoftToken Online pasa por la condici�n else del estatusLLave.
     * Cuando el componente es renderizado solamente desde un fragmento EntrustSoftToken Offline entra en el primer bloque, el cual determina el nuevo despacho.</li> 
     * 
     * </ul>
     * </p>
     * @param context contexto.
     * @throws IOException excepci�n de entrada/salida.
     * @since 1.0
     */
    @Override
    public void encodeBegin(FacesContext context) throws IOException {

        if (logger.isEnabledFor(Level.INFO)){
            logger.info("[encodeBegin] [BCI_INI]");
        }
        if (((this.estadoSegundaClave!=null) && (this.estadoSegundaClave.getSegundaClaveEnum().getNombre()).equalsIgnoreCase("EntrustSoftToken")
                && (getUsuarioSegundaClave().getGrupoClienteEntrust().equalsIgnoreCase(OFFLINE)))){
        
        try{
            this.generarLlaveQR();
            super.encodeBegin(context);
			if (logger.isEnabledFor(Level.INFO)){
				logger.info("[encodeBegin] [BCI_FINOK]");
            }
            return;
        }
        catch (Exception e){
			if (logger.isEnabledFor(Level.WARN) ) { 
            	logger.warn("[encodeBegin][Exception]" + "Error al generar llave: " + e.getMessage(), e);
            }
        }
        
        }
        try {
            if (!isInitialized) {
                initialize(context);
            }

            if (logger.isEnabledFor(Level.DEBUG)) {
                logger.debug("[encodeBegin] render.booleanValue() = " + render.booleanValue());
            }

            // no toda segunda clave requiere llave
            if (render.booleanValue()) {
                if (renderImageToBinary) {
                    if (logger.isEnabledFor(Level.DEBUG)) {
                        logger.debug("[encodeBegin] Borrando la llave");
                    }
                    ConectorStruts.removeAttribHttp("imagenqr");
                }
                if (getEstadoSegundaClave().isEstatus()
                        && getEstadoSegundaClave().getSegundaClaveEnum().isRequiereLlave()) {
                    camposDeLlave = getCamposDeLlave();

                    if (camposDeLlave.getNumeroElementos()==0 && 
                        SegundaClaveEnum.SAFESIGNER.equals(estadoSegundaClave.getSegundaClaveEnum())){
						if (logger.isEnabledFor(Level.ERROR)) {
							logger.error("[encodeBegin][BCI_FINEX]Para SafeSigner es necesario recibir parametros (segundaclaveparametros:data)");
						}
                        throw new IllegalArgumentException("Numero de parametros (segundaclaveparametros:data) invalido");
                    }

                    // obtener llave
                    LlaveSegundaClaveTO llave = jerarquiaClaves.generarLlave(getEstadoSegundaClave()
                            .getSegundaClaveEnum(), servicio, usuarioSegundaClave, camposDeLlave, ONLINE);
                    
                    

                    String valorLlave = "";
                    
                    if (!esEntrust()){
                            
						if (logger.isEnabledFor(Level.DEBUG)) {
                            logger.debug("[encodeBegin] No genera llave por jerarquia ENTRUST");
						}
                            valorLlave = llave.getLlave().getClaveAlfanumerica();
                    }
                    
                    if (logger.isEnabledFor(Level.DEBUG)) {
                        logger.debug("[encodeBegin] valorLlave: " + valorLlave);
                    }

                    if (llave.isEstatus()) {
                        
                        /*
                         * mostrar QR
                         */
                        UIGraphic llaveImage = (UIGraphic) findComponent("llave-" + layout + "-"
                                + getEstadoSegundaClave().getSegundaClaveEnum().getNombre());
                        if (llaveImage != null) {
                            if (renderImageToBinary) {
                                Random generator = new Random();
                                int iKey = generator.nextInt();
                                if (logger.isEnabledFor(Level.DEBUG)) {
                                    logger.debug("[encodeBegin] Seteando la llave");
                                }

                                ConectorStruts.setAttribHttp("imagenqr", valorLlave);
                                llaveImage.setValue("/segundaClave/ImagenQR?key="+iKey);
                            }
                            else {
                                llaveImage.setValue("data:image/png;base64," + valorLlave);
                            }
                        }
                    }
                    else {
                        if (esEntrust()){
                            resuelveIPEjecucion();
                            tipoOperacionEntrust = ONLINE;
                            getUsuarioSegundaClave().setGrupoClienteEntrust(ONLINE);
                            estadoSegundaClave.setEstatus(true);
                            if (llave.getSerial()!=null){
                                if (logger.isEnabledFor(Level.DEBUG)) {
                                    logger.debug("[encodeBegin] valor TransaccionID : " + llave.getSerial());
                                }
                                jerarquiaClaves.setTransactionId(llave.getSerial());
                            }
                        }
                         else    
                        estadoSegundaClave.setEstatus(false);
                    }
                }
            }
        }
        catch (Exception e) {
           
			if (logger.isEnabledFor(Level.WARN) ) { 
            	logger.warn("[encodeBegin][Exception] " + "No es posible dibujar SegundaClaveUIInput: " + e.getMessage(), e);
            }			
            if (esEntrust()){
                    this.tipoOperacionEntrust=ERROR;
                    this.glosaErrorEntrust=e.getMessage();
            } else {
					if (logger.isEnabledFor(Level.ERROR)) {
						logger.error("[encodeBegin][BCI_FINEX][Exception]No es posible dibujar SegundaClaveUIInput: " + e.getMessage(), e);
					}
                throw new IOException("No es posible dibujar SegundaClaveUIInput: " + e.getMessage(), e);
            }
        }


        super.encodeBegin(context);
		 if (logger.isEnabledFor(Level.INFO)) {
            logger.info("[encodeBegin] [BCI_FINOK]");
        }
    }

    /**
     * Lee el valor de la clave ingresada por el usuario y lo define como el
     * valor del componente de entrada.
     * 
     * @param context
     * contexto.
     */
    @Override
    public void decode(FacesContext context) {
        logger.debug("[decode] Inicio");

        if (estadoSegundaClave == null){
            logger.debug("[decode] No se registra consulta de estado de segunda clave");
            super.decode(context);

            logger.debug("[decode] Fin");
            return;
        }

        if (!render.booleanValue() || !estadoSegundaClave.isEstatus()) {
            logger.debug("[decode] No se dibuja segunda clave");
            super.decode(context);

            logger.debug("[decode] Fin");
            return;
        }

        // obtener valor enviado
        UIInput clave = (UIInput) findComponent("clave-" + layout + "-"
            + estadoSegundaClave.getSegundaClaveEnum().getNombre());
        if (clave == null) {
            logger.debug("[decode] clave == null");
            setSubmittedValue(null);
        }
        else {
            logger.debug("[decode] clave != null");
            Object value = clave.getSubmittedValue();
            codeConfirmation = (String) value;
            setSubmittedValue(value);
        }

        if (logger.isDebugEnabled()) {
            Validator[] validators = getValidators();

            for (Validator validator : validators) {
                logger.debug("[decode] Validator: " + validator.getClass().getName());
            }
        }

        if (logger.isDebugEnabled()) {
            logger.debug("[decode] Antes de super.decode");
        }
        super.decode(context);

        logger.debug("[decode] Fin");
    }

    @Override
    public Object saveState(FacesContext context) {
        if (logger.isDebugEnabled()) {
            logger.debug("[saveState] Inicio");
        }

        /*
         * Remover validators
         */
        if (segundaClaveValidator != null) {
            removeValidator(segundaClaveValidator);
        }
        Object[] values = new Object[SIZE];
        values[ELEMENTO_CERO] = super.saveState(context);
        values[ELEMENTO_UNO] = isInitialized;
        values[ELEMENTO_DOS] = servicio;
        values[ELEMENTO_TRES] = usuarioSegundaClave;
        values[ELEMENTO_CUATRO] = layout;
        values[ELEMENTO_CINCO] = estadoSegundaClave;
        values[ELEMENTO_SEIS] = camposDeLlave;
        values[ELEMENTO_SIETE] = renderImageToBinary;
        values[ELEMENTO_OCHO] = codeConfirmation;
        values[ELEMENTO_NUEVE] = render;
        values[ELEMENTO_DIEZ] = jerarquiaClaves;
        values[ELEMENTO_ONCE] = tipoBotonDestino;
        values[ELEMENTO_DOCE] = tipoBotonRetorno;

        values[ELEMENTO_TRECE] = destinoButtonOnclick;
        values[ELEMENTO_CATORCE] = destinoButtonRetornoOnclick;

        if (logger.isDebugEnabled()) {
            logger.debug("[saveState] Fin");
        }
        
        return values;
    }

    @Override
    public void restoreState(FacesContext context, Object state) {
        if (logger.isDebugEnabled()) {
            logger.debug("[restoreState] Inicio");
        }

        Object[] values = (Object[]) state;
        super.restoreState(context, values[ELEMENTO_CERO]);

        isInitialized = (Boolean) values[ELEMENTO_UNO];
        servicio = (String) values[ELEMENTO_DOS];
        usuarioSegundaClave = (UsuarioSegundaClaveTO) values[ELEMENTO_TRES];
        layout = (String) values[ELEMENTO_CUATRO];
        estadoSegundaClave = (EstadoSegundaClaveTO) values[ELEMENTO_CINCO];
        camposDeLlave = (CamposDeLlaveTO) values[ELEMENTO_SEIS];
        renderImageToBinary = (Boolean) values[ELEMENTO_SIETE];
        codeConfirmation = (String) values[ELEMENTO_OCHO];
        render = (Boolean) values[ELEMENTO_NUEVE];
        jerarquiaClaves = (JerarquiaClavesModelMB) values[ELEMENTO_DIEZ];
        tipoBotonDestino = (String) values[ELEMENTO_ONCE];
        tipoBotonRetorno = (String) values[ELEMENTO_DOCE];
        destinoButtonOnclick = (String) values[ELEMENTO_TRECE];
        destinoButtonRetornoOnclick = (String) values[ELEMENTO_CATORCE];

        /*
         * Obtener validator para la segunda clave que corresponda.
         * Si se pinta la componente y si esta inicializado,
         * entonces se agrega los validators.
         */
        if (render.booleanValue() && isInitialized && estadoSegundaClave.isEstatus()) {
            if (SOFT_TOKEN_ENTRUST.equalsIgnoreCase(getEstadoSegundaClave().getSegundaClaveEnum().getNombre()) 
                    && usuarioSegundaClave.getGrupoClienteEntrust() != null
                    && usuarioSegundaClave.getGrupoClienteEntrust().equalsIgnoreCase(ONLINE)){
                logger.debug("[restoreState] No se agrega validator para Entrust Online");
            }
            else {
                logger.debug("[restoreState] Agregando validador");
                segundaClaveValidator = SegundaClaveValidatorFactory.crear(estadoSegundaClave);
                addValidator(segundaClaveValidator);
            }
        }

        if (logger.isDebugEnabled()) {
            logger.debug("[restoreState] Fin");
        }

    }

    /**
     * Para encodear la imagen como parametro para un servlet o pagina que
     * dibuja la imagen.
     * @param string
     * entrada.
     * @return P
     */
    private String encodeURI(String string) {
        if (logger.isDebugEnabled()) {
            logger.debug("[encodeURI] string: " + string);
        }
        String result = null;

        try {
            result = URLEncoder.encode(string, "UTF-8").replaceAll("\\%28", "(").replaceAll("\\%29", ")")
            .replaceAll("\\+", "%20").replaceAll("\\%27", "'").replaceAll("\\%21", "!")
            .replaceAll("\\%7E", "~");

            if (logger.isDebugEnabled()) {
                logger.debug("[encodeURI] result " + result);
            }

        }
        catch (UnsupportedEncodingException e) {
            logger.error("[encodeURI] [UnsupportedEncodingException] ", e);
            result = string;
        }
        if (logger.isDebugEnabled()) {
            logger.debug("[encodeURI] Fin");
        }
        return result;
    }

    /**
     * Verifica autenticacion.
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 (04/05/2013 Mauricio Palma. (Orand S.A.)): versi�n inicial.</li>
     * <li>1.1 03/09/2013 Pablo Romero C�ceres (SEnTRA) : Se agrega el traspaso de datos para utilizar la 
     *                                                    estrategia de EToken.
     * <li>1.2 11/08/2015 Luis Silva (TINet): Se mueve la l�gica de agregar datos para firma EToken al m�todo
     * {@link #agregarDatosFirmaEToken(ParametrosEstrategiaSegundaClaveTO)}</li>
     * </ul>
     * 
     * @param context
     * @return boolean que indica si esta autenticado.
     * @throws SeguridadException
     * @throws SeguridadException en caso de error.
     * Excepcion.
     */
    public boolean verificarAutenticacion() throws SeguridadException {
        
        if (SOFT_TOKEN_ENTRUST.equalsIgnoreCase(getEstadoSegundaClave().getSegundaClaveEnum().getNombre()) 
                && getUsuarioSegundaClave().getGrupoClienteEntrust() != null
                && getUsuarioSegundaClave().getGrupoClienteEntrust().equalsIgnoreCase(ONLINE)) {
            logger.debug("cambio valor de codeCOnfirmation");
            codeConfirmation = jerarquiaClaves.getTransactionId();
        }
        RepresentacionAlfanumericaSegundaClaveTO representacionAlfanumericaSegundaClave = 
            new RepresentacionAlfanumericaSegundaClaveTO(
                    codeConfirmation);

        ParametrosEstrategiaSegundaClaveTO parametrosEstrategiaSegundaClaveTO = 
            new ParametrosEstrategiaSegundaClaveTO(
                    SegundaClaveUtil.toUsuarioSegundaClaveTO(this.usuarioSegundaClave),
                    representacionAlfanumericaSegundaClave,
                    ConectorStruts.getSessionBCI());
        parametrosEstrategiaSegundaClaveTO.setCanal(this.usuarioSegundaClave.getCanalId());

        agregarDatosFirmaEToken(parametrosEstrategiaSegundaClaveTO);

        if (SOFT_TOKEN_ENTRUST.equalsIgnoreCase(getEstadoSegundaClave().getSegundaClaveEnum().getNombre())){ 
                if (getUsuarioSegundaClave().getGrupoClienteEntrust() != null
                && getUsuarioSegundaClave().getGrupoClienteEntrust().equalsIgnoreCase(ONLINE)) {
                    parametrosEstrategiaSegundaClaveTO.setTipoSegundaClave(ONLINE);
                }
                else {
                    parametrosEstrategiaSegundaClaveTO.setTipoSegundaClave(OFFLINE);
                }
        }
        
        if (this.estadoSegundaClave.isEstatus() && this.estadoSegundaClave.getSegundaClaveEnum().isRequiereLlave()){
            parametrosEstrategiaSegundaClaveTO.setCamposDeLlave(SegundaClaveUtil.toCamposDeLlaveTO(this.camposDeLlave));
            parametrosEstrategiaSegundaClaveTO.setServicio(this.servicio);
        }

        try {
            EstrategiaSegundaClave estrategiaSegundaClave = EstrategiaAutenticacionFactory
            .obtenerEstrategiaSegundaClave(SegundaClaveUtil
                    .toPrioridadSegundaClaveIdTO(getEstadoSegundaClave().getSegundaClaveEnum()));

            ResultadoOperacionSegundaClaveTO resultado = estrategiaSegundaClave
            .autenticar(parametrosEstrategiaSegundaClaveTO);

            if (resultado.getIdCodigoEstado() == AUTENTICACION_OK) {
            	journalizar(parametrosEstrategiaSegundaClaveTO, true, false);
                return true;
            }

            journalizar(parametrosEstrategiaSegundaClaveTO, false, true);
            if (logger.isDebugEnabled()) {
                logger.debug("[verificarAutenticacion] Validacion erronea " + resultado.getGlosa());
            }
            throw new SeguridadException(resultado.getGlosa());

        }
        catch (SeguridadException se) {
            if (logger.isDebugEnabled()) {
                logger.debug("[verificarAutenticacion] Validacion Codigo [" + se.getCodigo() + "]");
                logger.debug("[verificarAutenticacion] Validacion erronea " + se.getInfoAdic());
            }
            String codigos = TablaValores.getValor(TABLASEGURIDAD, "bloqueoSegundaClave", "codigos");
            String[] codigosBloqueos = StringUtil.divide(codigos, ",");
            journalizar(parametrosEstrategiaSegundaClaveTO, false, false);
              for (int i = 0; i < codigosBloqueos.length; i++) {
                if (codigosBloqueos[i].equals(se.getCodigo())) {
                    estadoSegundaClave.setEstatus(false);
                    throw se;
                }
            }
            
            if (esEntrust()){
                this.fragmentManejaError=true;
            }
            throw se;
        }
    }

    public String getTipoBotonDestino() {
        return tipoBotonDestino;
    }

    public void setTipoBotonDestino(String tipoBotonDestino) {
        this.tipoBotonDestino = tipoBotonDestino;
    }

    public String getTipoBotonRetorno() {
        return tipoBotonRetorno;
    }

    public void setTipoBotonRetorno(String tipoBotonRetorno) {
        this.tipoBotonRetorno = tipoBotonRetorno;
    }

    /**
     * Journaliza el resultado de la validacion de segunda clave.
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 21/07/2013 Victor Enero. (Orand S.A.): versi�n inicial.</li>
     * <li>1.1 29/04/2014 Rodrigo Briones O. (Bci) : Se corrige journalizaci�n para que lea desde
     * una tabla de parametros el id de producto segun la segunda clave que est� usando el cliente.</li>
     * <li>1.2 10/07/2017 Luis L�pez Alamos (SEnTRA) - Ricardo Carrasco C�ceres (Ing. Soft. BCI): Se mueve l�gica de journalizaci�n
     * a SegundaClave.parametros, Se agrega l�gica para journalizaci�n Entrust Token y SoftToken.</li>
     * </ul>
     * 
     * @param parametrosEstrategiaSegundaClaveTO Parametro de segunda clave.
     * @param isOk Si la transaccion es OK.
     * @param isError Si la transaccion es un error o una excepcion.
     */
    private void journalizar(ParametrosEstrategiaSegundaClaveTO parametrosEstrategiaSegundaClaveTO, 
            boolean isOk,
            boolean isError) {
        try {
            JournalUtilityMB journal = new JournalUtilityMB();
            Eventos evento = new Eventos();

            String rutCliente = String.valueOf(parametrosEstrategiaSegundaClaveTO.getUsuarioSegundaClave()
                    .getRut());
            String dv = String.valueOf(parametrosEstrategiaSegundaClaveTO.getUsuarioSegundaClave().getDv());
            
            if (logger.isInfoEnabled()) {
                logger.debug("[journalizar] [" + rutCliente +"][BCI_INI] inicio");
            }
            
            
            String tipoSegundaClave = getEstadoSegundaClave().getSegundaClaveEnum().getNombre();

            if (tipoSegundaClave == null){
            	tipoSegundaClave = TablaValores.getValor(TABLA_PARAMETROS_JOURNAL, "Journal", "tipoSegundaClaveDefecto");
            }
            if (logger.isDebugEnabled()) {
            	logger.debug("[journalizar] [" + rutCliente + "] tipo segunda clave usada: " + tipoSegundaClave);
            }
            
            String tipoJournal = "";
            String campoVariable = "";
                    
            if (SOFT_TOKEN_ENTRUST.equalsIgnoreCase(tipoSegundaClave)){
                tipoJournal = TablaValores.getValor(TABLA_PARAMETROS_JOURNAL, "JournalStk", DESC);
                if (parametrosEstrategiaSegundaClaveTO.getTipoSegundaClave()!=null) {
                	campoVariable = parametrosEstrategiaSegundaClaveTO.getTipoSegundaClave();
                }
                
            }
            else if (TOKEN_ENTRUST.equalsIgnoreCase(tipoSegundaClave)){
                tipoJournal = TablaValores.getValor(TABLA_PARAMETROS_JOURNAL, "JournalHtk", DESC);    
            }
            else {
                tipoJournal = TablaValores.getValor(TABLA_PARAMETROS_JOURNAL, "Journal", DESC);
            }

            evento.setIdProducto(TablaValores.getValor(TABLA_PARAMETROS_JOURNAL, "ProductoJournal", tipoSegundaClave));
            evento.setIdCanal(parametrosEstrategiaSegundaClaveTO.getCanal().getCanalID());
            evento.setRutCliente(rutCliente);
            evento.setDvCliente(dv);
            evento.setIdMedio(ConectorStruts.getRemoteAddr());

            if (isOk) {
                evento.setEstadoEventoNegocio(TablaValores.getValor(TABLA_PARAMETROS_JOURNAL, tipoJournal, "TrxProcesada"));
                evento.setSubCodEventoNegocio(TablaValores.getValor(TABLA_PARAMETROS_JOURNAL, tipoJournal,"codSubtipoOk"));
                evento.setCodEventoNegocio(TablaValores.getValor(TABLA_PARAMETROS_JOURNAL, tipoJournal, "CodEventoNegocio"));

            }
            else if (isError) {
                evento.setEstadoEventoNegocio(TablaValores.getValor(TABLA_PARAMETROS_JOURNAL, tipoJournal, "TrxProcesada"));
                evento.setSubCodEventoNegocio(TablaValores.getValor(TABLA_PARAMETROS_JOURNAL, tipoJournal,"codSubtipoNOk"));
                evento.setCodEventoNegocio(TablaValores.getValor(TABLA_PARAMETROS_JOURNAL, tipoJournal, "CodEventoNegocio"));
            }
            else {
                evento.setEstadoEventoNegocio(TablaValores.getValor(TABLA_PARAMETROS_JOURNAL, tipoJournal, "estEventoError"));
                evento.setSubCodEventoNegocio(TablaValores.getValor(TABLA_PARAMETROS_JOURNAL, tipoJournal,"codSubtipoError"));
                evento.setCodEventoNegocio(TablaValores.getValor(TABLA_PARAMETROS_JOURNAL, tipoJournal, "CodEventoNegocio"));
            }
            
            evento.setCampoVariable(campoVariable);
         
            journal.journalizar(evento);
            if (logger.isInfoEnabled()) {
                logger.debug("[journalizar] [" + rutCliente +"][BCI_FINOK] inicio");
            }
        }
        catch (Exception ex) {
            logger.error("[journalizar] [BCI_EX] Error en la journalizacion de segunda clave", ex);
        }
    }

    public String getDestinoButtonOnclick() {
        return destinoButtonOnclick;
    }

    public void setDestinoButtonOnclick(String destinoButtonOnclick) {
        this.destinoButtonOnclick = destinoButtonOnclick;
    }

    public String getDestinoButtonRetornoOnclick() {
        return destinoButtonRetornoOnclick;
    }

    public void setDestinoButtonRetornoOnclick(String destinoButtonRetornoOnclick) {
        this.destinoButtonRetornoOnclick = destinoButtonRetornoOnclick;
    }

    /**
     * Agrega a los parametros necesarios para validar segunda clave los datos usados para firma etoken.
     * <p>
     * Registro de versiones:<ul>
     * 
     * <li>1.0 11/08/2015 Luis Silva (TINet): Versi�n inicial.
     * </ul><p>
     * @param parametrosEstrategiaSegundaClaveTO Objeto en donde se establece los datos necesarios para
     * la validaci�n de segunda clave.
     * @since 1.6
     */
    protected void agregarDatosFirmaEToken(ParametrosEstrategiaSegundaClaveTO parametrosEstrategiaSegundaClaveTO) {
        HttpServletRequest request = 
            (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();

        firmaDigitalModelMB = (FirmaDigitalModelMB) ConectorStruts.getAttribHttp("firmaDigitalModelMB");
        if (firmaDigitalModelMB == null) {
            firmaDigitalModelMB = new FirmaDigitalModelMB();
        }
        String mensajeOriginal = this.firmaDigitalModelMB.getMensaje();
        String dataToSign = request.getParameter("dataToSign");
        String dataSignature = request.getParameter("dataSignature");
        parametrosEstrategiaSegundaClaveTO.setDataToSign(dataToSign);
        parametrosEstrategiaSegundaClaveTO.setDataSignature(dataSignature);
        parametrosEstrategiaSegundaClaveTO.setMensajeOriginal(mensajeOriginal);
    }
    
    /**
     * Metodo encargado de generar una LLave QR cuando se este utilizando autenticaci�n Entrust.
     * <p>
     * 
     * Registro de Versiones:
     * <ul>
     * <li>1.0 10/07/2017 Luis L�pez Alamos (SEnTRA) - Ricardo Carrasco C�ceres (Ing. Soft. BCI): Versi�n Inicial.</li>
     * </ul>
     * <P>
     * @throws Exception en caso de falta de argumentos o por error de las clases de InetAdress.
     * @since 1.7
     */
    public void generarLlaveQR() throws IOException{
        if (logger.isEnabledFor(Level.INFO)) {
            logger.info("[generarLlaveQR][BCI_INI]");
        }
        try {
        	if (getUsuarioSegundaClave()==null) {
                logger.info("[generarLlaveQR] Se vuelve a cargar usuarioConectado");
        	    usuarioSegundaClave = SegundaClaveUtil.getUsuarioSegundaClave();
        	}
        	
        	getUsuarioSegundaClave().setGrupoClienteEntrust(OFFLINE);
            jerarquiaClaves.setTransactionId(null);
            llavesQREntrust = jerarquiaClaves.generarLlave(getEstadoSegundaClave().getSegundaClaveEnum(), servicio, this.usuarioSegundaClave, camposDeLlave, OFFLINE);
          	tipoOperacionEntrust=OFFLINE;
          	llaveQR=llavesQREntrust.getLlaves()[0].getClaveAlfanumerica();
        
        } catch (Exception e) {
            logger.error("[generarLlaveQR] " + "No es posible dibujar Clave QR para Autenticaci�n Entrust : " + e.getMessage(), e);
                    this.tipoOperacionEntrust=ERROR;
                    this.glosaErrorEntrust=e.getMessage();
        }
        if (logger.isEnabledFor(Level.INFO)) {
            logger.info("[generarLlaveQR][BCI_FINOK]");
        }
        
    }

    /**
     * Metodo encargado de obtener desde tabla de parametros el valor de frecuencia con el cual se consulta al pooling de curus 'Enstrust'.
     * <p>
     * 
     * Registro de Versiones:
     * <ul>
     * <li>1.0 10/07/2017 Luis L�pez Alamos (SEnTRA) - Ricardo Carrasco C�ceres (Ing. Soft. BCI): Versi�n Inicial.</li>
     * </ul>
     * <P>
     * @since 1.7
     */
    public String getFrecuenciaEntrust() {
        String frecuencia = TablaValores.getValor(TABLA_PARAMETROS_ENTRUST, "frecuenciaConsultaTransaccion", DESC);
        if (frecuencia != null){
            frecuenciaEntrust = frecuencia;
        } else {
            if (logger.isEnabledFor(Level.ERROR)){
                logger.error("[getFrecuenciaEntrust][BCI_FINEX]");
            }
            throw new IllegalArgumentException("Valor nulo para frecuencia");
        }
        return frecuenciaEntrust;
        
    }

    /**
     * Metodo encargado de obtener desde tabla de parametros el valor de TIME_OUT con el cual se consulta al pooling de curus 'Enstrust'.
     * <p>
     * 
     * Registro de Versiones:
     * <ul>
     * <li>1.0 10/07/2017 Luis L�pez Alamos (SEnTRA) - Ricardo Carrasco C�ceres (Ing. Soft. BCI): Versi�n Inicial.</li>
     * </ul>
     * <P>
     * @since 1.7
     */
    public String getTimeOutEntrust() {
        String timeOut = TablaValores.getValor(TABLA_PARAMETROS_ENTRUST, "timeOutConsultaTransaccion", DESC);
        if (timeOut != null){
            timeOutEntrust = timeOut;
        } else {
            if (logger.isEnabledFor(Level.ERROR)){
                logger.error("[getTimeOutEntrust][BCI_FINEX]");
            }
            throw new IllegalArgumentException("Valor nulo para timeout");
        }
        return timeOutEntrust;
    }
    
    

    public String getTipoOperacionEntrust() {
        return tipoOperacionEntrust;
    }
    
    public String getGlosaErrorEntrust() {
        return glosaErrorEntrust;
    }

    public void setGlosaErrorEntrust(String glosaErrorEntrust) {
        this.glosaErrorEntrust = glosaErrorEntrust;
    }
    
    
     /**
     *Metodo encargado de setear en caso de error, el resultado de consulta de desafio entrust SoftToken.
     * <p>
     * 
     * Registro de Versiones:
     * <ul>
     * <li>1.0 10/07/2017 Luis L�pez Alamos (SEnTRA) - Ricardo Carrasco C�ceres (Ing. Soft. BCI): Versi�n Inicial.</li>
     * <li>2.0 13/10/2017 Jaime Suazo Diaz  (SEnTRA) - Ricardo Carrasco C�ceres (Ing. Soft. BCI): Se agrega captura de parametro
     *         <code>paramEstadoConsulta</code> desde el FacesContext para setear el mensaje correspondiente al estado de transaccion
     *         <br>
     *         Este metodo es llamado desde los fragments de Entrust SoftToken.</li>
     * </ul>
     * <P>
     * @since 1.7
     */
    public void resuelveResultado(){
         if (logger.isEnabledFor(Level.INFO)) {
             logger.info("[resuelveResultado][BCI_INI]");
         }
         this.tipoOperacionEntrust=ERROR;
         
         String paramEstadoConsulta = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("paramEstadoConsulta");
         
         if (logger.isEnabledFor(Level.DEBUG)) {
        	 logger.debug("[resuelveResultado][paramEstadoConsulta][" + paramEstadoConsulta + "]");	 
         }
         
         if (paramEstadoConsulta != null && !paramEstadoConsulta.trim().equals("")) {
             if (logger.isEnabledFor(Level.DEBUG)) {
            	 logger.debug("[resuelveResultado][if][paramEstadoConsulta][" + paramEstadoConsulta + "]");	 
             }
        	 
             String glosaError = TablaValores.getValor(TABLA_PARAMETROS_ENTRUST, paramEstadoConsulta, "glosa");
             if (glosaError != null) {
                 this.setGlosaErrorEntrust(glosaError);
             }
             
         } 
         else if (jerarquiaClaves != null && jerarquiaClaves.getEstadoTransaccionEntrust() != null) {
        	 
             if (logger.isEnabledFor(Level.DEBUG)) {
            	 logger.debug("[resuelveResultado][else if][paramEstadoConsulta][" + paramEstadoConsulta + "]");	 
             }
        	 
             String estadoTransaccion = jerarquiaClaves.getEstadoTransaccionEntrust().toUpperCase();
             String glosaError = TablaValores.getValor(TABLA_PARAMETROS_ENTRUST, estadoTransaccion, "glosa");
             if (glosaError != null) {
                 this.setGlosaErrorEntrust(glosaError);
         }
         }
         
         if (logger.isEnabledFor(Level.INFO)) {
             logger.info("[resuelveResultado][BCI_FINOK]" + this.getTipoOperacionEntrust() + " - " + this.getGlosaErrorEntrust());
         }
     }
    
    /**
     * Metodo encargado de setear el atributo urlConsultaPoolingEntrust con la direccion f�sica de la aplicaci�n web que se est� ejecutando en un determinado servidor.
     * <p>
     * 
     * Registro de Versiones:
     * <ul>
     * <li>1.0 10/07/2017 Luis L�pez Alamos (SEnTRA) - Ricardo Carrasco C�ceres (Ing. Soft. BCI): Versi�n Inicial.</li>
     * <li>1.1 11/10/2017 Jaime Suazo D. (SEnTRA) - Ricardo Carrasco C�ceres (Ing. Soft. BCI): Se corrige conformaci�n de URL del 
     *         consulta al pooling de producci�n o de otro ambiente.<br><br>
     *         
     *         protocolo       = <code>TablaValores.getValor(TABLA_PARAMETROS_ENTRUST, "pathRest", "protocolo")</code>
     *         ://             = <code>FORWARD_IP</code>
     *         IP o Dominio    = <code>request.getServerName()</code>
     *         rutaRestService = <code>/svcRest/segundaclave/consultarEstadoOperacion</code>
     *         <br>
     *         <br>
     *         
     *         Para consultas por m�quina se obtiene los siguientes datos
     *         <br>
     *         
     *         protocolo       = <code>TablaValores.getValor(TABLA_PARAMETROS_ENTRUST, "pathRest", "protocolo")</code>
     *         ://             = <code>FORWARD_IP</code>
     *         IP o Dominio    = <code>request.getServerName()</code>
     *         :puerto         = <code>TablaValores.getValor(TABLA_PARAMETROS_ENTRUST, "pathRest", "puerto")</code>
     *         rutaRestService = <code>/svcRest/segundaclave/consultarEstadoOperacion</code>
     *         
     *         .</li>
     * </ul>
     * <P>
     * @throws Exception en caso de falta de argumentos o por error de las clases de InetAdress.
     * @since 1.7
     */
    private void resuelveIPEjecucion() throws Exception{
        if(logger.isEnabledFor(Level.INFO)){
            logger.info("[resuelveIPEjecucion][BCI_INI]");
            
        }
        try{
            
            HttpServletRequest request =(HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest(); 
            
            if (request.getServerName() == null ||  request.getScheme() == null) {  
            	logger.info("[resuelveIPEjecucion] No se pudo obtener dominio de ejecucu�n");
                throw new IllegalArgumentException();
            }
            
            StringBuilder hostConexion = new StringBuilder();
            
            hostConexion.append(TablaValores.getValor(TABLA_PARAMETROS_ENTRUST, "pathRest", "protocolo"));
            
            hostConexion.append(FORWARD_IP);
            
            String ipONombreDominio = request.getServerName();
            
            if (logger.isEnabledFor(Level.DEBUG)) {
                logger.debug("[resuelveIPEjecucion][ipONombreDominio][" + ipONombreDominio + "]");
            }
            
            hostConexion.append(ipONombreDominio);
            
            String comienzoIPBanco = TablaValores.getValor(TABLA_PARAMETROS_ENTRUST, "pathRest", "comienzoIPBanco");
            
            if (ipONombreDominio!= null && ipONombreDominio.startsWith(comienzoIPBanco)) {
            	
                if (logger.isEnabledFor(Level.DEBUG)) {
                    logger.debug("[resuelveIPEjecucion][PRUEBA POR MAQUINA][" + ipONombreDominio + "]");
                }
            	
                String puerto = TablaValores.getValor(TABLA_PARAMETROS_ENTRUST, "pathRest", "puerto");
            	hostConexion.append(puerto);
            	
                if (logger.isEnabledFor(Level.DEBUG)) {
                    logger.debug("[resuelveIPEjecucion][PRUEBA POR MAQUINA][" + hostConexion.toString() + "]");
                }
            }            
            //OJO SOLO SE USA PARA AMBIENTES LOCALES, NO PASAR A OTROS AMBIENTES
            //OJO SOLO SE USA PARA AMBIENTES LOCALES, NO PASAR A OTROS AMBIENTES
            //OJO SOLO SE USA PARA AMBIENTES LOCALES, NO PASAR A OTROS AMBIENTES
            hostConexion.append(":7001");
            //OJO SOLO SE USA PARA AMBIENTES LOCALES, NO PASAR A OTROS AMBIENTES
            //OJO SOLO SE USA PARA AMBIENTES LOCALES, NO PASAR A OTROS AMBIENTES
            //OJO SOLO SE USA PARA AMBIENTES LOCALES, NO PASAR A OTROS AMBIENTES

            hostConexion.append(TablaValores.getValor(TABLA_PARAMETROS_ENTRUST, "pathRest", "rutaRestService"));
            if(logger.isEnabledFor(Level.DEBUG)){
                logger.debug("[resuelveIPEjecucion] ruta de ejecuci�n = " + hostConexion.toString());
            }
            
            
            if (hostConexion.toString().indexOf("null")!=-1){
                logger.info("[resuelveIPEjecucion] Faltan datos en la tabla de parametros");
                throw new IllegalArgumentException(TablaValores.getValor(TABLA_PARAMETROS_ENTRUST, ERROR_DE_ARGUMENTO, DESC));
            }
            
            urlConsultaPoolingEntrust = hostConexion.toString();
            
            }
            catch (Exception ue){
                if(logger.isEnabledFor(Level.ERROR)){
                    logger.error("[resuelveIPEjecucion][Exception][BCI_FINEX] Excepcion al obtener la direcci�n de ejecuci�n , mensaje=<"+ ue.getMessage() + ">", ue);
                }
                throw new Exception(ue.getMessage());
            }
        
        if(logger.isEnabledFor(Level.INFO)){
            logger.info("[resuelveIPEjecucion][BCI_FINOK]");
        }    
    }
    
    
    /**
     * Metodo encargado de reconocer si la instancia actual de ejecuci�n corresponde a una autenticaci�n Entrust.
     * <p>
     * 
     * Registro de Versiones:
     * <ul>
     * <li>1.0 10/07/2017 Luis L�pez Alamos (SEnTRA) - Ricardo Carrasco C�ceres (Ing. Soft. BCI): Versi�n Inicial.</li>
     * </ul>
     * <P>
     * @since 1.7
     */
    private boolean esEntrust(){
        boolean resultado = false;
        if (SOFT_TOKEN_ENTRUST.equalsIgnoreCase(getEstadoSegundaClave().getSegundaClaveEnum().getNombre()) 
                || TOKEN_ENTRUST.equalsIgnoreCase(getEstadoSegundaClave().getSegundaClaveEnum().getNombre())){
                resultado = true;
        }
        return resultado;
    }

    public boolean isFragmentManejaError() {
        return fragmentManejaError;
    }

    public void setFragmentManejaError(boolean fragmentManejaError) {
        this.fragmentManejaError = fragmentManejaError;
    }

    public String getLlaveQR() {
        return llaveQR;
    }

    public void setLlaveQR(String llaveQR) {
        this.llaveQR = llaveQR;
    }
        
}
