package cl.bci.aplicaciones.seguridad.bloqueotdd.mb;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import wcorp.model.actores.Cliente;
import wcorp.model.actores.DireccionClienteBci;
import wcorp.serv.clientes.RetornoTipCli;
import wcorp.serv.cuentas.Cta;
import wcorp.serv.direcciones.ServiciosDirecciones;
import wcorp.serv.direcciones.ServiciosDireccionesHome;
import wcorp.serv.seguridad.ServiciosSeguridad;
import wcorp.serv.seguridad.ServiciosSeguridadHome;
import wcorp.serv.tarjetas.DetalleTarjeta;
import wcorp.serv.tarjetas.ServiciosTarjetas;
import wcorp.serv.tarjetas.ServiciosTarjetasHome;
import wcorp.serv.tarjetas.TarjetaCliente;
import wcorp.serv.tarjetas.to.DatosTarjetaDeDebitoTO;
import wcorp.util.EnhancedServiceLocator;
import wcorp.util.FechasUtil;
import wcorp.util.StringUtil;
import wcorp.util.TablaValores;
import wcorp.util.mensajeria.ParametrosCorreoBloqueoTarjetaDebito;

import cl.bci.aplicaciones.cliente.mb.ClienteMB;
import cl.bci.aplicaciones.colaborador.mb.ColaboradorModelMB;
import cl.bci.aplicaciones.seguridad.bloqueotdc.mb.TarjetaDeCreditoModelMB;
import cl.bci.aplicaciones.utilitarios.iconosinteligentes.mb.IconoInteligenteUtilityMB;
import cl.bci.infraestructura.utilitarios.mensajeria.mb.ServicioMensajeriaUtilityMB;
import cl.bci.infraestructura.web.journal.Journalist;
import cl.bci.infraestructura.web.seguridad.mb.SesionMB;

/**
 * MB con los datos del formulario de habilitacion/deshabilitacion de la tarjeta
 * de debito.
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 12/09/2013 Yon Sing Sius (ExperimentoSocial): Version inicial.</li>
 * <li>1.1 10/12/2013 Yon Sing Sius (ExperimentoSocial): Se agrega subcodigo en el listado de servicios.</li>
 * <li>1.2 19/10/2015 Claudia Aracena(SEnTRA) - Juan Sagredo(ing Soft. BCI):  se realizan modificaciones para aplicacion
 * sistemas TDD bcinet. Se agregan los atributos {@link #rutClienteBuscado}, {@link #dvCliente}, {@link #emailCliente},
 * {@link #clienteConsulta}, {@link #resultadoBusqueda}, {@link #listaTDD}, {@link #cuentasCtes},
 * {@link #cuentaCteSeleccionada}, {@link #tarjetasDeDebito}, {@link #tarjetaDebitoSeleccionada}, {@link #nombreCapitalizadoCliente},{@link #VALUE_ESTADO_BLOQUEADO},
 * {@link #TIPO_EMAIL}, {@link #CANAL_PERSONAS}, {@link #ESPACIO_SEPARADOR_CORREO},{@link #TEXTO_DEFAULT_COMBO}, {@link #VALUE_DEFAULT_COMBO},{@link #informacionTDD}
 * {@link #FLAG_NOCLIENTE}, {@link #FLAG_NOOK}, {@link #FLAG_NOPERMISO}, {@link #FLAG_NOSUCCESS}, {@link #FLAG_OK}, {@link #FLAG_SUCCESS}, {@link #CLIENTE_NO_EXISTE}
 * {@link #TEXTO_DESCONOCIDO} y {@link #ESTADO_TARJETA_ACTIVA}.
 * Se realiza inyeccion de {@link #colaboradorMB} con sus respectivos get y set. 
 * Se agregan metodos {@link #buscarClientePorRut()},{@link #obtenerTarjetaDeDebitoPorCtaCte()}, {@link #obtenerTarjetasDebito()},
 * {@link #cambiaEstadoTDD()}, {@link #enviarCorreo(String, String, String)}, {@link #getContenidoMail(Date, String, String, String, String)},
 * {@link #obtenerEmailRegistrado()}, {@link #createEjbDirecciones()}, {@link #createEjbTarjetas()}, {@link #createEjbSeguridad()},
 * {@link #journalizar(String, String, String, String, String, String)}, {@link #getLogger()}.
 * Se normaliza metodo {@link #getTarjetasDebito()} por correccion de import prohibido.</li>
 * <li>1.3 24/12/2015 Claudia Aracena (SEnTRA) - Juan Sagredo(ing Soft. BCI): se modifica metodo {@link #journalizar(String, String, String, String, String, String)}.</li>
 * <li>1.4 03/02/2016 Claudia Aracena (SEnTRA) - Juan Sagredo(ing Soft. BCI): se elimina el metodo {@link #getContenidoMail(Date, String, String, String, String)},
 * se elimina atributo ESPACIO_SEPARADOR_CORREO, y se agrega {@link #CANAL_TBANC}, {@link #SERVICIO_ENVIO_CORREO} y {@link #servicioMensajeriaUtilityMB}. 
 * se modifica {@link #enviarCorreo(String, String, Date, String, String, String)} y {@link #cambiaEstadoTDD()}.</li>
 * <li>1.5 19/10/2017 Andr�s Silva H.(SEnTRA) - Ricardo Carrasco (Ing. Soft. BCI): Se modifica la implementacion el m�todo {@link #getServicioNombre()} 
 * adicionalmente se agregan atributos :
*<ul>
*<li>{@link #DESC_SOLICITUD_HABILITACION}</li>
*<li>{@link #TIPO_SOLICITUD_HABILITACION}</li>
*<li>{@link #TIPO_SOLICITUD_DESHABILITACION}</li>
*<li>{@link #DESC_SOLICITUD_DESHABILITACION}</li>
*</ul>
 * asi tambien, setter y getter para : fechaSolicitud,tituloComprobante, esto para la rederizaci�n del comprobante.</li>
 * 
 * </ul>
 * <B>Todos los derechos reservados por Banco de Cr�dito e Inversiones. </B>
 */
@ManagedBean
@ViewScoped
public class BloqueoTarjetaDebitoViewMB implements Serializable {

	/**
	 * Tipo de solicitud para habilitar la tarjeta de d�bito.
	 */
	public static final int TIPO_SOLICITUD_HABILITACION = 1;
	/**
	 * Descripcion de solicitud para habilitar la tarjeta de d�bito.
	 */
	public static final String DESC_SOLICITUD_HABILITACION = "Habilitaci�n";
	/**
	 * Tipo de solicitud para deshabilitar la tarjeta de d�bito.
	 */
	public static final int TIPO_SOLICITUD_DESHABILITACION = 2;
	/**
	 * Descripcion de solicitud para deshabilitar la tarjeta de d�bito.
	 */
	public static final String DESC_SOLICITUD_DESHABILITACION = "Deshabilitaci�n";
	/**
	 * Recurso de mensajes.
	 */
	public static final String RECURSO_MENSAJE = "cl.bci.aplicaciones.seguridad.bloqueotdd.bloqueoTDD";

	/**
	 * Tabla de par�metros para la obtenci�n del servicio RedBanc.
	 */
	private static final String TABLA_COD_TRANS_REDBANC = "integracionRedBanc.parametros";
	
	/**
	 * Valor por default a desplegar en el combobox.
	 */
	private static final String TEXTO_DEFAULT_COMBO ="Seleccione...";
	
	/**
	 * value por default a manejar en el combobox.
	 */
	private static final String VALUE_DEFAULT_COMBO = "-1";
	
	/**
	 * Codigo para estado bloqueado.
	 */
	private static final String VALUE_ESTADO_BLOQUEADO = "43";
	/**
	 * Posici�n del campo que contiene el c�digo del servicio en la tabla de
	 * parametros.
	 */
	private static final int POSCODSRV = 0;

	/**
	 * Posici�n del campo que contiene el nombre del servicio en la tabla de
	 * parametros.
	 */
	private static final int POSNOMSRV = 1;

	/**
	 * Posici�n del campo que contiene el c�digo del sub-servicio en la tabla de
	 * parametros.
	 */
	private static final int POSSUBSRV = 2;
	
	/**
	 * Posici�n dentro del arreglo donde se encuentra el id del servicio.
	 */
	private static final int POSIDSRV = 3;
	
	/**
	 * Numeros de campos que conforman un servicio en la tabla de parametros.
	 */
	private static final int CAMPOSSRV = 4;

	/**
	 * Serial Id.
	 */
	private static final long serialVersionUID = 3592277971376020957L;
	
	/**
	 * C�digos para tarjetas ATM.
	 */
	private static final String TABLA_TARJETAS_ATM = "tarjetasATM.codigos";

	/**
	 * D�gitos ocultos de PAN.
	 */
	private static final int OCULTARPAN = 12;

	/**
	 * Tipo de direccion email.
	 */
	private static final char TIPO_EMAIL = '7';
	
	/**
	 * Id de canal personas.
	 */
	private static final String CANAL_PERSONAS ="110";
	
	/**
	 * Id de canal TBanc.
	 */
	private static final String CANAL_TBANC = "100";
	
	/**
	 * Flag de error para cuando el cliente no existe.
	 */
	private static final String CLIENTE_NO_EXISTE = "CLIENTE NO EXISTE";
	/**
	 * Flag para consultas ok de clientes.
	 */
	private static final String FLAG_OK = "OK";
	
	/**
	 * Flag para problemas en la busqueda de clientes.
	 */
	private static final String FLAG_NOOK = "NOOK";
	
	/**
	 * Flag para exito en el cambio de estado de tdd.
	 */
	private static final String FLAG_SUCCESS = "SUCCESS";
	
	/**
	 * Flag para no cliente.
	 */
	private static final String FLAG_NOCLIENTE = "NOCLIENTE";
	
	/**
	 * Flag para consulta de cliente sin permisos.
	 */
	private static final String FLAG_NOPERMISO = "NOPERMISO";
	
	/**
	 * Flag para error al cambio de estado de tdd.
	 */
	private static final String FLAG_NOSUCCESS = "NOSUCCESS";
	
	/**
	 * Variable para identificar la ip que solicita la llamada a la pagina.
	 */
	private static final String FORWARD_IP = "X-FORWARDED-FOR";
	
	/**
	 * Valor que indica estado de activo para las tdd.
	 */
	private static final String ESTADO_TARJETA_ACTIVA = "03";
	
	/**
	 * Valor que representa texto "desconocido" retornado por las cabeceras de request.
	 */
	private static final String TEXTO_DESCONOCIDO = "unknown";
	
	/**
	 * Llave con nombre de servicio para envio de correo.
	 */
	private static final String SERVICIO_ENVIO_CORREO = "BloqueoTDD";
	
	/**
	 * Fecha de la solicitud, corresponde a la fecha para mostrar en el comprobante.
	 */
	private  Date fechaSolicitud;
	/**
	 * Log de la clase.
	 */
	private transient Logger logger = (Logger) Logger.getLogger(BloqueoTarjetaDebitoViewMB.class);

	/**
	 * Todas las tarjetas de d�bito vigentes del cliente.
	 */
	private ArrayList<DetalleTarjeta> tarjetasDebito;

	/**
	 * Listado de las tarjetas por cuenta.
	 */
	private ArrayList<String> tarjetasDebitoPorCuenta;

	/**
	 * Lista de cuentas corrientes del cliente.
	 */
	private ArrayList<Cta> cuentasCorrientes;

	/**
	 * Cuenta que selecciona el cliente desde el formulario.
	 */
	private String cuentaSeleccionada;

	/**
	 * Tarjeta seleccionada para la h�bilitaci�n/deshabilitaci�n.
	 */
	private String tarjetaSeleccionada;

	/**
	 * Tipo de solicitud, puede ser habilitaci�n o deshabilitaci�n de tarjeta.
	 */
	private String servicioSeleccionado;
	
	/**
	 * Listado con las acciones disponibles para realizar con la tarjeta de d�bito.
	 */
	private String[][] servicios;
	
	/**
	 * Url de redirecci�n a la p�gina de RedBank.
	 */
	private String urlRedBank;

	/**
	 * Listado con las cuenta que tengan estado vigente y al menos una tarjeta de d�bito.
	 */
	private ArrayList<String> cuentasFiltradas;

	/**
	 * variable que guardara el rut consultado desde la vista.
	 */
	private long rutClienteBuscado;
	
	/**
	 * digito verificador de rut de cliente.
	 */
	private char dvCliente;
	
	/**
	 * variable con mail para comprobante cliente.
	 */
	private String emailCliente;
	
	/**
	 * Atributo cliente.
	 */
	private Cliente clienteConsulta;
	
	/**
	 * Flag para identificar los resultados de la busqueda.
	 */
	private String resultadoBusqueda;
	
	/**
	 * listado de tarjetas de debito, ordenados acorde a los numeros de cuenta corriente.
	 */
	private HashMap listaTDD;
	
	/**
	 * TO que contiene informacion de TDD. inicialmente solo con estado y fecha creacion (ultima modificacion).
	 */
	private DatosTarjetaDeDebitoTO informacionTDD;
	/**
	 * Arreglo simple de cuentas corrientes a desplegar	.
	 */
	private ArrayList<SelectItem> cuentasCtes;
	
	/**
	 * Cuenta corriente seleccionada del combobox de cuentas.
	 */
	private String cuentaCteSeleccionada;
	
	/**
	 * Listado de tarjetas de debito por cuenta seleccionada.
	 */
	private ArrayList<SelectItem> tarjetasDeDebito;
	
	/**
	 * Tarjeta de debito seleccionada.
	 */
	private String tarjetaDebitoSeleccionada;
	/**
	 * Nombre capitalizado del cliente.
	 */
	private String nombreCapitalizadoCliente;
	/**
	 * Titulo servicio del Comprobante.
	 */
	private String tituloComprobante;
	
	/**
	 * Managed Bean inyectado que entrega datos del cliente.
	 */
	@ManagedProperty(value = "#{clienteMB}")
	private ClienteMB clienteMB;

	/**
	 * Managed Bean inyectado que entrega datos de la sesi�n.
	 */
	@ManagedProperty(value = "#{sesionMB}")
	private SesionMB sesionMB;

	/**
	 * Managed Bean inyectado que se comunica con la capa de negocios.
	 */
	@ManagedProperty(value = "#{tarjetaDeCreditoModelMB}")
	private TarjetaDeCreditoModelMB tarjetasModelMB;

	/**
	 * Referencia al managed bean utitlitario de los procesos express
	 * (�conos inteligentes).
	 */
	@ManagedProperty(value = "#{iconoInteligenteUtilityMB}")
	private IconoInteligenteUtilityMB iconoInteligenteUtilityMB;

    /**
     * Inyeccion del colaboradorModelMB.
     */
    @ManagedProperty(value = "#{colaboradorModelMB}")
    private ColaboradorModelMB colaboradorMB;
    
    
	/**
	 * Dada una cuenta seleccionada se obtiene las tarjetas de d�bito asociadas
	 * 
	 * <p>Registro de versiones:<ul>
	 * <li>1.0 20/09/2013, Yon Sing Sius. (ExperimentoSocial): Versi�n inicial.</li>
	 * </ul>
	 * 
	 * @return nombre de la vista a desplegar.
	 * @since 1.0
	 */
	public ArrayList<String> getTarjetasDebitoPorCuenta() {
		tarjetasDebitoPorCuenta = new ArrayList<String>();
		getTarjetasDebito();
		if (cuentaSeleccionada != null) {
			for (int i=0; i < tarjetasDebito.size(); i++) {
				if (logger.isDebugEnabled()) {
					logger.debug("[getTarjetasDebitoPorCuenta][" + clienteMB.getRut() + "]" + " '" 
							+ tarjetasDebito.get(i).numCuentaCorriente + "'" + "=" + "'" 
							+ cuentaSeleccionada + "'");
				} 
				if (tarjetasDebito.get(i).numCuentaCorriente.equals(cuentaSeleccionada)) {
					String pan = tarjetasDebito.get(i).numeroTarjeta;
					if (logger.isDebugEnabled()) {
						logger.debug("Se agrega Tarjeta: " +  formatearTarjeta(pan));					
					} 
					tarjetasDebitoPorCuenta.add(formatearTarjeta(pan));
				}
			}
		}
		return tarjetasDebitoPorCuenta;
	}

	/**
	 * Formatea el n�mero de la tarjeta, dejando s�lo los �ltimos d�gitos visibles.
	 * 
	 * <p>Registro de versiones:<ul>
	 * <li>1.0 20/09/2013, Yon Sing Sius. (ExperimentoSocial): Versi�n inicial.</li>
	 * </ul>
	 * 
	 * @param pan n�mero tarjeta de d�bito
	 * @return nombre de la vista a desplegar.
	 * @since 1.0
	 */
	public String formatearTarjeta(String pan) {
		return "XXXX-XXXX-XXXX-" + pan.substring(OCULTARPAN);
	}
	
	public void setTarjetasDebitoPorCuenta(ArrayList<String> tarjetasDebitoPorCuenta) {
		this.tarjetasDebitoPorCuenta = tarjetasDebitoPorCuenta;
	}

	public String getServicioSeleccionado() {
		return servicioSeleccionado;
	}
	
	/**
	 * Retorna el codigo de del servicio seleccionado.
	 * 
	 * <p>Registro de versiones:<ul>
	 * <li>1.0 10/12/2013, Yon Sing Sius. (ExperimentoSocial): Versi�n inicial.</li>
	 * </ul>
	 * 
	 * @return Codigo de servicio
	 * @since 1.1
	 */
	public String getCodServicioSeleccionado() {
		String cod = null;
		if (servicioSeleccionado != null) {
			cod = servicioSeleccionado.split("-")[0];
		}
		return cod;
	}
	
	public String getUrlRedBank() {
		return urlRedBank;
	}

	public void setUrlRedBank(String urlRedBank) {
		this.urlRedBank = urlRedBank;
	}

	public void setServicioSeleccionado(String servicioSeleccionado) {
		this.servicioSeleccionado = servicioSeleccionado;
	}

	public String getTarjetaSeleccionada() {
		return tarjetaSeleccionada;
	}

	public void setTarjetaSeleccionada(String tarjetaSeleccionada) {
		this.tarjetaSeleccionada = tarjetaSeleccionada;
	}

	public String getCuentaSeleccionada() {
		return cuentaSeleccionada;
	}

	public void setCuentaSeleccionada(String cuentaSeleccionada) {
		this.cuentaSeleccionada = cuentaSeleccionada;
	}

	public void setCuentasCorrientes(ArrayList<Cta> cuentasCorrientes) {
		this.cuentasCorrientes = cuentasCorrientes;
	}


	public IconoInteligenteUtilityMB getIconoInteligenteUtilityMB() {
		return iconoInteligenteUtilityMB;
	}

	public void setIconoInteligenteUtilityMB(
			IconoInteligenteUtilityMB iconoInteligenteUtilityMB) {
		this.iconoInteligenteUtilityMB = iconoInteligenteUtilityMB;
	}

	public TarjetaDeCreditoModelMB getTarjetasModelMB() {
		return tarjetasModelMB;
	}

	public void setTarjetasModelMB(TarjetaDeCreditoModelMB tarjetasModelMB) {
		this.tarjetasModelMB = tarjetasModelMB;
	}

	public SesionMB getSesionMB() {
		return sesionMB;
	}

	public void setSesionMB(SesionMB sesionMB) {
		this.sesionMB = sesionMB;
	}

	public ClienteMB getClienteMB() {
		return clienteMB;
	}

	public void setClienteMB(ClienteMB clienteMB) {
		this.clienteMB = clienteMB;
	}

	public ColaboradorModelMB getColaboradorMB() {
		return colaboradorMB;
	}

	public void setColaboradorMB(ColaboradorModelMB colaboradorMB) {
		this.colaboradorMB = colaboradorMB;
	}

	/**
	 * Obtiene las tarjetas de d�bito que est�n asociadas a una cuenta vigente. 
	 * 
	 * <p>Registro de versiones:<ul>
	 * <li>1.0 20/09/2013, Yon Sing Sius. (ExperimentoSocial): Versi�n inicial.</li>
	 * <li>1.0 19/10/2015, Claudia Aracena(SEnTRA) - Juan Sagredo(ing Soft. BCI): se normaliza llamada a ejb, tras normalizacion
	 * de import prohibido InitialContext.</li>
	 * </ul>
	 * 
	 * @return Listado con el detalle de cada tarjeta.
	 * @since 1.0
	 */
	public ArrayList<DetalleTarjeta> getTarjetasDebito() {
		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[getTarjetasDebito] [BCI_INI]");
		}
		if (tarjetasDebito == null) {
			String clasesPermitidas = TablaValores.getValor(TABLA_TARJETAS_ATM, "ClasesPermitidas", "Desc");

			tarjetasDebito = new ArrayList<DetalleTarjeta>();

			TarjetaCliente tarjsATM = null;
			try {
				
				ServiciosTarjetas servTarj = createEjbTarjetas();
				if(servTarj != null){
				tarjsATM = (TarjetaCliente) servTarj.obtenerTarjetasDebito(clienteMB.getRut());
					if(tarjsATM != null && tarjsATM.getTarjeta() != null){
				ArrayList<DetalleTarjeta> tarjetasTmp = 
						new ArrayList<DetalleTarjeta>(Arrays.asList(tarjsATM.getTarjeta()));

			for(int i=0; i < tarjetasTmp.size(); i++ ) {
			DetalleTarjeta det = tarjetasTmp.get(i);
			if (det.getEstado().equals("03") 
					&& verificaCuenta(det.getNumCuentaCorriente())) {
				if(clasesPermitidas != null &&  clasesPermitidas.indexOf(det.getClase()) != -1){
					if (getLogger().isEnabledFor(Level.DEBUG)) {
						getLogger().debug("[getTarjetasDebito][" + clienteMB.getRut() + "] Se agrega tarjeta: " 
								+ formatearTarjeta(tarjetasTmp.get(i).numeroTarjeta));
					}
					tarjetasDebito.add(tarjetasTmp.get(i));
				}
				
				
			}
		}
			} 
					
				}
				
			} 
			catch (Exception e) {
				if (getLogger().isEnabledFor(Level.ERROR)) {
					getLogger().error("[getTarjetasDebito] [Exception][BCI_FINEX]: " +e.getMessage(), e);
				}
				return null;
			}
		}
		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[getTarjetasDebito] [BCI_FINOK]");
		}
		return tarjetasDebito;
	}

	/**
	 * Verifica si un n�mero de cuenta pertenece a una cuenta vigente 
	 * 
	 * <p>Registro de versiones:<ul>
	 * <li>1.0 20/09/2013, Yon Sing Sius. (ExperimentoSocial): Versi�n inicial.</li>
	 * </ul>
	 * 
	 * @param numeroCuenta a verificar 
	 * @return true si es una cuenta vigente, false en caso contrario.
	 * @since 1.0
	 */
	private boolean verificaCuenta(String numeroCuenta) {
		for(int i=0; i<getCuentasCorrientes().size(); i++) {
			if (getCuentasCorrientes().get(i).cuenta.equals(numeroCuenta)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Obtiene las cuentas vigentes de un cliente 
	 * 
	 * <p>Registro de versiones:<ul>
	 * <li>1.0 20/09/2013, Yon Sing Sius. (ExperimentoSocial): Versi�n inicial.</li>
	 * </ul>
	 * 
	 * @return Lista de cuentas vigentes asociadas al cliente.
	 * @since 1.0
	 */
	public ArrayList<Cta> getCuentasCorrientes() {
		if (cuentasCorrientes == null) {
			Cta[] cuentas;
			try {
				cuentas = clienteMB.getCuentas().cuentas;
			} 
			catch (Exception e) {
				if (logger.isEnabledFor(Level.ERROR)) {
					logger.error("[getCuentasCorrientes][" + clienteMB.getRut() + "] Exception: " + e);
				}
				return null;
			}

			ArrayList<Cta> cuentasVigentes = new ArrayList<Cta>();
			for (int i=0; i< cuentas.length; i++){
				if (cuentas[i].getCodEstado().equals("VIG")){
					cuentasVigentes.add(cuentas[i]);
					if (logger.isDebugEnabled()) {
						logger.error("[getCuentasCorrientes][" + clienteMB.getRut() + "] Se agrega cuenta: " 
								+ cuentas[i].getCuenta());
					}

				}
			}
			cuentasCorrientes = cuentasVigentes;
		}
		return cuentasCorrientes;
	}

	/**
	 * Obtiene las cuentas que tengan al menos una tarjeta asociada
	 * 
	 * <p>Registro de versiones:<ul>
	 * <li>1.0 20/09/2013, Yon Sing Sius. (ExperimentoSocial): Versi�n inicial.</li>
	 * </ul>
	 * 
	 * @return Lista con las cuentas filtradas que tengan al menos una tarjeta.
	 * @since 1.0
	 */
	public ArrayList<String> getCuentasFiltradas() {
		if (cuentasFiltradas == null) {
			cuentasFiltradas = new ArrayList<String>();
			ArrayList<DetalleTarjeta> tarjetas = getTarjetasDebito();
			for (int i=0; i < tarjetas.size(); i++) {
				DetalleTarjeta t = tarjetas.get(i);

				if (!cuentasFiltradas.contains(t.getNumCuentaCorriente()) 
						&& verificaCuenta(t.getNumCuentaCorriente())) {
					cuentasFiltradas.add(t.getNumCuentaCorriente());
				}
			}
		}
		return cuentasFiltradas;
	}

	/**
	 * Obtiene el mensaje a desplegar en la p�gina de inicio. 
	 * 
	 * <p>Registro de versiones:<ul>
	 * <li>1.0 20/09/2013, Yon Sing Sius. (ExperimentoSocial): Versi�n inicial.</li>
	 * </ul>
	 * 
	 * @return Lista con las cuentas filtradas que tengan al menos una tarjeta.
	 * @since 1.0
	 */
	public String getBanner() {
		String banner = TablaValores.getValor(TABLA_TARJETAS_ATM, "MensajeBanner", "Desc");
		return banner;
	}

	/**
	 * Dado el numero de tarjeta formateada, retorna el n�mero de tarjeta original
	 * 
	 * <p>Registro de versiones:<ul>
	 * <li>1.0 20/09/2013, Yon Sing Sius. (ExperimentoSocial): Versi�n inicial.</li>
	 * </ul>
	 * 
	 * @return numero de la tarjera sin formatear
	 * @since 1.0
	 */
	public String getNumeroTarjetaSeleccionada() {
		ArrayList<DetalleTarjeta> tarjetas = getTarjetasDebito();
		String tarjeta = "";
		for(int i =0; i<tarjetas.size(); i++) {
			String pan = tarjetas.get(i).numeroTarjeta.substring(OCULTARPAN);
			if (pan.equals(getTarjetaSeleccionada().replace("XXXX-XXXX-XXXX-", ""))) {
				tarjeta = tarjetas.get(i).numeroTarjeta;
			}
		}
		return tarjeta;
	}
	
	/**
	 * Obtiene los servicios o acciones que el cliente 
	 * puede realizar con su tarjeta. Estos servicios
	 * son realizados desde la p�gina de RedBanc.
	 * 
	 * <p>Registro de versiones:<ul>
	 * <li>1.0 20/09/2013, Yon Sing Sius. (ExperimentoSocial): Versi�n inicial.</li>
	 * </ul>
	 * 
	 * @return Arreglo de servicios y sus atributos: [codigo, nombre, subServicio]
	 * @since 1.0
	 */
	public String[][] getServicios() {
		if (servicios == null) {
			int cantSrv = Integer.parseInt(TablaValores.getValor(TABLA_COD_TRANS_REDBANC, 
					"paramGenerales", "cantSrv"));
	
			if (logger.isDebugEnabled()) {
				logger.debug("[obtenerTarjetasDebito] Cantidad de servicios a mostrar: " + cantSrv);
			}
			String[][] servs = new String[cantSrv][CAMPOSSRV];
			for (int i = 0; i < cantSrv; i++) {
				servs[i][POSCODSRV] = TablaValores.getValor(TABLA_COD_TRANS_REDBANC, "srv" + (i + 1),
						"cod");
				servs[i][POSNOMSRV] = TablaValores.getValor(TABLA_COD_TRANS_REDBANC, "srv" + (i + 1),
						"nom");
				servs[i][POSSUBSRV] = TablaValores.getValor(TABLA_COD_TRANS_REDBANC, "srv" + (i + 1),
						"subSrv");
				servs[i][POSIDSRV] = servs[i][POSCODSRV] + "-" + servs[i][POSSUBSRV];
				
				if (logger.isDebugEnabled()) {
					logger.debug("[obtenerTarjetasDebito] Servicio [" + servs[i][1] + "]");
				}
			}
			if (logger.isDebugEnabled()) {
				logger.debug("[obtenerTarjetasDebito] Retornando 'success'");
			}
			servicios = servs;
		}
		return servicios.clone();
	}
	
	/**
	 * Retorna el atributo nombre del servicio seleccionado.
	 * 
	 * <p>Registro de versiones:<ul>
	 * <li>1.0 20/09/2013, Yon Sing Sius. (ExperimentoSocial): Versi�n inicial.</li>
     * <li>1.1 20/09/2013, Yon Sing Sius. (ExperimentoSocial): Se agrega  subcodigo.</li>
     * <li>1.2 19/10/2017 Andr�s Silva H.(SEnTRA) - Ricardo Carrasco (Ing. Soft. BCI): Se modifica implementaci�n para proyecto Entrust.</li>
	 * </ul>
	 * 
	 * @return String con el nombre del servicio
	 * @since 1.0
	 */
	public String getServicioNombre() {
		String nombre = null;
		switch (Integer.valueOf(servicioSeleccionado)) {
		case TIPO_SOLICITUD_HABILITACION:
				nombre = this.DESC_SOLICITUD_HABILITACION;
				break;
		case TIPO_SOLICITUD_DESHABILITACION:
				nombre = this.DESC_SOLICITUD_DESHABILITACION;
				break;
		}
		return nombre;
	}
	
	/**
	 * Retorna el codigo de subservicio del servicio seleccionado.
	 * 
	 * <p>Registro de versiones:<ul>
	 * <li>1.0 20/09/2013, Yon Sing Sius. (ExperimentoSocial): Versi�n inicial.</li>
     * <li>1.1 20/09/2013, Yon Sing Sius. (ExperimentoSocial): Se modifica obtenci�n del subcodigo.</li>
	 * </ul>
	 * 
	 * @return Codigo de subservicio
	 * @since 1.0
	 */
	public String getCodSubServicio() {
		String subCod = null;
		if (servicioSeleccionado != null) {
			subCod = servicioSeleccionado.split("-")[1];
		}
		return subCod;
	}
	
	/**
	 * Metodo encargado de realizar busqueda por rut de cliente.
	 * Registro de versiones:<ul>
	 * <li>1.0 19/10/2015 Claudia Aracena(SEnTRA)-Juan Sagredo (ing Soft. BCI): version inicial.</li>
	 * </ul>
	 * @since 1.2
	 */
	public void buscarClientePorRut(){
		if(getLogger().isEnabledFor(Level.INFO)){
			getLogger().info("[buscarClientePorRut][BCI_INI]");
		}
		FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest request= (HttpServletRequest)context.getExternalContext().getRequest();
        try{
        	if(getLogger().isEnabledFor(Level.DEBUG)){
        	getLogger().debug("[buscarClientePorRut] request.getParameter(contenidoform:central:rutClienteBuscado)->" + request.getParameter("contenidoform:central:rutClienteBuscado")
        			+ " contenidoform:central:dvClienteBuscado-> "+request.getParameter("contenidoform:central:dvClienteBuscado"));
        	}
        	if(request.getParameter("contenidoform:central:rutClienteBuscado")!=null || request.getParameter("rutClienteBuscado") != null){
        		if(request.getParameter("rutClienteBuscado") != null){
        			rutClienteBuscado = Long.parseLong(request.getParameter("rutClienteBuscado"));
        			dvCliente = request.getParameter("dvClienteBuscado")==null?null:request.getParameter("dvClienteBuscado").charAt(0);
        		}
        		else{
        			rutClienteBuscado = Long.parseLong(request.getParameter("contenidoform:central:rutClienteBuscado"));
        			dvCliente = request.getParameter("contenidoform:central:dvClienteBuscado")==null?null:request.getParameter("contenidoform:central:dvClienteBuscado").charAt(0);
        		}
    			
    			this.cuentasCtes = null;
    			this.listaTDD = null;
    			this.emailCliente = null;
    			informacionTDD = null;
    			this.resultadoBusqueda = FLAG_OK;
    			try{
    				if(getLogger().isEnabledFor(Level.DEBUG)){
        				getLogger().debug("[buscarClientePorRut]se procede a instanciacion de cliente con rut [" + rutClienteBuscado + "-" + dvCliente+"]");
        			}
    				Cliente cliente = new Cliente(rutClienteBuscado, dvCliente);
    				if(cliente != null){
    				cliente.setDatosBasicos();
    				
    				this.clienteConsulta = cliente;
        				this.nombreCapitalizadoCliente =  StringUtil.capitalizarString(clienteConsulta.getFullName());
    				RetornoTipCli retornoTipCli = cliente.consultaDatosGeneralesDeCliente();
    				if(retornoTipCli != null){
    					boolean hayPermiso = colaboradorMB.tienePermisoSobreElCliente(retornoTipCli,rutClienteBuscado);
        				if(getLogger().isEnabledFor(Level.DEBUG)){
            					getLogger().debug("[buscarClientePorRut] hay permiso sobre cliente? [" + hayPermiso + "]");
        				}
        				if(!hayPermiso){
            				resultadoBusqueda = FLAG_NOPERMISO;
        				}
    				}
    				else{
        					resultadoBusqueda =FLAG_NOOK;
    				}
    				}
    				
    				this.emailCliente = obtenerEmailRegistrado();
    			}
    			catch(Exception e){
    				String texterror = e.getLocalizedMessage();
    				nombreCapitalizadoCliente = null;
                	if (texterror.toUpperCase().indexOf(CLIENTE_NO_EXISTE) != -1) {
                		if(getLogger().isEnabledFor(Level.ERROR)){
                			getLogger().error("[buscarClientePorRut][Exception] cliente no existe");
                		}
                		resultadoBusqueda = FLAG_NOCLIENTE;
                	}
                	else{
                		resultadoBusqueda = FLAG_NOOK;
                	}
                	clienteConsulta = null;
                	if(getLogger().isEnabledFor(Level.ERROR)){
            			getLogger().error("[buscarClientePorRut] [Exception] " + e.getMessage(),e);
            		}
    			}
    			
            }
        }
        catch(Exception e){
        	if(getLogger().isEnabledFor(Level.ERROR)){
    			getLogger().error("[buscarClientePorRut] error en la obtencion de parametros [Exception] " + e.getMessage(),e);
    		}
        }
		
		if(getLogger().isEnabledFor(Level.INFO)){
			getLogger().info("[buscarClientePorRut][BCI_FINOK] resultado: " 
					+ resultadoBusqueda);
		}
	}
	
	/**
	 * Metodo encargado de obtener el listado actualizado de tarjetas de debito.
	 * Registro de versiones:<ul>
	 * <li>1.0 19/10/2015 Claudia Aracena(SEnTRA) - Juan Sagredo(ing Soft. BCI): version inicial.</li>
	 * </ul>
	 * @since 
	 */
	public void obtenerTarjetaDeDebitoPorCtaCte(){
		if(getLogger().isEnabledFor(Level.INFO)){
			getLogger().info("[obtenerTarjetaDeDebitoPorCtaCte][BCI_INI]");
		}
		if (listaTDD != null) {
			String cuentaIterada = "";
			if(getLogger().isEnabledFor(Level.DEBUG)){
				getLogger().debug("[obtenerTarjetaDeDebitoPorCtaCte] cuenta corriente seleccionada: " + cuentaCteSeleccionada);
			}
			ArrayList<String> listadoTDD = null;
			Iterator it = listaTDD.entrySet().iterator();
			if(getLogger().isEnabledFor(Level.DEBUG)){
				getLogger().debug("[obtenerTarjetaDeDebitoPorCtaCte] se itera HashMap listaTDD y se limpia select de tdd.");
			}
			this.tarjetasDeDebito = new ArrayList<SelectItem>();
			while(it.hasNext()){
				Map.Entry elemento = (Map.Entry)it.next();
				cuentaIterada = (String)elemento.getKey(); //cuentas
				if(cuentaIterada.equals(cuentaCteSeleccionada)){
					listadoTDD = (ArrayList<String>)elemento.getValue();
					if(listadoTDD != null){
					for(int i = 0; i < listadoTDD.size(); i++){
						if(getLogger().isEnabledFor(Level.DEBUG)){
							getLogger().debug("[obtenerTarjetaDeDebitoPorCtaCte] se han encontrado TDD para cuenta seleccionada[" + cuentaCteSeleccionada + "]");
						}
						String numeroTarjeta = (String)listadoTDD.get(i);
						tarjetasDeDebito.add(new SelectItem(numeroTarjeta,formatearTarjeta(numeroTarjeta)));
					}
					tarjetaDebitoSeleccionada = (String)listadoTDD.get(0);
					}
					
					if(getLogger().isEnabledFor(Level.DEBUG)){
						getLogger().debug("[obtenerTarjetaDeDebitoPorCtaCte] tarjeta de debito seleccionada es [" + tarjetaDebitoSeleccionada + "]");
					}
		
				}
			}
			if(getLogger().isEnabledFor(Level.DEBUG)){
				getLogger().debug("[obtenerTarjetaDeDebitoPorCtaCte] fin while iteracion de mapa cuentas");
			}
		}
		else{
			if(getLogger().isEnabledFor(Level.DEBUG)){
				getLogger().debug("[obtenerTarjetaDeDebitoPorCtaCte] no existen elementos en sesion.");
			}
		}
		if(getLogger().isEnabledFor(Level.INFO)){
			getLogger().info("[obtenerTarjetaDeDebitoPorCtaCte] [BCI_FINOK]");
		}
	}
	
	/**
	 * Metodo que obtiene tarjetas de debito asociadas a cuentas vigentes.
	 * Registro de versiones:<ul>
	 * <li>1.0 19/10/2015 Claudia Aracena(SEnTRA) - Juan Sagredo(ing Soft. BCI): version inicial</li>
	 * </ul>
	 * @since 1.2
	 */
	public void obtenerTarjetasDebito(){
		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[obtenerTarjetasDebito] [BCI_INI]");
		}
		if (listaTDD == null) {

			TarjetaCliente tarjsATM = null;
			ServiciosTarjetas servTarj = createEjbTarjetas();
			if(servTarj != null){
				try {
					String clasesPermitidas = TablaValores.getValor(TABLA_TARJETAS_ATM, "ClasesPermitidas", "Desc");
					if (getLogger().isEnabledFor(Level.DEBUG)) {
						getLogger().debug("[obtenerTarjetasDebito] se filtra por clasesPermitidas["+clasesPermitidas+"]");
					}
					tarjsATM = (TarjetaCliente) servTarj.obtenerTarjetasDebito(rutClienteBuscado);
					ArrayList<DetalleTarjeta> tarjetasTmp = 
							new ArrayList<DetalleTarjeta>(Arrays.asList(tarjsATM.getTarjeta()));
					listaTDD = new HashMap();
					this.cuentasCtes = new ArrayList<SelectItem>();
					this.cuentasCtes.add(new SelectItem(VALUE_DEFAULT_COMBO,TEXTO_DEFAULT_COMBO));
					this.tarjetasDeDebito = new ArrayList<SelectItem>();
					this.tarjetasDeDebito.add(new SelectItem(VALUE_DEFAULT_COMBO,TEXTO_DEFAULT_COMBO));
					for(int i=0; i < tarjetasTmp.size(); i++ ) {
						DetalleTarjeta det = tarjetasTmp.get(i);
						if (!det.getEstado().equals(ESTADO_TARJETA_ACTIVA) ) {//tarjeta debe estar activa
							continue;
						}	
						if(clasesPermitidas != null && clasesPermitidas.indexOf(det.getClase()) != -1){//debe estar configurada en las clases para despliegue.
						if (getLogger().isEnabledFor(Level.DEBUG)) {
							getLogger().debug("[obtenerTarjetasDebito] detalle tarjeta ["+det.toString()+"]");
						}
						//cuando ya se encuentras las cta ctes en el map, se agrega la tdd dentro del registro map correspondiente
		                if (listaTDD.get(det.getNumCuentaCorriente()) != null) {
		                    ArrayList<String> tarjetasDeb = (ArrayList<String>)listaTDD.get(det.getNumCuentaCorriente());  
		                    tarjetasDeb.add(det.getNumeroTarjeta());
		                    listaTDD.put(det.getNumCuentaCorriente(), tarjetasDeb);
		                }
		                else {
		                	//se agrega cuentas corrientes no repetidas a listado simple de cuentas corrientes.
		                	String cuentaCte = StringUtil.sacaCeros(det.getNumCuentaCorriente());
		                	if(cuentaCte != null && cuentaCte.trim().equals("")){
		                		this.cuentasCtes.add(new SelectItem(det.getNumCuentaCorriente(),det.getNumCuentaCorriente()));
		                	}
		                	else{
		                	this.cuentasCtes.add(new SelectItem(det.getNumCuentaCorriente(),StringUtil.sacaCeros(det.getNumCuentaCorriente())));
		                	}
		                	
		                    ArrayList<String> tarjetasDeb = new ArrayList<String>();
		                    tarjetasDeb.add(det.getNumeroTarjeta());
		                    listaTDD.put(det.getNumCuentaCorriente(), tarjetasDeb);
		                }
						if (getLogger().isEnabledFor(Level.DEBUG)) {
							getLogger().debug("[obtenerTarjetasDebito]rut[" + rutClienteBuscado + "] Se agrega tarjeta: " 
									+ formatearTarjeta(tarjetasTmp.get(i).numeroTarjeta) + " para cuenta : "+ det.getNumCuentaCorriente());
						}
						}
					}
					this.cuentaCteSeleccionada = (String)cuentasCtes.get(0).getValue();
				} 
				catch (Exception e) {
					if (getLogger().isEnabledFor(Level.ERROR)) {
						getLogger().error("[obtenerTarjetasDebito] [Exception] error en obtencion de tdds: " + e);
					}
				}
			}
			
		}
		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[obtenerTarjetasDebito] [BCI_FINOK]");
		}
	}
	
	
	/**
	 * Metodo encargado de realizar cambio de estado de tarjeta de debito internacional.
	 * Registro de versiones:<ul>
	 * <li>1.0 19/10/2015 Claudia Aracena(SEnTRA) - Juan Sagredo(ing Soft. BCI): version inicial. </li>
         * <li>1.1 03/02/2016 Claudia Aracena(SEnTRA) - Juan Sagredo(ing Soft. BCI): se cambia llamada a metodo de envio de correo.</li>
	 * </ul>
	 * @since 1.2
	 */
	public void cambiaEstadoTDD(){
		if(getLogger().isEnabledFor(Level.INFO)){
			getLogger().info("[cambiaEstadoTDD] [BCI_INI]");
		}
		try{
	        FacesContext context = FacesContext.getCurrentInstance();
	        HttpServletRequest request= (HttpServletRequest)context.getExternalContext().getRequest();
	        String numCuentaCte ="";
			String numTDD = "";
			boolean bloquear = true;
			
			//numero cuenta corriente
	        if(request.getParameter("numCuentaCte")!= null){
	        	numCuentaCte =(String) request.getParameter("numCuentaCte"); 
	        }
	        else{
	        	numCuentaCte =(String) request.getParameter("contenidoform:central:numCuentaCte");
	        }
	        //numero de tarjeta de debito
	        if(request.getParameter("numTDD")!= null){
	        	numTDD =(String) request.getParameter("numTDD"); 
	        }
	        else{
	        	numTDD =(String) request.getParameter("contenidoform:central:numTDD");
	        }
	        if(informacionTDD == null){//si no existe informacion previa de la tdd, entonces
	        	bloquear = false;  //Habilitar
	        }
	        else{//si existe informacion, esta bloqueada? desbloqueo, si no, bloqueo.
	        	bloquear =informacionTDD.getEstadoTarjeta().trim().equals(VALUE_ESTADO_BLOQUEADO)?false:true;
	        }
	        
	        if(getLogger().isEnabledFor(Level.DEBUG)){
				getLogger().debug("[cambiaEstadoTDD]numCuentaCte [" + numCuentaCte +"] numTDD [" + numTDD
				+"] bloquear [" + bloquear + "]");
			}
			
			ServiciosSeguridad srv = createEjbSeguridad();
			if(srv != null){
				try{
					if(numTDD != null && numTDD.length()>0){
						if(getLogger().isEnabledFor(Level.DEBUG)){
							getLogger().debug("[cambiaEstadoTDD] numero Tarjeta debito [" + formatearTarjeta(numTDD) + "]");
						}
					srv.bloquearTarjetaDebitoInternacional(rutClienteBuscado, numCuentaCte, numTDD, bloquear);
						String codEventoNegocio = bloquear?"BLOQUEA":"DESBLOQUEA";
						String subCodEventoNegocio = bloquear?"DESTDB":"HABTDB";
						
						journalizar(codEventoNegocio, subCodEventoNegocio, "TDB", "P", numCuentaCte,numTDD);
					
					if(emailCliente != null && !emailCliente.trim().equals("")){
						String solicitud = bloquear? "Deshabilitaci�n" : "Habilitaci�n";
						if(getLogger().isEnabledFor(Level.DEBUG)){
							getLogger().debug("[cambiaEstadoTDD]se enviara comprobante de->  solicitud [" + solicitud + "] canal [" + CANAL_PERSONAS + "] emailCliente[" + emailCliente+ "]");
						}
						if(clienteConsulta.getPerfilTbanc() != null){ //es cliente TBanc
							enviarCorreo(CANAL_TBANC, emailCliente, new Date(), numCuentaCte, numTDD, solicitud);
						}
						else{//es cliente webBci
							enviarCorreo(CANAL_PERSONAS, emailCliente, new Date(), numCuentaCte, numTDD, solicitud);
						}
					}
					resultadoBusqueda=FLAG_SUCCESS;
				}
					else{
						if(getLogger().isEnabledFor(Level.DEBUG)){
							getLogger().debug("[cambiaEstadoTDD]Numero tarjeta debito es null, error en aplicacion.");
						}
						resultadoBusqueda = FLAG_NOSUCCESS;
					}
					
				}
				catch(Exception e){
					if(getLogger().isEnabledFor(Level.ERROR)){
						getLogger().error("[cambiaEstadoTDD] [Exception] error al realizar cambio estado TDD->" + e.getMessage(),e);
					}
					resultadoBusqueda = FLAG_NOSUCCESS;
				}
			}
		}
		catch(Exception e){
			if(getLogger().isEnabledFor(Level.ERROR)){
				getLogger().error("[cambiaEstadoTDD] [Exception] error al recuperar informacion de TDD --> " + e.getMessage(),e);
			}
		}
		if(getLogger().isEnabledFor(Level.INFO)){
			getLogger().info("[cambiaEstadoTDD] [BCI_FINOK]");
		}
	}
	
	/**
	 * Metodo encargado de realizar envio de correo.
	 * registro de versiones:<ul>
	 * <li>1.0 19/10/2015 Claudia Aracena(SEnTRA) - Juan Sagredo (ing soft. BCI): version inicial.</li>
	 * <li>1.1 03/02/2016 Claudia Aracena(SEnTRA) - Juan Sagredo (ing soft. BCI): se cambia implementacion, para pasar
	 * por nuevo componente para envios de correo.</li>
	 * </ul>
	 * @param canal canal del cliente.
	 * @param correo mail del destinatario.
	 * @param fecha fecha de bloqueo.
	 * @param cuenta cuenta asociada a la tdd
	 * @param pan cuenta tdd.
	 * @param solicitud glosa de tipo de solicitud.
	 * @since 1.2
	 */
    private void enviarCorreo(String canal, String correo, Date fecha, String cuenta, String pan, String solicitud) {
    	if(getLogger().isEnabledFor(Level.INFO)){
			getLogger().info("[enviarCorreo] [BCI_INI] canal[" + canal + "] correo [" + correo + "]");
		}
            ParametrosCorreoBloqueoTarjetaDebito paramsMail = new ParametrosCorreoBloqueoTarjetaDebito(); 
            
            paramsMail.setAsunto(TablaValores.getValor(TABLA_TARJETAS_ATM, "subject", "Desc"));
            paramsMail.setCorreoDestino(correo);
            paramsMail.setCanalCliente(canal);
            paramsMail.setRutClienteFormateado(this.clienteConsulta.getFullRut());
            paramsMail.setNombreCliente(nombreCapitalizadoCliente);
            paramsMail.setNumeroCuenta(cuenta);
            paramsMail.setNumeroPan(formatearTarjeta(pan));
            paramsMail.setSolicitud(solicitud);
            paramsMail.setFechaBloqueo(FechasUtil.convierteDateAString(fecha, "dd/MM/yyyy HH:mm"));
            
            
            try {
            	ServicioMensajeriaUtilityMB servicioMensajeriaUtilityMB = new ServicioMensajeriaUtilityMB();
				servicioMensajeriaUtilityMB.envioCorreo(paramsMail, SERVICIO_ENVIO_CORREO);
			} 
            catch (Exception e) {
				if(getLogger().isEnabledFor(Level.ERROR)){
	    			getLogger().error("[enviarCorreo][Exception] error ->" + e.getMessage(),e);
	    		}
			}
            if(getLogger().isEnabledFor(Level.DEBUG)){
    			getLogger().debug("[enviarCorreo] to[" + correo + "] ");
    		}
            
        
        if(getLogger().isEnabledFor(Level.INFO)){
			getLogger().info("[enviarCorreo] [BCI_FINOK]");
		}
    }
    
	
	/**
	 * Metodo encargado de obtener el email del cliente desde el servicio direcciones.
	 * Registro de versiones:<ul>
	 * <li>1.0 19/10/2015 Claudia Aracena(SEnTRA) - Juan Eduardo Sagredo(Ing soft. BCI): version inicial.</li>
	 * </ul>
	 * @return String
	 * @since 1.2
	 */
	private String obtenerEmailRegistrado() {
		if(getLogger().isEnabledFor(Level.INFO)){
			getLogger().info("[obtenerEmailRegistrado][BCI_INI]");
		}
		String email = "";
		DireccionClienteBci mail = null;
		DireccionClienteBci[] direcciones = null;
		ServiciosDirecciones serviciosDirecciones = createEjbDirecciones();
		if(serviciosDirecciones != null){
			try {
				if(getLogger().isEnabledFor(Level.DEBUG)){
					getLogger().debug("[obtenerEmailRegistrado]se obtendra direcciones del cliente [" + rutClienteBuscado + "]");
				}
				direcciones = serviciosDirecciones.getAddressBci(rutClienteBuscado);
				if (direcciones != null) {
					for (int i = 0; mail == null && i < direcciones.length; i++) {
						if (direcciones[i].tipoDireccion == TIPO_EMAIL){
							mail = direcciones[i];
						}
					}	
				}
			} 
			catch (Exception e) {
				if(getLogger().isEnabledFor(Level.ERROR)){
					getLogger().error("[obtenerEmailRegistrado][Exception] error al obtener direcciones cliente ["+e.getMessage() + "]",e);
				}
			}
		}

		if (mail != null && mail.direccion != null){
			email = mail.direccion.trim();
		}
		if(getLogger().isEnabledFor(Level.INFO)){
			getLogger().info("[obtenerEmailRegistrado][BCI_FINOK] retorna E-mail: ["+email + "]");
		}
		return email;
	}
	
	
	/**
	 * Metodo encargado de generar instancia de ejb servicio direcciones.
	 * Registro de versiones:<ul>
	 * <li>1.0 19/10/2015 Claudia Aracena(SEnTRA) - Juan Sagredo(ing Soft. BCI): version inicial.</li>
	 * </ul>
	 * @return ServiciosDirecciones instancia ejb serviciosDirecciones.
	 * @since 1.2
	 */
	private ServiciosDirecciones createEjbDirecciones(){
		if(getLogger().isEnabledFor(Level.INFO)){
			getLogger().info("[createEjbDirecciones] [BCI_INI]");
		}
		ServiciosDireccionesHome serviciosDireccionesHome = null;
		try {
			serviciosDireccionesHome = (ServiciosDireccionesHome) EnhancedServiceLocator.getInstance().getHome(
			   "wcorp.serv.direcciones.ServiciosDirecciones",ServiciosDireccionesHome.class);
			ServiciosDirecciones services = null;
			if(serviciosDireccionesHome != null){
				services = serviciosDireccionesHome.create();
			}
			if(getLogger().isEnabledFor(Level.INFO)){
				getLogger().info("[createEjbDirecciones] [BCI_FINOK]");
			}
			return services;
		}
		catch (Exception ex) {
			if(getLogger().isEnabledFor(Level.ERROR)){
				getLogger().error("[createEjbDirecciones][Exception][BCI_FINEX] [" + ex.getMessage() + "]",ex);
			}
			return null;
		}
	}
	
	/**
	 * Metodo encargado de obtener instancia de ejb tarjetas.
	 * Registro de versiones:<ul>
	 * <li>1.0 19/10/2015 Claudia Aracena(SEnTRA) - Juan Sagredo(Ing Soft. BCI): version inicial.</li>
	 * </ul>
	 * @return ServiciosTarjetas instancia ejb.
	 * @since 1.2
	 */
	private ServiciosTarjetas createEjbTarjetas(){
		if(getLogger().isEnabledFor(Level.INFO)){
			getLogger().info("[createEjbTarjetas] [BCI_INI]");
		}
		ServiciosTarjetasHome serviciosTarjetasHome = null;
		try {
			serviciosTarjetasHome = (ServiciosTarjetasHome) EnhancedServiceLocator.getInstance().getHome(
			   "wcorp.serv.tarjetas.ServiciosTarjetas",ServiciosTarjetasHome.class);
			ServiciosTarjetas services = null;
			if(serviciosTarjetasHome != null){
				services = serviciosTarjetasHome.create();
			}
			if(getLogger().isEnabledFor(Level.INFO)){
				getLogger().info("[createEjbTarjetas] [BCI_FINOK]");
			}
			return services;
		} 
		catch (Exception ex) {
			if(getLogger().isEnabledFor(Level.ERROR)){
				getLogger().error("[createEjbTarjetas][Exception][BCI_FINEX] [" + ex.getMessage() + "]",ex);
			}
			return null;
		}
    }

	/**
	 * Metodo encargado de crear instancia de serviciosSeguridad.
	 * Registro de versiones:<ul>
	 * <li>1.0 19/10/2015 Claudia Aracena(SEnTRA) - Juan Sagredo(ing Soft. BCI): version inicial.</li>
	 * </ul>
	 * @return ServiciosSeguridad instancia ejb
	 * @since 1.2
	 */
	private ServiciosSeguridad createEjbSeguridad(){
		if(getLogger().isEnabledFor(Level.INFO)){
			getLogger().info("[createEjbSeguridad] [BCI_INI]");
		}
		ServiciosSeguridadHome srvSeguridadHome = null;
		try {
			srvSeguridadHome = (ServiciosSeguridadHome) EnhancedServiceLocator.getInstance().getHome(
			   "wcorp.serv.seguridad.ServiciosSeguridad",ServiciosTarjetasHome.class);
			ServiciosSeguridad services = null;
			if(srvSeguridadHome != null){
				services = srvSeguridadHome.create();
			}
			if(getLogger().isEnabledFor(Level.INFO)){
				getLogger().info("[createEjbSeguridad] [BCI_FINOK]");
			}
			return services;
		} 
		catch (Exception ex) {
			if(getLogger().isEnabledFor(Level.ERROR)){
				getLogger().error("[createEjbSeguridad][Exception][BCI_FINEX] [" + ex.getMessage() + "]",ex);
			}
			return null;
		}
	}
	
	/**
	 * Metodo encargado de realizar la journalizacion correspondiente para activar o desactivar tarjetas
	 * de debito en el extranjero.
	 * Registro de versiones:<ul>
	 * <li>1.0 19/10/2015 Claudia Aracena (SEnTRA) - Juan Sagredo(ing Soft. BCI): version inicial.</li>
	 * <li>1.1 24/12/2015 Claudia Aracena (SEnTRA) - Juan Sagredo(ing Soft. BCI): se modifica clave secundaria y nombre de digito
	 * verificador de rut de cliente.</li>
	 * </ul>
	 * @param codEventoNegocio codigo evento negocio.
	 * @param subCodEventoNegocio sub codigo evento negocio.
	 * @param idProducto identificador de producto.
	 * @param estado a enviar
	 * @param clavePrincipal clave principal.
	 * @param claveSecundaria clave secundaria.
	 * @since 1.2
	 */
    private void journalizar(String codEventoNegocio, String subCodEventoNegocio, String idProducto, String estado, String clavePrincipal, String claveSecundaria) {
        if(getLogger().isEnabledFor(Level.INFO)){
            getLogger().info("[journalizar][BCI_INI] codEventoNegocio["+codEventoNegocio+"] subCodEventoNegocio["+subCodEventoNegocio
            		+"]idProducto["+idProducto+"]estado["+estado+"]clavePrincipal["+clavePrincipal+"]claveSecundaria["+claveSecundaria+"]");
        }
    	try{          
            if(getLogger().isEnabledFor(Level.DEBUG)){
                getLogger().debug("[journalizar] codEventoNegocio [" + codEventoNegocio
                		+"] subCodEventoNegocio [" + subCodEventoNegocio
                		+"] idProducto [" + idProducto
                		+"] estado [" + estado
                		+"] rutOperador [" + colaboradorMB.getRut()
                		+"] idCanal [" + sesionMB.getCanalId()
                		+"] idOperadorBci [" + colaboradorMB.getUsuario() 
                		+"]");
            }

            HashMap datos = new HashMap();
            datos.put("codEventoNegocio", codEventoNegocio);
            datos.put("subCodEventoNegocio", subCodEventoNegocio);
            datos.put("idProducto", idProducto);
            datos.put("clavePrincipal", clavePrincipal);
            datos.put("claveSecundaria", claveSecundaria);
            
            datos.put("rutOperador",colaboradorMB.getRut());
            datos.put("rutCliente",rutClienteBuscado);
            datos.put("dvCliente", dvCliente);
            
            datos.put("idCanal", sesionMB.getCanalId()); 
            datos.put("idOperadorBci", colaboradorMB.getUsuario());
            
            HttpServletRequest request =(HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest(); 
            String ipAddress = request.getHeader(FORWARD_IP);
            if(getLogger().isEnabledFor(Level.DEBUG)){
            	getLogger().debug("[journalizar] X-FORWARDED-FOR -> [" + ipAddress + "]");
            }
            if (ipAddress == null || ipAddress.length() == 0 || TEXTO_DESCONOCIDO.equalsIgnoreCase(ipAddress)) {  
            	if(getLogger().isEnabledFor(Level.DEBUG)){
            		getLogger().debug("[journalizar] IP address X-FORWARDED-FOR es null.");
            	}
                ipAddress = request.getRemoteAddr();
            }
            datos.put("idMedio", ipAddress);
            if(estado!= null){
           	 datos.put("estado", estado);
            }

            if(getLogger().isEnabledFor(Level.DEBUG)){
                getLogger().debug("[journalizar] datos: " + datos);
            }
            Journalist.getInstance().publicar(datos);
        }
        catch (Exception ex) {
            if(getLogger().isEnabledFor(Level.ERROR)){
                getLogger().error("[journalizar][Exception] "
                        + ex.getMessage(), ex);
            }
        } 
        
        if(getLogger().isEnabledFor(Level.INFO)){
            getLogger().info("[journalizar][BCI_FINOK]");
        }
    }
    
	/**
	 * Metodo encargado de retornar instancia de logger.
	 * Registro de versiones:<ul>
	 * <li>1.0 19/10/2015 Claudia Aracena(SEnTRA) - Juan Sagredo(ing Soft. BCI): version inicial.</li>
	 * </ul>
	 * @return Logger instancia log.
	 * @since 1.2
	 */
	public Logger getLogger() {
		if(logger==null){
			this.logger = Logger.getLogger(this.getClass());
		}
		return logger;
	}

	public long getRutClienteBuscado() {
		return rutClienteBuscado;
	}

	public void setRutClienteBuscado(long rutClienteBuscado) {
		this.rutClienteBuscado = rutClienteBuscado;
	}

	public char getDvCliente() {
		return dvCliente;
	}

	public void setDvCliente(char dvCliente) {
		this.dvCliente = dvCliente;
	}
	
	public String getEmailCliente() {
		return emailCliente;
	}

	public void setEmailCliente(String emailCliente) {
		this.emailCliente = emailCliente;
	}

	public Cliente getClienteConsulta() {
		return clienteConsulta;
	}

	public void setClienteConsulta(Cliente clienteConsulta) {
		this.clienteConsulta = clienteConsulta;
	}

	public String getResultadoBusqueda() {
		return resultadoBusqueda;
	}

	public void setResultadoBusqueda(String resultadoBusqueda) {
		this.resultadoBusqueda = resultadoBusqueda;
	}

	public HashMap getListaTDD() {
		return listaTDD;
	}

	public void setListaTDD(HashMap listaTDD) {
		this.listaTDD = listaTDD;
	}

	public ArrayList<SelectItem> getCuentasCtes() {
		return cuentasCtes;
	}

	public void setCuentasCtes(ArrayList<SelectItem> cuentasCtes) {
		this.cuentasCtes = cuentasCtes;
	}

	public String getNombreCapitalizadoCliente() {
		return nombreCapitalizadoCliente;
	}

	public void setNombreCapitalizadoCliente(String nombreCapitalizadoCliente) {
		this.nombreCapitalizadoCliente = nombreCapitalizadoCliente;
	}

	public String getCuentaCteSeleccionada() {
		return cuentaCteSeleccionada;
	}

	public void setCuentaCteSeleccionada(String cuentaCteSeleccionada) {
		this.cuentaCteSeleccionada = cuentaCteSeleccionada;
	}

	public ArrayList<SelectItem> getTarjetasDeDebito() {
		return tarjetasDeDebito;
	}

	public void setTarjetasDeDebito(ArrayList<SelectItem> tarjetasDeDebito) {
		this.tarjetasDeDebito = tarjetasDeDebito;
	}

	public String getTarjetaDebitoSeleccionada() {
		return tarjetaDebitoSeleccionada;
	}

	public void setTarjetaDebitoSeleccionada(String tarjetaDebitoSeleccionada) {
		this.tarjetaDebitoSeleccionada = tarjetaDebitoSeleccionada;
	}

	/**
	 * Metodo que retorna la informacion de tarjetas de debito.
	 * Registro de versiones:<ul>
	 * <li>1.0 19/10/2015 Claudia Aracena (SEnTRA) - Juan Sagredo(ing Soft. BCI): version inicial.</li>
	 * </ul>
	 * @param numTarjeta numero de tarjeta tdd.
	 * @return DatosTarjetaDeDebitoTO objeto con informacion de tarjeta.
	 * @since 1.2
	 */
	public DatosTarjetaDeDebitoTO getInformacionTDD(String numTarjeta) {
		if(getLogger().isEnabledFor(Level.INFO)){
			getLogger().info("[getInformacionTDD] [BCI_INI]  numTarjeta : " + numTarjeta);
		}
		if(informacionTDD == null && (numTarjeta != null && !numTarjeta.trim().equals(""))){
			if(getLogger().isEnabledFor(Level.DEBUG)){
				getLogger().debug("[getInformacionTDD] debe consultar estado de tarjeta -> " + formatearTarjeta(numTarjeta));
			}
			ServiciosSeguridad srvSeguridad = createEjbSeguridad();
			if(srvSeguridad != null){
				try {
					informacionTDD = srvSeguridad.consultaInfoTDD(numTarjeta, rutClienteBuscado);
					if(informacionTDD != null){
						if(getLogger().isEnabledFor(Level.DEBUG)){
							getLogger().debug("[getInformacionTDD] datos obtenidos: estado[" + informacionTDD.getEstadoTarjeta() + "] fecha["+informacionTDD.getFechaCreacion()+"]");
				} 
					}
					else{
						if(getLogger().isEnabledFor(Level.DEBUG)){
							getLogger().debug("[getInformacionTDD] Sin informacion sobre la TDD consultada.");
				}
			}
					
		}
				catch (Exception e) {
					if(getLogger().isEnabledFor(Level.ERROR)){
						getLogger().error("[getInformacionTDD] [Exception] " + e.getMessage(),e);
		}
	}
	}
	}
	if(getLogger().isEnabledFor(Level.INFO)){
			getLogger().info("[getInformacionTDD] [BCI_FINOK]");
	}
		return informacionTDD;
	}

	public void setInformacionTDD(DatosTarjetaDeDebitoTO informacionTDD) {
		this.informacionTDD = informacionTDD;
	}

	public DatosTarjetaDeDebitoTO getInformacionTDD() {
		return informacionTDD;
	}

	public String getTituloComprobante() {
		return tituloComprobante;
	}

	public void setTituloComprobante(String tituloComprobante) {
		this.tituloComprobante = tituloComprobante;
	}

	public Date getFechaSolicitud() {
		return fechaSolicitud;
	}

	public void setFechaSolicitud(Date fechaSolicitud) {
		this.fechaSolicitud = fechaSolicitud;
	}


	
}
