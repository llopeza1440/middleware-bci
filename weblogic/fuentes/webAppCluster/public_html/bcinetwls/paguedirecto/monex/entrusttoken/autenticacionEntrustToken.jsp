<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean" %>

<html class="no-js">
	<head>
		<title>Bci.cl/personas</title>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="description" content="">
		<meta name="viewport" content="width=device-width,initial-scale=1">
		<link href="/bcinetwls/common/css/frames.css" rel="stylesheet" type="text/css">
		<script type="text/javascript" src="/bcinetwls/common/js/multipass.js"></script>
	</head>

	<body onLoad="javascript:inicializar();">
		
		<div class="entrust_container opciones">
			<p class="elige"><bean:message key="entrustToken.contenido.tituloMultipass"/></p>
			
			<html:form id="form" action="PagoDirecto.do?metodo=validaToken" onsubmit="return aceptar();">
				<div class="multipass">
					<div class="img"></div>
					<div class="ingreso">
						<div class="label" id="label"><bean:message key="entrustToken.contenido.subtitulo"/></div>
						<input type="password" id="pass" name="token" placeholder="<bean:message key="entrustToken.contenido.placeholder"/>" maxlength="6">
						<div id="ver"></div>
					</div>
					<div class="paso"><bean:message key="entrustToken.contenido.cantCaracteres"/></div>
				</div>
				<button id="aceptar" class="aceptar disabled" disabled><bean:message key="entrustToken.boton.aceptar"/></button>

				<div id="loading"></div>

			</html:form>
		</div>
	</body>
</html>

<%--
/**************************************************************************************************************************
* Archivo:     autenticacionEntrustToken.jsp
* Descripci�n: Se utiliza para la autenticaci�n a trav�s del dispositivo fisico de Entrust.
* Path:        /bcinetwls/paguedirecto/monex/entrusttoken
* " Todos los derechos reservados por Banco de Cr�dito e Inversiones."              
* Historia Cambios
* -------------------------
* versi�n       fecha         autor                                                       cambios
* =======   ==========  ==========================================================        =================================
* 1.0       02-08-2017  Marcelo Fuentes (SEnTRA) - Ricardo Carrasco (Ing. Soft. BCI)      versi�n inicial. Ser� modificada.
*
**************************************************************************************************************************/
--%>
