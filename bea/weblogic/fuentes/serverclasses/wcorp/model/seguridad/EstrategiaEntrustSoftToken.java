package wcorp.model.seguridad;

import java.rmi.RemoteException;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import wcorp.model.seguridad.to.EstadoActivacionTokenTO;
import wcorp.model.seguridad.to.OtpEntrustTO;
import wcorp.model.seguridad.to.TokenEntrustTO;
import wcorp.serv.seguridad.SeguridadException;
import wcorp.util.EnvioDeCorreo;
import wcorp.util.GeneralException;
import wcorp.util.TablaValores;

/**
 * <b>Clase que contiene la l�gica de Estrategia de Autenticaci�n Entrust Token F�sico</b>
 * <p>
 * Registro de versiones:
 * <ul>
 *  <li>1.0 10/07/2017 Luis L�pez Alamos - Jaime Suazo D�az - Sergio Flores - Andres Silva H.(SEnTRA) - Ricardo Carrasco C�ceres (Ing. Soft. BCI): Versi�n Inicial.</li>
 * </ul>
 * <b>Todos los derechos reservados por Banco de Cr�dito e Inversiones.</b>
 * <p>
 */
public class EstrategiaEntrustSoftToken extends EstrategiaAbstractaEntrust  {

    /**
     * Log de la clase.
     */
    private transient Logger logger = (Logger) Logger.getLogger(EstrategiaEntrustSoftToken.class);
    
    /**
     * Metodo para consultarEstadoSegundaClave.
     * <p>
     * Registro de versiones: 
     * <ul>
     * <li>1.0 10/07/2017 Sergio Flores (SEnTRA) - Ricardo Carrasco (Ing. Soft. BCI): Version inicial.</li>
     * </ul>
     * <p> 
     * @param parametroEstrategiaSegundaClave parametros para ejecutar la consulta de estado.
     * @return EstadoSegundaClaveTO[] lista de token usuario.
     * @throws SeguridadException error lanzado por el metodo.
     * @since 1.0
     */
    public EstadoSegundaClaveTO consultarEstadoSegundaClave(ParametrosEstrategiaSegundaClaveTO parametroEstrategiaSegundaClave) throws SeguridadException {
        if (getLogger().isEnabledFor(Level.INFO)){
            getLogger().info("[consultarEstadoSegundaClave][BCI_INI] " + parametroEstrategiaSegundaClave);
        }
            EstadoSegundaClaveTO resultadoEstado = consultaEstadoDeSegundaClave(parametroEstrategiaSegundaClave, PrioridadSegundaClaveIdTO.ENTRUST_SOFTTOKEN);
            if (getLogger().isEnabledFor(Level.INFO)){
                getLogger().info("[consultarEstadoSegundaClave][BCI_FINOK] " + resultadoEstado);
            }
            return resultadoEstado;
        }

    
    /***
     * <p>
     * Implementaci�n NULA del m�todo generarLlave. No se necesita por el tipo de dispositivo asociado.
     * FALTA DOCUMENTAR Y COREGIR
     * </p>
     * 
     * @param parametroEstrategiaSegundaClave
     * parametro de segunda clave
     * @return ResultadoOperacionSegundaClaveTO resultado de la operacion segunda clave
     * @throws SeguridadException
     * Seguridad Excepcion
     */
    public LlaveSegundaClaveTO generarLlave(ParametrosEstrategiaSegundaClaveTO parametroEstrategiaSegundaClave) throws SeguridadException {
    	if (getLogger().isEnabledFor(Level.INFO)) {
    		getLogger().info("[generarLlave][BCI_INI] " + parametroEstrategiaSegundaClave.toString());	
    	}
        
        LlaveSegundaClaveTO llaveGenerada = new LlaveSegundaClaveTO();
        try {
            String grupoClienteEntrust = TablaValores.getValor(TABLA_DE_PARAMETROS, "pinToken", "desc");
            parametroEstrategiaSegundaClave.getUsuarioSegundaClave().setGrupoClienteEntrust(grupoClienteEntrust);
            serviciosSegundaClave = obtenerServiciosSegundaClave();
            llaveGenerada = serviciosSegundaClave.obtenerDesafioGenerico(parametroEstrategiaSegundaClave);
        } 
        catch (Exception e) {
            if (getLogger().isEnabledFor(Level.ERROR)){
                getLogger().error("[generarLlave][BCI_FINEX][Exception] Error al generar Llave:" + e.getMessage(), e);
        } 
            throw new SeguridadException(PREFIJO_ENTRUST + e.getMessage());
        } 
        
        if (getLogger().isEnabledFor(Level.INFO)) {
        	getLogger().info("[generarLlave][BCI_FINOK]Llave serial " + llaveGenerada.getSerial());	
        }
        
        return llaveGenerada;
    }

    /**
     * encargado de la autenticacion -- agregar mas descripcion.
     * <p>
     * 
     * Registro de Versiones:
     * <ul>
     * <li>1.0 10/07/2017 Luis L�pez Alamos (SEnTRA) - Ricardo Carrasco Caceres (Ing. Soft. BCI): Version Inicial.</li>
     * </ul>
     * <P>
     * @param parametroEstrategiaSegundaClave clave ingresada por el cliente para autenticar.
     * @return String con el resultado
     * @throws SeguridadException excepcion lanzada por el metodo.
     * @since 2.0
     */
    public ResultadoOperacionSegundaClaveTO autenticar(ParametrosEstrategiaSegundaClaveTO parametroEstrategiaSegundaClave) throws SeguridadException {

        if (getLogger().isEnabledFor(Level.INFO)){
            getLogger().info("[autenticar][BCI_INI] " + parametroEstrategiaSegundaClave);
            }
            ResultadoOperacionSegundaClaveTO response = new ResultadoOperacionSegundaClaveTO();
            try {
                String grupoEntrustBCI = TablaValores.getValor(TABLA_DE_PARAMETROS, "pinToken", "desc") != null ? TablaValores.getValor(TABLA_DE_PARAMETROS, "pinToken", "desc") : "";
                if (grupoEntrustBCI.equalsIgnoreCase("")){
                    getLogger().info("[autenticar] Hubo un problema al leer datos de tabla de parametros");
                    throw new IllegalArgumentException("ENTRUSTFRASE02");
                }
            parametroEstrategiaSegundaClave.getUsuarioSegundaClave().setGrupoClienteEntrust(grupoEntrustBCI);
            serviciosSegundaClave = obtenerServiciosSegundaClave();
            response = serviciosSegundaClave.autenticarDesafioGenerico(parametroEstrategiaSegundaClave);
           
        } 
        catch(Exception e){
        	if (getLogger().isEnabledFor(Level.ERROR)){
                getLogger().error("[autenticar][BCI_FINEX] Error al generar Llave:" + e.getMessage(), e);
            } 
        	throw new SeguridadException(PREFIJO_ENTRUST + e.getMessage());
                }
                
        if (getLogger().isEnabledFor(Level.INFO)){
            getLogger().info("[autenticar][BCI_FINOK]");
            }
            
            return response;
        }
    
    /**
     * consultarEstadoDesafioOperacion, -- agregar mas descripcion
     * <p>
     * 
     * Registro de Versiones:
     * <ul>
     * <li>1.0 10/07/2017 Luis L�pez Alamos (SEnTRA) - Ricardo Carrasco C�ceres (Ing. Soft. BCI): Versi�n Inicial.</li>
     * </ul>
     * <P>
     * @param codigoTransaccion clave ingresada por el cliente para autenticar
     * @return String con el resultado
     * @since 2.0
     */
    public String consultarEstadoDesafio(String codigoTransaccion){
        if (getLogger().isEnabledFor(Level.INFO)) {
        	getLogger().info("[consultarEstadoDesafio][Codigo Transaccion = " + codigoTransaccion + "][BCI_INI]");
        }
        
        try {
            serviciosSegundaClave = obtenerServiciosSegundaClave();
        String resultado = serviciosSegundaClave.consultarEstadoDesafio(codigoTransaccion);
        if (getLogger().isEnabledFor(Level.INFO)) {
        	getLogger().info("[consultarEstadoDesafio] [resultado =" + resultado + "] [BCI_FINOK]");
        }
        return resultado;
        } 
        catch (Exception e){
        	if(getLogger().isEnabledFor(Level.ERROR)){
                getLogger().error("[consultarEstadoDesafio] [" + codigoTransaccion +"] [BCI_FINEX] [Exception] Error con mensaje =<" + e.getMessage()+">", e);
            }
        }
        return null;
        
    }
    
    /**
     * Metodo que activa un soft token de cliente, es decir, le asigna el estado CURRENT y estado de activaci�n ACTIVE.
     * <p>
     * 
     * Registro de Versiones:
     * <ul>
     * <li>1.0 10/07/2017 Luis L�pez Alamos (SEnTRA) - Ricardo Carrasco C�ceres (Ing. Soft. BCI): Versi�n Inicial.</li>
     * </ul>
     * <P>
     * @param parametroEstrategiaSegundaClave sin implementaci�n concreta.
     * @return resultado de la operacion de activar softoken.
     * @throws GeneralException excepcion.
     * @since 1.0
     */
    public ResultadoOperacionSegundaClaveTO activar(ParametrosEstrategiaSegundaClaveTO parametroEstrategiaSegundaClave) throws GeneralException {
    	if (getLogger().isEnabledFor(Level.INFO)) {
            getLogger().info("[activar][BCI_INI][" + parametroEstrategiaSegundaClave + "]");
        }
        
    	EstadoActivacionTokenTO resultado = this.activarSoftTokenUsuario(parametroEstrategiaSegundaClave);
    	    	
    	ResultadoOperacionSegundaClaveTO respuesta = new ResultadoOperacionSegundaClaveTO();
    	
    	respuesta.setEstatus(resultado.getResultado().getCode() == 0);
    	respuesta.setGlosa(resultado.getResultado().getDescription());
    	respuesta.setIdCodigoEstado(resultado.getResultado().getCode());
    	    	
        if (getLogger().isEnabledFor(Level.INFO)) {
            getLogger().info("[bloquear][BCI_FINOK][" + respuesta + "]");
        }
    	
        return respuesta;
    }
    
    /**
     * Metodo para activar un nuevo soft token del cliente.
     * CAMBIAR A PRIVADO ESTE METODO.
     * <p>
     * Registro de versiones: 
     * <ul>
     * <li>1.0 10/07/2017 Sergio Flores (SEnTRA) - Ricardo Carrasco (Ing. Soft. BCI): Version inicial.</li>
     * </ul>
     * <p> 
     * @param parametroEstrategiaSegundaClave parametros para la activacion del token.
     * @return EstadoActivacionTokenTO estado activacion cliente.
     * @since 1.0
     */
    public EstadoActivacionTokenTO activarSoftTokenUsuario(ParametrosEstrategiaSegundaClaveTO parametroEstrategiaSegundaClave) {
    	if (getLogger().isEnabledFor(Level.INFO)) {
            getLogger().info("[activarSoftTokenUsuario][BCI_INI][" + parametroEstrategiaSegundaClave.toString() + "]");
        }
        EstadoActivacionTokenTO estadoActivacionTokenTO = new EstadoActivacionTokenTO();
        try {
        	String grupoClienteEntrust = TablaValores.getValor(TABLA_DE_PARAMETROS, "pinToken", "desc");
        	parametroEstrategiaSegundaClave.getUsuarioSegundaClave().setGrupoClienteEntrust(grupoClienteEntrust);
        	serviciosSegundaClave = obtenerServiciosSegundaClave();
        	estadoActivacionTokenTO = serviciosSegundaClave.activarSoftTokenUsuario(parametroEstrategiaSegundaClave);
        } 
        catch (RemoteException e) {
        	if(getLogger().isEnabledFor(Level.ERROR)){
                getLogger().error("[activarSoftTokenUsuario][BCI_FINEX]RemoteException : " + e.getMessage() );
            }
        } 
        catch (Exception e) {
        	if (getLogger().isEnabledFor(Level.ERROR)) {
                getLogger().error("[activarSoftTokenUsuario][Exception][BCI_FINEX] error con mensaje : " 
                        + e.getMessage(), e);
            }
        } 
        
        return estadoActivacionTokenTO;
    }
    
    /**
     * Metodo para activar un nuevo soft token del cliente.
     * <p>
     * Registro de versiones: 
     * <ul>
     * <li>1.0 10/07/2017 Sergio Flores (SEnTRA) - Ricardo Carrasco (Ing. Soft. BCI): Version inicial.</li>
     * </ul>
     * <p> 
     * @param usuarioSegundaClave datos del usuario a validar.
     * @return OtpEntrustTO estado cliente.
     * @since 1.0
     */
    public OtpEntrustTO obtenerOTP(UsuarioSegundaClaveTO usuarioSegundaClave) {
    	if (getLogger().isEnabledFor(Level.INFO)) {
            getLogger().info("[obtenerOTP][BCI_INI][" + usuarioSegundaClave.toString() + "]");
        }
    	OtpEntrustTO otpEntrustTO = new OtpEntrustTO();
        try {
            serviciosSegundaClave = obtenerServiciosSegundaClave();
        	otpEntrustTO = serviciosSegundaClave.obtenerOTP(usuarioSegundaClave);
        } 
        catch (RemoteException e) {
        	if(getLogger().isEnabledFor(Level.ERROR)){
                getLogger().error("[obtenerOTP][BCI_FINEX]RemoteException : " + e.getMessage() );
            }
        } 
        catch (Exception e) {
        	if (getLogger().isEnabledFor(Level.ERROR)) {
                getLogger().error("[obtenerOTP][Exception][BCI_FINEX] error con mensaje : " 
                        + e.getMessage(), e);
            }
        } 
        
        return otpEntrustTO;
    }
	    /**
     * encargado de enviar el token via Email indicando su estado.
     * <p>
     * 
     * Registro de Versiones:
     * <ul>
     * <li>1.0 10/07/2017 Andres Silva H.(SEnTRA) - Ricardo Carrasco Caceres (Ing. Soft. BCI): Version Inicial.</li>
     * </ul>
     * <P>
     * 
     * @param token dispositivo  .
     * @param mail Mail del cliente.
     * @param cuerpo cuerpo Mail del cliente.
     * @param subject subject del cliente.
     * @return int con el resultado del envio.
     * @since 2.0
     */
    public int enviarTokenEmail(TokenEntrustTO token, String mail, String cuerpo,String subject){

        if (getLogger().isEnabledFor(Level.INFO)){
            getLogger().info("[enviarTokenEmail][BCI_INI] ");
            }
        
		String fromNombre = TablaValores.getValor(TABLA_DE_PARAMETROS, "FROMNOMBRE", "desc");
		String fromDireccion = TablaValores.getValor(TABLA_DE_PARAMETROS, "FROMDIRECCION", "desc");
		String signature = TablaValores.getValor(TABLA_DE_PARAMETROS, "FIRMA", "desc");
        
              
        if (getLogger().isEnabledFor(Level.INFO)){
            getLogger().info("[enviarTokenEmail][BCI_FINOK]");
            }
        
        return EnvioDeCorreo.simple(fromNombre, fromDireccion, mail, subject, cuerpo, signature);
		}
    
    
    /**
     * <p>
     * M�todo que obtiene el objeto getLogger().
     * </p>
     * 
     * Registro de versiones:
     * <ul>
     * <li>1.0 10/07/2017 Luis L�pez Alamos (SEnTRA) - Ricardo Carrasco C�ceres (Ing. Soft. BCI): Versi�n Inicial.</li>
     * </ul>
     * <p>
     * 
     * @return Logger Objeto de log
     * @since 1.0
     */
    private Logger getLogger() {
        if ( logger == null ) {
            logger = Logger.getLogger( this.getClass() );
        }
        return logger;
    }
}

