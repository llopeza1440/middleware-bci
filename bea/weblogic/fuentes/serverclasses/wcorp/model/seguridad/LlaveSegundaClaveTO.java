package wcorp.model.seguridad;

import java.io.Serializable;

/**
 * 
 * <b>LlaveSegundaClaveTO</b>
 * <p>
 * Representa un identificador de clave generada por una 
 * estrategia que implemente este comportamiento de segunda
 * clave.
 * </p>
 * 
 * <p>Registro de versiones:</p>
 * 
 * <ul>
 * <li> 1.0 11/04/2013, Victor Hugo Enero (Orand): versi�n inicial</li>
 * <li> 2.0 10/07/2017 Luis L�pez Alamos (SEnTRA) - Ricardo Carrasco Caceres (Ing. Soft. BCI): Se agrega atributo {@link #llaves} encapsulado}</li>
 * </ul>
 * 
 * <p><b>Todos los derechos reservados por Banco de Cr�dito e Inversiones.</b></p>
 * 
 */
public class LlaveSegundaClaveTO  extends EstadoSegundaClaveTO implements Serializable  {
    
    /**
     * N�mero de versi�n utilizado durante la serializaci�n.
     */
    private static final long serialVersionUID = 1L;
    
    /**
     * Campos de llaves.
     */
    private CamposDeLlaveTO camposDeLlave;
    
    /**
     * Representacion de segunda clave.
     */
    private RepresentacionSegundaClave llave;

    /**
     * Arreglo de representacion de Segundas Claves.
     */
    private RepresentacionSegundaClave[] llaves;
    
    /**
     * Constructor de la clase.
     */
    public LlaveSegundaClaveTO(){

    }

    public CamposDeLlaveTO getCamposDeLlave() {
        return camposDeLlave;
    }

    public void setCamposDeLlave(CamposDeLlaveTO camposDeLlave) {
        this.camposDeLlave = camposDeLlave;
    }

    public RepresentacionSegundaClave getLlave() {
        return llave;
    }

    public void setLlave(RepresentacionSegundaClave llave) {
        this.llave = llave;
    }

    public RepresentacionSegundaClave[] getLlaves() {
        return llaves;
    }

    public void setLlaves(RepresentacionSegundaClave[] llaves) {
        this.llaves = llaves;
    }
    /**
     * toString de la clase.
     * 
     * @return String string.
     */
    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append("LlaveSegundaClaveTO: camposDeLlave[").append(camposDeLlave);
        sb.append("], llave[").append(llave);
		sb.append("], llaves[").append(llaves);
        sb.append("]");
        
        return sb.toString();
    }


}
