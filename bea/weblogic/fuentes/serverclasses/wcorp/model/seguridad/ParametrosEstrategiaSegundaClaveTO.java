package wcorp.model.seguridad;

import java.io.Serializable;

import wcorp.model.actores.Cliente;
import wcorp.model.seguridad.to.TokenEntrustTO;

/**
 * 
 * <b>ParametrosEstrategiaSegundaClaveTO</b>
 * <p>
 * Componente que respresenta los par�metros de segunda clave.
 * </p>
 * 
 * <p>Registro de versiones:</p>
 * 
 * <ul>
 * <li> 1.0 27/02/2013, Victor Enero, Yasmin Rocha (Orand): versi�n inicial</li>
 * <li> 1.1 02/09/2013  Pablo Romero C�ceres (SEnTRA): Se agregan atributos para funcionar con 
 *                                                      la estrategia de EToken.
 * <li> 1.2 19/11/2013, Yon Sing Sing Sius (ExperimentoSocial): Se agrega codigo autorizaci�n</li>
 * <li> 2.0 10/07/2017  Jaime Suazo Diaz (SEnTRA) - Ricardo Carrasco C�ceres (Ing. Soft. BCI): Por proyecto
 *          migracion plataforma autenticacion (Entrust) se agrega {@link #usuarioSegundaClave} al metodo 
 *          {@link #toString()}.</li>
 * <li> 3.0 02/08/2017 Marcelo Fuentes (SEnTRA) - Ricardo Carrasco (Ing. Soft. BCI): Se eliminan los atributos
 *          "request" de tipo HttpServletRequest y "idName" de tipo String y todas sus referencias en la clase. 
 *          El primero debido a que generaba erroral enviar este TO al EJB server y el segundo debido a que se
 *          usaba para obtener la session desde el request.</li>
 * </ul>
 * 
 * <p><b>Todos los derechos reservados por Banco de Cr�dito e Inversiones.</b></p>
 * 
 */
public class ParametrosEstrategiaSegundaClaveTO implements Serializable {

    /**
     * N�mero de versi�n utilizado durante la serializaci�n.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Identificador de Prioridad de Segunda Clave.
     */
    private String identificarPrioridadDeSegundaClave;

    /**
     * Pago transferencia.
     */
    private boolean pagoTransferencia;

    /**
     * Indentificador de Estado de Segunda Clave.
     */
    private String identificadorEstadoSegundaClave;

    /**
     * Numero de Rut del Cliente.
     */
    private String idUser;

    /**
     * Nombre del Serivicio que se esta operando.
     */
    private String servicio;

    /**
     * Campos de que contienen los datos con los que se van a generar la llave Segunda Clave.
     */
    private CamposDeLlaveTO camposDeLlave;

    /**
     * Segunda clave.
     */
    private RepresentacionSegundaClave segundaClave;

    /**
     * Cliente.
     */
    private Cliente cliente;

    /**
     * El usuario conectado a un dominio.
     */
    private UsuarioSegundaClaveTO usuarioSegundaClave;

    /**
     * Canal.
     */
    private Canal canal;

    /**
     * SessionBCI.
     */
    private SessionBCI sessionBCI;

    /**
     * Tipo segunda clave.
     */
    private String tipoSegundaClave;
    /**
     * Codigo Confirmaci�n para activar safesigner.
     */
    private String codigoConfirmacion;

    /**
     * Codigo de autorizaci�n, utilizado para enrolamiento SafeSigner
     */
    private String codigoAutorizacion;

    /**
     * Data a Firmar.
     */
    private String dataToSign;
    
    /**
     * Data Firmada.
     */
    private String dataSignature;
    
    /**
     * Mensaje original que se firma.
     */
    private String mensajeOriginal;  
    
    /**
     * Token entrust sea hard o softoken
     */
    private TokenEntrustTO tokenEntrust;
    
	/**
     * Constructor de la instancia.
     * @param servicio nombre servicio.
     * @param cliente cliente.
     * @param canal canal.
     * @param sessionBCI session bci.
     */
    public ParametrosEstrategiaSegundaClaveTO(String servicio,
        Cliente cliente, Canal canal, SessionBCI sessionBCI) {
        this.servicio = servicio;
        this.canal = canal;
        this.cliente = cliente;
        this.sessionBCI = sessionBCI;
    }

    /**
     * Constructor de la clase.
     * @param servicio nombre servicio.
     * @param cliente cliente.
     * @param canal canal.
     * @param sessionBCI session bci.
     * @param camposDeClaveTO campos de operacion segunda clave.
     */
    public ParametrosEstrategiaSegundaClaveTO(String servicio,
        Cliente cliente, Canal canal, SessionBCI sessionBCI, CamposDeLlaveTO camposDeClaveTO) {
        this.servicio = servicio;
        this.camposDeLlave = camposDeClaveTO;
        this.canal = canal;
        this.cliente = cliente;
        this.sessionBCI = sessionBCI;
    }

    /**
     * Constructor de la clase.
     * @param servicio
     * nombre servicio
     * @param cliente
     * cliente
     * @param canal
     * canal
     * @param camposDeClaveTO
     * campos de operacion segunda clave
     */
    public ParametrosEstrategiaSegundaClaveTO(String servicio,
        Cliente cliente, Canal canal, CamposDeLlaveTO camposDeClaveTO) {
        this.servicio = servicio;
        this.camposDeLlave = camposDeClaveTO;
        this.canal = canal;
        this.cliente = cliente;
        this.sessionBCI = sessionBCI;
    }

    /**
     * Constructor de la clase.
     * @param usuarioSegundaClave
     * usuario Segunda Clave
     */
    public ParametrosEstrategiaSegundaClaveTO(UsuarioSegundaClaveTO usuarioSegundaClave) {
        this.usuarioSegundaClave = usuarioSegundaClave;
    }
    
    /**
     * Constructor de la clase.
     * @param servicio nombre servicio
     * @param usuarioSegundaClave usuario de segunda clave.
     * @param camposDeClaveTO campos de clave
     * campos de operacion segunda clave
     */
    public ParametrosEstrategiaSegundaClaveTO(String servicio,
        UsuarioSegundaClaveTO usuarioSegundaClave, CamposDeLlaveTO camposDeClaveTO) {
        this.servicio = servicio;
        this.usuarioSegundaClave = usuarioSegundaClave;
        this.camposDeLlave = camposDeClaveTO;
    }

    /**
     * Constructor de la clase.
     * @param usuarioSegundaClave usuario de segunda clave.
     * @param segundaClave segunda clave
     * campos de operacion segunda clave
     * @param sessionBCI
     * session bci
     */
    public ParametrosEstrategiaSegundaClaveTO(UsuarioSegundaClaveTO usuarioSegundaClave, 
    		RepresentacionSegundaClave segundaClave, SessionBCI sessionBci) {
        this.servicio = servicio;
        this.usuarioSegundaClave = usuarioSegundaClave;
        this.segundaClave = segundaClave;
        this.sessionBCI = sessionBci;
    }
    
    /**
     * Constructor de la clase.
     * @param servicio nombre servicio.
     * @param cliente cliente.
     * @param canal canal.
     * @param sessionBCI session bci.
     * @param segundaClave segunda clave.
     */
    public ParametrosEstrategiaSegundaClaveTO(String servicio,
        Cliente cliente, Canal canal, SessionBCI sessionBCI, RepresentacionSegundaClave segundaClave) {
        this.servicio = servicio;
        this.segundaClave = segundaClave;
        this.canal = canal;
        this.cliente = cliente;
        this.sessionBCI = sessionBCI;
    }

    public ParametrosEstrategiaSegundaClaveTO( String anIdUser ) {
        idUser = anIdUser;
    }
    public String getIdentificarPrioridadDeSegundaClave() {
        return identificarPrioridadDeSegundaClave;
    }

    public void setIdentificarPrioridadDeSegundaClave(String identificarPrioridadDeSegundaClave) {
        this.identificarPrioridadDeSegundaClave = identificarPrioridadDeSegundaClave;
    }

    public boolean isPagoTransferencia() {
        return pagoTransferencia;
    }

    public void setPagoTransferencia(boolean pagoTransferencia) {
        this.pagoTransferencia = pagoTransferencia;
    }

    public String getIdentificadorEstadoSegundaClave() {
        return identificadorEstadoSegundaClave;
    }

    public void setIdentificadorEstadoSegundaClave(String identificadorEstadoSegundaClave) {
        this.identificadorEstadoSegundaClave = identificadorEstadoSegundaClave;
    }

    public String getServicio() {
        return servicio;
    }

    public void setServicio(String servicio) {
        this.servicio = servicio;
    }

    public String getIdUser() {
        return idUser;
    }

    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }

    public CamposDeLlaveTO getCamposDeLlave() {
        return camposDeLlave;
    }

    public void setCamposDeLlave(CamposDeLlaveTO camposDeLlave) {
        this.camposDeLlave = camposDeLlave;
    }

    public RepresentacionSegundaClave getSegundaClave() {
        return segundaClave;
    }

    public void setSegundaClave(RepresentacionSegundaClave segundaClave) {
        this.segundaClave = segundaClave;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public UsuarioSegundaClaveTO getUsuarioSegundaClave() {
        return usuarioSegundaClave;
    }

    public void setUsuarioSegundaClave(UsuarioSegundaClaveTO usuarioSegundaClave) {
        this.usuarioSegundaClave = usuarioSegundaClave;
    }
    
    public Canal getCanal() {
        return canal;
    }

    public void setCanal(Canal canal) {
        this.canal = canal;
    }

    public void setCanal(String canalId) {
        this.canal = new Canal(canalId);
    }
    
    public SessionBCI getSessionBCI() {
        return sessionBCI;
    }

    public void setSessionBCI(SessionBCI sessionBCI) {
        this.sessionBCI = sessionBCI;
    }

    public String getTipoSegundaClave() {
        return tipoSegundaClave;
    }

    public void setTipoSegundaClave(String tipoSegundaClave) {
        this.tipoSegundaClave = tipoSegundaClave;
    }

    public String getCodigoConfirmacion() {
        return codigoConfirmacion;
    }
    
    public String getDataToSign() {
        return dataToSign;
    }

    public String getDataSignature() {
        return dataSignature;
    }

    public String getMensajeOriginal() {
        return mensajeOriginal;
    }

    public void setCodigoConfirmacion(String codigoConfirmacion) {
        this.codigoConfirmacion = codigoConfirmacion;
    }

    public void setDataToSign(String dataToSign) {
        this.dataToSign = dataToSign;
    }

    public void setDataSignature(String dataSignature) {
        this.dataSignature = dataSignature;
    }

    public void setMensajeOriginal(String mensajeOriginal) {
        this.mensajeOriginal = mensajeOriginal;
    }

    public String getCodigoAutorizacion() {
		return codigoAutorizacion;
	}

	public void setCodigoAutorizacion(String codigoAutorizacion) {
		this.codigoAutorizacion = codigoAutorizacion;
	}
	
    public TokenEntrustTO getTokenEntrust() {
		return tokenEntrust;
	}

	public void setTokenEntrust(TokenEntrustTO tokenEntrust) {
		this.tokenEntrust = tokenEntrust;
	}

	/**
     * Metodo toString.
     * @return String string.
     */
    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append("ParametrosEstrategiaSegundaClaveTO: "
        + "identificarPrioridadDeSegundaClave[").append(identificarPrioridadDeSegundaClave);
        sb.append("], pagoTransferencia[").append(pagoTransferencia);
        sb.append("], identificadorEstadoSegundaClave[").append(identificadorEstadoSegundaClave);
        sb.append("], servicio[").append(servicio);
        sb.append("], idUser[").append(idUser);
        sb.append("], camposDeLlave[").append(camposDeLlave);
        sb.append("], segundaClave[").append(segundaClave);
        sb.append("], cliente[").append(cliente);
        sb.append("], canal[").append(canal);
        sb.append("], sessionBCI[").append(sessionBCI);
        sb.append("], tipoSegundaClave[").append(tipoSegundaClave);
        sb.append("], codigoConfirmacion[").append(codigoConfirmacion);
        sb.append("], dataToSign[").append(dataToSign);
        sb.append("], dataSignature[").append(dataSignature);
        sb.append("], mensajeOriginal[").append(mensajeOriginal);
        sb.append("], tokenEntrust[").append(tokenEntrust);
        sb.append("], usuarioSegundaClave[").append(usuarioSegundaClave);
        sb.append("]");
        return sb.toString();        
    }

}
