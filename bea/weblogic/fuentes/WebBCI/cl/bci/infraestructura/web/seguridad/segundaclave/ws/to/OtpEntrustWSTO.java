package cl.bci.infraestructura.web.seguridad.segundaclave.ws.to;

import java.io.Serializable;
import java.util.Date;

/**
 * OtpEntrustWSTO
 * <br>
 * Clase transportadora que representa la informacion de un OtpEntrustTO entrust.
 * <br>
 * Registro de versiones:
 * 
 * <ul>
 *   <li>1.0 10/07/2017 Sergio Flores (Sentra) - Ricardo Carrasco (Ing. Soft. BCI) : versi�n inicial.</li>
 * </ul>
 * <p>
 * <b>Todos los derechos reservados por Banco de Cr�dito e Inversiones.</b>
 * </p>
 */
public class OtpEntrustWSTO implements Serializable {

	/**
	 * numero serial de la clase. 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * Campo codigo usuario.
	 */
	private String userId;
	/**
	 * Campo grupo usuario.
	 */
	private String group;
	/**
	 * Campo identificador repositorio.
	 */
	private String repositoryId;
	/**
	 * Campo codigo otp respuesta.
	 */   
	private String otp;
	/**
	 * Campo fecha de creacion.
	 */       
	private Date createDate;
    
	/**
	 * Campo fecha de expiracion.
	 */    
	private Date expireDate;
	/**
	 * Campo id de la transaccion.
	 */    
	private String transactionId;
	
	/**
	 * Campo codigo respuesta WS entrust.
	 */
	private int codigoRespuestaWs = 0;
	
	/**
	 * Campo descripcion respuesta WS entrust.
	 */
	private String descripcionRespuestaWs = null;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getGroup() {
		return group;
	}

	public void setGroup(String group) {
		this.group = group;
	}

	public String getRepositoryId() {
		return repositoryId;
	}

	public void setRepositoryId(String repositoryId) {
		this.repositoryId = repositoryId;
	}

	public String getOtp() {
		return otp;
	}

	public void setOtp(String otp) {
		this.otp = otp;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getExpireDate() {
		return expireDate;
	}

	public void setExpireDate(Date expireDate) {
		this.expireDate = expireDate;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public int getCodigoRespuestaWs() {
		return codigoRespuestaWs;
	}

	public void setCodigoRespuestaWs(int codigoRespuestaWs) {
		this.codigoRespuestaWs = codigoRespuestaWs;
	}

	public String getDescripcionRespuestaWs() {
		return descripcionRespuestaWs;
	}

	public void setDescripcionRespuestaWs(String descripcionRespuestaWs) {
		this.descripcionRespuestaWs = descripcionRespuestaWs;
	}
    
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("OtpEntrustWSTO [userId=");
		sb.append(userId);
		sb.append(", group=");
		sb.append(group);
		sb.append(", repositoryId=");
		sb.append(repositoryId);
		sb.append(", otp=");
		sb.append(otp);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", expireDate=");
		sb.append(expireDate);
		sb.append(", transactionId=");
		sb.append(transactionId);
		sb.append(", codigoRespuestaWs=");
		sb.append(codigoRespuestaWs);
		sb.append(", descripcionRespuestaWs=");
		sb.append(descripcionRespuestaWs);
		sb.append("]");
		return sb.toString();
	}
}
