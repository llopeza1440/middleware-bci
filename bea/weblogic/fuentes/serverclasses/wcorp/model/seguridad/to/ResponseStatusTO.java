package wcorp.model.seguridad.to;

import java.io.Serializable;

/**
 * 
 * <b>ResponseStatusTO</b>
 * <p>
 *       Representa el resultado sobre algun registro o dato en Entrust - Fuse.
 * </p>
 * 
 * <p>
 * Registro de versiones:
 * </p>
 * 
 * <ul>
 *   <li>1.0 16/06/2017 Sergio Flores (SEnTRA) - Ricardo Carrasco (Ing. Soft. BCI): Version inicial.</li>
 * </ul>
 * 
 * <p>
 * <b>Todos los derechos reservados por Banco de Cr�dito e Inversiones.</b>
 * </p>
 * 
 */
public class ResponseStatusTO implements Serializable {
	/**
	 * N�mero de versi�n utilizado durante la serializaci�n.
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Campo codigo de respuesta.
	 */
	private int code;
	
	/**
	 * Campo descripcion de respuesta.
	 */
	private String description;

	/**
	 * Regitro fue reflejado (ingresado, modificado, eliminado) en tandem.
	 */
	private boolean resultadoTandem = false;
	
	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public boolean isResultadoTandem() {
		return resultadoTandem;
	}

	public void setResultadoTandem(boolean resultadoTandem) {
		this.resultadoTandem = resultadoTandem;
	}

	public String toString() {
		StringBuffer buffer = new StringBuffer();
		buffer.append("ResponseStatusTO [code=");
		buffer.append(code);
		buffer.append(", description=");
		buffer.append(description);
		buffer.append(", resultadoTandem=");
		buffer.append(resultadoTandem);		
		buffer.append("]");
		return buffer.toString();
	}

}
