package wcorp.infraestructura.seguridad;

import java.rmi.RemoteException;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.SessionBean;
import javax.ejb.SessionContext;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import wcorp.model.seguridad.EstadoSegundaClaveTO;
import wcorp.model.seguridad.LlaveSegundaClaveTO;
import wcorp.model.seguridad.ParametrosEstrategiaSegundaClaveTO;
import wcorp.model.seguridad.ResultadoOperacionSegundaClaveTO;
import wcorp.model.seguridad.UsuarioSegundaClaveTO;
import wcorp.model.seguridad.to.EstadoActivacionTokenTO;
import wcorp.model.seguridad.to.OtpEntrustTO;
import wcorp.model.seguridad.to.ResponseStatusTO;
import wcorp.model.seguridad.to.TokenEntrustTO;
import wcorp.serv.seguridad.dao.SegundaClaveDAO;

/**
 * <b>EJB Encargado de realizar las conexiones con servicios CURUS e IDG para resolver mediante jerarqu�a de claves la autenticaci�n ENTRUST</b>
 * <p>
 * Registro de versiones:
 * <ul>
 *  <li>1.0 10/07/2017 Luis L�pez Alamos (SEnTRA) - Ricardo Carrasco C�ceres (Ing. Soft. BCI): Versi�n Inicial.</li>
 * </ul>
 * <b>Todos los derechos reservados por Banco de Cr�dito e Inversiones.</b>
 * <p>
 */

public class ServiciosSegundaClaveBean implements SessionBean {
    
    /**
     * Serializaci�n por defecto.
     */
    private static final long serialVersionUID = 1L;
    
    /**
     * Log del EJB.
     */
    private transient Logger logger = (Logger) Logger.getLogger(ServiciosSegundaClaveBean.class);
    
    /**
     * Autentica Desafio Generico
     * <p>
     * 
     * Registro de Versiones:
     * <ul>
     * <li>1.0 10/07/2017 Luis L�pez Alamos (SEnTRA) - Ricardo Carrasco C�ceres (Ing. Soft. BCI): Versi�n Inicial.</li>
     * </ul>
     * <P>
     * 
     * @param parametroEstrategiaSegundaClave parametros de entrada. 
     * @return ResultadoOperacionSegundaClave con el resultado de la autenticaci�n.
     * @throws Exception en caso de error en respuesta.
     * @throws RemoteException en caso de error de comunicaci�n.
     * @since 1.0
     */
    public ResultadoOperacionSegundaClaveTO autenticarDesafioGenerico(ParametrosEstrategiaSegundaClaveTO parametroEstrategiaSegundaClave) throws Exception, RemoteException{
        if (getLogger().isInfoEnabled()) {
            getLogger().info("[autenticaDesafioGenerico][BCI_INI]");
        }
        SegundaClaveDAO segundaClaveDao = new SegundaClaveDAO();
        ResultadoOperacionSegundaClaveTO resultado = new ResultadoOperacionSegundaClaveTO();
        resultado =  segundaClaveDao.autenticarDesafioGenerico(parametroEstrategiaSegundaClave);
        if (getLogger().isInfoEnabled()) {
            getLogger().info("[consultarEstadoDesafio] [resultado =" + resultado + "] [BCI_FINOK]");
        }
        return resultado;
    }
    
    /**
     * Obtiene un desafio gen�rico de cualquier tipo para obtener un identificador de transacci�n Entrust.
     * <p>
     * 
     * Registro de Versiones:
     * <ul>
     * <li>1.0 10/07/2017 Luis L�pez Alamos (SEnTRA) - Ricardo Carrasco C�ceres (Ing. Soft. BCI): Versi�n Inicial.</li>
     * </ul>
     * <P>
     * 
     * @param parametroEstrategiaSegundaClave objeto con todos los datos necesarios. 
     * @return LlaveSegundaClaveTO con el detalle de un challenge.
     * @throws Exception en caso de error en respuesta.
     * @throws RemoteException en caso de error de comunicaci�n.
     * @since 1.0
     */
    public LlaveSegundaClaveTO obtenerDesafioGenerico(ParametrosEstrategiaSegundaClaveTO parametroEstrategiaSegundaClave) throws Exception, RemoteException{
        if (getLogger().isInfoEnabled()) {
            getLogger().info("[obtenerDesafioGenerico][BCI_INI] " + parametroEstrategiaSegundaClave);
        }
        SegundaClaveDAO segundaClaveDao = new SegundaClaveDAO();
        LlaveSegundaClaveTO respuestaDAO = new LlaveSegundaClaveTO();
        try{
            respuestaDAO = segundaClaveDao.obtenerDesafioGenerico(parametroEstrategiaSegundaClave);
        }
        catch (Exception e){
            if (getLogger().isEnabledFor(Level.ERROR)){
                getLogger().error("[obtenerDesafioGenerico][BCI_FINEX] Error al consultar desafio generico:" + e.getMessage(), e);
            }
            throw e;
        }
        if (getLogger().isInfoEnabled()) {
            getLogger().info("[obtenerDesafioGenerico][BCI_FINOK]");
        }
        return respuestaDAO;
    }    

    public void setSessionContext(SessionContext paramSessionContext) throws EJBException, RemoteException {
    }
    
    public void ejbCreate() throws RemoteException, CreateException {
    }

    public void ejbRemove() throws EJBException, RemoteException {
    }

    public void ejbActivate() throws EJBException, RemoteException {
    }

    public void ejbPassivate() throws EJBException, RemoteException {
    }
    
    /**
     * <p>
     * M�todo que obtiene el objeto getLogger().
     * </p>
     * 
     * Registro de versiones:
     * <ul>
     * <li>1.0 10/07/2017 Luis L�pez Alamos (SEnTRA) - Ricardo Carrasco C�ceres (Ing. Soft. BCI): Versi�n Inicial.</li>
     * </ul>
     * <p>
     * 
     * @return Logger Objeto de log
     * @since 1.0
     */
    public Logger getLogger() {
        if (logger == null) {
            logger = Logger.getLogger(this.getClass());
        }
        return logger;
    }

    /**
     * Metodo que actualiza (cambia el estado) de un softoken de un cliente dado.
     * Los servicios llamados son: fuse <code>actualizarTokenUsuario</code>, entrust <code>userTokenUpdate</code>.
     * <br><br>
     * En caso de error el servicio retornara una instancia de TokenEntrustTO nula. 
     * 
     * 
     * Registro de versiones:
     * <ul>
     * <li>1.0 10/07/2017 Jaime Suazo. (SEnTRA) - Ricardo Carrasco (Ing. Soft. BCI): versi�n inicial.</li>
     * </ul>
     * 
     * @param parametroEstrategiaSegundaClave datos del cliente y del token a modificar su estado.
     * @return ResponseStatusTO objeto con el resultado de la ejecucion del servicio en fuse v�a entrust.
     * @since 2.0
     * @see SegundaClaveDAO#actualizarTokenUsuario(TokenEntrustTO)
     */        
    public ResponseStatusTO actualizarTokenUsuario(ParametrosEstrategiaSegundaClaveTO parametroEstrategiaSegundaClave) throws RemoteException {
        if (getLogger().isEnabledFor(Level.INFO)) {
            getLogger().info("[actualizarTokenUsuario][BCI_INI][" + parametroEstrategiaSegundaClave + "]");
        }
        
        ResponseStatusTO resultado = null;
        
        try {
        	SegundaClaveDAO segundaClaveDao = new SegundaClaveDAO();
        	resultado = segundaClaveDao.actualizarTokenUsuario(parametroEstrategiaSegundaClave);
        	
            if (getLogger().isEnabledFor(Level.DEBUG)) {
            	getLogger().debug("[actualizarTokenUsuario][respuesta obtenida OK]"
            			+ "[segundaClaveDao.actualizarTokenUsuario][" + resultado + "]");
            }
        	
		} catch (Exception e) {
            if (getLogger().isEnabledFor(Level.ERROR)) {
                getLogger().error("[actualizarTokenUsuario][BCI_FINEX][Exception]" + e.getMessage(), e);
            }			
		}
        
        if (getLogger().isEnabledFor(Level.INFO)) {
            getLogger().info("[actualizarTokenUsuario][BCI_FINOK][" + resultado + "]");
        }
        
        return resultado;
    }
    
    
    /**
     * Consulta si el cliente realiz� alguna acci�n al obtener un desafio de transacci�n en Entrust.
     * <p>
     * 
     * Registro de Versiones:
     * <ul>
     * <li>1.0 10/07/2017 Luis L�pez Alamos (SEnTRA) - Ricardo Carrasco C�ceres (Ing. Soft. BCI): Versi�n Inicial.</li>
     * </ul>
     * <P>
     * 
     * @param codigoTransaccion con el id por confirmar. 
     * @return String con el resultado.
     * @since 1.0
     */
    public String consultarEstadoDesafio(String codigoTransaccion){
        if (getLogger().isInfoEnabled()) {
            getLogger().info("[consultarEstadoDesafio][Codigo Transaccion = " + codigoTransaccion + "][BCI_INI]");
        }
        SegundaClaveDAO segundaClaveDao = new SegundaClaveDAO();
        String resultado = segundaClaveDao.consultarEstadoDesafio(codigoTransaccion);
        
        if (getLogger().isInfoEnabled()) {
            getLogger().info("[consultarEstadoDesafio] [resultado =" + resultado + "] [BCI_FINOK]");
        }
        return resultado;
    }
    
    /**
     * Metodo encargado de llamar al servicio de listar los token de usuario.
     * <p>
     * Registro de versiones: 
     * <ul>
     * <li>1.0 10/07/2017 Sergio Flores (SEnTRA) - Ricardo Carrasco (Ing. Soft. BCI): Version inicial.</li>
     * </ul>
     * <p> 
     * @param userId usuario de consulta.
     * @return TokenEntrustTO lista de token usuario.
     * @throws Exception
     * @throws RemoteException LlaveSegundaClaveTO
     * @since 1.0
     */
    public TokenEntrustTO[] listarTokenUsuario(ParametrosEstrategiaSegundaClaveTO parametroEstrategiaSegundaClave) throws RemoteException{
        if (getLogger().isEnabledFor(Level.INFO)) {
            getLogger().info("[listarTokenUsuario][BCI_INI] " + parametroEstrategiaSegundaClave);
        }
        SegundaClaveDAO segundaClaveDao = new SegundaClaveDAO();
        if (getLogger().isEnabledFor(Level.INFO)) {
            getLogger().info("[listarTokenUsuario][BCI_FINOK]");
        }
        return segundaClaveDao.listarTokenUsuario(parametroEstrategiaSegundaClave);
    }
    
    /**
     * Metodo para activar un nuevo soft token del cliente.
     * <p>
     * Registro de versiones: 
     * <ul>
     * <li>1.0 10/07/2017 Sergio Flores (SEnTRA) - Ricardo Carrasco (Ing. Soft. BCI): Version inicial.</li>
     * </ul>
     * <p> 
     * @param parametroEstrategiaSegundaClave valores de consulta.
     * @return EstadoActivacionTokenTO respuesta del servicio.
     * @throws Exception
     * @throws RemoteException LlaveSegundaClaveTO
     * @since 1.0
     */
    public EstadoActivacionTokenTO activarSoftTokenUsuario(ParametrosEstrategiaSegundaClaveTO parametroEstrategiaSegundaClave) throws RemoteException{
        if (getLogger().isEnabledFor(Level.INFO)) {
            getLogger().info("[activarSoftTokenUsuario][BCI_INI] " + parametroEstrategiaSegundaClave);
        }
        SegundaClaveDAO segundaClaveDao = new SegundaClaveDAO();
        if (getLogger().isEnabledFor(Level.INFO)) {
            getLogger().info("[activarSoftTokenUsuario][BCI_FINOK]");
        }
        return segundaClaveDao.activarSoftTokenUsuario(parametroEstrategiaSegundaClave);
    }
    
    /**
     * Metodo para activar un nuevo soft token del cliente.
     * <p>
     * Registro de versiones: 
     * <ul>
     * <li>1.0 10/07/2017 Sergio Flores (SEnTRA) - Ricardo Carrasco (Ing. Soft. BCI): Version inicial.</li>
     * </ul>
     * <p> 
     * @param rut valores de consulta.
     * @param digitoVerificador valores de consulta.
     * @param codigoTipoProd valores de consulta.
     * @return EstadoSegundaClaveTO respuesta del servicio.
     * @throws Exception
     * @throws RemoteException LlaveSegundaClaveTO
     * @since 1.0
     */
    public EstadoSegundaClaveTO[] consultarEstadoSegundaClave(String rut, String digitoVerificador, String codigoTipoProd)  throws RemoteException{
        if (getLogger().isEnabledFor(Level.INFO)) {
            getLogger().info("[consultarEstadoSegundaClave][BCI_INI] " + rut);
        }
        
        EstadoSegundaClaveTO[] resultado=null;
        try {
             SegundaClaveDAO segundaClaveDao = new SegundaClaveDAO();
             resultado = segundaClaveDao.consultarEstadoSegundaClave(rut,digitoVerificador,codigoTipoProd);
             
             if (getLogger().isEnabledFor(Level.DEBUG)) {
             	getLogger().debug("[consultarEstadoSegundaClave][respuesta obtenida]"
             			+ "[segundaClaveDao.consultarEstadoSegundaClave][" + resultado + "]");
             }
        
        } catch (Exception e) {
            if (getLogger().isEnabledFor(Level.ERROR)) {
                getLogger().error("[consultarEstadoSegundaClave][BCI_FINEX][Exception][" + rut + "]" + e.getMessage(), e);
            }			
		}
        if (getLogger().isEnabledFor(Level.INFO)) {
            getLogger().info("[consultarEstadoSegundaClave][BCI_FINOK]");
        }
        return resultado;
    }
    
/**
     * Metodo que obtiene desafio OTP.
 * <p>
 * Registro de versiones: 
 * <ul>
      * <li>1.0 10/07/2017 Sergio Flores (SEnTRA) - Ricardo Carrasco (Ing. Soft. BCI): Version inicial.</li>
 * </ul>
 * <p> 
     * @param usuarioSegundaClaveTO
     * @return OtpEntrustTO
     * @throws RemoteException OtpEntrustTO
 * @since 1.0
 */
public OtpEntrustTO obtenerOTP(UsuarioSegundaClaveTO usuarioSegundaClaveTO)  throws RemoteException{
    if (getLogger().isEnabledFor(Level.INFO)) {
        getLogger().info("[obtenerOTP][BCI_INI] " + usuarioSegundaClaveTO);
    }
    
    OtpEntrustTO resultado=null;
    try {
         SegundaClaveDAO segundaClaveDao = new SegundaClaveDAO();
         resultado = segundaClaveDao.obtenerOTP(usuarioSegundaClaveTO);
         
         if (getLogger().isEnabledFor(Level.DEBUG)) {
         	getLogger().debug("[obtenerOTP][respuesta obtenida]"
         			+ "[segundaClaveDao.obtenerOTP][" + resultado + "]");
         }
    
    } catch (Exception e) {
        if (getLogger().isEnabledFor(Level.ERROR)) {
            getLogger().error("[obtenerOTP][BCI_FINEX][Exception][" + usuarioSegundaClaveTO + "]" + e.getMessage(), e);
        }			
	}
    if (getLogger().isEnabledFor(Level.INFO)) {
        getLogger().info("[obtenerOTP][BCI_FINOK]");
    }
    return resultado;
 }
}
