package cl.bci.infraestructura.web.seguridad.segundaclave;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.oltu.oauth2.common.exception.OAuthProblemException;

import wcorp.model.seguridad.PrioridadSegundaClaveIdTO;
import wcorp.model.seguridad.to.TokenEntrustTO;
import wcorp.util.TablaValores;
import cl.bci.infraestructura.web.portal.util.Dominio;
import cl.bci.infraestructura.web.seguridad.mb.SesionMB;
import cl.bci.infraestructura.web.seguridad.segundaclave.to.CamposDeLlaveTO;
import cl.bci.infraestructura.web.seguridad.segundaclave.to.EstadoSegundaClaveTO;
import cl.bci.infraestructura.web.seguridad.segundaclave.to.ItemDeLlaveTO;
import cl.bci.infraestructura.web.seguridad.segundaclave.to.LlaveSegundaClaveTO;
import cl.bci.infraestructura.web.seguridad.segundaclave.to.ResultadoOperacionSegundaClaveTO;
import cl.bci.infraestructura.web.seguridad.segundaclave.to.SegundaClaveEnum;
import cl.bci.infraestructura.web.seguridad.segundaclave.to.UsuarioSegundaClaveTO;
import cl.bci.infraestructura.web.seguridad.to.UsuarioConectadoTO;
import cl.bci.infraestructura.web.seguridad.util.ConectorStruts;

/**
 * Clase con utilidades JSF requeridas dentro del contexto de infraestructura de
 * segunda clave.
 * 
 * Registro de versiones:
 * <ul>
 * <li>1.0 (30/04/2013 Mauricio Palma, Victor Hugo Enero. (Orand S.A.)): versi�n inicial.</li>
 * <li>1.1 23/06/2014 Venancio Aranguiz.(SEnTRA): Se modifican los siguientes m�todos: 
 *                   {@link #toEstadoSegundaClaveTO(wcorp.model.seguridad.EstadoSegundaClaveTO)} 
 *                   {@link #toEstadoSegundaClaveTO(EstadoSegundaClaveTO)} 
 *                   dado que se agreg� MetodosAutenticacion al trasvasije de dichos TO.</li>
 * <li>1.2 27/04/2016 Victor Enero (Orand) - Pablo Rompentin (Ing. Soft. BCI): Se agregan los siguientes m�todos:
 *                   {@link #getUsuarioSegundaClave((HttpServletRequest)},
 *                   {@link #getUserFromToken((HttpServletRequest)} para utilizar segunda clave en autenticaci�n
 *                   oauth.
 * <li>2.0 10/07/2017, Jaime Suazo Diaz (SEnTRA) - Ricardo Carrasco (Ing. Soft. BCI): Por proyecto migracion plataforma
 *         de autenticacion se modifica el metodo {@link #toEstadoSegundaClaveTO(wcorp.model.seguridad.EstadoSegundaClaveTO)}},
 *         {@link #toEstadoSegundaClaveTO(EstadoSegundaClaveTO)}</li>
 * </li>
 * </ul>
 * <p>
 * <b>Todos los derechos reservados por Banco de Cr�dito e Inversiones.</b>
 * </p>
 */
public class SegundaClaveUtil {

    /**
     * Logger de la clase.
     */
    private static Logger logger = (Logger) Logger.getLogger(SegundaClaveUtil.class);

    /**
     * tabla de par�metros de entrust.
     */
    private static final String TABLADEPARAMETROS = "entrust.parametros";
    

    /**
     * Esta clase solo tiene metodos estaticos.
     */
    private SegundaClaveUtil() {
    }

    /**
     * Obtiene usuario segunda clave.
     * 
     * @return UsuarioSegundaClaveTO usuario.
     */
    public static UsuarioSegundaClaveTO getUsuarioSegundaClave() {
        if (logger.isDebugEnabled()) {
            logger.debug("[getUsuarioSegundaClave] inicio");
        }
        SesionMB sesionMB = (SesionMB) ConectorStruts.getAttribHttp("sesionMB");
        String canalId = sesionMB.getCanalId();
        return new UsuarioSegundaClaveTO(sesionMB.getUsuarioConectado().getIdUsuarioConectado(), 
            Dominio.obtieneDominio(canalId).toString(), canalId);
    }
    
    /**
     * Obtiene usuario segunda clave.
     * <p>
     * <ul>
     * <li>1.0 27/04/2016 Victor Enero (Orand) - Pablo Rompentin (Ing. Soft. BCI): versi�n inicial.</li>
     * </ul>
     * 
     * @param req HttpServletRequest
     * @return UsuarioSegundaClaveTO usuario.
     * @throws OAuthProblemException Excepcion
	 * @since 1.2
     */
    public static UsuarioSegundaClaveTO getUsuarioSegundaClave(HttpServletRequest req) throws OAuthProblemException {
        if (logger.isDebugEnabled()) {
            logger.debug("[getUsuarioSegundaClave] [BCI_INI]");
        }
        
		UsuarioSegundaClaveTO usuario = null;
		
        if (req == null) {
            if (logger.isDebugEnabled()) {
                logger.debug("[getUsuarioSegundaClave] req == null");
            }
			
			usuario = getUsuarioSegundaClave();
			
			if (logger.isDebugEnabled()) {
				logger.debug("[getUsuarioSegundaClave] [BCI_FINOK]");
            }
			
            return usuario;
        }
        
        if (req.getRequestedSessionId() != null && req.isRequestedSessionIdValid()) {
        
            SesionMB sesionMB = (SesionMB) ConectorStruts.getAttribHttp("sesionMB");
            String canalId = sesionMB.getCanalId();
    
            if (logger.isDebugEnabled()) {
                logger.debug("[getUsuarioSegundaClave] Antes de obtener UsuarioConectadoTO");
            }
            UsuarioConectadoTO usuarioConectadoTO = sesionMB.getUsuarioConectado();
    
            if (usuarioConectadoTO != null) {
                if (logger.isDebugEnabled()) {
                    logger.debug("[getUsuarioSegundaClave] usuarioConectadoTO != null");
                }
                
                usuario = new UsuarioSegundaClaveTO(usuarioConectadoTO.getIdUsuarioConectado(), Dominio
                    .obtieneDominio(canalId).toString(), canalId);
            }
            else {
                if (logger.isDebugEnabled()) {
                    logger.debug("[getUsuarioSegundaClave] usuarioConectadoTO = null");
                }
                usuario = getUserFromToken(req);
                
            }
    
            if (logger.isDebugEnabled()) {
                logger.debug("[getUsuarioSegundaClave] [BCI_FINOK]");
            }
            return usuario;
        }
        
		usuario = getUserFromToken(req);
		
		if (logger.isDebugEnabled()) {
            logger.debug("[getUsuarioSegundaClave] [BCI_FINOK]");
        }
			
        return usuario;
    }
    
    /**
     * Metodo que obtiene el rut y el canal desde el token Oauth.
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 27/04/2016 Victor Enero (Orand) - Pablo Rompentin (Ing. Soft. BCI): Versi�n inicial.</li>
     * </ul>
     * <p>
     * @param req HttpServletRequest
     * @return UsuarioSegundaClaveTO Usuario conectado.
     * @throws OAuthProblemException Excepcion.
	 * @since 1.2
     */
    public static UsuarioSegundaClaveTO getUserFromToken(HttpServletRequest req) throws OAuthProblemException {

        if (logger.isEnabledFor(Level.DEBUG))
            logger.debug("[getUserFromToken] [BCI_INI]");

        String rut = OAuthUtils.getUsernameFromToken(req);
        if (rut == null) {
			if (logger.isEnabledFor(Level.DEBUG))
            	logger.debug("[getUserFromToken] [BCI_FINEX] Rut nulo");

            throw OAuthProblemException.error("Rut Invalid", "Rut Invalid.");
        }
		else {
        	rut = rut.replaceAll("-", "");
			if (logger.isEnabledFor(Level.DEBUG))
            	logger.debug("[getUserFromToken] ["+rut+"] Rut");
        }
        
        String canalId = OAuthUtils.getCanalFromToken(req);
        if (canalId == null) {

			if (logger.isEnabledFor(Level.DEBUG))
            	logger.debug("[getUserFromToken] [BCI_FINEX] Canal nulo");

            throw OAuthProblemException.error("Canal Invalid", "Canal Invalid.");
        }
        
        UsuarioSegundaClaveTO usuario = new UsuarioSegundaClaveTO(rut, Dominio
            .obtieneDominio(canalId).toString(), canalId);

        if (logger.isEnabledFor(Level.DEBUG))
                logger.debug("[getUserFromToken] [BCI_FINOK]");

        return usuario;

    }
    

    /**
     * Transferencia de datos de UsuarioSegundaClaveTO.
     * 
     * @param usuarioSegundaClave usuario de segunda clave.
     * @return UsuarioSegundaClaveTO usuario.
     */
    public static wcorp.model.seguridad.UsuarioSegundaClaveTO toUsuarioSegundaClaveTO(
        UsuarioSegundaClaveTO usuarioSegundaClave) {
        if (usuarioSegundaClave == null)
            return null;

        wcorp.model.seguridad.UsuarioSegundaClaveTO usuarioSegundaClaveTO = 
            new wcorp.model.seguridad.UsuarioSegundaClaveTO(usuarioSegundaClave.getUsuarioId(),
                usuarioSegundaClave.getDominioId(), usuarioSegundaClave.getCanalId());

        return usuarioSegundaClaveTO;
    }


    /**
     * Instancia de datos de UsuarioSegundaClaveTO.
     * 
     * @return UsuarioSegundaClaveTO usuario.
     */
    public static wcorp.model.seguridad.UsuarioSegundaClaveTO getUsuarioSegundaClaveWcorp() {
        SesionMB sesionMB = (SesionMB) ConectorStruts.getAttribHttp("sesionMB");
        String canalId = sesionMB.getCanalId();

        UsuarioConectadoTO usuarioConectadoTO = sesionMB.getUsuarioConectado();
        wcorp.model.seguridad.UsuarioSegundaClaveTO usuario = null;

            usuario = new wcorp.model.seguridad.UsuarioSegundaClaveTO(usuarioConectadoTO.getIdUsuarioConectado(),
                Dominio.obtieneDominio(canalId).toString(), canalId);
        


        return usuario;
    }

    
    /**
     * Transferencia de datos de PrioridadSegundaClaveIdTO.
     * 
     * @param segundaClaveEnum segunda clave.
     * @return PrioridadSegundaClaveIdTO prioridad.
     */
    public static PrioridadSegundaClaveIdTO toPrioridadSegundaClaveIdTO(SegundaClaveEnum segundaClaveEnum) {
        if (segundaClaveEnum == null)
            return null;
        return PrioridadSegundaClaveIdTO.getPorNombre(segundaClaveEnum.getNombre());
    }

    /**
     * Transferencia de datos de SegundaClaveEnum.
     * 
     * @param prioridadSegundaClaveIdTO prioridad de segunda clave.
     * @return SegundaClaveEnum enum de segunda clave.
     */
    public static SegundaClaveEnum toSegundaClaveEnum(PrioridadSegundaClaveIdTO prioridadSegundaClaveIdTO) {
        if (prioridadSegundaClaveIdTO == null)
            return null;
        return SegundaClaveEnum.obtieneSegundaClaveEnum(prioridadSegundaClaveIdTO.getNombre());
    }

    /**
     * M�todo que realiza la transferencia de datos de EstadoSegundaClaveTO.
     * 
     * Registro de versiones:
     * <ul>
     * <li>1.0 30/04/2013 Mauricio Palma, Victor Hugo Enero. (Orand S.A.): versi�n inicial.</li>
     * <li>1.1 23/06/2014 Venancio Aranguiz.(SEnTRA): Se agrega seteo del m�todo getMetodosAutenticacion del 
     *                    EstadoSegundaClaveTO al trasvasije.</li>
     * <li>2.0 10/07/2017, Jaime Suazo Diaz (SEnTRA) - Ricardo Carrasco (Ing. Soft. BCI): Por proyecto migracion plataforma de autenticacion
     *         se agregan seteos a este m�todo.</li>                   
     * </ul>
     * @param estadoSegundaClave estado de segunda clave.
     * @return EstadoSegundaClaveTO estado de segunda clave.
     */
    public static EstadoSegundaClaveTO toEstadoSegundaClaveTO(
        wcorp.model.seguridad.EstadoSegundaClaveTO estadoSegundaClave) {
        if (estadoSegundaClave == null)
            return null;

        EstadoSegundaClaveTO estadoSegundaClaveTO = new EstadoSegundaClaveTO();
        estadoSegundaClaveTO.setEstatus(estadoSegundaClave.isEstatus());
        estadoSegundaClaveTO.setGlosa(estadoSegundaClave.getGlosa());
        estadoSegundaClaveTO.setIdCodigoEstado(estadoSegundaClave.getIdCodigoEstado());
        estadoSegundaClaveTO.setMetodosAutenticacion(estadoSegundaClave.getMetodosAutenticacion());

        estadoSegundaClaveTO.setInformacionAnexa(estadoSegundaClave.getInformacionAnexa());

        if (estadoSegundaClave.getIdSegundaClave() == null)
            estadoSegundaClaveTO.setSegundaClaveEnum(null);
        else {
            estadoSegundaClaveTO.setSegundaClaveEnum(SegundaClaveEnum.obtieneSegundaClaveEnum(
                estadoSegundaClave.getIdSegundaClave().getNombre()));

        }
        ArrayList listaWebDispositivos = null;
        
        if (estadoSegundaClave.getListaDispositivos() != null && estadoSegundaClave.getListaDispositivos().length > 0) {
        	TokenEntrustTO[] listaDispositivos = estadoSegundaClave.getListaDispositivos();
        	
        	cl.bci.infraestructura.web.seguridad.segundaclave.to.TokenEntrustTO tokenWeb = null;
        	cl.bci.infraestructura.web.seguridad.segundaclave.to.UsuarioSegundaClaveTO usuarioWeb = null;
        	listaWebDispositivos = new ArrayList();
        	
        	int indice = 0;
        	for (; indice < listaDispositivos.length; indice++) {
        		TokenEntrustTO tokenWcorp = listaDispositivos[indice];
        		tokenWeb = new cl.bci.infraestructura.web.seguridad.segundaclave.to.TokenEntrustTO();
        		
        		tokenWeb.setCodigoRespuestaWs(tokenWcorp.getCodigoRespuestaWs());
        		tokenWeb.setDescripcionRespuestaWs(tokenWcorp.getDescripcionRespuestaWs());
        		tokenWeb.setEstado(tokenWcorp.getEstado());
        		
        		String estadoDespliegue = TablaValores.getValor(TABLADEPARAMETROS, tokenWcorp.getEstado(), "desc");
        		tokenWeb.setEstadoDespliegue(estadoDespliegue);
        		
        		tokenWeb.setFechaModificacion(tokenWcorp.getFechaModificacion());
        		tokenWeb.setGrupoToken(tokenWcorp.getGrupoToken());
        		tokenWeb.setNombreDispositivo(tokenWcorp.getNombreDispositivo());
        		tokenWeb.setSerialNumber(tokenWcorp.getSerialNumber());
        		tokenWeb.setTipoDispositivo(tokenWcorp.getTipoDispositivo());
        		
        		listaWebDispositivos.add(tokenWeb);
			}
        	
        	estadoSegundaClaveTO.setListaDispositivos((cl.bci.infraestructura.web.seguridad.segundaclave.to.TokenEntrustTO[]) 
        			listaWebDispositivos.toArray(new cl.bci.infraestructura.web.seguridad.segundaclave.to.TokenEntrustTO[0]));
        }

        return estadoSegundaClaveTO;
    }

    /**
     * M�todo que realiza la transferencia de datos de EstadoSegundaClaveTO.
     * 
     * Registro de versiones:
     * <ul>
     * <li>1.0 30/04/2013 Mauricio Palma, Victor Hugo Enero. (Orand S.A.): versi�n inicial.</li>
     * <li>1.1 23/06/2014 Venancio Aranguiz.(SEnTRA): Se agrega seteo del m�todo getMetodosAutenticacion del 
     *                    EstadoSegundaClaveTO al trasvasije.</li>
     * <li>2.0 10/07/2017, Jaime Suazo Diaz (SEnTRA) - Ricardo Carrasco (Ing. Soft. BCI): Por proyecto migracion plataforma de autenticacion
     *         se agregan seteos a este m�todo.</li>                   
     * </ul>
     * @param estadoSegundaClave estado de segunda clave.
     * @return EstadoSegundaClaveTO estado de segunda clave.
     */
    public static wcorp.model.seguridad.EstadoSegundaClaveTO toEstadoSegundaClaveTO(
        EstadoSegundaClaveTO estadoSegundaClave) {
        if (estadoSegundaClave == null)
            return null;

        wcorp.model.seguridad.EstadoSegundaClaveTO estadoSegundaClaveTO = 
            new wcorp.model.seguridad.EstadoSegundaClaveTO();
        estadoSegundaClaveTO.setEstatus(estadoSegundaClave.isEstatus());
        estadoSegundaClaveTO.setGlosa(estadoSegundaClave.getGlosa());
        estadoSegundaClaveTO.setIdCodigoEstado(estadoSegundaClave.getIdCodigoEstado());

        estadoSegundaClaveTO.setInformacionAnexa(estadoSegundaClave.getInformacionAnexa());
        estadoSegundaClaveTO.setMetodosAutenticacion(estadoSegundaClave.getMetodosAutenticacion());

        if (estadoSegundaClave.getSegundaClaveEnum() == null)
            estadoSegundaClaveTO.setIdSegundaClave(null);
        else {
            estadoSegundaClaveTO.setIdSegundaClave(PrioridadSegundaClaveIdTO.getPorNombre(
                estadoSegundaClave.getSegundaClaveEnum().getNombre()));
        }

        ArrayList listaDispositivosWcorpUtils = null;
        
        if (estadoSegundaClave.getListaDispositivos() != null && estadoSegundaClave.getListaDispositivos().length > 0) {
        	cl.bci.infraestructura.web.seguridad.segundaclave.to.TokenEntrustTO[] listaDispositivos = estadoSegundaClave.getListaDispositivos();
        	
        	TokenEntrustTO tokenWcorpUtils = null;
        	wcorp.model.seguridad.UsuarioSegundaClaveTO usuarioWeb = null;
        	listaDispositivosWcorpUtils = new ArrayList();
        	
        	int indice = 0;
        	for (; indice < listaDispositivos.length; indice++) {
        		cl.bci.infraestructura.web.seguridad.segundaclave.to.TokenEntrustTO 
        			tokenWebBCI = (cl.bci.infraestructura.web.seguridad.segundaclave.to.TokenEntrustTO) listaDispositivos[indice];
        		
        		tokenWcorpUtils = new TokenEntrustTO();
        		
        		tokenWcorpUtils.setCodigoRespuestaWs(tokenWebBCI.getCodigoRespuestaWs());
        		tokenWcorpUtils.setDescripcionRespuestaWs(tokenWebBCI.getDescripcionRespuestaWs());
        		tokenWcorpUtils.setEstado(tokenWebBCI.getEstado());
        		        		
        		tokenWcorpUtils.setFechaModificacion(tokenWebBCI.getFechaModificacion());
        		tokenWcorpUtils.setGrupoToken(tokenWebBCI.getGrupoToken());
        		tokenWcorpUtils.setNombreDispositivo(tokenWebBCI.getNombreDispositivo());
        		tokenWcorpUtils.setSerialNumber(tokenWebBCI.getSerialNumber());
        		tokenWcorpUtils.setTipoDispositivo(tokenWebBCI.getTipoDispositivo());
        		
        		listaDispositivosWcorpUtils.add(tokenWcorpUtils);
			}
        	
        	estadoSegundaClaveTO.setListaDispositivos((TokenEntrustTO[]) listaDispositivosWcorpUtils.toArray(new TokenEntrustTO[0]));
        }
        

        return estadoSegundaClaveTO;
    }

    /**
     * Transferencia de datos de ItemDeLlaveTO.
     * 
     * @param itemDeLlave item de llave.
     * @return ItemDeLlaveTO objeto item de llave.
     */
    public static ItemDeLlaveTO toItemDeLlaveTO(
        wcorp.model.seguridad.ItemDeLlaveTO itemDeLlave){
        if (itemDeLlave == null)
            return null;
        ItemDeLlaveTO itemDeLlaveTO = new ItemDeLlaveTO();
        itemDeLlaveTO.setNombre(itemDeLlave.getNombre());
        itemDeLlaveTO.setPosicion(itemDeLlave.getPosicion());
        itemDeLlaveTO.setValorBooleano(itemDeLlave.getValorBooleano());
        itemDeLlaveTO.setValorCalendar(itemDeLlave.getValorCalendar());
        itemDeLlaveTO.setValorChar(itemDeLlaveTO.getValorChar());
        itemDeLlaveTO.setValorDate(itemDeLlave.getValorDate());
        itemDeLlaveTO.setValorNumerico(itemDeLlave.getValorNumerico());
        itemDeLlaveTO.setValorString(itemDeLlave.getValorString());

        return itemDeLlaveTO;
    }

    /**
     * Transferencia de datos de ItemDeLlaveTO.
     * 
     * @param itemDeLlave item de llave.
     * @return ItemDeLlaveTO objeto item de llave.
     */
    public static wcorp.model.seguridad.ItemDeLlaveTO toItemDeLlaveTO(
        ItemDeLlaveTO itemDeLlave){
        if (itemDeLlave == null)
            return null;
        wcorp.model.seguridad.ItemDeLlaveTO itemDeLlaveTO = new wcorp.model.seguridad.ItemDeLlaveTO();
        itemDeLlaveTO.setNombre(itemDeLlave.getNombre());
        itemDeLlaveTO.setPosicion(itemDeLlave.getPosicion());
        itemDeLlaveTO.setValorBooleano(itemDeLlave.getValorBooleano());
        itemDeLlaveTO.setValorCalendar(itemDeLlave.getValorCalendar());
        itemDeLlaveTO.setValorChar(itemDeLlave.getValorChar());
        itemDeLlaveTO.setValorDate(itemDeLlave.getValorDate());
        itemDeLlaveTO.setValorNumerico(itemDeLlave.getValorNumerico());
        itemDeLlaveTO.setValorString(itemDeLlave.getValorString());

        return itemDeLlaveTO;
    }

    /**
     * Transferencia de datos de ItemDeLlaveTO[].
     * 
     * @param itemsDeLlave items de llave.
     * @return ItemDeLlaveTO[] objeto item de llave.
     */
    public static ItemDeLlaveTO[] toItemsDeLlaveTO(
        wcorp.model.seguridad.ItemDeLlaveTO[] itemsDeLlave){
        if (itemsDeLlave == null)
            return null;

        ItemDeLlaveTO[] itemsDeLlaveTO = new ItemDeLlaveTO[itemsDeLlave.length];
        for (int i = 0; i < itemsDeLlave.length; i++) {
            itemsDeLlaveTO[i] = toItemDeLlaveTO(itemsDeLlave[i]);
        }

        return itemsDeLlaveTO;
    }

    /**
     * Transferencia de datos de ItemDeLlaveTO[].
     * 
     * @param itemsDeLlave items de llave.
     * @return ItemDeLlaveTO[] objeto item de llave.
     */
    public static wcorp.model.seguridad.ItemDeLlaveTO[] toItemsDeLlaveTO(
        ItemDeLlaveTO[] itemsDeLlave){
        if (itemsDeLlave == null)
            return null;

        wcorp.model.seguridad.ItemDeLlaveTO[] itemsDeLlaveTO = 
        		new wcorp.model.seguridad.ItemDeLlaveTO[itemsDeLlave.length];
        for (int i = 0; i < itemsDeLlave.length; i++) {
            itemsDeLlaveTO[i] = toItemDeLlaveTO(itemsDeLlave[i]);
        }

        return itemsDeLlaveTO;
    }

    /**
     * Transferencia de datos de CamposDeLlaveTO.
     * 
     * @param camposDeLlave campos de llave.
     * @return CamposDeLlaveTO objeto item de llave.
     */
    public static CamposDeLlaveTO toCamposDeLlaveTO(
        wcorp.model.seguridad.CamposDeLlaveTO camposDeLlave){
        if (camposDeLlave == null)
            return null;

        CamposDeLlaveTO camposDeLlaveTO = new CamposDeLlaveTO();
        camposDeLlaveTO.setHash(camposDeLlave.toHashMap());

        return camposDeLlaveTO;
    }

    /**
     * Transferencia de datos de CamposDeLlaveTO.
     * 
     * @param camposDeLlave campos de llave.
     * @return CamposDeLlaveTO objeto item de llave.
     */
    public static wcorp.model.seguridad.CamposDeLlaveTO toCamposDeLlaveTO(
        CamposDeLlaveTO camposDeLlave){
        if (camposDeLlave == null)
            return null;

        wcorp.model.seguridad.CamposDeLlaveTO camposDeLlaveTO = new wcorp.model.seguridad.CamposDeLlaveTO();
        camposDeLlaveTO.setHash(camposDeLlave.toHashMap());

        return camposDeLlaveTO;
    }

    /**
     * Transferencia de datos de LlaveSegundaClaveTO.
     * 
     * @param llaveSegundaClave llave segunda llave.
     * @return LlaveSegundaClaveTO objeto item de llave.
     */
    public static LlaveSegundaClaveTO toLlaveSegundaClaveTO(
        wcorp.model.seguridad.LlaveSegundaClaveTO llaveSegundaClave) {
        if (llaveSegundaClave == null)
            return null;

        LlaveSegundaClaveTO llaveSegundaClaveTO = new LlaveSegundaClaveTO();
        llaveSegundaClaveTO.setCamposDeLlave(toCamposDeLlaveTO(llaveSegundaClave.getCamposDeLlave()));
        llaveSegundaClaveTO.setEstatus(llaveSegundaClave.isEstatus());
        llaveSegundaClaveTO.setGlosa(llaveSegundaClave.getGlosa());
        llaveSegundaClaveTO.setIdCodigoEstado(llaveSegundaClave.getIdCodigoEstado());
        llaveSegundaClaveTO.setInformacionAnexa(llaveSegundaClave.getInformacionAnexa());
        llaveSegundaClaveTO.setLlave(llaveSegundaClave.getLlave());
        if (llaveSegundaClave.getSerial()!=null)
            llaveSegundaClaveTO.setSerial(llaveSegundaClave.getSerial());

        if (llaveSegundaClave.getIdSegundaClave() == null)
            llaveSegundaClaveTO.setSegundaClaveEnum(null);
        else {
            llaveSegundaClaveTO.setSegundaClaveEnum(SegundaClaveEnum.obtieneSegundaClaveEnum(
                llaveSegundaClave.getIdSegundaClave().getNombre()));
        }

        if (llaveSegundaClave.getLlaves() != null){
            llaveSegundaClaveTO.setLlaves(llaveSegundaClave.getLlaves());
        }

        return llaveSegundaClaveTO;
    }

    /**
     * Transferencia de datos de LlaveSegundaClaveTO.
     * 
     * 
     * @param llaveSegundaClave llave segunda llave.
     * @return LlaveSegundaClaveTO objeto item de llave.
     */
    public static wcorp.model.seguridad.LlaveSegundaClaveTO toLlaveSegundaClaveTO(
        LlaveSegundaClaveTO llaveSegundaClave) {
        if (llaveSegundaClave == null)
            return null;

        wcorp.model.seguridad.LlaveSegundaClaveTO llaveSegundaClaveTO = 
            new wcorp.model.seguridad.LlaveSegundaClaveTO();
        llaveSegundaClaveTO.setCamposDeLlave(toCamposDeLlaveTO(llaveSegundaClave.getCamposDeLlave()));
        llaveSegundaClaveTO.setEstatus(llaveSegundaClave.isEstatus());
        llaveSegundaClaveTO.setGlosa(llaveSegundaClave.getGlosa());
        llaveSegundaClaveTO.setIdCodigoEstado(llaveSegundaClave.getIdCodigoEstado());
        llaveSegundaClaveTO.setInformacionAnexa(llaveSegundaClave.getInformacionAnexa());
        llaveSegundaClaveTO.setLlave(llaveSegundaClave.getLlave());
        if (llaveSegundaClave.getSerial()!=null)
            llaveSegundaClaveTO.setSerial(llaveSegundaClave.getSerial());

        if (llaveSegundaClave.getSegundaClaveEnum() == null)
            llaveSegundaClaveTO.setIdSegundaClave(null);
        else {
            llaveSegundaClaveTO.setIdSegundaClave(PrioridadSegundaClaveIdTO.getPorNombre(
                llaveSegundaClave.getSegundaClaveEnum().getNombre()));
        }

        return llaveSegundaClaveTO;
    }

    /**
     * Transferencia de datos de LlaveSegundaClaveTO.
     * 
     * @param resultadoOperacionSegundaClave resultado de operacion de segunda llave.
     * @return ResultadoOperacionSegundaClaveTO objeto resultado de operacion de segunda llave.
     */
    public static wcorp.model.seguridad.ResultadoOperacionSegundaClaveTO toResultadoOperacionSegundaClaveTO(
        ResultadoOperacionSegundaClaveTO resultadoOperacionSegundaClave) {

        if (resultadoOperacionSegundaClave == null)
            return null;

        wcorp.model.seguridad.ResultadoOperacionSegundaClaveTO resultadoOperacionSegundaClaveTO = 
        		new wcorp.model.seguridad.ResultadoOperacionSegundaClaveTO();
        resultadoOperacionSegundaClaveTO.setEstatus(resultadoOperacionSegundaClave.isEstatus());
        resultadoOperacionSegundaClaveTO.setGlosa(resultadoOperacionSegundaClave.getGlosa());
        resultadoOperacionSegundaClaveTO.setIdCodigoEstado(resultadoOperacionSegundaClave.getIdCodigoEstado());
        resultadoOperacionSegundaClaveTO.setInformacionAnexa(resultadoOperacionSegundaClave.getInformacionAnexa());

        if (resultadoOperacionSegundaClave.getIdSegundaClave() == null)
            resultadoOperacionSegundaClaveTO.setIdSegundaClave(null);
        else {
            resultadoOperacionSegundaClaveTO.setIdSegundaClave(PrioridadSegundaClaveIdTO.getPorNombre(
                resultadoOperacionSegundaClave.getIdSegundaClave().getNombre()));
        }

        return resultadoOperacionSegundaClaveTO;
    }
}
