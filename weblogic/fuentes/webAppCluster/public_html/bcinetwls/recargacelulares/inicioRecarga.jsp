<%@taglib prefix="html"  uri="/WEB-INF/struts-html.tld"%>
<%@taglib prefix="bean"  uri="/WEB-INF/struts-bean.tld"%>
<%@taglib prefix="logic" uri="/WEB-INF/struts-logic.tld"%>
<%@taglib prefix="c"     uri="http://java.sun.com/jstl/core"%>
<%@taglib prefix="fmt"   uri="http://java.sun.com/jstl/fmt"%>
<%@page import="java.util.ArrayList" %>
<%@ page import="wcorp.util.TablaValores"%>
<%@ page import="wcorp.util.HTMLUtil"%>

<jsp:useBean id="sessionBci" scope="session" class="wcorp.model.seguridad.SessionBCI"></jsp:useBean>
<jsp:setProperty name="sessionBci" property="*" />

<fmt:setLocale value="es-CL"/>
<%
String mensajeSaldo = TablaValores.getValor("LineaEmergencia.parametros", "110-ttff", "mensaje") != null ? TablaValores.getValor("LineaEmergencia.parametros", "110-ttff", "mensaje"): TablaValores.getValor("LineaEmergencia.parametros", "110", "mensaje");
  mensajeSaldo = wcorp.util.HTMLUtil.codificaEntidades(mensajeSaldo); 
  String mensaje = TablaValores.getValor("recargaCelular.parametros","inicio","desc");
  String mensaje2 = TablaValores.getValor("recargaCelular.parametros","inicio2","desc");
  %>
<html:html>
<head>
<title>Recarga de Celular</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<script src="/bcinetwls/recargacelulares/js/common.js" type="text/javascript"></script>
<script src="/bcinetwls/recargacelulares/js/recargas.js" type="text/javascript" LANGUAGE="JavaScript1.5"></script>
<script src="/bcinetwls/recargacelulares/js/cuentas.js" type="text/javascript" LANGUAGE="JavaScript1.5"></script>
<script src="/bcinetwls/recargacelulares/js/validacionRecarga.js" type="text/javascript" LANGUAGE="JavaScript1.5"></script>
<link href="/bcinetwls/recargacelulares/style/estiloBcinuevo.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">

var operadoras=new Array(); 
<c:forEach var="operadora" items="${operadoras}">
   var unaOp=new Operadora('<c:out value="${operadora.idOperadora}"/>','<c:out value="${operadora.descripcionOperadora}"/>')
       unaOp.setTipoRango('<c:out value="${operadora.idTipoRango}"/>') 
       unaOp.setValorInicial('<c:out value="${operadora.valorInicial}"/>')
       unaOp.setValorFinal('<c:out value="${operadora.valorFinal}"/>')
       unaOp.setTipoOperadora('<c:out value="${operadora.tipoOperadora}"/>')

   <c:forEach var="promociones" items="${operadora.promociones}">
       unaOp.addMonto('<c:out value="${promociones}"/>')
   </c:forEach>
   
      operadoras['<c:out value="${operadora.idOperadora}"/>']=unaOp;   
</c:forEach>

var cuentas=new Array();
<c:forEach var="cuenta" items="${cuentas}">
     var unaCuenta=new Cuenta('<c:out value="${cuenta.numeroCuenta}"/>','<c:out value="${cuenta.tipoCuenta}"/>','<c:out value="${cuenta.tipoCuenta}"/>')
         unaCuenta.setSaldoCuenta(cambiaComaPunto('<fmt:formatNumber value="${cuenta.saldoDisponible}" pattern="$#,##0"/>'))
         unaCuenta.setSaldoSobreGiro(cambiaComaPunto('<fmt:formatNumber value="${cuenta.sobregiroDisponible}" pattern="$#,##0"/>'))
         unaCuenta.setTotalDisponible(cambiaComaPunto('<fmt:formatNumber value="${cuenta.saldoDisponible + cuenta.sobregiroDisponible}" pattern="$#,##0"/>'))
         unaCuenta.setSaldoCuentaNumber('<c:out value="${cuenta.saldoDisponible}" />')
         unaCuenta.setSaldoSobreGiroNumber('<c:out value="${cuenta.sobregiroDisponible}" />')
         unaCuenta.setTotalDisponibleNumber('<c:out value="${cuenta.saldoDisponible + cuenta.sobregiroDisponible}"/>')         
         unaCuenta.setSaldoLineaDeSobregiro('<fmt:formatNumber value="${cuenta.saldoLineaDeSobregiro}" pattern="$#,##0"/>')
         unaCuenta.setSaldoLineaDeEmergencia('<fmt:formatNumber value="${cuenta.saldoLineaDeEmergencia}" pattern="$#,##0"/>')
         
    cuentas['<c:out value="${cuenta.numeroCuenta}"/>']=unaCuenta;   
</c:forEach>

function showClientId() {
	o = document.getElementById('client-id-help');
	a = document.getElementById('client-id-help2');
	if ( o && a ) {
		o.style.display='block';
		a.style.display='none';
	}
}
function hideClientId() {
	o = document.getElementById('client-id-help');
	a = document.getElementById('client-id-help2');
	if ( o && a ) {
		o.style.display='none';
		a.style.display='block';
	}
}

function switchEmailEdit() {
	o = document.getElementById( 'edit-email-field' );
	b = document.getElementById( 'edit-email-button' );
	if ( o && b ) {
		if ( o.disabled ) {
			b.value='Deshacer';
			b.className = 'task undo-button';
			b.disabled = true;
			ge('edit-email-field-instruction').style.display='block';
		} else {
			b.value='Modificar email';
			o.value='pepe@hotmail.com';
			b.className = 'task';
			ge('edit-email-field-instruction').style.display='none';
		}
		o.disabled = !o.disabled;
	}
}

var tablasOperadores=new Array();

function construyeTabla(){
    
    var pos=0;
    <%
      ArrayList objOP=(ArrayList)request.getAttribute("matOp");
      if(objOP!=null){
      for(int x=0;x<objOP.size();x++){
         String[][] matrizOperadoras=(String[][])objOP.get(x);
         if(matrizOperadoras!=null){%>
         var table='<table border="0" style="border:solid 1px #CCCCCC"; id="montos_recarga"  cellpadding="0" cellspacing="0" width="80%"><tbody>';
         <%for(int i=0; i<matrizOperadoras.length;i++){%>
             if(<%=i%>==0){
            	table+="<tr class='monto' id='fila<%=i%>' style='border-left: 1px solid rgb(204, 204, 204);border-top: 1px solid rgb(204, 204, 204);' >";
            }else{
            	table+="<tr class='monto' id='fila<%=i%>' style='border-left: 1px solid rgb(204, 204, 204);border-top: 1px solid rgb(204, 204, 204);'>";
            }
            <%for(int j=0; j<matrizOperadoras[0].length;j++){%>
             if(<%=i%>==0){
             	table+="<th align='center'  style='border-right: 1px solid rgb(204, 204, 204); border-bottom: 1px solid rgb(204, 204, 204);'>"+ "<%=matrizOperadoras[i][j]%>"+"</th>";
             }else{
             	obj=document.getElementById("montoSelect");
				var indice=obj.selectedIndex+1;
				var estilo ="";
            	if(indice==<%=i%>){
            		estilo="background-color:#FFFF99;";
            	}else{
            		estilo="background-color:#F5F7FC;";
            	}
            	if(<%=j%>==0){
           		table+="<th align='center' style='border-right: 1px solid rgb(204, 204, 204); border-bottom: 1px solid rgb(204, 204, 204);"+estilo+"'>"+ "<%=matrizOperadoras[i][j]%>"+"</th>";
           		}else{
           			table+="<td align='center' style='border-right: 1px solid rgb(204, 204, 204); border-bottom: 1px solid rgb(204, 204, 204);"+estilo+"'>"+ "<%=matrizOperadoras[i][j]%>"+"</th>";
           		}
             }
            <%}%>
            table+="</tr>";
          <%}
          }else{%>
            table="";
          <%}%>
         table+="</tbody></table>";
         tablasOperadores[pos]=table;
        pos++;        
      <%}
      }%>      
}

function muestraTabla(pos){
   if(pos!=-1){
	var posFin = pos-1;
   	document.getElementById('tablaOp').innerHTML=tablasOperadores[posFin];
   }
   else{
   	document.getElementById('tablaOp').innerHTML="";
   }
}

function pintaFila(campo){
    construyeTabla();
	if(RecargaCelularesForm.operador_movil.selectedIndex != 0){
		muestraTabla(RecargaCelularesForm.operador_movil.selectedIndex);
	}
}

function despliegaSaldosCuenta() {
	selectSaldos();
	return '';
}

function cambia(which){ 
lineaAcambiar="fila"+which;

obj=document.getElementById("fila"+which);
if(obj!=null){

obj.style.background='#FFFF99'; 
var hayMasFilas=false;


    hayMasFilas=true;
}
var i=0;
while(hayMasFilas){
    nombre="fila"+i;
    obj=document.getElementById(nombre);
    if((obj!=null)){
        if(lineaAcambiar!=nombre){
          
           obj.style.background='#FFFFFF';
        }
    }else{
        hayMasFilas=false;
    }
    i++;
    
}
} 

var numerosAlias = new Array();
var contador=0;
<c:forEach items="${recargas}" var="alias">
	numerosAlias[contador] = new Array(4);
	numerosAlias[contador][0] = '<c:out value="${alias.nroCelularCliente}"/>';
	numerosAlias[contador][1] = '<c:out value="${alias.codigoAlias}"/>';
	numerosAlias[contador][2] = '<c:out value="${alias.aliasRecarga}"/>';
	numerosAlias[contador][3] = '<c:out value="${alias.nombreOperadora}"/>';
	contador++;
</c:forEach>

function mostrarAlias(){
	form = document.forms[0];
	<c:forEach items="${recargas}" var="alias">
		if(form.alias_recarga.value == '<c:out value='${alias.aliasRecarga}'/>'){
			form.operador_movil.value = <c:out value="${alias.idOperadora}"/>;
			form.codigo_alias.value = <c:out value="${alias.codigoAlias}"/>;
			var tipoOperadoraAlias = '<c:out value="${alias.tipoOperadora}"/>';
			form.tipoOperadora.value=tipoOperadoraAlias;
			muestraMontos();
			pintaFila(form.operador_movil.value);
			var celular = '<c:out value="${alias.nroCelularCliente}"/>';
			form.numeroCelular.value=celular;
			form.reNumeroCelular.value=celular;
			form.montoSelect = '<c:out value="${alias.monto}"/>';
			form.cuenta_origen = '<c:out value="${alias.nroCuentaCliente}"/>';
		}
	</c:forEach>
}
function cambiaDespliegue()
{
	for (i=0;i<document.forms[0].tipo_recarga.length;i++){
    	if (document.forms[0].tipo_recarga[i].checked){
        	if (document.forms[0].tipo_recarga[i].value == "1"){
				document.getElementById("modificar_recarga_frecuente").style.display = 'block';
        		document.forms[0].alias_recarga.disabled = false;
        		document.forms[0].operador_movil.disabled = true;
        		document.forms[0].numeroCelular.readOnly = true;
        		document.forms[0].reNumeroCelular.readOnly = true;
        	}
        	else{
				document.getElementById("modificar_recarga_frecuente").style.display = 'none';
        		document.forms[0].alias_recarga.value="-1";
        		document.forms[0].alias_recarga.disabled = true;
        		document.forms[0].operador_movil.value="-1";
        		document.forms[0].operador_movil.disabled = false;
        		document.forms[0].codigo_alias.value = 0;
        		muestraMontos();
        		pintaFila(document.forms[0].operador_movil.value);
        		document.forms[0].numeroCelular.readOnly = false;
        		document.forms[0].reNumeroCelular.readOnly = false;
        		document.forms[0].numeroCelular.value = "";
        		document.forms[0].reNumeroCelular.value = "";
        	}
        }
    }

}

function recargaFrecuente(){
	if('<c:out value="${recargaSeleccionada}"/>' != ""){
		form = document.forms[0];
		form.alias_recarga.value = '<c:out value="${recargaSeleccionada}"/>';
		form.tipo_recarga[0].checked = true;
		cambiaDespliegue();
		mostrarAlias();
	}
}
</script>
</head>
<fmt:setBundle basename="resources.mensajes-recargaCelTV"/>
<body onLoad="muestraMontos();construyeTabla();recargaFrecuente();">
<html:form action="/confirmaRecarga">
<html:hidden property="saldoCuentaCte"/>     
<html:hidden property="saldoSobreGiroCte"/>     
<html:hidden property="saldoDisponibleCte"/>     
<html:hidden property="nombreCuentaCte"/> 
<html:hidden property="monto_carga"/>
<html:hidden property="nombre_operador"/>
<html:hidden property="tipoOperadora"/>
<input name="codigo_alias" type="hidden" > 
<input name="disp_recarga" id="disp_recarga" type="hidden"/>
<input name="nombre_cuenta" id="nombre_cuenta" type="hidden"/>
<table width="100%" border="0" cellspacing="0" cellpadding="0" id="contenidos">
  <tr>
    <td width="1030" id="tdcontenido"><h1><fmt:message key="msg.recarga"/>&nbsp;<span class="titulo"><fmt:message key="msg.titulo"/></span></h1>
    <div class="mensaje_plano" id="modificar_recarga_frecuente" style="display:none;">
      <p><fmt:message key="msg.msje_plano"/> 
	  <a href="/bcinetwls/recargacelulares/eliminaAlias.do?accion=inicio"><fmt:message key="msg.aqui"/></a>           
    </div></p>
    
        
      <table width="350" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><h3><fmt:message key="msg.paso"/> <strong><fmt:message key="msg.paso1"/></strong> <fmt:message key="msg.pasoDe"/></h3> </td>
        </tr>
      </table>      
	<table border="0" cellpadding="0" cellspacing="0" id="borde">
      <tr>
        <th width="6"><img src="images/curvaizquierdaarriba.gif" width="6" height="6" /></th>
        <td id="bordearribatabla"></td>
        <td width="6" height="6" align="right"><img src="images/curvaderechaarriba.gif" width="6" height="6" /></td>
      </tr>
      <tr>
        <td width="6" height="6" id="bordeizqmorado">&nbsp;</td>
        <th class="morado">          </th>
        <td id="bordederechomorado">&nbsp;</td>
      </tr>
      <tr>
        <td id="tdbordeizq">&nbsp;</td>
        <td class="centrotablaformularios">
          <table border="0" cellpadding="0" cellspacing="0" id="datostabuladosformularios" >
          <tr  >
            <td colspan="3" id="titulosdatos"><fmt:message key="msg.titulo_seccion1"/></td>
          </tr>
          <tr  >
             <td width="22%" id="nombredato"><strong><fmt:message key="msg.recargarA"/>&nbsp;<fmt:message key="msg.dosPtos"/></strong></td>
             <td width="23%"><fmt:message key="msg.tripleBlank"/><html:radio property="tipo_recarga" value="1" onclick="cambiaDespliegue();"><fmt:message key="msg.combo"/></html:radio></td>
             <td align="left">
            <html:select property="alias_recarga" onchange="mostrarAlias();" disabled="true">
                <html:option value="-1"><fmt:message key="msg.seleccione"/></html:option>
                <logic:present name="recargas">
                <html:options collection="recargas" property="aliasRecarga" labelProperty="aliasRecarga" />
               	</logic:present>
            </html:select>
                 
            </td>
          </tr>
          <tr>
            <td></td>
            <td class="separador"><fmt:message key="msg.tripleBlank"/><html:radio property="tipo_recarga" value="2" onclick="cambiaDespliegue();"><fmt:message key="msg.seleccion"/></html:radio></td>
          </tr>
          <tr  >
            <td width="22%" id="nombredato"><strong><fmt:message key="msg.compania"/>&nbsp;<fmt:message key="msg.dosPtos"/></strong></td>
            <td width="10%"><html:select property="operador_movil" onchange="muestraMontos();pintaFila(this.value);revisaTipoOperadora();" disabled="true">
                <html:option value="-1"><fmt:message key="msg.seleccioneCia"/></html:option>
                <html:options collection="operadoras" property="idOperadora" labelProperty="descripcionOperadora" />
            </html:select>
            </td>
                <td rowspan="4" >
                <div id="tablaOp">&nbsp;</div>
                </td>            
          </tr>
          <tr >
            <td id="nombredato">
                <div id="glosaCelular"><strong><fmt:message key="msg.numCel"/>&nbsp;<fmt:message key="msg.dosPtos"/></strong></div>
                <div id="glosaRut" style="display:none;"><strong><fmt:message key="msg.ingreseRut"/>&nbsp;<fmt:message key="msg.dosPtos"/></strong></div>
            </td>
            <td>
                <table class="tabla_numCel_rut">
                <tr>
                    <td><html:text property="numeroCelular" size="10" maxlength="10" readonly="true" onkeypress="return soloNumerosyGuion(event);" onChange="derivaValidacion(this.value,1);"/></td>
                    <td id="ejemploCelular"><fmt:message key="msg.ejemCel"/></td>
                </tr>
                </table>
            </td>
          </tr>
          <tr >
            <td id="nombredato">
                <div id="reingreseglosaCelular"><fmt:message key="msg.reinCel"/>�<fmt:message key="msg.dosPtos"/></div>
                <div id="reingreseglosaRut" style="display:none;"><strong><fmt:message key="msg.reingrese"/>�<fmt:message key="msg.dosPtos"/></strong></div>
            </td>            <td>
                <html:text property="reNumeroCelular" size="10" maxlength="10" readonly="true" onkeypress="return soloNumerosyGuion(event);" onChange="derivaValidacion(this.value,2);"/>   
            </td> 
          </tr>
          <tr >
            <td id="nombredato"><fmt:message key="msg.monto"/>�<fmt:message key="msg.dosPtos"/></td>
            <td><div id="montoEnSelect"><select name="montoSelect" id="montoSelect" onChange="cambiaValorMonto();pintaFila(this);" >
            </select></div><div id="montoEnTextBox"><input type="text" maxlength="9" name="montoBox" onkeypress="return soloNumerosYPunto(event);" onChange="document.RecargaCelularesForm.monto_carga.value=this.value"><div class="destacadochico" align="left"  id="rangoValor" ></div>
            </div>
            </td>
          </tr>
        </table>
          <table border="0" cellpadding="0" cellspacing="0" id="datostabuladosformularios" >
            <tr  >
              <td colspan="2" id="titulosdatos"><fmt:message key="msg.cargocta"/></td>
            </tr>
            <tr >
              <td width="22%" id="nombredato"><strong><fmt:message key="msg.ctaOri"/>�<fmt:message key="msg.dosPtos"/></strong></td>
              <td width="45%"><html:select property="cuenta_origen" onchange="selectSaldos()"  >
                <html:option value="-1"><fmt:message key="msg.seleccioneCta"/></html:option>
                <c:forEach items="${cuentas}" var="cuenta">
                  <c:if test="${cuenta.tipoCuenta == 'CTA_CTE'}" >
                    <option value="<c:out value='${cuenta.numeroCuenta}'/>"><fmt:message key="msg.CtaCte"/>
                    <c:out value='${cuenta.numeroCuenta}'/>
                    </option>
                  </c:if>
                  <c:if test="${cuenta.tipoCuenta == 'CTA_PRM'}" >
                    <option value="<c:out value='${cuenta.numeroCuenta}'/>"><fmt:message key="msg.ctaCpr"/>
                      <c:out value='${cuenta.numeroCuenta}'/>
                    </option>
                  </c:if>
                </c:forEach>
              </html:select></td>
            </tr>
            <tr >
              <td id="nombredato"><fmt:message key="msg.sdoAct"/>�<fmt:message key="msg.dosPtos"/></td>
              <td><span id="saldo-cuenta-origen"><div  id="saldo_cuenta">�</div></span></td>
            </tr>
            <tr style="display:none;">
              <td id="nombredato"><fmt:message key="msg.sdoDispLsg"/>
                : </td>
              <td><span  id="saldo_sobregiro">�</span><span class="destacadochico"><%=mensajeSaldo%></span></td>
            </tr>
            <tr >
              <td id="nombredato"><fmt:message key="msg.sdoDispLsg"/>
                : </td>
              <td><span  id="saldoLineaDeSobregiro">�</span></td>
            </tr>
            <tr >
              <td id="nombredato"><fmt:message key="msg.sdoDispEme"/>�<fmt:message key="msg.dosPtos"/></td>
              <td><span  id="saldoLineaDeEmergencia">�</span></td>
            </tr>
            <tr >
              <td id="nombredato"><fmt:message key="msg.dispRec"/>�<fmt:message key="msg.dosPtos"/></td>
              <td class="importante"><strong class="saldo-destacado"><span id="saldo-cuenta-origen2"><div  id="saldo_recarga">�</div></span></strong></td>
            </tr>
          </table>
          <script>document.write(despliegaSaldosCuenta());</script></td>
        <td id="tdbordeder">&nbsp;</td>
      </tr>
      <tr>
        <td><img src="images/curvaabajoizquierda.gif" width="6" height="6" /></td>
        <td id="bordebajotabla"><img src="images/bordeblancoabajo.gif" width="6" height="6" /></td>
        <td><img src="images/curvaabajoderecha.gif" width="6" height="6" /></td>
      </tr>
    </table>
	<table border="0" cellspacing="0" cellpadding="0" id="botonera">
    <tr>
          <td><img src="images/btn_continuar.gif" width="90" height="21" border="0" title="Continuar" alt="Continuar"  onClick="enviaFormulario();" /></td>
        </tr>
  </table>    </td>
  </tr>
</table>
</html:form>
</body>
</html:html>
<%--
************************************************************************************************************
*
* Archivo:          inicioRecarga.jsp
*
* Descripci�n:      Pagina Inicial de la recarga de celular. 
*
* Path:             /bcinetwls/recargacelulares
* " Todos los derechos reservados por Banco de Cr�dito e Inversiones."
*
* Historia de cambios
*  version    fecha      autor                                   cambios
*  =======   ========== =======                                  =============================================
*  1.0       ??/??/???? Desconocido (BCI)                        Version Inicial.
*  1.1		 20/04/2009 Jessica Carroza E. (SEnTRA)			     Se agrega mensaje que indica que el saldo 
*                                                                de la l�nea de emergencia esta inclu�do en el
*                                                                saldo de la l�nea de sobregiro.
*  1.2       03/07/2009 Rodrigo Gonz�lez M. (Imagemaker IT)      Se reemplaza la frase "seleccione compa��a"
*                                                                por "seleccione recarga" en la zona de
*                                                                celular frecuente.
*  1.3       05/01/2010 Pablo Carre�o V. (TINet)                 Se agrega campo que muestra el saldo de la l�nea
*                                                                de emergencia, ademas se modifica el campo de
*                                                                saldo disponible en l�nea de sobregiro quedando
*                                                                separada de la l�nea de emergencia.La documentaci�n
*                                                                se cambia a la parte inferior del fuente.
*  1.4       26/07/2011 Rodrigo Gonz�lez M. (Imagemaker IT)      Se eliminan los input 'celular11', 'celular12',
*                                                                'celular21' y 'celular22', y todas las referencias
*                                                                a ellos en las funciones javascript de la presente
*                                                                JSP. Estas referencias e imputs se reemplazan por
*                                                                'numeroCelular' y 'reNumeroCelular' para tener el
*                                                                n�mero celular del cliente en un s�lo campo y no
*                                                                en 2 (c�digo celular y resto del n�mero).                                                             
*  1.5       03/02/2012 Diego Urrutia (Imagemaker IT)            Se agrega div para informar que sobre la posibilidad
*                                                                de modificar la empresa o el alias de una recarga frecuente.                                                              
*  1.6       16/04/2012 Rodrigo Navarro (Imagemaker IT)          Se agrega javascript para flujo desde recargas frecuentes.
*
*  1.7       27/04/2012 Eduardo Mascayano (TINet)                Se modifica costo a: 0,18UF (IVA inc.).
*  1.8	    28/05/2012	 Fernando Mateluna B. (SEnTRA)				 Se modifica link solicitud multipass seg�n nuevo menu cenefa de BCI personas.
*  1.9	    25/10/2012	  Marco Aicon				Se elimina Link volver y se agrega logica de canal a link solicitud de multipass.
*  2.0       26/07/2012  Diego Urrutia Guerra (Imagemaker IT)    Se corrige funcion pintaFila para que al agregar las nuevas operadoras
*                                                                no se generen errores en la pagina.                                                           
*  2.1      05/06/2013   Ver�nica Cestari (ORAND)                Se agrega mensaje de inicio e inicio2 para multipass movil 
*  2.2      18/06/2015	 Felipe Carvajal (Imagemaker IT)		 Se agrega mensaje el cual se pintara seg�n el canal al 
*																 que pertenezca el mensaje a pintar, para el multipass.    
*  2.3  	11/08/2015   Felipe Carvajal (Imagemaker IT)         Se agrega mensajeSegundaClave para indicar el mensaje al cliente
*																 para uso del multipass, adicionalmente sirve para clientes con
*																 safesigner.  
*
*  2.4      08/01/2016   Felipe Carvajal (Imagemaker IT) V�ctor Ortiz (Ing BCI) Se modifica el largo del n�mero celular a 9                                                       
*  2.5      02/02/2017   Luis Lopez Alamos (SEnTRA) - Christian Moraga Roldan (Ing. Soft. BCI) Se parametrizan valores en duro, se agrega logica para cambio visual 
*                                                                                              en caso de modificar el tipo de operadora de recarga.                                                     
*  2.6      30/08/2017  Marcelo Fuentes (SEnTRA) - Ricardo Carrasco (Ing. Soft. BCI) Se elimina mensaje que indicaba que era necesario tener multipass.
**************************************************************************************************************
--%>

