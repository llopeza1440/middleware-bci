package wcorp.model.seguridad.to;

import java.io.Serializable;

/**
 * 
 * <b>EstadoActivacionTokenTO</b>
 * <p>
 * Representa estado del usuario si existe o no.
 * </p>
 * 
 * <p>
 * Registro de versiones:
 * </p>
 * 
 * <ul>
 * <li>1.0 16/06/2017 Sergio Flores (SEnTRA) - Ricardo Carrasco (Ing. Soft.BCI): Version inicial.</li>
 * </ul>
 * 
 * <p>
 * <b>Todos los derechos reservados por Banco de Cr�dito e Inversiones.</b>
 * </p>
 * 
 */
public class EstadoActivacionTokenTO implements Serializable {

	/**
	 * N�mero de versi�n utilizado durante la serializaci�n.
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * Campo codigo de activacion.
	 */
	private String activationCode;
	/**
	 * Campo numero de serial.
	 */
	private String serialNumber;

	/**
	 * estado del resultado.
	 */
	private ResponseStatusTO resultado;
    
	
	public String getActivationCode() {
		return activationCode;
	}

	public void setActivationCode(String activationCode) {
		this.activationCode = activationCode;
	}

	public String getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

	public ResponseStatusTO getResultado() {
		return resultado;
	}

	public void setResultado(ResponseStatusTO resultado) {
		this.resultado = resultado;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String toString() {
		StringBuffer buffer = new StringBuffer();
		buffer.append("EstadoActivacionTokenTO [activationCode=");
		buffer.append(activationCode);
		buffer.append(", serialNumber=");
		buffer.append(serialNumber);
		buffer.append(", resultado=");
		buffer.append(resultado);
		buffer.append("]");
		return buffer.toString();
	}
}
