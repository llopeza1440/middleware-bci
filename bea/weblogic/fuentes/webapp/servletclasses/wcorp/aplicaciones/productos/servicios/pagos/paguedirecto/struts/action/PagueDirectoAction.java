package wcorp.aplicaciones.productos.servicios.pagos.paguedirecto.struts.action;

import java.io.IOException;
import java.math.BigInteger;
import java.rmi.RemoteException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import java.util.Vector;

import javax.naming.InitialContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

import wcorp.aplicaciones.infraestructuradenegocios.transaccion.to.TransaccionPendienteDeFirmaTO;
import wcorp.aplicaciones.operacion.legal.firmasypoderes.delegate.ServiciosFirmasyPoderesDelegateImpl;
import wcorp.aplicaciones.productos.servicios.pagos.paguedirecto.delegate.PagueDirectoBusinessDelegate;
import wcorp.aplicaciones.productos.servicios.pagos.paguedirecto.delegate.PagueDirectoBusinessDelegateImpl;
import wcorp.aplicaciones.productos.servicios.pagos.paguedirecto.exception.PagueDirectoException;
import wcorp.aplicaciones.productos.servicios.pagos.paguedirecto.parametros.PagueDirectoParametros;
import wcorp.aplicaciones.productos.servicios.pagos.paguedirecto.struts.form.PagueDirectoForm;
import wcorp.aplicaciones.productos.servicios.pagos.paguedirecto.struts.helper.JournalizadorHelper;
import wcorp.aplicaciones.productos.servicios.pagos.paguedirecto.struts.helper.PagueDirectoTOHelper;
import wcorp.aplicaciones.productos.servicios.pagos.paguedirecto.struts.helper.TransaccionPendienteDeFirmaTOHelper;
import wcorp.aplicaciones.productos.servicios.pagos.paguedirecto.struts.helper.ValidaSesion;
import wcorp.aplicaciones.productos.servicios.pagos.paguedirecto.to.AliasBotonPagoTO;
import wcorp.aplicaciones.productos.servicios.pagos.paguedirecto.to.ApoderadoTO;
import wcorp.aplicaciones.productos.servicios.pagos.paguedirecto.to.CuentaTO;
import wcorp.aplicaciones.productos.servicios.pagos.paguedirecto.to.DatosConvenioTO;
import wcorp.aplicaciones.productos.servicios.pagos.paguedirecto.to.DatosPagadorTO;
import wcorp.aplicaciones.productos.servicios.pagos.paguedirecto.to.PagueDirectoTO;
import wcorp.aplicaciones.productos.servicios.pagos.paguedirecto.to.ProductoTO;
import wcorp.aplicaciones.productos.servicios.pagos.paguedirecto.to.RespuestaCompraTO;
import wcorp.aplicaciones.productos.servicios.pagos.paguedirecto.to.TransaccionTO;
import wcorp.aplicaciones.productos.servicios.pagos.paguedirecto.util.numbers.BigDecimalUtil;
import wcorp.infraestructura.seguridad.autenticacion.util.SafeSignerUtil;
import wcorp.model.actores.Cliente;
import wcorp.model.seguridad.CamposDeLlaveTO;
import wcorp.model.seguridad.Canal;
import wcorp.model.seguridad.ControllerBCI;
import wcorp.model.seguridad.EstadoSegundaClaveTO;
import wcorp.model.seguridad.LlaveSegundaClaveTO;
import wcorp.model.seguridad.ParametrosEstrategiaSegundaClaveTO;
import wcorp.model.seguridad.RepresentacionNumericaSegundaClaveTO;
import wcorp.model.seguridad.RepresentacionSegundaClave;
import wcorp.model.seguridad.ResultadoOperacionSegundaClaveTO;
import wcorp.model.seguridad.SessionBCI;
import wcorp.serv.pagos.PagosException;
import wcorp.serv.pagos.ServiciosPagos;
import wcorp.serv.pagos.ServiciosPagosDelegate;
import wcorp.serv.pagos.ServiciosPagosHome;
import wcorp.serv.seguridad.NoSessionException;
import wcorp.serv.seguridad.SeguridadException;
import wcorp.util.ErroresUtil;
import wcorp.util.GeneralException;
import wcorp.util.StringUtil;
import wcorp.util.TablaValores;
import wcorp.util.com.JNDIConfig;
import wcorp.util.com.TuxedoException;

/**
 * DispatchAction que se encarga del manejo de los flujos del Pago con Moneda Extranjera.
 * 
 * <p>
 * Registro de Versiones:
 * <ul>
 * <li>1.0 08/11/2007, Robson Watt (Neoris Chile): versi�n inicial
 * <li>1.1 23/05/2008, Ra�l Eduardo Plata (Neoris Chile): Unifica logs a [PagueDirectoAction].
 *                     Modificaci�n de validaApoderado, controla error de tipo SeguridadException, validando si 
 *                     es de tipo TOKEN y redirigirlo a este. Esto ocurre para apoderados que tienen asociado 
 *                     multipass.
 *                     
 * <li>1.2 30/04/2012, Pedro Carmona Escobar (SEnTRA): Se realizan cambios en el m�todo
 *                      {@link #validaToken(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)}.
 *                      Adem�s, se reemplaza la clase Logger por la clase wcorp.org.apache.Logger. Por �ltimo, se
 *                      realizan modificaciones en toda la clase para cumplir la normativa Bci sobre revisi�n
 *                      de c�digo fuente.
 *                      
 * <li>1.3 25/06/2013, Pedro Carmona Escobar (SEnTRA): Se realizan cambios en los m�todos
 *                      {@link #validaUsuario(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)}
 *                      , {@link {@link #validaApoderado(ActionMapping, ActionForm, HttpServletRequest
 *                      , HttpServletResponse)}}, {@link #validaToken(ActionMapping, ActionForm
 *                      , HttpServletRequest, HttpServletResponse)}. Adem�s, se incorpora la constante
 *                      'RUT_DIRECTEMAR'.
 *
 * <li>1.4 07/04/2014, Pablo Salazar (Imagemaker IT): Se realizan cambios en los m�todos
 *                      {@link #validaUsuario(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)}
 *                      , {@link #confirmaPago(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)}
 *                      , {@link #validaToken(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)}. 
 *                       Adem�s se a�aden 3 constantes para la journalizaci�n del metodo validaToken
 *                       'EVE_LOGIN', 'SUB_COD_OK', 'ID_PRODTOKEN'
 *
 * <li>1.5 27/11/2014, Mauricio Hernandez(Imagemaker IT): Se realizan cambio en el metodo validaUsuario.
 *                     {@link #validaUsuario(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)}
 *                     Se validan los pasos posteriores al multipass (cuando el usuario es persona), para que se 
 *                     llame al servicio que ejecuta el insert en la tabla COMPRA_MONEX de forma correcta.
 *                     Se agrega variable boolean noRequiereSeguridad, para control de flujo.
 *
 * <li>1.6 04/03/2015, Ricardo Hidalgo (SEnTRA) - Kay Vera (Ing. Soft. BCI): Por proyecto de migraci�n de pagos
 *                     previred en m�todo <br> 
 *                     {@link #confirmaPago(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)},
 *                     <br> se agrega l�gica para insertar pagos en el repositorio de transacciones pendientes 
 *                     de firma.
 *                     Se normaliza log de la clase {@link #getLogger()}.
 *
 * <li>1.7 10/04/2015 Rodolfo kafack Ghinelli (SEnTRA), Gonzalo Villagr�n (SEnTRA) - Rodrigo Briones O. (Arq. de Soluciones): Se modifican los m�todos 
 *                     {@link #validaUsuario(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)} y 
 *                     {@link #validaApoderado(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)}
 *                     para incorporar la validaci�n de SafeSigner. Se crea el m�todo nuevo 
 *                     {@link #validaSafeSigner(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)}.
 *                     Se modifica metodo {@link #confirmaPago(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)}
 *                     para quitar la referencia a metodo split.</li>
 * <li>1.7 14/10/2015, Oscar Nahuelpan (SEnTRA) - Marcelo Avenda�o (ing. soft. BCI): Se modifican metodos 
 * {@link #validaApoderado(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)}, 
 * {@link #validaToken(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)}</li>
 * <li>1.8 11/10/2016  Adolfo Cid P. (IMIT) - Marcelo Avendano (BCI) - Se elimina todo lo de PASSCODE y se agrega
 *      del metodo validacion a la SeguridadException del m�todo 
 *     {@link #confirmaPago(ActionMapping mapping, ActionForm actionForm, HttpServletRequest request,
 *           HttpServletResponse response)}.
 * </li>
 * <li>1.9 30/08/2017 Sergio Bustos B. (Imagemaker) - Miguel Anza (Ing. Soft. BCI): Se realizan modificaciones en metodos 
 * 					{@link #inicio(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)},
 * 					{@link #validaUsuario(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)},
 * 					{@link #validaApoderado(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)},
 * 					{@link #validaToken(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)},
 * 					{@link #validaSafeSigner(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)}
 *  				y se agregan contantes estatica {@link #CODIGO_EXISTE}, {@link #CANTIDAD_20}</li>
 * <li>1.10 25/09/2017  Marcelo Fuentes (SEnTRA) - Ricardo Carrasco (Ing. Soft. BCI): Se modifica m�todo
 * {@link #validaUsuario(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)},
 * {@link #validaApoderado(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)},
 * {@link #validaToken(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)}.
 * Se agregan los siguientes metodos:
 * {@link #validaEntrustSoftTokenOnline(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)},
 * {@link #validaEntrustSoftTokenOffline(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)},
 * {@link #validaEntrustSoftToken(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse, String, String)}
 * {@link #generarLlaveQR(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse, String, String)}.
 * Ademas se agregan las constantes {@link #SUB_COD_NOK}, {@link #EST_EVENTO_NEGOCIO_REC}, {@link #ID_PRODENTRUSTTOKEN},
 * {@link #ID_PRODENTRUSTSOFTTOKEN}, {@link #TABLA_PARAMETROS_ENTRUST}, {@link #ENTRUST_TOKEN}, {@link #TABLA_SEGURIDAD},
 * {@link #ENTRUST_SOFTTOKEN}, {@link #CANAL_PYME}, {@link #ONLINE} y {@link #OFFLINE}.</li>
 * 
 * </ul>
 * </p>
 * 
 * <p>
 * <b>Todos los derechos reservados por Banco de Cr�dito e Inversiones.</b>
 * </p>
 */
public class PagueDirectoAction extends DispatchAction {

    /** Para Journalizacion. */
    /** Eventos. */
    private static final String EVE_LOGIN                 = "LOGIN"; 
    
    /** Sub codigo evento. */
    private static final String SUB_COD_OK                = "OK";
    
    /**
     * Sub codigo evento No Exitoso.
     */
    private static final String SUB_COD_NOK               = "NOK";

    /** Producto. */
    private static final String ID_PRODTOKEN              = "TOK";

    /**
     * Estado evento de negocio Rechazado.
     */
    private static final String EST_EVENTO_NEGOCIO_REC    = "R";
    
    /**
     * Estado evento de negocio Procesado.
     */
    private static final String EST_EVENTO_NEGOCIO_PRO    = "P";
    
    /**
     * Identificador del producto Entrust Token.
     */
    private static final String ID_PRODENTRUSTTOKEN       = "HTK";
    
    /**
     * Identificador del producto Entrust SoftToken.
     */
    private static final String ID_PRODENTRUSTSOFTTOKEN   = "STK";

    /** Nombre del Objeto que est� en session. */
    private static final String OBJETO_PAGUE_DIRECTO = "pagueDirecto";

    /** Nombre del servicio. */
    private static final String SERVICIO = "PagueDirecto";

    /** Cantidad de letras posibles para la claveDos. */
    private static final int CANTIDAD_LETRAS = 10;
    /**
     * Session Bci.
     */
    private static final String IDNAME = "sessionBci";
    /**
     * Tabla de parametros de Recargas.
     */
    private static final String PARAMSAFESIGNER = "safesigner.parametros";    

    /** Rut Directemar. */
    private static final String RUT_DIRECTEMAR = TablaValores.getValor("paguedirectoMonex.parametros"
            , "DIRECTEMAR", "rut");
    
	/**
	 * Tabla con los valores globales de operaci�n.  Para el caso particular de
	 * esta clase lo que interesa rescatar de esta tabla es la configuraci�n
	 * para los pagos multimoneda.
	 */
    private static final String REPO_TRANSACIONES = "repositorioTransaccionesaFirmar.parametros";
    
    /**
    * Url sitio Tbanc.
    */
    private static final String URL_TBANC = "www.tbanc.cl";
	
    /**
    * Url sitio Bci Personas.
    */
    private static final String URL_BCI_PERSONA = "www.bci.cl/personas";
	
    /**
    * Url sitio Bci Empresas.
    */
    private static final String URL_BCI_EMPRESA = "www.bci.cl/empresas";
	
    /**
     * Nombre de tabla de errores.
     */
    private static final String TABLA_ERRORES = "errores.codigos";
    /**
     * Contiene el codigo de Safe Signer.
     */
    private static final String SAFE_SIGNER = "MPM";
    
    /**
     * Codigo de error si existe registro de ID.
     */
    private static final String CODIGO_EXISTE = "MNX-30004";
    
    /**
     * Identificador cantidad 20.
     */
    private static final int CANTIDAD_20 = 20;
    
    /**
     * Canal Pyme.
     */
    private static final String CANAL_PYME = "132";
    
    /**
     * tabla de par�metros de entrust.
     */
    private static final String TABLA_SEGURIDAD = "Seguridad.parametros";
    
    /**
     * tabla de par�metros de entrust.
     */
    private static final String TABLA_PARAMETROS_ENTRUST = "entrust.parametros";
    
    /**
     * Tipo de Segunda Clave Softtoken Entrust.
     */
    private static final String ENTRUST_TOKEN = TablaValores.getValor(TABLA_PARAMETROS_ENTRUST, "nombreToken", "desc");
    
    /**
     * Tipo de Segunda Clave Softtoken Entrust.
     */
    private static final String ENTRUST_SOFTTOKEN = TablaValores.getValor(TABLA_PARAMETROS_ENTRUST, "nombreSoftToken", "desc");
    
    /**
     * tipo de segunda clave para SoftToken Entrust.
     */
    private static final String ONLINE = TablaValores.getValor(TABLA_PARAMETROS_ENTRUST, "modoOnline", "desc");
    
    /**
     * tipo de segunda clave para SoftToken Entrust.
     */
    private static final String OFFLINE = TablaValores.getValor(TABLA_PARAMETROS_ENTRUST, "modoOffline", "desc");
    
    /** LOGGER de la clase. */
    private transient Logger logger = (Logger) Logger.getLogger(PagueDirectoAction.class);

    /**
     * M�todo que inicia el proceso de pago. Recibe los datos de la transacci�n, arma el objeto correspondiente
     * usando la clase helper PagueDirectoTOHelper, de acuerdo al tipo de entrada al m�todo, las que
     * pueden ser mediante Servicio de Impuestos Internos (SII), Tesorer�a General de la Rep�blica (TGR) o por
     * medio de otra empresa. Adem�s de obtener los antecedentes de convenio asociados a la empresa pagadora.
     * 
     * <p>
     * Registro de Versiones:
     * <ul>
     * <li>1.0 08/11/2007, Robson Watt (Neoris Chile): versi�n inicial
     * <li>1.1 23/05/2008, Ra�l Eduardo Plata (Neoris Chile): Unifica log a [PagueDirectoAction]
     * <li>1.2 28/05/2013, Rodrigo Bravo C. (SEnTRA): Se crea variable numeroTrx para loguear el numero de 
     *                     transacci�n.
     * <li>1.3 30/08/2017 Sergio Bustos B.(Imagemaker) - Miguel Anza (Ing. Soft. BCI): Se cambia el registro del evento, para dejar registro
     *                    luego del inicio de sesion. En cambio se deja validacion para comprobar si existe registro de ID en caso de vulnerabilidad.</li>
     * </ul>
     * </p>
     * 
     * @param mapping Representa el mapeo de una instancia del formulario.
     * @param form ActionForm asociado al ActionMapping.
     * @param request La solicitud HTTP que se esta procesando.
     * @param response La respuesta entregada.
     * @return Env�a los datos a la p�gina de respuesta.
     * @throws GeneralException 
     * @throws RemoteException 
     * @throws PagosException 
     * @throws TuxedoException 
     * @since 1.0
     */
    public ActionForward inicio(ActionMapping mapping, ActionForm form, HttpServletRequest request,
            HttpServletResponse response) throws TuxedoException, PagosException, RemoteException, GeneralException {
        String numeroTrx = "";
            if (getLogger().isDebugEnabled()){
            	getLogger().debug("[PagueDirectoAction][inicio]");
            }
            ServiciosPagos pagosbean    = null;
            try {
                InitialContext ic = JNDIConfig.getInitialContext();
                ServiciosPagosHome pagoshome    = (ServiciosPagosHome)ic.lookup("wcorp.serv.pagos.ServiciosPagos");
                pagosbean    = (ServiciosPagos)pagoshome.create();
			} 
            catch (Exception e) {
				if (getLogger().isEnabledFor(Level.ERROR)){
					getLogger().error("[" + numeroTrx + "][inicio] Error:" + ErroresUtil.extraeStackTrace(e));
	            }
            }

        try {
            form = new PagueDirectoForm();
            PagueDirectoBusinessDelegate delegate = new PagueDirectoBusinessDelegateImpl();
            HttpSession hs = request.getSession(true);
            PagueDirectoTO pagueDirecto = null;
            pagueDirecto = PagueDirectoTOHelper.extraerDatosPagueDirectoTO(request);
            String canalTemp = PagueDirectoParametros.CANAL_ID + obtenerCanal(request.getServletPath());
            pagueDirecto.setCanal(canalTemp);
            numeroTrx = StringUtil.completaPorLaIzquierda(pagueDirecto.getNumeroTransaccion(), CANTIDAD_20, '0');
            
            PagueDirectoTOHelper pagueTOHelper = new PagueDirectoTOHelper();
            TransaccionTO transaccion = pagueTOHelper.getTransaccionTO(pagueDirecto);
           
            pagueDirecto.setDatosConvenio(delegate.datosConvenioPagueDirecto(transaccion));
            transaccion.setCodigoConvenio(pagueDirecto.getDatosConvenio().getCodigoConvenio());
            
            if (getLogger().isDebugEnabled()){
            	getLogger().debug("[inicio] [existeSerialPagueDirecto] numeroTrx: " +numeroTrx+ " CodigoConvenio: " + pagueDirecto.getDatosConvenio().getCodigoConvenio());
            }
            if (pagosbean.existeSerialPagueDirecto(numeroTrx, pagueDirecto.getDatosConvenio().getCodigoConvenio())) {
                if (getLogger().isDebugEnabled()){
                	getLogger().debug("[PagueDirectoAction][inicio]: Serial Repetido"
                			+ pagueDirecto.getDatosConvenio().getCodigoConvenio() + "-" + numeroTrx);
                }
                throw new PagueDirectoException(CODIGO_EXISTE);
            }
            
            if (getLogger().isDebugEnabled()){
                getLogger().debug("[" + numeroTrx + "][inicio] [transaccion ]:"+ transaccion);
            }
            hs.setAttribute("pagueDirecto", pagueDirecto);
            return mapping.findForward("login");
        }
        catch (PagueDirectoException ex) {
            if (getLogger().isEnabledFor(Level.ERROR)){
                getLogger().error("[" + numeroTrx + "][inicio] Error:" + ErroresUtil.extraeStackTrace(ex));
            }
            guardarError("error.pagueDirecto", new String[] { ex.getSimpleMessage() }, request);
            return mapping.findForward("error");
        }
    }

    /**
     * M�todo que valida el rut logeado en la p�gina de login para identificar si se trata de una empresa o de una
     * persona natural mediante la llamada al m�todo del delegate llamado datosPagador(), el que retorna adem�s
     * informaci�n de cuentas y montos v�lidos. Adem�s obtiene las cuentas del cliente de tipo CCT o CPR, con las
     * cuales se le dar� opci�n de pago.
     * 
     * <p>
     * Registro de Versiones:
     * <ul>
     * <li>1.0 08/11/2007, Robson Watt (Neoris Chile): versi�n inicial
     * <li>1.1 23/05/2008, Ra�l Eduardo Plata (Neoris Chile): Unifica log a [PagueDirectoAction], Agrega log al 
     *                     crear sesi�n
     * <li>1.2 28/05/2013, Rodrigo Bravo C. (SEnTRA): Se crea variable numeroTrx para loguear el numero de 
     *                     transacci�n.
     * <li>1.3 25/06/2013, Pedro Carmona Escobar (SEnTRA): Se incorpora l�gica de agrupaci�n de productos en caso
     *                          de que el pago provenga de Directemar.
     * <li>1.4 07/04/2014, Pablo Salazar (Imagemaker IT): -Se agrego metodo que resetea PagueDirectoForm
     *                                    -Se agrega un seteo de pagina de retorno
     *                                    -Se corrige un seteo de compraSimilar
     * <li>1.5 27/11/2014, Mauricio Hernandez(Imagemaker IT): 
     *                     Se validan los pasos posteriores al multipass (cuando el usuario es persona), para que 
     *                     se llame al servicio que ejecuta el insert en la tabla COMPRA_MONEX de forma correcta.
     *                     Se agrega variable boolean noRequiereSeguridad, para control de flujo.
     * </li>
     * <li>1.6 10/04/2015 Rodolfo kafack Ghinelli (SEnTRA), Gonzalo Villagr�n (SEnTRA) - Rodrigo Briones O. (Arq. de Soluciones): 
     *                     Se agrega validacion de SafeSigner. Se corrigen logs del m�todo.
     * <li>1.7 30/08/2017 Sergio Bustos B. (Imagemaker) - Miguel Anza (Ing. Soft. BCI): Se agrega registro de evento al iniciar sesion.</li>
     * <li>1.8 25/09/2017  Marcelo Fuentes (SEnTRA) - Ricardo Carrasco (Ing. Soft. BCI): Se agrega validaci�n que 
     * revisa si se requiere autenticaci�n Entrust Token o Entrust SoftToken.
     * </li>
     * </ul>
     * </p>
     * 
     * @param mapping Representa el mapeo de una instancia del formulario.
     * @param form ActionForm asociado al ActionMapping.
     * @param request La solicitud HTTP que se esta procesando.
     * @param response La respuesta entregada.
     * @return Env�a los datos a la p�gina de respuesta.
     * @throws Exception exception gen�rica arrojada por el m�todo y capturada por p�gina de error global.
     * @since 1.0
     */
    public ActionForward validaUsuario(ActionMapping mapping, ActionForm form, HttpServletRequest request,
            HttpServletResponse response) throws Exception {
        String numeroTrx = "";
        try {
            if (getLogger().isEnabledFor(Level.INFO)){
                getLogger().info("[validaUsuario][BCI_INI] iniciado.");
            }
            PagueDirectoBusinessDelegate delegate = new PagueDirectoBusinessDelegateImpl();
            
            ValidaSesion valida = new ValidaSesion(request);
            HttpSession hs = request.getSession(false);
            PagueDirectoTO pagueDirecto = (PagueDirectoTO) hs.getAttribute(OBJETO_PAGUE_DIRECTO);
            DatosConvenioTO convenio = pagueDirecto.getDatosConvenio();
            numeroTrx = pagueDirecto.getNumeroTransaccion();
            if (getLogger().isDebugEnabled()){
                getLogger().debug("[validaUsuario][" + numeroTrx + "] [convenio]:" + convenio);
            }
            PagueDirectoTOHelper pagueTOHelper = new PagueDirectoTOHelper();
            TransaccionTO transaccion = pagueTOHelper.getTransaccionTO(pagueDirecto);
            if (getLogger().isDebugEnabled()){
                getLogger().debug("[validaUsuario][" + numeroTrx + "] [Obteniendo TransaccionTO ]:"+ transaccion );
            }
            String canalTemp = PagueDirectoParametros.CANAL_ID + obtenerCanal(request.getServletPath());
            Canal canal = new Canal(canalTemp);

            PagueDirectoForm pagueDirectoForm = (PagueDirectoForm) form;
            pagueDirectoForm.resetValidaUsuario();
            
            if (getLogger().isDebugEnabled()){
                getLogger().debug("[validaUsuario][" + numeroTrx + "] Se resetea Formulario");
            }

            pagueDirectoForm.setSubTotal(BigDecimalUtil.substract(pagueDirecto.getMontoMonedaDestino()
                    , pagueDirecto.getCostoEnvio()));
            pagueDirectoForm.setCostoEnvio(pagueDirecto.getCostoEnvio());
            pagueDirectoForm.setTotal(pagueDirecto.getTotal());
            pagueDirectoForm.setEmpresa(convenio.getEmpresa());
            pagueDirectoForm.setTipoMoneda(convenio.getCodigoMoneda());
            pagueDirectoForm.setImagen(convenio.getImagen());
            pagueDirectoForm.setPaginaRetorno(pagueDirecto.getPaginaRetorno());     

            if (getLogger().isDebugEnabled()){
                getLogger().debug("[validaUsuario][" + numeroTrx + "] [pagueDirecto ]:"+ pagueDirecto  );
            }
            
            String userIdrut = pagueDirectoForm.getRutUsuario();
            String userIdAux = pagueDirectoForm.getDigitoUsuario();
            String pin = pagueDirectoForm.getClaveUsuario();
            
            if (getLogger().isDebugEnabled()){
                getLogger().debug("[validaUsuario][" + numeroTrx + "] [userIdrut=" + userIdrut + "][userIdAux="
                    + userIdAux + "]");
            }

            pagueDirecto.setDatosPagador(delegate.datosPagador(new Long(userIdrut), transaccion));

            delegate.validarMonto(convenio.getTipoConvenio(),convenio.getMontoMaximoAutorizado(),
                    transaccion.getMontoTransaccion());
            
            pagueDirectoForm.setCompraSimilar(pagueDirecto.getDatosPagador().isCompraExistente());
            
            boolean usuarioBel = delegate.isUsuarioConConvenioBel(convenio.getTipoConvenio(),
                    pagueDirecto.getDatosPagador());
            if (getLogger().isDebugEnabled()){
                getLogger().debug("[validaUsuario][" + numeroTrx + "] [ usuarioBel ]:" + usuarioBel  );
            }
            pagueDirecto.getDatosPagador().setDebeActuarComoEmpresa(usuarioBel);

            try{
                if (getLogger().isDebugEnabled()){
                    getLogger().debug("[validaUsuario][" + numeroTrx + "] rut convenio: " 
                            + pagueDirecto.getDatosConvenio().getRut());
                    getLogger().debug("[validaUsuario][" + numeroTrx + "] rut directemar: " 
                            + Long.parseLong(RUT_DIRECTEMAR));
                    getLogger().debug("[validaUsuario][" + numeroTrx + "] se hara agrupaci�n para directemar?: " 
                            + (pagueDirecto.getDatosConvenio().getRut().longValue() == new Long(RUT_DIRECTEMAR
                                    ).longValue()));
                }
                if (pagueDirecto.getDatosConvenio().getRut().longValue() == new Long(RUT_DIRECTEMAR).longValue()){
                    pagueDirectoForm.setProductos(pagueTOHelper
                            .agruparProductosDirectemar(pagueDirecto.getProducto()));
                }
                else{
            pagueDirectoForm.setProductos(pagueDirecto.getProducto());
                }
                if (getLogger().isDebugEnabled()){
                    getLogger().debug("[validaUsuario][" + numeroTrx + "] form: "
                            + StringUtil.contenidoDe(pagueDirectoForm.getProductos()));
                }
            }
            catch (Exception e){
                if (getLogger().isEnabledFor(Level.ERROR)){
                    getLogger().error("[validaUsuario][" + numeroTrx + "] exception: "
                    		+ e.getMessage(), e);
                }
            }
            pagueDirectoForm.setNombreUsuario(pagueDirecto.getDatosPagador().getNombre());
            hs.setAttribute(OBJETO_PAGUE_DIRECTO, pagueDirecto);
            if (getLogger().isDebugEnabled()){
                getLogger().debug("[validaUsuario][" + numeroTrx + "] [ pagueDirecto.getProducto ]:"
                        + pagueDirectoForm);
            }

            CuentaTO[] cuentas = delegate.listarCuentas(new Long(userIdrut), convenio.getCodigoMoneda());

            if (getLogger().isDebugEnabled()){
                getLogger().debug("[validaUsuario][" + numeroTrx + "] [ listarCuentas ]:"
                        + StringUtil.contenidoDe(cuentas));
            }
            pagueDirectoForm.setCuentas(cuentas);            

            if (usuarioBel) {
                if (getLogger().isDebugEnabled()){
                    getLogger().debug("[" + numeroTrx + "][validaUsuario] [Usuario actua como Empresa -> " 
                        + "login de apoderado]");
                }
                return mapping.findForward("loginApoderado");
            }
            else {
                if (getLogger().isDebugEnabled()){
                    getLogger().debug("[" + numeroTrx + "][validaUsuario] [Usuario ingresado es Persona]");
                }
                
                boolean noRequiereSeguridad = false;
                
                try {
                    valida.crearSession(canal.getCanalID(), userIdrut, userIdAux, pin);
                    if (getLogger().isDebugEnabled()){
                        getLogger().debug("[" + numeroTrx + "][validaUsuario] [sesion creada]");
                    }
                    noRequiereSeguridad = true;
                }
                catch (SeguridadException se) {
                    if (getLogger().isEnabledFor(Level.ERROR)){
                        getLogger().error("[validaUsuario][" + numeroTrx + "] Ex:" + se.getMessage(), se);
                    }
                	noRequiereSeguridad = false;
                	
                	if (se.getCodigo().equals("TOKEN")) {
                        return mapping.findForward("token");
                    }
                    else if(se.getCodigo().equals("SAFESIGNER")){

                        DecimalFormat df = new DecimalFormat();
                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMddHHmm");
                        
                        String fechaActual = simpleDateFormat.format(new Date());

                        String montoTotal = df.format(pagueDirectoForm.getTotal());

                        String  nombresDeLLave = TablaValores.getValor(PARAMSAFESIGNER,SERVICIO,"datosLlave");
                        String [] valoresDeLlave = {canal.getNombre(),fechaActual,montoTotal, 
                                StringUtils.trimToEmpty(convenio.getEmpresa())};
                        
                        String data;
                        CamposDeLlaveTO campoDeLlaves = new CamposDeLlaveTO();
                        SafeSignerUtil safeSignerUtil = new SafeSignerUtil();
                        campoDeLlaves = safeSignerUtil.seteaCamposDeLlave(nombresDeLLave, valoresDeLlave);

                        LlaveSegundaClaveTO llaveSegundaClave = 
                                ControllerBCI.generarLlavesDeSegundaClave(request,IDNAME , SERVICIO
                                        , campoDeLlaves);  
                        if (getLogger().isEnabledFor(Level.ERROR)){
                            getLogger().error("[validaUsuario][" + numeroTrx + "] llaveSegundaClave:"
                                    + llaveSegundaClave);
                        }
	                    if (!llaveSegundaClave.isEstatus()) {
                            if (getLogger().isEnabledFor(Level.ERROR)){
                                getLogger().error("[validaUsuario][" + numeroTrx + "] No se genero la llave previa"); 
                            }
                            saveToken(request);
	                        guardarError("error.seguridad", new String[] { se.getSimpleMessage() }, request);
	                        return mapping.findForward("error");
	                    }
                        else {
                            if (getLogger().isEnabledFor(Level.ERROR)){
                                getLogger().debug("[validaUsuario][" + numeroTrx + "] Se genero la llave previa");
                            }
                            SessionBCI sessionBci = (SessionBCI)hs.getAttribute(ValidaSesion.SESSION_BCI);
                            Cliente cliente = sessionBci.getCliente();
                            ParametrosEstrategiaSegundaClaveTO parametrosEstrategiaSegundaClaveTO = 
                                    new ParametrosEstrategiaSegundaClaveTO(
                                    		SERVICIO, cliente, canal, sessionBci);
                            if (getLogger().isEnabledFor(Level.ERROR)){
                                getLogger().error("[validaUsuario][" + numeroTrx + "] parametrosEstrategia:"
                                        + parametrosEstrategiaSegundaClaveTO);
                            }
                            data = llaveSegundaClave.getLlave().getClaveAlfanumerica();
                            request.setAttribute("parametrosEstrategiaSegundaClaveTO",
                                    parametrosEstrategiaSegundaClaveTO);
                            request.setAttribute("data", data);
                            request.setAttribute("servicioQR", SERVICIO); 
                            hs.setAttribute("camposDeLlave", campoDeLlaves);
							hs.setAttribute("imagenqr", data);
                
                            if (getLogger().isEnabledFor(Level.INFO)){
                                getLogger().info("[validaUsuario][" + numeroTrx + "][BCI_FINOK] retorna pagina safesigner.");
                            }
                            return mapping.findForward("safesigner");
                        }
                    }
                    else if (se.getCodigo().equals("ENTRUSTSOFTTOKEN")) {
                        
                        SessionBCI sessionBci = (SessionBCI)hs.getAttribute(ValidaSesion.SESSION_BCI);
                        EstadoSegundaClaveTO estadoSegundaClave = ControllerBCI.estadoSegundaClave(sessionBci.getParametrosEstrategiaSegundaClaveTO());
                        String requiereLlave = TablaValores.getValor(TABLA_SEGURIDAD, "SegundaClaveRequiereLlave", ENTRUST_SOFTTOKEN);
                        String servicio = sessionBci.getParametrosEstrategiaSegundaClaveTO().getServicio();
                        
                        if (logger.isEnabledFor(Level.DEBUG)) {
                            logger.debug("[asignaMetodosAutenticacion] " + estadoSegundaClave.getIdSegundaClave().getNombre() + "status:["
                                    + estadoSegundaClave.isEstatus() + "], " + estadoSegundaClave.toString() );
                        }
                        
                        if (estadoSegundaClave.isEstatus() && (requiereLlave != null && requiereLlave.equalsIgnoreCase("true"))) {
                            CamposDeLlaveTO campoDeLlaves = obtenerCamposDellaves(request, pagueDirectoForm);
                            if (logger.isEnabledFor(Level.DEBUG)) {
                                logger.debug("datos para Servicio " + servicio);
                                logger.debug("datos para campoDeLlaves " + campoDeLlaves.toString());
                            }
                            
                            LlaveSegundaClaveTO llave = ControllerBCI.generarLlave(estadoSegundaClave.getIdSegundaClave().getNombre(), 
                                                                                   servicio, 
                                                                                   sessionBci.getParametrosEstrategiaSegundaClaveTO().getUsuarioSegundaClave(), 
                                                                                   campoDeLlaves, 
                                                                                   ONLINE);
                            
                            if (logger.isEnabledFor(Level.DEBUG)) {
                                logger.debug("to String llave " + llave.toString());
                                logger.debug("Estado de la llave : " + llave.isEstatus() + ", " + llave.toString());
                            }
                            
                            sessionBci.setAttrib("transactionId", llave.getSerial());
                            sessionBci.setAttrib("camposDeLLavePD", campoDeLlaves);
                            request.setAttribute("frecuenciaEntrust", ControllerBCI.getFrecuenciaEntrust());
                            request.setAttribute("timeOutEntrust", ControllerBCI.getTimeOutEntrust());
                            return mapping.findForward("entrustSoftTokenOnline");
                        }else {
                            return mapping.findForward("error");
                        }
                    }
                    else if (se.getCodigo().equals("ENTRUSTTOKEN")) {
                        return mapping.findForward("entrustToken");
                    }
                    else {
                        guardarError("error.seguridad", new String[] { se.getSimpleMessage() }, request);
                        
                        if (getLogger().isEnabledFor(Level.INFO)){
                            getLogger().info("[validaUsuario][" + numeroTrx + "][BCI_FINOK] retorna a pagina de error.");
                        }
                        return mapping.findForward("error");
                    }
                }
                
                if(noRequiereSeguridad ){
                    
                    if (getLogger().isEnabledFor(Level.INFO)){
                        getLogger().info("[validaUsuario][" + numeroTrx + "][BCI_FINOK]");
                }
                TransaccionTO transaccionRegistra = new TransaccionTO();
                String numTrx = StringUtil.completaPorLaIzquierda(pagueDirecto.getNumeroTransaccion(), CANTIDAD_20, '0');
                transaccionRegistra.setNumeroTransaccion(numTrx);
                transaccionRegistra.setCodigoConvenio(convenio.getCodigoConvenio());
                if(getLogger().isDebugEnabled()){
                	getLogger().debug("[validaUsuario] [registrarTrxPagueDirecto] numTrx: " + numTrx + " CodigoConvenio: " + convenio.getCodigoConvenio());
                }
                delegate.registrarTrxPagueDirecto(transaccionRegistra);
                registrarCompraPagueDirecto(pagueDirecto);
                return mapping.findForward("pago");
                }
                else{
                    return mapping.findForward("error");
                }
            }
        }
        catch (PagueDirectoException pdex) {
            if (getLogger().isEnabledFor(Level.ERROR)){
                getLogger().error("[" + numeroTrx + "][validaUsuario] Error:" + ErroresUtil.extraeStackTrace(pdex));
            }
            guardarError("error.pagueDirecto", new String[] { pdex.getSimpleMessage() }, request);
            return mapping.findForward("error");
        }
        catch (Exception pdex) {
            if (getLogger().isEnabledFor(Level.ERROR)){
                getLogger().error("[" + numeroTrx + "][validaUsuario] Error:" + ErroresUtil.extraeStackTrace(pdex));
            }
            guardarError("error.pagueDirecto", new String[] { pdex.getMessage() }, request);
            return mapping.findForward("error");
        }
    }
    
    /**
     * M�todo que valida que el rut del apoderado ingresado sea efectivamente apoderado del cliente, obtiene las
     * cuentas donde se har� el cargo del pago, las que son del tipo CCT o CPR.
     * 
     * <p>
     * Registro de Versiones:
     * <ul>
     * <li>1.0 08/11/2007, Robson Watt (Neoris Chile): versi�n inicial
     * <li>1.1 23/05/2008, Ra�l Eduardo Plata (Neoris Chile): Unifica log a [PagueDirectoAction], establece el
     *                     nombre del usuario para desplegarlo luego de un error de tipo SeguridadException. 
     *                     Valida si hay un error de tipo TOKEN para solicitarlo. Esto ocurre para apoderados que 
     *                     tienen asociado multipass.
     * <li>1.2 28/05/2013, Rodrigo Bravo C. (SEnTRA): Se crea variable numeroTrx para loguear el numero de 
     *                     transacci�n.
     * <li>1.3 25/06/2013, Pedro Carmona Escobar (SEnTRA): Se incorpora l�gica de agrupaci�n de productos en caso
     *                          de que el pago provenga de Directemar.     
     * <li>1.3 10/04/2015 Rodolfo kafack Ghinelli (SEnTRA) - Rodrigo Briones O. (Arq. de Soluciones): Se agrega validacion de SafeSigner.
     *                     Se corrigen logs del m�todo.
     * </li>
     * <li>1.4 14/10/2015  Oscar Nahuelpan (SEnTRA) - Marcelo Avenda�o (ing. soft. BCI): Se agrega flujo 
     * de validaci�n de servicios.</li>
     * <li>1.5 30/08/2017 Sergio Bustos B. (Imagemaker) - Miguel Anza (Ing. Soft. BCI): Se agrega registro de evento al iniciar sesion.</li>
     * <li>1.6 25/09/2017  Marcelo Fuentes (SEnTRA) - Ricardo Carrasco (Ing. Soft. BCI): Se agrega validaci�n que   
     * revisa si se requiere autenticaci�n Entrust.</li>
     * </ul>
     * </p>
     * 
     * @param mapping Representa el mapeo de una instancia del formulario.
     * @param actionForm ActionForm asociado al ActionMapping.
     * @param request La solicitud HTTP que se esta procesando.
     * @param response La respuesta entregada.
     * @return Env�a los datos a la p�gina de respuesta.
     * @since 1.0
     */
    public ActionForward validaApoderado(ActionMapping mapping, ActionForm actionForm, HttpServletRequest request,
            HttpServletResponse response) {
        String numeroTrx = "";
        try {
            if (getLogger().isEnabledFor(Level.INFO)){
                getLogger().info("[validaApoderado][BCI_INI] iniciado.");
            }
            PagueDirectoBusinessDelegate delegate = new PagueDirectoBusinessDelegateImpl();
            
            ValidaSesion valida = new ValidaSesion(request);
            HttpSession hs = request.getSession(false); 
            PagueDirectoTO pagueDirecto = (PagueDirectoTO) hs.getAttribute(OBJETO_PAGUE_DIRECTO);
            Canal canal = new Canal(PagueDirectoParametros.CANAL_ID);
            DatosConvenioTO convenio = pagueDirecto.getDatosConvenio();
            numeroTrx = pagueDirecto.getNumeroTransaccion();
            if (getLogger().isEnabledFor(Level.INFO)) {
                getLogger().info("[" + numeroTrx + "][validaApoderado] [ DatosConvenioTO convenio ]:" + convenio.toString());
            }
            PagueDirectoForm form = (PagueDirectoForm) actionForm;
            if (getLogger().isEnabledFor(Level.INFO)) {
                getLogger().info("[" + numeroTrx + "][validaApoderado] [ DatosConvenioTO validar sesion ] canal:"
                    + canal.getCanalID() + ", rutApoderado:" + form.getRutApoderado() + "-"
                    + form.getDigitoVerificadorApoderado() + ", clave:"
                        +  StringUtil.censura(form.getClaveApoderado(), '*', 0, false, false));
            }
            ApoderadoTO apoderado = delegate.getApoderadoEmpresa(new Long(form.getRutApoderado()),
                    new Long(form.getRutUsuario()), convenio.getTipoConvenio());


            if (getLogger().isEnabledFor(Level.INFO)) {
                getLogger().info("[" + numeroTrx + "][validaApoderado] [ ApoderadoTO ]:" + apoderado.toString());
            }
            pagueDirecto.getDatosPagador().setApoderado(apoderado);
            try {
                valida.crearSession(canal.getCanalID(), form.getRutApoderado(), 
                        form.getDigitoVerificadorApoderado(), form.getClaveApoderado());
                if (getLogger().isEnabledFor(Level.INFO)) {
                    getLogger().info("[" + numeroTrx + "][validaApoderado] [sesion creada]");
                }
            }
            catch (SeguridadException se) {
                if (getLogger().isEnabledFor(Level.ERROR)){
                    getLogger().error("[" + numeroTrx + "][validaApoderado] Ex:" + ErroresUtil.extraeStackTrace(se));
                }
                SessionBCI sesionBCI = (SessionBCI)hs.getAttribute(ValidaSesion.SESSION_BCI);
                if (sesionBCI != null && sesionBCI.getCliente() != null) {
                    Cliente cliente = sesionBCI.getCliente();
                    String nombreApoderado = cliente.nombres + " " + cliente.apellidoMaterno
                            + " " + cliente.apellidoPaterno;
                    form.setNombreUsuario(nombreApoderado);
                }
                if (se.getCodigo().equals("TOKEN")) {
                    return mapping.findForward("token");
                }
                else if (se.getCodigo().equals("SAFESIGNER")) {
                    return mapping.findForward("safesigner");
                }
                else if (se.getCodigo().equals(ENTRUST_SOFTTOKEN)) {
                    return mapping.findForward("entrustSoftTokenOnline");
                }
                else if (se.getCodigo().equals(ENTRUST_TOKEN)) {
                    return mapping.findForward("entrustToken");
                }
                else {
                    if (getLogger().isEnabledFor(Level.ERROR)){
                        getLogger().error("[" + numeroTrx + "][validaApoderado] ex:" + ErroresUtil.extraeStackTrace(se));
                    }
                    guardarError("error.seguridad", new String[] { se.getSimpleMessage() }, request);
                    return mapping.findForward("error");
                }
            }

            form.setTipoConvenioBEL(apoderado.getTipoConvenioApoderado());

            if (apoderado != null && apoderado.getCodigoConvenioBel() != null) {
                if (getLogger().isEnabledFor(Level.INFO)) {
                    getLogger().info("[" + numeroTrx + "][validaApoderado] [ConvenioBel]:"
                        + apoderado.getCodigoConvenioBel());
                }
                form.setDobleFirma(true);
            }
            
            form.setNombreUsuario(pagueDirecto.getDatosPagador().getNombre());
            if (getLogger().isEnabledFor(Level.INFO)) {
                getLogger().info("[" + numeroTrx + "][ValidaApoderado] rut convenio: " 
                        + pagueDirecto.getDatosConvenio().getRut());
                getLogger().info("[" + numeroTrx + "][ValidaApoderado] rut directemar: " 
                        + Long.parseLong(RUT_DIRECTEMAR));
                getLogger().info("[" + numeroTrx + "][validaApoderado] se hara agrupaci�n para directemar?: " 
                        + (pagueDirecto.getDatosConvenio().getRut().longValue() == new Long(RUT_DIRECTEMAR)
                        .longValue()));
            }
            if (pagueDirecto.getDatosConvenio().getRut().longValue() == new Long(RUT_DIRECTEMAR).longValue()){
                PagueDirectoTOHelper pagueTOHelper = new PagueDirectoTOHelper();
                form.setProductos(pagueTOHelper
                        .agruparProductosDirectemar(pagueDirecto.getProducto()));
            }
            else{
            form.setProductos(pagueDirecto.getProducto());
            }
            if (getLogger().isEnabledFor(Level.INFO)) {
                getLogger().info("[" + numeroTrx + "][ValidaApoderado] form: "
                        + StringUtil.contenidoDe(form.getProductos()));
            }

            form.setCantidad(new BigInteger(pagueDirecto.getCantidadProductos().toString()));
            form.setFechaActual(new Date());
            form.setTransaccion(pagueDirecto.getNumeroTransaccion());
            
            if (getLogger().isEnabledFor(Level.INFO)) {
                getLogger().info("[" + numeroTrx + "][validaApoderado] [ Datos Pagador y Transacci�n]:" + form.toString());
                getLogger().info("[" + numeroTrx + "][validaApoderado] actuarComoEmpresa:" + pagueDirecto.getDatosPagador().isDebeActuarComoEmpresa());
            }
            
            String flagActivarValidacion = "";
            if (pagueDirecto.getDatosPagador().isDebeActuarComoEmpresa()) {
                flagActivarValidacion = TablaValores.getValor("paguedirectoMonex.parametros", "FlagFlujoSeguridadEmpresa", "estado");
            }
            else {
                flagActivarValidacion = TablaValores.getValor("paguedirectoMonex.parametros", "FlagFlujoSeguridadPersona", "estado");
            }
            if (getLogger().isEnabledFor(Level.INFO)) {
                getLogger().info("[validaApoderado] flag[" + flagActivarValidacion + "], numConvenio[" + pagueDirecto.getDatosConvenio().getCodigoConvenio()
                        + "], rutCliente[" + form.getRutUsuario() + "]");
            }
            
            if (flagActivarValidacion.equals("OK")) {
               String numConvenio = pagueDirecto.getDatosConvenio().getCodigoConvenio();
               String mensajeErrorInscripcion = TablaValores.getValor(TABLA_ERRORES, "MNX-30040", "Desc");
               String mensajeErrorActivacion = TablaValores.getValor(TABLA_ERRORES, "MNX-30041", "Desc");
               mensajeErrorInscripcion = StringUtil.reemplazaTodo(mensajeErrorInscripcion, "{link}", URL_BCI_EMPRESA);
               mensajeErrorActivacion = StringUtil.reemplazaTodo(mensajeErrorActivacion, "{link}", URL_BCI_EMPRESA);
               try {
                  ServiciosPagosDelegate pagosDelegate = ServiciosPagosDelegate.getInstance();
                  DatosConvenioTO datosConvenio = pagosDelegate.obtenerDatosConvenio(numConvenio);
                  if (getLogger().isEnabledFor(Level.INFO)) {
                      getLogger().info("[validaApoderado] datosConvenio[" + datosConvenio.toString() + "] rutUsuario[" + form.getRutUsuario() + "]");
                  }
                  AliasBotonPagoTO datos = new AliasBotonPagoTO();
                  datos.setRutCliente(Long.parseLong(form.getRutUsuario()));
                  DatosConvenioTO datosConv = new DatosConvenioTO();
                  datosConv.setCodigoConvenio(numConvenio);
                  datos.setDatosConvenio(datosConv);
                  if (datosConvenio.getMarcaRiesgo() == 1) {
                      AliasBotonPagoTO[] datosSuscripcion = pagosDelegate.obtenerDatosSuscripcion(datos);
                      if (datosSuscripcion == null || datosSuscripcion.length == 0) {
                          mensajeErrorInscripcion = StringUtil.reemplazaTodo(mensajeErrorInscripcion, "{urlPortal}", datosConvenio.getUrl());
                          guardarError("error.seguridad", new String[] {mensajeErrorInscripcion}, request);
                          return mapping.findForward("error");
                      }
                      if (getLogger().isEnabledFor(Level.INFO)) {
                          getLogger().info("[validaApoderado] aliasBtnPago[" + datosSuscripcion[0].toString() + "]");
                      }
                      if (!datosSuscripcion[0].getEstado().equals("ACT")) {
                          mensajeErrorActivacion = StringUtil.reemplazaTodo(mensajeErrorActivacion, "{urlPortal}", datosConvenio.getUrl());
                          guardarError("error.seguridad", new String[] {mensajeErrorActivacion}, request);
                          return mapping.findForward("error");
                      }
                      
                      if (getLogger().isEnabledFor(Level.INFO)) {
                          getLogger().info("[validaApoderado] Monto maximo servicio:[" + datosSuscripcion[0].getMontoMaximo().longValue() + "]");
                          getLogger().info("[validaApoderado] Total a pagar:[" + form.getTotal() + "]");
                          getLogger().info("[validaApoderado] Costo envio:[" + form.getCostoEnvio() + "]");
                      }
                      for (int cont = 0; cont < form.getProductos().length; cont++) {
                          ProductoTO producto = form.getProductos()[cont];
                          if (getLogger().isEnabledFor(Level.INFO)) {
                              getLogger().info("[validaApoderado] monto detalle a pagar:[" + (producto.getMonto().longValue() * producto.getCantidad().intValue()) + "]");
                          }
                          if (datosSuscripcion[0].getMontoMaximo().longValue() < form.getTotal().longValue()) {
                              guardarError("error.seguridad", new String[] {TablaValores.getValor(TABLA_ERRORES, "MNX-30042", "Desc")}, request);
                              return mapping.findForward("error");
                          }
                      }
                  }
              }
              catch (Exception e) {
                  if(getLogger().isEnabledFor(Level.ERROR)){
                      getLogger().error("[validaApoderado][BCI_FINEX][Exception] e:" + e.toString(), e);
                  }
                  guardarError("error.pagueDirecto", new String[] { e.getMessage() }, request);
                  return mapping.findForward("error");
              }
              
            }
            
            hs.setAttribute(OBJETO_PAGUE_DIRECTO, pagueDirecto);
            TransaccionTO transaccionRegistra = new TransaccionTO();
            String numTrx = StringUtil.completaPorLaIzquierda(pagueDirecto.getNumeroTransaccion(), CANTIDAD_20, '0');
            transaccionRegistra.setNumeroTransaccion(numTrx);
            transaccionRegistra.setCodigoConvenio(convenio.getCodigoConvenio());
            if(getLogger().isDebugEnabled()){
            	getLogger().debug("[validaApoderado] [registrarTrxPagueDirecto] numTrx: " + numTrx + " CodigoConvenio: " + convenio.getCodigoConvenio());
            }
            delegate.registrarTrxPagueDirecto(transaccionRegistra);
            registrarCompraPagueDirecto(pagueDirecto);
            return mapping.findForward("pago");
        }
        catch (PagueDirectoException pdex) {
            if (getLogger().isEnabledFor(Level.ERROR)){
                getLogger().error("[" + numeroTrx + "][validaApoderado] Error:" + ErroresUtil.extraeStackTrace(pdex));
            }
            guardarError("error.pagueDirecto", new String[] { pdex.getSimpleMessage() }, request);
            return mapping.findForward("error");
        }
    }

    /**
     * M�todo que permite obtener una instancia de Logger de la clase.
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 04/03/2015, Jaime Suazo.(SEnTRA): Versi�n inicial.</li>
     * </ul>
     * </p>
     * 
     * @return logger con la instancia del Log.
     * @since 1.6
     */    
    public Logger getLogger() {
        if (logger == null){
            logger = Logger.getLogger(this.getClass());
        }
        return logger;
    }

    /**
     * M�todo que confirma el pago por parte del cliente. En caso de tratarse de cliente de tipo empresa, este
     * m�todo graba la compra en la base de datos y mapea el flujo a la p�gina de comprobante. En caso de que el
     * cliente sea de tipo persona, este m�todo redirecciona el flujo de la aplicaci�n a la p�gina de ingreso de
     * passcode.
     * 
     * <p>
     * Registro de Versiones:
     * <ul>
     * <li>1.0 08/11/2007, Robson Watt (Neoris Chile): versi�n inicial
     * <li>1.1 23/05/2008, Ra�l Eduardo Plata (Neoris Chile): Unifica log a [PagueDirectoAction]
     * <li>1.2 28/05/2013, Rodrigo Bravo C. (SEnTRA): Se crea variable numeroTrx para loguear el numero de 
     *                     transacci�n.
     * <li>1.4 07/04/2014, Pablo Salazar (Imagemaker IT): -Se valida que la cuenta de cargo seleccionada 
     *                                    para efectuar el pago corresponde a una cuenta del cliente.
     * <li>1.5 04/03/2014, Ricardo Hidalgo (SEnTRA) - Kay Vera (Ing. Soft. BCI): Se agrega l�gica para registrar 
     *                     pagos pague directo previred en el repositorio de transacciones pendientes de 
     *                     firma.
     * <li>1.6 10/09/2015, Rodrigo Briones O. (Arq. Soluciones Bci): Se quita referencia a m�todo split por
     *                     compatibilidad con JDK 1.3. </li>
     * <li>1.7 20/10/2016  Adolfo Cid P. (IMIT) - Marcelo Avendano (BCI) - Se elimina todo lo de PASSCODE y
     *            cuando ocurra una SeguridadException se valida que el m�todo de autentificacion sea distinto a 
     *            SafeSigner, se enviara a la pagina de error. 
     * </li>                     	 
     * </ul>
     * </p>
     * 
     * @param mapping Representa el mapeo de una instancia del formulario.
     * @param actionForm ActionForm asociado al ActionMapping.
     * @param request La solicitud HTTP que se esta procesando.
     * @param response La respuesta entregada.
     * @return Env�a los datos a la p�gina de respuesta.
     * @since 1.0
     */
    public ActionForward confirmaPago(ActionMapping mapping, ActionForm actionForm, HttpServletRequest request,
            HttpServletResponse response) {
        if (getLogger().isEnabledFor(Level.INFO)) {
            getLogger().info("[confirmaPago][BCI_INI]");
        }
        String numeroTrx = "";
        String infoLog = "";
        try {

            ServiciosFirmasyPoderesDelegateImpl delegateServicioFirma = new ServiciosFirmasyPoderesDelegateImpl();
            TransaccionPendienteDeFirmaTOHelper transaccionPendienteDeFirmaTOHelper = new 
            		TransaccionPendienteDeFirmaTOHelper();
            
            PagueDirectoBusinessDelegateImpl delegate = new PagueDirectoBusinessDelegateImpl();
            HttpSession hs = request.getSession(false);
            SessionBCI session = getSessionBci(request);
            PagueDirectoTO pagueDirecto = (PagueDirectoTO) hs.getAttribute(OBJETO_PAGUE_DIRECTO);
            numeroTrx = pagueDirecto.getNumeroTransaccion();
            
            infoLog = "[" + numeroTrx + "][confirmaPago] ";
            if (getLogger().isEnabledFor(Level.DEBUG)) {
                getLogger().debug("[confirmaPago]"+infoLog + "pagueDirecto:" + pagueDirecto);
            }

            DatosPagadorTO pagador = pagueDirecto.getDatosPagador();

            if (getLogger().isEnabledFor(Level.DEBUG)) {
                getLogger().debug("[confirmaPago]"+infoLog + "DatosPagadorTO:" + pagador);
            }

            ApoderadoTO apoderado = pagador.getApoderado();

            if (getLogger().isEnabledFor(Level.DEBUG)) {
                getLogger().debug("[confirmaPago]"+infoLog + "DatosPagadorTO:" + apoderado);
            }

            PagueDirectoForm pagueDirectoForm = (PagueDirectoForm) actionForm;
            String numeroCuenta= pagueDirectoForm.getCuentaSeleccion();
            if (numeroCuenta==null) {
            	numeroCuenta="";
            }

            Long userIdrut = pagador.getRut();
        	DatosConvenioTO convenio = pagueDirecto.getDatosConvenio();
        	CuentaTO[] cuentas = delegate.listarCuentas(userIdrut, convenio.getCodigoMoneda());

        	boolean existeCuenta = false;
       		if(cuentas!= null && cuentas.length>0 && "".compareTo(numeroCuenta)!=0) {
       			for (int i = 0; i < cuentas.length && !existeCuenta ; i++) {
       				if(cuentas[i]!=null) {
       					existeCuenta = numeroCuenta.compareTo(cuentas[i].getNumeroCuenta()) == 0; 
       				}
				}
       		}
       		
       		if (!existeCuenta) {
       			getLogger().error("[confirmaPago] Error cuenta no corresponde con rut. Numero de cuenta: " + numeroCuenta + ", rut:" + userIdrut);
       			throw new PagueDirectoException("MNX-30023");
       		}
       		
           if (getLogger().isEnabledFor(Level.DEBUG)) {
               getLogger().debug("[confirmaPago]"+infoLog 
                       + "Se valida que la cuenta pertenezca al cliente");
           }
            
            pagueDirecto.setCuentaCargo(pagueDirectoForm.getCuentaSeleccion());
            pagueDirectoForm.setFechaActual(new Date());
            pagueDirectoForm.setTransaccion(pagueDirecto.getNumeroTransaccion());
           
            CuentaTO cuenta = new CuentaTO();
            cuenta.setNumeroCuenta(pagueDirectoForm.getCuentaSeleccion());

            if (getLogger().isEnabledFor(Level.DEBUG)) {
                getLogger().debug("[confirmaPago]"+infoLog + "cuenta:" + cuenta);
            }

            pagueDirecto.getDatosPagador().setCuenta(cuenta);
            
            TransaccionPendienteDeFirmaTO pagoPreviredMultimonedaPendienteFirmaTO = new TransaccionPendienteDeFirmaTO();
            if(pagueDirecto.getDatosPagador().getApoderado() != null && pagueDirecto.getDatosPagador().getApoderado().getCodigoConvenioBel() != null) {
                pagoPreviredMultimonedaPendienteFirmaTO = transaccionPendienteDeFirmaTOHelper.getPagoPreviredMultimonedaPendienteFirma(pagueDirecto);
            }
            
            boolean convenioHabilitado =false;
            
			String[] listaConvenios= StringUtil.divide(TablaValores.getValor(
            		REPO_TRANSACIONES,"CONVENIOS_PAGUEDIRECTO", "convenios"), ",");
            
            if (getLogger().isEnabledFor(Level.DEBUG)) {
                getLogger().debug("[confirmaPago]"+infoLog + "listaEmpresasRecaudadoras:" 
               + listaConvenios.length);
            }
            
            if(listaConvenios!=null && listaConvenios.length>0){
            for(int i=0;i<listaConvenios.length;i++){
                    if(listaConvenios[i].equalsIgnoreCase(
                            pagueDirecto.getDatosConvenio().getCodigoConvenio())) {
            		convenioHabilitado=true;
            	}
            	}
            }
       
            boolean tipoConvenioHabilitado =false;
            String[] listaTipoConvenios= StringUtil.divide(TablaValores.getValor(
            		REPO_TRANSACIONES,
					"TIPO_CONVENIO", "tipo"), ",");
					
            if (getLogger().isEnabledFor(Level.DEBUG)) {
                getLogger().debug("[confirmaPago]"+infoLog + "listaTipoConvenios:" + listaTipoConvenios.length);
            }

            if(listaTipoConvenios!=null && listaTipoConvenios.length>0){
	            for(int i=0;i<listaTipoConvenios.length;i++){
		            	if(listaTipoConvenios[i].equalsIgnoreCase(
	                        pagueDirecto.getDatosConvenio().getTipoConvenio())) {
	            		tipoConvenioHabilitado=true;
	            	}
	            }
            }
            
            String productoPaguedirecto= TablaValores.getValor(
    				REPO_TRANSACIONES,
    				"PRODUCTO_PAGUEDIRECTO", "valorPorDefecto");
            if (getLogger().isEnabledFor(Level.DEBUG)) {
                getLogger().debug("[confirmaPago]"+infoLog + "productoPaguedirecto:" + productoPaguedirecto);
            }        
            
    		boolean productoConvenioHabilitado = productoPaguedirecto.equalsIgnoreCase(
    				pagoPreviredMultimonedaPendienteFirmaTO.getCodigoProducto())? true:false;
            if (getLogger().isEnabledFor(Level.DEBUG)) {
                getLogger().debug("[confirmaPago]"+infoLog + "pagoPreviredMultimonedaPendienteFirmaTO:" 
                + pagoPreviredMultimonedaPendienteFirmaTO);
            } 
            if (getLogger().isEnabledFor(Level.DEBUG)) {
                getLogger().debug("[confirmaPago]"+infoLog + "Form:" + pagueDirectoForm);
            } 

            if (pagador.isDebeActuarComoEmpresa()) {
                grabarCompraPagueDirecto(pagueDirecto);
                pagueDirectoForm.setPaginaRetorno(pagueDirecto.getPaginaRetorno());
                	
                String servicioDobleFirma =TablaValores.getValor(REPO_TRANSACIONES, "SERVICIO_PAGUEDIRECTO", 
                		"valorPorDefecto");
                pagoPreviredMultimonedaPendienteFirmaTO.getCodigoServicio();
                boolean tieneServicioDobleFirma = pagoPreviredMultimonedaPendienteFirmaTO.getCodigoServicio() != null
                		&& pagoPreviredMultimonedaPendienteFirmaTO.getCodigoServicio().equalsIgnoreCase(servicioDobleFirma);
                if (getLogger().isEnabledFor(Level.DEBUG)) {
                    getLogger().debug("[confirmaPago]"+infoLog + "[productoConvenioHabilitado][" 
                    + productoConvenioHabilitado +  "][tipoConvenioHabilitado][" + tipoConvenioHabilitado
                    + "][convenioHabilitado][" + convenioHabilitado + "][tieneServicioDobleFirma]"
                    + "[" + tieneServicioDobleFirma + "]");
                }
                
                if (apoderado != null && apoderado.getCodigoConvenioBel()!= null) {
                	  
                	  if(productoConvenioHabilitado && tieneServicioDobleFirma && tipoConvenioHabilitado 
                			  && convenioHabilitado) {
                    delegateServicioFirma.insertarTransaccionPendienteDeFirma(
                    		pagoPreviredMultimonedaPendienteFirmaTO);
                	}
                      if (getLogger().isEnabledFor(Level.INFO)) {
                          getLogger().info("[confirmaPago][BCI_FINOK]"+infoLog + "Aprobado");
                      } 
                    return mapping.findForward("aprobado");
                }
                else {
                    if (getLogger().isEnabledFor(Level.INFO)) {
                        getLogger().info("[confirmaPago][BCI_FINOK]"+infoLog + "Comprobante");
                    }
                    return mapping.findForward("comprobante");
                }
            }
            else { 
                ValidaSesion autentica = new ValidaSesion(request);
                try {
                    autentica.validaAcceso(ValidaSesion.SESSION_BCI, SERVICIO);
                }
                catch (SeguridadException pdex) {
                	 if(logger.isEnabledFor(Level.ERROR)){
              	       getLogger().error("[confirmaPago] Error, Contiene "+SAFE_SIGNER+": "
                	                  + session.getMetodosAutenticacion().containsKey(SAFE_SIGNER));
                            } 
                   if (!session.getMetodosAutenticacion().containsKey(SAFE_SIGNER) 
                		   && !session.getMetodosAutenticacion().containsKey(ID_PRODENTRUSTSOFTTOKEN)
                				&&   !session.getMetodosAutenticacion().containsKey(ID_PRODENTRUSTTOKEN)){   
                	   if(logger.isEnabledFor(Level.ERROR)){
                  	       getLogger().error("[confirmaPago] Error, codigo:" + pdex.getCodigo() + ", mensaje:" + pdex.getSimpleMessage()); 
                        }
                	   guardarError("error.pagueDirecto", new String[] { pdex.getSimpleMessage() }, request);
                            return mapping.findForward("error");
                        }
                }
                grabarCompraPagueDirecto(pagueDirecto);
                pagueDirectoForm.setPaginaRetorno(pagueDirecto.getPaginaRetorno());
            }
            if (getLogger().isEnabledFor(Level.INFO)) {
                getLogger().info("[confirmaPago][BCI_FINOK]");
            }
            return mapping.findForward("comprobante");
        }
        catch (PagueDirectoException pdex) {
            guardarError("error.pagueDirecto", new String[] { pdex.getSimpleMessage() }, request);
            if (getLogger().isEnabledFor(Level.ERROR)){
                getLogger().error(infoLog + "[confirmaPago]["+infoLog+"][BCI_FINEX][PagueDirectoException]:" + pdex.getMessage(), pdex); 
            }
            return mapping.findForward("error");
        } 
        catch (Exception ex) {
               guardarError("error.pagueDirecto", new String[] { ex.getMessage() }, request);
            if (getLogger().isEnabledFor(Level.ERROR)){
                getLogger().error(infoLog + "[confirmaPago]["+infoLog+"][BCI_FINEX][Exception]:" + ex.getMessage(), ex); 
            }
            return mapping.findForward("error");
        }
    }

    /**
     * M�todo valida el passcode ingresado por el usuario.
     * 
     * <p>
     * Registro de Versiones:
     * <ul>
     * <li>1.0 08/11/2007, Robson Watt (Neoris Chile): versi�n inicial
     * <li>1.1 23/05/2008, Ra�l Eduardo Plata (Neoris Chile): Unifica log a [PagueDirectoAction]
     * <li>1.2 28/05/2013, Rodrigo Bravo C. (SEnTRA): Se crea variable numeroTrx para loguear el numero de 
     *                     transacci�n.
     * </ul>
     * </p>
     * 
     * @param mapping Representa el mapeo de una instancia del formulario.
     * @param actionForm ActionForm asociado al ActionMapping.
     * @param request La solicitud HTTP que se esta procesando.
     * @param response La respuesta entregada.
     * @return Env�a los datos a la p�gina de respuesta.
     * @since 1.0
     */
    public ActionForward validaPasscode(ActionMapping mapping, ActionForm actionForm, HttpServletRequest request,
            HttpServletResponse response) {
        String numeroTrx = "";
        if (logger.isDebugEnabled()){
            logger.debug("[PagueDirectoAction][validaPasscode]");
        }
        try {
            HttpSession hs = request.getSession(false);
            SessionBCI session = getSessionBci(request);
            PagueDirectoForm pagueDirectoForm = (PagueDirectoForm) actionForm;
            PagueDirectoTO pagueDirecto = (PagueDirectoTO) hs.getAttribute(OBJETO_PAGUE_DIRECTO);
            numeroTrx = pagueDirecto.getNumeroTransaccion();
            pagueDirectoForm.setFechaActual(new Date());
            pagueDirectoForm.setTransaccion(pagueDirecto.getNumeroTransaccion());
            String claveDos = String.valueOf(pagueDirectoForm.getClv1())
                    + String.valueOf(pagueDirectoForm.getClv2()) + String.valueOf(pagueDirectoForm.getClv3())
                    + String.valueOf(pagueDirectoForm.getClv4());
            Vector letras = new Vector();
            letras = (Vector) session.getAttrib("passcode");
            String cl = "";
            for (int k = 0; k < claveDos.length(); k++) {
                cl = cl + letras.get(new Integer(claveDos.substring(k, k + 1)).intValue());
            }
            ValidaSesion autentica = new ValidaSesion(request);
            try {
                autentica.autenticaSession(cl, "120");
                if (logger.isDebugEnabled()){
                    logger.debug("[" + numeroTrx + "][validaPasscode] [ cl ]");
                }
            }
            catch (SeguridadException e) {
                if (logger.isEnabledFor(Level.ERROR)){
                    logger.error("[" + numeroTrx + "][validaPasscode] Ex:" + ErroresUtil.extraeStackTrace(e));
                }
                guardarError("error.seguridad", new String[] { e.getSimpleMessage() }, request);
                return mapping.findForward("error");
            }
            session.setAttrib("autenticado", "Si");
            try {
                autentica.validaAcceso(ValidaSesion.SESSION_BCI, SERVICIO);
            }
            catch (SeguridadException se) {
                if (logger.isEnabledFor(Level.ERROR)){
                    logger.error("[" + numeroTrx + "][validaPasscode] ex:" + ErroresUtil.extraeStackTrace(se));
                }
                guardarError("error.seguridad", new String[] { se.getSimpleMessage() }, request);
                return mapping.findForward("error");
            }
            grabarCompraPagueDirecto(pagueDirecto);
            pagueDirectoForm.setPaginaRetorno(pagueDirecto.getPaginaRetorno());
            return mapping.findForward("comprobante");
        }
        catch (PagueDirectoException pdex) {
            if (logger.isEnabledFor(Level.ERROR)){
                logger.error("[" + numeroTrx + "][validaPasscode] Error:" + ErroresUtil.extraeStackTrace(pdex));
            }
            guardarError("error.pagueDirecto", new String[] { pdex.getSimpleMessage() }, request);
            return mapping.findForward("error");
        }
    }

    /**
     * M�todo que valida que el n�mero de token ingresado sea el que est� asociado al asociado al cliente.
     * 
     * <p>
     * Registro de Versiones:
     * <ul>
     * <li>1.0 08/11/2007, Robson Watt (Neoris Chile): versi�n inicial.
     * <li>1.1 23/05/2008, Ra�l Eduardo Plata (Neoris Chile): Unifica log a [PagueDirectoAction].
     * <li>2.0 27/04/2010, Pedro Carmona Escobar (SEnTRA): Se agrega la obtenci�n del apoderado una vez ya
     *                  validado el multipass, con el fin de setear este valor en el objeto pagueDiretoTO, y as�
     *                  sea almacenado en la SessionHttp.
     * <li>2.1 28/05/2013, Rodrigo Bravo C. (SEnTRA): Se crea variable numeroTrx para loguear el numero de 
     *                     transacci�n.
     * <li>2.2 25/06/2013, Pedro Carmona Escobar (SEnTRA): Se incorpora l�gica de agrupaci�n de productos en caso
     *                          de que el pago provenga de Directemar. 
     * <li>1.4 07/04/2014, Pablo Salazar (Imagemaker IT): -Se agrega Journalizaci�n    
     * <li>1.5 14/10/2015  Oscar Nahuelpan (SEnTRA) - Marcelo Avenda�o (ing. soft. BCI): Se agrega flujo 
     * de validaci�n de servicios.</li></li>
     * <li>1.6 30/08/2017 Sergio Bustos B. (Imagemaker) - Miguel Anza (Ing. Soft. BCI): Se agrega registro de evento al iniciar sesion.</li>   
     * <li>1.7 25/09/2017  Marcelo Fuentes (SEnTRA) - Ricardo Carrasco (Ing. Soft. BCI): Se agrega validaci�n que   
     * revisa si se requiere autenticaci�n Entrust con dispositivo fisico.</li>
     * </ul>
     * </p>
     * 
     * @param mapping Representa el mapeo de una instancia del formulario.
     * @param actionForm ActionForm asociado al ActionMapping.
     * @param request La solicitud HTTP que se esta procesando.
     * @param response La respuesta entregada.
     * @return Env�a los datos a la p�gina de respuesta.
     * @since 1.0
     */

    public ActionForward validaToken(ActionMapping mapping, ActionForm actionForm, HttpServletRequest request,
            HttpServletResponse response) {
        String numeroTrx = "";
        if (getLogger().isEnabledFor(Level.INFO)) {
            getLogger().info("[PagueDirectoAction][validaToken]");
        }
        HttpSession hs = request.getSession(false);
        SessionBCI session = getSessionBci(request);
        PagueDirectoTO pagueDirecto = (PagueDirectoTO) hs.getAttribute(OBJETO_PAGUE_DIRECTO);
        PagueDirectoForm pagueDirectoForm = (PagueDirectoForm) actionForm;
        ValidaSesion autentica = new ValidaSesion(request);
        numeroTrx = pagueDirecto.getNumeroTransaccion();
        String canalAut = "";
        try {
            canalAut = session.getMetodosAutenticacion().containsKey(ID_PRODENTRUSTTOKEN) ? "entrustToken" : "token";
            if (canalAut.equalsIgnoreCase("token")) {
                autentica.autenticaSession(pagueDirectoForm.getToken(), canalAut);
                
                JournalizadorHelper.journaliza(session, EVE_LOGIN, SUB_COD_OK,
                                    String.valueOf(session.getCliente().getRut()), ID_PRODTOKEN, null);
                if (getLogger().isEnabledFor(Level.INFO)) {
                    getLogger().info("[" + numeroTrx + "][validaToken] [ Token  ]" + autentica.toString());
                }
            }
            else if (canalAut.equalsIgnoreCase("entrustToken")) {
                boolean resultado = ControllerBCI.verificarAutenticacion(ENTRUST_TOKEN, null, 
                        session.getParametrosEstrategiaSegundaClaveTO().getUsuarioSegundaClave(), null, null, pagueDirectoForm.getToken());
                
                if (getLogger().isEnabledFor(Level.INFO)) {
                    getLogger().info("[" + numeroTrx + "][validaToken] [ Entrust Token] " + resultado);
                }
                if (resultado) {
                    JournalizadorHelper.journaliza(session, EVE_LOGIN, SUB_COD_OK,
                            String.valueOf(session.getCliente().getRut()), ID_PRODENTRUSTTOKEN, null, null, EST_EVENTO_NEGOCIO_PRO);
                }
                else {
                    JournalizadorHelper.journaliza(session, EVE_LOGIN, SUB_COD_NOK,
                            String.valueOf(session.getCliente().getRut()), ID_PRODENTRUSTTOKEN, null, null, EST_EVENTO_NEGOCIO_REC);
                }
            }
        }
        catch (SeguridadException e) {
            if(getLogger().isEnabledFor(Level.ERROR)){
                getLogger().error("[" + numeroTrx + "][ValidaToken] Ex:" + ErroresUtil.extraeStackTrace(e));
            }
            if (canalAut.equalsIgnoreCase("token")) {
                JournalizadorHelper.journaliza(session, EVE_LOGIN, SUB_COD_NOK,
                        String.valueOf(session.getCliente().getRut()), ID_PRODTOKEN, null);
            }
            else {
                JournalizadorHelper.journaliza(session, EVE_LOGIN, SUB_COD_NOK,
                        String.valueOf(session.getCliente().getRut()), ID_PRODENTRUSTTOKEN, null, null, EST_EVENTO_NEGOCIO_REC);
            }
            guardarError("error.seguridad", new String[] { e.getSimpleMessage() }, request);
            return mapping.findForward("error");
        }
        request.removeAttribute("flagToken");
        session.cambiaEstadoMetodoAutenticacion("TOK", true);
        try {
            if (getLogger().isEnabledFor(Level.INFO)) {
                getLogger().info("[" + numeroTrx + "][validaToken] [ isDebeActuarComoEmpresa() ]:"
                        + pagueDirecto.getDatosPagador().isDebeActuarComoEmpresa());
            }
            if (pagueDirecto.getDatosPagador().isDebeActuarComoEmpresa()){
                if (getLogger().isEnabledFor(Level.INFO)) {
                    getLogger().info("[" + numeroTrx + "][validaToken] [ rutApoderado ]:"
                            + pagueDirectoForm.getRutApoderado());
                    getLogger().info("[" + numeroTrx + "][validaToken] [ rutUsuario ]:"
                            + pagueDirectoForm.getRutUsuario());
                    getLogger().info("[" + numeroTrx + "][validaToken] [ tipoConvenio ]:"
                            + pagueDirecto.getDatosConvenio().getTipoConvenio());
                }
                PagueDirectoBusinessDelegateImpl delegate = new PagueDirectoBusinessDelegateImpl();
                ApoderadoTO apoderado = delegate.getApoderadoEmpresa(new Long(pagueDirectoForm.getRutApoderado()),
                        new Long(pagueDirectoForm.getRutUsuario())
                , pagueDirecto.getDatosConvenio().getTipoConvenio());


                if (getLogger().isEnabledFor(Level.INFO)) {
                    getLogger().info("[" + numeroTrx + "][validaToken] [ ApoderadoTO ]:" + apoderado);
                }
                pagueDirecto.getDatosPagador().setApoderado(apoderado);
                pagueDirectoForm.setTipoConvenioBEL(apoderado.getTipoConvenioApoderado());

                if (apoderado != null && apoderado.getCodigoConvenioBel() != null) {
                    if (getLogger().isEnabledFor(Level.INFO)) {
                        getLogger().info("[" + numeroTrx + "][validaToken]:" + apoderado.getCodigoConvenioBel());
                    }
                    pagueDirectoForm.setDobleFirma(true);
                }
            }
            pagueDirectoForm.setNombreUsuario(pagueDirecto.getDatosPagador().getNombre());
            if (getLogger().isEnabledFor(Level.INFO)) {
                getLogger().info("[" + numeroTrx + "][validaToken] rut convenio: " 
                        + pagueDirecto.getDatosConvenio().getRut());
                getLogger().info("[" + numeroTrx + "][validaToken] rut directemar: " 
                        + Long.parseLong(RUT_DIRECTEMAR));
                getLogger().info("[" + numeroTrx + "][validaToken] se hara agrupaci�n para directemar?: " 
                        + (pagueDirecto.getDatosConvenio().getRut().longValue() == new Long(RUT_DIRECTEMAR)
                        .longValue()));
            }
            if (pagueDirecto.getDatosConvenio().getRut().longValue() == new Long(RUT_DIRECTEMAR).longValue()){
                PagueDirectoTOHelper pagueTOHelper = new PagueDirectoTOHelper();
                pagueDirectoForm.setProductos(pagueTOHelper
                        .agruparProductosDirectemar(pagueDirecto.getProducto()));
            }
            else{
        pagueDirectoForm.setProductos(pagueDirecto.getProducto());
            }
            if (getLogger().isEnabledFor(Level.INFO)) {
                getLogger().info("[" + numeroTrx + "][validaToken] form: "
                        + StringUtil.contenidoDe(pagueDirectoForm.getProductos()));
            }
            pagueDirectoForm.setCantidad(new BigInteger(pagueDirecto.getCantidadProductos().toString()));
            pagueDirectoForm.setFechaActual(new Date());
            pagueDirectoForm.setTransaccion(pagueDirecto.getNumeroTransaccion());

            if (getLogger().isEnabledFor(Level.INFO)) {
                getLogger().info("[" + numeroTrx + "][validaToken] [ Datos Pagador y Transacci�n  ]:"
                        + pagueDirectoForm.toString());
                getLogger().info("[" + numeroTrx + "][validaToken] [ pagueDirectoTO  ]:" + pagueDirecto.toString());
                getLogger().info("[" + numeroTrx + "][validaToken] actuarComoEmpresa:" + pagueDirecto.getDatosPagador().isDebeActuarComoEmpresa());
            }
            
            String flagActivarValidacion = "";
            if (pagueDirecto.getDatosPagador().isDebeActuarComoEmpresa()) {
                flagActivarValidacion = TablaValores.getValor("paguedirectoMonex.parametros", "FlagFlujoSeguridadEmpresa", "estado");
            }
            else {
                String canal = obtenerCanal(request.getServletPath());
                if (getLogger().isEnabledFor(Level.INFO)) {
                    getLogger().info("[" + numeroTrx + "][validaToken] canal:" + canal);
                }
                flagActivarValidacion = TablaValores.getValor("paguedirectoMonex.parametros", "FlagFlujoSeguridadPersona", "estado");
            }
            
            if (getLogger().isEnabledFor(Level.INFO)) {
              getLogger().info("[validaToken] flag[" + flagActivarValidacion+"] numConvenio [" + pagueDirecto.getDatosConvenio().getCodigoConvenio() + "]");
            }
          
            if (flagActivarValidacion.equals("OK")) {
                String numConvenio = pagueDirecto.getDatosConvenio().getCodigoConvenio();
                String mensajeErrorInscripcion = TablaValores.getValor(TABLA_ERRORES, "MNX-30040", "Desc");
                String mensajeErrorActivacion = TablaValores.getValor(TABLA_ERRORES, "MNX-30041", "Desc");
                String canal = obtenerCanal(request.getServletPath());
                if (pagueDirecto.getDatosPagador().isDebeActuarComoEmpresa()) {
                    mensajeErrorInscripcion = StringUtil.reemplazaTodo(mensajeErrorInscripcion, "{link}", URL_BCI_EMPRESA);
                    mensajeErrorActivacion = StringUtil.reemplazaTodo(mensajeErrorActivacion, "{link}", URL_BCI_EMPRESA);
                }
                else {
                    if (canal.equals(PagueDirectoParametros.PATH_TBANC)) {
                        mensajeErrorInscripcion = StringUtil.reemplazaTodo(mensajeErrorInscripcion, "{link}", URL_TBANC);
                        mensajeErrorActivacion = StringUtil.reemplazaTodo(mensajeErrorActivacion, "{link}", URL_TBANC);
                    }
                    else {
                        mensajeErrorInscripcion = StringUtil.reemplazaTodo(mensajeErrorInscripcion, "{link}", URL_BCI_PERSONA);
                        mensajeErrorActivacion = StringUtil.reemplazaTodo(mensajeErrorActivacion, "{link}", URL_BCI_PERSONA);
                    }
                }
                
                try {
                    ServiciosPagosDelegate pagosDelegate = ServiciosPagosDelegate.getInstance();
                    DatosConvenioTO datosConvenio = pagosDelegate.obtenerDatosConvenio(numConvenio);
                    if (getLogger().isEnabledFor(Level.INFO)) {
                        getLogger().info("[validaToken] datosConvenio[" + datosConvenio.toString() + "], rutCliente[" + pagueDirectoForm.getRutUsuario() + "]");
                    }
                    if (datosConvenio.getMarcaRiesgo() == 1) {
                        AliasBotonPagoTO datos = new AliasBotonPagoTO();
                        datos.setRutCliente(Long.parseLong(pagueDirectoForm.getRutUsuario()));
                        DatosConvenioTO datosConv = new DatosConvenioTO();
                        datosConv.setCodigoConvenio(numConvenio);
                        datos.setDatosConvenio(datosConv);
                        AliasBotonPagoTO[] datosSuscripcion = pagosDelegate.obtenerDatosSuscripcion(datos);
                        if (datosSuscripcion == null || datosSuscripcion.length == 0){
                            mensajeErrorInscripcion = StringUtil.reemplazaTodo(mensajeErrorInscripcion, "{urlPortal}", datosConvenio.getUrl());
                            guardarError("error.seguridad", new String[] {mensajeErrorInscripcion}, request);
                            return mapping.findForward("error");
                        }
                        if (getLogger().isEnabledFor(Level.INFO)) {
                            getLogger().info("[validaToken] datosSuscripcion[" + datosSuscripcion[0].toString() + "]");
                        }
                        if (!datosSuscripcion[0].getEstado().equals("ACT")) {
                            mensajeErrorActivacion = StringUtil.reemplazaTodo(mensajeErrorActivacion, "{urlPortal}", datosConvenio.getUrl());
                            guardarError("error.seguridad", new String[] {mensajeErrorActivacion}, request);
                            return mapping.findForward("error");
                        }
                        
                        if (getLogger().isEnabledFor(Level.INFO)) {
                            getLogger().info("[validaToken] Monto maximo servicio:[" + datosSuscripcion[0].getMontoMaximo().longValue() + "]");
                            getLogger().info("[validaToken] Total a pagar:[" + pagueDirectoForm.getTotal() + "]");
                            getLogger().info("[validaToken] Costo envio:[" + pagueDirectoForm.getCostoEnvio() + "]");
                        }
                        for (int cont = 0; cont < pagueDirectoForm.getProductos().length; cont++) {
                            ProductoTO producto = pagueDirectoForm.getProductos()[cont];
                            if (getLogger().isEnabledFor(Level.INFO)) {
                                getLogger().info("[validaToken] monto detalle a pagar:[" + (producto.getMonto().longValue() * producto.getCantidad().intValue()) + "]");
                            }
                            if (datosSuscripcion[0].getMontoMaximo().longValue() < pagueDirectoForm.getTotal().longValue()) {
                                guardarError("error.seguridad", new String[] {TablaValores.getValor(TABLA_ERRORES, "MNX-30042", "Desc")}, request);
                                return mapping.findForward("error");
                            }
                        }
                    }
                }
                catch (Exception e) {
                    if(getLogger().isEnabledFor(Level.ERROR)){
                        getLogger().error("[validaToken][BCI_FINEX][Exception] e:" + e.toString(),e);
                    }
                    guardarError("error.pagueDirecto", new String[] { e.getMessage() }, request);
                    return mapping.findForward("error");
                }
            }
            PagueDirectoBusinessDelegateImpl delegate = new PagueDirectoBusinessDelegateImpl();
        hs.setAttribute(OBJETO_PAGUE_DIRECTO, pagueDirecto);
            TransaccionTO transaccionRegistra = new TransaccionTO();
            String numTrx = StringUtil.completaPorLaIzquierda(pagueDirecto.getNumeroTransaccion(), CANTIDAD_20, '0');
            transaccionRegistra.setNumeroTransaccion(numTrx);
            transaccionRegistra.setCodigoConvenio(pagueDirecto.getDatosConvenio().getCodigoConvenio());
            if(getLogger().isDebugEnabled()){
            	getLogger().debug("[validaToken] [registrarTrxPagueDirecto] numTrx: "+numTrx+" CodigoConvenio: " + pagueDirecto.getDatosConvenio().getCodigoConvenio());
            }
            delegate.registrarTrxPagueDirecto(transaccionRegistra);
            registrarCompraPagueDirecto(pagueDirecto);
            return mapping.findForward("pago");
        }
        catch (PagueDirectoException pdex) {
            if(getLogger().isEnabledFor(Level.ERROR)){
                getLogger().error("[" + numeroTrx + "][validaToken] Error:" + ErroresUtil.extraeStackTrace(pdex));
            }
            guardarError("error.pagueDirecto", new String[] { pdex.getSimpleMessage() }, request);
            return mapping.findForward("error");
        }
    }

    /**
     * M�todo se encarga de cancelar la operaci�n realizada hasta el momento.
     * 
     * <p>
     * Registro de Versiones:
     * <ul>
     * <li>1.0 08/11/2007, Robson Watt (Neoris Chile): versi�n inicial
     * <li>1.1 23/05/2008, Ra�l Eduardo Plata (Neoris Chile): Unifica log a [PagueDirectoAction]
     * </ul>
     * </p>
     * 
     * @param mapping Representa el mapeo de una instancia del formulario.
     * @param actionForm ActionForm asociado al ActionMapping.
     * @param request La solicitud HTTP que se esta procesando.
     * @param response La respuesta entregada.
     * @return Env�a los datos a la p�gina de respuesta.
     * @since 1.0
     */

    public ActionForward cancelar(ActionMapping mapping, ActionForm actionForm, HttpServletRequest request,
            HttpServletResponse response) {
        if (logger.isDebugEnabled()){
            logger.debug("[PagueDirectoAction][cancelar]");
        }
        HttpSession hs = request.getSession(false);
        PagueDirectoTO pagueDirecto = (PagueDirectoTO) hs.getAttribute(OBJETO_PAGUE_DIRECTO);
        try {
            PagueDirectoBusinessDelegate delegate = new PagueDirectoBusinessDelegateImpl();
            delegate.cancelarCompraPagueDirecto(pagueDirecto);
        }
        catch (PagueDirectoException ex) {
            if (logger.isEnabledFor(Level.ERROR)){
                logger.error("[" + pagueDirecto.getNumeroTransaccion() + "][cancelar] Error:" 
                        + ErroresUtil.extraeStackTrace(ex));
            }
            guardarError("error.pagueDirecto", new String[] { ex.getSimpleMessage() }, request);
            return mapping.findForward("error");
        }
        return mapping.findForward("errorCancelar");
    }

    /**
     * M�todo que registra la compra en la base de datos.
     * 
     * <p>
     * Registro de Versiones:
     * <ul>
     * <li>1.0 08/11/2007, Robson Watt (Neoris Chile): versi�n inicial
     * <li>1.1 23/05/2008, Ra�l Eduardo Plata (Neoris Chile): Agrega log.
     * </ul>
     * </p>
     * 
     * @param pagueDirecto con la informacion del pago.
     * @throws PagueDirectoException en caso de error.
     * @since 1.0
     */

    private void registrarCompraPagueDirecto(PagueDirectoTO pagueDirecto) throws PagueDirectoException {
        if (logger.isDebugEnabled()){
            log.debug("[" + pagueDirecto.getNumeroTransaccion() + "][registrarCompraPagueDirecto]");
        }
        PagueDirectoBusinessDelegate delegate = new PagueDirectoBusinessDelegateImpl();
        delegate.registrarCompraPagueDirecto(pagueDirecto);
    }

    /**
     * M�todo que graba la compra en la base de datos.
     * 
     * <p>
     * Registro de Versiones:
     * <ul>
     * <li>1.0 08/11/2007, Robson Watt (Neoris Chile): versi�n inicial
     * <li>1.1 23/05/2008, Ra�l Eduardo Plata (Neoris Chile): Unifica log a [PagueDirectoAction]
     * </ul>
     * </p>
     * 
     * @param pagueDirecto con informaci�n del pago.
     * @throws PagueDirectoException en caso de error.
     * @since 1.0
     */

    private void grabarCompraPagueDirecto(PagueDirectoTO pagueDirecto) throws PagueDirectoException {
        if (logger.isDebugEnabled()){
            logger.debug("[" + pagueDirecto.getNumeroTransaccion() + "][grabarCompraPagueDirecto]");
        }
        PagueDirectoBusinessDelegate delegate = new PagueDirectoBusinessDelegateImpl();
        RespuestaCompraTO respuesta = delegate.grabarCompraPagueDirecto(pagueDirecto);
        ApoderadoTO apoderado = pagueDirecto.getDatosPagador().getApoderado();
        String convenioBel = apoderado == null ? null : apoderado.getCodigoConvenioBel();

        String estado = PagueDirectoParametros.getEstadoRetorno(pagueDirecto.getDatosConvenio().getRut()
                .toString(), respuesta.getCodigoError(), respuesta.getCodigoRetorno(), convenioBel);

        String urlRetorno = pagueDirecto.getPaginaRetorno() + "?"
                + PagueDirectoParametros.getURLRetorno(pagueDirecto.getDatosConvenio().getRut().toString(),
                        pagueDirecto.getNumeroTransaccion(), pagueDirecto.getCantidadProductos().toString(), estado);
        if (logger.isDebugEnabled()){
            logger.debug("[" + pagueDirecto.getNumeroTransaccion() + "][grabarCompraPagueDirecto] - "
                + urlRetorno);
        }
        pagueDirecto.setPaginaRetorno(urlRetorno);
    }

    /**
     * M�todo construye un objeto ActionError donde se tiene el control de las PagueDirectoException para ser
     * desplegadas en la jsp de error.
     * 
     * <p>
     * Registro de Versiones:
     * <ul>
     * <li>1.0 08/11/2007, Robson Watt (Neoris Chile): versi�n inicial
     * </ul>
     * </p>
     * 
     * @param error con el error producido.
     * @param mensaje con los mensajes de error. 
     * @param request con la solicitud HTTP que se esta procesando.
     * @since 1.0
     */
    private void guardarError(String error, String[] mensaje, HttpServletRequest request) {
        ActionErrors errors = new ActionErrors();
        if (mensaje != null) {
            errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(error, mensaje));
        }
        else {
            errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(error));
        }
        saveErrors(request, errors);
    }

    
    /**
     * M�todo construye un objeto de SessionBCI.
     * 
     * <p>
     * Registro de Versiones:
     * <ul>
     * <li>1.0 08/11/2007, Robson Watt (Neoris Chile): versi�n inicial
     * </ul>
     * </p>
     * 
     * @param request con la solicitud HTTP que se esta procesando.
     * @return SessionBCI con la sesi�n Bci.
     * @since 1.0
     */
    private SessionBCI getSessionBci(HttpServletRequest request) {
        HttpSession sesionHttp = request.getSession(false);
        if (logger.isDebugEnabled()){
            logger.debug("sesionHttp = [" + sesionHttp + "]");
        }
        if (sesionHttp != null) {
            return (SessionBCI) sesionHttp.getAttribute(ValidaSesion.SESSION_BCI);
        }
        else {
            return null;
        }
    }

    /**
     * M�todo retorna parte del path de donde se est� accediendo a la aplicaci�n. La idea de este m�todo es que
     * retorne un String "bci", si es que se est� accediendo a la aplicaci�n mediante BCI, un String "tbc", si es
     * que se est� accediendo mediante TBANC. Esto permitir� concatenar el canal existente con esta informaci�n
     * para crear un nuevo objeto Canal donde se est� invocando a este m�todo.
     * La implementaci�n de este m�todo para obtener el canal se realiza debido a la ausencia de alg�n par�metro
     * que indique el canal de ingreso en la petici�n inicial de usuario. La mensajer�a ha sido definida por SII y
     * TGR.
     * 
     * <p>
     * Registro de Versiones:
     * <ul>
     * <li>1.0 08/11/2007, Robson Watt (Neoris Chile): versi�n inicial
     * </ul>
     * </p>
     * 
     * @param path con el path a procesar.
     * @return String para concatenar al canal.
     * @throws PagueDirectoException en caso de error.
     * @since 1.0
     */
    private String obtenerCanal(String path) throws PagueDirectoException {
        String bci = TablaValores.getValor("canales.parametros","540bci","pathJSP");
        String tbc = TablaValores.getValor("canales.parametros","540tbc","pathJSP");
        if (path.startsWith(bci)) {
            return PagueDirectoParametros.PATH_BCI;
        }
        else if (path.startsWith(tbc)) {
            return PagueDirectoParametros.PATH_TBANC;
        }
        else {
            throw new PagueDirectoException("Canal no v�lido");
        }

    }
    

    /**
     * M�todo construye un objeto Vector con n�meros aleatorios de 0 a 9, los que son desplegados en la p�gina de
     * ingreso de passcode.Este m�todo fue modificado del m�todo existente en el servlet de Login.
     * 
     * <p>
     * Registro de Versiones:
     * <ul>
     * <li>1.0 08/11/2007, Robson Watt (Neoris Chile): versi�n inicial
     * </ul>
     * </p>
     * 
     * @return Vector con n�meros aleatorios.
     * @see wcorp.servletcorp.seguridad.Login
     * @since 1.0
     */
    public static Vector obtenerLetrasPasscodeAleatoreas() {
        String[] letras = new String[] { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J" };
        String[] orden = new String[CANTIDAD_LETRAS];
        int i = 0;
        int numero = 0;
        Random rand = new Random();
        Vector vec = new Vector();
        int j=0;
        while (i < CANTIDAD_LETRAS) {
            numero = rand.nextInt(CANTIDAD_LETRAS);
            if (orden[numero] == null) {
                orden[numero] = letras[i];                
                i++;
            }
        }       
        for (j = 0; j < CANTIDAD_LETRAS; j++) {
            if (orden[j] != null) {
                vec.add(orden[j]);             
            }
       }       
        return vec;
    }

    /**
     * M�todo que valida el codigo ingresado de SafeSigner.
     * 
     * <p>
     * Registro de Versiones:
     * <ul>
     * <li>1.0 10/04/2015 Rodolfo Kafack Ghinelli (SEnTRA) - Rodrigo Briones O. (Arq. de Soluciones): Se agrega validacion de SafeSigner.</li>
     * <li>1.1 30/08/2017 Sergio Bustos B. (Imagemaker) - Miguel Anza (Ing. Soft. BCI): Se agrega registro de evento al iniciar sesion.</li>
     * </ul>
     * </p>
     * 
     * @param mapping Representa el mapeo de una instancia del formulario.
     * @param actionForm ActionForm asociado al ActionMapping.
     * @param request La solicitud HTTP que se esta procesando.
     * @param response La respuesta entregada.
     * @return Env�a los datos a la p�gina de respuesta.
     * @since 1.7
     */
    public ActionForward validaSafeSigner(ActionMapping mapping, ActionForm actionForm, HttpServletRequest request,
            HttpServletResponse response){

        if (getLogger().isEnabledFor(Level.INFO)){
            getLogger().info("[validaSafeSigner][BCI_INI] iniciado.");
        }
        HttpSession hs = request.getSession(false);
        SessionBCI sessionBci = (SessionBCI)hs.getAttribute(ValidaSesion.SESSION_BCI);

        PagueDirectoTO pagueDirecto = (PagueDirectoTO) hs.getAttribute(OBJETO_PAGUE_DIRECTO);
        PagueDirectoForm pagueDirectoForm = (PagueDirectoForm) actionForm;

        String codigoConfirmacion = (String) pagueDirectoForm.getCodigoConfirmacionQR();

        if(sessionBci.getAttrib("identificadorPrioridadSegundaClave").equals("SafeSigner")
                && sessionBci.getAttrib("identificadorEstadoSegundaClave").equals("Active")
                && codigoConfirmacion.length() > 0) {



            int estatusTbl = Integer.parseInt(TablaValores.getValor(PARAMSAFESIGNER, "estatusok", "valor"));
            boolean estatusOK=false;

            if(estatusTbl==0){
                estatusOK=true;	
            }

            RepresentacionSegundaClave segundaClave = new RepresentacionNumericaSegundaClaveTO();
            segundaClave.setTipoDeClave(RepresentacionSegundaClave.CLAVE_ALFANUMERICA);
            segundaClave.setClaveAlfanumerica(codigoConfirmacion);

            try {
                ResultadoOperacionSegundaClaveTO resultado = 
                        ControllerBCI.validarSegundaClave(request, IDNAME, SERVICIO, segundaClave);

                if(getLogger().isDebugEnabled()){
                    getLogger().debug("[validaSafeSigner]resultado: " + resultado);
                }
                if(resultado == null || resultado.isEstatus() != estatusOK){
                    guardarError("error.seguridad", 
                            new String[] { "error de autentecaci�n safeSigner" }, request);
                    sessionBci.cerrar();
                    return mapping.findForward("error");
                }

            } 
            catch (SeguridadException e) {
                if(getLogger().isEnabledFor(Level.ERROR)){
                    getLogger().error("[validaSafeSigner][BCI_FINEX] SeguridadException", e);
                }
                guardarError("error.seguridad", new String[] {e.getSimpleMessage() }, request);
                sessionBci.cerrar();
                return mapping.findForward("error");

            } 
            catch (NoSessionException e) {
                if(getLogger().isEnabledFor(Level.ERROR)){
                    getLogger().error("[validaSafeSigner][BCI_FINEX] NoSessionException", e);
                }
                guardarError("error.seguridad", new String[] {e.getSimpleMessage() }, request);
                sessionBci.cerrar();
                return mapping.findForward("error");
            } 
            catch (IOException e) {
                if(getLogger().isEnabledFor(Level.DEBUG)){
                    getLogger().debug("[validaSafeSigner][BCI_FINEX] IOException", e);
                }
                guardarError("error.seguridad", new String[] {e.getMessage() }, request);
                sessionBci.cerrar();
                return mapping.findForward("error");
            }

            request.removeAttribute("flagSafeSigner");
            sessionBci.cambiaEstadoMetodoAutenticacion("MPM", true);
            hs.setAttribute("sessionBci", sessionBci);
            if(getLogger().isDebugEnabled()){
                getLogger().debug("[validaSafeSigner] metodos: " + StringUtil.contenidoDe(sessionBci.getMetodosAutenticacion()));
            }
            try {
            	PagueDirectoBusinessDelegateImpl delegate = new PagueDirectoBusinessDelegateImpl();
                if (pagueDirecto.getDatosPagador().isDebeActuarComoEmpresa()) {
                    ApoderadoTO apoderado = delegate.getApoderadoEmpresa(
                            new Long(pagueDirectoForm.getRutApoderado()),
                            new Long(pagueDirectoForm.getRutUsuario()),
                            pagueDirecto.getDatosConvenio().getTipoConvenio());

                    pagueDirecto.getDatosPagador().setApoderado(apoderado);
                    pagueDirectoForm.setTipoConvenioBEL(apoderado
                            .getTipoConvenioApoderado());

                    if (apoderado != null
                            && apoderado.getCodigoConvenioBel() != null) {

                        pagueDirectoForm.setDobleFirma(true);
                    }
                }
                pagueDirectoForm.setNombreUsuario(pagueDirecto
                        .getDatosPagador().getNombre());
                pagueDirectoForm.setProductos(pagueDirecto.getProducto());
                pagueDirectoForm.setCantidad(new BigInteger(pagueDirecto
                        .getCantidadProductos().toString()));
                pagueDirectoForm.setFechaActual(new Date());
                pagueDirectoForm.setTransaccion(pagueDirecto
                        .getNumeroTransaccion());

                hs.setAttribute(OBJETO_PAGUE_DIRECTO, pagueDirecto);

                TransaccionTO transaccionRegistra = new TransaccionTO();
                String numTrx = StringUtil.completaPorLaIzquierda(pagueDirecto.getNumeroTransaccion(), CANTIDAD_20, '0');
                transaccionRegistra.setNumeroTransaccion(numTrx);
                transaccionRegistra.setCodigoConvenio(pagueDirecto.getDatosConvenio().getCodigoConvenio());
                if(getLogger().isDebugEnabled()){
                	getLogger().debug("[validaSafeSigner] [registrarTrxPagueDirecto] numTrx: " + numTrx + " CodigoConvenio: " + pagueDirecto.getDatosConvenio().getCodigoConvenio());
                }
                delegate.registrarTrxPagueDirecto(transaccionRegistra);
                registrarCompraPagueDirecto(pagueDirecto);
                
                if (getLogger().isEnabledFor(Level.INFO)){
                    getLogger().info("[validaSafeSigner][BCI_FINOK].");
                }
                
                return mapping.findForward("pago");
            }
            catch (PagueDirectoException pdex) {
                if(getLogger().isEnabledFor(Level.ERROR)){
                    getLogger().error("[validaSafeSigner][BCI_FINEX] PagueDirectoException", pdex);
                }
                guardarError("error.pagueDirecto", new String[] { pdex.getSimpleMessage() }, request);
                return mapping.findForward("error");
            }

        } 
        else {

            guardarError("error.seguridad",
                    new String[] { "error de autentecaci�n safeSigner" },
                    request);
            sessionBci.cerrar();
            return mapping.findForward("error");
        }

    }
    
    /**
     * <p>
     * M�todo que valida la clave ingresada por el usuario cuando el metodo de autenticacion corresponde a 
     * Entrust Soft Token en modo Online.
     * </p>
     * 
     * Registro de versiones:
     * <ul>
     * <li>1.0 25/09/2017, Marcelo Fuentes (SEnTRA) - Ricardo Carrasco (Ing. Soft. BCI): Versi�n Inicial.</li>
     * </ul>
     * <p>
     * 
     * @param mapping representa el mapeo de una instancia del formulario.
     * @param actionForm ActionForm asociado al ActionMapping.
     * @param request la solicitud HTTP que se esta procesando.
     * @param response la respuesta entregada.
     * @return Env�a los datos a la p�gina de respuesta.
     * @since 1.10
     */
    public ActionForward validaEntrustSoftTokenOnline(ActionMapping mapping, ActionForm actionForm, HttpServletRequest request,
            HttpServletResponse response) {
        getLogger().info("[validaEntrustSoftTokenOnline] Entro");
        
        HttpSession hs = request.getSession(false);
        SessionBCI sessionBci = (SessionBCI)hs.getAttribute(ValidaSesion.SESSION_BCI);
        String codeConfirmation = (String)sessionBci.getAttrib("transactionId");
        sessionBci.removeAttrib("transactionId");
        return validaEntrustSoftToken(mapping, actionForm, request, response, ONLINE, codeConfirmation);
    }
    
    /**
     * <p>
     * M�todo que valida la clave ingresada por el usuario cuando el metodo de autenticacion corresponde a 
     * Entrust Soft Token en modo Offline.
     * </p>
     * 
     * Registro de versiones:
     * <ul>
     * <li>1.0 25/09/2017, Marcelo Fuentes (SEnTRA) - Ricardo Carrasco (Ing. Soft. BCI): Versi�n Inicial.</li>
     * </ul>
     * <p>
     * 
     * @param mapping representa el mapeo de una instancia del formulario.
     * @param actionForm ActionForm asociado al ActionMapping.
     * @param request la solicitud HTTP que se esta procesando.
     * @param response la respuesta entregada.
     * @return Env�a los datos a la p�gina de respuesta.
     * @since 1.10
     */
    public ActionForward validaEntrustSoftTokenOffline(ActionMapping mapping, ActionForm actionForm, HttpServletRequest request,
            HttpServletResponse response) {
        getLogger().info("[validaEntrustSoftTokenOffline] Entro");
        
        HttpSession hs = request.getSession(false);
        PagueDirectoForm pagueDirectoForm = (PagueDirectoForm) actionForm;
        
        String codeConfirmation = (String) pagueDirectoForm.getCodigoConfirmacionQR();
        return validaEntrustSoftToken(mapping, actionForm, request, response, OFFLINE, codeConfirmation);
    }
    
    /**
     * <p>
     * M�todo que se encarga de validar la clave ingresada por el usuario contra el servicio de Entrust.
     * </p>
     * 
     * Registro de versiones:
     * <ul>
     * <li>1.0 25/09/2017, Marcelo Fuentes (SEnTRA) - Ricardo Carrasco (Ing. Soft. BCI): Versi�n Inicial.</li>
     * </ul>
     * <p>
     * 
     * @param mapping representa el mapeo de una instancia del formulario.
     * @param actionForm ActionForm asociado al ActionMapping.
     * @param request la solicitud HTTP que se esta procesando.
     * @param response la respuesta entregada.
     * @param modoAutenticacion El modo de autenticacion: ONLINE u OFFLINE.
     * @param codeConfirmation La clave a validar.
     * @return Env�a los datos a la p�gina de respuesta.
     * @since 1.10
     */
    private ActionForward validaEntrustSoftToken(ActionMapping mapping, ActionForm actionForm, HttpServletRequest request,
            HttpServletResponse response, String modoAutenticacion, String codeConfirmation) {
        
        getLogger().info("[validaEntrustSoftToken][BCI_INI] iniciado.");
        
        HttpSession hs = request.getSession(false);
        SessionBCI sessionBci = (SessionBCI)hs.getAttribute(ValidaSesion.SESSION_BCI);
        
        PagueDirectoTO pagueDirecto = (PagueDirectoTO) hs.getAttribute(OBJETO_PAGUE_DIRECTO);
        PagueDirectoForm pagueDirectoForm = (PagueDirectoForm) actionForm;
        
        try {
            
            String servicio = sessionBci.getParametrosEstrategiaSegundaClaveTO().getServicio();
            if (getLogger().isEnabledFor(Level.DEBUG)) {
                getLogger().debug("[asignaMetodosAutenticacion] Se agrega parametro para EntrustSoftToken");
            }
            
            CamposDeLlaveTO campoDeLlaves = (CamposDeLlaveTO)sessionBci.getAttrib("camposDeLLavePD");
            sessionBci.removeAttrib("camposDeLLavePD");
            boolean resultado = ControllerBCI.verificarAutenticacion(ENTRUST_SOFTTOKEN, 
                    modoAutenticacion, sessionBci.getParametrosEstrategiaSegundaClaveTO().getUsuarioSegundaClave(), campoDeLlaves, servicio, codeConfirmation);
            
            if(getLogger().isDebugEnabled()){
                getLogger().debug("[validaEntrustSoftToken]resultado: " + resultado);
            }
            
            JournalizadorHelper.journaliza(sessionBci, EVE_LOGIN, SUB_COD_OK,
                    String.valueOf(sessionBci.getCliente().getRut()), ID_PRODENTRUSTSOFTTOKEN, null, modoAutenticacion, EST_EVENTO_NEGOCIO_PRO);
        } 
        catch (SeguridadException e) {
            if(getLogger().isEnabledFor(Level.ERROR)){
                getLogger().error("[validaEntrustSoftToken][BCI_FINEX] SeguridadException", e);
            }
            JournalizadorHelper.journaliza(sessionBci, EVE_LOGIN, SUB_COD_NOK,
                    String.valueOf(sessionBci.getCliente().getRut()), ID_PRODENTRUSTSOFTTOKEN, null, modoAutenticacion, EST_EVENTO_NEGOCIO_REC);
            guardarError("error.seguridad", new String[] {e.getSimpleMessage() }, request);
            sessionBci.cerrar();
            return mapping.findForward("error");
            
        }
        catch (Exception e) {
            if(getLogger().isEnabledFor(Level.ERROR)){
                getLogger().error("[validaEntrustSoftToken][BCI_FINEX] Exception", e);
            }
            JournalizadorHelper.journaliza(sessionBci, EVE_LOGIN, SUB_COD_NOK,
                    String.valueOf(sessionBci.getCliente().getRut()), ID_PRODENTRUSTSOFTTOKEN, null, modoAutenticacion, EST_EVENTO_NEGOCIO_REC);
            guardarError("error.seguridad", new String[] {e.getMessage() }, request);
            sessionBci.cerrar();
            return mapping.findForward("error");
            
        }
        
        request.removeAttribute("flagEntrustSoftToken");
        sessionBci.cambiaEstadoMetodoAutenticacion(ID_PRODENTRUSTSOFTTOKEN, true);
        hs.setAttribute("sessionBci", sessionBci);
        if(getLogger().isDebugEnabled()){
            getLogger().debug("[validaEntrustSoftToken] metodos: " + StringUtil.contenidoDe(sessionBci.getMetodosAutenticacion()));
        }
        try {
            if (pagueDirecto.getDatosPagador().isDebeActuarComoEmpresa()) {
                PagueDirectoBusinessDelegateImpl delegate = new PagueDirectoBusinessDelegateImpl();
                ApoderadoTO apoderado = delegate.getApoderadoEmpresa(
                        new Long(pagueDirectoForm.getRutApoderado()),
                        new Long(pagueDirectoForm.getRutUsuario()),
                        pagueDirecto.getDatosConvenio().getTipoConvenio());
                
                pagueDirecto.getDatosPagador().setApoderado(apoderado);
                pagueDirectoForm.setTipoConvenioBEL(apoderado.getTipoConvenioApoderado());
                
                if (apoderado != null && apoderado.getCodigoConvenioBel() != null) {
                    pagueDirectoForm.setDobleFirma(true);
                }
            }
            pagueDirectoForm.setNombreUsuario(pagueDirecto.getDatosPagador().getNombre());
            pagueDirectoForm.setProductos(pagueDirecto.getProducto());
            pagueDirectoForm.setCantidad(new BigInteger(pagueDirecto.getCantidadProductos().toString()));
            pagueDirectoForm.setFechaActual(new Date());
            pagueDirectoForm.setTransaccion(pagueDirecto.getNumeroTransaccion());
            
            hs.setAttribute(OBJETO_PAGUE_DIRECTO, pagueDirecto);
            
            registrarCompraPagueDirecto(pagueDirecto);
            
            if (getLogger().isEnabledFor(Level.INFO)){
                getLogger().info("[validaEntrustSoftToken][BCI_FINOK].");
            }
            
            return mapping.findForward("pago");
        }
        catch (PagueDirectoException pdex) {
            if(getLogger().isEnabledFor(Level.ERROR)){
                getLogger().error("[validaEntrustSoftToken][BCI_FINEX] PagueDirectoException", pdex);
            }
            guardarError("error.pagueDirecto", new String[] { pdex.getSimpleMessage() }, request);
            return mapping.findForward("error");
        }
        
    }
    
    /**
     * <p>
     * M�todo que genera la llave QR y redirecciona a la pagina para presentacion al usuario.
     * </p>
     * 
     * Registro de versiones:
     * <ul>
     * <li>1.0 25/09/2017, Marcelo Fuentes (SEnTRA) - Ricardo Carrasco (Ing. Soft. BCI): Versi�n Inicial.</li>
     * </ul>
     * <p>
     * 
     * @param mapping representa el mapeo de una instancia del formulario.
     * @param actionForm ActionForm asociado al ActionMapping.
     * @param request la solicitud HTTP que se esta procesando.
     * @param response la respuesta entregada.
     * @return Env�a los datos a la p�gina de respuesta.
     * @since 1.10
     */
    public ActionForward generarLlaveQR(ActionMapping mapping, ActionForm actionForm, HttpServletRequest request,
            HttpServletResponse response) {
        if (getLogger().isEnabledFor(Level.INFO)){
            getLogger().info("[generarLlaveQR][BCI_INI] iniciado.");
        }
        HttpSession hs = request.getSession(false);
        SessionBCI sessionBci = (SessionBCI)hs.getAttribute(ValidaSesion.SESSION_BCI);
        
        PagueDirectoTO pagueDirecto = (PagueDirectoTO) hs.getAttribute(OBJETO_PAGUE_DIRECTO);
        PagueDirectoForm pagueDirectoForm = (PagueDirectoForm) actionForm;
        
        try {
            
            String servicio = sessionBci.getParametrosEstrategiaSegundaClaveTO().getServicio();
            
            CamposDeLlaveTO campoDeLlaves = obtenerCamposDellaves(request, pagueDirectoForm);
            LlaveSegundaClaveTO llavesQREntrust = ControllerBCI.generarLlave(ENTRUST_SOFTTOKEN, 
                                                                            servicio, 
                                                                            sessionBci.getParametrosEstrategiaSegundaClaveTO().getUsuarioSegundaClave(), 
                                                                            campoDeLlaves, 
                                                                            OFFLINE);
            
            if (getLogger().isDebugEnabled()) {
                getLogger().debug("[generarLlaveQR] vuelta largo total  " + llavesQREntrust.getLlaves().length);
            }
            
            getLogger().debug("[generarLlaveQR] Seteando la llave");
            
            sessionBci.setAttrib("camposDeLLavePD", campoDeLlaves);
            request.setAttribute("servicioQR", SERVICIO); 
            request.setAttribute("imagenesqr", llavesQREntrust.getLlaves());
           
           getLogger().debug("[generarLlaveQR] [BCI_FINOK]");
           
           return mapping.findForward("entrustSoftTokenOffline");
           
        } catch (Exception e) {
            if(getLogger().isEnabledFor(Level.ERROR)){
                getLogger().error("[generarLlaveQR][BCI_FINEX] PagueDirectoException", e);
            }
            guardarError("error.pagueDirecto", new String[] { e.getMessage() }, request);
            return mapping.findForward("error");
        }
        
    }
    
    /**
     * <p>
     * M�todo que obtiene los datos para las transacciones bancarias que operan con segunda clave.
     * </p>
     * 
     * Registro de versiones:
     * <ul>
     * <li>1.0 25/09/2017, Marcelo Fuentes (SEnTRA) - Ricardo Carrasco (Ing. Soft. BCI): Versi�n Inicial.</li>
     * </ul>
     * <p>
     * 
     * @param request la solicitud HTTP que se esta procesando.
     * @param pagueDirectoForm la respuesta entregada.
     * @return Objeto con los datos para las transacciones bancarias que operan con segunda clave.
     * @throws PagueDirectoException en caso de error.
     * @since 1.10
     */
    private CamposDeLlaveTO obtenerCamposDellaves(HttpServletRequest request, PagueDirectoForm pagueDirectoForm) 
            throws PagueDirectoException {
        HttpSession hs = request.getSession(false);
        PagueDirectoTO pagueDirecto = (PagueDirectoTO) hs.getAttribute(OBJETO_PAGUE_DIRECTO);
        DatosConvenioTO convenio = pagueDirecto.getDatosConvenio();
        
        String canalTemp = PagueDirectoParametros.CANAL_ID + obtenerCanal(request.getServletPath());
        Canal canal = new Canal(canalTemp);
        
        DecimalFormat df = new DecimalFormat();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMddHHmm");
        
        String fechaActual = simpleDateFormat.format(new Date());
        
        String montoTotal = df.format(pagueDirectoForm.getTotal());
        String  nombresDeLLave = TablaValores.getValor(TABLA_SEGURIDAD, "Llave_" + SERVICIO, "datosLlave");
        String[] valoresDeLlave = {canal.getNombre(),fechaActual,montoTotal, StringUtils.trimToEmpty(convenio.getEmpresa())};
        
        CamposDeLlaveTO campoDeLlaves = new CamposDeLlaveTO();
        SafeSignerUtil safeSignerUtil = new SafeSignerUtil();
        campoDeLlaves = safeSignerUtil.seteaCamposDeLlave(nombresDeLLave, valoresDeLlave);
        return campoDeLlaves;
    }
    
}