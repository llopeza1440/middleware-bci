package cl.bci.aplicaciones.seguridad.administracion.web.entrust.ws;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import wcorp.bprocess.tcreditoonline.TCreditoOnline;
import wcorp.bprocess.tcreditoonline.TCreditoOnlineHome;
import wcorp.bprocess.tcreditoonline.vo.ResultadoServicioCuentasVO;
import wcorp.model.actores.Cliente;
import wcorp.model.actores.DireccionClienteBci;
import wcorp.model.seguridad.AutenticacionEnrolamientoTO;
import wcorp.model.seguridad.EstrategiaEntrustSoftToken;
import wcorp.model.seguridad.ParametrosEstrategiaSegundaClaveTO;
import wcorp.model.seguridad.UsuarioSegundaClaveTO;
import wcorp.model.seguridad.to.EstadoActivacionTokenTO;
import wcorp.model.seguridad.to.TokenEntrustTO;
import wcorp.serv.clientes.DatosBasicosCliente;
import wcorp.serv.clientes.Operacion;
import wcorp.serv.clientes.ServiciosCliente;
import wcorp.serv.clientes.ServiciosClienteHome;
import wcorp.serv.clientes.ServiciosClienteDelegate;
import wcorp.serv.seguridad.DatosPin;
import wcorp.serv.seguridad.SeguridadException;
import wcorp.serv.seguridad.ServiciosSeguridad;
import wcorp.serv.seguridad.ServiciosSeguridadHome;
import wcorp.serv.tarjetas.DetalleTarjeta;
import wcorp.serv.tarjetas.ServiciosTarjetas;
import wcorp.serv.tarjetas.ServiciosTarjetasHome;
import wcorp.serv.tarjetas.TarjetaCliente;
import wcorp.serv.tarjetas.TarjetasException;
import wcorp.util.EnhancedServiceLocator;
import wcorp.util.FechasUtil;
import wcorp.util.GeneralException;
import wcorp.util.StringUtil;
import wcorp.util.TablaValores;
import wcorp.util.com.TuxedoException;

import cl.bci.aplicaciones.cliente.mb.ClienteMB;
import cl.bci.aplicaciones.seguridad.autenticacion.to.ErrorEnrolamientoEnum;
import cl.bci.aplicaciones.seguridad.autenticacion.ws.to.EstadoSegundaClaveWSTO;
import cl.bci.aplicaciones.seguridad.autenticacion.ws.to.ResultadoEnrolamientoWSTO;
import cl.bci.infraestructura.web.journal.Journalist;
import cl.bci.infraestructura.web.portal.util.Dominio;
import cl.bci.infraestructura.web.seguridad.mb.SesionMB;
import cl.bci.infraestructura.web.seguridad.segundaclave.to.ResultadoOperacionSegundaClaveTO;
import cl.bci.infraestructura.web.seguridad.segundaclave.ws.to.DetalleTarjetaWSTO;
import cl.bci.infraestructura.web.seguridad.segundaclave.ws.to.EstadoActivacionTokenWSTO;
import cl.bci.infraestructura.web.seguridad.segundaclave.ws.to.OtpEntrustWSTO;
import cl.bci.infraestructura.web.seguridad.segundaclave.ws.to.ResponseStatusWSTO;
import cl.bci.infraestructura.web.seguridad.util.ConectorStruts;
import cl.bci.infraestructura.ws.ServicioRestWS;
import cl.bci.infraestructura.ws.excepcion.NegocioException;
import cl.bci.infraestructura.ws.excepcion.SistemaException;
import cl.bci.infraestructura.ws.util.MessageBundleUtil;
import cl.bci.mensajeria.sdp.conector.ConectorServicioEnvioSDP;
import cl.bci.mensajeria.sdp.sms.dto.MensajeLatiniaDTO;

/**
 * @NombreServicio Servicio que permite el enrolamiento mediante entrust.
 * 
 * @DescripcionServicio Permite enrolar,bloquear,desbloquear y desenrolar dispositivos mediante entrust.
 * 
 * @RegistroVersiones
 *   - 1.0 | 10/07/2017 | Sergio Flores | Ricardo Carrasco C�ceres | SEnTRA | Version Inicial.
 *   - 1.1 | 10/10/2017 | Sergio Flores | Ricardo Carrasco C�ceres | SEnTRA | Se modifica m�todo para poder obtener codigo otp en el caso que no existe cliente {@link #obtenerOTP()}.
 * <b>Todos los derechos reservados por Banco de Cr�dito e Inversiones.</b>
 */

@Path("/enrolamientoEntrustWS/")
public class EnrolamientoEntrustWS extends ServicioRestWS {

	/**
	 * Caracter censurador tarjetas de debito. 
	 */
	private static final char CARACTER_CENSURADOR_TDD = ' ';

	/**
	 * Cantidad de caracteres libres de censura.
	 */
    private static final int CANTIDAD_CARACTERES_LIBRES = 4;


	/**
	 *  bundle por defecto. 
	 */
	private static final String BUNDLE_DEFAULT = "cl.bci.aplicaciones.seguridad.administracion.web.entrust.entrust";
	
	
	/** 
	 * Constante Serial Version. 
	 */
	private static final long serialVersionUID = 1L;
	
	/**
     * tabla de par�metros de entrust.
     */
    private static final String TABLA_DE_PARAMETROS = "entrust.parametros";
	
    /**
     * Atributo que indica el tipo Titular al momento de consultar tarjetas de debito.
     */
    private static final char INDICADOR = 'T';
    
    /**
     * Constante que tiene el codigo interno para la direcci�n de celular.
     */
    private static final char CODIGO_DIRECCION_CELULAR = '8';
    
    /**
     * Constante que tiene el tipo de codigo para autenticar desafio.
     */
    private static final String TIPO_OTP = "OTP";
    /**
     * Usuario para consulta.
     */
    private static final String USUARIO = "0001";
    
    /**
     *Constante que describe el tipo de direccion.
     */
    private static final String TIPO_DIRECCION = "7";
    
    /**
     *Constante que describe el tipo de error 600.
     */
    private static final int CODE_600_ERROR = 600;
    
    /**
     * Valor cliente en caso de no encontarlo.
     */
    private static final String NO_SE_OBTUVO_CLIENTEMB = "No se encontr� rut de cliente";
    
    /**
     * Valor de producto para jornalizar.
     */
    private static final String PRODUCTO = TablaValores.getValor(TABLA_DE_PARAMETROS, "Journal", "ProductoStk");
    
    /**
     * Valor de codigo de evento para jornalizar.
     */
    private static final String COD_EVENTO = TablaValores.getValor(TABLA_DE_PARAMETROS, "Journal", "codEventoActivar");
    
    
    /**
     * Variable representa la posici�n inicial para el medio.
     */
    private static final  int POSICION_INICIAL_ID_MEDIO = 0;

    /**
     * Variable representa el largo del medio.
     */
    private static final  int LARGO_ID_MEDIO = 15;
    
    /**
     * valor para validar tarjeta tdd.
     */
    private static final int CUATRO_DIGITOS_TDD = 4;
    
    /**
     * Constante que tiene el estado activo de tdd.
     */
    private static final String ESTADO_TDD_ACTIVA = "003";
    
    /**
     * Constante que tiene descripcion sin dispositos.
     */
    private static final String JOURNAL_SIN_DISPO = "SIN DISPOSITIVOS";
    
    /**
     * Constante que tiene el estado de la ctacte Activa.
     */
    private static final String ESTADO_CTACTE_ACTIVA = "VIG";
    
    /**
     * Constante que tiene el estado de la ctacte Eliminada.
     */
    private static final String ESTADO_CTACTE_ELIMINADA = "ELI";
    
    /**
     * Constante que tiene el estado de la ctacte Eliminada.
     */
    private static final String TIPO_OPERACION_CCT = "CCT";
    
    /**
     *  Atributo que indica un error en el envio de email.
     */
    private static final int ESTADO_NOK_ENVIO_EMAIL = 1;
    
    /**
     * tabla de par�metros jndi.
     */
    private static final String TABLA_DE_PARAMETROS_JNDI = "JNDIConfig.parametros";
    
    /**
     * C�digo indicador de operacion vigente.
     */
    private static final String OPERACION_VIGENTE = "VIG";

    /**
     * C�digo indicador de operacion del tipo cuenta corriente.
     */
    private static final String OPERACION_CUENTA_CORRIENTE = "CCT";

    /**
     * C�digo indicador de operacion del tipo cuenta prima.
     */
    private static final String OPERACION_CUENTA_PRIMA = "CPR";
    
	 /**
     * Variable para asignar log de ejecuci�n.
     */
    private transient Logger logger = (Logger) Logger.getLogger(EnrolamientoEntrustWS.class);
    
    /**
     * Representa el celular de un cliente.
     */
    private String celularCliente;
    
	/**
	 * Atributo para validar el datoUnico de cliente.
	 */
	private String datoUnicoLog;
	
	/**
     * Servicio de clientes.
     */
    private ServiciosCliente serviciosCliente;
    
    /**
     * Servicio de TCredito.
     */
    private TCreditoOnline tCreditoOnline;
	
	/**
	 * @DescripcionMetodo Permite obtener el listado de dispositivos enrolados con entrust.
	 * 
	 * @EjemploRequest
	 *  
	 * @EjemploResponse
	 * {
     *  "status": false,
     *  "nombre": null,
     *  "requiereLlave": true,
     *  "glosa": null,
     *   "listaDispositivos": [
     *    {
     *       "codigoRespuestaWs": 0,
     *       "descripcionRespuestaWs": null,
     *       "estado": "HOLD",
     *       "estadoDespliegue": "BLOQUEADO",
     *       "serialNumber": "47356-86596",
     *       "fechaModificacion": null,
     *       "tipoDispositivo": "STK",
     *       "nombreDispositivo": "SFTRXcreated0",
     *       "usuarioSegundaClave": null,
     *       "grupoToken": "BCI"
     *     }
     *   ]
     * }
	 *
	 * @CodigosRespuesta
	 *  - 200: respuesta OK
	 *  - 600: Error gen�rico, ver logs para m�s detalles.
	 *  - 500: Ha ocurrido un error de sistema.
	 *
	 * @RegistroVersiones
	 *
	 *   - 1.0 | 10/07/2017 | Sergio Flores | Ricardo Carrasco C�ceres | SEnTRA | Version Inicial.
	 *
	 * @throws NegocioException Excepci�n espec�fica de negocio.
	 * @return EstadoSegundaClaveWSTO que posee los datos de la consulta de estado.
	 */

	@POST
	@Path("consultarEstadoSegundaClave")
	@Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
     public EstadoSegundaClaveWSTO consultarEstadoSegundaClave() throws NegocioException{

       if (getLogger().isEnabledFor(Level.INFO)) {
		   getLogger().info("[consultarEstadoSegundaClave] [" + datoUnicoCliente() + "] [BCI_INI]");
	   }
       wcorp.model.seguridad.EstadoSegundaClaveTO estadoSegundaClaveTO = null;
       String codigoEvento = null;
       try {
    	    
    	    SesionMB sesionMB = (SesionMB) getManagedBean("sesionMB");
    	    ClienteMB clienteMB = (ClienteMB) getManagedBean("clienteMB");
    	    codigoEvento = TablaValores.getValor(TABLA_DE_PARAMETROS, "Journal", "codEventoConsulta");
    	    EstrategiaEntrustSoftToken estretegia = new EstrategiaEntrustSoftToken();
    	    
    	    String canal = sesionMB.getCanalId() != null ? sesionMB.getCanalId() : "";
        	String userId = clienteMB.getFullRut();
        	
        	if (getLogger().isEnabledFor(Level.DEBUG)) {
                 getLogger().debug("[consultarEstadoSegundaClave] [" + userId +"], canal: " + canal);
            } 
           
            UsuarioSegundaClaveTO usuarioSegundaClave = 
            		new UsuarioSegundaClaveTO(userId, Dominio.obtieneDominio(canal).toString(),canal);
            
            ParametrosEstrategiaSegundaClaveTO parametrosEstrategiaSegundaClaveTO = new ParametrosEstrategiaSegundaClaveTO(
            		usuarioSegundaClave);

            estadoSegundaClaveTO = estretegia.consultarEstadoSegundaClave(parametrosEstrategiaSegundaClaveTO);
            
            
            if (getLogger().isEnabledFor(Level.DEBUG)) {
                getLogger().debug("[consultarEstadoSegundaClave] [" + userId +"], estadoSegundaClaveTO: " + StringUtil.contenidoDe(estadoSegundaClaveTO));
            } 
            
             
            String valorDispositivo=null;
            
            if(estadoSegundaClaveTO.getListaDispositivos()!=null) {
            	valorDispositivo = PRODUCTO;
            } else {
            	valorDispositivo = JOURNAL_SIN_DISPO;
            }
            
            journalizarEnrolarEntrust(PRODUCTO, 
            		codigoEvento, 
            		TablaValores.getValor(TABLA_DE_PARAMETROS, "Journal", "SubCodEventoOK"), 
            		TablaValores.getValor(TABLA_DE_PARAMETROS, "Journal", "estadoOK") , 
            		valorDispositivo); 
            
            if (getLogger().isEnabledFor(Level.INFO)) {
      			getLogger().info("[consultarEstadoSegundaClave][" + userId +"][BCI_FINOK]");
      		}
            
            return traspasarEstadoSegundaClave(estadoSegundaClaveTO);
           
          }
       catch (NegocioException ex) {
           if (getLogger().isEnabledFor(Level.ERROR)) {
               getLogger().error("[consultarEstadoSegundaClave][BCI_FINEX][NegocioException] error con mensaje=<" + ex.getMessage()+">", ex);
           }
		
           journalizarEnrolarEntrust(PRODUCTO, 
        		   codigoEvento, 
    			   TablaValores.getValor(TABLA_DE_PARAMETROS, "Journal", "SubCodEventoNOK"), 
    			   TablaValores.getValor(TABLA_DE_PARAMETROS, "Journal", "estadoNOK") ,
    			   StringUtil.contenidoDe(estadoSegundaClaveTO.getListaDispositivos())); 
           
           throw ex;
       }
       catch (Exception ex) {
    	   if (getLogger().isEnabledFor(Level.ERROR)) {
               getLogger().error("[consultarEstadoSegundaClave][BCI_FINEX][Exception] error con mensaje=<" + ex.getMessage()+">", ex);
           }
    	   journalizarEnrolarEntrust(PRODUCTO, 
    			   codigoEvento, 
    			   TablaValores.getValor(TABLA_DE_PARAMETROS, "Journal", "SubCodEventoError"), 
    			   TablaValores.getValor(TABLA_DE_PARAMETROS, "Journal", "estError") ,
    			   StringUtil.contenidoDe(estadoSegundaClaveTO.getListaDispositivos())); 
           throw new SistemaException(MessageBundleUtil.getMensajeBundle(BUNDLE_DEFAULT, "ERROR_DE_SISTEMA"));
       }
    }
	
 
	/**
	 * Metodo que pasa los estados segunda clave a EstadoSegundaClaveWSTO.
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	  * <li>1.0 10/07/2017 Sergio Flores (SEnTRA) - Ricardo Carrasco C�ceres (Ing. Soft. BCI): Versi�n Inicial.</li>
	 * </ul>
	 * <p>
	 * 
	 * @param retorno EstadoSegundaClaveTO que contiene las cuentas a ser transapasadas.
	 * @throws NegocioException Excepci�n espec�fica de negocio.
	 * @throws SistemaException  Excepci�n de Sistema.
	 * @throws Exception Error al traspasar estado segunda clave.
	 * @return EstadoSegundaClaveWSTO resultado del servicio consulta de estados para entrust.
	 * @since 1.0
	 */
	private EstadoSegundaClaveWSTO traspasarEstadoSegundaClave(wcorp.model.seguridad.EstadoSegundaClaveTO retorno) 
			throws NegocioException, SistemaException, Exception {
		if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[traspasarEstadoSegundaClave][" + datoUnicoCliente() + "][BCI_INI]");
		}

		try {
			ClienteMB clienteMB = (ClienteMB) getManagedBean("clienteMB");
        	
        	if (getLogger().isEnabledFor(Level.DEBUG)) {
                 getLogger().debug("[traspasarEstadoSegundaClave] [" + datoUnicoCliente() +"][" + retorno + "]");
            } 

			if( retorno!= null) {
				EstadoSegundaClaveWSTO respuesta;
				respuesta = new EstadoSegundaClaveWSTO();
				respuesta.setStatus(retorno.isEstatus());
				respuesta.setRequiereLlave(Boolean.TRUE);

				if (retorno.getListaDispositivos()!=null && retorno.getListaDispositivos().length > 0) {
					TokenEntrustTO[] listaDispositivos = retorno.getListaDispositivos();

					cl.bci.infraestructura.web.seguridad.segundaclave.ws.to.TokenEntrustTOWSTO tokenRest = null;
					ArrayList listaRestDispositivos = new ArrayList();

					int indice = 0;
					for (; indice < listaDispositivos.length; indice++) {

						TokenEntrustTO tokenWcorp = listaDispositivos[indice];
						tokenRest = new cl.bci.infraestructura.web.seguridad.segundaclave.ws.to.TokenEntrustTOWSTO();

						tokenRest.setCodigoRespuestaWs(tokenWcorp.getCodigoRespuestaWs());
						tokenRest.setDescripcionRespuestaWs(tokenWcorp.getDescripcionRespuestaWs());
						tokenRest.setEstado(tokenWcorp.getEstado());

						String estadoDespliegue = TablaValores.getValor(TABLA_DE_PARAMETROS, tokenWcorp.getEstado(), "desc");
						tokenRest.setEstadoDespliegue(estadoDespliegue);

						tokenRest.setFechaModificacion(tokenWcorp.getFechaModificacion());
						tokenRest.setGrupoToken(tokenWcorp.getGrupoToken());
						tokenRest.setNombreDispositivo(tokenWcorp.getNombreDispositivo());
						tokenRest.setSerialNumber(tokenWcorp.getSerialNumber());
						tokenRest.setTipoDispositivo(tokenWcorp.getTipoDispositivo());

						listaRestDispositivos.add(tokenRest);
					}

					respuesta.setListaDispositivos((cl.bci.infraestructura.web.seguridad.segundaclave.ws.to.TokenEntrustTOWSTO[]) 
							listaRestDispositivos.toArray(new cl.bci.infraestructura.web.seguridad.segundaclave.ws.to.TokenEntrustTOWSTO[0]));                	  
				}
				
				 if (getLogger().isEnabledFor(Level.INFO)) {
		     			getLogger().info("[traspasarEstadoSegundaClave][" +  datoUnicoCliente() +"][BCI_FINOK]");
		     	 }

				return respuesta;
			}
			else {
				throw new NegocioException(MessageBundleUtil.getMensajeBundle(BUNDLE_DEFAULT, "SIN_DISPOSITIVOS"));
			}
		}
		catch (NegocioException ex) {
	           if (getLogger().isEnabledFor(Level.ERROR)) {
	               getLogger().error("[traspasarEstadoSegundaClave][BCI_FINEX][NegocioException] error con mensaje=<" + ex.getMessage()+">", ex);
	           }
			

	           throw ex;
	       }
	       catch (Exception ex) {
	    	   if (getLogger().isEnabledFor(Level.ERROR)) {
	               getLogger().error("[traspasarEstadoSegundaClave][BCI_FINEX][Exception] error con mensaje=<" + ex.getMessage()+">", ex);
	           }
	           throw new SistemaException(MessageBundleUtil.getMensajeBundle(BUNDLE_DEFAULT, "ERROR_DE_SISTEMA"));
	       }

	}
	
	/**
	 * @DescripcionMetodo M�todo permite activar un cliente en IDG y curus.
	 * 
	 * @EjemploRequest
	 *  
	 * @EjemploResponse
	 * {
     *  {
	 *    "activationCode":"data:image/png;"
	 *    "serialNumber":"92460-20847",
	 *     "resultado":{
	 *			        "code":0,
	 *			        "description":"Accion realizada exitosamente",
	 *			        "resultadoTandem":false
	 *			} 
     * }
	 *
	 * @CodigosRespuesta
	 *  - 200: respuesta OK
	 *  - 600: Error gen�rico, ver logs para m�s detalles.
	 *  - 500: Ha ocurrido un error de sistema.
	 *
	 * @RegistroVersiones
	 *
	 *   - 1.0 | 10/07/2017 | Sergio Flores | Ricardo Carrasco C�ceres | SEnTRA | Version Inicial.
	 *
	 * @throws NegocioException Excepci�n espec�fica de negocio.
	 * @return EstadoActivacionTokenWSTO resultado del servicio activarToken.
	 */
	@POST
	@Path("activarSoftTokenUsuario")
	@Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    public EstadoActivacionTokenWSTO activarSoftTokenUsuario() throws NegocioException {

       if (getLogger().isEnabledFor(Level.INFO)) {
		   getLogger().info("[activarSoftTokenUsuario][" + datoUnicoCliente() + "][BCI_INI]");
	   }
       EstadoActivacionTokenTO estadoActivacionTokenTO = null;
       String codigoEvento = null;
       try {
    	    
        	SesionMB sesionMB = (SesionMB) getManagedBean("sesionMB");
     	    ClienteMB clienteMB = (ClienteMB) getManagedBean("clienteMB");
        	String canal = sesionMB.getCanalId() != null ? sesionMB.getCanalId() : "";
        	String userId = String.valueOf(clienteMB.getRut());
            
            EstrategiaEntrustSoftToken estretegia = new EstrategiaEntrustSoftToken();
            UsuarioSegundaClaveTO usuarioSegundaClave = 
            		new UsuarioSegundaClaveTO(userId, Dominio.obtieneDominio(canal).toString(), canal);
            
            usuarioSegundaClave.setNombreCliente(clienteMB.getCliente().getFullName());
            
            ParametrosEstrategiaSegundaClaveTO parametrosEstrategiaSegundaClaveTO = 
            		new ParametrosEstrategiaSegundaClaveTO(usuarioSegundaClave);
            parametrosEstrategiaSegundaClaveTO.setCliente(clienteMB.getCliente());

            estadoActivacionTokenTO = estretegia.activarSoftTokenUsuario(parametrosEstrategiaSegundaClaveTO);
            
            if (getLogger().isEnabledFor(Level.DEBUG)) {
	            getLogger().debug("[activarSoftTokenUsuario][" + userId +"][EstadoActivacionTokenTO][" + StringUtil.contenidoDe(estadoActivacionTokenTO) + "]");
	        } 
            
 
            codigoEvento = TablaValores.getValor(TABLA_DE_PARAMETROS, "Journal", "codEventoActivar");
            
            journalizarEnrolarEntrust(PRODUCTO, 
            		codigoEvento, 
            		TablaValores.getValor(TABLA_DE_PARAMETROS, "Journal", "SubCodEventoOK"), 
            		TablaValores.getValor(TABLA_DE_PARAMETROS, "Journal", "estadoOK") , 
            		estadoActivacionTokenTO.getSerialNumber()); 
            
            if (getLogger().isEnabledFor(Level.INFO)) {
     			getLogger().info("[activarSoftTokenUsuario][BCI_FINOK][" + datoUnicoCliente() +"]");
     		}
             
            return traspasarEstadoActivacionToken(estadoActivacionTokenTO);
        }
       catch (NegocioException ex) {
           if (getLogger().isEnabledFor(Level.ERROR)) {
               getLogger().error("[activarSoftTokenUsuario][BCI_FINEX][NegocioException] error con mensaje=<" + ex.getMessage()+">", ex);
           }
		
           journalizarEnrolarEntrust(PRODUCTO, 
        		   codigoEvento, 
    			   TablaValores.getValor(TABLA_DE_PARAMETROS, "Journal", "SubCodEventoNOK"), 
    			   TablaValores.getValor(TABLA_DE_PARAMETROS, "Journal", "estadoNOK") ,
    			   estadoActivacionTokenTO.getSerialNumber()); 
           throw ex;
       }
       catch (Exception ex) {
    	   if (getLogger().isEnabledFor(Level.ERROR)) {
               getLogger().error("[activarSoftTokenUsuario][BCI_FINEX][Exception] error con mensaje=<" + ex.getMessage()+">", ex);
           }
    	   
    	   journalizarEnrolarEntrust(PRODUCTO, 
    			   codigoEvento, 
    			   TablaValores.getValor(TABLA_DE_PARAMETROS, "Journal", "SubCodEventoError"), 
    			   TablaValores.getValor(TABLA_DE_PARAMETROS, "Journal", "estError") ,
    			   estadoActivacionTokenTO.getSerialNumber()); 
           throw new SistemaException(MessageBundleUtil.getMensajeBundle(BUNDLE_DEFAULT, "ERROR_DE_SISTEMA"));
       }
    }
	
	 /**
     * <p>
	 * Metodo que pasa los estados segunda clave a EstadoSegundaClaveWSTO.
	 * 
	 * <p>
	 * Registro de versiones:
	 * <ul>
	 * <li>1.0 10/07/2017 Sergio Flores (SEnTRA) - Ricardo Carrasco C�ceres (Ing. Soft. BCI): Versi�n Inicial.</li>
	 * </ul>
	 * 
	 * @param retorno que contiene las cuentas a ser transapasadas.
	 * @throws Exception error al traspasar estado de activacion token.
	 * @return EstadoActivacionTokenWSTO resultado del servicio activarToken.
	 * @since 1.0
	 */
	private EstadoActivacionTokenWSTO traspasarEstadoActivacionToken(EstadoActivacionTokenTO retorno) throws Exception{
		if (getLogger().isEnabledFor(Level.INFO)) {
		    getLogger().info("[traspasarEstadoActivacionToken][" + datoUnicoCliente() + "][BCI_INI][" + retorno + "]");
		}
		
		try {
    	      if(retorno!=null) {
    	    	  EstadoActivacionTokenWSTO respuesta = new EstadoActivacionTokenWSTO();
    	    	  
    	    	  if(retorno.getResultado() != null) {

    	    		  if (retorno.getResultado().getCode() == 0) {
    	    			  respuesta.setActivationCode(retorno.getActivationCode());
    	    			  respuesta.setSerialNumber(retorno.getSerialNumber());
        	    		  respuesta.setResultado(new ResponseStatusWSTO());
        	    		  respuesta.getResultado().setCode(retorno.getResultado().getCode());
        	    		  respuesta.getResultado().setDescription(retorno.getResultado().getDescription());
        	    		  respuesta.getResultado().setResultadoTandem(retorno.getResultado().isResultadoTandem());
    	    		  } 
    	    		  else if (retorno.getResultado().getCode() == CODE_600_ERROR) {
    	    			  throw new NegocioException(MessageBundleUtil.getMensajeBundle(BUNDLE_DEFAULT, "CODE_600_ERROR"));
    	    		  }
    	    		  else {
    	    			  throw new NegocioException(retorno.getResultado().getDescription());
    	    		  }
    	    	  }
    	    	  else {
    	    		  throw new NegocioException(MessageBundleUtil.getMensajeBundle(BUNDLE_DEFAULT, "ERROR_ACTIVACION_TOKEN"));
    	    	  }
    	    	  
    	    	  if (getLogger().isEnabledFor(Level.INFO)) {
		     			getLogger().info("[traspasarEstadoActivacionToken][" +  datoUnicoCliente() +"][BCI_FINOK]");
		     	  }
    	    	  return respuesta;
    	      }
    	      else {
                  throw new NegocioException(MessageBundleUtil.getMensajeBundle(BUNDLE_DEFAULT, "SIN_ACTIVACION_TOKEN"));
              }
          }
		catch (NegocioException ex) {
	           if (getLogger().isEnabledFor(Level.ERROR)) {
	               getLogger().error("[traspasarEstadoActivacionToken][BCI_FINEX][NegocioException] error con mensaje=<" + ex.getMessage()+">", ex);
	           }
			

	           throw ex;
	       }
	       catch (Exception ex) {
	    	   if (getLogger().isEnabledFor(Level.ERROR)) {
	               getLogger().error("[traspasarEstadoActivacionToken][BCI_FINEX][Exception] error con mensaje=<" + ex.getMessage()+">", ex);
	           }
	           throw new SistemaException(MessageBundleUtil.getMensajeBundle(BUNDLE_DEFAULT, "ERROR_DE_SISTEMA"));
	       }
		
	}
	
	/**
	 * @DescripcionMetodo M�todo permite desplegar en la vista enrolar si el cliente tiene TDDs.
	 * 
	 * @EjemploRequest
	 * 
	 * @EjemploResponse
	 * [
	 *   {
     *    "numeroTarjeta": "1131",
     *    "estado": "003",
     *    "numCuentaCorriente": "****************6276",
     *    "fechaInicio": "20090413000000",
     *    "fecVencimiento": "20291130230000"
     *   }
     * ]
	 *
	 * @CodigosRespuesta
	 *  - 200: respuesta OK
	 *  - 600: Error gen�rico, ver logs para m�s detalles.
	 *  - 500: Ha ocurrido un error de sistema.
	 *
	 * @RegistroVersiones
	 *
	 *   - 1.0 | 10/07/2017 | Sergio Flores | Ricardo Carrasco C�ceres | SEnTRA | Version Inicial.
	 *
	 * @throws NegocioException Excepci�n espec�fica de negocio.
	 * @return DetalleTarjetaWSTO[] resultado de arreglo de tarjetas para servicio rest.
	 */    
	@POST
	@Path("obtenerListaTDDEnrolar")
	@Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    public DetalleTarjetaWSTO[] obtenerListaTDDEnrolar() throws NegocioException {

       if (getLogger().isEnabledFor(Level.INFO)) {
		   getLogger().info("[obtenerListaTDDEnrolar][" + datoUnicoCliente() + "][BCI_INI]");
	   }
        
        
        try {
        	DetalleTarjeta[] detalleTarjeta = null;
        	SesionMB sesionMB = (SesionMB) getManagedBean("sesionMB");
     	    ClienteMB clienteMB = (ClienteMB) getManagedBean("clienteMB");
        	String canal = sesionMB.getCanalId() != null ? sesionMB.getCanalId() : "";
            long rutCliente = clienteMB.getRut();
           
            TarjetaCliente tarjetaCliente = obtieneTarjetasATM(rutCliente, INDICADOR, canal);            
            
            if (getLogger().isEnabledFor(Level.DEBUG)) {
                getLogger().debug("[obtenerListaTDDEnrolar] [" + datoUnicoCliente() +"], tarjetaCliente: " + StringUtil.contenidoDe(tarjetaCliente));
            } 
            
            detalleTarjeta = tarjetaCliente.getTarjeta();
            
            if (getLogger().isEnabledFor(Level.INFO)) {
    			getLogger().info("[obtenerListaTDDEnrolar][" + datoUnicoCliente() +"][BCI_FINOK]");
    		}
            
            return traspasarDetalleTarjetaEnrolar(detalleTarjeta); 
        }
        catch (NegocioException ex) {
	           if (getLogger().isEnabledFor(Level.ERROR)) {
	               getLogger().error("[obtenerListaTDDEnrolar][BCI_FINEX][NegocioException] error con mensaje=<" + ex.getMessage()+">", ex);
	           }
			

	           throw ex;
	       }
	    catch (Exception ex) {
	       if (getLogger().isEnabledFor(Level.ERROR)) {
	              getLogger().error("[obtenerListaTDDEnrolar][BCI_FINEX][Exception] error con mensaje=<" + ex.getMessage()+">", ex);
	       }
	          throw new SistemaException(MessageBundleUtil.getMensajeBundle(BUNDLE_DEFAULT, "ERROR_DE_SISTEMA"));
	    }
        
    }
	
	/**
	 * @DescripcionMetodo M�todo permite desplegar en la vista administrara si el cliente tiene TDDs.
	 * 
	 * @EjemploRequest
	 * 
	 * @EjemploResponse
	 * [
	 *   {
     *    "numeroTarjeta": "1131",
     *    "estado": "003",
     *    "numCuentaCorriente": "****************6276",
     *    "fechaInicio": "20090413000000",
     *    "fecVencimiento": "20291130230000"
     *   }
     * ]
	 *
	 * @CodigosRespuesta
	 *  - 200: respuesta OK
	 *  - 600: Error gen�rico, ver logs para m�s detalles.
	 *  - 500: Ha ocurrido un error de sistema.
	 *
	 * @RegistroVersiones
	 *
	 *   - 1.0 | 10/07/2017 | Sergio Flores | Ricardo Carrasco C�ceres | SEnTRA | Version Inicial.
	 *
	 * @throws NegocioException Excepci�n espec�fica de negocio.
	 * @return DetalleTarjetaWSTO[] resultado de arreglo de tarjetas para servicio rest.
	 */    
	@POST
	@Path("obtenerListaTDDAdministrar")
	@Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    public DetalleTarjetaWSTO[] obtenerListaTDDAdministrar() throws NegocioException {

       if (getLogger().isEnabledFor(Level.INFO)) {
		   getLogger().info("[obtenerListaTDDDAdministrar][" + datoUnicoCliente() + "][BCI_INI]");
	   }
        
        
        try {
        	DetalleTarjeta[] detalleTarjeta = null;
        	SesionMB sesionMB = (SesionMB) getManagedBean("sesionMB");
     	    ClienteMB clienteMB = (ClienteMB) getManagedBean("clienteMB");
        	String canal = sesionMB.getCanalId() != null ? sesionMB.getCanalId() : "";
            long rutCliente = clienteMB.getRut();
           
            TarjetaCliente tarjetaCliente = obtieneTarjetasATM(rutCliente, INDICADOR, canal);            
            
            if (getLogger().isEnabledFor(Level.DEBUG)) {
                getLogger().debug("[obtenerListaTDDDAdministrar] [" + datoUnicoCliente() +"], tarjetaCliente: " + StringUtil.contenidoDe(tarjetaCliente));
            } 
            
            detalleTarjeta = tarjetaCliente.getTarjeta();
            
            if (getLogger().isEnabledFor(Level.INFO)) {
    			getLogger().info("[obtenerListaTDDDAdministrar][" + datoUnicoCliente() +"][BCI_FINOK]");
    		}
            
            return traspasarDetalleTarjetaAdministrar(detalleTarjeta); 
        }
        catch (NegocioException ex) {
	           if (getLogger().isEnabledFor(Level.ERROR)) {
	               getLogger().error("[obtenerListaTDDDAdministrar][BCI_FINEX][NegocioException] error con mensaje=<" + ex.getMessage()+">", ex);
	           }
			

	           throw ex;
	       }
	    catch (Exception ex) {
	       if (getLogger().isEnabledFor(Level.ERROR)) {
	              getLogger().error("[obtenerListaTDDDAdministrar][BCI_FINEX][Exception] error con mensaje=<" + ex.getMessage()+">", ex);
	       }
	          throw new SistemaException(MessageBundleUtil.getMensajeBundle(BUNDLE_DEFAULT, "ERROR_DE_SISTEMA"));
	    }
        
    }
	
	/**
     * <p>
     * M�todo realiza el traspaso de datos desde el TO de middleware a WSTO.
     * </p>
     * 
     * Registro de versiones:
     * <ul>
     * <li>1.0 10/07/2017 Sergio Flores (SEnTRA) - Ricardo Carrasco C�ceres (Ing. Soft. BCI): Versi�n Inicial.</li>
     * </ul>
     *
     * @param retorno DetalleTarjeta que contiene las tarjetas.
     * @throws Exception error al traspasar detalle de tarjetas.
     * @return DetalleTarjetaWSTO[] detalle de la tarjeta.
     * @since 1.0
     */
	private DetalleTarjetaWSTO[] traspasarDetalleTarjetaEnrolar(DetalleTarjeta[] retorno) throws Exception{
		if (getLogger().isEnabledFor(Level.INFO)) {
		    getLogger().info("[traspasarDetalleTarjetaEnrolar][" + datoUnicoCliente() + "][BCI_INI] [" + retorno + "]");
		}
		
		try {
			
			ClienteMB clienteMB = (ClienteMB) getManagedBean("clienteMB");
			DetalleTarjetaWSTO[] respuesta = null;
			 
			boolean isMonoproducto = this.validarClienteMonoproducto( clienteMB.getRut(),  clienteMB.getDigitoVerif());
       	      if (getLogger().isEnabledFor(Level.DEBUG)) {
	                getLogger().debug("[traspasarDetalleTarjetaEnrolar] [" + datoUnicoCliente() +"], validarClienteMonoproducto "+isMonoproducto);
	          }
       	      
       	      if(isMonoproducto) {
       	    	 
					if (getLogger().isEnabledFor(Level.DEBUG)) {
		                getLogger().debug("[traspasarDetalleTarjetaEnrolar] [" + datoUnicoCliente() +"], enviarSmsOTP Monoproduco ");
		            } 
					enviarSmsOTP();
					
					respuesta = new DetalleTarjetaWSTO[0];
					return respuesta;
       	      }
			
       	      if (retorno != null && retorno.length > 0) {
				
				int canTddActivas = tieneTddActiva(retorno);
				
				if (getLogger().isEnabledFor(Level.DEBUG)) {
		                getLogger().debug("[traspasarDetalleTarjetaEnrolar] [" + datoUnicoCliente() +"], canTddActivas: " + canTddActivas);
		        } 
				 
				if(canTddActivas > 0) {
				  respuesta = new DetalleTarjetaWSTO[canTddActivas];
				  int i=0;
				  int j=0;
		          for(; i < retorno.length; i++) {
                	if(retorno[i].getEstado().equals(ESTADO_TDD_ACTIVA)) {
                          respuesta[j] = new DetalleTarjetaWSTO();
                          
                          respuesta[j].setNumeroTarjeta(StringUtil.censura(retorno[i].getNumeroTarjeta(), 
                    		    CARACTER_CENSURADOR_TDD, CANTIDAD_CARACTERES_LIBRES, false, false).trim());
                          respuesta[j].setNumCuentaCorriente(StringUtil.censura(retorno[i].getNumCuentaCorriente()));
                          respuesta[j].setEstado(retorno[i].getEstado());
                          respuesta[j].setFechaInicio(retorno[i].getFechaInicio());
                          respuesta[j].setFecVencimiento(retorno[i].getFecVencimiento());
                          j++;
                	}
                  }
                  
				}
				else if(canTddActivas == 0 && tieneCuentaCorriente(ESTADO_CTACTE_ACTIVA)) { 
					throw new NegocioException(MessageBundleUtil.getMensajeBundle(BUNDLE_DEFAULT, "TARJETA_DEBITO_BLOQUEADA"));
				}
                
                if (getLogger().isEnabledFor(Level.INFO)) {
        			getLogger().info("[traspasarDetalleTarjetaEnrolar][" + datoUnicoCliente() +"][BCI_FINOK]");
        		}
                return respuesta;
            }
            else {
            	 
            	  throw new NegocioException(MessageBundleUtil.getMensajeBundle(BUNDLE_DEFAULT, "SIN_TARJETAS_DEBITO"));
            }
          }
        catch (NegocioException ex) {
	           if (getLogger().isEnabledFor(Level.ERROR)) {
	               getLogger().error("[traspasarDetalleTarjetaEnrolar][BCI_FINEX][NegocioException] error con mensaje=<" + ex.getMessage()+">", ex);
	           }
			

	           throw ex;
	       }
	    catch (Exception ex) {
	       if (getLogger().isEnabledFor(Level.ERROR)) {
	              getLogger().error("[traspasarDetalleTarjetaEnrolar][BCI_FINEX][Exception] error con mensaje=<" + ex.getMessage()+">", ex);
	       }
	          throw new SistemaException(MessageBundleUtil.getMensajeBundle(BUNDLE_DEFAULT, "ERROR_DE_SISTEMA"));
	    }
		
	}
	
	/**
     * <p>
     * M�todo realiza el traspaso de datos desde el TO de middleware a WSTO.
     * </p>
     * 
     * Registro de versiones:
     * <ul>
     * <li>1.0 10/07/2017 Sergio Flores (SEnTRA) - Ricardo Carrasco C�ceres (Ing. Soft. BCI): Versi�n Inicial.</li>
     * </ul>
     *
     * @param retorno DetalleTarjeta que contiene las tarjetas.
     * @throws Exception error al traspasar detalle de tarjetas.
     * @return DetalleTarjetaWSTO[] detalle de la tarjeta.
     * @since 1.0
     */
	private DetalleTarjetaWSTO[] traspasarDetalleTarjetaAdministrar(DetalleTarjeta[] retorno) throws Exception{
		if (getLogger().isEnabledFor(Level.INFO)) {
		    getLogger().info("[traspasarDetalleTarjetaAdministrar][" + datoUnicoCliente() + "][BCI_INI] [" + retorno + "]");
		}
		
		try {
			ClienteMB clienteMB = (ClienteMB) getManagedBean("clienteMB");
			DetalleTarjetaWSTO[] respuesta = null;
			
			boolean isMonoproducto = this.validarClienteMonoproducto( clienteMB.getRut(),  clienteMB.getDigitoVerif());
     	      if (getLogger().isEnabledFor(Level.DEBUG)) {
	                getLogger().debug("[traspasarDetalleTarjetaAdministrar] [" + datoUnicoCliente() +"], validarClienteMonoproducto "+isMonoproducto);
	          }
     	      
     	      if(isMonoproducto) {
     	    	 
     	    	 if (getLogger().isEnabledFor(Level.DEBUG)) {
		                getLogger().debug("[traspasarDetalleTarjetaAdministrar] [" + datoUnicoCliente() +"], enviarTokenEmail Monoproduco ");
		            } 
					enviarTokenEmail();
					
					respuesta = new DetalleTarjetaWSTO[0];
					return respuesta;
     	    }
     	      
			if (retorno != null && retorno.length > 0) {
				
				int canTddActivas = tieneTddActiva(retorno);
				
				if (getLogger().isEnabledFor(Level.DEBUG)) {
		                getLogger().debug("[traspasarDetalleTarjetaAdministrar] [" + datoUnicoCliente() +"], canTddActivas: " + canTddActivas);
		        } 
				 
				if(canTddActivas > 0) {
				  respuesta = new DetalleTarjetaWSTO[canTddActivas];
				  int i=0;
				  int j=0;
		          for(; i < retorno.length; i++) {
                	if(retorno[i].getEstado().equals(ESTADO_TDD_ACTIVA)) {
                          respuesta[j] = new DetalleTarjetaWSTO();
                          respuesta[j].setNumeroTarjeta(StringUtil.censura(retorno[i].getNumeroTarjeta(), 
                    		    CARACTER_CENSURADOR_TDD, CANTIDAD_CARACTERES_LIBRES, false, false).trim());
                          respuesta[j].setNumCuentaCorriente(StringUtil.censura(retorno[i].getNumCuentaCorriente()));
                          respuesta[j].setEstado(retorno[i].getEstado());
                          respuesta[j].setFechaInicio(retorno[i].getFechaInicio());
                          respuesta[j].setFecVencimiento(retorno[i].getFecVencimiento());
                          j++;
                	}
                  }
                  
				}
				else if(canTddActivas == 0 && tieneCuentaCorriente(ESTADO_CTACTE_ACTIVA)) { 
					throw new NegocioException(MessageBundleUtil.getMensajeBundle(BUNDLE_DEFAULT, "TARJETA_DEBITO_BLOQUEADA"));
				}
                
                if (getLogger().isEnabledFor(Level.INFO)) {
        			getLogger().info("[traspasarDetalleTarjetaAdministrar][" + datoUnicoCliente() +"][BCI_FINOK]");
        		}
                return respuesta;
            }
            else {
            	  throw new NegocioException(MessageBundleUtil.getMensajeBundle(BUNDLE_DEFAULT, "SIN_TARJETAS_DEBITO"));
            }
          }
        catch (NegocioException ex) {
	           if (getLogger().isEnabledFor(Level.ERROR)) {
	               getLogger().error("[traspasarDetalleTarjetaAdministrar][BCI_FINEX][NegocioException] error con mensaje=<" + ex.getMessage()+">", ex);
	           }
			

	           throw ex;
	       }
	    catch (Exception ex) {
	       if (getLogger().isEnabledFor(Level.ERROR)) {
	              getLogger().error("[traspasarDetalleTarjetaAdministrar][BCI_FINEX][Exception] error con mensaje=<" + ex.getMessage()+">", ex);
	       }
	          throw new SistemaException(MessageBundleUtil.getMensajeBundle(BUNDLE_DEFAULT, "ERROR_DE_SISTEMA"));
	    }
		
	}
	
	 /**
     * M�todo que valida si el cliente tiene tarjetas de debito activas <br>
     * Registro de versiones:
     * <ul>
     * <li>1.0 10/07/2017 Sergio Flores (SEnTRA) - Ricardo Carrasco C�ceres (Ing. Soft. BCI): Versi�n Inicial.</li>
     * </ul>
     * @param retorno DetalleTarjeta[] que contiene todas las tarjetas de debito.
     * @return cantidadActiva int cantidad de tarjetas de debito activas.
     * @since 1.0
     */
	private int tieneTddActiva(DetalleTarjeta[] retorno) {
		if (getLogger().isEnabledFor(Level.INFO)) {
		    getLogger().info("[tieneTddActiva][" + datoUnicoCliente() + "][BCI_INI] [" + retorno + "]");
		}
		int cantidadActiva=0;
		try {
				
                for (int i = 0; i < retorno.length; i++) {
                	if(retorno[i].getEstado().equals(ESTADO_TDD_ACTIVA)) {
                		cantidadActiva++;
                	}
                }
                
                if (getLogger().isEnabledFor(Level.DEBUG)) {
                    getLogger().debug("[tieneTddActiva] [" + datoUnicoCliente() +"], cantidad: " + cantidadActiva);
                } 
                
                if (getLogger().isEnabledFor(Level.INFO)) {
        			getLogger().info("[tieneTddActiva][" + datoUnicoCliente() +"][BCI_FINOK]");
        		}
                return cantidadActiva;
          }
 
	    catch (Exception ex) {
	       if (getLogger().isEnabledFor(Level.ERROR)) {
	              getLogger().error("[tieneTddActiva][BCI_FINEX][Exception] error con mensaje=<" + ex.getMessage()+">", ex);
	       }
	          throw new SistemaException(MessageBundleUtil.getMensajeBundle(BUNDLE_DEFAULT, "ERROR_DE_SISTEMA"));
	    }
	}
	
    /**
     * M�todo que evalua accesos de cliente asociado a cuenta corriente. <br>
     * Registro de versiones:
     * <ul>
     * <li>1.0 10/07/2017 Sergio Flores (SEnTRA) - Ricardo Carrasco C�ceres (Ing. Soft. BCI): Versi�n Inicial.</li>
     * </ul>
     * @param estado String que contiene estado a consultar de ctacte.
     * @return valor booleano true en caso de existir cuenta de corriente vigente.
     * @since 1.0
     */
    public boolean tieneCuentaCorriente(String estado){
    	if (getLogger().isEnabledFor(Level.INFO)) {
		    getLogger().info("[tieneCuentaCorriente][" + datoUnicoCliente() + "][BCI_INI] [" + estado + "]");
		}
        try{
        	ClienteMB clienteMB = (ClienteMB) getManagedBean("clienteMB");
   
        	if (getLogger().isEnabledFor(Level.DEBUG)) {
        		getLogger().debug("[tieneCuentaCorriente]:cliente MB:" + clienteMB);
            }
            Cliente cliente = new Cliente(clienteMB.getRut(), clienteMB.getDigitoVerif());
            if (getLogger().isEnabledFor(Level.DEBUG)) {
                getLogger().debug("[tieneCuentaCorriente]:cliente:" + cliente);
            } 

            if (getLogger().isEnabledFor(Level.DEBUG)) {
            	getLogger().debug("[tieneCuentaCorriente]:cliente TipoUsuario:" + clienteMB.getTipoUsuario());
            } 
            
            Operacion[] ope = cliente.getOperaciones(clienteMB.getTipoUsuario().charAt(0));
            if (getLogger().isEnabledFor(Level.DEBUG)) {
            	getLogger().debug("[tieneCuentaCorriente]:ope:" + ope+ " Largo de ope :"+ope.length );
            }
            if (ope != null && ope.length > 0) {
                for (int i = 0; i < ope.length; i++) {
                	if (getLogger().isEnabledFor(Level.DEBUG)) {
                    	getLogger().debug("[tieneCuentaCorriente]:ope:" + ope+ " TIPO_OPERACION_CCT :"+ope[i].getTipoOperacion());
                    }
                    if (ope[i].getTipoOperacion().equals(TIPO_OPERACION_CCT)
                            && ope[i].getCodEstado().equals(estado)) {
                        return true;
                    }
                }
            }
        }
        catch (Exception e) {
        	 if (getLogger().isEnabledFor(Level.ERROR)) {
        		 getLogger().error("[tieneCuentaCorriente]:Excepcion al obtener cuenta Corriente:" + e.toString());
            }
        }
        return false;
    }
	
    /**
     * <P>
     * M�todo encargado de consultar las tarjetas ATM de un cliente en particular.
     * </P>
     * 
     * Registro de versiones:
     * <UL>
      * <li>1.0 10/07/2017 Sergio Flores (SEnTRA) - Ricardo Carrasco C�ceres (Ing. Soft. BCI): Versi�n Inicial.</li>
     * </UL>
     * 
     * @param rut par�metro que representa el Rut de cliente a consultar.
     * @param indicador par�metro que representa el tipo de cliente, ya sea titular o adicional.
     * @param canal Canal del cliente.
     * @throws TarjetasException Error de tarjetas
     * @throws TuxedoException Error de tuxedo
     * @throws GeneralException Error general
     * @return TarjetaCliente Objeto que registra el resultado de la consulta.
     * @since 1.0
     */
    private TarjetaCliente obtieneTarjetasATM(long rut, char indicador, String canal) throws TarjetasException,
        TuxedoException, GeneralException {
        if (getLogger().isEnabledFor(Level.INFO)) {
        	getLogger().info("[ObtieneTarjetasATM][" + rut + "] [" + indicador + "]");
        }
        TarjetaCliente tarjetaCliente = null;
       
        try {
            tarjetaCliente = srvTarjetas().getTarjetasATMXInd(rut, indicador);
        }
        catch (TarjetasException tex) {
            if (getLogger().isEnabledFor(Level.ERROR)) {
            	getLogger().fatal("[ObtieneTarjetasATM][BCI_FINEX][TarjetasException][" + rut + "]" + tex.getMessage());
            }
            throw new GeneralException("EMETDC-" + canal);
        }
        catch (GeneralException gex) {
            if (getLogger().isEnabledFor(Level.ERROR)) {
            	getLogger().error("[ObtieneTarjetasATM][BCI_FINEX][GeneralException][" + rut + "]" + gex.getMessage());
            }
            throw new GeneralException("EMETDC-" + canal);
        }
        catch (Exception e) {
            if (getLogger().isEnabledFor(Level.ERROR)) {
            	getLogger().error("[ObtieneTarjetasATM][BCI_FINEX][Exception][" + rut + "]" + e.getMessage());
            }
            throw new GeneralException("EMETDC-" + canal);
        }
        
        if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[obtieneTarjetasATM][BCI_FINOK][" + rut + "]");
		}
        return tarjetaCliente;
    }
    
    
	/**
	 * @DescripcionMetodo M�todo permite autenticar un desafio generico de tipo otp.
	 * 
	 * @EjemploRequest
	 *  - tokenOtp: "8J8MJ0Y9"
	 * @EjemploResponse
	 * {
	 *    "idCodigoEstado":"null";
	 *    "estatus":"true",
	 *    "glosa": "null",
	 *    "informacionAnexa":"null"
     * }
	 *
	 * @CodigosRespuesta
	 *  - 200: respuesta OK
	 *  - 600: Error gen�rico, ver logs para m�s detalles.
	 *  - 500: Ha ocurrido un error de sistema.
	 *
	 * @RegistroVersiones
	 *
	 *   - 1.0 | 10/07/2017 | Sergio Flores | Ricardo Carrasco C�ceres | SEnTRA | Version Inicial.
	 *
	 * @param tokenOtp token a autenticar.
	 * @throws NegocioException Excepci�n espec�fica de negocio.
	 * @return ResultadoOperacionSegundaClaveTO resultado del servicio de autenticar un token.
	 */    
    @POST
	@Path("autenticarTokenOTP")
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public ResultadoOperacionSegundaClaveTO autenticarTokenOTP(@FormParam("tokenOtp") String tokenOtp) 
    		throws NegocioException{

       if (getLogger().isEnabledFor(Level.INFO)) {
		   getLogger().info("[autenticarTokenOTP] [" +tokenOtp+ "] [BCI_INI]");
	   }
       
       wcorp.model.seguridad.ResultadoOperacionSegundaClaveTO resultadoOperacionSegundaClaveTO = null;
       String codigoEvento = null;
        try {
        	SesionMB sesionMB = (SesionMB) getManagedBean("sesionMB");
     	    ClienteMB clienteMB = (ClienteMB) getManagedBean("clienteMB");
        	String canal = sesionMB.getCanalId() != null ? sesionMB.getCanalId() : "";
        	String userId = clienteMB.getFullRut();
            String grupoClienteEntrust = TablaValores.getValor(TABLA_DE_PARAMETROS, "pinToken", "desc");
            
            EstrategiaEntrustSoftToken estretegia = new EstrategiaEntrustSoftToken();
            UsuarioSegundaClaveTO usuarioSegundaClave = 
            		new UsuarioSegundaClaveTO(userId, Dominio.obtieneDominio(canal).toString(), canal);
            
            usuarioSegundaClave.setNombreCliente(clienteMB.getFullName());
            usuarioSegundaClave.setGrupoClienteEntrust(grupoClienteEntrust);
            
            ParametrosEstrategiaSegundaClaveTO parametrosEstrategiaSegundaClaveTO = new ParametrosEstrategiaSegundaClaveTO(
            		usuarioSegundaClave);
            
            parametrosEstrategiaSegundaClaveTO.setTipoSegundaClave(TIPO_OTP);
            parametrosEstrategiaSegundaClaveTO.setCodigoConfirmacion(tokenOtp);
            
            resultadoOperacionSegundaClaveTO = estretegia.autenticar(parametrosEstrategiaSegundaClaveTO);
            
            if (getLogger().isEnabledFor(Level.DEBUG)) {
                getLogger().debug("[autenticarTokenOTP] [" + datoUnicoCliente() +"], resultadoOperacionSegundaClaveTO: " + StringUtil.contenidoDe(resultadoOperacionSegundaClaveTO));
            } 
        	

            codigoEvento = TablaValores.getValor(TABLA_DE_PARAMETROS, "Journal", "codEventoAutentica");
            
            journalizarEnrolarEntrust(PRODUCTO, 
            		codigoEvento, 
            		TablaValores.getValor(TABLA_DE_PARAMETROS, "Journal", "SubCodEventoOK"), 
            		TablaValores.getValor(TABLA_DE_PARAMETROS, "Journal", "estadoOK") , 
            		tokenOtp); 
            
            if (getLogger().isEnabledFor(Level.INFO)) {
    			getLogger().info("[autenticarTokenOTP][BCI_FINOK]");
    		}
            
            return traspasarResultadoOperacionSegundaClaveTO(resultadoOperacionSegundaClaveTO, MessageBundleUtil.getMensajeBundle(BUNDLE_DEFAULT, "EXITO_ENROLAR_DISPOSITIVO"));
       }
      catch (NegocioException ex) {
          if (getLogger().isEnabledFor(Level.ERROR)) {
              getLogger().error("[autenticarTokenOTP][BCI_FINEX][NegocioException] error con mensaje=<" + ex.getMessage()+">", ex);
          }
		
          journalizarEnrolarEntrust(PRODUCTO, 
        	   codigoEvento, 
   			   TablaValores.getValor(TABLA_DE_PARAMETROS, "Journal", "SubCodEventoNOK"), 
   			   TablaValores.getValor(TABLA_DE_PARAMETROS, "Journal", "estadoNOK") ,
   			tokenOtp); 
          throw ex;
      }
      catch (Exception ex) {
   	   if (getLogger().isEnabledFor(Level.ERROR)) {
              getLogger().error("[autenticarTokenOTP][BCI_FINEX][Exception] error con mensaje=<" + ex.getMessage()+">", ex);
          }
            journalizarEnrolarEntrust(PRODUCTO, 
            		codigoEvento, 
			   TablaValores.getValor(TABLA_DE_PARAMETROS, "Journal", "SubCodEventoError"), 
			   TablaValores.getValor(TABLA_DE_PARAMETROS, "Journal", "estError") ,
			   tokenOtp); 
          throw new SistemaException(MessageBundleUtil.getMensajeBundle(BUNDLE_DEFAULT, "ERROR_DE_SISTEMA"));
      }
   }
   
     
 	/**
      * Realiza el proceso de obtener el codigo otp y realiza envio de SMS a cliente.
      * <p>
      * 
      * Registro de Versiones:
      * <ul>
      * <li>1.0 10/07/2017 Luis L�pez Alamos (SEnTRA) - Ricardo Carrasco C�ceres (Ing. Soft. BCI): Versi�n Inicial.</li>
      * <li>1.1 10/10/2017 Sergio Flores (SEnTRA) - Ricardo Carrasco C�ceres (Ing. Soft. BCI): Se agrega nombre cliente a la consulta obtener otp enrolamiento.</li>
      * </ul>
      * <P>
      * 
      * @throws NegocioException Excepci�n espec�fica de negocio.
      * @return OtpEntrustWSTO resultado del servicio que contiene el codigo desafio otp.
      * @since 1.0 
      */
     private OtpEntrustWSTO obtenerOTP() throws NegocioException{
         if (getLogger().isEnabledFor(Level.INFO)){
             getLogger().info("[obtenerOTP][" + datoUnicoCliente() + "][BCI_INI]");
         }
             
         wcorp.model.seguridad.to.OtpEntrustTO auxEntrustOTP= null;
         try {
             
        	 SesionMB sesionMB = (SesionMB) getManagedBean("sesionMB");
      	     ClienteMB clienteMB = (ClienteMB) getManagedBean("clienteMB");
         	 String canal = sesionMB.getCanalId() != null ? sesionMB.getCanalId() : "";
         	 String userId = clienteMB.getFullRut();
         	 
         	 if (getLogger().isEnabledFor(Level.DEBUG)) {
                 getLogger().debug("[obtenerOTP][" + datoUnicoCliente() +"], Nombre Cliente: " + clienteMB.getCliente().getFullName());
             } 
         	
             EstrategiaEntrustSoftToken estretegia = new EstrategiaEntrustSoftToken();
             UsuarioSegundaClaveTO usuarioSegundaClave = 
            		 new UsuarioSegundaClaveTO(userId, Dominio.obtieneDominio(canal).toString(), canal);
             
             String grupoClienteEntrust = TablaValores.getValor(TABLA_DE_PARAMETROS, "pinToken", "desc");
             usuarioSegundaClave.setGrupoClienteEntrust(grupoClienteEntrust);
             usuarioSegundaClave.setNombreCliente(clienteMB.getCliente().getFullName());
             
             auxEntrustOTP = estretegia.obtenerOTP(usuarioSegundaClave);
             
             if (getLogger().isEnabledFor(Level.DEBUG)) {
                 getLogger().debug("[obtenerOTP][" + datoUnicoCliente() +"], otpEntrustTO: " + StringUtil.contenidoDe(auxEntrustOTP));
             } 
             
             if (getLogger().isEnabledFor(Level.INFO)) {
     			getLogger().info("[obtenerOTP][" + datoUnicoCliente() +"][BCI_FINOK]");
     		}
             
             return traspasarResultadoOtpEntrustTO(auxEntrustOTP);
         }
         catch (NegocioException ex) {
             if (getLogger().isEnabledFor(Level.ERROR)) {
                 getLogger().error("[obtenerOTP][BCI_FINEX][NegocioException] error con mensaje=<" + ex.getMessage()+">", ex);
             }
  		
            throw ex;
         }
         catch (Exception ex) {
      	   if (getLogger().isEnabledFor(Level.ERROR)) {
                 getLogger().error("[obtenerOTP][BCI_FINEX][Exception] error con mensaje=<" + ex.getMessage()+">", ex);
             }
             throw new SistemaException(MessageBundleUtil.getMensajeBundle(BUNDLE_DEFAULT, "ERROR_DE_SISTEMA"));
         }
      }
     
 	/**
 	 * Metodo que pasa los estados segunda clave a EstadoSegundaClaveWSTO
 	 * 
 	 * *
 	 * <p>
 	 * Registro de versiones:
 	 * <ul>
 	  * <li>1.0 10/07/2017 Sergio Flores (SEnTRA) - Ricardo Carrasco C�ceres (Ing. Soft. BCI): Versi�n Inicial.</li>
 	 * </ul>
 	 * <p>
 	 * 
 	 * @param retorno EstadoSegundaClaveTO que contiene las cuentas a ser transapasadas.
 	 * @throws NegocioException Excepci�n espec�fica de negocio.
 	 * @throws SistemaException Excepci�n de Sistema.
 	 * @throws Exception error al traspasar resultado otp entrust.
 	 * @return OtpEntrustWSTO resultado del servicio que contiene el codigo desafio otp.
 	 * @since 1.0
 	 */
 	private OtpEntrustWSTO traspasarResultadoOtpEntrustTO(wcorp.model.seguridad.to.OtpEntrustTO retorno) 
 			throws NegocioException, SistemaException, Exception{
 		if (getLogger().isEnabledFor(Level.INFO)) {
 		    getLogger().info("[traspasarResultadoOtpEntrustTO][" + datoUnicoCliente() +"][BCI_INI][" + retorno + "]");
 		}
 		
 		try {
     	      if(retorno!=null) {
     	    	     OtpEntrustWSTO respuesta = new OtpEntrustWSTO();
     	    	  
     	    		  if (retorno.getCodigoRespuestaWs() == 0) {
     	    			  respuesta.setOtp(retorno.getOtp());
     	    			  respuesta.setCreateDate(retorno.getCreateDate());
     	    			  respuesta.setExpireDate(retorno.getExpireDate());
     	    			  respuesta.setTransactionId(retorno.getTransactionId());
     	    		  } 
     	    		  else if (retorno.getCodigoRespuestaWs() == CODE_600_ERROR) {
     	    			  throw new NegocioException(MessageBundleUtil.getMensajeBundle(BUNDLE_DEFAULT, "CODE_600_ERROR"));
     	    		  }
     	    		  else {
     	    			  throw new NegocioException(retorno.getDescripcionRespuestaWs());
     	    		  }
     	    		
     	    		if (getLogger().isEnabledFor(Level.INFO)) {
     	     			getLogger().info("[traspasarResultadoOtpEntrustTO][" + datoUnicoCliente() +"][BCI_FINOK]");
     	     		}

     	    	  return respuesta;
     	      }
     	      else {
     	    	 if (getLogger().isEnabledFor(Level.INFO)) {
  	     			getLogger().info("[traspasarResultadoOtpEntrustTO][" + datoUnicoCliente() +"][BCI_FINOK]");
  	     		}
                   throw new NegocioException(MessageBundleUtil.getMensajeBundle(BUNDLE_DEFAULT, "SIN_ACTIVACION_TOKEN"));
               }
           }
 		catch (NegocioException ex) {
 	           if (getLogger().isEnabledFor(Level.ERROR)) {
 	               getLogger().error("[traspasarResultadoOtpEntrustTO][BCI_FINEX][NegocioException] error con mensaje=<" + ex.getMessage()+">", ex);
 	           }
 			

 	           throw ex;
 	       }
 	       catch (Exception ex) {
 	    	   if (getLogger().isEnabledFor(Level.ERROR)) {
 	               getLogger().error("[traspasarResultadoOtpEntrustTO][BCI_FINEX][Exception] error con mensaje=<" + ex.getMessage()+">", ex);
 	           }
 	           throw new SistemaException(MessageBundleUtil.getMensajeBundle(BUNDLE_DEFAULT, "ERROR_DE_SISTEMA"));
 	       }
 		
 	}
     
 	
	/**
	 * @DescripcionMetodo M�todo permite validar las TDDs.
	 * 
	 * @EjemploRequest
	 *  - numTarjeta: "1107"
	 *  - claveRedBanc:"1234"
	 * @EjemploResponse
     * {
     *  "idCodigoEstado": 0,
     *  "status": 0,
     *  "estatus": true,
     *  "data": null,
     *  "glosa": null,
     *  "informacionAnexa": null,
     *  "desc": null,
     *  "celular": "+56612348770"
     * }
	 *
	 * @CodigosRespuesta
	 *  - 200: respuesta OK
	 *  - 600: Error gen�rico, ver logs para m�s detalles.
	 *  - 500: Ha ocurrido un error de sistema.
	 *
	 * @RegistroVersiones
	 *
	 *   - 1.0 | 10/07/2017 | Sergio Flores | Ricardo Carrasco C�ceres | SEnTRA | Version Inicial.
	 *
	 * @param numTarjeta par�metro que representa el numero de tarjeta a consultar.
     * @param claveRedBanc par�metro que representa password a validar.
	 * @return ResultadoEnrolamientoWSTO resultado de la validacion de tarjeta.
	 */  	
    @POST
	@Path("validarTarjetaYClaveRedbanc")
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public ResultadoEnrolamientoWSTO validarTarjetaYClaveRedbanc(@FormParam("numTarjeta") String numTarjeta, 
   		 @FormParam("claveRedBanc") String claveRedBanc) {

       if (getLogger().isEnabledFor(Level.INFO)) {
		   getLogger().info("[validarTarjetaYClaveRedbanc][" + datoUnicoCliente() +"] , numTarjeta [" +numTarjeta+ "][BCI_INI]");
	    }
       ResultadoEnrolamientoWSTO resultadoEnrolamiento=null;  
       String codigoEvento = null;
       
       try {
       	     SesionMB sesionMB = (SesionMB) getManagedBean("sesionMB");
    	     ClienteMB clienteMB = (ClienteMB) getManagedBean("clienteMB");
       	     String canal = sesionMB.getCanalId() != null ? sesionMB.getCanalId() : "";
       	     codigoEvento = TablaValores.getValor(TABLA_DE_PARAMETROS, "Journal", "codEventoActivar");
           
        	 AutenticacionEnrolamientoTO autenticacionEnrolamientoTO = new AutenticacionEnrolamientoTO();
            
            autenticacionEnrolamientoTO.setRut(clienteMB.getRut());
            autenticacionEnrolamientoTO.setCanal(canal);
            autenticacionEnrolamientoTO.setNumeroTarjeta(numTarjeta);
            autenticacionEnrolamientoTO.setClave(claveRedBanc);
            
            resultadoEnrolamiento = validaExisteTarjetaDebito(autenticacionEnrolamientoTO);
            
            if (getLogger().isEnabledFor(Level.DEBUG)) {
            	getLogger().debug("[validarTarjetaYClaveRedbanc] [" +  datoUnicoCliente() +"], resultadoEnrolamiento: " + StringUtil.contenidoDe(resultadoEnrolamiento));
            }
            
            if(resultadoEnrolamiento.isEstatus()){
            	if (getLogger().isEnabledFor(Level.DEBUG)) {
                	getLogger().debug("[validarTarjetaYClaveRedbanc] [" +  datoUnicoCliente() +"], resultadoEnrolamiento: "+resultadoEnrolamiento.isEstatus() );
                }
            	
            	enviarSmsOTP();
            	resultadoEnrolamiento.setCelular(TablaValores.getValor(TABLA_DE_PARAMETROS,"CONFIGURACION_LATINIA_NOTIFICACIONES", "prefixTelefonoSms") +celularCliente.trim());
            } else {
            	 throw new NegocioException(resultadoEnrolamiento.getGlosa());
            }
              
        }
       catch (NegocioException ex) {
           if (getLogger().isEnabledFor(Level.ERROR)) {
               getLogger().error("[validarTarjetaYClaveRedbanc][BCI_FINEX][NegocioException] error con mensaje=<" + ex.getMessage()+">", ex);
           }
		
          throw ex;
       }
        catch (Exception e) {
        	if(getLogger().isEnabledFor(Level.WARN)){
                getLogger().warn("[validarTarjetaYClaveRedbanc] [" +  datoUnicoCliente() +"] [Exception] mensaje de warning =<" + e.getMessage()+">", e);
            }
        	journalizarEnrolarEntrust(PRODUCTO, 
        		   codigoEvento, 
     			   TablaValores.getValor(TABLA_DE_PARAMETROS, "Journal", "SubCodEventoError"), 
     			   TablaValores.getValor(TABLA_DE_PARAMETROS, "Journal", "estError") ,
     			   numTarjeta); 
        }
               

        journalizarEnrolarEntrust(PRODUCTO, 
        		codigoEvento, 
        		TablaValores.getValor(TABLA_DE_PARAMETROS, "Journal", "SubCodEventoOK"), 
        		TablaValores.getValor(TABLA_DE_PARAMETROS, "Journal", "estadoOK") , 
        		numTarjeta); 
        if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[validarTarjetaYClaveRedbanc] [" + datoUnicoCliente() +"] [BCI_FINOK]");
		}
        
        return resultadoEnrolamiento;

    }

     
     /**
      * Metodo que valida la tarjeta de debito.
      * <p>
      * Registro de versiones: 
      * <ul>
      * <li>1.0 10/07/2017 Sergio Flores (SEnTRA) - Ricardo Carrasco C�ceres (Ing. Soft. BCI): Versi�n Inicial.</li>
      * </ul>
      * <p> 
      * @param autenticacion objeto con los datos para realizar la validacion de tarjeta tdd.
      * @return ResultadoOperacionSegundaClaveTO resultado del servicio de validacion de tarjeta tdd.
      * @since 1.0
      */
     private ResultadoEnrolamientoWSTO validaExisteTarjetaDebito(AutenticacionEnrolamientoTO autenticacion){
        if (getLogger().isEnabledFor(Level.INFO)) {
  		   getLogger().info("[validaExisteTarjetaDebito][" + autenticacion.getRut() + "][BCI_INI]");
  	   }
        
         TarjetaCliente tarjetaCliente = null;
         ResultadoEnrolamientoWSTO resultado = new ResultadoEnrolamientoWSTO();
         
         try {
                         
             tarjetaCliente = obtieneTarjetasATM(autenticacion.getRut(), INDICADOR, autenticacion.getCanal());
             
             if (getLogger().isEnabledFor(Level.DEBUG)) {
                 getLogger().debug("[validaExisteTarjetaDebito] [" + autenticacion.getRut() +"], tarjetaCliente: " + StringUtil.contenidoDe(tarjetaCliente));
             } 
             
             if(tarjetaCliente!=null) {
	             DetalleTarjeta[] detalleTarjeta = tarjetaCliente.getTarjeta();
	             
	             if (getLogger().isEnabledFor(Level.DEBUG)) {
	                 getLogger().debug("[validaExisteTarjetaDebito] autenticacion : "+ autenticacion.getNumeroTarjeta());
	             }
	            
	            boolean errores = true;
	            int i=0;
	         	for(; i< detalleTarjeta.length; i++) {

	         		if(autenticacion.getNumeroTarjeta().equals(
	         				detalleTarjeta[i].getNumeroTarjeta().substring(
	         							detalleTarjeta[i].getNumeroTarjeta().length() -CUATRO_DIGITOS_TDD , 
	         							detalleTarjeta[i].getNumeroTarjeta().length()))){
	         			errores = false;
	         			
	         			if(validaPin(USUARIO, detalleTarjeta[i].getNumeroTarjeta(), autenticacion.getClave())!=null){
	         				resultado.setEstatus(true);
	         			}
	         			else{
	         				resultado.setEstatus(false);
	         				throw new NegocioException(MessageBundleUtil.getMensajeBundle(BUNDLE_DEFAULT, "ERROR_CLAVE_TARJETAS_DEBITO"));
	         			}
	         			
	         		}
	             	
	             }
	         	

	         	if (getLogger().isEnabledFor(Level.DEBUG)) {
	                getLogger().debug("[validaExisteTarjetaDebito] [" + autenticacion.getRut() +"], resultado: " + StringUtil.contenidoDe(resultado));
	            } 
         	
           }
           else {
        	     resultado.setEstatus(false);
			     setError(resultado, ErrorEnrolamientoEnum.ERROR_TARJETA_INVALIDA); 
           }
         }
         catch (NegocioException ex) {
	           if (getLogger().isEnabledFor(Level.ERROR)) {
	        	   getLogger().error("[validaExisteTarjetaDebito][BCI_FINEX][NegocioException] error con mensaje=<" + ex.getMessage()+">", ex);
	           }
			

	           throw ex;
	       }
         catch (Exception e) {
         	if(getLogger().isEnabledFor(Level.WARN)){
                 getLogger().warn("[validaExisteTarjetaDebito] [" + autenticacion.getRut() +"] [Exception] mensaje de warning =<" + e.getMessage()+">", e);
             }
         	setError(resultado, ErrorEnrolamientoEnum.ERROR_SISTEMA);
         }
         
     	
        if (getLogger().isEnabledFor(Level.INFO)) {
 		   getLogger().info("[validaExisteTarjetaDebito] [" + autenticacion.getRut() + "] [BCI_FINOK]");
 	   }
        return resultado;
     }
     
     /**
 	 * <p>
 	 * Establece los campos descriptivos del error generado en el objeto pasado por par�metros.
 	 * </p>
 	 *
 	 * Registro de versiones:
 	 * <ul>
 	 * <li>1.0 10/07/2017 Sergio Flores (SEnTRA) - Ricardo Carrasco C�ceres (Ing. Soft. BCI): Versi�n Inicial.</li>
 	 * </ul>
 	 * <br>
 	 * @param res objeto con los datos del resultado de la operaci�n.
 	 * @param errorResultado error rescatado desde la clase Enum.
 	 * @since 1.0
 	 */
 	private void setError(ResultadoEnrolamientoWSTO res, ErrorEnrolamientoEnum errorResultado) {
 		if (getLogger().isEnabledFor(Level.INFO)) {
 	 		   getLogger().info("[setError][BCI_INI]");
 	 	}
 		
 		res.setEstatus(false);
 		res.setInformacionAnexa("");
 		res.setIdCodigoEstado(errorResultado.getId());
 		res.setGlosa(errorResultado.getMensaje());
 		
 		if (getLogger().isEnabledFor(Level.INFO)) {
 			   getLogger().info("[setError][BCI_FINOK]");
 		}
 	}
 	
    /**
     * Metodo que ejemplo:"recupera los datos iniciales."
     * <p>
     * Registro de versiones: 
     * <ul>
     * <li>1.0 10/07/2017 Sergio Flores (SEnTRA) - Ricardo Carrasco C�ceres (Ing. Soft. BCI): Versi�n Inicial.</li>
     * </ul>
     * <p> 
     * @param usuario usuario a validar.
     * @param nTarjeta tarjeta a validar.
     * @param pin codigo pin a validar.
     * @throws SeguridadException captura errores de Seguridad cuando falla la validacion del pin.
     * @throws TuxedoException captura errores de comunicacion con los servicios tuxedos.
     * @throws GeneralException captura errores de comunicacion con ServiciosSeguridad.
     * @throws RemoteException captura errores de comunicacion DatosPin.     
     * @return DatosPin datos del pin validado.
     * @since 1.0
     */
    private DatosPin validaPin(String usuario, String nTarjeta, String pin) throws SeguridadException,
    TuxedoException, GeneralException, RemoteException {
    DatosPin datosPin = null;
    try {
    	
    	 if(getLogger().isEnabledFor(Level.INFO)){
 			getLogger().info("[validaPin][BCI_INI]Usuario: ["+usuario+"]NumTarjeta: ["+nTarjeta
 			+"]Pin: ["+pin+"]");
 		}
        
        ServiciosSeguridad servicioSeguridad = this.obtenerServiciosSeguridad();
       
        datosPin = servicioSeguridad.validaPin(usuario, nTarjeta, pin);
        
        if (getLogger().isEnabledFor(Level.DEBUG)) {
        	getLogger().debug("[validaPin] [" +  datoUnicoCliente() +"], datosPin: " + StringUtil.contenidoDe(datosPin));
        }
        
        if (getLogger().isEnabledFor(Level.INFO)) {
			   getLogger().info("[validaPin][BCI_FINOK]");
		}
    }
    catch (TuxedoException tE) {
        
    	if(getLogger().isEnabledFor(Level.ERROR)){
            getLogger().error("[validaPin] [BCI_FINEX] [TuxedoException] Error con mensaje =<" + tE.getMessage()+">", tE);
        }
    	
        throw tE;
    }
    catch (SeguridadException sE) {
    	if(getLogger().isEnabledFor(Level.WARN)){
            getLogger().warn("[validaPin] [BCI_FINEX] [" +  datoUnicoCliente() +"] [SeguridadException] Error con mensaje =<" + sE.getMessage()+">", sE);
        }

    }
    
    if (getLogger().isEnabledFor(Level.INFO)) {
		   getLogger().info("[validaPin] [BCI_FINOK]");
    }
    return datosPin;

   }
    
    /**
     * M�todo encargado de retornar una instancia del EJB de ServiciosSeguridad
     * <p>
     * Registro de versiones:<ul>
     * 
     * <li>1.0 10/07/2017 Jaime Suazo. (SEnTRA) - Ricardo Carrasco (Ing. Soft. BCI): versi�n inicial.</li>
     * 
     * </ul>
     * <p>
     *  @return ServiciosSeguridad instancia del ejb Servicios de Seguridad. 
     *  @throws GeneralException error de service.
     *  @since 1.0
     */
	private ServiciosSeguridad obtenerServiciosSeguridad() throws GeneralException {
		if (getLogger().isEnabledFor(Level.INFO)) {
	 		   getLogger().info("[obtenerServiciosSeguridad][BCI_INI]");
	 	}
		ServiciosSeguridadHome serviciosSeguridadHome = null;
        ServiciosSeguridad serviciosSeguridad = null;

        try {
            serviciosSeguridadHome = (ServiciosSeguridadHome) EnhancedServiceLocator.getInstance().getHome(
                "wcorp.serv.seguridad.ServiciosSeguridad", ServiciosSeguridadHome.class);
            serviciosSeguridad = serviciosSeguridadHome.create();
        }
        catch (Exception e) {
        	if (getLogger().isEnabledFor(Level.ERROR)) {
	               getLogger().error("[obtenerServiciosSeguridad][BCI_FINEX][Exception] error con mensaje=<" + e.getMessage()+">", e);
	        }

            throw new GeneralException("Servicio no disponible");
        }
        
        if (getLogger().isEnabledFor(Level.INFO)) {
	 		   getLogger().info("[obtenerServiciosSeguridad][BCI_FINOK]");
	 	}
        return serviciosSeguridad;
    }
    
    /**
     * Realiza el proceso de envio de SMS a cliente. 
     * <p>
     * 
     * Registro de Versiones:
     * <ul>
     * <li>1.0 10/07/2017 Luis L�pez Alamos (SEnTRA) - Ricardo Carrasco C�ceres (Ing. Soft. BCI): Versi�n Inicial.</li>
     * </ul>
     * <P>
     * @param otpEntrust codigo otp a enviar mediante sms.
     * @since 1.0
     * 
     */
    private void enviarMensajesSMS(OtpEntrustWSTO otpEntrust) {
        
        if (getLogger().isEnabledFor(Level.INFO)){
            getLogger().info("[obtenerMensajesSMS] [" + datoUnicoCliente() + "] [BCI_INI]");
        }
        
         SesionMB sesionMB = (SesionMB) getManagedBean("sesionMB");
	     ClienteMB clienteMB = (ClienteMB) getManagedBean("clienteMB");
   	     String canal = sesionMB.getCanalId() != null ? sesionMB.getCanalId() : "";
      
         try {
        	 
              if (!StringUtil.esVacio(celularCliente)) {
        	       String codigoOtp = otpEntrust.getOtp();

                   if (getLogger().isEnabledFor(Level.DEBUG)) {
	                   getLogger().debug("[obtenerMensajesSMS][" + clienteMB.getRut() +"][Enviando SMS] otp : "+ codigoOtp);
	               } 
                   
                   if (codigoOtp != null) {
		                 String contexto = TablaValores.getValor(TABLA_DE_PARAMETROS_JNDI, "cluster", "param");
		                 String empresa = TablaValores.getValor(TABLA_DE_PARAMETROS,"enrolamientoEnvioSMS", "Empresa");
		                 String tipoMensaje = TablaValores.getValor(TABLA_DE_PARAMETROS,"enrolamientoEnvioSMS", "tipoMensaje");
		                 String credencial = TablaValores.getValor(TABLA_DE_PARAMETROS,"enrolamientoEnvioSMS", "credencial");
		                 String encabezadoMedio = TablaValores.getValor(TABLA_DE_PARAMETROS, "enrolamientoEnvioSMS", "encabezadoMedio");
		                 String refProducto = TablaValores.getValor(TABLA_DE_PARAMETROS,"enrolamientoEnvioSMS", "refProduct");
		                 String refContrato = TablaValores.getValor(TABLA_DE_PARAMETROS,"enrolamientoEnvioSMS", "refContrato");
		            
		                 String textMensaje = TablaValores.getValor(TABLA_DE_PARAMETROS,"enrolamientoVariablesMensajeSMS", "valor");
		                 textMensaje = StringUtil.reemplazaUnaVez(textMensaje, "{0}", codigoOtp);
		            
			                if(getLogger().isDebugEnabled()){ 
			                    getLogger().debug("[obtenerMensajesSMS][" + datoUnicoCliente() + "] parametros: " 
			                            + "empresa: " + empresa + " tipoMensaje: " + tipoMensaje
			                            + " credencial: " + credencial + " encabezadoMedio: " + encabezadoMedio
			                            + " refProducto: " + refProducto + " refContrato: " + refContrato
			                            + " refProducto: " + refProducto + " mensaje: " + textMensaje
			                            + " contexto: " + contexto + " destinatario: " + celularCliente); 
			                }
		            
			            MensajeLatiniaDTO mensaje = new MensajeLatiniaDTO();
			            mensaje.setEmpresa(empresa);
			            mensaje.setDestinatario(encabezadoMedio + celularCliente);
			            mensaje.setMensaje(textMensaje);
			            mensaje.setRefProduct(refProducto);
			            mensaje.setTipoMensaje(tipoMensaje);
			            mensaje.setCredencial(credencial);
			            mensaje.setRefContract(refContrato);
            
          
            	 
			            ConectorServicioEnvioSDP instance = new ConectorServicioEnvioSDP(contexto);
            	        boolean status =  instance.envioSMSSDP(mensaje);
            	        if (getLogger().isDebugEnabled()) {
            	            getLogger().debug("[obtenerMensajesSMS][" + datoUnicoCliente() + "] status: " + status);
            	        }
            	 }
            	 else {
            		 if (getLogger().isEnabledFor(Level.DEBUG)) {
            			 getLogger().debug("[obtenerMensajesSMS][" + datoUnicoCliente() +"] no Se pudo acceder al codigo OTP del cliente");
                     }
            	 }
        }
        else {
        	if (getLogger().isEnabledFor(Level.DEBUG)) {
                getLogger().debug("[obtenerMensajesSMS][" + datoUnicoCliente() +"] no tiene telefono registrado. No se envia sms");
            }
        }
      }
      catch (Exception e) {
    	  if(getLogger().isEnabledFor(Level.WARN)){
              getLogger().warn("[obtenerMensajesSMS] [" + datoUnicoCliente() +"] [Exception] mensaje de warning =<" + e.getMessage()+">", e);
          }

      }
        
        if (getLogger().isEnabledFor(Level.INFO)){
            getLogger().info("[obtenerMensajesSMS][" + datoUnicoCliente() +"][BCI_FINOK]");
        }
    }
    
    /**
     * Realiza el proceso de envio de token por email al cliente. 
     * <p>
     * 
     * Registro de Versiones:
     * <ul>
     * <li>1.0 10/07/2017 Luis L�pez Alamos (SEnTRA) - Ricardo Carrasco C�ceres (Ing. Soft. BCI): Versi�n Inicial.</li>
     * </ul>
     * <P>
     * @param otpEntrust codigo otp a enviar mediante email.
     * @since 1.0
     * 
     */
    private void enviarMensajesEmail(OtpEntrustWSTO otpEntrust) {
        
        if (getLogger().isEnabledFor(Level.INFO)){
            getLogger().info("[enviarMensajesEmail] [" + datoUnicoCliente() + "] [BCI_INI]");
        }
        
         SesionMB sesionMB = (SesionMB) getManagedBean("sesionMB");
	     ClienteMB clienteMB = (ClienteMB) getManagedBean("clienteMB");
   	     String canal = sesionMB.getCanalId() != null ? sesionMB.getCanalId() : "";
         String correoCliente = clienteMB.getEmail();
      
         int codResp = ESTADO_NOK_ENVIO_EMAIL;
         String empresa = TablaValores.getValor(TABLA_DE_PARAMETROS, "enrolamientoEnvioSMS","EMPRESA");
         String asunto = "";
         String contrato = "";
         String refTemplate = "";
         String contexto = TablaValores.getValor(TABLA_DE_PARAMETROS_JNDI, "cluster","param");
         
         if (getLogger().isEnabledFor(Level.DEBUG)) {
             getLogger().debug("[enviarMensajesEmail][Contexto: " + contexto);
         }
         
         String codigoOtp = otpEntrust.getOtp();

         if (getLogger().isEnabledFor(Level.DEBUG)) {
             getLogger().debug("[enviarMensajesEmail][" + clienteMB.getRut() +"][Enviando Email] otp : "+ codigoOtp);
         } 
         
         if (codigoOtp != null) {
	         ServiciosClienteDelegate clienteDelegate = new ServiciosClienteDelegate();
	         DatosBasicosCliente datosCliente = null;
	         try {
	             datosCliente = clienteDelegate.datosBasicos(clienteMB.getRut(), clienteMB.getDigitoVerif());
	             if (getLogger().isEnabledFor(Level.DEBUG)) {
	                 getLogger().debug("[enviarMensajesEmail]datosCliente : " + datosCliente);
	             }
	         }
	         catch (TuxedoException te) {
	             if (getLogger().isEnabledFor(Level.ERROR)) {
	                 getLogger().error("[enviarMensajesEmail]TuxedoException : " + te.getMessage());
	             }
	         }
	         catch (RemoteException re) {
	             if (getLogger().isEnabledFor(Level.ERROR)) {
	                 getLogger().error("[enviarMensajesEmail]RemoteException : " + re.getMessage());
	             }
	         }
	         catch (GeneralException ge) {
	             if (getLogger().isEnabledFor(Level.ERROR)) {
	                 getLogger().error("[enviarMensajesEmail]GeneralException : " + ge.getMessage());
	             }
	         }
	         if (datosCliente != null) {
	             asunto = TablaValores.getValor(TABLA_DE_PARAMETROS, "administrarVariablesMensajeEmail", "asunto") +codigoOtp;
	             contrato = TablaValores.getValor(TABLA_DE_PARAMETROS,"administrarEmail", "refContrato");
	             refTemplate = TablaValores.getValor(TABLA_DE_PARAMETROS, "administrarEmail", "refTemplate");
	             
	             if (getLogger().isEnabledFor(Level.DEBUG)) {
	                 getLogger().debug("[enviarMensajesEmail] contrato : " + contrato + " template : " + refTemplate);
	             }
	             try {
	                 ConectorServicioEnvioSDP csesdp = new ConectorServicioEnvioSDP(contexto);
	                 Date fechaHoraActual = new Date();
	                 String fechaActual = FechasUtil.convierteDateAString(fechaHoraActual, "dd/MM/yyyy");
	                 String horaActual = FechasUtil.convierteDateAString(fechaHoraActual, "HH:mm");
	                 
	                 Map<String, String> parametrosMail = new HashMap<String, String>();
	                 parametrosMail.put("fromName:", empresa);
	                 parametrosMail.put("to", TablaValores.getValor(TABLA_DE_PARAMETROS, "contacto", "Desc")); //datosCliente.getEmail().trim()
	                 parametrosMail.put("subject", asunto);
	                 parametrosMail.put("body", " ");
	                 parametrosMail.put("mainContent", "body");
	                 
	                 HashMap paramsTemplate = new HashMap();
	                 paramsTemplate.put("Nombre1 Apellido1", datosCliente.getNombre().trim() + datosCliente.getApellidoPaterno().trim());
	                 paramsTemplate.put("fecha", fechaActual);
	                 paramsTemplate.put("hora", horaActual);
	                 paramsTemplate.put("Descripcion", codigoOtp);
	                 paramsTemplate.put("Multipass", "");
	                 
	                 codResp = csesdp.sendMail(empresa, contrato, refTemplate, parametrosMail, paramsTemplate);
	                 
	                 if (getLogger().isEnabledFor(Level.DEBUG)) {
	                     getLogger().debug("[enviarMensajesEmail] codResp: " + codResp);
	                 }
	             }
	             catch (Exception e) {
	                 codResp = ESTADO_NOK_ENVIO_EMAIL;
	                 if (getLogger().isEnabledFor(Level.ERROR)) {
	                     getLogger().error("[enviarMensajesEmail]Exception : " + e.getMessage());
	                 }
	             }
	         }
	         else {
	             if (getLogger().isEnabledFor(Level.DEBUG)) {
	                 getLogger().debug("[enviarMensajesEmail]error al consultar los datos del cliente.");
	             }
	             codResp = ESTADO_NOK_ENVIO_EMAIL;
	         }
         }
         if (getLogger().isEnabledFor(Level.INFO)) {
             getLogger().info("[enviarMensajesEmail][BCI_FINOK]");
         }
     }
    
    /**
     * Realiza el proceso de envio de token por email al cliente. 
     * <p>
     * 
     * Registro de Versiones:
     * <ul>
     * <li>1.0 10/07/2017 Luis L�pez Alamos (SEnTRA) - Ricardo Carrasco C�ceres (Ing. Soft. BCI): Versi�n Inicial.</li>
     * </ul>
     * <P>
     * @param desc descripcion que se envia mediante email.
     * @since 1.0
     * 
     */
    private void enviarMensajesEmailEntrust(String desc) {
        
        if (getLogger().isEnabledFor(Level.INFO)){
            getLogger().info("[enviarMensajesEmailEntrust] [" + datoUnicoCliente() + "] [BCI_INI]");
        }
        
         SesionMB sesionMB = (SesionMB) getManagedBean("sesionMB");
	     ClienteMB clienteMB = (ClienteMB) getManagedBean("clienteMB");
   	     String canal = sesionMB.getCanalId() != null ? sesionMB.getCanalId() : "";
         String correoCliente = clienteMB.getEmail();
      
         int codResp = ESTADO_NOK_ENVIO_EMAIL;
         String empresa = TablaValores.getValor(TABLA_DE_PARAMETROS, "enrolamientoEnvioSMS","EMPRESA");
         String asunto = "";
         String contrato = "";
         String refTemplate = "";
         String contexto = TablaValores.getValor(TABLA_DE_PARAMETROS_JNDI, "cluster","param");
         
         if (getLogger().isEnabledFor(Level.DEBUG)) {
             getLogger().debug("[enviarMensajesEmailEntrust][Contexto: " + contexto);
         }
         

         if (getLogger().isEnabledFor(Level.DEBUG)) {
             getLogger().debug("[enviarMensajesEmailEntrust][" + clienteMB.getRut() +"][Enviando Email] descripcion : "+ desc);
         } 
         
         if (desc != null) {
	         ServiciosClienteDelegate clienteDelegate = new ServiciosClienteDelegate();
	         DatosBasicosCliente datosCliente = null;
	         try {
	             datosCliente = clienteDelegate.datosBasicos(clienteMB.getRut(), clienteMB.getDigitoVerif());
	             if (getLogger().isEnabledFor(Level.DEBUG)) {
	                 getLogger().debug("[enviarMensajesEmailEntrust]datosCliente : " + datosCliente);
	             }
	         }
	         catch (TuxedoException te) {
	             if (getLogger().isEnabledFor(Level.ERROR)) {
	                 getLogger().error("[enviarMensajesEmailEntrust]TuxedoException : " + te.getMessage());
	             }
	         }
	         catch (RemoteException re) {
	             if (getLogger().isEnabledFor(Level.ERROR)) {
	                 getLogger().error("[enviarMensajesEmailEntrust]RemoteException : " + re.getMessage());
	             }
	         }
	         catch (GeneralException ge) {
	             if (getLogger().isEnabledFor(Level.ERROR)) {
	                 getLogger().error("[enviarMensajesEmailEntrust]GeneralException : " + ge.getMessage());
	             }
	         }
	         if (datosCliente != null) {
	             asunto =  desc;
	             contrato = TablaValores.getValor(TABLA_DE_PARAMETROS,"administrarEmail", "refContrato");
	             refTemplate = TablaValores.getValor(TABLA_DE_PARAMETROS, "administrarEmail", "refTemplate");
	             
	             if (getLogger().isEnabledFor(Level.DEBUG)) {
	                 getLogger().debug("[enviarMensajesEmailEntrust] contrato : " + contrato + " template : " + refTemplate);
	             }
	             try {
	                 ConectorServicioEnvioSDP csesdp = new ConectorServicioEnvioSDP(contexto);
	                 Date fechaHoraActual = new Date();
	                 String fechaActual = FechasUtil.convierteDateAString(fechaHoraActual, "dd/MM/yyyy");
	                 String horaActual = FechasUtil.convierteDateAString(fechaHoraActual, "HH:mm");
	                 
	                 Map<String, String> parametrosMail = new HashMap<String, String>();
	                 parametrosMail.put("fromName:", empresa);
	                 parametrosMail.put("to", datosCliente.getEmail().trim()); 
	                 parametrosMail.put("subject", asunto);
	                 parametrosMail.put("body", " ");
	                 parametrosMail.put("mainContent", "body");
	                 
	                 HashMap paramsTemplate = new HashMap();
	                 paramsTemplate.put("Nombre1 Apellido1", datosCliente.getNombre().trim() + datosCliente.getApellidoPaterno().trim());
	                 paramsTemplate.put("fecha", fechaActual);
	                 paramsTemplate.put("hora", horaActual);
	                 paramsTemplate.put("Descripcion", desc);
	                 paramsTemplate.put("Multipass", "");
	                 
	                 codResp = csesdp.sendMail(empresa, contrato, refTemplate, parametrosMail, paramsTemplate);
	                 
	                 if (getLogger().isEnabledFor(Level.DEBUG)) {
	                     getLogger().debug("[enviarMensajesEmailEntrust] codResp: " + codResp);
	                 }
	             }
	             catch (Exception e) {
	                 codResp = ESTADO_NOK_ENVIO_EMAIL;
	                 if (getLogger().isEnabledFor(Level.ERROR)) {
	                     getLogger().error("[enviarMensajesEmailEntrust]Exception : " + e.getMessage());
	                 }
	             }
	         }
	         else {
	             if (getLogger().isEnabledFor(Level.DEBUG)) {
	                 getLogger().debug("[enviarMensajesEmailEntrust]error al consultar los datos del cliente.");
	             }
	             codResp = ESTADO_NOK_ENVIO_EMAIL;
	         }
         }
         if (getLogger().isEnabledFor(Level.INFO)) {
             getLogger().info("[enviarMensajesEmailEntrust][BCI_FINOK]");
         }
     }
    
     
	/**
     * Valida si cliente posee celular
     * <p>
     * 
     * Registro de Versiones:
     * <ul>
     * <li>1.0 10/07/2017 Luis L�pez Alamos (SEnTRA) - Ricardo Carrasco C�ceres (Ing. Soft. BCI): Versi�n Inicial.</li>
     * </ul>
     * <P>
     * 
     * @return boolean true si tiene celular o false si no tiene celular o cae por excepci�n.
     * @since 1.0
     */
	private boolean validarClienteTieneCelular() {
	    if (getLogger().isEnabledFor(Level.INFO)){
            getLogger().info("[validarClienteTieneCelular] [" + datoUnicoCliente() + "] [BCI_INI]");
        }

	    try {
	    	 ClienteMB clienteMB = (ClienteMB) getManagedBean("clienteMB");
	         DireccionClienteBci[] direccionesCliente= clienteMB.obtieneDireccionesCliente();
	         for (int i = 0; i < direccionesCliente.length; i++) {
	            switch (direccionesCliente[i].getTipoDireccion()) {
	            case CODIGO_DIRECCION_CELULAR:
                    if(direccionesCliente[i].getAnexo() != null || direccionesCliente[i].getAnexo() != ""){
                        celularCliente = direccionesCliente[i].getFono() + direccionesCliente[i].getAnexo();
                    }
                    else{
                        celularCliente = direccionesCliente[i].getFono();
                    }
                    return true;
	            }
	        }
        } 
	    catch (Exception e) {
	    	if (getLogger().isEnabledFor(Level.WARN)) {
	            getLogger().warn("[validarClienteTieneCelular][" + datoUnicoCliente() + "] [Exception] mensaje de warning =<" + e.getMessage()+">", e);
	        }
            
        }
	    if (getLogger().isEnabledFor(Level.INFO)){
            getLogger().info("[validarClienteTieneCelular] No se encontr� Celular de Cliente [" +  datoUnicoCliente() +"][BCI_FINOK]");
        }
	    return false;
	}
	
	/**
     * <p>
     * M�todo permite enviar token mediante mensaje de texto sms.
     * </p>
     * 
     * Registro de versiones:
     * <ul>
     * <li>1.0 10/07/2017 Luis L�pez Alamos (SEnTRA) - Ricardo Carrasco C�ceres (Ing. Soft. BCI): Versi�n Inicial.</li>
     * </ul>
     * @since 1.0
     */
     private void enviarSmsOTP() {
       if (getLogger().isEnabledFor(Level.INFO)) {
		   getLogger().info("[enviarSmsOTP][" + datoUnicoCliente() + "][BCI_INI]");
	   }
       
       try {
           boolean clienteTieneCelular = validarClienteTieneCelular();
           ClienteMB clienteMB = (ClienteMB) getManagedBean("clienteMB");
       
           if (clienteTieneCelular){
        	   OtpEntrustWSTO otpEntrust = obtenerOTP();
        	   enviarMensajesSMS(otpEntrust);
           } 
       else {
    	    if (getLogger().isEnabledFor(Level.DEBUG)) {
   			    getLogger().debug("[enviarSmsOTP][" + datoUnicoCliente() +"]");
   		    }
        }
       }
       catch (Exception e) {
           if (getLogger().isEnabledFor(Level.WARN ) ) { 
        	   getLogger().warn("[enviarSmsOTP][" + datoUnicoCliente() +"][" + datoUnicoCliente() + "] [Exception] mensaje de warning =<" + e.getMessage()+">", e);
           }
           
       }
               
        if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[enviarSmsOTP][" + datoUnicoCliente() +"][BCI_FINOK]");
		}
    }
     
 	/**
      * <p>
      * M�todo permite enviar el token mediante email.
      * </p>
      * 
      * Registro de versiones:
      * <ul>
      * <li>1.0 10/07/2017 Luis L�pez Alamos (SEnTRA) - Ricardo Carrasco C�ceres (Ing. Soft. BCI): Versi�n Inicial.</li>
      * </ul>
      * @since 1.0
      */
      private void enviarTokenEmail() {
        if (getLogger().isEnabledFor(Level.INFO)) {
 		   getLogger().info("[enviarTokenEmail][" + datoUnicoCliente() + "][BCI_INI]");
 	   }
        
        try {
         	   OtpEntrustWSTO otpEntrust = obtenerOTP();
         	   enviarMensajesEmail(otpEntrust);
        }
        catch (Exception e) {
            if (getLogger().isEnabledFor(Level.WARN ) ) { 
         	   getLogger().warn("[enviarTokenEmail][" + datoUnicoCliente() +"][" + datoUnicoCliente() + "] [Exception] mensaje de warning =<" + e.getMessage()+">", e);
            }
            
        }
                
         if (getLogger().isEnabledFor(Level.INFO)) {
 			getLogger().info("[enviarTokenEmail][" + datoUnicoCliente() +"][BCI_FINOK]");
 		}
     }
     
     
 	/**
 	 * @DescripcionMetodo M�todo permite desenrolar ("logicamente") un dispotivo.
 	 * 
 	 * @EjemploRequest
 	 *  - tokenSerial: "W24M2DP9"
 	 * @EjemploResponse
     * {
     *  "idCodigoEstado": 0,
     *  "estatus": true,
     *  "glosa": null
     * }
 	 *
 	 * @CodigosRespuesta
 	 *  - 200: respuesta OK
 	 *  - 600: Error gen�rico, ver logs para m�s detalles.
 	 *  - 500: Ha ocurrido un error de sistema.
 	 *
 	 * @RegistroVersiones
 	 *
 	 *   - 1.0 | 10/07/2017 | Sergio Flores | Ricardo Carrasco C�ceres | SEnTRA | Version Inicial.
 	 *
 	 * @param token par�metro que representa el token del usuario a consultar.
 	 * @param serialDispositivo par�metro que representa el serialDispositivo a consultar.
 	 * @throws NegocioException Excepci�n espec�fica de negocio.
 	 * @return ResultadoOperacionSegundaClaveTO obtiene resultado del servicio desenrolar.
 	 */       
    @POST
 	@Path("desenrolarDispositivoConToken")
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED) 	
    public ResultadoOperacionSegundaClaveTO desenrolarDispositivoConToken(@FormParam("token") String token,
    		@FormParam("serialDispositivo") String serialDispositivo) throws NegocioException {

       if (getLogger().isEnabledFor(Level.INFO)) {
 		   getLogger().info("[desenrolarTokenUsuario][" + datoUnicoCliente() + "][BCI_INI]");
 	   }
       wcorp.model.seguridad.ResultadoOperacionSegundaClaveTO resultado = null;
       String codigoEvento = null;
       try {
    	   
        if(token != null ){
        	
        	SesionMB sesionMB = (SesionMB) getManagedBean("sesionMB");
   	        ClienteMB clienteMB = (ClienteMB) getManagedBean("clienteMB");
   	        
   	        String canal = sesionMB.getCanalId() != null ? sesionMB.getCanalId() : "";
            String grupoClienteEntrust = TablaValores.getValor(TABLA_DE_PARAMETROS, "pinToken", "desc");
   	        String userId = clienteMB.getFullRut();
   	        
   	        EstrategiaEntrustSoftToken estretegia = new EstrategiaEntrustSoftToken();
   	        
   	        UsuarioSegundaClaveTO usuarioSegundaClave = 
   	        		new UsuarioSegundaClaveTO(userId, Dominio.obtieneDominio(canal).toString(), canal);
   	        
   	        usuarioSegundaClave.setNombreCliente(clienteMB.getFullName());
            usuarioSegundaClave.setGrupoClienteEntrust(grupoClienteEntrust);
         
             ParametrosEstrategiaSegundaClaveTO parametrosEstrategiaSegundaClaveTO = new ParametrosEstrategiaSegundaClaveTO(
         		usuarioSegundaClave);
             
             parametrosEstrategiaSegundaClaveTO.setTipoSegundaClave(TIPO_OTP);
             parametrosEstrategiaSegundaClaveTO.setCodigoConfirmacion(token);
             
             wcorp.model.seguridad.ResultadoOperacionSegundaClaveTO resultadoOperacionSegundaClaveTO = estretegia.autenticar(parametrosEstrategiaSegundaClaveTO);
                  
             if (getLogger().isEnabledFor(Level.DEBUG)) {
                 getLogger().debug("[desenrolarTokenUsuario] [" + datoUnicoCliente() +"], autenticar: " + StringUtil.contenidoDe(resultadoOperacionSegundaClaveTO));
             } 
             
             if(resultadoOperacionSegundaClaveTO.isEstatus()){
                 parametrosEstrategiaSegundaClaveTO.setTokenEntrust(new TokenEntrustTO());
                 parametrosEstrategiaSegundaClaveTO.getTokenEntrust().setSerialNumber(serialDispositivo);
            
                 resultado = estretegia.eliminar(parametrosEstrategiaSegundaClaveTO);
             
                 if (getLogger().isEnabledFor(Level.DEBUG)) {
                     getLogger().debug("[desenrolarTokenUsuario] [" + userId +"], resultado: " + StringUtil.contenidoDe(resultado));
                 } 
             }
         	
         }
        
        codigoEvento = TablaValores.getValor(TABLA_DE_PARAMETROS, "Journal", "codEventoEliminar");
        
        journalizarEnrolarEntrust(PRODUCTO, 
        		codigoEvento, 
        		TablaValores.getValor(TABLA_DE_PARAMETROS, "Journal", "SubCodEventoOK"), 
        		TablaValores.getValor(TABLA_DE_PARAMETROS, "Journal", "estadoOK") , 
        		serialDispositivo); 
        
        if (getLogger().isEnabledFor(Level.INFO)) {
 			getLogger().info("[desenrolarTokenUsuario] [" + datoUnicoCliente() + "] [BCI_FINOK]");
 		 }
         
        return traspasarResultadoOperacionSegundaClaveTO(resultado, MessageBundleUtil.getMensajeBundle(BUNDLE_DEFAULT, "EXITO_DESENROLAR_DISPOSITIVO"));
       }
       catch (NegocioException ex) {
           if (getLogger().isEnabledFor(Level.ERROR)) {
               getLogger().error("[desenrolarTokenUsuario][BCI_FINEX][NegocioException] error con mensaje=<" + ex.getMessage()+">", ex);
           }
		
           journalizarEnrolarEntrust(PRODUCTO, 
        		   codigoEvento, 
    			   TablaValores.getValor(TABLA_DE_PARAMETROS, "Journal", "SubCodEventoNOK"), 
    			   TablaValores.getValor(TABLA_DE_PARAMETROS, "Journal", "estadoNOK") ,
    			   serialDispositivo); 
           throw ex;
       }
       catch (Exception ex) {
    	   if (getLogger().isEnabledFor(Level.ERROR)) {
               getLogger().error("[desenrolarTokenUsuario][BCI_FINEX][Exception] error con mensaje=<" + ex.getMessage()+">", ex);
           }
    	   journalizarEnrolarEntrust(PRODUCTO, 
    			   codigoEvento, 
    			   TablaValores.getValor(TABLA_DE_PARAMETROS, "Journal", "SubCodEventoError"), 
    			   TablaValores.getValor(TABLA_DE_PARAMETROS, "Journal", "estError") ,
    			   serialDispositivo); 
           throw new SistemaException(MessageBundleUtil.getMensajeBundle(BUNDLE_DEFAULT, "ERROR_DE_SISTEMA"));
       }
    }
    
	/**
 	 * @DescripcionMetodo M�todo permite desenrolar ("logicamente") un dispotivo con tarjeta de debito.
 	 * 
 	 * @EjemploRequest
 	 *  - numTarjeta :  "0054"
 	 *  - claveRedBanc: "1234"
 	 *  - serialDispositivo: "62887-12430" 
 	 * @EjemploResponse
     * {
     *  "idCodigoEstado": 0,
     *  "estatus": true,
     *  "glosa": null
     * }
 	 *
 	 * @CodigosRespuesta
 	 *  - 200: respuesta OK
 	 *  - 600: Error gen�rico, ver logs para m�s detalles.
 	 *  - 500: Ha ocurrido un error de sistema.
 	 *
 	 * @RegistroVersiones
 	 *
 	 *   - 1.0 | 10/07/2017 | Sergio Flores | Ricardo Carrasco C�ceres | SEnTRA | Version Inicial.
 	 *
 	 * @param numTarjeta par�metro que representa el numero Tarjeta a consultar.
 	 * @param claveRedBanc par�metro que representa la claveRedBanc a consultar.
 	 * @param serialDispositivo par�metro que representa el serialDispositivo a consultar.
 	 * @throws NegocioException Excepci�n espec�fica de negocio.
 	 * @return ResultadoOperacionSegundaClaveTO obtiene resultado del servicio desenrolar.
 	 */       
    @POST
 	@Path("desenrolarDispositivoConTarjeta")
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED) 	
    public ResultadoOperacionSegundaClaveTO desenrolarDispositivoConTarjeta(
    		@FormParam("numTarjeta") String numTarjeta,
    		@FormParam("claveRedBanc") String claveRedBanc,
    		@FormParam("serialDispositivo") String serialDispositivo) throws NegocioException {

       if (getLogger().isEnabledFor(Level.INFO)) {
 		   getLogger().info("[desenrolarDispositivoConTarjeta][" + datoUnicoCliente() +"] , numTarjeta [" +numTarjeta+ "] , serialDispositivo [" +serialDispositivo+ "][BCI_INI]");
 	   }
       wcorp.model.seguridad.ResultadoOperacionSegundaClaveTO resultado = null;
       ResultadoEnrolamientoWSTO resultadoEnrolamiento=null;  
       String codigoEvento = null;
       try {
    	   codigoEvento = TablaValores.getValor(TABLA_DE_PARAMETROS, "Journal", "codEventoEliminar");
    	   SesionMB sesionMB = (SesionMB) getManagedBean("sesionMB");
  	       ClienteMB clienteMB = (ClienteMB) getManagedBean("clienteMB");
  	       String canal = sesionMB.getCanalId() != null ? sesionMB.getCanalId() : "";
  	       String userId = clienteMB.getFullRut();
  	       
  	       AutenticacionEnrolamientoTO autenticacionEnrolamientoTO = new AutenticacionEnrolamientoTO();
         
           autenticacionEnrolamientoTO.setRut(clienteMB.getRut());
           autenticacionEnrolamientoTO.setCanal(canal);
           autenticacionEnrolamientoTO.setNumeroTarjeta(numTarjeta);
           autenticacionEnrolamientoTO.setClave(claveRedBanc);
         
           resultadoEnrolamiento = validaExisteTarjetaDebito(autenticacionEnrolamientoTO);
         
           if (getLogger().isEnabledFor(Level.DEBUG)) {
         	  getLogger().debug("[desenrolarDispositivoConTarjeta] [" +  datoUnicoCliente() +"], resultadoEnrolamiento: " + StringUtil.contenidoDe(resultadoEnrolamiento));
           }
         
        if(serialDispositivo != null && resultadoEnrolamiento.isEstatus()){
        	
   	        UsuarioSegundaClaveTO usuarioSegundaClave = 
   	        		new UsuarioSegundaClaveTO(userId, Dominio.obtieneDominio(canal).toString(), canal);
         
             ParametrosEstrategiaSegundaClaveTO parametrosEstrategiaSegundaClaveTO = new ParametrosEstrategiaSegundaClaveTO(
         		usuarioSegundaClave);
             
             parametrosEstrategiaSegundaClaveTO.setTokenEntrust(new TokenEntrustTO());
             parametrosEstrategiaSegundaClaveTO.getTokenEntrust().setSerialNumber(serialDispositivo);
            
             EstrategiaEntrustSoftToken estrategiaEntrust = new EstrategiaEntrustSoftToken();
             resultado = estrategiaEntrust.eliminar(parametrosEstrategiaSegundaClaveTO);
             
             if (getLogger().isEnabledFor(Level.DEBUG)) {
                 getLogger().debug("[desenrolarDispositivoConTarjeta] [" + userId +"], resultado: " + StringUtil.contenidoDe(resultado));
             } 
             
             journalizarEnrolarEntrust(PRODUCTO, 
             		codigoEvento, 
             		TablaValores.getValor(TABLA_DE_PARAMETROS, "Journal", "SubCodEventoOK"), 
             		TablaValores.getValor(TABLA_DE_PARAMETROS, "Journal", "estadoOK") , 
             		serialDispositivo); 
         	
         }else {
        	 throw new NegocioException(resultadoEnrolamiento.getGlosa());
         }
        
        if (getLogger().isEnabledFor(Level.INFO)) {
 			getLogger().info("[desenrolarDispositivoConTarjeta] [" + datoUnicoCliente() + "] [BCI_FINOK]");
 		}
         
        return traspasarResultadoOperacionSegundaClaveTO(resultado, MessageBundleUtil.getMensajeBundle(BUNDLE_DEFAULT, "EXITO_DESENROLAR_DISPOSITIVO"));
       }
       catch (NegocioException ex) {
           if (getLogger().isEnabledFor(Level.ERROR)) {
               getLogger().error("[desenrolarDispositivoConTarjeta][BCI_FINEX][NegocioException] error con mensaje=<" + ex.getMessage()+">", ex);
           }
		
           journalizarEnrolarEntrust(PRODUCTO, 
        		   codigoEvento, 
    			   TablaValores.getValor(TABLA_DE_PARAMETROS, "Journal", "SubCodEventoNOK"), 
    			   TablaValores.getValor(TABLA_DE_PARAMETROS, "Journal", "estadoNOK") ,
    			   serialDispositivo); 
           throw ex;
       }
       catch (Exception ex) {
    	   if (getLogger().isEnabledFor(Level.ERROR)) {
               getLogger().error("[desenrolarDispositivoConTarjeta][BCI_FINEX][Exception] error con mensaje=<" + ex.getMessage()+">", ex);
           }
    	   journalizarEnrolarEntrust(PRODUCTO, 
    			   codigoEvento, 
    			   TablaValores.getValor(TABLA_DE_PARAMETROS, "Journal", "SubCodEventoError"), 
    			   TablaValores.getValor(TABLA_DE_PARAMETROS, "Journal", "estError") ,
    			   serialDispositivo); 
           throw new SistemaException(MessageBundleUtil.getMensajeBundle(BUNDLE_DEFAULT, "ERROR_DE_SISTEMA"));
       }
    }
    
	/**
	 * Metodo que pasa los estados segunda clave a EstadoSegundaClaveWSTO
	 * 
	 * *
	 * <p>
	 * Registro de versiones:
	 * <ul>
	  * <li>1.0 10/07/2017 Sergio Flores (SEnTRA) - Ricardo Carrasco C�ceres (Ing. Soft. BCI): Versi�n Inicial.</li>
	 * </ul>
	 * <p>
	 * 
	 * @param retorno EstadoSegundaClaveTO que contiene las cuentas a ser transapasadas.
	 * @param desc String que contiene la descripcion que se enviara por email.
	 * @throws NegocioException Excepci�n espec�fica de negocio.
	 * @throws SistemaException Excepci�n de Sistema.
	 * @throws Exception error al traspasar resultado operacion segunda clave.
	 * @return ResultadoOperacionSegundaClaveTO resultado del servicio bloquear,desbloquear o eliminar un dispotivo.
	 * @since 1.0 
	 */
	private ResultadoOperacionSegundaClaveTO traspasarResultadoOperacionSegundaClaveTO(wcorp.model.seguridad.ResultadoOperacionSegundaClaveTO retorno, String desc) 
			throws NegocioException, SistemaException, Exception{
		if (getLogger().isEnabledFor(Level.INFO)) {
		    getLogger().info("[traspasarResultadoOperacionSegundaClaveTO][" + datoUnicoCliente() + "][BCI_INI][" + retorno + "]");
		}
		
		try {
    	      if(retorno!=null) {
    	    	  if (getLogger().isEnabledFor(Level.INFO)) {
    	    			getLogger().info("[traspasarResultadoOperacionSegundaClaveTO][Code ][" + retorno.isEstatus() + "]");
    	    	  }
    	    	  ResultadoOperacionSegundaClaveTO respuesta = new ResultadoOperacionSegundaClaveTO();
    	    	  
    	    		  if (retorno.isEstatus()) {
    	    			  respuesta.setIdCodigoEstado(retorno.getIdCodigoEstado());
    	    			  respuesta.setEstatus(retorno.isEstatus());
    	    			  respuesta.setGlosa(retorno.getGlosa());
    	    			  enviarMensajesEmailEntrust(desc);
    	    		  } 
    	    		  else if (retorno.getIdCodigoEstado() == CODE_600_ERROR) {
    	    			  throw new NegocioException(MessageBundleUtil.getMensajeBundle(BUNDLE_DEFAULT, "CODE_600_ERROR"));
    	    		  }
    	    		  else {
    	    			  throw new NegocioException(retorno.getGlosa());
    	    		  }
    	    		
    	    		if (getLogger().isEnabledFor(Level.INFO)) {
    	    				getLogger().info("[traspasarResultadoOperacionSegundaClaveTO][" + datoUnicoCliente() +"][BCI_FINOK]");
    	    		}

    	    	  return respuesta;
    	      }
    	      else {
    	    	  if (getLogger().isEnabledFor(Level.INFO)) {
	    				getLogger().info("[traspasarResultadoOperacionSegundaClaveTO][" + datoUnicoCliente() +"][BCI_FINOK]");
	    		}
                  throw new NegocioException(MessageBundleUtil.getMensajeBundle(BUNDLE_DEFAULT, "SIN_ACTIVACION_TOKEN"));
              }
          }
		catch (NegocioException ex) {
	           if (getLogger().isEnabledFor(Level.ERROR)) {
	               getLogger().error("[traspasarResultadoOperacionSegundaClaveTO][BCI_FINEX][NegocioException] error con mensaje=<" + ex.getMessage()+">", ex);
	           }
			

	           throw ex;
	       }
	       catch (Exception ex) {
	    	   if (getLogger().isEnabledFor(Level.ERROR)) {
	               getLogger().error("[traspasarResultadoOperacionSegundaClaveTO][BCI_FINEX][Exception] error con mensaje=<" + ex.getMessage()+">", ex);
	           }
	           throw new SistemaException(MessageBundleUtil.getMensajeBundle(BUNDLE_DEFAULT, "ERROR_DE_SISTEMA"));
	       }
		
	}
    

 	/**
 	 * @DescripcionMetodo M�todo permite bloquear ("logicamente") un dispotivo.
 	 * 
 	 * @EjemploRequest
 	 *  - tokenSerial: "W24M2DP9"
 	 * @EjemploResponse
     * {
     *  "idCodigoEstado": 0,
     *  "estatus": true,
     *  "glosa": null
     * }
 	 *
 	 * @CodigosRespuesta
 	 *  - 200: respuesta OK
 	 *  - 600: Error gen�rico, ver logs para m�s detalles.
 	 *  - 500: Ha ocurrido un error de sistema.
 	 *
 	 * @RegistroVersiones
 	 *
 	 *   - 1.0 | 10/07/2017 | Sergio Flores | Ricardo Carrasco C�ceres | SEnTRA | Version Inicial.
 	 *
 	 * @param tokenSerial par�metro que representa el token del usuario a consultar.
 	 * @throws NegocioException Excepci�n espec�fica de negocio.
 	 * @return ResultadoOperacionSegundaClaveTO obtiene resultado del servicio bloquear.
 	 */ 	
    @POST
 	@Path("bloquearTokenUsuario")
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public ResultadoOperacionSegundaClaveTO bloquearTokenUsuario(@FormParam("tokenSerial") String tokenSerial) throws NegocioException {

       if (getLogger().isEnabledFor(Level.INFO)) {
 		   getLogger().info("[bloquearTokenUsuario][" + datoUnicoCliente() + "][BCI_INI]");
 	   }
        wcorp.model.seguridad.ResultadoOperacionSegundaClaveTO resultado = null;
        String codigoEvento = null;
       try {
        	
        if(tokenSerial != null ){
        	SesionMB sesionMB = (SesionMB) getManagedBean("sesionMB");
   	        ClienteMB clienteMB = (ClienteMB) getManagedBean("clienteMB");
   	        
   	        String canal = sesionMB.getCanalId() != null ? sesionMB.getCanalId() : "";
   	        String userId = clienteMB.getFullRut();
   	        UsuarioSegundaClaveTO usuarioSegundaClave = 
   	        		new UsuarioSegundaClaveTO(userId, Dominio.obtieneDominio(canal).toString(), canal);
         
             ParametrosEstrategiaSegundaClaveTO parametrosEstrategiaSegundaClaveTO = new ParametrosEstrategiaSegundaClaveTO(
         		usuarioSegundaClave);
             
             parametrosEstrategiaSegundaClaveTO.setTokenEntrust(new TokenEntrustTO());
             parametrosEstrategiaSegundaClaveTO.getTokenEntrust().setSerialNumber(tokenSerial);
            
             EstrategiaEntrustSoftToken estrategiaEntrust = new EstrategiaEntrustSoftToken();
             resultado = estrategiaEntrust.bloquear(parametrosEstrategiaSegundaClaveTO);
             
             if (getLogger().isEnabledFor(Level.DEBUG)) {
                 getLogger().debug("[bloquearTokenUsuario] [" + userId +"], resultado: " + StringUtil.contenidoDe(resultado));
             } 
         	
             if (getLogger().isEnabledFor(Level.INFO)) {
     			getLogger().info("[bloquearTokenUsuario][BCI_FINOK]");
     		 }

        }
        codigoEvento = TablaValores.getValor(TABLA_DE_PARAMETROS, "Journal", "codEventobloquear");
        
        journalizarEnrolarEntrust(PRODUCTO, 
        		codigoEvento, 
        		TablaValores.getValor(TABLA_DE_PARAMETROS, "Journal", "SubCodEventoOK"), 
        		TablaValores.getValor(TABLA_DE_PARAMETROS, "Journal", "estadoOK") , 
        		tokenSerial); 
       
        return traspasarResultadoOperacionSegundaClaveTO(resultado, MessageBundleUtil.getMensajeBundle(BUNDLE_DEFAULT, "EXITO_BLOQUEAR_TOKEN"));
       }
       catch (NegocioException ex) {
           if (getLogger().isEnabledFor(Level.ERROR)) {
               getLogger().error("[bloquearTokenUsuario][BCI_FINEX][NegocioException] error con mensaje=<" + ex.getMessage()+">", ex);
           }
		
           journalizarEnrolarEntrust(PRODUCTO, 
        		   codigoEvento, 
    			   TablaValores.getValor(TABLA_DE_PARAMETROS, "Journal", "SubCodEventoNOK"), 
    			   TablaValores.getValor(TABLA_DE_PARAMETROS, "Journal", "estadoNOK") ,
    			   tokenSerial); 
           throw ex;
       }
       catch (Exception ex) {
          if (getLogger().isEnabledFor(Level.ERROR)) {
              getLogger().error("[bloquearTokenUsuario][BCI_FINEX][Exception] error con mensaje=<" + ex.getMessage()+">", ex);
          }
          journalizarEnrolarEntrust(PRODUCTO, 
        	   codigoEvento, 
   			   TablaValores.getValor(TABLA_DE_PARAMETROS, "Journal", "SubCodEventoError"), 
   			   TablaValores.getValor(TABLA_DE_PARAMETROS, "Journal", "estError") ,
   			   tokenSerial); 
          throw new SistemaException(MessageBundleUtil.getMensajeBundle(BUNDLE_DEFAULT, "ERROR_DE_SISTEMA"));
      }
  }
    
 	/**
 	 * @DescripcionMetodo M�todo permite desbloquear ("logicamente") un dispotivo.
 	 * 
 	 * @EjemploRequest
 	 *  - tokenSerial: "W24M2DP9"
 	 * @EjemploResponse
     * {
     *  "idCodigoEstado": 0,
     *  "estatus": true,
     *  "glosa": null
     * }
 	 *
 	 * @CodigosRespuesta
 	 *  - 200: respuesta OK
 	 *  - 600: Error gen�rico, ver logs para m�s detalles.
 	 *  - 500: Ha ocurrido un error de sistema.
 	 *
 	 * @RegistroVersiones
 	 *
 	 *   - 1.0 | 10/07/2017 | Sergio Flores | Ricardo Carrasco C�ceres | SEnTRA | Version Inicial.
 	 *
 	 * @param tokenSerial par�metro que representa el token del usuario a consultar.
 	 * @throws NegocioException Excepci�n espec�fica de negocio.
 	 * @return ResultadoOperacionSegundaClaveTO obtiene resultado del servicio desenrolar.
 	 */ 
     @POST
  	 @Path("desbloquearTokenUsuario")
     @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
     @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
     public ResultadoOperacionSegundaClaveTO desbloquearTokenUsuario(@FormParam("tokenSerial") String tokenSerial) throws NegocioException {

        if (getLogger().isEnabledFor(Level.INFO)) {
  		   getLogger().info("[desbloquearTokenUsuario][" + datoUnicoCliente() + "][BCI_INI]");
  	   }
        wcorp.model.seguridad.ResultadoOperacionSegundaClaveTO resultado = null;
        String codigoEvento = null;
        
        try {
        	
        	  if(tokenSerial != null ){
         	      SesionMB sesionMB = (SesionMB) getManagedBean("sesionMB");
    	          ClienteMB clienteMB = (ClienteMB) getManagedBean("clienteMB");
    	        
    	          String canal = sesionMB.getCanalId() != null ? sesionMB.getCanalId() : "";
    	          String userId = clienteMB.getFullRut();
    	          UsuarioSegundaClaveTO usuarioSegundaClave = 
    	    		  new UsuarioSegundaClaveTO(userId, Dominio.obtieneDominio(canal).toString(), canal);
          
                  ParametrosEstrategiaSegundaClaveTO parametrosEstrategiaSegundaClaveTO = new ParametrosEstrategiaSegundaClaveTO(
          		    usuarioSegundaClave);
              
                  parametrosEstrategiaSegundaClaveTO.setTokenEntrust(new TokenEntrustTO());
                  parametrosEstrategiaSegundaClaveTO.getTokenEntrust().setSerialNumber(tokenSerial);
             
                  EstrategiaEntrustSoftToken estrategiaEntrust = new EstrategiaEntrustSoftToken();
                  resultado = estrategiaEntrust.desBloquear(parametrosEstrategiaSegundaClaveTO);

                  if (getLogger().isEnabledFor(Level.DEBUG)) {
                      getLogger().debug("[desbloquearTokenUsuario] [" + userId +"], resultado: " + StringUtil.contenidoDe(resultado));
                  } 
              	
             }
        	  
        	  codigoEvento = TablaValores.getValor(TABLA_DE_PARAMETROS, "Journal", "codEventoDesbloquea");
        	  
        	  journalizarEnrolarEntrust(PRODUCTO, 
        			codigoEvento, 
              		TablaValores.getValor(TABLA_DE_PARAMETROS, "Journal", "SubCodEventoOK"), 
              		TablaValores.getValor(TABLA_DE_PARAMETROS, "Journal", "estadoOK") , 
              		tokenSerial); 
        	  
        	if (getLogger().isEnabledFor(Level.INFO)) {
        		getLogger().info("[desbloquearTokenUsuario][BCI_FINOK]");
        	}
             return traspasarResultadoOperacionSegundaClaveTO(resultado, MessageBundleUtil.getMensajeBundle(BUNDLE_DEFAULT, "EXITO_DESBLOQUEAR_TOKEN"));
        }
        catch (NegocioException ex) {
            if (getLogger().isEnabledFor(Level.ERROR)) {
                getLogger().error("[desbloquearTokenUsuario][BCI_FINEX][NegocioException] error con mensaje=<" + ex.getMessage()+">", ex);
            }
 		
            journalizarEnrolarEntrust(PRODUCTO, 
            		codigoEvento, 
     			   TablaValores.getValor(TABLA_DE_PARAMETROS, "Journal", "SubCodEventoNOK"), 
     			   TablaValores.getValor(TABLA_DE_PARAMETROS, "Journal", "estadoNOK") ,
     			  tokenSerial);  
            throw ex;
        }
        catch (Exception ex) {
           if (getLogger().isEnabledFor(Level.ERROR)) {
               getLogger().error("[desbloquearTokenUsuario][BCI_FINEX][Exception] error con mensaje=<" + ex.getMessage()+">", ex);
           }
           journalizarEnrolarEntrust(PRODUCTO, 
        		   codigoEvento, 
       			   TablaValores.getValor(TABLA_DE_PARAMETROS, "Journal", "SubCodEventoError"), 
       			   TablaValores.getValor(TABLA_DE_PARAMETROS, "Journal", "estError") ,
       			   tokenSerial); 
           throw new SistemaException(MessageBundleUtil.getMensajeBundle(BUNDLE_DEFAULT, "ERROR_DE_SISTEMA"));
       }
       
    }
     
 	/**
 	 * M�todo encargado de journalizar entrust.
 	 * <p>
 	 * Registro de versiones:
 	 * <ul>
 	 * <li>1.0 10/07/2017 Sergio Flores (SEnTRA) - Ricardo Carrasco C�ceres (Ing. Soft. BCI): Versi�n Inicial.</li>
 	 * </ul>
 	 * <p>
 	 * @param producto solicitado para la journalizacion.
 	 * @param codEventoNegocio solicitado para la journalizacion.
 	 * @param subCodEventoNegocio solicitado para la journalizacion.
 	 * @param estadoEvento solicitado para la journalizacion.
 	 * @param campoVariableJournal solicitado para la journalizacion.
 	 * @since 1.0
 	 */
     private void journalizarEnrolarEntrust(String producto, String codEventoNegocio, String subCodEventoNegocio, String estadoEvento, String campoVariableJournal) {
         if (getLogger().isEnabledFor(Level.INFO)) {
             getLogger().info("[journalizarEnrolarEntrust][" + datoUnicoCliente() + "][BCI_INI]");
         }
         try {
        	 
        	  SesionMB sesionMB = (SesionMB) getManagedBean("sesionMB");
	          ClienteMB clienteMB = (ClienteMB) getManagedBean("clienteMB");
	          String canal = sesionMB.getCanalId() != null ? sesionMB.getCanalId() : "";
	          
	          String idMedio = (ConectorStruts.getRemoteAddr().length() > LARGO_ID_MEDIO)
	                  ? ConectorStruts.getRemoteAddr().substring(POSICION_INICIAL_ID_MEDIO,LARGO_ID_MEDIO)
	                          : ConectorStruts.getRemoteAddr(); 
              
              if (getLogger().isEnabledFor(Level.DEBUG)) {
                  getLogger().debug("[journalizarEnrolarEntrust][" + datoUnicoCliente() + "] idMedio ["+ idMedio + "]");
              }
	          
             Map datosJournalizacion = new HashMap();
             datosJournalizacion.put("rut", String.valueOf(clienteMB.getRut()));
             datosJournalizacion.put("dv",   String.valueOf(clienteMB.getDigitoVerif()));
             datosJournalizacion.put("canal", canal);
             
             datosJournalizacion.put("idProducto", producto);
             datosJournalizacion.put("codEventoNegocio", codEventoNegocio);
             datosJournalizacion.put("subCodEventoNegocio", subCodEventoNegocio);
             datosJournalizacion.put("estadoEvento", estadoEvento);
             datosJournalizacion.put("idMedio", idMedio);
             datosJournalizacion.put("campoVariable", campoVariableJournal);
             Journalist journalist = Journalist.getInstance();
             journalist.publicar(datosJournalizacion);

             
             if (getLogger().isEnabledFor(Level.DEBUG)) {
                 getLogger().debug("[journalizarEnrolarEntrust][" + datoUnicoCliente() + "] datosJournalizacion ["+ StringUtil.contenidoDe(datosJournalizacion) + "]");
             }

         }
         catch (Exception e) {
             if (getLogger().isEnabledFor(Level.WARN)) {
                 getLogger().warn("[journalizarEnrolarEntrust] [" + datoUnicoCliente() + "] [Exception]  mensaje de warning : ", e);
             }

         }
         if (getLogger().isEnabledFor(Level.INFO)) {
             getLogger().info("[journalizarEnrolarEntrust][" + datoUnicoCliente() + "][BCI_FINOK]");
         }
     }
 	
     /**
      *  M�todo encargado de obtener datos del cliente.
      * <p>
      * Este m�todo devuelve el dato unico de cliente correspondiente a su rut, con el fin de ser utilizado para logueo de la clase.
      * Registro de versiones:
      * <ul>
      * <li>1.0 10/07/2017 Sergio Flores (SEnTRA) - Ricardo Carrasco C�ceres (Ing. Soft. BCI): Versi�n Inicial.</li>
      * </ul>
      * <p>
      * @return String dato utilizado para loguear.
      * @since 1.0
      */
     private String datoUnicoCliente(){
    	 if (getLogger().isEnabledFor(Level.INFO)) {
             getLogger().info("[datoUnicoCliente][BCI_INI]");
         }
    	 if (datoUnicoLog==null){
    		 ClienteMB clienteMB = (ClienteMB) getManagedBean("clienteMB");
             if (clienteMB!=null && clienteMB.getRut()!=0){
                 datoUnicoLog = String.valueOf(clienteMB.getRut());
             }
             else {
                 datoUnicoLog = NO_SE_OBTUVO_CLIENTEMB;
             }
             if (getLogger().isEnabledFor(Level.DEBUG)) {
                 getLogger().debug("[datoUnicoCliente] Seteo valor de rut de cliente : " + datoUnicoLog);
             }
         }
    	 
    	 if (getLogger().isEnabledFor(Level.INFO)) {
             getLogger().info("[datoUnicoCliente][BCI_FINOK]");
         }
         return datoUnicoLog;
     }
    
    /**
     * Metodo que Retorna una Instancia del EJB ServiciosTarjetas
     * <p>
     * Registro de versiones: 
     * <ul>
     * <li>1.0 10/07/2017 Sergio Flores (SEnTRA) - Ricardo Carrasco C�ceres (Ing. Soft. BCI): Versi�n Inicial.</li>
     * </ul>
     * <p> 
     * 
     * @throws GeneralException Error al obtener instacia de ServiciosTarjetas. 
     * @return ServiciosTarjetas retorna instancia de EJB ServiciosTarjetas
     * @since 1.0
     */
    private ServiciosTarjetas srvTarjetas() throws GeneralException {
        if (getLogger().isEnabledFor(Level.INFO)) {
 		   getLogger().info("[srvTarjetas][BCI_INI]");
 	   	}
    	
        wcorp.serv.tarjetas.ServiciosTarjetasHome servTarjetasHome = null;
        String jndiName = "wcorp.serv.tarjetas.ServiciosTarjetas";
        Class homeClass = wcorp.serv.tarjetas.ServiciosTarjetasHome.class;
        ServiciosTarjetas servTarjetas = null;
        try {
            servTarjetasHome = (ServiciosTarjetasHome) EnhancedServiceLocator.getInstance().getHome(jndiName,
                homeClass);
            servTarjetas = servTarjetasHome.create();
            if (getLogger().isEnabledFor(Level.DEBUG)) {
            	getLogger().debug("[srvTarjetas]:: recupera servTarjetas desde EnhancedServiceLocator1");	
            }
        }
        catch (Exception ex) {
            
         	if(getLogger().isEnabledFor(Level.WARN)){
                getLogger().warn("[srvTarjetas][Exception] mensaje de warning [" + ex.getMessage()+"]", ex);
        }
        	
            try {
                EnhancedServiceLocator.getInstance().clear(servTarjetasHome);
                servTarjetasHome = (ServiciosTarjetasHome) EnhancedServiceLocator.getInstance().getHome(jndiName,
                    homeClass);
                servTarjetas = servTarjetasHome.create();
                if (getLogger().isEnabledFor(Level.DEBUG)) {
                	getLogger().debug("[srvTarjetas]:: recupera servTarjetas desde EnhancedServiceLocator2");	
                }
                
            }
            catch (Exception e) {
             	if(getLogger().isEnabledFor(Level.ERROR)){
                    getLogger().error("[srvTarjetas][BCI_FINEX][Exception]2 No se pudo recuperar la instancia de [" 
                    		+ jndiName + "][" + e.getMessage()+"]", e);
                }
            	
                throw new GeneralException("ESPECIAL", "No se pudo cargar informacion :" + jndiName);
            }
        }
	    if (getLogger().isEnabledFor(Level.INFO)) {
			getLogger().info("[srvTarjetas][BCI_FINOK]");
	    }
        
        return servTarjetas;
    }
    
    /**
     * <p>M�todo para validar si cliente corresponde a MonoProducto (Tarjeta de Cr�dito).</p>
     *
     * <p>Registro de versiones:</p><ul>
     * <li>1.0 10/07/2017 Sergio Flores (SEnTRA) - Ricardo Carrasco C�ceres (Ing. Soft. BCI): Versi�n Inicial.</li>
     * </ul>
     *
     * <B>Todos los derechos reservados por Banco de Cr�dito e Inversiones.</B>
     *
     * @param rut Rut del Cliente MonoProducto.
     * @param dv Digito Verificador del Cliente MonoProducto.
     * @throws Exception si ocurre un error en la validaci�n.
     * @return boolean verdadero si es cliente monoProducto.
     */
	private boolean validarClienteMonoproducto(long rut, char dv)
			throws Exception {
	    if (getLogger().isInfoEnabled()) {
            getLogger().info("[validarClienteMonoproducto] [BCI_INI] [" + rut + "] rut=<" + rut 
                    + ">, dv=<" + dv + ">");
		}
	    ClienteMB clienteMB = (ClienteMB) getManagedBean("clienteMB");
		char tipoCliente = clienteMB.getTipoUsuario().charAt(0);
		if (getLogger().isDebugEnabled()) {
            getLogger().debug("[validarClienteMonoproducto] [" + rut + "] tipoCliente=<" + tipoCliente + ">");
        }
		boolean esTDC = true;
		boolean esCCT = false;
		boolean esCPR = false;

		try {
			Operacion[] operacionesCliente = this.getServiciosCliente().consultaOperaciones(rut, dv, tipoCliente);
			if (getLogger().isDebugEnabled()) {
			    int largo = operacionesCliente!=null?operacionesCliente.length:0;
                getLogger().debug("[validarClienteMonoproducto] [" + rut + "] operacionesCliente=<"
                        + StringUtil.contenidoDe(operacionesCliente) + ">, largo=<" + largo+ ">");
			}
			for (int i = 0; i < operacionesCliente.length; i++) {
				if (OPERACION_VIGENTE.equals(operacionesCliente[i].codEstado)
						&& OPERACION_CUENTA_CORRIENTE.equals(operacionesCliente[i].tipoOperacion)) {
					esCCT = true;
					esTDC = false;
					break;
				} 
				else if (OPERACION_VIGENTE.equals(operacionesCliente[i].codEstado)
						&& OPERACION_CUENTA_PRIMA.equals(operacionesCliente[i].tipoOperacion)) {
					esCPR = true;
					esTDC = false;
					break;
				}
			}
			if (getLogger().isDebugEnabled()) {
                getLogger().debug("[validarClienteMonoproducto] [" + rut + "] esCPR=<" + esCPR + ">, esCCT=<"
                        + esCCT + ">");
			}

			if (!esCCT && !esCPR) {
				ResultadoServicioCuentasVO cuentasCliente = this.getTCreditoOnline().getCuentasPorRut(rut, dv);
				if (getLogger().isDebugEnabled()) {
                    getLogger().debug("[validarClienteMonoproducto] [" + rut + "] obtiendo cuentasCliente ["+ StringUtil.contenidoDe(cuentasCliente) + "]");
				}
				if (cuentasCliente != null && cuentasCliente.getCuentas() != null 
						&& cuentasCliente.getCuentas().length > 0) {
					if (getLogger().isDebugEnabled()) {
                        getLogger().debug("[validarClienteMonoproducto] [" + rut + "] obtiendo cuenta TDC.");
					}
					esTDC = true;
				} 
				else {
					if (getLogger().isEnabledFor(Level.WARN)) {
                        getLogger().warn("[validarClienteMonoproducto] [" + rut + "] cliente sin TDC.");
					}
					esTDC = false;
				}
			} 
			else {
				if (getLogger().isEnabledFor(Level.WARN)) {
                    getLogger().warn("[validarClienteMonoproducto] [" + rut + "] cliente no monoproducto tdc");
				}
				esTDC = false;
			}
		} 
		catch (GeneralException e) {
		    if (getLogger().isEnabledFor(Level.ERROR)) {
                getLogger().error("[validarClienteMonoproducto] [" + rut + "] [GeneralException] [BCI_FINEX] "
                        + e.getMessage(), e);
            }
			throw e;
		} 
		catch (Exception ex) {
		    if (getLogger().isEnabledFor(Level.ERROR)) {
                getLogger().error("[validarClienteMonoproducto] [" + rut + "] [Exception] [BCI_FINEX] Lanzando nueva Exception. "
                        + ex.getMessage(), ex);
            }
			throw new Exception(TablaValores.getValor("errores.codigos", "EDDA-015", "Desc"), ex);
		}
		if (getLogger().isInfoEnabled()) {
            getLogger().info("[validarClienteMonoproducto] [" + rut + "] [BCI_FINOK] esTDC=<" + esTDC + ">");
		}
		return esTDC;
    }
	
	/**
	 * <p>M�todo para obtener instancia de ejb de ServiciosCliente.</p>
	 * <br>
	 * Registro de versiones:<ul>
     * <li>1.0 10/07/2017 Sergio Flores (SEnTRA) - Ricardo Carrasco C�ceres (Ing. Soft. BCI): Versi�n Inicial.</li>
	 * </ul>
	 * @return instancia del ejb ServiciosCliente.
	 * @throws Exception en caso de error de la creacion del servicio.
	 * @since 1.0
	 */
	private ServiciosCliente getServiciosCliente() throws Exception {
		ClienteMB clienteMB = (ClienteMB) getManagedBean("clienteMB");
		long rut = clienteMB.getRut();
	    if (getLogger().isInfoEnabled()) {
            getLogger().info("[getServiciosCliente] [" + rut + "] [BCI_INI]");
        }
		if (serviciosCliente == null) {
		    if (getLogger().isDebugEnabled()) {
                getLogger().debug("[getServiciosCliente] [" + rut + "] servicioCliente es null.");
            }
			try {
				Class<ServiciosCliente> homeClass = ServiciosCliente.class;
				EnhancedServiceLocator locator = EnhancedServiceLocator.getInstance();
				ServiciosClienteHome serviciosClienteHome = (ServiciosClienteHome) locator.getGenericService(
								"wcorp.serv.clientes.ServiciosCliente", homeClass);
				serviciosCliente = serviciosClienteHome.create();
				if (getLogger().isDebugEnabled()) {
                    getLogger().debug("[getServiciosCliente] [" + rut + "] servicio instanciado");
                }
			} 
			catch (Exception ex) {
			    if (getLogger().isEnabledFor(Level.ERROR)) {
                    getLogger().error("[getServiciosCliente] [" + rut 
                            + "] [Exception] [BCI_FINEX] lanzo nueva Exception. " + ex.getMessage(), ex);
				}
				throw new Exception("Error al crear instancia de ejb ServiciosCliente", ex);
			}
		} 
		if (getLogger().isInfoEnabled()) {
            getLogger().info("[getServiciosCliente] [" + rut + "] [BCI_FINOK] retorno instancia.");
        }
			return serviciosCliente;
	}
	
	/**
	 * <p>M�todo para obtener instancia de ejb TCreditoOnline.</p>
	 * <br>
	 * Registro de versiones:<ul>
     * <li>1.0 10/07/2017 Sergio Flores (SEnTRA) - Ricardo Carrasco C�ceres (Ing. Soft. BCI): Versi�n Inicial.</li>
	 * </ul>
	 * @return instancia del ejb
	 * @throws Exception en caso de error de la creacion del servicio
	 * @since 1.0
	 */
	private TCreditoOnline getTCreditoOnline() throws Exception {
		ClienteMB clienteMB = (ClienteMB) getManagedBean("clienteMB");
		long rut = clienteMB.getRut();
	    if (getLogger().isInfoEnabled()) {
            getLogger().info("[getTCreditoOnline] [" + rut + "] [BCI_INI]");
        }
		if (tCreditoOnline == null) {
		    if (getLogger().isDebugEnabled()) {
                getLogger().debug("[getTCreditoOnline] [" + rut + "] tCreditoOnline es null.");
            }
			try {
				Class<TCreditoOnline> homeClass = TCreditoOnline.class;
				EnhancedServiceLocator locator = EnhancedServiceLocator.getInstance();
				TCreditoOnlineHome tCreditoOnlineHome = (TCreditoOnlineHome) locator.getGenericService(
				        "wcorp.bprocess.tcreditoonline.TCreditoOnline", homeClass);
				tCreditoOnline = tCreditoOnlineHome.create();
				if (getLogger().isDebugEnabled()) {
                    getLogger().debug("[getTCreditoOnline] [" + rut + "] servicio instanciado");
                }
			} 
			catch (Exception ex) {
				if (getLogger().isEnabledFor(Level.ERROR)) {
                    getLogger().error("[getTCreditoOnline] [" + rut 
                            + "] [Exception] [BCI_FINEX] lanzo nueva Exception. " + ex.getMessage(), ex);
				}
				throw new Exception("Error al crear instancia de ejb TCreditoOnline",ex);
			}
		}
		if (getLogger().isInfoEnabled()) {
            getLogger().info("[getTCreditoOnline] [" + rut + "] [BCI_FINOK] retorno instancia");
        }
			return tCreditoOnline;
	}

    
	   /**
	    * M�todo que setea la clase a loggear.
	    * <p>
	    * Registro de versiones: 
	    * <ul>
	    * <li>1.0 10/07/2017 Sergio Flores (SEnTRA) - Ricardo Carrasco C�ceres (Ing. Soft. BCI): Versi�n Inicial.</li>
	    * </ul>
	    * <p> 
	    * @return Logger retorna el log.
	    * @since 1.0
	    */
	    private Logger getLogger() {
	        if (logger == null) {
	            logger = Logger.getLogger(EnrolamientoEntrustWS.class);
	        }
	        return logger;
	    }
	    
	    public String getCelularCliente(){
	        return this.celularCliente;
	    }
}


