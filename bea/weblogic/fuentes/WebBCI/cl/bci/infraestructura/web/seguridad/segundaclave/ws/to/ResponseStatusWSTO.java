package cl.bci.infraestructura.web.seguridad.segundaclave.ws.to;

import java.io.Serializable;

/**
 * 
 * <b>ResponseStatusWSTO</b>
 * <p>
 *       Representa el resultado sobre algun registro o dato en Entrust - Fuse.
 * </p>
 * 
 * <p>
 * Registro de versiones:
 * </p>
 * 
 * <ul>
 *   <li>1.0 10/07/2017 Sergio Flores (Sentra) - Ricardo Carrasco (Ing. Soft. BCI) : versi�n inicial.</li>
 * </ul>
 * 
 * <p>
 * <b>Todos los derechos reservados por Banco de Cr�dito e Inversiones.</b>
 * </p>
 * 
 */
public class ResponseStatusWSTO implements Serializable {
	/**
	 * N�mero de versi�n utilizado durante la serializaci�n.
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Campo codigo de respuesta.
	 */
	private int code;
	
	/**
	 * Campo descripcion de respuesta.
	 */
	private String description;

	/**
	 * Regitro fue reflejado (ingresado, modificado, eliminado) en tandem.
	 */
	private boolean resultadoTandem = false;
	
	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public boolean isResultadoTandem() {
		return resultadoTandem;
	}

	public void setResultadoTandem(boolean resultadoTandem) {
		this.resultadoTandem = resultadoTandem;
	}
    
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("ResponseStatusWSTO [code=");
		sb.append(code);
		sb.append(", description=");
		sb.append(description);
		sb.append(", resultadoTandem=");
		sb.append(resultadoTandem);		
		sb.append("]");
		return sb.toString();
	}

}
