package wcorp.model.seguridad;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Hashtable;

import javax.ejb.CreateException;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import wcorp.infraestructura.seguridad.ServiciosSegundaClave;
import wcorp.infraestructura.seguridad.ServiciosSegundaClaveBean;
import wcorp.infraestructura.seguridad.ServiciosSegundaClaveHome;
import wcorp.model.seguridad.to.ResponseStatusTO;
import wcorp.model.seguridad.to.TokenEntrustTO;
import wcorp.serv.seguridad.SeguridadException;
import wcorp.serv.seguridad.ServiciosSeguridad;
import wcorp.serv.seguridad.ServiciosSeguridadHome;
import wcorp.util.EnhancedServiceLocator;
import wcorp.util.EnhancedServiceLocatorException;
import wcorp.util.GeneralException;
import wcorp.util.RUTUtil;
import wcorp.util.StringUtil;
import wcorp.util.TablaValores;

/**
 * <b>Clase abstracta encargada de dar soporte a componentes entrust softtoken y entrust token f�sico</b>
 * <p>
 * Registro de versiones:
 * <ul>
 *  <li>1.0 10/07/2017 Luis L�pez Alamos - Jaime Suazo D�az (SEnTRA) - Ricardo Carrasco C�ceres (Ing. Soft. BCI): Versi�n Inicial.</li>
 *  <li>2.0 20/10/2017 Jaime Suazo D�az (SEnTRA) - Ricardo Carrasco C�ceres (Ing. Soft. BCI): Se modifica el m�todo 
 *          {@link #consultaEstadoDeSegundaClave(ParametrosEstrategiaSegundaClaveTO, PrioridadSegundaClaveIdTO)}, adem�s, se crearon
 *          los m�todos {@link #isEstadoNoPermitidoEntrust(String)}, {@link #isEstadoActivacionPermitidoEntrust(String)}</li>
 *  
 * </ul>
 * <b>Todos los derechos reservados por Banco de Cr�dito e Inversiones.</b>
 * <p>
 */
public abstract class EstrategiaAbstractaEntrust implements EstrategiaSegundaClave {
    
    
    /**
     * prefijo utilizado para manejar error le�dos de la tabla errores.codigos.
     */
    protected static final String PREFIJO_ENTRUST = "ENTRUST_";
    /**
     * tabla de par�metros de entrust.
     */
    protected static final String TABLA_DE_PARAMETROS = "entrust.parametros";
    
    /**
     * tabla de par�metros de safesigner.
     */
    protected static final String TABLA_DE_PARAMETROS_SAFESIGNER = "safesigner.parametros";
    
    /**
     * Indica la posicion del arreglo que contiene el parametro de ubicacion para el cambio de estado en TANDEM.
     */
    protected static final int POSICION_UBICACION_CAMBIO_ESTADO = 0;
    
    /**
     * Flag registrado en entrust.parametros para saber si la llave de la tabla TANDEM TBPSC02 fue modificada.
     */
    protected static final String CAMBIO_LLAVE_TBPSC02 = "CAMBIOLLAVETBPSC02";
    
    /**
     * Indica la posicion del arreglo que contiene el parametro con el valor para el cambio de estado.
     */
    protected static final int POSICION_PARAMETRO_CAMBIO_ESTADO = 1;
    
    /**
	 * Nombre de accion entrust para bloquedar.
	 */
	protected static final String ACCION_BLOQUEAR = "BLOQUEAR";

	/**
	 * Nombre de accion entrust para habilitar.
	 */
	private static final String ACCION_HABILITAR = "HABILITAR";
	
	/**
	 * Nombre JNDI del ejb de ServiciosSeguridad.
	 */
	private static final String JNDI_NAME_SEGURIDAD = TablaValores.getValor(TABLA_DE_PARAMETROS, "ejbSeguridad", "desc");
	
	/**
     * Nombre JNDI del ejb de ServiciosSegundaClave.
     */
	private static final String JNDI_NAME_SEGUNDA_CLAVE = TablaValores.getValor(TABLA_DE_PARAMETROS, "ejbSegundaClave", "desc");
	
	/**
	 * Nombre de accion entrust para habilitar.
	 */
	private static final String ACCION_ELIMINAR = "ELIMINADO";

	/**
	 * Valor cero.
	 */
	private static final int VALOR_CERO = 0;
	

	/**
	 * Instancia EJB ServiciosSegundaClave.
	 */
	protected ServiciosSegundaClave serviciosSegundaClave;
    
    /**
	 * Instancia EJB ServiciosSeguridad.
     */
	protected ServiciosSeguridad serviciosSeguridad;

    /**
     * Log de la clase.
     */
    private transient Logger logger = (Logger) Logger.getLogger(EstrategiaAbstractaEntrust.class);
    

    /**
     * Implementaci�n nula de m�todo #{@link #generarLlave(String)}, se realiza de esta forma ya que no es necesaria para entrust.
     * <p>
     * 
     * Registro de Versiones:
     * <ul>
     * <li>1.0 10/07/2017 Luis L�pez Alamos (SEnTRA) - Ricardo Carrasco C�ceres (Ing. Soft. BCI): Versi�n Inicial.</li>
     * </ul>
     * <P>
     * @param idUsuario sin implementaci�n concreta.
     * @return null sin implementaci�n concreta.
     * @throws GeneralException excepcion.
     * @since 1.0
     */
    public ResultadoOperacionSegundaClaveTO generarLlave(String idUsuario) throws GeneralException {
        return null;
    }

    /**
     * Implementaci�n nula de m�todo #{@link #activar(ParametrosEstrategiaSegundaClaveTO)}, se realiza de esta forma ya que no es necesaria para entrust.
     * <p>
     * 
     * Registro de Versiones:
     * <ul>
     * <li>1.0 10/07/2017 Luis L�pez Alamos (SEnTRA) - Ricardo Carrasco C�ceres (Ing. Soft. BCI): Versi�n Inicial.</li>
     * </ul>
     * <P>
     * @param parametroEstrategiaSegundaClave sin implementaci�n concreta.
     * @return null sin implementaci�n concreta.
     * @throws GeneralException excepcion.
     * @since 1.0
     */
    public ResultadoOperacionSegundaClaveTO activar(ParametrosEstrategiaSegundaClaveTO parametroEstrategiaSegundaClave) throws GeneralException {
        return null;
    }
    
    /**
     * M�todo que bloquea un soft token entrust, es decir, le asigna el estado HOLD.
     * <p>
     * 
     * Registro de Versiones:
     * <ul>
     * <li>1.0 10/07/2017 Luis L�pez Alamos (SEnTRA) - Ricardo Carrasco C�ceres (Ing. Soft. BCI): Versi�n Inicial.</li>
     * </ul>
     * <P>
     * @param parametroEstrategiaSegundaClave sin implementaci�n concreta.
     * @return resultado de la operacion de bloqueo.
     * @throws GeneralException excepcion.
     * @since 1.0
     */
    public ResultadoOperacionSegundaClaveTO bloquear(ParametrosEstrategiaSegundaClaveTO parametroEstrategiaSegundaClave) throws GeneralException {
        if (getLogger().isEnabledFor(Level.INFO)) {
            getLogger().info("[bloquear][BCI_INI][" + parametroEstrategiaSegundaClave + "]");
        }
        
        if (parametroEstrategiaSegundaClave.getUsuarioSegundaClave() != null) {
            if (getLogger().isEnabledFor(Level.DEBUG)) {
                getLogger().debug("[bloquear][" + parametroEstrategiaSegundaClave.getUsuarioSegundaClave().getRutEntrust() + "]");
            }
        	
        	if (parametroEstrategiaSegundaClave.getUsuarioSegundaClave().getGrupoClienteEntrust() == null 
        			|| parametroEstrategiaSegundaClave.getUsuarioSegundaClave().getGrupoClienteEntrust().trim().equals("")) {

        		String grupoClienteEntrust = TablaValores.getValor(TABLA_DE_PARAMETROS, "pinToken", "desc");
        		parametroEstrategiaSegundaClave.getUsuarioSegundaClave().setGrupoClienteEntrust(grupoClienteEntrust);
        	}
        }
        
        if (parametroEstrategiaSegundaClave.getTokenEntrust() != null ) {
            if (getLogger().isEnabledFor(Level.DEBUG)) {
                getLogger().debug("[bloquear][" + parametroEstrategiaSegundaClave.getTokenEntrust().getSerialNumber() + "]");
            }
            
            String estadoNuevo = TablaValores.getValor(TABLA_DE_PARAMETROS, ACCION_BLOQUEAR, "desc");
            parametroEstrategiaSegundaClave.getTokenEntrust().setEstado(estadoNuevo);
        }
        
        ResponseStatusTO respuestaServicio = actualizarTokenUsuario(parametroEstrategiaSegundaClave);
        
        ResultadoOperacionSegundaClaveTO respuesta = new ResultadoOperacionSegundaClaveTO();
        
        respuesta.setEstatus(respuestaServicio.getCode() == 0);
        respuesta.setGlosa(respuestaServicio.getDescription());
        respuesta.setIdCodigoEstado(respuestaServicio.getCode());
            	
        if (getLogger().isEnabledFor(Level.INFO)) {
            getLogger().info("[bloquear][BCI_FINOK][" + respuesta + "]");
        }
        
        return respuesta;
    }

    /**
     * M�todo que desbloquea un soft token entrust, es decir, le asigna el estado CURRENT.
     * <p>
     * 
     * Registro de Versiones:
     * <ul>
     * <li>1.0 10/07/2017 Luis L�pez Alamos (SEnTRA) - Ricardo Carrasco C�ceres (Ing. Soft. BCI): Versi�n Inicial.</li>
     * </ul>
     * <P>
     * @param parametroEstrategiaSegundaClave sin implementaci�n concreta.
     * @return resultado de la operacion de desbloqueo.
     * @throws GeneralException excepcion.
     * @since 1.0
     */
    public ResultadoOperacionSegundaClaveTO desBloquear(
            ParametrosEstrategiaSegundaClaveTO parametroEstrategiaSegundaClave) throws GeneralException {
        if (getLogger().isEnabledFor(Level.INFO)) {
            getLogger().info("[desBloquear][BCI_INI][" + parametroEstrategiaSegundaClave + "]");
        }
        
        if (parametroEstrategiaSegundaClave.getUsuarioSegundaClave() != null) {
            if (getLogger().isEnabledFor(Level.DEBUG)) {
                getLogger().debug("[desBloquear][" + parametroEstrategiaSegundaClave.getUsuarioSegundaClave().getRutEntrust() + "]");
            }
        	
        	if (parametroEstrategiaSegundaClave.getUsuarioSegundaClave().getGrupoClienteEntrust() == null 
        			|| parametroEstrategiaSegundaClave.getUsuarioSegundaClave().getGrupoClienteEntrust().trim().equals("")) {

        		String grupoClienteEntrust = TablaValores.getValor(TABLA_DE_PARAMETROS, "pinToken", "desc");
        		parametroEstrategiaSegundaClave.getUsuarioSegundaClave().setGrupoClienteEntrust(grupoClienteEntrust);
        	}
        }
        
        if (parametroEstrategiaSegundaClave.getTokenEntrust() != null ) {
            if (getLogger().isEnabledFor(Level.DEBUG)) {
                getLogger().debug("[desBloquear][" + parametroEstrategiaSegundaClave.getTokenEntrust().getSerialNumber() + "]");
            }
            
            String estadoNuevo = TablaValores.getValor(TABLA_DE_PARAMETROS, ACCION_HABILITAR, "desc");
            parametroEstrategiaSegundaClave.getTokenEntrust().setEstado(estadoNuevo);
        }
        
        ResponseStatusTO respuestaServicio = actualizarTokenUsuario(parametroEstrategiaSegundaClave);
        
        ResultadoOperacionSegundaClaveTO respuesta = new ResultadoOperacionSegundaClaveTO();
        
        respuesta.setEstatus(respuestaServicio.getCode() == 0);
        respuesta.setGlosa(respuestaServicio.getDescription());
        respuesta.setIdCodigoEstado(respuestaServicio.getCode());
            	
        if (getLogger().isEnabledFor(Level.INFO)) {
            getLogger().info("[desBloquear][BCI_FINOK][" + respuesta + "]");
        }
        
        return respuesta;
    }

    /**
     * Implementaci�n nula de m�todo #{@link #eliminar(ParametrosEstrategiaSegundaClaveTO)}, se realiza de esta forma ya que no es necesaria para entrust.
     * <p>
     * 
     * Registro de Versiones:
     * <ul>
     * <li>1.0 10/07/2017 Luis L�pez Alamos (SEnTRA) - Ricardo Carrasco C�ceres (Ing. Soft. BCI): Versi�n Inicial.</li>
     * </ul>
     * <P>
     * @param parametroEstrategiaSegundaClave sin implementaci�n concreta.
     * @return null sin implementaci�n concreta.
     * @throws GeneralException excepcion.
     * @since 1.0
     */
    public ResultadoOperacionSegundaClaveTO eliminar(ParametrosEstrategiaSegundaClaveTO parametroEstrategiaSegundaClave) throws GeneralException {
        if (getLogger().isEnabledFor(Level.INFO)) {
            getLogger().info("[eliminar][BCI_INI][" + parametroEstrategiaSegundaClave + "]");
        }
        
        if (parametroEstrategiaSegundaClave.getUsuarioSegundaClave() != null) {
            if (getLogger().isEnabledFor(Level.DEBUG)) {
                getLogger().debug("[eliminar][" + parametroEstrategiaSegundaClave.getUsuarioSegundaClave().getRutEntrust() + "]");
            }
        	
        	if (parametroEstrategiaSegundaClave.getUsuarioSegundaClave().getGrupoClienteEntrust() == null 
        			|| parametroEstrategiaSegundaClave.getUsuarioSegundaClave().getGrupoClienteEntrust().trim().equals("")) {

        		String grupoClienteEntrust = TablaValores.getValor(TABLA_DE_PARAMETROS, "pinToken", "desc");
        		parametroEstrategiaSegundaClave.getUsuarioSegundaClave().setGrupoClienteEntrust(grupoClienteEntrust);
        	}
        }
        
        if (parametroEstrategiaSegundaClave.getTokenEntrust() != null ) {
            if (getLogger().isEnabledFor(Level.DEBUG)) {
                getLogger().debug("[eliminar][" + parametroEstrategiaSegundaClave.getTokenEntrust().getSerialNumber() + "]");
            }
            
            String estadoNuevo = TablaValores.getValor(TABLA_DE_PARAMETROS, ACCION_ELIMINAR, "desc");
            parametroEstrategiaSegundaClave.getTokenEntrust().setEstado(estadoNuevo);
        }
        
        ResponseStatusTO respuestaServicio = actualizarTokenUsuario(parametroEstrategiaSegundaClave);
        
        ResultadoOperacionSegundaClaveTO respuesta = new ResultadoOperacionSegundaClaveTO();
        
        respuesta.setEstatus(respuestaServicio.getCode() == 0);
        respuesta.setGlosa(respuestaServicio.getDescription());
        respuesta.setIdCodigoEstado(respuestaServicio.getCode());
            	
        if (getLogger().isEnabledFor(Level.INFO)) {
            getLogger().info("[eliminar][BCI_FINOK][" + respuesta + "]");
        }
        
        return respuesta;
    }

    /**
     * Implementaci�n nula de m�todo #{@link #registrar(ResultadoOperacionSegundaClaveTO)}, se realiza de esta forma ya que no es necesaria para entrust.
     * <p>
     * 
     * Registro de Versiones:
     * <ul>
     * <li>1.0 10/07/2017 Luis L�pez Alamos (SEnTRA) - Ricardo Carrasco C�ceres (Ing. Soft. BCI): Versi�n Inicial.</li>
     * </ul>
     * <P>
     * @param parametroRegistro sin implementaci�n concreta.
     * @return null sin implementaci�n concreta.
     * @throws GeneralException excepcion.
     * @since 1.0
     */
    public ResultadoOperacionSegundaClaveTO registrar(RegistrarDispositivoTO parametroRegistro) throws GeneralException {
        return null;
    }

    /**
     * Implementaci�n nula de m�todo #{@link #generarCodigoAutorizacion(ParametrosEstrategiaSegundaClaveTO)}, se realiza de esta forma ya que no es necesaria para entrust.
     * <p>
     * 
     * Registro de Versiones:
     * <ul>
     * <li>1.0 10/07/2017 Luis L�pez Alamos (SEnTRA) - Ricardo Carrasco C�ceres (Ing. Soft. BCI): Versi�n Inicial.</li>
     * </ul>
     * <P>
     * @param params sin implementaci�n concreta.
     * @return null sin implementaci�n concreta.
     * @throws GeneralException excepcion.
     * @since 1.0
     */
    public ResultadoOperacionSegundaClaveTO generarCodigoAutorizacion(ParametrosEstrategiaSegundaClaveTO params) throws GeneralException {
        return null;
    }
    
    /**
     * Implementaci�n nula de m�todo #{@link #consultarEstadoSegundaClave(String)}, se realiza de esta forma ya que no es necesaria para entrust.
     * <p>
     * 
     * Registro de Versiones:
     * <ul>
     * <li>1.0 10/07/2017 Luis L�pez Alamos (SEnTRA) - Ricardo Carrasco C�ceres (Ing. Soft. BCI): Versi�n Inicial.</li>
     * </ul>
     * <P>
     * @param idUsuario sin implementaci�n concreta.
     * @return null sin implementaci�n concreta.
     * @throws GeneralException excepcion.
     * @since 1.0
     */
    public EstadoSegundaClaveTO consultarEstadoSegundaClave(String idUsuario) throws GeneralException {
        return null;
    }

    /**
     * Implementaci�n nula de m�todo #{@link #validarCodigoAutorizacion(ParametrosEstrategiaSegundaClaveTO)}, se realiza de esta forma ya que no es necesaria para entrust.
     * <p>
     * 
     * Registro de Versiones:
     * <ul>
     * <li>1.0 10/07/2017 Luis L�pez Alamos (SEnTRA) - Ricardo Carrasco C�ceres (Ing. Soft. BCI): Versi�n Inicial.</li>
     * </ul>
     * <P>
     * @param params sin implementaci�n concreta.
     * @return null sin implementaci�n concreta.
     * @throws GeneralException excepcion.
     * @since 1.0
     */
    public ResultadoOperacionSegundaClaveTO validarCodigoAutorizacion(ParametrosEstrategiaSegundaClaveTO params) throws GeneralException {
        return null;
    }
    
    
    /**
     * Metodo que obtiene el un valor (ejemplo un estado) para alguna accion en tandem.
     * <br><br>
     * 
     * Estos valores son para actuar principalmente en la tabla TANDEM TBPSC02.
     * 
     * 
     * Registro de versiones:
     * <ul>
     * <li>1.0 10/07/2017 Jaime Suazo. (SEnTRA) - Ricardo Carrasco (Ing. Soft. BCI): versi�n inicial.</li>
     * </ul>
     * 
     * @param nombreValorParaTandem estado entrust.
     * @return valor para tandem. 
     * @since 1.0
     */        
    protected String obtenerValorParaAccionEnTandem(String nombreValorParaTandem) {
        if (getLogger().isEnabledFor(Level.INFO)) {
            getLogger().info("[obtenerValorParaAccionEnTandem][BCI_INI][" + nombreValorParaTandem + "]");
        }
        
        String valorParaTandem = TablaValores.getValor(TABLA_DE_PARAMETROS, 
                String.valueOf(nombreValorParaTandem), "desc");
        
        if (getLogger().isEnabledFor(Level.DEBUG)) {
            getLogger().debug("[obtenerValorParaAccionEnTandem][descripcionErrorEntrust][" 
                    + valorParaTandem + "] [" + nombreValorParaTandem + "]");
        }
        
        if (getLogger().isEnabledFor(Level.INFO)) {
            getLogger().info("[obtenerValorParaAccionEnTandem][BCI_FINOK][" + nombreValorParaTandem + "][" + valorParaTandem + "]");
        }
        
        return valorParaTandem;
    }
    
    
    /**
     * <p>
     * M�todo que obtiene el objeto getLogger().
     * </p>
     * 
     * Registro de versiones:
     * <ul>
     * <li>1.0 10/07/2017 Luis L�pez Alamos (SEnTRA) - Ricardo Carrasco C�ceres (Ing. Soft. BCI): Versi�n Inicial.</li>
     * </ul>
     * <p>
     * 
     * @return Logger Objeto de log
     * @since 1.0
     */
    private Logger getLogger() {
        if ( logger == null ) {
            logger = Logger.getLogger( this.getClass() );
        }
        return logger;
    }

    
    /**
     * Metodo para consultarEstadoSegundaClave.
     * <p>
     * Registro de versiones: 
     * <ul>
     * <li>1.0 10/07/2017 Luis L�pez Alamos (SEnTRA) - Ricardo Carrasco (Ing. Soft. BCI): Version inicial.</li>
     * <li>2.0 20/10/2017 Jaime Suazo D (SEnTRA) - Ricardo Carrasco (Ing. Soft. BCI): Se realiza correci�n de agregaci�n de dispositivo
     *         a lista de resultado, s�lo se agregan dispositivos que su ESTADO no se encuentren en entrust.parametros llave 
     *         <code>estadosEntrustNoPermitidos</code> y que su ESTADO DE ACTIVACI�N se encuentre en llave
     *         <code>estadosActivacionEntrustPermitidos</code>. Por ejemplo, se agregan dispositivos distintos de estado <code><b>CANCELED</b></code>
     *         y con estado de activaci�n <code><b>ACTIVATED</b></code>.<br><br>
     *         Para efectos de la estatus de la segunda clave, s�lo ser� seteado en TRUE cuando alguno de los dispositivos del
     *         cliente se encuentre en estado <code><b>CURRENT</b></code> y con estado de activaci�n <code><b>ACTIVATED</code></b>
     *         <br><br>
     *         El estado de activaci�n <code><b>ACTIVATED</b></code> significa que el cliente enrol� o enlaz� su dispositivo a trav�s de la aplicaci�n m�vil
     *         KIM-IT contra el registro que cre� el IDG (en estado <code><b>PENDING</b></code> estado de activaci�n <code><b>CREATED</b></code>) 
     *         al momento de hacer el scanning del QR en el enrolamiento.
     *         </li>
     * </ul>
     * <p> 
     * @param parametroEstrategiaSegundaClave parametros para ejecutar la consulta de estado.
     * @return EstadoSegundaClaveTO[] lista de token usuario.
     * @throws SeguridadException error lanzado por el metodo.
     * @since 1.0
     */
    protected EstadoSegundaClaveTO consultaEstadoDeSegundaClave(ParametrosEstrategiaSegundaClaveTO parametroEstrategiaSegundaClave, PrioridadSegundaClaveIdTO idSegundaClave) throws SeguridadException {
        if (getLogger().isEnabledFor(Level.INFO)){
            getLogger().info("[consultarEstadoSegundaClave][BCI_INI] " + parametroEstrategiaSegundaClave);
        }
        
    	String rutCliente = null;
            EstadoSegundaClaveTO resultadoEstado = new EstadoSegundaClaveTO();
            resultadoEstado.setIdSegundaClave(idSegundaClave);

    	String dv = null;
    	if (parametroEstrategiaSegundaClave.getCliente() != null) {
    		rutCliente = String.valueOf(parametroEstrategiaSegundaClave.getCliente().getRut());
    		dv = String.valueOf(parametroEstrategiaSegundaClave.getCliente().getDigito());
    	}
    	else if (parametroEstrategiaSegundaClave.getUsuarioSegundaClave() != null) {
    		rutCliente = String.valueOf(parametroEstrategiaSegundaClave.getUsuarioSegundaClave().getRut());
    		dv = String.valueOf(parametroEstrategiaSegundaClave.getUsuarioSegundaClave().getDv());
    	}
    	else {
    		if (getLogger().isEnabledFor(Level.ERROR)) {
    			getLogger().error("[consultarEstadoSegundaClave] [" + rutCliente + "] [BCI_FINEX] ");
    		}
    		throw new IllegalArgumentException("ParametrosEstrategiaSegundaClaveTO invalido");
    	}

    	Hashtable metodosAutenticacion = new Hashtable();
    	metodosAutenticacion.put("CLI", new Boolean(true));
    	
            try {
                String grupoClienteEntrust = TablaValores.getValor(TABLA_DE_PARAMETROS, "pinToken", "desc");
    		String codTipProd = null;
    		
    		
    		if ((idSegundaClave.getNombre() != null) && (idSegundaClave.getNombre().equalsIgnoreCase("EntrustSoftToken"))) {
    			codTipProd = TablaValores.getValor(TABLA_DE_PARAMETROS, "SOFTTOKEN", "valor" );	
    		}
    		else if ((idSegundaClave.getNombre() != null) && (idSegundaClave.getNombre().equalsIgnoreCase("EntrustToken"))) {
    			codTipProd = TablaValores.getValor(TABLA_DE_PARAMETROS, "EntrustToken", "valor" );
    		}
    		
                String serialSafeSigner = TablaValores.getValor(TABLA_DE_PARAMETROS, "parametrosTANDEM", "serial" );
                String[] cambioEstado = StringUtil.divide(TablaValores.getValor(TABLA_DE_PARAMETROS, "CAMBIO_ESTADO", "HABILITADO"), ',');
                
                parametroEstrategiaSegundaClave.getUsuarioSegundaClave().setGrupoClienteEntrust(grupoClienteEntrust);
                if( getLogger().isDebugEnabled()) {
                    getLogger().debug("[consultarEstadoSegundaClave] parametroEstrategiaSegundaClave[" + parametroEstrategiaSegundaClave + "] ");
                 }
                
                serviciosSegundaClave = obtenerServiciosSegundaClave();
                
                TokenEntrustTO[] tokenEntrustTO = serviciosSegundaClave.listarTokenUsuario(parametroEstrategiaSegundaClave);
                
                if( getLogger().isDebugEnabled()) {
                    getLogger().debug("[consultarEstadoSegundaClave] tokenEntrustTO[" + tokenEntrustTO + "] ");
                }
                
                EstadoSegundaClaveTO[] estadoSegundaClaveTO = serviciosSegundaClave.consultarEstadoSegundaClave(
    				rutCliente, dv, codTipProd);
                
                if( getLogger().isDebugEnabled()) {
                    getLogger().debug("[consultarEstadoSegundaClave] estadoSegundaClaveTO[" + estadoSegundaClaveTO + "] ");
                 }
                
                serviciosSeguridad = this.obtenerServiciosSeguridad();
                
                ArrayList listaSofttokens = new ArrayList();
                
                String cambioLLave = this.obtenerValorParaAccionEnTandem(CAMBIO_LLAVE_TBPSC02);
                boolean status = (cambioLLave != null && !cambioLLave.equalsIgnoreCase("OK")) ? true : false;
                
                if(status){
                        if((cambioLLave != null && cambioLLave.equalsIgnoreCase("OK")) &&estadoSegundaClaveTO != null && estadoSegundaClaveTO.length > 0 && tokenEntrustTO != null && tokenEntrustTO.length > 0 ) {
                            int i=0;
                            for(; i<tokenEntrustTO.length; i++){
                                int x=0;
                                for(; x<estadoSegundaClaveTO.length; x++){
                                    if(tokenEntrustTO[i].getSerialNumber().equals(estadoSegundaClaveTO[x].getSerial()) 
                                            && tokenEntrustTO[i].getEstado().equals("PENDING") &&  estadoSegundaClaveTO[x].isEstatus() ==false){
                                        
                                        status =  serviciosSeguridad.modificarRegistroSegundaClave(String.valueOf(
                                        parametroEstrategiaSegundaClave.getUsuarioSegundaClave().getRut()), 
                                        serialSafeSigner, 
                                        codTipProd,
                                        cambioEstado[POSICION_UBICACION_CAMBIO_ESTADO], cambioEstado[POSICION_PARAMETRO_CAMBIO_ESTADO]);
                                        
                                        if( getLogger().isDebugEnabled()) {
                                            getLogger().debug("[consultarEstadoSegundaClave] status[" + status + "] ");
                                         }
                                    }
                                }
                            }
                        }
                }   
                     
                
                    if(tokenEntrustTO != null && tokenEntrustTO.length > 0 ){
                        int i=0;
                        String estadoCurrent = TablaValores.getValor(TABLA_DE_PARAMETROS, "CURRENT", "valor");
    				
                        for(;i<tokenEntrustTO.length; i++){
    				if (tokenEntrustTO[i].getTipoDispositivo().equalsIgnoreCase(codTipProd) 
    						&& !isEstadoNoPermitidoEntrust(tokenEntrustTO[i].getEstado()) 
    						&& isEstadoActivacionPermitidoEntrust(tokenEntrustTO[i].getEstadoActivacion()) ) {

                                listaSofttokens.add(tokenEntrustTO[i]);
                        }
                       
                        if (tokenEntrustTO[i].getTipoDispositivo().equalsIgnoreCase(codTipProd) 
    						&& tokenEntrustTO[i].getEstado().equalsIgnoreCase(estadoCurrent)
    						&& isEstadoActivacionPermitidoEntrust(tokenEntrustTO[i].getEstadoActivacion()) ) {
                        	
                                resultadoEstado.setEstatus(true);
                        }
                    }
                        
    				resultadoEstado.setListaDispositivos((TokenEntrustTO[]) listaSofttokens.toArray(new TokenEntrustTO[0]));
                    }
    		
    		resultadoEstado.setMetodosAutenticacion(metodosAutenticacion);
                }  
            catch (Exception se) {
                if (getLogger().isEnabledFor(Level.ERROR)){
                    getLogger().error("[consultarEstadoSegundaClave][BCI_FINEX] Error al consultar estado:" + se.getMessage(), se);
                }
                throw new SeguridadException(se.getMessage());
            }
            
            if (getLogger().isEnabledFor(Level.INFO)){
                getLogger().info("[consultarEstadoSegundaClave][BCI_FINOK] " + resultadoEstado);
            }
            
            return resultadoEstado;
            
        }
    
    /**
     * M�todo que determina si un estado de activaci�n entrust dado de un dispositivo est� permitido 
     * para efectos de la consulta de estado de la segunda clave entrust.  
     * 
     * <p>
     * Registro de versiones: 
     * <ul>
     * <li>1.0 19/07/2017 Jaime Suazo D (SEnTRA) - Ricardo Carrasco (Ing. Soft. BCI): Version inicial.</li>
     * </ul>
     * <p>  
     *  
     * @param estadoActivacionEntrust estado entrust (ACTIVATED, CREATED).
     * @return si el estado es permitido.
     */
    private boolean isEstadoActivacionPermitidoEntrust(String estadoActivacionEntrust) {
    	if (getLogger().isEnabledFor(Level.INFO)) {
    		getLogger().info("[isEstadoActivacionPermitidoEntrust][BCI_INI][" + estadoActivacionEntrust + "]");
    	}
    	
    	boolean resultado = false;
    	String listaEstadosActivacionPermitidos = TablaValores.getValor(TABLA_DE_PARAMETROS, "estadosActivacionEntrustPermitidos", "desc");
    	
    	if (getLogger().isEnabledFor(Level.DEBUG)) {
    		getLogger().debug("[isEstadoActivacionPermitidoEntrust][" + estadoActivacionEntrust +"][" + listaEstadosActivacionPermitidos+ "]");
    	}

    	if (estadoActivacionEntrust!= null && listaEstadosActivacionPermitidos != null) {
    		String[] arregloEstadosActivacionPermitidos = StringUtil.divide(listaEstadosActivacionPermitidos, ",");
    		int indice = VALOR_CERO;
    		
    		for (; indice < arregloEstadosActivacionPermitidos.length; indice++) {
				if (arregloEstadosActivacionPermitidos[indice].equalsIgnoreCase(estadoActivacionEntrust)) {
					resultado = true;					
				}
			}
    	}
    	
    	if (getLogger().isEnabledFor(Level.INFO)) {
    		getLogger().info("[isEstadoActivacionPermitidoEntrust][BCI_FINOK][" + estadoActivacionEntrust +"][resultado][" + resultado+ "]");
    	}
    	
    	return resultado;
    }
    
    /**
     * M�todo que determina si un estado entrust dado de un dispositivo no est� permitido 
     * para efectos de la consulta de estado de la segunda clave entrust. 
     * 
     * <p>
     * Registro de versiones: 
     * <ul>
     * <li>1.0 19/07/2017 Jaime Suazo D (SEnTRA) - Ricardo Carrasco (Ing. Soft. BCI): Version inicial.</li>
     * </ul>
     * <p>  
     * 
     * @param estadoEntrust estado entrust (HOLD, PENDING, CURRENT, CANCELED).
     * @return si el estado no es permitido.
     */
    private boolean isEstadoNoPermitidoEntrust(String estadoEntrust) {
    	if (getLogger().isEnabledFor(Level.INFO)) {
    		getLogger().info("[isEstadoPermitidoEntrust][BCI_INI][" + estadoEntrust + "]");
    	}
    	
    	boolean resultado = false;
    	
    	String listaEstadosNoPermitidos = TablaValores.getValor(TABLA_DE_PARAMETROS, "estadosEntrustNoPermitidos", "desc");

    	if (getLogger().isEnabledFor(Level.DEBUG)) {
    		getLogger().debug("[isEstadoPermitidoEntrust][" + estadoEntrust +"][" + listaEstadosNoPermitidos+ "]");
    	}

    	if (estadoEntrust!= null && listaEstadosNoPermitidos != null) {
    		String[] arregloEstadosNoPermitidos = StringUtil.divide(listaEstadosNoPermitidos, ",");
    		int indice = VALOR_CERO;
    		
    		for (; indice < arregloEstadosNoPermitidos.length; indice++) {
				if (arregloEstadosNoPermitidos[indice].equalsIgnoreCase(estadoEntrust)) {
					resultado = true;					
				}
			}
    	}
    	
    	if (getLogger().isEnabledFor(Level.INFO)) {
    		getLogger().info("[isEstadoPermitidoEntrust][BCI_FINOK][" + estadoEntrust +"][resultado][" + resultado+ "]");
    	}
    	
    	return resultado;
    }
    
    
    /**
     * Metodo que actualiza (cambia el estado) de un softoken de un cliente dado.
     * <br><br>
     * En caso de error el servicio retornara una instancia de TokenEntrustTO nula. 
     * 
     * 
     * Registro de versiones:
     * <ul>
     * <li>1.0 10/07/2017 Jaime Suazo. (SEnTRA) - Ricardo Carrasco (Ing. Soft. BCI): versi�n inicial.</li>
     * </ul>
     * 
     * @param parametroEstrategiaSegundaClave datos del token a modificar su estado. 
     * @return ResponseStatusTO objeto con el resultado de la ejecucion del servicio en fuse v�a entrust.  
     * @since 1.0
     * @see ServiciosSegundaClaveBean#actualizarTokenUsuario(TokenEntrustTO)
     */        
    private ResponseStatusTO actualizarTokenUsuario(ParametrosEstrategiaSegundaClaveTO parametroEstrategiaSegundaClave) {
        if (getLogger().isEnabledFor(Level.INFO)) {
            getLogger().info("[actualizarTokenUsuario][BCI_INI][" + parametroEstrategiaSegundaClave + "]");
        }
        ResponseStatusTO resultado = null;
        
        String userId = obtenerRutCliente(parametroEstrategiaSegundaClave);

        if (parametroEstrategiaSegundaClave == null || parametroEstrategiaSegundaClave.getTokenEntrust() == null) {
        	throw new IllegalArgumentException("ParametrosEstrategiaSegundaClaveTO TokenEntrust invalido");
        }
        
    	try {
    	    serviciosSegundaClave = obtenerServiciosSegundaClave();
    		resultado = serviciosSegundaClave.actualizarTokenUsuario(parametroEstrategiaSegundaClave);
    		String cambioLLave = this.obtenerValorParaAccionEnTandem(CAMBIO_LLAVE_TBPSC02);
    		
            if (getLogger().isEnabledFor(Level.DEBUG)) {
                getLogger().debug( "[actualizarTokenUsuario][cambioLLave][" + cambioLLave +"]" );
            }                
    		
    		if (resultado.getCode() == 0 && (cambioLLave != null && cambioLLave.equalsIgnoreCase("OK"))) {
    			
    		    serviciosSeguridad = this.obtenerServiciosSeguridad();
    			String estadoParaTandem = this.obtenerValorParaAccionEnTandem(parametroEstrategiaSegundaClave.getTokenEntrust().getEstado());
    			
                if (getLogger().isEnabledFor(Level.DEBUG)) {
                    getLogger().debug( "[actualizarTokenUsuario][" + userId 
                    	+ "] estadoParaTandem [" + estadoParaTandem +"]" );
                }                
    			    			
                String[] cambioEstado = StringUtil.divide(
                        TablaValores.getValor(TABLA_DE_PARAMETROS_SAFESIGNER, "CAMBIO_ESTADO", estadoParaTandem), ',');
                
                if (getLogger().isEnabledFor(Level.DEBUG)) {
                    getLogger().debug( "[actualizarTokenUsuario][" + userId + "] cambioEstado [" + StringUtil.contenidoDe( cambioEstado ) +"]" );
                }                
    			
                boolean bloquearSegundaClave = serviciosSeguridad.modificarRegistroSegundaClave(userId, 
                		parametroEstrategiaSegundaClave.getTokenEntrust().getSerialNumber(), 
                		parametroEstrategiaSegundaClave.getTokenEntrust().getTipoDispositivo(), 
    					cambioEstado[POSICION_UBICACION_CAMBIO_ESTADO], 
    					cambioEstado[POSICION_PARAMETRO_CAMBIO_ESTADO]);
                
                resultado.setResultadoTandem(bloquearSegundaClave);
                
                if (getLogger().isEnabledFor( Level.DEBUG )){
                    getLogger().debug( "[actualizarTokenUsuario][" + userId + "] resultado en tandem [" 
                    		+ bloquearSegundaClave + "]");
                }
    		}
    		
		} 
    	catch (Exception e) {
            if (getLogger().isEnabledFor(Level.ERROR)) {
                getLogger().error("[actualizarTokenUsuario][BCI_FINEX][Exception][" + userId 
                	+ "]" + e.getMessage(), e);
            }			
		}
    	
        if (getLogger().isEnabledFor(Level.INFO)) {
            getLogger().info("[actualizarTokenUsuario][BCI_FINOK][" + userId + "]");
        }
        
        return resultado;
    }
    
    /**
     * M�todo que obtiene el RUT de cliente BCI.
     * <p>
     * Registro de versiones: 
     * <ul>
     * <li>1.0 10/07/2017 Sergio Flores (SEnTRA) - Ricardo Carrasco (Ing. Soft. BCI): Version inicial.</li>
     * </ul>
     * <p> 
     * @param parametroEstrategiaSegundaClave datos del cliente.
     * @return RUT de cliente BCI.
     * @since 1.0
     */
	private String obtenerRutCliente(ParametrosEstrategiaSegundaClaveTO parametroEstrategiaSegundaClave) {
		String userId = null; 
    	
    	if (parametroEstrategiaSegundaClave.getCliente() != null) {
    		userId = String.valueOf(parametroEstrategiaSegundaClave.getCliente().getRut());
    	}
    	else if (parametroEstrategiaSegundaClave.getUsuarioSegundaClave() != null) {
    		userId = parametroEstrategiaSegundaClave.getUsuarioSegundaClave().getRutEntrust();
    	}
    	else {
    		if (getLogger().isEnabledFor(Level.ERROR)) {
    			getLogger().error("[consultarEstadoSegundaClave][" + userId + "][BCI_FINEX] ");
    		}
    		throw new IllegalArgumentException("ParametrosEstrategiaSegundaClaveTO invalido");
    	}
		return userId;
	}    

    /**
     * M�todo encargado de retornar una instancia del EJB de ServiciosSegundaClave
     * <p>
     * Registro de versiones:<ul>
     * 
     * <li>1.0 10/07/2017 Jaime Suazo. (SEnTRA) - Ricardo Carrasco (Ing. Soft. BCI): versi�n inicial.</li>
     * 
     * </ul>
     * <p>
     *  @return instancia del ejb servicioRiesgo. 
     *  @throws RemoteException error remoto.
     *  @throws EnhancedServiceLocatorException error de service locator.
     *  @throws CreateException error de creaci�n.
     *  @since 1.0
     */
    protected ServiciosSegundaClave obtenerServiciosSegundaClave() throws RemoteException, EnhancedServiceLocatorException, CreateException {
        if (getLogger().isEnabledFor(Level.INFO)) {
            getLogger().info("[obtenerServiciosSegundaClave][BCI_INI]");
        }
        if (this.serviciosSegundaClave==null){
        ServiciosSegundaClaveHome serviciosSegundaClavedHome = null;
        serviciosSegundaClavedHome = (ServiciosSegundaClaveHome) 
                    EnhancedServiceLocator.getInstance().getHome(JNDI_NAME_SEGUNDA_CLAVE, ServiciosSegundaClaveHome.class);
            serviciosSegundaClave = serviciosSegundaClavedHome.create();
        }
        
        if (getLogger().isEnabledFor(Level.INFO)) {
            getLogger().info("[obtenerServiciosSegundaClave][BCI_FINOK]");
        }
        
        return serviciosSegundaClave;
    }
    
    /**
     * M�todo encargado de retornar una instancia del EJB de ServiciosSeguridad
     * <p>
     * Registro de versiones:<ul>
     * 
     * <li>1.0 10/07/2017 Jaime Suazo. (SEnTRA) - Ricardo Carrasco (Ing. Soft. BCI): versi�n inicial.</li>
     * 
     * </ul>
     * <p>
     *  @return instancia del ejb servicioRiesgo. 
     *  @throws RemoteException error remoto.
     *  @throws EnhancedServiceLocatorException error de service locator.
     *  @throws CreateException error de creaci�n.
     *  @since 1.0
     */
	protected ServiciosSeguridad obtenerServiciosSeguridad() throws RemoteException, EnhancedServiceLocatorException, CreateException {
        if (getLogger().isEnabledFor(Level.INFO)) {
            getLogger().info("[obtenerServiciosSeguridad][BCI_INI]");
        }
		if (this.serviciosSeguridad==null){
		ServiciosSeguridadHome serviciosSeguridadHome = null;
		serviciosSeguridadHome = (ServiciosSeguridadHome) 															 
				EnhancedServiceLocator.getInstance().getHome(JNDI_NAME_SEGURIDAD, ServiciosSeguridadHome.class);
		        this.serviciosSeguridad = serviciosSeguridadHome.create();
		}
        if (getLogger().isEnabledFor(Level.INFO)) {
            getLogger().info("[obtenerServiciosSeguridad][BCI_FINOK]");
        }
        
        return serviciosSeguridad;
	}   
	    
}
