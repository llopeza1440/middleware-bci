<%@ page language="java" %>
<%@taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@taglib uri="/WEB-INF/fmt.tld" prefix="fmt"%>
<jsp:useBean id="sessionBci" scope="session" class="wcorp.model.seguridad.SessionBCI"></jsp:useBean>
<jsp:setProperty name="sessionBci" property="*" />
<fmt:setBundle basename="resources.messages-PagueDirecto"/>

<html>
	<head>
		<title>BCI</title>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="description" content="">
		<meta name="viewport" content="width=device-width,initial-scale=1">
		<link href="/bcinetwls/common/css/frames.css" rel="stylesheet" type="text/css">
		<script type="text/javascript" src="/bcinetwls/common/js/bcipassOffline.js"></script>
		<script type="text/javascript" src="/bcinetwls/common/js/jquery.js"></script>
	</head>
	
	<body onLoad="javascript:inicializar();cargaQRInicial();">

		<div class="entrust_container dos">
			<form name="PagueDirectoForm" id="form" action="/servlet/paguedirecto">
				<input type="hidden" name="canal" value="entrustSoftToken">
				<input type="hidden" name="modoAutenticacion" value="OFFLINE">
				<input type="hidden" name="servicioQR" id="servicioQR" value="<c:out value="${servicioQR}"/>" />
				
				<c:if test="${!empty imagenesqr}">
					<p class="parrafo2"><fmt:message key="entrustSoftToken.contenido.textoPrincipalOffline" /></p>
					<div class="col qr">
						<img id="imgQr">
					</div>

					<div class="col form">
						<div class="desc"><fmt:message key="entrustSoftToken.contenido.textoComboBox" /></div>

						<p>
							<select name="codigosqr" class="input" onchange="mostrarImagenQr(this.value);">
								<c:forEach var="item" items="${imagenesqr}">
									<option value="<c:out value="${item.claveAlfanumerica}"/>" <c:if test="${status.first}">selected</c:if>><c:out value="${item.nombreDispositivoAsociado}"/></option>
								</c:forEach>
							</select>
						</p>
						<p>
							<input type="password" id="codigoConfirmacionQR" name="codigoConfirmacionQR" placeholder="<fmt:message key="entrustSoftToken.contenido.placeholder" />" class="input" pattern="[0-9.]+" maxlength="8">
						</p>

						<div class="paso"><fmt:message key="entrustSoftToken.contenido.cantCaracteres" /></div>

					</div>

					<button id="aceptar" class="aceptar disabled" disabled><fmt:message key="entrustSoftToken.boton.aceptar" /></button>

					<div id="loading"></div>
				
				</c:if>
				
				<c:if test="${empty imagenesqr}">
					<div id="error" class="alert hide alert-danger">
						<i class="fa fa-times" aria-hidden="true"></i>
						<p>
							<strong><fmt:message key="entrustSoftToken.error.titulo" /></strong>
							<br>
							<fmt:message key="entrustSoftToken.error.imagenQr" />
						</p>
					</div>
				</c:if>
			</form>
		</div>

	</body>
</html>

<%--
/**************************************************************************************************************************
* Archivo:     autenticacionEntrustSoftTokenOffline.jsp
* Descripci�n: Se utiliza para la autenticaci�n Entrust SoftToken Offline.
* Path:        /bcinetwls/paguedirecto/entrustsofttoken
* " Todos los derechos reservados por Banco de Cr�dito e Inversiones."              
* Historia Cambios
* -------------------------
* versi�n       fecha         autor                                                       cambios
* =======   ==========  ==========================================================        =================================
* 1.0       02-08-2017  Marcelo Fuentes (SEnTRA) - Ricardo Carrasco (Ing. Soft. BCI)      versi�n inicial. Ser� modificada.
*
**************************************************************************************************************************/
--%>
