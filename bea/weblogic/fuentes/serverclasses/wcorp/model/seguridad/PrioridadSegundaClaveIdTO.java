package wcorp.model.seguridad;

import java.io.Serializable;

/**
 * 
 * <b>PrioridadSegundaClaveIdTO</b>
 * <p>
 * Estructura para obtener el id de prioridad de segunda clave.
 * </p>
 * 
 * <p>Registro de versiones:</p>
 * 
 * <ul>
 * <li> 1.0 28/04/2013, Victor Hugo Enero (Orand): version inicial</li>
 * <li> 1.1 12/12/2016 Darlyn Delgado P�rez (SEnTRA) - Daniel Araya Ureta (Ing. Soft. BCI): Se modifica m�todo {@link #getPorNombre(String)} y se agrega atributo {@link #ETOKENMULTIBROWSER}.</li>
 * <li> 1.2 10/07/2017 Luis L�pez Alamos (SEnTRA) - Ricardo Carrasco C�ceres (Ing. Soft. BCI): Se modifica m�todo {@link #getPorNombre(String)} y se agrega atributo {@link #ENTRUST_SOFTTOKEN} y {@link #ENTRUST_TOKEN}.</li>
 * </ul>
 * 
 * <p><b>Todos los derechos reservados por Banco de Cr�dito e Inversiones.</b></p>
 * 
 */
public class PrioridadSegundaClaveIdTO implements Serializable{    
    
    /**
     * safesigner.
     */
    public static final PrioridadSegundaClaveIdTO SAFESIGNER = 
        new PrioridadSegundaClaveIdTO( 10, "SafeSigner", true);
    
    /**
     * multipass.
     */
    public static final PrioridadSegundaClaveIdTO MULTIPASS = 
        new PrioridadSegundaClaveIdTO( 20, "Multipass", false);

    /**
     * clave internet.
     */
    public static final PrioridadSegundaClaveIdTO INTERNET = 
        new PrioridadSegundaClaveIdTO( 30, "Internet", false);
    
    /**
     * firma electronica avanzada.
     */
    public static final PrioridadSegundaClaveIdTO ETOKEN = 
        new PrioridadSegundaClaveIdTO( 50, "EToken", false);
    
    /**
     * Firma Electronica Avanzada Multi Browser.
     */
    public static final PrioridadSegundaClaveIdTO ETOKENMULTIBROWSER = 
        new PrioridadSegundaClaveIdTO( 60, "ETokenMultiBrowser", false);
    
    /**
     * Soft Token Entrust.
     */
    public static final PrioridadSegundaClaveIdTO ENTRUST_SOFTTOKEN = 
       new PrioridadSegundaClaveIdTO( 70, "EntrustSoftToken", true);
    
    /**
     * Token F�sico Entrust.
     */
    public static final PrioridadSegundaClaveIdTO ENTRUST_TOKEN = 
       new PrioridadSegundaClaveIdTO( 80, "EntrustToken", false);
    
    /**
     * Numero de version utilizado durante la serializacion.
     */
    private static final long serialVersionUID = 1L;
    
    /**
     * Identificador.
     */
    private final int id;
    
    /**
     * Nombre.
     */
    private final String nombre;
    
    /**
     * Indica si requiere clave.
     */
    private final boolean requiereLlave;
        

    /**
     * Constructor de la clase.
     * 
     * @param id identificador.
     * @param nombre nombre.
     * @param requiereLlave requiere llave.
     */
    private PrioridadSegundaClaveIdTO(int id, String nombre, boolean requiereLlave) {
        this.id = id;
        this.nombre = nombre;
        this.requiereLlave = requiereLlave;
    }    
    
    /**
     * Obtiene prioridad por nombre.
     * <p>
     * <b>Registro de versiones:</b>
     * <ul> 
     * <li> 1.0 xx/xx/xxxx Desconocido - Desconocido (Ing. Soft. BCI): Versi�n inicial.</li>
     * <li> 1.1 12/12/2016 Darlyn Delgado (SEnTRA) - Daniel Araya Ureta (Ing. Soft. BCI): Se agrega nueva estrategia EtokenMultiBrowser y se agrega documentacion version inicial.</li>
     * <li> 1.2 10/07/2017 Luis L�pez Alamos (SEnTRA) - Ricardo Carrasco C�ceres (Ing. Soft. BCI): Se agregan nuevas estrategias <code>ENTRUST_SOFTTOKEN</code> y <code>ENTRUST_TOKEN</code> </li>
     * </ul>
     * <p>
     * 
     * @param otroNombre parametro de entrada.
     * @return PrioridadSegundaClaveIdTO prioridades.
     * @since 1.0
     */
    public static PrioridadSegundaClaveIdTO getPorNombre(String otroNombre) {
        if (SAFESIGNER.getNombre().equals(otroNombre)) {
            return SAFESIGNER;
        }
        else if (MULTIPASS.getNombre().equals(otroNombre)) {
            return MULTIPASS ;
        }
        else if (INTERNET.getNombre().equals(otroNombre)) {
            return INTERNET;
        }
        else if (ETOKEN.getNombre().equals(otroNombre)) {
            return ETOKEN;
        }
        else if (ETOKENMULTIBROWSER.getNombre().equals(otroNombre)) {
            return ETOKENMULTIBROWSER;
        }
        else if (ENTRUST_SOFTTOKEN.getNombre().equals(otroNombre)) {
            return ENTRUST_SOFTTOKEN;
        }
        else if (ENTRUST_TOKEN.getNombre().equals(otroNombre)) {
            return ENTRUST_TOKEN;
        }

        return null;
    }    
    
    
    public int getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }

    public boolean isRequiereLlave() {
        return requiereLlave;
    }
    
    
    /**
     * M�todo toString.
     * 
     * @return String datos de la clase.
     */
    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append("PrioridadSegundaClaveIdTO: [id=").append(id);
        sb.append("], nombre=[").append(nombre);
        sb.append("], requiereLlave=[").append(requiereLlave);  
        sb.append("]"); 
        return sb.toString();
    }
    
}

