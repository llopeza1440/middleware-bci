package wcorp.model.seguridad;

import java.io.IOException;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.Vector;

import javax.naming.InitialContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import wcorp.bprocess.seguridad.token.TokenDelegate;
import wcorp.model.actores.Cliente;
import wcorp.model.productos.PassCode;
import wcorp.serv.seguridad.NoSessionException;
import wcorp.serv.seguridad.SeguridadException;
import wcorp.serv.seguridad.ServiciosSeguridad;
import wcorp.serv.seguridad.ServiciosSeguridadHome;
import wcorp.util.ErroresUtil;
import wcorp.util.GeneralException;
import wcorp.util.TablaValores;
import wcorp.util.com.JNDIConfig;
import wcorp.util.journal.Eventos;
import wcorp.util.journal.Journal;
import wcorp.util.mensajeria.ControlErrorMsg;


/**
 * <b>CONTROLADOR BCI</b>
 * <p>
 * Objeto de Negocio SuperClase para Servlet Controler MVC
 * <p>
 *
 * Registro de versiones:<ul>
 *
 * <li>1.0  (??/??/????, Desconocido): version inicial
 *
 * <li>1.1 (06/11/2003, Michael Palomino B. (Novared): Se agregan cambios a implementaci�n de restricciones clientes
 *                                                      Telecanal. Se permite la configuraci�n de restricciones
 *                                                      en un subconjunto de pasos de un mismo servlet (servicio), a
 *                                                      trav�s de una tabla de par�metros.
 * <li>1.2 (04/05/2004, Hugo Vilavicencio (Apolo)):Se reparan validaciones de la expiraci�n de sesi�n bci que
 *                                                  obligaban al usuario a reiniciar el browser.
 *                                                  Se mejora la verificaci�n de la relaci�n sesi�n mopa y
 *                                                  sesi�n weblogic.
 * <li>1.3 (15/07/2004, Luis Eduardo Dujovne F.(BEE)):Se modifica el controller para permitir obtener el Passcode
 *		                                               para el Canal 230
 * <li>1.4 (20/10/2004, Rodrigo Zambrano(Novared)):Se agregan cambios de re-autenticaci�n de tokens para
 *                                                 transacciones de pagos y transferencias de fondos correspondientes
 *                                                 al proyecto de mejoras de seguridad. Estos cambios fueron
 *                                                 realizados al metodo validadAcceso.
 * <li>1.5 (25/10/2005, Victor Urra(Novared)):No permitir re-autenticaci�n para paguedirecto.
 * <li>1.6 (21/06/2006, Victor Urra(Novared)): Se agrego Validacion de canal en c�digo "privilegio.equals("ClaveInternet")",
 *                                              ya que para Tbanc(100) no existe clave Dos
 * <li>1.7 (30/06/2006, Luis Cruz (ImageMaker IT)): Agregada funcionalidad para TDC (bypass de chequeo de acceso cuando sea via Nexus)
 * <li>1.8 (13/09/2006, Paola Parra (SEnTRA)): Se agrega log para saber el canal por el cual se conecta el cliente y
 *                                             adem�s si tiene activa la clave dos
 * <li>1.9 (29/09/2006, Victor Urra(Novared)):Se agrego l�gica que determina  para un cliente si
 *                                             debe realizar la autentificaci�n de token despu�s de
 *                                             la autentificaci�n del banco enlinea, esta l�gica se agrego a la llamada del
 *                                             m�todo <b>validarToken</b>, adem�s se modifico l�gica para
 *                                             permitir la autentificaci�n del token cuando no este autenticado.
 * <li>1.10 (18/05/2009, Pedro Carmona E.(SEnTRA)): Se modifica el m�todo validarAcceso() para poder retornar correctamente a la p�gina de "espera" tras haber
 * 												ingresado el multipass. Adem�s, se cambia el sistema de logueo para cumplir con normativa del banco, y se
 * 												organizan import de la clase.
 * <li>1.11 (26/07/2010, Oliver Lopez (ADA Ltda.)):Se agrega validaci�n en el m�todo validarAcceso, para el nuevo canal 615 (d-one).
 *                                                 Este nuevo canal pertenece a las nuevas m�quinas cliente que tendr�n los mismos servicios que el micromaticoTouchScreen actual.
 *
 * <li> 1.12 (28/01/2013, H�ctor Hern�ndez Orrego (SEnTRA)) : Se incorpora verificaci�n para permitir el uso
 * de m�todo {@link #esPagoTransferencia()} por tipoUsuario = 'E', empresa, a�adiendo condici�n al flujo.

 * <li>1.13 (04/03/2013, Victor Hugo Enero (Orand S.A.)): Se agregan metodos para generar llaves con SafeSigner,
 * consultar el estado del mismo y validar
 * el codigo de la llave SafeSigner. generarLlavesDeSegundaClave, obtenerJerarquiaYEstadosSegundaClave y
 * validarSegundaClave y generarLlavesDeSegundaClave
 * <li>1.14 (12/04/2013, Victor Hugo Enero (Orand S.A.)): Se cambia el tipo de metodo a retornar en 
 * obtenerJerarquiaYEstadosSegundaClave
 * <li>1.15 (18/04/2013, Victor Hugo Enero (Orand S.A.)): Se hacen ajusta al dise�o de segunda clave
 * <li>1.16 (21/08/2013, Ver�nica Cestari (Orand S.A.)): Se realizo cambio de validacion de estatus bloqueado
 * para remover sesi�n de identificadorEstadoSegundaClave 
 * <li>1.17 (05/09/2013, Victor Hugo Enero (Orand S.A)): Se coloca en session el parametro camposDellaveTO y
 * se asigna al parametro antes de llamar de autenticar de EstrategiaSafesigner
 *
 * <li>1.18 (25/11/2013, Pedro Carmona Escobar.(SEnTRA)): Se modifican los m�todos
 *                              {@link #validarToken(HttpServletRequest, String, String)} y
 *                              {@link #validarAcceso(HttpServletRequest, String, String)}.                                   
 * <li>1.19 02/08/2017, Marcelo Fuentes (SEnTRA) - Ricardo Carrasco (Ing. Soft. BCI): Se sobrecargan los siguientes m�todos:
 * {@link #consultaJerarquia(HttpServletRequest, String, String)}, {@link #tienePrivilegio(Vector, HttpServletRequest, String)},
 * {@link #checkToken(HttpServletRequest, String)}, {@link #checkPasscode(HttpServletRequest, String)}.
 * Se modificaron los siguientes metodos:
 * {@link #validarToken(HttpServletRequest, String, String)}, {@link #obtenerEstadoSegundaClave(ParametrosEstrategiaSegundaClaveTO)},
 * {@link #generarLlavesDeSegundaClave(HttpServletRequest, String, String, CamposDeLlaveTO)}, 
 * {@link #obtenerJerarquiaYEstadosSegundaClave(ParametrosEstrategiaSegundaClaveTO, HttpServletRequest)},
 * {@link #validarSegundaClave(HttpServletRequest, String, String, RepresentacionSegundaClave)}.
 * Se crearon los siguientes metodos:
 * {@link #verificarAutenticacion(String, String, UsuarioSegundaClaveTO, CamposDeLlaveTO, String, boolean, String)},
 * {@link #estadoSegundaClave(ParametrosEstrategiaSegundaClaveTO paramSegundaClave)},
 * {@link #generarLlave(String, String, UsuarioSegundaClaveTO, CamposDeLlaveTO, String)},
 * {@link #getFrecuenciaEntrust()}, {@link #getTimeOutEntrust()}, {@link #obtieneDominio(String canal)}, {@link #getLogger()}.
 * Ademas se agregan las constantes {@link #IDNOOK}, {@link #TABLA_PARAMETROS_ENTRUST}, {@link #TABLA_SEGURIDAD}
 * {@link #ONLINE}, {@link #OFFLINE}, {@link #ENTRUST_SOFTTOKEN} y {@link #CANAL_PYME}.</li>
 * </ul>
 *
 * <p><b>Todos los derechos reservados por Banco de Cr�dito e Inversiones.</b></p>
 */

public class ControllerBCI {

    private static final ControllerBCI cb= new ControllerBCI();
    private static Logger logger = (Logger)Logger.getLogger(ControllerBCI.class);
    private static String tituloPassCode= null;
    private static String tituloPassSupUser= null;
    private static String PagueDirecto="PagueDirecto";

    private static final String archivoConfiguracionRetoma= "Retoma.parametros";

    /**
     * Nombre de tabla de restricciones
     */
    private static final String TABLA_RESTRICCIONES_TELECANAL= "restricciones.parametros";

    /**
     * Atributo que identifica al canal Bc Empresa.
     */
    private static final String CANAL_EMPRESA = "230";

    /**
     * String que identifica el canal bci express.
     */
    private static final String CANAL_BCI_EXPRESS = "230";

    
    /**
     * tabla de par�metros a utilizar.
     */
    private static final String TABLADEPARAMETROS = "safesigner.parametros";
    
    /**
     * Id de NO OK.
     */
    private static final int IDNOOK = -1;
    
    /**
     * tabla de par�metros de entrust.
     */
    private static final String TABLA_PARAMETROS_ENTRUST = "entrust.parametros";
    
    /**
     * tabla de par�metros de Seguridad.
     */
    private static final String TABLA_SEGURIDAD = "Seguridad.parametros";
    
    
    /**
     * tipo de segunda clave para SoftToken Entrust.
     */
    private static final String ONLINE = TablaValores.getValor(TABLA_PARAMETROS_ENTRUST, "modoOnline", "desc");
    
    /**
     * tipo de segunda clave para SoftToken Entrust.
     */
    private static final String OFFLINE = TablaValores.getValor(TABLA_PARAMETROS_ENTRUST, "modoOffline", "desc");
    
    /**
     * Tipo de Segunda Clave Softtoken Entrust.
     */
    private static final String ENTRUST_TOKEN = TablaValores.getValor(TABLA_PARAMETROS_ENTRUST, "nombreToken", "desc");
    
    /**
     * Tipo de Segunda Clave Softtoken Entrust.
     */
    private static final String ENTRUST_SOFTTOKEN = TablaValores.getValor(TABLA_PARAMETROS_ENTRUST, "nombreSoftToken", "desc");
    
    /**
     * Canal Pyme.
     */
    private static final String CANAL_PYME = "132";
    
    
    /**
     * Valida el acceso a servlet.
     * <p>
     * Registro de versiones:<ul>
     * <li> 1.0 (??/??/????, Desconocido) : version inicial
     * <li> 1.1 (20/10/2004, Nelly Reyes Quezada- SEnTRA): Modificaciones para proyecto mejoras de seguridad.
     * <li> 1.2 (29/09/2006, Victor Urra(Novared)):se agrego logica que determina  para un cliente si
     *                                             debe realizar la autentificaci�n de token despu�s de
     *                                             la autentificaci�n del banco enlinea, esto se aplica a la llamada del
     *                                             metodo <b>validarToken</b>, adem�s se modifico l�gica para
     *                                             permitir la autentificaci�n de token cuando no este autenticado.
     * <li> 1.3 (02/01/2009, Pedro Carmona Escobar (SEnTRA): En caso de detectar que se solicitar� multipass, se revisa si viene una
     * 												en el request valor para 'despachoModificado'. Se realiza esto para poder volver a una direcci�n definida
     * 												tras el ingreso del multipass.
     * <li> 1.4 (26/07/2010, Oliver Lopez (ADA Ltda.)):Se agrega validaci�n para el nuevo canal 615 (d-one).
     *                                                 Este nuevo canal pertenece a las nuevas m�quinas cliente que tendr�n los mismos servicios que el micromaticoTouchScreen actual.
     *
     * <li> 1.5 (25/11/2013, Pedro Carmona Escobar (SenTRA.)):Se modifica la llama al m�todo
     *                              {@link #validarToken(HttpServletRequest, String, String)} a ra�z de cambio
     *                              en su firma.												
     *
     * </ul>
     * <p>
     * @param req
     * @param idName
     * @param servicio
     * @exception IOException
     * @exception SeguridadException
     * @exception NoSessionException
     * @since 1.0
     */
    public static void validarAcceso(HttpServletRequest req, String idName, String servicio) throws IOException, SeguridadException, NoSessionException {

        if (req == null) {
            throw new wcorp.serv.seguridad.NoSessionException("NOSESSION");
        }
         TokenDelegate delegate = new TokenDelegate();
        // Doble Click
        /*
         HttpSession hsess = req.getSession(false); // Recupera Sesion HTTP

         if (hsess != null){
         String url1 = (String)hsess.getAttribute("CHECK_DOUBLE_SUBMIT_URL");
         if (url1 != null){
         Date date1 = (Date)hsess.getAttribute("CHECK_DOUBLE_SUBMIT_DATE");
         String url2 = req.getRequestURI();
         System.out.println("*********** URI = " + url2);
         if (url1.equals(url2)){
         Date date2 = (Date)Calendar.getInstance().getTime();
         //Delta de tiempo
         long del = Math.abs(date1.getTime() - date2.getTime());
         if ( del < 1000l){
         System.out.println("****** Doble Click = " + del + " ms.");
         }
         }
         }
         hsess.setAttribute("CHECK_DOUBLE_SUBMIT_URL", req.getRequestURI());
         hsess.setAttribute("CHECK_DOUBLE_SUBMIT_DATE", (Date)Calendar.getInstance().getTime());
         }
         // FIN Doble Click
         */
        // Controller Para Sessiones BCIExpres MOPAIII. Si es MopaIII, termina...
        HttpSession hsess= req.getSession(false); // Recupera Sesion HTTP
        if (hsess != null) {
            SessionBCI sesionPpal= (SessionBCI)hsess.getAttribute(idName); // Recupera Sesion BCI (Generica)

            // inicio log
            if (sesionPpal == null) {
                if (logger.isDebugEnabled()) {logger.debug( "sesionPpal == null");}
            } else {
                if (logger.isDebugEnabled()) {logger.debug( "sesionPpal.getAttrib(\"esMopaIII\") : " + sesionPpal.getAttrib("esMopaIII"));}
            }
            // fin log

            if (sesionPpal != null) {

                // Chequea que este Activa
                boolean sesionPpalActiva= true;
                try {
                    sesionPpal.checkAlive();
                } catch (Exception e) {
                    sesionPpalActiva= false;
                }
                //if (logger.isDebugEnabled()) {logger.debug( "sesionPpalActiva            : " + sesionPpalActiva);}

                // If incluido para evitar algunas muertes de sesi�n mopa incorrectas
                if (!("true".equals(sesionPpal.getAttrib("esMopaIII")) && !sesionPpalActiva)) {
                    if (logger.isDebugEnabled()) {logger.debug( "Entra a bloque if [001]. RutUsuario: " + sesionPpal.getRutUsuario());}

                    String retomo= TablaValores.getValor(archivoConfiguracionRetoma, sesionPpal.getCanal().getCanalID(), "retomar");
                    if (retomo == null) retomo= "";

                    if (!(retomo.equals("NO"))) {
                        // Retoma
                        try {
                            sesionPpal.checkAlive(); // Chequea que este Activa
                        } catch (SeguridadException se) {
                            //Asignaci�n de URL para retoma inteligente
                            //if (logger.isDebugEnabled()) {logger.debug( "Excepcion de seguridad, seteando retoma");}
                            asignaServicioRetoma(hsess, sesionPpal.getCanal().getCanalID(), servicio, String.valueOf(sesionPpal.getCliente().getRut()));
                            sesionPpal.cerrar();
                            EliminarSession.eliminar((String)sesionPpal.getAttrib("sessionId"));
                            //if (logger.isDebugEnabled()) {logger.debug( "Excepcion de sguridad, session eliminada");}
                            throw se;
                        }
                        // Fin Retoma
                    }

                    // verifica si la transaccion que se va a realizar implica una trans-
                    // ferencia o pago y si es necesario que el usuario active su token
                    // caso de que posea uno.
                    boolean transacPago = esPagoTransferencia(req, idName, servicio);
                    boolean necActivarToken = false;
                    if (transacPago)
                       necActivarToken = necesitaActivarToken(req, sesionPpal);
                    if (transacPago && necActivarToken) {
                       String dis = req.getRequestURI();
                       dis = dis.concat("?paso=" + req.getParameter("paso"));
                       //if (logger.isDebugEnabled()) {logger.debug( "dis para token (despues de activar) :" + dis);}
                       sesionPpal.setAttrib("despachoToken", dis);
                       //if (logger.isDebugEnabled()) {logger.debug("validarAcceso: cliente debe activar token, redirect a "+dis);}
                       throw new SeguridadException("ACTIVARTOKEN");
                    }
                    try{
                           //Validaci�n si el cliente debe realizar login token
                            if (sesionPpal.getAttrib("permitirAutentificacion") != null){
                                if (logger.isDebugEnabled()) {logger.debug(">>>>>>>>>>>>>>>Antes de validar token");}
                                //Validaci�n para todos los servicios: Si posee token activo, debe estar autenticado con �l.
                                validarToken(req, idName, servicio);
                            }else
                                if (logger.isDebugEnabled()) {logger.debug("No esta solicitando login token");}
                    }catch (GeneralException Gex){
                              if (logger.isEnabledFor(Level.ERROR)) {logger.error("No pudo realizar la validacion de token");}
                              throw new SeguridadException("SEG00");
                    }

                    //MICROMATICOS
                    Canal cnl= sesionPpal.getCanal();
                    String canal= cnl.getCanalID();

                    if ((canal != null) && ( canal.equals("600") || canal.equals("601")|| canal.equals("615"))) {
                        String ipClient= req.getRemoteAddr();
                        String regMicro= TablaValores.getValor("micromaticos.parametros", ipClient, servicio);
                        if (regMicro != null && regMicro.equalsIgnoreCase("SI"))
                            return;
                        else
                            throw new wcorp.serv.seguridad.SeguridadException("MICROMATICO");
                    }
                    //FIN MICROMATICOS

                    String passcode= (String)sesionPpal.getAttrib("autenticado");
                    /* averiguar tipo de autenticacion requerida (tipo = checkSecurity())*/

                    // Se consulta por la jerarquia del servicio (Modos con los que me
                    //puedo autenticar en dicho servocio en el orden en que estan la tabla Servicios.parametros)
                    Vector test;
                    String privilegio= null;
                    //if (logger.isDebugEnabled()) {logger.debug( "Antes de ejecutar consultarJerarquia");}
                    test= consultaJerarquia(req, idName, servicio);

                    // Si el servicio esta en dicha tabla consultamos por los privilegios que tiene el cliente

                    if (test != null && test.size() != 0) {
                        //if (logger.isDebugEnabled()) {logger.debug( "ControllerBCI Seguridad.parametros test no es nulo");}
                        privilegio= tienePrivilegio(test, req, idName);
                        //if (logger.isDebugEnabled()) {logger.debug( "ControllerBCI Seguridad.parametros privilegio :" + privilegio);}
                    }else{
                        //if (logger.isDebugEnabled()) {logger.debug( "No tiene jerarquia en Seguridad.parametros");}
                        privilegio = "SinJerarquia";
                    }

                    if (test != null && test.size() != 0 && privilegio.equals("ServSinPermiso")) {
                        //if (logger.isDebugEnabled()) {logger.debug( "ControllerBCI Seguridad.parametros test es blanco Servicio sin privilegios");}
                        throw new SeguridadException("OTRO_01");
                    }

                    // Si el cliente tiene privilegio passcode

                    if (privilegio != null && privilegio.equals("Passcode")) {
                        if (checkPassCode(req, idName, servicio)) {
                            /* tipo == PASSCODE */
                            if (passcode == null) {
                                if (logger.isDebugEnabled()) {logger.debug( "[validarAcceso] rut ["+sesionPpal.getRutUsuario()+"] Autenticar PassCode");}
                                String tope;
                                String dis= req.getRequestURI();
                                //if (logger.isDebugEnabled()) {logger.debug( "dis :" + dis);}
                                sesionPpal.setAttrib("dispatcherPassCode", dis);
                                sesionPpal.setAttrib("tituloPassCode", tituloPassCode);
                                //if (logger.isDebugEnabled()) {logger.debug( "servicio :" + servicio);}
                                //if (logger.isDebugEnabled()) {logger.debug( "tituloPassCode :" + tituloPassCode);}
                                sesionPpal.setAttrib("servicioPassCode", servicio);
                                // Seteo del tipo de autenticacion
                                sesionPpal.setAttrib("tipoAutenticacion", "Passcode");
                                if (logger.isDebugEnabled()) {logger.debug( "[validarAcceso] rut ["+sesionPpal.getRutUsuario()+"] Tipo de autenticacion Passcode");}
                                throw new SeguridadException("PASSCODE");
                            } else {
                                if (logger.isDebugEnabled()) {logger.debug( "[validarAcceso] rut ["+sesionPpal.getRutUsuario()+"] PassCode autenticado");}
                                sesionPpal.removeAttrib("autenticado");
                            }
                        }
                    }

                    // Si el cliente tiene privilegio Token

                    else if (privilegio != null && privilegio.equals("Token")) {
                        //if (logger.isDebugEnabled()) {logger.debug( "Tiene Token");}

                         //Logica de Token

                        Hashtable seguridad= (Hashtable)sesionPpal.getMetodosAutenticacion();
                        if (seguridad != null) {
                            //if (logger.isDebugEnabled()) {logger.debug( "tckeckToken   : seguridad no null");}
                            Boolean estadoMetodo= (Boolean)seguridad.get("TOK");
                            if (logger.isDebugEnabled()) {logger.debug( "estadoMetodo   : "+estadoMetodo);}
                            if (estadoMetodo != null) {
                                //if (logger.isDebugEnabled()) {logger.debug( "ckeckToken   :Posee Token");

                                    //verifica si la transaccion es un pago o una
                                    //transferencia para reAutenticar el token
                            	      if (logger.isDebugEnabled()) {logger.debug( "transacPago   : "+transacPago);}
                                      if(transacPago && !servicio.equals(PagueDirecto)){
                                          String tokenAutorizado = (String) sesionPpal.getAttrib("tokenAutorizadoPagoTransfer");
                                          if (logger.isDebugEnabled()) {logger.debug( "tokenAutorizado   : "+tokenAutorizado);}
                                          if (tokenAutorizado == null || !tokenAutorizado.equals("true")) {
                                            //necesita autenticar de nuevo para estas transacciones
                                            String dis = req.getRequestURI();
                                            if (logger.isDebugEnabled()) {logger.debug( "req.getParameter(despachoModificado) :" + req.getParameter("despachoModificado"));}
                                            if (req.getParameter("despachoModificado")!=null){
                                            	dis = req.getParameter("despachoModificado");
                                            	if (logger.isDebugEnabled()) {logger.debug( "dis para token :" + dis);}
                                            }
                                            dis = dis.concat("?paso=" + req.getParameter("paso"));
                                            if (logger.isDebugEnabled()) {logger.debug( "dis para token :" + dis);}
                                            sesionPpal.setAttrib("despacho", dis);
                                            sesionPpal.setAttrib("autentToken", "true");
                                            throw (SeguridadException)ControlErrorMsg.modificaCodigo((Exception)new SeguridadException("TOKEN"), sesionPpal);
                                          } else {
                                            if (logger.isDebugEnabled()) {logger.debug("[validarAcceso] rut ["+sesionPpal.getRutUsuario()+"] ckeckToken   :token autorizado para pago o transferencia");}
                                            sesionPpal.removeAttrib("tokenAutorizadoPagoTransfer");
                                          }
                                    }
                            } else {
                                //Modificado por Victor Urra, Novared S.A., 17/08/2004
                                estadoMetodo= (Boolean)seguridad.get("TOKBLO");
                                if (estadoMetodo != null){
                                    if (logger.isDebugEnabled()) {logger.debug( "[validarAcceso] rut ["+sesionPpal.getRutUsuario()+"] tckeckToken   : bloqueado");}
                                    throw (SeguridadException)ControlErrorMsg.modificaCodigo((Exception)new SeguridadException("TK07"), sesionPpal);
                                }else{
                                	if (logger.isDebugEnabled()) {logger.debug( "[validarAcceso] rut ["+sesionPpal.getRutUsuario()+"] tckeckToken   : estado metodo null");}
                                    throw (SeguridadException)ControlErrorMsg.modificaCodigo((Exception)new SeguridadException("TK_04"), sesionPpal);
                                }
                            }

                        } else {
                            if (logger.isDebugEnabled()) {logger.debug( "[validarAcceso] rut ["+sesionPpal.getRutUsuario()+"] tckeckToken   : seguridad null");}
                            throw (SeguridadException)ControlErrorMsg.modificaCodigo((Exception)new SeguridadException("TK_04"), sesionPpal);
                        }
                        // Seteo del tipo de autenticacion
                        sesionPpal.setAttrib("tipoAutenticacion", "Token");
                        if (logger.isDebugEnabled()) {logger.debug( "[validarAcceso] rut ["+sesionPpal.getRutUsuario()+"] Tipo de autenticacion Token");}
                    }
                    // Usuario con clave Internet

                    else if (privilegio != null && privilegio.equals("ClaveInternet")) {
                        //if (logger.isDebugEnabled()) {logger.debug( "Tiene Clave Internet");}
                        // Seteo del tipo de autenticacion
                        sesionPpal.setAttrib("tipoAutenticacion", "ClaveInternet");
                        if (logger.isDebugEnabled()) {logger.debug( "[validarAcceso] rut ["+sesionPpal.getRutUsuario()+"] Tipo de autenticacion Clave Internet canal ["+sesionPpal.getCanal().getCanalID()+"]");}
                        // Lineas agregadas para segunda etapa Token ###### INICIO ########

                        // Para la Mantencion Segunda Clave se agrego metodo validaCanalClave
                        // que identifica si para un canal en especial utilizara la clave passcode,
                        // esto debido a que por ej: en tbanc no se utiliza clave dos

                        if(validaCanalClaveDos(sesionPpal.getCanal().getCanalID())){
                            if (logger.isDebugEnabled()) {logger.debug( "[validarAcceso] rut ["+sesionPpal.getRutUsuario()+"] EL CANAL TIENE ACTIVO LA CLAVE DOS");}
                            passcode= (String)sesionPpal.getAttrib("autenticado");
                            if (checkPassCode(req, idName, servicio)) {
                                if (passcode == null) {
                                    //if (logger.isDebugEnabled()) {logger.debug( "Autenticar PassCode");}
                                    String dis= req.getRequestURI();
                                    sesionPpal.setAttrib("dispatcherPassCode", dis);
                                    sesionPpal.setAttrib("tituloPassCode", tituloPassCode);
                                    sesionPpal.setAttrib("servicioPassCode", servicio);
                                    throw new SeguridadException("PASSCODE");
                                } else {
                                    if (logger.isDebugEnabled()) {logger.debug( "[validarAcceso] rut ["+sesionPpal.getRutUsuario()+"] PassCode autenticado");}
                                    sesionPpal.removeAttrib("autenticado");
                                }
                            }
                        }else{
                            if (logger.isDebugEnabled()) {logger.debug( "[validarAcceso] rut ["+sesionPpal.getRutUsuario()+"] EL CANAL NO TIENE ACTIVO LA CLAVE DOS");}
                        }
                    } else if (privilegio != null && privilegio.equals("SinJerarquia")){
                        if (logger.isDebugEnabled()) {logger.debug( "[validarAcceso] rut ["+sesionPpal.getRutUsuario()+"] SERVICIO NO PROTEGIDO");}
                        // Lineas agregadas para segunda etapa Token ###### FIN ########
                    } else {
                        if (logger.isDebugEnabled()) {logger.debug( "[validarAcceso] rut ["+sesionPpal.getRutUsuario()+"] SERVICIO NO PROTEGIDO");}
                        throw new SeguridadException("SEG00");
                    }

                    String SupUserCode= (String)sesionPpal.getAttrib("supUser");
                    if (checkPassSupUser(req, idName, servicio)) {

                        if (SupUserCode == null) {
                            String dis= req.getRequestURI();
                            //if (logger.isDebugEnabled()) {logger.debug( "pagina", dis);}
                            //if (logger.isDebugEnabled()) {logger.debug( "pagina", dis);}
                            //if (logger.isDebugEnabled()) {logger.debug( "pagina", dis);}

                            sesionPpal.setAttrib("dispatcherSupUser", dis);
                            sesionPpal.setAttrib("tituloSupUser", tituloPassSupUser);
                            sesionPpal.setAttrib("servicioSupUser", servicio);
                            throw new SeguridadException("SUPUSER");
                        } else {
                            if (logger.isDebugEnabled()) {logger.debug( "[validarAcceso] rut ["+sesionPpal.getRutUsuario()+"] supUser autenticado");}
                            sesionPpal.removeAttrib("supUser");
                        }
                    }

                    if (tieneRestriccionesTelecanal(servicio, req, sesionPpal)) {
                        throw new SeguridadException("AUTORIZA");
                    }

                    ////
                    ////                // Codigo Restricci�n Clientes TeleCanal
                    ////                String codigoRestriccionCallCenter = (String)sesionPpal.getAttrib("codigoRestriccionCallCenter");
                    ////                if (logger.isDebugEnabled()) {logger.debug( "codigoRestriccionCallCenter : " + codigoRestriccionCallCenter);}
                    ////                if (codigoRestriccionCallCenter != null){
                    ////                	String tabla = sesionPpal.getCanal().getPathTablas() + sesionPpal.getCanal().getDispositivo() + ".servicios.parametros";
                    ////                	if (logger.isDebugEnabled()) {logger.debug( "Tabla : " + tabla);}
                    ////                	String autoriza = TablaValores.getValor(tabla, servicio, "autoriza");
                    /// /                	if (logger.isDebugEnabled()) {logger.debug( "Autoriza : " + autoriza);}
                    ////                	if (autoriza != null){
                    ////                		autoriza += ",";
                    ////                		boolean autorizado = false;
                    ////                		while (autoriza.indexOf(",") != -1){
                    ////                			if (autoriza.substring(0,autoriza.indexOf(",")).equalsIgnoreCase(codigoRestriccionCallCenter))
                    ////                                autorizado = true;
                    ////                            autoriza = autoriza.substring(autoriza.indexOf(",") + 1, autoriza.length());
                    ////                        }
                    ////                        if(!autorizado)
                    ////                			throw new SeguridadException("AUTORIZA");
                    ////                	}
                    ////                }
                }
            }
        }

        try {
            if (ControllerMOPAIII.validarAcceso(req, idName, servicio)) return;
        } catch (SeguridadException e) {
            throw e;
        } catch (NoSessionException e) {
            throw e;
        }

        if (hsess == null) {
            if (logger.isDebugEnabled()) {logger.debug( "Excepcion de sguridad, httpSesion null");}
            throw new wcorp.serv.seguridad.NoSessionException("NOSESSION");
        }

        SessionBCI sesionPpal= (SessionBCI)hsess.getAttribute(idName); // Recupera Sesion BCI (Generica)
        if (sesionPpal == null) throw new wcorp.serv.seguridad.NoSessionException("NOSESSION");

        try {
            sesionPpal.checkAlive(); // Chequea que este Activa
        } catch (SeguridadException se) {

            sesionPpal.cerrar();
            EliminarSession.eliminar((String)sesionPpal.getAttrib("sessionId"));
            throw se;
        }

        String objeto= (String)req.getParameter("objeto"); // Recupera Objeto del Parametro
        String checkServicioObjetoTdc = null;


        if (logger.isDebugEnabled()) {
        	logger.debug( " objeto ..:: [ " + req.getParameter("objeto") + "]");
        	logger.debug( " checkServicioObjetoTdc ..1:: [ " + checkServicioObjetoTdc + "]");
        	logger.debug( " servicio ..1:: [ " + servicio + "]");
        }

        // se agrega funcionalidad para implementaci�n del proyecto Full OnLine TDC
        // en el caso de que el objeto !=null se revisa la tabla seguridad.parametros
        // se env�a el servicio y se rescata el valor de checkServicioObjetoTdc
        // en el caso que se encuentre no se realiza la checkServicioObjetoTdc
        if (objeto!=null) {
            if (logger.isDebugEnabled()) {logger.debug( " servicio ..:: [ " + servicio + "]");}
            checkServicioObjetoTdc = TablaValores.getValor("Seguridad.parametros", servicio, "checkServicioObjeto");
            if (logger.isDebugEnabled()) {logger.debug( " checkServicioObjetoTdc ..:: [ " + checkServicioObjetoTdc + "]");}
        }

        if (checkServicioObjetoTdc == null) {
        	if (objeto != null) {
        		sesionPpal.checkServicioObjeto(servicio, objeto);
        		sesionPpal.setAttrib("objeto", objeto);
        	} else {
        		sesionPpal.removeAttrib("objeto");
        		try {
        			sesionPpal.checkServicio(servicio);
        		} catch (SeguridadException e) {
        			throw e;
        		}
        	}
        } else { //no se valida ya que es tdc, pero igual se setAttrib en session
        	if (logger.isDebugEnabled()) {logger.debug( " No realizamos checkServicioObjeto ..:: [ " + objeto + "]");}
        	sesionPpal.setAttrib("objeto", objeto);
        }
        hsess.setAttribute(idName, sesionPpal); // Registra nuevamente la Sesion en ambiente HTTP
        }

    /**
     * Valida las restricciones para un cliente Telecanal.
     * Valida las restricciones para un cliente Telecanal en base a la
     * configuraci�n establecida en una tabla de par�metros.
     * <p>
     * La tabla de par�metros browser.servicios.parametros debe contener las
     * siguientes variables para cada servicio:
     * <p>
     * <ul>
     *     <li> variable
     * </ul>
     * <p>
     *
     *
     * <p>
     * El par�metro '<i>variable</i>' reprsenta el nombre de la variable a ser
     * considerada en la detecci�n del paso que se est� ejecutando (i.e. 'paso').<br>
     * Si este par�metro tiene el valor asterisco ('*'), se considera que todos
     * los pasos del servicio est�n sujetos a restricci�n.
     * <p>
     * De esta forma, al estar en 'SI' el par�metro '<i>prohibido</i>', se lee
     * la tabla de par�metros dada por la variable TABLA_RESTRICCIONES_TELECANAL.
     * En esta tabla se configuran las restricciones para los servicios con el
     * siguiente formato:<p>
     * <servicio>_<paso>;prohibido=<SI|NO>;restriccion=<codigo restriccion>[, <codigo restriccion>]*
     * <p>
     *
     * Si '<i>prohibido</i>' tiene el valor 'SI' y, adem�s, la variable de sesi�n
     * 'prohibicionCallCenter' es distinta de null, se deniega el acceso al
     * servicio
     *
     * <br>
     * <p>
     *  Ejemplo:<p>
     *  Talonarios_26;prohibido=NO;restriccion=01,02,93;
     *  Talonarios_27;prohibido=NO;restriccion=01,02,93;
     * <p>
     *  Cuando se restringe el servicio completo (parametro <i>variable</i> con
     *  asterisco), no se agregan los pasos. Como ejemplo:<p>
     *  Talonarios;prohibido=NO;restriccion=01,02,93;
     * <p>
     *
     * @param  servicio  La sesi�n BCI
     * @param  req
     * @param  sesion  La sesi�n BCI
     * @return true, si hay restricciones asociadas al cliente<br>
     *         false, si no las hay.
     */
    private static boolean tieneRestriccionesTelecanal(String servicio, HttpServletRequest req, SessionBCI sesion) {
        String variablePaso= null;
        String valorPaso= null;
        String llaveRestriccion= null;
        String prohibicionCallCenter= null;
        String tablaServicios= null;
        String tablaRestricciones= null;


         // primero se lee la variable de paso que utiliza el servicio

        tablaServicios= sesion.getCanal().getPathTablas() + sesion.getCanal().getDispositivo() + ".servicios.parametros";
        if (logger.isDebugEnabled()) {logger.debug( "[tieneRestriccionesTelecanal] Tabla Servicios: " + tablaServicios);}

        variablePaso= TablaValores.getValor(tablaServicios, servicio, "variable");

        if (variablePaso == null) {
            return false;
        }


         // luego se determina el valor actual de la variable definida

        valorPaso= req.getParameter(variablePaso);


        // Si la variable de paso no existe o no tiene valor y se ha restringido
        // el servicio (asterisco), entonces se debe continuar la
        // validaci�n

        if (valorPaso == null && !variablePaso.equalsIgnoreCase("*")) {
            return false;
        }

        llaveRestriccion= servicio;
        if (!variablePaso.equalsIgnoreCase("*")) {
            llaveRestriccion+= "-" + valorPaso.trim();
        }

        // Transacciones Sensibles TeleCanal
        prohibicionCallCenter= (String)sesion.getAttrib("prohibicionCallCenter");
        if (logger.isDebugEnabled()) {logger.debug( "[tieneRestriccionesTelecanal]  ProhibicionCallCenter : " + prohibicionCallCenter);}

        tablaRestricciones= sesion.getCanal().getPathTablas() + TABLA_RESTRICCIONES_TELECANAL;
        if (logger.isDebugEnabled()) {logger.debug( "[tieneRestriccionesTelecanal]  Tabla Restricciones: " + tablaRestricciones);}

        if (prohibicionCallCenter != null) {
            String prohibir= TablaValores.getValor(tablaRestricciones, llaveRestriccion, "prohibir");
            if (logger.isDebugEnabled()) {logger.debug( "[tieneRestriccionesTelecanal]  Prohibir : " + prohibir);}
            if (prohibir != null && prohibir.equalsIgnoreCase("SI")) {
                /*
                 * No autorizado
                 */
                return true;
            }

        }

        // Codigo Restricci�n Clientes TeleCanal
        String codigoRestriccionCallCenter= (String)sesion.getAttrib("codigoRestriccionCallCenter");
        if (logger.isDebugEnabled()) {logger.debug( "[tieneRestriccionesTelecanal]  codigoRestriccionCallCenter : " + codigoRestriccionCallCenter);}

        if (codigoRestriccionCallCenter != null) {
            String autoriza= TablaValores.getValor(tablaRestricciones, llaveRestriccion, "autoriza");
            if (logger.isDebugEnabled()) {logger.debug( "[tieneRestriccionesTelecanal]  Autoriza : " + autoriza);}
            if (autoriza != null) {
                autoriza+= ",";
                boolean autorizado= false;
                while (autoriza.indexOf(",") != -1) {
                    if (autoriza.substring(0, autoriza.indexOf(",")).equalsIgnoreCase(codigoRestriccionCallCenter)) autorizado= true;
                    autoriza= autoriza.substring(autoriza.indexOf(",") + 1, autoriza.length());
                }
                if (!autorizado) {

                     // Se encontr� restricci�n

                    return true;
                }
            }
        }


         // No se encontr� restricci�n

        return false;
    }

    public static boolean checkPassCode(HttpServletRequest req, String idName, String servicio) {
        HttpSession ss= req.getSession(false);
        SessionBCI ssbci= (SessionBCI)ss.getAttribute(idName);
        Canal cnl= ssbci.getCanal();
        String id= cnl.getCanalID() + "-" + servicio;
        String condicion= TablaValores.getValor("Seguridad.parametros", id, "condicion");
        String valor= TablaValores.getValor("Seguridad.parametros", id, "valor");
        String activo= TablaValores.getValor("Seguridad.parametros", id, "activo");
        tituloPassCode= TablaValores.getValor("Seguridad.parametros", id, "titulo");

        if(ssbci.getCanal().getCanalID().equals("540") || ssbci.getCanal().getCanalID().equals("540bci") || ssbci.getCanal().getCanalID().equals("540tbc")){
              wcorp.serv.pagos.PagueDirecto pd= (wcorp.serv.pagos.PagueDirecto)ssbci.getAttrib("PagueDirecto");
              if (pd != null && pd.getRutEmpresa() != 0){
                  return false;
              }
        }
        if (ssbci.getCliente() != null && ssbci.getCliente().tipoUsuario != null && ssbci.getCliente().tipoUsuario.equals("P")) {
            if (activo != null) if (activo.equals("Si")) {
                String parametro= req.getParameter(condicion);
                if (parametro != null) if (parametro.equals(valor)) return true;
            }
        }
        return false;
    }
    
    /**
     * Metodo para verificar la jerarquia que posee el servicio.
     * Retorna un vector con dicha jerarquia Ejemplo: Token, Passcode.
     * <p>
     * Registro de versiones: 
     * <ul>
     * <li>1.0 ??/??/?? Desconocido : Versi�n Inicial.</li>
     * <li>2.0 02/08/2017 Marcelo Fuentes (SEnTRA) - Ricardo Carrasco (Ing. Soft. BCI): Se mueve la logica al metodo
     * {@link #consultaJerarquia(SessionBCI, String)} pasando la session BCI como parametro.</li>
     * </ul>
     * <p> 
     * @param request la solicitud HTTP que se esta procesando.
     * @param idName Identificador del atributo sesion BCI.
     * @param servicio Identificador del servicio.
     * @return Vector con las jerarquias de segunda clave a evaluar.
     * @since ?.?
     */
    public static Vector consultaJerarquia(HttpServletRequest req, String idName, String servicio) {
        HttpSession ss = req.getSession(false);
        SessionBCI ssbci = (SessionBCI)ss.getAttribute(idName);
        return consultaJerarquia(ssbci, servicio);
    }
    
    /**
     * Metodo para verificar la jerarquia que posee el servicio.
     * Retorna un vector con dicha jerarquia Ejemplo: Token, Passcode.
     * <p>
     * Registro de versiones: 
     * <ul>
     * <li>1.0 02/08/2017 Marcelo Fuentes (SEnTRA) - Ricardo Carrasco (Ing. Soft. BCI): Version inicial.</li>
     * </ul>
     * <p> 
     * @param ssbci Sesion BCI.
     * @param servicio Identificador del servicio.
     * @return Vector con las jerarquias de segunda clave a evaluar.
     * @since 1.19
     */
    public static Vector consultaJerarquia(SessionBCI ssbci, String servicio) {
        String jerarquiaSeg = "";
        String jerarquiaEmp = "";
        Vector jerarquia = null;
        Canal cnl = ssbci.getCanal();
        String id = cnl.getCanalID() + "-" + servicio;
        
        if (ssbci.getCliente() != null && ssbci.getCliente().tipoUsuario != null && ssbci.getCliente().tipoUsuario.equals("E")) {
            if (getLogger().isDebugEnabled()) {
                getLogger().debug( "[consultaJerarquia] rut ["+ssbci.getCliente().getRut()+"] es Empresa o Pyme");
            }
            jerarquiaEmp= TablaValores.getValor("Seguridad.parametros", id, "JerarquiasEmp");
            if (jerarquiaEmp != null && !jerarquiaEmp.trim().equals("")) {
                StringTokenizer st = new StringTokenizer(jerarquiaEmp, ",", false);
                jerarquia = new Vector();
                while (st.hasMoreElements()) {
                    jerarquia.add(((String)st.nextElement()).trim());
                }
                return jerarquia;
            }
        }
        
        jerarquiaSeg = TablaValores.getValor("Seguridad.parametros", id, "Jerarquias");
        
        if (jerarquiaSeg != null && !jerarquiaSeg.trim().equals("")) {
            StringTokenizer st= new StringTokenizer(jerarquiaSeg, ",", false);
            jerarquia= new Vector();
            while (st.hasMoreElements()) {
                jerarquia.add(((String)st.nextElement()).trim());
            }
            return jerarquia;
        }
        else if (jerarquiaSeg != null && jerarquiaSeg.trim().equals("")) {
            jerarquia= new Vector();
            jerarquia.add("SinPermiso");
            if (getLogger().isDebugEnabled()) {
                getLogger().debug( "[consultaJerarquia] rut ["+ssbci.getCliente().getRut()+"] consultaJerarquia   : SinPermiso");
            }
            return jerarquia;
        }
        return jerarquia;
    }
    
    /**
     * Metodo que obtiene el modo en que el usuario se autenticara.
     * Recibe como parametro un vector con la jerarquia que posee el servicio.
     * Retorna el privilegio que se usara para la transferencia.
     *
     * Registro de versiones:<ul>
     *
     * <li>1.0 ??/??/????, Desconocido: version inicial.
     * <li>2.0 02/08/2017, Marcelo Fuentes (SEnTRA) - Ricardo Carrasco (Ing. Soft. BCI): Se mueve la logica al metodo
     * {@link #tienePrivilegio(Vector, SessionBCI)} pasando la session BCI como parametro.</li>
     *
     * @param jerarquia Vector con la jerarquia.
     * @param req la solicitud HTTP que se esta procesando.
     * @paramidName Identificador del atributo sesion BCI.
     * @return String Retorna el privilegio.
     * @since ?.?
     */
    public static String tienePrivilegio(Vector jerarquia, HttpServletRequest req, String idName) {
        HttpSession ss = req.getSession(false);
        SessionBCI ssbci = (SessionBCI) ss.getAttribute(idName);
        return tienePrivilegio(jerarquia, ssbci);
    }
    
    /**
     * Metodo que obtiene el modo en que el usuario se autenticara.
     * Recibe como parametro un vector con la jerarquia que posee el servicio.
     * Retorna el privilegio que se usara para la transferencia.
     *
     * Registro de versiones:<ul>
     *
     * <li>1.0 02/08/2017, Marcelo Fuentes (SEnTRA) - Ricardo Carrasco (Ing. Soft. BCI): version inicial.
     *
     * @param jerarquia Vector con la jerarquia.
     * @param session Sesion BCI.
     * @return String Retorna el privilegio.
     * @since 1.19
     */
    public static String tienePrivilegio(Vector jerarquia, SessionBCI session) {
        if (jerarquia != null && jerarquia.size() != 0) {
            for (Enumeration codigos= jerarquia.elements(); codigos.hasMoreElements();) {
                String elemento= (String)codigos.nextElement();
                if (elemento.equals("SafeSigner")) {
                    getLogger().debug("[tienePrivilegio]   : SafeSigner");
                    
                    String activoSegundaClave = TablaValores.getValor(TABLADEPARAMETROS, "active", "valor");
                    if (getLogger().isDebugEnabled()) {
                        getLogger().debug("[tienePrivilegio]: identificadorEstadoSegundaClave "
                            + (String) session.getAttrib("identificadorEstadoSegundaClave"));
                    }
                    String estadoSS = (String) session.getAttrib("identificadorEstadoSegundaClave");
                    
                    if (activoSegundaClave.equalsIgnoreCase(estadoSS)) {
                        getLogger().debug("[tienePrivilegio] Es SafeSigner y esta activo");
                        return "SafeSigner";
                    }
                    
                    try {
                        EstrategiaSafeSigner estrategia = new EstrategiaSafeSigner();
                        
                        getLogger().debug("[tienePrivilegio]:antes de obtener cliente");
                        Cliente cliente = session.getCliente();
                        getLogger().debug("[tienePrivilegio]:antes de obtener canal ");
                        Canal canal = session.getCanal();
                        getLogger().debug("[tienePrivilegio]:antes de new ParametrosEstrategiaSegundaClaveTO");
                        ParametrosEstrategiaSegundaClaveTO paramSegundaClave = new 
                        ParametrosEstrategiaSegundaClaveTO(
                            "",
                            cliente,
                            canal,
                            session);
                        
                        EstadoSegundaClaveTO estatus = estrategia.consultarEstadoSegundaClave(paramSegundaClave);
                        
                        if (estatus != null) {
                            if (estatus.isEstatus()) {
                            	session.setAttrib("identificadorPrioridadSegundaClave", "SafeSigner");
								session.setAttrib("identificadorEstadoSegundaClave", estatus.getGlosa());
                                return "SafeSigner";
                            }
                        }
                    }
                    catch (Exception e) {
                        if (getLogger().isEnabledFor(Level.ERROR)) {
                            getLogger().error("Error al [consultaEstadoSegundaClave]" + e);
                        }
                    }
                }
                else if (elemento.equals("Token")) {
                    if (checkToken(session) == true) {
                        getLogger().debug( "tienePrivilegio   : Token");
                        return "Token";
                    }
                }
                else if (elemento.equals("Passcode")) {
                    if (checkPasscode(session) == true) {
                        getLogger().debug( "tienePrivilegio   : Passcode");
                        return "Passcode";
                    }
                }
                else if (elemento.equals(ENTRUST_TOKEN) || elemento.equals(ENTRUST_SOFTTOKEN)) {
                    try {
                        String usuarioId = "";
                        String activoSegundaClave = TablaValores.getValor(TABLA_PARAMETROS_ENTRUST, "active", "valor");
                        
                        getLogger().debug("[tienePrivilegio]:antes de obtener cliente");
                        Cliente cliente = session.getCliente();
                        getLogger().debug("[tienePrivilegio]:antes de obtener canal ");
                        Canal canal = session.getCanal();
                        if (canal.getCanalID().equals(CANAL_PYME)) {
                            usuarioId = session.getRutUsuario() + String.valueOf(session.getDvUsuario().charAt(0));
                        }
                        else {
                            usuarioId = String.valueOf(cliente.getRut()) + String.valueOf(cliente.getDigito());
                        }
                        
                        getLogger().debug("[tienePrivilegio]:antes de new ParametrosEstrategiaSegundaClaveTO");
                        UsuarioSegundaClaveTO usuarioTO = new UsuarioSegundaClaveTO(usuarioId, obtieneDominio(canal.getCanalID()), canal.getCanalID());
                        ParametrosEstrategiaSegundaClaveTO paramSegundaClave = new ParametrosEstrategiaSegundaClaveTO(usuarioTO);
                        
                        PrioridadSegundaClaveIdTO prio = PrioridadSegundaClaveIdTO.getPorNombre(elemento);
                        EstrategiaSegundaClave estrategiaSegundaClave = EstrategiaAutenticacionFactory.obtenerEstrategiaSegundaClave(prio);
                        EstadoSegundaClaveTO estatus = estrategiaSegundaClave.consultarEstadoSegundaClave(paramSegundaClave);
                        
                        if (estatus != null) {
                            if (estatus.isEstatus()) {
                                session.setAttrib("identificadorPrioridadSegundaClave", elemento);
                                session.setAttrib("identificadorEstadoSegundaClave", estatus.getGlosa() != null ? estatus.getGlosa() : activoSegundaClave);
                                return elemento;
                            }
                        }
                        
                    }
                    catch (Exception e) {
                        logger.error("[estadoSegundaClave] [" + e.getMessage());
                     }
                }
                else if (elemento.equals("ClaveInternet")) {
                    getLogger().debug( "tienePrivilegio   : ClaveInternet");
                    return "ClaveInternet";
                }
                else if (elemento.equals("SinPermiso")) {
                    getLogger().debug( "tienePrivilegio    : SinPermiso");
                    return "ServSinPermiso";
                }
            }
            getLogger().debug( "tienePrivilegio   : usuario sin permiso");
            return "UsuSinPermiso";
        }
        else {
        	getLogger().debug( "tienePrivilegio   : servicio sin permiso");
        }
        return "ServSinPermiso";
    }
    
    /**
     * Verifica si el cliente posee Token como dispositivo de seguridad.
     *
     * Registro de versiones:<ul>
     *
     * <li>1.0 ??/??/????, Desconocido: version inicial.
     * <li>2.0 02/08/2017, Marcelo Fuentes (SEnTRA) - Ricardo Carrasco (Ing. Soft. BCI): Se mueve la logica al metodo
     * {@link #checkToken(SessionBCI)} pasando la session BCI como parametro.</li>
     *
     * @param req la solicitud HTTP que se esta procesando.
     * @param idName Identificador del atributo sesion BCI.
     * @return verdadero, si lo tiene, falso de lo contrario.
     * @since ?.?
     */
    public static boolean checkToken(HttpServletRequest req, String idName) {
        HttpSession ss = req.getSession(false);
        SessionBCI ssbci = (SessionBCI)ss.getAttribute(idName);
        return checkToken(ssbci);
    }
    
    /**
     * Verifica si el cliente posee Token como dispositivo de seguridad.
     *
     * Registro de versiones:<ul>
     *
     * <li>1.0 02/08/2017, Marcelo Fuentes (SEnTRA) - Ricardo Carrasco (Ing. Soft. BCI): version inicial.
     *
     * @param session Sesion BCI.
     * @return verdadero, si lo tiene, falso de lo contrario.
     * @since 1.19
     */
    public static boolean checkToken(SessionBCI ssbci) {
        Hashtable seguridad= (Hashtable)ssbci.getMetodosAutenticacion();
        
        if (seguridad != null) {
            Boolean estadoMetodo = (Boolean)seguridad.get("TOK");
            if (estadoMetodo != null) {
                if (getLogger().isDebugEnabled()) {
                    getLogger().debug( "[ckeckToken] rut["+ssbci.getCliente().getRut()+"]  :Posee Token");
                }
                return true;
            }
            
            estadoMetodo = (Boolean)seguridad.get("TOKBLO");
            if (estadoMetodo != null) {
                if (getLogger().isDebugEnabled()) {
                    getLogger().debug( "[ckeckToken] rut["+ssbci.getCliente().getRut()+"]  :Posee Token bloqueado");
                }
                return true;
            }
        }
        if (getLogger().isDebugEnabled()) {
            getLogger().debug( "[ckeckToken] rut["+ssbci.getCliente().getRut()+"]  : return false");
        }
        return false;
    }
    
    /**
     * Verifica si el cliente posee Passcode como dispositivo de seguridad.
     *
     * Registro de versiones:<ul>
     *
     * <li>1.0 ??/??/????, Desconocido: version inicial.
     * <li>2.0 02/08/2017, Marcelo Fuentes (SEnTRA) - Ricardo Carrasco (Ing. Soft. BCI): Se mueve la logica al metodo
     * {@link #checkPasscode(SessionBCI)} pasando la session BCI como parametro.</li>
     *
     * @param req la solicitud HTTP que se esta procesando.
     * @param idName Identificador del atributo sesion BCI.
     * @return verdadero, si lo tiene, falso de lo contrario.
     * @since ?.?
     */
    public static boolean checkPasscode(HttpServletRequest req, String idName) {
        HttpSession ss = req.getSession(false);
        SessionBCI ssbci = (SessionBCI)ss.getAttribute(idName);
        return checkPasscode(ssbci);
    }
    
    /**
     * Verifica si el cliente posee Passcode como dispositivo de seguridad.
     *
     * Registro de versiones:<ul>
     *
     * <li>1.0 02/08/2017, Marcelo Fuentes (SEnTRA) - Ricardo Carrasco (Ing. Soft. BCI): version inicial.
     *
     * @param session Sesion BCI.
     * @return verdadero, si lo tiene, falso de lo contrario.
     * @since 1.19
     */
    public static boolean checkPasscode(SessionBCI ssbci) {
        boolean tienePasscode;
        try {
            if (!ssbci.getCanal().getCanalID().equals("150")) {
                PassCode passCode= new PassCode(ssbci.getCanal().getCanalID());
                
                if(ssbci.getCanal().getCanalID().equals("230")) {
                    if (getLogger().isDebugEnabled()) {
                        getLogger().debug("[checkPasscode] rut ["+ssbci.getCliente().getRut()+"]: El canal es BCIExpress");
                    }
                    tienePasscode = passCode.tienePassCode(Long.parseLong(ssbci.getRutUsuario()), ssbci.getDvUsuario().charAt(0));
                    if (tienePasscode) {
                        if (getLogger().isDebugEnabled()) {
                            getLogger().debug( "[checkPasscode] rut ["+ssbci.getCliente().getRut()+"]: Tiene passcode");
                        }
                        return true;
                    } else {
                        if (getLogger().isDebugEnabled()) {
                            getLogger().debug( "[checkPasscode] rut ["+ssbci.getCliente().getRut()+"]:No Tiene passcode");
                        }
                        return false;
                    }
                } else {
                    tienePasscode = passCode.tienePassCode(ssbci.getCliente().getRut(), ssbci.getCliente().getDigito());
                    if (tienePasscode) {
                        if (getLogger().isDebugEnabled()) {
                            getLogger().debug( "[checkPasscode] rut ["+ssbci.getCliente().getRut()+"]: Tiene passcode");
                        }
                        return true;
                    } else {
                        if (getLogger().isDebugEnabled()) {
                            getLogger().debug( "[checkPasscode] rut ["+ssbci.getCliente().getRut()+"]:No Tiene passcode");
                        }
                        return false;
                    }
                }
            }
        }
        catch (Exception e) {
            if (getLogger().isEnabledFor(Level.ERROR)) {
                getLogger().error( "[checkPasscode] rut ["+ssbci.getCliente().getRut()+"]:No Tiene passcode por catch:"
                        + ErroresUtil.extraeStackTrace(e));
            }
            return false;
        }
        if (getLogger().isDebugEnabled()) {
            getLogger().debug( "[checkPasscode] rut ["+ssbci.getCliente().getRut()+"]:No Tiene passcode final");
        }
        return false;
    }

     /**
     *
     * <p>
     * Registro de versiones:<ul>
     * <li> 1.0 (??/??/????, Desconocido) : version inicial
     * </ul>
     * <p>
     * @param req
     * @param idName
     * @param servicio
     * @since 1.4
     */
    public static boolean checkPassSupUser(HttpServletRequest req, String idName, String servicio) {

        HttpSession ss= req.getSession(false);
        SessionBCI ssbci= (SessionBCI)ss.getAttribute(idName);

        Canal cnl= ssbci.getCanal();
        String id= cnl.getCanalID() + "-" + servicio;
        String condicion= TablaValores.getValor("SupUser.parametros", id, "condicion");
        String valor= TablaValores.getValor("SupUser.parametros", id, "valor");
        String activo= TablaValores.getValor("SupUser.parametros", id, "activo");
        tituloPassSupUser= TablaValores.getValor("SupUser.parametros", id, "titulo");

        if (activo != null) if (activo.equals("Si")) {
            String parametro= req.getParameter(condicion);
            if (parametro != null) if (parametro.equals(valor)) return true;
        }
        return false;
    }

    /**
     * Valida el estado de un token
     * <p>
     * Registro de versiones:<ul>
     * <li> 1.0 (??/??/????, Desconocido) : version inicial.
     * <li> 2.0 (25/11/2013, Pedro Carmona Escobar (SEnTRA) : Se incorpora la llamada para consultar por el estado
     *              de segundaClave. En caso de operar con safeSigner se finaliza la ejecuci�n del m�todo. A ra�z
     *              de esta incorporaci�n, se realiza un cambio en la firma del m�todo, agregando el 
     *              par�metros 'servicio'.
     * <li> 2.1 02/08/2017  Marcelo Fuentes (SEnTRA) - Ricardo Carrasco (Ing. Soft. BCI): Se modifica instanciacion del 
     *  objeto "parametrosEstrategiaSegundaClaveTO" invocando a otro constructor que no utiliza el request como atributo.</li>
     * </ul>
     * <p>
     * @param req con el request.
     * @param idName con el id de la sessionBci.
     * @param servicio con el id de servicio por el cual se esta validando la solicitud de token.
     * @exception SeguridadException en caso de excepci�n.
     * @since 1.4
     */
    private static void validarToken(HttpServletRequest req, String idName, String servicio) throws SeguridadException {

        //if (logger.isDebugEnabled()) {logger.debug( "inicia validarToken()");}
        HttpSession ss= req.getSession(false);
        SessionBCI ssbci= (SessionBCI)ss.getAttribute(idName);
        //if (logger.isDebugEnabled()) {logger.debug( "LA SESION ES: <" + ssbci + ">");}
        Hashtable seguridad= (Hashtable)ssbci.getMetodosAutenticacion();
        //if (logger.isDebugEnabled()) {logger.debug( "LA TABLA DE SEGURIDAZ ES: <" + seguridad + ">");}

        String tokenActivo;
        String id= ssbci.getCanal().getCanalID();
        tokenActivo= TablaValores.getValor("Seguridad.parametros", "tokenActivo", id);

        /**
         * Verifica el estado de la autenticaci�n token en el canal
         */
        if (tokenActivo == null || !tokenActivo.equalsIgnoreCase("true")) {
            tokenActivo= "false";
            return;
        }

        ParametrosEstrategiaSegundaClaveTO parametrosEstrategiaSegundaClaveTO;
        parametrosEstrategiaSegundaClaveTO = new ParametrosEstrategiaSegundaClaveTO(
                servicio, ssbci.getCliente(), ssbci.getCanal(), ssbci,
                new CamposDeLlaveTO());
		
        try {
			if (obtenerEstadoSegundaClave(parametrosEstrategiaSegundaClaveTO)) {
                if (logger.isDebugEnabled()) {logger.debug( "[validarToken] rut ["+ssbci.getCliente().getRut()
                        +"]: Est� autenticado con SafeSigner");}
				return;
			}
		} catch (NoSessionException e) {
		    if (logger.isEnabledFor(Level.ERROR)){
		        logger.error( "[validarToken] rut ["+ssbci.getCliente().getRut()+"] NoSessionException " ,e);
		    }
		} catch (IOException e) {
		    if (logger.isEnabledFor(Level.ERROR)){
                logger.error( "[validarToken] rut ["+ssbci.getCliente().getRut()+"] IOException " ,e);
            }
		}
		
        if (seguridad != null) {
            //if (logger.isDebugEnabled()) {logger.debug( "validarToken(): variable de sesi�n 'MetodosAutenticacion' no es null");}
            Boolean estadoMetodoToken= (Boolean)seguridad.get("TOK");
            if (estadoMetodoToken != null) {
                if (logger.isDebugEnabled()) {logger.debug( "[validarToken] rut ["+ssbci.getCliente().getRut()+"]: Posee Token");}
                if (estadoMetodoToken.booleanValue() == true) {
                    if (logger.isDebugEnabled()) {logger.debug( "[validarToken] rut ["+ssbci.getCliente().getRut()+"]: Est� autenticado con token");}
                } else {
                    if (logger.isDebugEnabled()) {logger.debug( "[validarToken] rut ["+ssbci.getCliente().getRut()+"]: No est� autenticado con token");}
                    throw new SeguridadException("TOKEN");
                }
            }
            return;
        }
        //if (logger.isDebugEnabled()) {logger.debug( "finaliza validarToken()");}
        return;
    }

    /**
     * M�todo que asigna una URL de retoma en la sesi�n HTTP, para ser utilizada como
     * primer servicio tras una desconexi�n por timeout.
     * <p>
     * Registro de versiones:<ul>
     * <li> 1.0 (??/??/????, Desconocido) : version inicial
     * </ul>
     * <p>
     * @param sesionHttp
     * @param canal
     * @param servicio
     * @param rut
     * @since 1.4
     */
    private static void asignaServicioRetoma(HttpSession sesionHttp, String canal, String servicio, String rut) {

        //if (logger.isDebugEnabled()) {logger.debug( "En, asignaServicioRetoma =======================");}
        String llave= null;
        String estado= null;
        String llaveRetoma= null;
        String urlRetoma= null;
        llave= canal + "-" + servicio;
        //////////     llaveRetoma = idName + "-retoma";
        llaveRetoma= rut + "llaveRetoma";

        estado= TablaValores.getValor(archivoConfiguracionRetoma, llave, "activo");
        //if (logger.isDebugEnabled()) {logger.debug( "Estado de retoma para servicio '" + servicio + "': " + estado);}
        if (estado == null || !estado.equalsIgnoreCase("si")) {
            //retoma no activa para este servicio
            //if (logger.isDebugEnabled()) {logger.debug( "retoma no activa para este servicio");
            sesionHttp.setAttribute(llaveRetoma, "");
            return;
        }

        urlRetoma= TablaValores.getValor(archivoConfiguracionRetoma, llave, "URLRetoma");
        if (urlRetoma == null) {
            //url de retoma no configurada
            if (logger.isDebugEnabled()) {logger.debug( "[asignaServicioRetoma] url de retoma no configurada");}
            sesionHttp.setAttribute(llaveRetoma, "");
            return;
        }
        //if (logger.isDebugEnabled()) {logger.debug( "Asignando url de retoma OK: '" + urlRetoma + "'");}
        //if (logger.isDebugEnabled()) {logger.debug( "Asignando llave de retoma OK: '" + llaveRetoma + "'");}
        sesionHttp.setAttribute(llaveRetoma, urlRetoma);
    }


    /**
     * Verifica si la transaccion corresponde a una transac. de pago o
     * transferencia.
     * <p>
     * Registro de versiones:<ul>
     * <li> 1.0 (??/??/????, Desconocido) : version inicial
     * <li> 1.1 (28/01/2013, H�ctor Hern�ndez Orrego (SEnTRA)) : Se incorpora verificaci�n para permitir el uso
     * de este m�todo por tipoUsuario = 'E', empresa, a�adiendo condici�n al flujo.
     * Se a�aden logs de valores obtenidos desde tabla de parametros.
     * </ul>
     * <p>
     * @param req
     * @param idName
     * @param servicio
     * @return retorna si es pago de transferencia
     * @since 1.4
     */
    public static boolean esPagoTransferencia(HttpServletRequest req, String idName,
            String servicio) {
		HttpSession ss = req.getSession(false);
		SessionBCI ssbci = (SessionBCI) ss.getAttribute(idName);
		Canal cnl = ssbci.getCanal();
		String id = cnl.getCanalID() + "-" + servicio;
		String condicion = TablaValores.getValor("Seguridad.parametros", id,
		               "condicion");
		String valor = TablaValores.getValor("Seguridad.parametros", id, "valor");
		String activo = TablaValores.getValor("Seguridad.parametros", id, "activo");
		tituloPassCode = TablaValores.getValor("Seguridad.parametros", id, "titulo");
		
		if (logger.isDebugEnabled()) {
			logger.debug("[esPagoTransferencia]activo[" + activo + "]");
			logger.debug("[esPagoTransferencia]valor[" + valor + "]");
			logger.debug("[esPagoTransferencia]tituloPassCode[" + tituloPassCode + "]");
		}
		if (ssbci.getCliente() != null && ssbci.getCliente().tipoUsuario != null
				&& (ssbci.getCliente().tipoUsuario.equals("P")|| ssbci.getCliente().tipoUsuario.equals("E"))
				&& activo != null && activo.equals("Si")) {
		String parametro = req.getParameter(condicion);
		if (parametro != null && parametro.equals(valor))
			return true;
		}
        return false;
    }

    /**
     * Verifica si el usuario necesita activar su token.
     * <p>
     * Registro de versiones:<ul>
     * <li> 1.0 (??/??/????, Desconocido) : version inicial
     * </ul>
     * <p>
     * @param req
     * @param sessbci
     * @return retorna si es necesario activar token
     * @exception IOException
     * @exception SeguridadException
     * @since 1.4
     */
      private static boolean necesitaActivarToken(HttpServletRequest req, SessionBCI sessbci)
          throws IOException, SeguridadException {

        ServiciosSeguridad seguridadBean = null;
        try {
            InitialContext ic = JNDIConfig.getInitialContext();
            ServiciosSeguridadHome homeServSeg =
             (ServiciosSeguridadHome)ic.lookup("wcorp.serv.seguridad.ServiciosSeguridad");
            seguridadBean = (ServiciosSeguridad)homeServSeg.create();
            //if (logger.isDebugEnabled()) {logger.debug( "metodo necesitaActivarToken: servicio creado");}
        } catch(Exception te){
        	if (logger.isEnabledFor(Level.ERROR)) {logger.error( "[necesitaActivarToken]: error en el ejb");}
        	return false;
        }

        //verifico el estado del token
        // y si es clave autorizada se debe activar.
        String tokenEstadoLlave = null;
        try{
           tokenEstadoLlave =  seguridadBean.determinaEstadoToken(
                                sessbci.getCliente().getRut(),
                                sessbci.getCliente().getDigito()
                              );
        }
        catch(Exception te){
          return false;
        }
       if(tokenEstadoLlave==null)
           return false;

       // en caso de que el estado del token sea autorizado es  necesario
       // enviar el usuario a la pagina de activacion
       if( (tokenEstadoLlave.trim()).toUpperCase().equals("TOKENST04") ){
         if (logger.isDebugEnabled()) {logger.debug( "[necesitaActivarToken]: cliente debe activar token");}
         return true;
      }
      return false;
    }

    /**
     * M�todo que valida si para algun canal se encuentra activa la clave dos, ya que para algunos canales no se encuentra
     * activa la clave dos ej Tbanc.
     * <p>
     * Registro de versiones:<ul>
     * <li> 1.0 (21/06/2006, Victor Urra(Novared)) : version inicial
     * <li> 2.0 (13/09/2006, Paola Parra (SEnTRA)) : Se agrega log para canal y boolean, valor que indica si canal tiene activa
     *                                               la clave dos, adem�s se extrae valor de activaci�n de clave dos en archivo
     *                                               de parametros, instanciando a la clase Boolean para extraer un valor de tipo
     *                                               "boolean" y no utilizando el metodo "getBoolean", como estaba anteriormente.
     * </ul>
     * <p>
     * @param idCanal id del canal que se esta evaluando
     * @return verdadero si la clave dos se encuentra activa para el idCanal
     * @since 1.6
     */
      private static boolean validaCanalClaveDos(String idCanal){
        if (logger.isDebugEnabled()) {logger.debug("[validaCanalClaveDos] Canal["+idCanal+"]");}
        boolean activaClaveDos = new Boolean(TablaValores.getValor("Seguridad.parametros", "passcodeActivo", idCanal)).booleanValue();
        if (logger.isDebugEnabled()) {logger.debug("[validaCanalClaveDos] activaClaveDos["+activaClaveDos+"]");}
        return activaClaveDos;
      }
      
     /**
     * M�todo que construye la respuesta de SafeSigner.
     * Consulta el estado de SafeSigner
     * <p>
     * Registro de versiones:<ul>
     * <li> 1.0 (09/05/2013, Ver�nica Cestari (Orand)) : Primera versi�n. Se incluye la generacion de llave para
     * SafeSigner.</li>
     * <li> 1.1 02/08/2017  Marcelo Fuentes (SEnTRA) - Ricardo Carrasco (Ing. Soft. BCI): Se modifica metodo obteniendo
     * directamente desde el parametro "parametrosEstrategiaSegundaClaveTO" la sesion BCI y pasandola como parametro 
     * a los metodos consultaJerarquia(SessionBCI, String) y tienePrivilegio(Vector, HttpServletRequest, String).
     * </li>
     * </ul>
     * <p>
     * @param parametrosEstrategiaSegundaClaveTO parametros de estrategia segunda clave.
     * @return boolean Devuelve el c�digo QR de la operaci�n o la respuesta de la validaci�n del c�digo QR.
     * @throws IOException excepcion de entrada y salida.
     * @throws SeguridadException seguridad excepcion.
     * @throws NoSessionException excepcion de no sesion.
     * @since 1.0
     */
    public static boolean obtenerEstadoSegundaClave(
        ParametrosEstrategiaSegundaClaveTO parametrosEstrategiaSegundaClaveTO) throws IOException,
        SeguridadException, NoSessionException {
        try {
            String sactive = TablaValores.getValor(TABLADEPARAMETROS, "active", "valor");
            String segundaClaveValor = TablaValores.getValor(TABLADEPARAMETROS, "segundaclave", "valor");
            int estatusOK = Integer.parseInt(TablaValores.getValor(TABLADEPARAMETROS, "estatusok", "valor"));
            
            if (logger.isDebugEnabled()) {
                logger.debug("[obtenerEstadoSegundaClave] Inicio");
            }
            
            SessionBCI sesionPpal = parametrosEstrategiaSegundaClaveTO.getSessionBCI();
            
            if (sesionPpal != null) {
                String rut= String.valueOf(sesionPpal.getCliente().getRut());
                String segundaClaveSS = (String) ((SessionBCI) parametrosEstrategiaSegundaClaveTO
                    .getSessionBCI()).getAttrib("identificadorPrioridadSegundaClave");
                
                if (segundaClaveSS == null) {
                    Vector test;
                    if (logger.isInfoEnabled()) {
                        logger.info("[obtenerEstadoSegundaClave] [" + rut
                            + "] Segunda Clave no esta en SessionBCI");
                    }
                    String privilegio = null;
                    if (logger.isInfoEnabled()) {
                        logger.info("[obtenerEstadoSegundaClave] [" + rut
                            + "] Antes de ejecutar consultarJerarquia");
                    }
                    test = consultaJerarquia(parametrosEstrategiaSegundaClaveTO.getSessionBCI(),
                        parametrosEstrategiaSegundaClaveTO.getServicio());
                    if (test != null && test.size() != 0) {
                        if (logger.isInfoEnabled()) {
                            logger.info("[obtenerEstadoSegundaClave] [" + rut
                                + "]" + "ControllerBCI Seguridad.parametros test no es nulo");
                        }
                        privilegio = tienePrivilegio(test, sesionPpal);
                        if (logger.isInfoEnabled()) {
                            logger.info("[obtenerEstadoSegundaClave] [" + rut
                                + "]" + "ControllerBCI Seguridad.parametros privilegio :" + privilegio);
                        }
                        parametrosEstrategiaSegundaClaveTO.setIdentificarPrioridadDeSegundaClave(privilegio);
                        sesionPpal.setAttrib("identificadorPrioridadSegundaClave", privilegio);
                        if (logger.isInfoEnabled()) {
                            logger.info("[obtenerEstadoSegundaClave] [" + rut
                                + "]" + "SessionBCI identificadorPrioridadSegundaClave :" + privilegio);
                        }
                    }
                    else {
                        if (logger.isInfoEnabled()) {
                            logger.info("[obtenerEstadoSegundaClave] [" + rut
                                + "]" + "No tiene jerarquia en Seguridad.parametros");
                        }
                        privilegio = "SinJerarquia";
                    }
                    if (test != null && test.size() != 0 && privilegio.equals("ServSinPermiso")) {

                        throw new SeguridadException("OTRO_01");
                    }
                }
                
                String estadoSS = (String) ((SessionBCI) parametrosEstrategiaSegundaClaveTO.getSessionBCI())
                    .getAttrib("identificadorEstadoSegundaClave");
                segundaClaveSS = (String) ((SessionBCI) parametrosEstrategiaSegundaClaveTO.getSessionBCI())
                    .getAttrib("identificadorPrioridadSegundaClave");
                
                EstadoSegundaClaveTO estatus = new EstadoSegundaClaveTO();
                
                if (segundaClaveValor.equals(segundaClaveSS)) {
                    if (sactive.equals(estadoSS)) {
                        estatus.setEstatus(true);
                        estatus.setIdCodigoEstado(estatusOK);
                        estatus.setGlosa(estadoSS);
                    }
                    else {
                        EstrategiaSegundaClave estrategia = new EstrategiaSafeSigner();
                        estatus = estrategia
                            .consultarEstadoSegundaClave(parametrosEstrategiaSegundaClaveTO);
                        sesionPpal.setAttrib("identificadorEstadoSegundaClave", estatus.getGlosa());
                        parametrosEstrategiaSegundaClaveTO.setIdentificadorEstadoSegundaClave(estatus.getGlosa());
                    }
                    
                    if(!estatus.isEstatus())
                    {
                        sesionPpal.removeAttrib("identificadorEstadoSegundaClave");
                    }
                    return estatus.isEstatus();
                }
            }
            else {
                if (logger.isDebugEnabled()) {
                    logger.debug("[obtenerEstadoSegundaClave] [sesionPpal]  la session principal es nula");
                }
            }
            return false;
        }
        catch (Exception ex) {
            logger.error("[obtenerEstadoSegundaClave] [Exception] Error de medodo,mensaje=<" + ex.getMessage()
                + ">", ex);
            return false;
        }
    }
    /**
     * M�todo que construye la respuesta de SafeSigner para realizar una transacci�n.
     * Consulta el estado de SafeSigner y genera el c�digo SafeSigner
     * <p>
     * Registro de versiones:<ul>
     * <li> 1.0 (04/03/2013, Victor Hugo Enero (Orand)) : Primera versi�n. Se incluye la generacion de llave para
     * SafeSigner
     * <li>1.1 02/08/2017  Marcelo Fuentes (SEnTRA) - Ricardo Carrasco (Ing. Soft. BCI): Se modifica instanciacion del 
     *  objeto "parametrosEstrategiaSegundaClaveTO" invocando a otro constructor que no utiliza el request como atributo.</li>
     * </ul>
     * <p>
     * @param request HTTP request.
     * @param idName Nombre de la sesi�n.
     * @param servicio Operaci�n que se est� realizando.
     * @param camposDeLlave hasmap Contiene la estructura general de safesigner y sus datos.
     * @return LlaveSegundaClaveTO Devuelve el c�digo QR de la operaci�n o la respuesta de la validaci�n del c�digo QR.
     * @throws IOException excepcion de entrada y salida.
     * @throws SeguridadException seguridad excepcion.
     * @throws NoSessionException excepcion de no sesion.
     * @since 1.13
     */
    public static LlaveSegundaClaveTO generarLlavesDeSegundaClave(HttpServletRequest request, String idName,
        String servicio, CamposDeLlaveTO camposDeLlave) throws IOException, 
        SeguridadException, NoSessionException {
        String activoSegundaClave = TablaValores.getValor(TABLADEPARAMETROS, "active", "valor");
        int estatusNOK = Integer.parseInt(TablaValores.getValor(TABLADEPARAMETROS, "estatusnok", "valor"));
        String glosa = TablaValores.getValor(TABLADEPARAMETROS, "estatusnok", "glosa");
        LlaveSegundaClaveTO llaveGenerada = new LlaveSegundaClaveTO();
        llaveGenerada.setEstatus(false);
        llaveGenerada.setIdCodigoEstado(estatusNOK);
        llaveGenerada.setGlosa(glosa);
        try {

            if (request == null) {
                throw new wcorp.serv.seguridad.NoSessionException("NOSESSION");
            }

            HttpSession hsess = request.getSession(false);
            if (hsess != null) {
                SessionBCI sesionPpal = (SessionBCI) hsess.getAttribute(idName);

                if (sesionPpal == null) {
                    if (logger.isDebugEnabled()) {
                        logger.debug("[generarLlavesDeSegundaClave] [sesionPpal]  la session principal es nula");
                    }
                }
                else {
                    if (logger.isInfoEnabled()) {
                        logger.info("[generarLlavesDeSegundaClave]  [" + sesionPpal.getAttrib("esMopaIII")
                            + "] Se obtiene de sesion principal esMopaIII");
                    }
                }

                if (sesionPpal != null) {
                    String rut= String.valueOf(sesionPpal.getCliente().getRut()+ sesionPpal.getCliente().getDigito());
                    boolean sesionPpalActiva = true;
                    try {
                        sesionPpal.checkAlive();
                    }
                    catch (Exception e) {
                        logger.error(
                            "[generarLlavesDeSegundaClave] [Exception] Error de metodo,mensaje=<" + e.getMessage()
                                + ">", e);
                        sesionPpalActiva = false;
                    }

                    if (!("true".equals(sesionPpal.getAttrib("esMopaIII")) && !sesionPpalActiva)) {
                        if (logger.isInfoEnabled()) {
                            logger.info("[generarLlavesDeSegundaClave] [" + rut
                                + "] Entra a bloque if [001]. RutUsuario");
                        }

                        String retomo = TablaValores.getValor(archivoConfiguracionRetoma, sesionPpal.getCanal()
                            .getCanalID(), "retomar");
                        if (retomo == null)
                            retomo = "";

                        if (!(retomo.equals("NO"))) {
                            try {
                                sesionPpal.checkAlive();
                            }
                            catch (SeguridadException se) {
                                logger.error(
                                    "[generarLlavesDeSegundaClave] [SeguridadException] Error de metodo,mensaje=<"
                                        + se.getMessage() + ">", se);
                                asignaServicioRetoma(hsess, sesionPpal.getCanal().getCanalID(), servicio,
                                    String.valueOf(rut));
                                sesionPpal.cerrar();
                                EliminarSession.eliminar((String) sesionPpal.getAttrib("sessionId"));

                                throw se;
                            }
                        }
                    }

                    ParametrosEstrategiaSegundaClaveTO parametrosEstrategiaSegundaClaveTO;
                    if (sesionPpal.getCliente() != null && sesionPpal.getCanal() != null) {
                        if (logger.isInfoEnabled()) {
                            logger.info("[generarLlavesDeSegundaClave] [" + rut
                                + "] Obtener Canal de SessionBCI" + sesionPpal.getCanal());
                        }
                        if (logger.isInfoEnabled()) {
                            logger.info("[generarLlavesDeSegundaClave] [" + rut
                                + "] Se inicializa parametrosEstrategiaSegundaClaveTO()");
                        }
                        parametrosEstrategiaSegundaClaveTO = new ParametrosEstrategiaSegundaClaveTO(
                            servicio, sesionPpal.getCliente(), sesionPpal.getCanal(), sesionPpal,
                            camposDeLlave);
                        if (logger.isInfoEnabled()) {
                            logger.info("[generarLlavesDeSegundaClave] [" + rut
                                + "] Rut del Cliente");
                        }

                        String identificadorPrioridadSegundaClave = (String) sesionPpal
                            .getAttrib("identificadorPrioridadSegundaClave");
                        if (identificadorPrioridadSegundaClave == null
                            || "".equals(identificadorPrioridadSegundaClave)) {
                            if (logger.isInfoEnabled()) {
                                logger.info("[generarLlavesDeSegundaClave] [" + rut
                                    + "] La variable de sessionBCI "
                                    + "identificadorPrioridadSegundaClave es nula o vacia");
                                logger.info("[generarLlavesDeSegundaClave] [" + rut
                                    + "] Antes de ejecutar obtenerJerarquiaYEstadosSegundaClave");
                            }
                            llaveGenerada = 
                                obtenerJerarquiaYEstadosSegundaClave(parametrosEstrategiaSegundaClaveTO, request);
                        }
                        else {
                            if (!isTieneLlave(identificadorPrioridadSegundaClave)) {
                                if (logger.isInfoEnabled()) {
                                    logger.info("[generarLlavesDeSegundaClave] ["
                                        + rut
                                        + "] No tiene llave previa y se rompre el flujo ");
                                }
                                return llaveGenerada;
                            }
                            if (obtenerEstadoSegundaClave(parametrosEstrategiaSegundaClaveTO))
                            {
	                            String identificadorEstadoSegundaClave = (String) sesionPpal
	                                .getAttrib("identificadorEstadoSegundaClave");
	                            if (logger.isInfoEnabled()) {
	                                logger
	                                    .info("[generarLlavesDeSegundaClave] [" + rut
	                                        + "] El identificadorEstadoSegundaClave es "
	                                        + identificadorEstadoSegundaClave);
	                            }
	                            EstrategiaSegundaClave estrategia = new EstrategiaSafeSigner();
	                            if (identificadorEstadoSegundaClave.equalsIgnoreCase(activoSegundaClave)) {
	                                if (logger.isInfoEnabled()) {
	                                    logger.info("[generarLlavesDeSegundaClave] ["
	                                        + rut + "] Antes de ejecutar a generarLlave");
	                                }
	                                
	                                hsess.setAttribute("camposDeLlave", parametrosEstrategiaSegundaClaveTO.getCamposDeLlave());
	                                llaveGenerada = estrategia.generarLlave(parametrosEstrategiaSegundaClaveTO);
	                                
	                                if(!llaveGenerada.isEstatus())
	                                {
	                                    sesionPpal.removeAttrib("identificadorEstadoSegundaClave");
	                                }
	                                
	                                if (logger.isInfoEnabled()) {
	                                    logger
	                                        .info("[generarLlavesDeSegundaClave] [" + rut
	                                            + "] Llave Generada es" + llaveGenerada);
	                                }
	                            }
	                            else {
	                                if (logger.isInfoEnabled()) {
	                                    logger.info("[generarLlavesDeSegundaClave] ["
	                                        + rut
	                                        + "] El Estado de Safesigner no es Activo y se rompe el flujo");
	                                }
	                                return llaveGenerada;
	                            }
                            }
                        }
                    }

                }
            }

            return llaveGenerada;
        }
        catch (Exception ex) {
            logger.error("[generarLlavesDeSegundaClave] [Exception] Error de medodo,mensaje=<" + ex.getMessage()
                + ">", ex);
            return llaveGenerada;
        }
    }

    /**
     * M�todo para obtener la jerarquia y estado de segunda clave
     * en caso de ser activa la segunda clave se genera la llave
     * <p>
     * Registro de versiones:<ul>
     * <li> 1.0 (04/03/2013, Victor Hugo Enero (Orand)) : Primera versi�n. Se incluye la generacion de llave para
     * SafeSigner
     * <li> 2.0 02/08/2017  Marcelo Fuentes (SEnTRA) - Ricardo Carrasco (Ing. Soft. BCI): Se modifica firma del metodo
     * agregando parametro de tipo HttpServletRequest. Lo anterior debido a que se elimino de TO 
     * "ParametrosEstrategiaSegundaClaveTO" este atributo y se necesita setear en el request el atributo "camposDeLlave".</li>
     * </ul>
     * <p>
     * @param paramSegundaClaveTO Parametro de estrategia segunda clave.
     * @param req la solicitud HTTP que se esta procesando.
     * @return RespuestaSegundaClave Devuelve el c�digo QR de la operaci�n o la respuesta de la validaci�n del c�digo QR.
     * @throws IOException excepcion de entrada y salida.
     * @throws SeguridadException seguridad excepcion.
     * @throws NoSessionException excepcion de no sesion.
     * @since 1.0
     * 
     */
    public static LlaveSegundaClaveTO obtenerJerarquiaYEstadosSegundaClave(
        ParametrosEstrategiaSegundaClaveTO paramSegundaClaveTO, HttpServletRequest req) throws IOException, SeguridadException,
        NoSessionException {
        int estatusNOK = Integer.parseInt(TablaValores.getValor(TABLADEPARAMETROS, "estatusnok", "valor"));
        String glosa = TablaValores.getValor(TABLADEPARAMETROS, "estatusnok", "glosa");
        LlaveSegundaClaveTO llaveGenerada = new LlaveSegundaClaveTO();

        llaveGenerada.setEstatus(false);
        llaveGenerada.setIdCodigoEstado(estatusNOK);
        llaveGenerada.setGlosa(glosa);

        try {
            String activoSegundaClave = TablaValores.getValor(TABLADEPARAMETROS, "active", "valor");
            Vector test;
            HttpSession hsess = req.getSession(false);
            SessionBCI sesionPpal = paramSegundaClaveTO.getSessionBCI();
            if (hsess != null) {
                String rut= String.valueOf(sesionPpal.getCliente().getRut()+ sesionPpal.getCliente().getDigito());
                String privilegio = null;
                if (logger.isInfoEnabled()) {
                    logger.info("[obtenerJerarquiaYEstadosSegundaClave] [" + rut
                        + "] Antes de ejecutar consultarJerarquia");
                }
                test = consultaJerarquia(sesionPpal, paramSegundaClaveTO.getServicio());
                if (test != null && test.size() != 0) {
                    if (logger.isInfoEnabled()) {
                        logger.info("[obtenerJerarquiaYEstadosSegundaClave] [" + rut
                            + "]" + "ControllerBCI Seguridad.parametros test no es nulo");
                    }
                    privilegio = tienePrivilegio(test, sesionPpal);
                    if (logger.isInfoEnabled()) {
                        logger.info("[obtenerJerarquiaYEstadosSegundaClave] [" + rut
                            + "]" + "ControllerBCI Seguridad.parametros privilegio :" + privilegio);
                    }
                    paramSegundaClaveTO.setIdentificarPrioridadDeSegundaClave(privilegio);
                    sesionPpal.setAttrib("identificadorPrioridadSegundaClave", privilegio);
                    if (logger.isInfoEnabled()) {
                        logger.info("[obtenerJerarquiaYEstadosSegundaClave] [" + rut
                            + "]" + "SessionBCI identificadorPrioridadSegundaClave :" + privilegio);
                    }
                }
                else {
                    if (logger.isInfoEnabled()) {
                        logger.info("[obtenerJerarquiaYEstadosSegundaClave] [" + rut
                            + "]" + "No tiene jerarquia en Seguridad.parametros");
                    }
                    privilegio = "SinJerarquia";
                }

                if (test != null && test.size() != 0 && privilegio.equals("ServSinPermiso")) {

                    throw new SeguridadException("OTRO_01");
                }

                if (logger.isInfoEnabled()) {
                    logger.info("[obtenerJerarquiaYEstadosSegundaClave] [" + rut
                        + "]" + " Antes de ejecutar esPagoTransferencia");
                }
                boolean transacPago = esPagoTransferencia(req, "sessionBci",
                        paramSegundaClaveTO.getServicio());
                paramSegundaClaveTO.setPagoTransferencia(transacPago);

                if (!isTieneLlave(privilegio)) {
                    if (logger.isInfoEnabled()) {
                        logger.info("[obtenerJerarquiaYEstadosSegundaClave] [" + rut
                            + "]" + "No tiene llave previa y se rompe el flujo");
                    }
                    return llaveGenerada;
                }

                EstrategiaSegundaClave estrategia = new EstrategiaSafeSigner();

                if (logger.isInfoEnabled()) {
                    logger.info("[obtenerJerarquiaYEstadosSegundaClave] ["
                        + sesionPpal.getCliente().getRut()
                        + "]"
                        + "El parametro de Session identificadorEstadoSegundaClave es"
                        + ((SessionBCI) paramSegundaClaveTO.getSessionBCI())
                            .getAttrib("identificadorEstadoSegundaClave"));
                }

                String estadoSS = (String) ((SessionBCI) paramSegundaClaveTO.getSessionBCI())
                    .getAttrib("identificadorEstadoSegundaClave");
				
				if (activoSegundaClave.equalsIgnoreCase(estadoSS)) {
                    if (logger.isInfoEnabled()) {
                        logger.info("[obtenerJerarquiaYEstadosSegundaClave] [" + rut
                            + "]" + "Antes de ejecutar a generarLlave");
                    }
                    hsess.setAttribute("camposDeLlave", paramSegundaClaveTO.getCamposDeLlave());
                    llaveGenerada = estrategia.generarLlave(paramSegundaClaveTO);
                }
                else {
                    if (logger.isInfoEnabled()) {
                        logger.info("[obtenerJerarquiaYEstadosSegundaClave] [" + rut
                            + "]" + " El estatus de consultarEstadoSegundaClave "
                            + "es diferente a Activo y se rompe flujo");
                    }
                    return llaveGenerada;
                }

            }
            return llaveGenerada;
        }
        catch (Exception ex) {
            logger.error(
                "[obtenerJerarquiaYEstadosSegundaClave] [Exception] Error de medodo,mensaje=<" + ex.getMessage()
                    + ">", ex);
            return llaveGenerada;
        }
    }

    /**
     * M�todo que valida codigo de segunda clave para aprobar o rechazar una transacci�n.
     * <p>
     * Registro de versiones:<ul>
     * <li> 1.0 (05/03/2013, Ver�nica Cestari(Orand)) : Primera versi�n. Se incluye validacion de llave para
     * SafeSigner
     * <li>1.2 02/08/2017  Marcelo Fuentes (SEnTRA) - Ricardo Carrasco (Ing. Soft. BCI): Se modifica instanciacion del 
     *  objeto "parametrosEstrategiaSegundaClaveTO" invocando a otro constructor que no utiliza el request como atributo.
     * </ul>
     * <p>
     * @param request El Request de la session.
     * @param idName El idName de la session.
     * @param servicio El nombre del servicio que se esta operando.
     * @param segundaClave Datos que contiene el dato de validacion.Contiene el rut del cliente y el codigo de confirmaci�n.
     * @return RespuestaSegundaClave Devuelve el resultado de la validaci�n ya sea aprobado o rechazado.
     * @throws IOException excepcion de entrada y salida.
     * @throws SeguridadException seguridad excepcion.
     * @throws NoSessionException excepcion de no sesion.
     * @since 1.0
     */
    public static ResultadoOperacionSegundaClaveTO validarSegundaClave(HttpServletRequest request, String idName,
        String servicio, RepresentacionSegundaClave segundaClave)throws IOException, SeguridadException,
        NoSessionException {

        String activoSegundaClave = TablaValores.getValor(TABLADEPARAMETROS, "active", "valor");
        int estatusNOK = Integer.parseInt(TablaValores.getValor(TABLADEPARAMETROS, "estatusnok", "valor"));
        int estatusOK = Integer.parseInt(TablaValores.getValor(TABLADEPARAMETROS, "estatusok", "valor"));
        int estatusBloqueado=Integer.parseInt(TablaValores.getValor(TABLADEPARAMETROS, 
            "estatusintentofallido", "valor"));
        String glosa = TablaValores.getValor(TABLADEPARAMETROS, "estatusnok", "glosa");
        ResultadoOperacionSegundaClaveTO resultadoValidacion = new ResultadoOperacionSegundaClaveTO();

        resultadoValidacion.setEstatus(false);
        resultadoValidacion.setIdCodigoEstado(estatusNOK);
        resultadoValidacion.setGlosa(glosa);
        
        
        try {           
            if (request == null) {
                throw new wcorp.serv.seguridad.NoSessionException("NOSESSION");
            }
            HttpSession hsess = request.getSession(false);
            if (hsess != null) {
                SessionBCI sesionPpal = (SessionBCI) hsess.getAttribute(idName);
                if (sesionPpal == null) {
                    if (logger.isDebugEnabled()) {
                        logger.debug("[validarSegundaClave] [sesionPpal] la session principal es nula");
                    }
                }
                else {
                    if (logger.isInfoEnabled()) {
                        logger.info("[validarSegundaClave] [" + sesionPpal.getAttrib("esMopaIII")
                            + "] Se obtiene de sesion principal esMopaIII");
                    }
                }
                if (sesionPpal != null) {
                    String rut= String.valueOf(sesionPpal.getCliente().getRut()+ sesionPpal.getCliente().getDigito());
                    boolean sesionPpalActiva = true;
                    try {
                        sesionPpal.checkAlive();
                    }
                    catch (Exception e) {
                        sesionPpalActiva = false;
                    }
                    if (!("true".equals(sesionPpal.getAttrib("esMopaIII")) && !sesionPpalActiva)) {
                        if (logger.isInfoEnabled()) {
                            logger.info("[validarSegundaClave] [" + rut + "]"
                                + "Entra a bloque if [001]");
                        }
                        if (sesionPpal.getCliente() != null && sesionPpal.getCanal() != null) {
                            if (logger.isInfoEnabled()) {
                            logger.info("[validarSegundaClave] [" + rut + "]"
                                + "Antes de ejecutar validarRetomaDeSesion: " + servicio);
                            }
                            validarRetomaDeSesion(servicio, hsess, sesionPpal, sesionPpal.getCliente(),
                                sesionPpal.getCanal());
                            if (logger.isInfoEnabled()) {
                            logger.info("[validarSegundaClave] [" + rut + "]"
                                + "Antes de ejecutar validarAccesoDeMicromatico: ")
                                ;
                            }
                            validarAccesoDeMicromatico(request, servicio, sesionPpal.getCanal());
                            if (logger.isInfoEnabled()) {
                            logger.info("[validarSegundaClave] [" + rut + "]"
                                + "Antes de ejecutar validarAccesoSuperUsuario: ");
                            }
                            validarAccesoSuperUsuario(request, idName, servicio, sesionPpal);
                            if (logger.isInfoEnabled()) {
                            logger.info("[validarSegundaClave] [" + rut + "]"
                                + "Antes de ejecutar validarAccesoTeleCanal: ");
                            }
                            validarAccesoTeleCanal(request, idName, servicio, sesionPpal);

                            if (CANAL_EMPRESA.equals(sesionPpal.getCanal())
                                || CANAL_BCI_EXPRESS.equals(sesionPpal.getCanal())) {
                                if (logger.isInfoEnabled()) {
                                logger.info("[validarSegundaClave] [" + rut + "]"
                                    + "Antes de ejecutar validarAccesoSesionMopa: ");
                                }
                                validarAccesoSesionMopa(request, idName, servicio);
                            }
                            ParametrosEstrategiaSegundaClaveTO parametrosEstrategiaSegundaClaveTO;
                            if (logger.isDebugEnabled()) {
                            logger.debug("[validarSegundaClave] [" + rut + "]"
                                + "Inicializacion de parametrosEstrategiaSegundaClaveTO "
                                + "(req, idName, servicio, cliente, canal, sessionBCI, segundaClave)");
                            }
                            parametrosEstrategiaSegundaClaveTO = new ParametrosEstrategiaSegundaClaveTO(
                                servicio, sesionPpal.getCliente(), sesionPpal.getCanal(), sesionPpal,
                                segundaClave);

                            String identificadorPrioridadSegundaClave = (String) sesionPpal
                                .getAttrib("identificadorPrioridadSegundaClave");
                            if (logger.isInfoEnabled()) {
                            logger.info("[validarSegundaClave] [" + rut + "]"
                                + " El identificadorPrioridadSegundaClave: " 
                                + identificadorPrioridadSegundaClave);
                            }
                            parametrosEstrategiaSegundaClaveTO
                                .setIdentificarPrioridadDeSegundaClave(identificadorPrioridadSegundaClave);

                            String identificadorEstadoSegundaClave = (String) sesionPpal
                                .getAttrib("identificadorEstadoSegundaClave");
                            if (logger.isInfoEnabled()) {
                            logger.info("[validarSegundaClave] [" + rut + "]"
                                + "El identificadorPrioridadSegundaClave: " + identificadorEstadoSegundaClave);
                            }

                            parametrosEstrategiaSegundaClaveTO
                                .setIdentificadorEstadoSegundaClave(identificadorEstadoSegundaClave);

                            EstrategiaSegundaClave estrategia = new EstrategiaSafeSigner();

                            if (identificadorPrioridadSegundaClave == null
                                || "".equals(identificadorPrioridadSegundaClave)) {
                                if (logger.isDebugEnabled()) {
                                    logger.debug("[validarSegundaClave] [" + rut
                                        + "]" + "El identificadorPrioridadSegundaClave es null o vacio");
                                }
                                if (!isTieneLlave(identificadorPrioridadSegundaClave)) {
                                    if (logger.isDebugEnabled()) {
                                        logger.debug("[validarSegundaClave] [" + rut
                                            + "]" + "No tiene llave previa");
                                    }
                                    return resultadoValidacion;
                                }
                            }
                            else {
                                if (logger.isInfoEnabled()) {
                                    logger.info("[validarSegundaClave] [" + rut + "]"
                                        + " El identificadorPrioridadSegundaClave:"
                                        + identificadorPrioridadSegundaClave);
                                    logger.info("[validarSegundaClave] [" + rut + "]"
                                        + " Antes de ejecutar consultarEstadoSegundaClave");
                                }
                                EstadoSegundaClaveTO estatus = estrategia
                                    .consultarEstadoSegundaClave(parametrosEstrategiaSegundaClaveTO);
                                if (estatus != null) {
                                    if (logger.isInfoEnabled()) {
                                        logger.info("[validarSegundaClave] [" + rut
                                            + "]" + " Consultar Estado Segunda Clave:" 
                                            + estatus);
                                        }
                                    if (estatus.getGlosa().equalsIgnoreCase(activoSegundaClave)) {
                                        if (logger.isDebugEnabled()) {
                                            logger.debug("[validarSegundaClave] ["
                                                + rut + "]"
                                                + "Consultar Estado Segunda "
                                                + "Clave es Activo");                                            
                                        }
                                        if (logger.isDebugEnabled()) {
                                            logger.debug("[validarSegundaClave] [" 
                                                + rut
                                                + "]" + "Setear el parametro de SessionBCI"
                                                + "IdentificadorEstadoSegundaClave");
                                        }
                                        parametrosEstrategiaSegundaClaveTO
                                            .setIdentificadorEstadoSegundaClave(estatus.getGlosa());
                                        if (logger.isDebugEnabled()) {
                                            logger.debug("[validarSegundaClave] [" 
                                                + rut
                                                + "]" + "Obtener el parametro de SessionBCI"
                                                + "identificadorEstadoSegundaClave");
                                        }
                                        sesionPpal.setAttrib("identificadorEstadoSegundaClave",
                                            parametrosEstrategiaSegundaClaveTO
                                                .getIdentificadorEstadoSegundaClave());
                                    }
                                    else {
                                        if (logger.isInfoEnabled()) {
                                            logger.info("[validarSegundaClave]"
                                                + "[" + rut
                                                + "] El estado devuelto es " + estatus.getGlosa()
                                                + "y se rompe el flujo");
                                        }
                                        return resultadoValidacion;
                                    }
                                }
                            }
                            if (parametrosEstrategiaSegundaClaveTO.getIdentificadorEstadoSegundaClave()
                                .equalsIgnoreCase(activoSegundaClave)) {
                                if (logger.isDebugEnabled()) {
                                    logger.debug("[validarSegundaClave] [" + rut
                                        + "]" + " El identificadorEstadoSegundaClave es Activo");
                                    logger.debug("[validarSegundaClave] [" + rut
                                        + "]" + "Antes de ejecutar a autenticar");
                                }
                                //Se rescata de la sesion y se setea en parametros
                                
                                parametrosEstrategiaSegundaClaveTO.setCamposDeLlave((CamposDeLlaveTO)hsess.getAttribute("camposDeLlave"));
                                resultadoValidacion = estrategia.autenticar(parametrosEstrategiaSegundaClaveTO);
                                
                                if(resultadoValidacion.getIdCodigoEstado()== estatusBloqueado)
                                {
                                    sesionPpal.removeAttrib("identificadorEstadoSegundaClave");
                                }                              
                               
                                if (logger.isInfoEnabled()) {
                                    logger.info("[validarSegundaClave] [" + rut + "]"
                                        + "Resultado de validac�n: " + resultadoValidacion);
                                }
                            }
                            else {
                                  sesionPpal.removeAttrib("identificadorEstadoSegundaClave");
                                                               
                                if (logger.isInfoEnabled()) {
                                    logger.info("[validarSegundaClave] [" + rut + "]"
                                        + " El estado devuelto es "
                                        + parametrosEstrategiaSegundaClaveTO.getIdentificadorEstadoSegundaClave());
                                }
                                return resultadoValidacion;
                            }
                            if (logger.isDebugEnabled()) {
                                logger.debug("[validarSegundaClave] ["
                                    + rut + "]" 
                                    + "Inicio de Journalizar");
                            }
                            Journal journal = new Journal();
                            
                            Eventos evento = new Eventos();
                            evento.setIdCanal(sesionPpal.getCanal().getNombre());
                            evento.setRutCliente(String.valueOf( sesionPpal.getCliente().getRut()));
                            evento.setDvCliente(String.valueOf( sesionPpal.getCliente().getDigito()));
                            evento.setIdMedio(request.getRemoteAddr());
                            String Producto = TablaValores.getValor(TABLADEPARAMETROS, "Journal", "Producto");
                            evento.setIdProducto(Producto);
                            String infoVariable = TablaValores.getValor(TABLADEPARAMETROS, "journal_" + servicio, "idproducto");
                            infoVariable += ";" + Producto;
                            evento.setCampoVariable(infoVariable);

                            if (logger.isDebugEnabled()) {
                                logger.debug("[validarSegundaClave] [" + rut + "]"
                                    + " Journalizar resultado de Segunda Clave");
                            }
                            if (resultadoValidacion != null) {
                                if (logger.isInfoEnabled()) {
                                    logger.info("[validarSegundaClave] [" + rut + "]"
                                        + "Journalizar resultado de validacion no es" + resultadoValidacion);
                                }
                                if (resultadoValidacion.getIdCodigoEstado() == estatusOK) {
                                    evento.setEstadoEventoNegocio( TablaValores.getValor(TABLADEPARAMETROS, "Journal", "TrxProcesada"));
                                    evento.setCodsubtipoEventoNegocio( TablaValores.getValor(TABLADEPARAMETROS, "Journal", "codSubtipoOk"));
                                    evento.setCodEventoNegocio( TablaValores.getValor(TABLADEPARAMETROS, "Journal", "CodEventoNegocio"));
                                }
                                else {
                                    evento.setEstadoEventoNegocio( TablaValores.getValor(TABLADEPARAMETROS, "Journal", "TrxProcesada"));
                                    evento.setCodsubtipoEventoNegocio( TablaValores.getValor(TABLADEPARAMETROS, "Journal", "codSubtipoNOk"));
                                    evento.setCodEventoNegocio( TablaValores.getValor(TABLADEPARAMETROS, "Journal", "CodEventoNegocio"));
                                }
                            }
                            else {
                                evento.setEstadoEventoNegocio( TablaValores.getValor(TABLADEPARAMETROS, "Journal", "estEventoError"));
                                evento.setCodsubtipoEventoNegocio( TablaValores.getValor(TABLADEPARAMETROS, "Journal", "codSubtipoError"));
                                evento.setCodEventoNegocio( TablaValores.getValor(TABLADEPARAMETROS, "Journal", "CodEventoNegocio"));
                            }
                            
                            journal.journalizar(evento);
                        }
                    }
                    else {
                        if (logger.isDebugEnabled()) {
                            logger.debug("[validarSegundaClave] [" + rut + "]"
                                + "Antes de ejecutar guardarEnSesionBCIIdServicio");
                        }
                        guardarEnSesionBCIIdServicio(request, idName, servicio, sesionPpal, hsess);
                    }
                }
                else {
                    throw new NoSessionException("NOSESSION");
                }
            }
            else {
                throw new NoSessionException("NOSESSION");
            }
            return resultadoValidacion;
        }
        catch (Exception ex) {
            logger
                .error("[validarSegundaClave] [Exception] Error de medodo,mensaje=<" + ex.getMessage() 
                    + ">", ex);
            return resultadoValidacion;
        }
    }
    
    /**
     * Metodo que me dice si el modo de validacion tiene generacion de llave previa.
     * <p>
     * Registro de versiones:<ul>
     * <li> 1.0 (05/03/2013, Ver�nica Cestari(Orand)) : Primera versi�n. Se incluye validacion de llave para
     * SafeSigner
     * </ul>
     * <p>
     * @param modoValidacion
     * Nombre de la llave
     * @return boolean si tiene llave previa
     * @throws NoSessionException
     * @since 1.0
     */
    public static boolean isTieneLlave(String modoValidacion) {
        try {
            if (modoValidacion == null) {
                if (logger.isDebugEnabled()) {
                    logger.debug("[isTieneLlave] [modoValidacion] El modo de Validacion es null");
                }
                return true;
            }
            if (logger.isInfoEnabled()) {
                logger.info("[isTieneLlave] [modoValidacion] El modo de validacion es"
                    + modoValidacion);
            }
            String valor = TablaValores.getValor("Seguridad.parametros", modoValidacion, "tieneLlave");
            if (valor == null) {
                if (logger.isDebugEnabled()) {
                    logger.debug("[isTieneLlave] [valor] No existe modo de validacion");
                }
                return false;
            }
            if (logger.isInfoEnabled()) {
                logger.info("[isTieneLlave] [" + valor 
                    + "] Tiene llave");
            }
            if (valor.equals("Si")) {
                return true;
            }
            return false;
        }
        catch (Exception ex) {
            logger
            .error("[isTieneLlave] [Exception] Error al tratar si tiene llave previa,mensaje=<" + ex.getMessage() 
                + ">", ex);
             return true;
        }
    }

    /**
     * Metodo para validar el acceso a micromatico.
     * <p>
     * Registro de versiones:<ul>
     * <li> 1.0 (11/03/2013, Ver�nica Cestari(Orand)) : Primera versi�n. Se incluye validacion de llave para
     * Segunda Clave
     * </ul>
     * <p>
     * @param anHttpServletRequest
     * request de la sesion
     * @param anServicio
     * nombre del servicio
     * @param canalSesionBCI
     * canal de session BCI
     * @throws SeguridadException
     * seguridad excepcion
     * @since 1.0
     */
    private static void validarAccesoDeMicromatico(HttpServletRequest anHttpServletRequest, String anServicio,
        Canal canalSesionBCI) throws SeguridadException {
        logger.debug("[validarAccesoDeMicromatico] [inicio] inicio de metodo");
        String canalSesionBCIStr = canalSesionBCI.getCanalID();

        if ((canalSesionBCIStr != null)
            && (canalSesionBCIStr.equals("600") || canalSesionBCIStr.equals("601") || canalSesionBCIStr
                .equals("615"))) {
            String ipClient = anHttpServletRequest.getRemoteAddr();
            String regMicro = TablaValores.getValor("micromaticos.parametros", ipClient, anServicio);
            if (logger.isInfoEnabled()) {
                logger.info("[validarAccesoDeMicromatico] [" + ipClient 
                    + "] Ip de Cliente [" + regMicro + "] reg de Micro");
            }
            if (regMicro != null && regMicro.equalsIgnoreCase("SI")) {
                if (logger.isDebugEnabled()) {
                    logger.debug("[validarAccesoDeMicromatico][" + regMicro
                        +"] Quiebre de flujo micromatico");
                }
                return;
            }
            else {
                if (logger.isDebugEnabled()) {
                    logger.debug("[validarAccesoDeMicromatico] [regMicro] Lanza excepcion micromatico");
                }
                throw new SeguridadException("MICROMATICO");
            }
        }
        logger.debug("[validarAccesoDeMicromatico] [" + canalSesionBCIStr 
            + "canal SesionBCI string");
    }

    /**
     * Metodo para validar el acceso a sesion Mopa.
     * <p>
     * Registro de versiones:<ul>
     * <li> 1.0 (11/03/2013, Ver�nica Cestari(Orand)) : Primera versi�n. Se incluye validacion de llave para
     * Segunda Clave
     * </ul>
     * <p>
     * @param anHttpServletRequest
     * request de la sesion
     * @param anIdName
     * id name
     * @param anServicio
     * nombre del servicio
     * @throws SeguridadException
     * seguridad excepcion
     * @throws IOException
     * IO excepcion
     * @throws NoSessionException
     * No session excepcion
     * @since 1.0
     */
    private static void validarAccesoSesionMopa(HttpServletRequest anHttpServletRequest, String anIdName,
        String anServicio) throws IOException, SeguridadException, NoSessionException {
        try {
            if (ControllerMOPAIII.validarAcceso(anHttpServletRequest, anIdName, anServicio))
                return;
        }
        catch (SeguridadException e) {
            logger.error("[validarAccesoSesionMopa] [" + anIdName + "] ControllerMOPAIII anIdName [" 
                + anServicio + "] anServicio [SeguridadException] excepcion de seguridad", e);
            throw e;
        }
        catch (NoSessionException e) {
            logger.error("[validarAccesoSesionMopa] [" + anIdName + "] ControllerMOPAIII anIdName [" 
                + anServicio + "] anServicio [NoSessionException] excepcion de session", e);
            throw e;
        }

    }

    /**
     * Metodo para validar el acceso a super Usuario.
     * <p>
     * Registro de versiones:<ul>
     * <li> 1.0 (11/03/2013, Ver�nica Cestari(Orand)) : Primera versi�n. Se incluye validacion de llave para
     * Segunda Clave
     * </ul>
     * <p>
     * @param anHttpServletRequest
     * request de la sesion
     * @param anIdName
     * id name
     * @param anServicio
     * nombre del servicio
     * @param sesionPpal
     * sesion principal bci
     * @throws SeguridadException
     * seguridad excepcion
     * @since 1.0
     */
    private static void validarAccesoSuperUsuario(HttpServletRequest anHttpServletRequest, String anIdName,
        String anServicio, SessionBCI sesionPpal) throws SeguridadException {
        String rutUsuarioStr = sesionPpal.getRutUsuario();
        if (logger.isDebugEnabled()) {
            logger.debug("[validarAccesoSuperUsuario]  [" + rutUsuarioStr + "] Rut supUser autenticado");
        }
        String supUserCode = (String) sesionPpal.getAttrib("supUser");
        if (checkPassSupUser(anHttpServletRequest, anIdName, anServicio)) {
            if (supUserCode == null) {
                String dis = anHttpServletRequest.getRequestURI();
                sesionPpal.setAttrib("dispatcherSupUser", dis);
                sesionPpal.setAttrib("tituloSupUser", tituloPassSupUser);
                sesionPpal.setAttrib("servicioSupUser", anServicio);
                throw new SeguridadException("SUPUSER");
            }
            else {
                if (logger.isDebugEnabled()) {
                    logger.debug("[validarAccesoSuperUsuario] [" + rutUsuarioStr + "] Rut supUser autenticado");
                }
                sesionPpal.removeAttrib("supUser");
            }
        }
    }

    /**
     * Metodo para validar el acceso a Tele Canal.
     * <p>
     * Registro de versiones:<ul>
     * <li> 1.0 (11/03/2013, Ver�nica Cestari(Orand)) : Primera versi�n. Se incluye validacion de llave para
     * Segunda Clave
     * </ul>
     * <p>
     * @param anHttpServletRequest
     * request de la sesion
     * @param anIdName
     * id name
     * @param anServicio
     * nombre del servicio
     * @param sesionPpal
     * sesion principal bci
     * @throws SeguridadException
     * seguridad excepcion
     * @since 1.0
     */
    private static void validarAccesoTeleCanal(HttpServletRequest anHttpServletRequest, String anIdName,
        String anServicio, SessionBCI sesionPpal) throws SeguridadException {
        if (tieneRestriccionesTelecanal(anIdName, anHttpServletRequest, sesionPpal)) {
            logger.error("[validarAcceso] [" + anIdName + "] anIdName [" 
                + anServicio + "] anServicio [Excepcion] de restriccion telecanal");               
            throw new SeguridadException("AUTORIZA");
        }
    }

    /**
     * Metodo para validar la retoma de sesion.
     * <p>
     * Registro de versiones:<ul>
     * <li> 1.0 (11/03/2013, Ver�nica Cestari(Orand)) : Primera versi�n. Se incluye validacion de llave para
     * Segunda Clave
     * </ul>
     * <p>
     * @param anServicio
     * nombre del servicio
     * @param hsess
     * session
     * @param sesionPpal
     * sesion principal bci
     * @param clienteEnSesionBCI
     * cliente en sesion
     * @param canalEnSesionBCI
     * canal en sesion
     * @throws SeguridadException
     * seguridad excepcion
     * @throws IOException
     * IO excepcion
     * @throws NoSessionException
     * No session excepcion
     * @since 1.0
     */
    private static void validarRetomaDeSesion(String anServicio, HttpSession hsess, SessionBCI sesionPpal,
        Cliente clienteEnSesionBCI, Canal canalEnSesionBCI) throws IOException, SeguridadException,
        NoSessionException {
        String retomo = TablaValores
            .getValor(archivoConfiguracionRetoma, canalEnSesionBCI.getCanalID(), "retomar");
        if (retomo == null) {
            retomo = "";
        }
        if (logger.isDebugEnabled()) {
            logger.debug("[validarRetomaDeSesion] [" + retomo + "] retomo");
        }
        String rutClienteSesionBCIStr = String.valueOf(clienteEnSesionBCI.getRut());
        String canalSesionBCIStr = canalEnSesionBCI.getCanalID();
        if (!(retomo.equals("NO"))) {
            try {
                sesionPpal.checkAlive();
            }
            catch (SeguridadException se) {
                logger.error("[validarRetomaDeSesion] [" + rutClienteSesionBCIStr + "] retomo rut, ["
                    + canalSesionBCIStr + "] canal [SeguridadException] "
                    + "error de seguridad, mensaje=<" + se.getMessage() +">", se);
                asignaServicioRetoma(hsess, canalSesionBCIStr, anServicio, rutClienteSesionBCIStr);
                sesionPpal.cerrar();
                EliminarSession.eliminar((String) sesionPpal.getAttrib("sessionId"));
                throw se;
            }
        }
    }

    /**
     * Metodo para guardar en sesion bci el id del servicio.
     * <p>
     * Registro de versiones:<ul>
     * <li> 1.0 (11/03/2013, Ver�nica Cestari(Orand)) : Primera versi�n. Se incluye validacion de llave para
     * Segunda Clave
     * </ul>
     * <p>
     * @param anHttpServletRequest
     * servlet request
     * @param anIdName
     * id name
     * @param anServicio
     * nombre del servicio
     * @param aSesionBCI
     * session
     * @param httpSession
     * sesion
     * @throws SeguridadException
     * seguridad excepcion
     * @throws IOException
     * IO excepcion
     * @throws NoSessionException
     * No session excepcion
     * @since 1.0
     */
    private static void guardarEnSesionBCIIdServicio(HttpServletRequest anHttpServletRequest, String anIdName,
        String anServicio, SessionBCI aSesionBCI, HttpSession httpSession) throws SeguridadException,
        NoSessionException, IOException {
        try {
            aSesionBCI.checkAlive();
        }
        catch (SeguridadException se) {
            logger.error("[guardarEnSesionBCIIdServicio] sesionPpal anIdName='" + anIdName
                + "' error en validacion de sesion", se);
            aSesionBCI.cerrar();
            EliminarSession.eliminar((String) aSesionBCI.getAttrib("sessionId"));
            throw se;
        }
        String objeto = (String) anHttpServletRequest.getParameter("objeto");
        String checkServicioObjetoTdc = null;
        if (logger.isDebugEnabled()) {
            logger.debug("[guardarEnSesionBCIIdServicio] objeto='" + objeto + "' checkServicioObjetoTdc='"
                + checkServicioObjetoTdc + "' servicio='" + anServicio + "'");
        }
        if (objeto != null) {
            checkServicioObjetoTdc = TablaValores.getValor("Seguridad.parametros", anServicio,
                "checkServicioObjeto");
            if (logger.isDebugEnabled()) {
                logger.debug("[guardarEnSesionBCIIdServicio] checkServicioObjetoTdc='" + checkServicioObjetoTdc
                    + "'");
            }
        }
        if (checkServicioObjetoTdc == null) {
            if (objeto != null) {
                aSesionBCI.checkServicioObjeto(anServicio, objeto);
                aSesionBCI.setAttrib("objeto", objeto);
            }
            else {
                aSesionBCI.removeAttrib("objeto");
                try {
                    aSesionBCI.checkServicio(anServicio);
                }
                catch (SeguridadException e) {
                    logger
                        .error(
                            "[guardarEnSesionBCIIdServicio] Error en check de servicio checkServicioObjetoTdc='"
                                + checkServicioObjetoTdc + " objetoc='" + objeto + "' anServicio='" + anServicio
                                + "'", e);
                    throw e;
                }
            }
        }
        else {
            if (logger.isDebugEnabled()) {
                logger.debug("[guardarEnSesionBCIIdServicio] No realizamos checkServicioObjeto='" + objeto + "' ");
            }
            aSesionBCI.setAttrib("objeto", objeto);
        }
        httpSession.setAttribute(anIdName, aSesionBCI);
    }
    
    /**
     * Verifica autenticacion.
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 02/08/2017, Marcelo Fuentes (SEnTRA) - Ricardo Carrasco (Ing. Soft. BCI): : versi�n inicial.</li>
     * </ul>
     * 
     * @param tipoSegundaClave con el tipo de la segunda clave a autenticar.
     * @param modoAutenticacion modo de autenticacion: ONLINE u OFFLINE.
     * @param usuarioSegundaClave con los datos del usuario a validar.
     * @param campoDeLlaves con los datos de la operacion a realizar.
     * @param servicio con el nombre del servicio de la operacion.
     * @param codeConfirmation la clave a validar.
     * @return boolean que indica si esta autenticado correctamente.
     * @throws SeguridadException en caso de error.
     * @since 1.19
     */
    public static boolean verificarAutenticacion(String tipoSegundaClave, String modoAutenticacion, UsuarioSegundaClaveTO usuarioSegundaClave, 
            CamposDeLlaveTO campoDeLlaves, String servicio, String codeConfirmation) throws SeguridadException {
        
        RepresentacionAlfanumericaSegundaClaveTO representacionAlfanumericaSegundaClave = new RepresentacionAlfanumericaSegundaClaveTO(codeConfirmation);
        
        ParametrosEstrategiaSegundaClaveTO parametrosEstrategiaSegundaClaveTO = new ParametrosEstrategiaSegundaClaveTO(usuarioSegundaClave);
        parametrosEstrategiaSegundaClaveTO.setCanal(usuarioSegundaClave.getCanalId());
        parametrosEstrategiaSegundaClaveTO.setSegundaClave(representacionAlfanumericaSegundaClave);
        parametrosEstrategiaSegundaClaveTO.setTipoSegundaClave(modoAutenticacion);
        
        String strRequiereLlave = TablaValores.getValor(TABLA_SEGURIDAD, "SegundaClaveRequiereLlave", tipoSegundaClave);
        boolean requiereLlave = (strRequiereLlave != null && strRequiereLlave.equalsIgnoreCase("true")) ? true : false;
        
        if (requiereLlave) {
            parametrosEstrategiaSegundaClaveTO.setCamposDeLlave(campoDeLlaves);
            parametrosEstrategiaSegundaClaveTO.setServicio(servicio);
        }
        
        try {
            PrioridadSegundaClaveIdTO prio = PrioridadSegundaClaveIdTO.getPorNombre(tipoSegundaClave);
            EstrategiaSegundaClave estrategiaSegundaClave = EstrategiaAutenticacionFactory.obtenerEstrategiaSegundaClave(prio);
            
            ResultadoOperacionSegundaClaveTO resultado = estrategiaSegundaClave.autenticar(parametrosEstrategiaSegundaClaveTO);
            
            if (resultado.getIdCodigoEstado() == 0) {
                return true;
            }
            if (logger.isDebugEnabled()) {
                logger.debug("[verificarAutenticacion] Validacion erronea " + resultado.getGlosa());
            }
            throw new SeguridadException(resultado.getGlosa());
            
        }
        catch (SeguridadException se) {
            if (logger.isDebugEnabled()) {
                logger.debug("[verificarAutenticacion] Validacion Codigo [" + se.getCodigo() + "]");
                logger.debug("[verificarAutenticacion] Validacion erronea " + se.getInfoAdic());
            }
            
            throw se;
        }
    }
    
    /**
     * Obtiene el estado de la segunda clave.
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 02/08/2017, Marcelo Fuentes (SEnTRA) - Ricardo Carrasco (Ing. Soft. BCI): : versi�n inicial.</li>
     * </ul>
     * 
     * @param paramSegundaClave con los datos para obtener estado de la segunda clave.
     * @return objeto con el resultado de la segunda clave a solicitar al cliente.
     * @throws SeguridadException en caso de error.
     * @since 1.19
     */
    public static EstadoSegundaClaveTO estadoSegundaClave(ParametrosEstrategiaSegundaClaveTO paramSegundaClave)
          throws SeguridadException {
        if (logger.isInfoEnabled()) {
            logger.info("[estadoSegundaClave] [" + paramSegundaClave + "]");
        }
        String servicio = paramSegundaClave.getServicio();
        UsuarioSegundaClaveTO usuarioSegundaClave = paramSegundaClave.getUsuarioSegundaClave();
        
        if( usuarioSegundaClave == null) {
            String usuarioId = "";
            SessionBCI session = paramSegundaClave.getSessionBCI();
            if (session.getCanal().getCanalID().equals(CANAL_PYME)) {
                usuarioId = session.getRutUsuario() + String.valueOf(session.getDvUsuario().charAt(0));
            }
            else {
                usuarioId = String.valueOf(session.getCliente().getRut()) + String.valueOf(session.getCliente().getDigito());
            }
            usuarioSegundaClave = new UsuarioSegundaClaveTO(usuarioId, obtieneDominio(session.getCanal().getCanalID()), session.getCanal().getCanalID());
            paramSegundaClave.setUsuarioSegundaClave(usuarioSegundaClave);
        }
        
        if (logger.isInfoEnabled()) {
            logger.info("[estadoSegundaClave] [" + usuarioSegundaClave.getUsuarioId() + "]");
        }
        
        Vector segundasClavesParaServicio = consultaJerarquia(paramSegundaClave.getSessionBCI(), servicio);
        
        if (segundasClavesParaServicio == null || segundasClavesParaServicio.size() == 0) {
            if (logger.isInfoEnabled()) {
                logger.info("[estadoSegundaClave] [" + servicio + "] servicio no existe en la tabla de parametros");
            }
            throw new SeguridadException("SAFESIGNER05");
        }
        
        for (int j = 0; j < segundasClavesParaServicio.size(); j++) {
            String strSegClave = String.valueOf(segundasClavesParaServicio.get(j));
            if (logger.isInfoEnabled()) {
                logger.info("[estadoSegundaClave] [" + usuarioSegundaClave.getUsuarioId() + "], servicio = " + servicio 
                        + " segundasClavesParaServicio = " + strSegClave);
            }
            
            try {
                logger.debug("[estadoSegundaClave]:antes de new ParametrosEstrategiaSegundaClaveTO");
                
                PrioridadSegundaClaveIdTO prio = PrioridadSegundaClaveIdTO.getPorNombre(strSegClave);
                EstrategiaSegundaClave estrategiaSegundaClave = EstrategiaAutenticacionFactory.obtenerEstrategiaSegundaClave(prio);
                
                EstadoSegundaClaveTO estadoSegundaClave = estrategiaSegundaClave.consultarEstadoSegundaClave(paramSegundaClave);
                
                if (estadoSegundaClave.isEstatus()) {
                    if (logger.isInfoEnabled()) {
                          logger.info("[estadoSegundaClave] [" + usuarioSegundaClave.getUsuarioId()
                                  + "] Estatus OK ::::::" + estadoSegundaClave.toString());
                    }
                    return estadoSegundaClave;
                }
                else {
                    if (logger.isInfoEnabled()) {
                          logger.info("[estadoSegundaClave] [" + usuarioSegundaClave.getUsuarioId()
                                  + "] Estatus NOK, pasando a siguiente prioridad ");
                    }
                }
            }
            catch (Exception e) {
                if (logger.isEnabledFor(Level.ERROR)) {
                    logger.error("[estadoSegundaClave] [" + e.getMessage());
                }
            }
        }
          
        if (logger.isEnabledFor(Level.ERROR)) {
            logger.error("[estadoSegundaClave] [" + usuarioSegundaClave.getUsuarioId()
                    + "] No hay segunda clave valida para servicio: " + servicio);
        }
        
        EstadoSegundaClaveTO estado = new EstadoSegundaClaveTO();
        
        estado.setIdCodigoEstado(IDNOOK);
        estado.setEstatus(false);
        return estado;
    }
    
    /**
     * Metodo que genera la llave de autenticacion de transaccion.
     * <p>
     * Registro de Versiones:
     * <ul>
     * <li>1.0 02/08/2017, Marcelo Fuentes (SEnTRA) - Ricardo Carrasco (Ing. Soft. BCI): : versi�n inicial.</li>
     * </ul>
     * </p>
     * @param tipoSegundaClave tipo de segunda clave.
     * @param servicio servicio.
     * @param usuarioSegundaClave usuario de segunda clave.
     * @param campos los campos de la operacion a validar.
     * @param modoTransaccion ONLINE/OFFLINE si vienen como Entrust SoftToken.
     * @return LlaveSegundaClaveTO llave de segunda clave.
     * @throws SeguridadException excepcion de seguridad.
     * @since 1.19
     */
    public static LlaveSegundaClaveTO generarLlave(String tipoSegundaClave, String servicio,
            UsuarioSegundaClaveTO usuarioSegundaClave, CamposDeLlaveTO campos, String modoTransaccion) throws SeguridadException {
        if (logger.isEnabledFor(Level.INFO)) {
            logger.info("[generarLlave] [BCI_INI] [" + usuarioSegundaClave.getUsuarioId() + "] tipoSegundaClave: " + tipoSegundaClave
                    + " servicio: " + servicio + " usuarioSegundaClave: " + usuarioSegundaClave.toString() + " camposDeLlave "
                    + campos.toString() + " modoTransaccion " + modoTransaccion);
        }
        
        EstrategiaSegundaClave estrategiaSegundaClave = EstrategiaAutenticacionFactory
                .obtenerEstrategiaSegundaClave(PrioridadSegundaClaveIdTO.getPorNombre(tipoSegundaClave));
        
        ParametrosEstrategiaSegundaClaveTO parametrosEstrategiaSegundaClaveTO = new ParametrosEstrategiaSegundaClaveTO(servicio, usuarioSegundaClave, campos);
        logger.debug("[generarLlave] [" + usuarioSegundaClave.getUsuarioId() + "] Antes de generar llave");
        
        if ((ENTRUST_SOFTTOKEN.equalsIgnoreCase(tipoSegundaClave)) && (modoTransaccion != null) 
                && ((modoTransaccion.equalsIgnoreCase(ONLINE) || modoTransaccion.equalsIgnoreCase(OFFLINE)))){
            if (logger.isEnabledFor(Level.INFO)){
                logger.debug("[generarLlave] seteo el modo Transaccion " + modoTransaccion + " para " + tipoSegundaClave);
            }
            parametrosEstrategiaSegundaClaveTO.setTipoSegundaClave(modoTransaccion);
            
        }
        
        LlaveSegundaClaveTO llaveSegundaClaveTO = estrategiaSegundaClave.generarLlave(parametrosEstrategiaSegundaClaveTO);
        if (logger.isEnabledFor(Level.INFO)){
            logger.info("[generarLlave] [BCI_FINOK] " + llaveSegundaClaveTO);
        }
        return llaveSegundaClaveTO;
    }
    
    /**
     * <p>
     * M�todo que obtiene la frecuencia para consultar si cliente valido transaccion via Entrust SoftToken.
     * </p>
     * 
     * Registro de versiones:
     * <ul>
     * <li>1.0 02/08/2017, Marcelo Fuentes (SEnTRA) - Ricardo Carrasco (Ing. Soft. BCI): Versi�n Inicial.</li>
     * </ul>
     * <p>
     * 
     * @return La frecuencia para consultar.
     * @since 1.9
     */
    public static String getFrecuenciaEntrust() {
        String frecuenciaEntrust = "";
        String frecuencia = TablaValores.getValor("entrust.parametros", "frecuenciaConsultaTransaccion", "desc");
        if (frecuencia != null){
            frecuenciaEntrust = frecuencia;
        } else {
            if (getLogger().isEnabledFor(Level.ERROR)){
                getLogger().error("[getTimeOutEntrust]Error No hay valor definido para frecuencia");
            }
            throw new IllegalArgumentException("Valor nulo para frecuencia");
        }
        return frecuenciaEntrust;
        
    }
    
    /**
     * <p>
     * M�todo que obtiene el timeout para consultar si cliente valido transaccion via Entrust SoftToken.
     * </p>
     * 
     * Registro de versiones:
     * <ul>
     * <li>1.0 02/08/2017, Marcelo Fuentes (SEnTRA) - Ricardo Carrasco (Ing. Soft. BCI): Versi�n Inicial.</li>
     * </ul>
     * <p>
     * 
     * @return El timeout para consultar.
     * @since 1.9
     */
    public static String getTimeOutEntrust() {
        String timeOutEntrust = "";
        String timeOut = TablaValores.getValor("entrust.parametros", "timeOutConsultaTransaccion", "desc");
        if (timeOut != null){
            timeOutEntrust = timeOut;
        } else {
            if (getLogger().isEnabledFor(Level.ERROR)){
                getLogger().error("[getTimeOutEntrust]Error No hay valor definido para timeout");
            }
            throw new IllegalArgumentException("Valor nulo para timeout");
        }
        return timeOutEntrust;
    }
    
    /**
     * <p>
     * M�todo que obtiene el dominio de un canal.
     * </p>
     * 
     * Registro de versiones:
     * <ul>
     * <li>1.0 02/08/2017,  Marcelo Fuentes (SEnTRA) - Ricardo Carrasco (Ing. Soft. BCI): Versi�n Inicial.</li>
     * </ul>
     * <p>
     * 
     * @param canal canal del Cliente.
     * @return String con el dominio del canal.
     * @since 1.19
     */
    public static String obtieneDominio(String canal) {
        String dominio = "";
        Hashtable canalesDominio = TablaValores.getTabla("dominios.parametros");
        Iterator it = canalesDominio.entrySet().iterator();
        String strCanales = "";
        Map.Entry e = null;
        Map valores = null;
        while (it.hasNext()) {
            e = (Map.Entry)it.next();
            valores = (Map) e.getValue();
            strCanales = (String)valores.get("CANALES");
            if (strCanales.indexOf(canal) >= 0){
                dominio = (String)e.getKey();
                break;
            }
        }
        return dominio;
    }
    
    /**
     * Metodo encargado de obtener el logger().
     * <p>
     *
     * Registro de versiones:<ul>
     * <li> 1.0 02/08/2017,  Marcelo Fuentes (SEnTRA) - Ricardo Carrasco (Ing. Soft. BCI): Versi�n Inicial.</li>
     * </ul>
     * <p>
     * @return el logger.
     * 
     * @since 1.19
     */
    public static Logger getLogger(){
        if (logger == null) {
            logger = Logger.getLogger(ControllerBCI.class);
        }
        return logger;
    }
    
}
