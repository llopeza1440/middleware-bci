package wcorp.recargacelulares.ui.actions;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.DynaActionForm;

import wcorp.bprocess.recargacelulares.RecargaCelularDelegate;
import wcorp.env.WCorpConfig;
import wcorp.infraestructura.seguridad.autenticacion.util.SafeSignerUtil;
import wcorp.model.seguridad.CamposDeLlaveTO;
import wcorp.model.seguridad.ControllerBCI;
import wcorp.model.seguridad.EstadoSegundaClaveTO;
import wcorp.model.seguridad.LlaveSegundaClaveTO;
import wcorp.model.seguridad.ParametrosEstrategiaSegundaClaveTO;
import wcorp.model.seguridad.SessionBCI;
import wcorp.model.seguridad.UsuarioSegundaClaveTO;
import wcorp.recargacelulares.IngresoTxVO;
import wcorp.recargacelulares.ui.helpers.RecargaCelularesHelper;
import wcorp.serv.seguridad.SeguridadException;
import wcorp.util.FechasUtil;
import wcorp.util.GeneralException;
import wcorp.util.StringUtil;
import wcorp.util.TablaValores;
import wcorp.util.recargacelulares.RecargaCelularesUtil;

/**
 * Action struts responsable de Caputurar los datos ingresados por el Cliente, y
 * Entregarlos a una pagina de Confirmacion para que el Cliente pueda Confirmar
 * su Transaccion. Datos: Celular a Cargar, Operadora, monto a cargar, numero de
 * cuenta
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 09/05/2006 Enrique Villar ( Neoris Chile ):Versi�n inicial
 * 
 * <li>1.1 03/10/2006 Gonzalo Silva (Neoris Chile): Se agreg� lo siguiente :
 * Logs de parametros enviados desde las paginas Jsp Logs de objetos recuperados
 * dentro del m�todo execute Se eliminaron las comparaciones ternarias
 * 
 * <li>1.2 11/12/2006 Kendru Estrada ( Neoris Chile ): Se agreg� la validaci�n del monto
 * m�ximo de recarga, dependiendo del tipo de clave del cliente. Adem�s, se
 * agrega el m�todo setMontosMaximosAutorizados. que permite obtener el monto
 * correspondiente para la validaci�n.
 * 
 * <li>1.3 26/02/2007 Pedro Carmona E. (SEnTRA): Se hace correccci�n a la
 * validacion del monto maximo autorizado, contrast�ndolo con el monto acumulado
 * diario de recargas hechas por el cliente.
 * 
 * <li>1.4 16/04/2007 Pedro Carmona E. (SEnTRA): Se elimina el m�todo
 * setMontosMaximosAutorizados(), en su reemplazo se invoca el mismo m�todo pero
 * de la clase RecargaCelularesHelper.
 * 
 * 
 * <li>1.5 22/05/2007 Pablo C�ceres F. (Ada Ltda) : Se agrega metodo
 * saveToken(); para evitar sobre submit en el Action que realiza la recarga del
 * celular.
 * 
 * <li>1.6 11/09/2007 Jorge Ib��ez E (BCI) : Se modifica llamada a m�todo de validaci�n
 * montos m�ximos que consultaba a tabla de moviminentos local de la aplicaci�n (reccel)
 * para que apunte a la tabla de movimiento de base de datos pagos por integraci�n a
 * producto PagueDirecto
 * 
 * <li>1.7 21/04/2007 Andr�s Mor�n O. (SEnTRA): Se agrega el c�lculo del monto adicional en el
 * caso de que exista una promoci�n.
 * 
 * <li>1.8 26/05/2008 Andr�s Mor�n O. (SEnTRA): Se agrega la busqueda de la vigencia de la recarga.
 * <li>
 * 1.9 (26/07/2011, Rodrigo Gonz�lez Merino (Imagemaker IT)): Se modifica la obtenci�n desde el form del
 * par�metro asociado al n�mero de celular, de obtener los par�metros 'celular11' y
 * 'celular12' a obtener el �nico par�metro 'numeroCelular'.
 * Se cambia la referencia de la clase de Log de {@link wcorp.util.LogFile} a {@link org.apache.log4j.Logger}, y se
 * mejoran los mensajes de logs.
 * Se optimizan los imports y se corrige el checkstyke en cuanto a los caracteres por l�nea.
 * </li>
 * <li>1.10 30/01/2013, Darlyn Delgado (SEnTRA): Se estandarizan logs
 * 
 * <li>1.11 25/02/2013 Victor Enero (Orand S.A): Se agrega integraci�n con SafeSigner.
 * <li>1.12 22/09/2016 Jaime Collao (ImageMaker)- Eduardo Espinoza (Ing. de Software Bci): Se comprueba que la variable de 
 * sesi�n venga marcada del paso uno, y se marca el paso dos.
 * <li>1.13 06/02/2017 Luis L�pez Alamos (SEnTRA) - Christian Moraga Rold�n (Ing. Soft. BCI): Se modifica {@link #execute(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)}</li>
 * <li>1.14 30/08/2017 Marcelo Fuentes (SEnTRA) - Ricardo Carrasco (Ing. Soft. BCI): Se modifica m�todo 
 * {@link #execute(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)}. Ademas se agrega metodo
 * {@link #getLogger()} y las constantes {@link #TABLA_PARAMETROS_ENTRUST}, {@link #ENTRUST_TOKEN} y {@link #ENTRUST_SOFTTOKEN}.</li>
 * </ul>
 * <p>
 * 
 * <B>Todos los derechos reservados por Banco de Cr�dito e Inversiones.</B>
 * <P>
 */

public class ConfirmaRecargaAction extends Action {
    /**
     * Tabla de parametros de Recargas.
     */
    private static final String PARAMSAFESIGNER = "safesigner.parametros";
    
    private static final String paramFile = "recargaCelular.parametros";
    
    /**
     * tabla de par�metros de entrust.
     */
    private static final String TABLA_PARAMETROS_ENTRUST = "entrust.parametros";
    
    /**
     * Tipo de Segunda Clave Softtoken Entrust.
     */
    private static final String ENTRUST_TOKEN = TablaValores.getValor(TABLA_PARAMETROS_ENTRUST, "nombreToken", "desc");
    
    /**
     * Tipo de Segunda Clave Softtoken Entrust.
     */
    private static final String ENTRUST_SOFTTOKEN = TablaValores.getValor(TABLA_PARAMETROS_ENTRUST, "nombreSoftToken", "desc");
    
    /**
     * Atributo usado para el despliegue de mensajes logs.
     */
    private transient Logger logger = Logger.getLogger(this.getClass());

    private String pagina = null;

    private RecargaCelularDelegate delegate = new RecargaCelularDelegate();

    private RecargaCelularesHelper helper = new RecargaCelularesHelper();

    private final SimpleDateFormat formatoFechaContable = new SimpleDateFormat("dd/MM/yyyy");

    private RecargaCelularesUtil reccelUtil = new RecargaCelularesUtil();

    /**
     * Session Bci.
     */
    private String idName = "sessionBci";

    /**
     * Recarga Celular.
     */
    private String servicio = "RecargaCelular";

    /**
     * Data segunda clave.
     */
    private String data;
    
    /**
     * <b>Confirma Recarga</b><br>
     * 
     * <br>
     * Registro de Versiones:<ul>
     * <li>1.0 xx/xx/xxxx, Desconocido. (Desconocido): versi�n inicial.</li>
     * <li>1.1 21/09/2016 Jaime Collao (ImageMaker)- Eduardo Espinoza (Ing. de Software Bci): Se comprueba que la 
     * variable de sesi�n venga marcada del paso uno, y se marca el paso dos. </li>
     * <li>1.2 01/02/2017 Luis L�pez Alamos (SEnTRA) - Christian Moraga Rold�n (Ing. Soft. BCI): Se agrega atributo tipoOperadora en el caso que exista.</li>
     * <li>1.3 30/08/2017  Marcelo Fuentes (SEnTRA) - Ricardo Carrasco (Ing. Soft. BCI): Se agrega logica que revisa 
     * si se requiere autenticaci�n Entrust Token o Entrust SoftToken. Ademas se normaliza log.<li>
     * </ul>
     * <br>
     * 
     * @param mapping Mapeo de forwards para la acci�n.
     * @param form    ActionForm con las variables de acci�n si corresponde.
     * @param request Petici�n HTTP que se est� procesando.
     * @param response Respuesta HTTP que se esta generando.
     * @return El forward correspondiente al formulario por defecto.
     * @throws Exception En caso de producirse un error.
     * @since 1.0
     * <P>
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
        HttpServletResponse response) throws Exception {
        // Obtiene la sesi�n Http
        HttpSession sesionHttp = request.getSession(false);
        // Obtiene la sesi�n Bci para recuperar datos del cliente
        SessionBCI sesionBci = (SessionBCI) sesionHttp.getAttribute(WCorpConfig.SESION_BCI);
        // Verifica que se curs� el paso uno.

        try {
        	
            if(sesionBci.getAttrib("primerPaso") == null || ((Boolean) sesionBci.getAttrib("primerPaso")).booleanValue() != true  ) {
            	pagina = "fracaso";
            	return mapping.findForward(pagina);
            } 
            else {
            	sesionBci.removeAttrib("primerPaso");
            	sesionBci.setAttrib("SegundoPaso", new Boolean(true));
            }
        	
            DynaActionForm recargaCelularesForm = (DynaActionForm) form;
            String rutCliente = String.valueOf(sesionBci.getCliente().getRut()) + "-"
                + String.valueOf(sesionBci.getCliente().getDigito());
            if (getLogger().isInfoEnabled()) {
                getLogger().info("[execute][BCI_INI][" + rutCliente + "]");
            }
            sesionHttp.removeAttribute("montoAdicional");
            sesionHttp.removeAttribute("promociones");
            sesionHttp.removeAttribute("vigenciaRecarga");
            sesionHttp.removeAttribute("existeAlias");

            String celular = (String) recargaCelularesForm.get("numeroCelular");
            recargaCelularesForm.set("rut_cliente", rutCliente);
            request.setAttribute("rut_cliente", rutCliente);
            
            request.setAttribute("celular", celular);

            String montoCarga = (String) recargaCelularesForm.get("monto_carga");
            // obtener adicional
            String canal = sesionBci.getCanal().getCanalID();
            String nombreOperador = (String) recargaCelularesForm.get("nombre_operador");
            String aliasRecarga = (String) recargaCelularesForm.get("alias_recarga");
            String tipoRecarga = (String) recargaCelularesForm.get("tipo_recarga");

            String codigoAlias = (String) recargaCelularesForm.get("codigo_alias");

            String existeAlias = "no";
            if (!aliasRecarga.equalsIgnoreCase("")) {
                existeAlias = "si";
            }
            
            String tipoOperadora = recargaCelularesForm.get("tipoOperadora") != null ? (String)recargaCelularesForm.get("tipoOperadora") : "";
            getLogger().info("[execute] verifica que exista tipoOperadora como atributo de formulario");
            if (tipoOperadora.equalsIgnoreCase("")){
                getLogger().info("[execute] No va parametro tipoOperadora");
            }
            else{
                if (getLogger().isInfoEnabled()){
                    getLogger().info("[execute] se pasa atributo tipoOperadora " + tipoOperadora);
                }
                request.setAttribute("tipoOperadora", tipoOperadora);
            }
            

            request.setAttribute("existeAlias", existeAlias);

            int montoAdicional = 0;
            String fechaInicio = TablaValores.getValor(paramFile, canal + "-" + nombreOperador, "FECHAINI");
            String fechaTermino = TablaValores.getValor(paramFile, canal + "-" + nombreOperador, "FECHATER");
            if (getLogger().isInfoEnabled()) {
                getLogger().info("[execute][" + rutCliente + "] celular: " + celular + ", montoCarga: " + montoCarga
                    + ", nombreOperador: " + nombreOperador + ", aliasRecarga: " + aliasRecarga
                    + ", tipoRecarga: " + tipoRecarga + ", codigoAlias: " + codigoAlias + ", existeAlias: "
                    + existeAlias + ", Fecha Inicio:" + fechaInicio + ", Fecha Termino:" + fechaTermino);
            }

            if (fechaInicio != null && fechaTermino != null) {
                int fechaIni = FechasUtil.comparaDias(
                    FechasUtil.convierteStringACalendar(fechaInicio, new SimpleDateFormat("yyyyMMdd")),
                    FechasUtil.ahora());
                int fechaTer = FechasUtil.comparaDias(
                    FechasUtil.convierteStringACalendar(fechaTermino, new SimpleDateFormat("yyyyMMdd")),
                    FechasUtil.ahora());
                if (fechaIni <= 0 && fechaTer >= 0) {
                    int porcentajeAdicional = TablaValores.getValor(paramFile, canal + "-" + nombreOperador + "-"
                        + "ADICIONAL", "M" + montoCarga) != null ? Integer.parseInt(TablaValores.getValor(
                        paramFile, canal + "-" + nombreOperador + "-" + "ADICIONAL", "M" + montoCarga)) : 0;

                    montoAdicional = (Integer.parseInt(montoCarga) * porcentajeAdicional) / 100;
                }
            }
            sesionHttp.setAttribute("montoAdicional", String.valueOf(montoAdicional));

            String promociones = reccelUtil.obtenerPromocionesPorMonto(canal, nombreOperador, montoCarga);

            if (promociones != null) {
                sesionHttp.setAttribute("promociones", promociones);
            }

            String vigenciaRecarga = TablaValores.getValor(paramFile, canal + "-" + nombreOperador + "-"
                + "VIGENCIA", "M" + montoCarga) != null ? TablaValores.getValor(paramFile, canal + "-"
                + nombreOperador + "-" + "VIGENCIA", "M" + montoCarga) : null;

            if (vigenciaRecarga != null) {
                sesionHttp.setAttribute("vigenciaRecarga", vigenciaRecarga);
            }

            String cuentaCargo = (String) recargaCelularesForm.get("cuenta_origen");

            String nombresDeLLave = TablaValores.getValor(PARAMSAFESIGNER, servicio, "datosLlave");

            String[] valoresDeLlave = { celular, nombreOperador, montoCarga, cuentaCargo
            };

            SafeSignerUtil seteaCamposDeLlave = new SafeSignerUtil();
            CamposDeLlaveTO campoDeLlaves = seteaCamposDeLlave.seteaCamposDeLlave(nombresDeLLave, valoresDeLlave);
            
            ParametrosEstrategiaSegundaClaveTO parametrosEstrategiaSegundaClaveTO;
            parametrosEstrategiaSegundaClaveTO = new ParametrosEstrategiaSegundaClaveTO(servicio, 
                                                                                        sesionBci.getCliente(), 
                                                                                        sesionBci.getCanal(), 
                                                                                        sesionBci);
            
            EstadoSegundaClaveTO estadoSegundaClaveTO = ControllerBCI.estadoSegundaClave(parametrosEstrategiaSegundaClaveTO);
            
            String segundaClaveSS = "";
            if (estadoSegundaClaveTO != null && estadoSegundaClaveTO.getIdSegundaClave() != null) {
                segundaClaveSS = estadoSegundaClaveTO.getIdSegundaClave().getNombre();
                segundaClaveSS = segundaClaveSS != null ? segundaClaveSS : "";
                
                if (getLogger().isEnabledFor(Level.DEBUG)) {
                    getLogger().debug("[execute] " + segundaClaveSS + ", status:[" + estadoSegundaClaveTO.isEstatus() + "]");
                }
                
                if (!estadoSegundaClaveTO.isEstatus() && (segundaClaveSS.equals(ENTRUST_SOFTTOKEN) || segundaClaveSS.equals(ENTRUST_TOKEN))) {
                    return mapping.findForward("fracaso");
                }
            }
            
            if (segundaClaveSS.equals(ENTRUST_SOFTTOKEN)) {
                String requiereLlave = TablaValores.getValor("Seguridad.parametros", "SegundaClaveRequiereLlave", ENTRUST_SOFTTOKEN);
                
                if (requiereLlave != null && requiereLlave.equalsIgnoreCase("true")) {
                    try {
                        if (getLogger().isEnabledFor(Level.DEBUG)) {
                            getLogger().debug("[execute] datos para Servicio " + servicio);
                            getLogger().debug("[execute] datos para campoDeLlaves " + campoDeLlaves.toString());
                        }
                        
                        String usuarioId = String.valueOf(sesionBci.getCliente().getRut()) + String.valueOf(sesionBci.getCliente().getDigito());
                        UsuarioSegundaClaveTO usuarioSegundaClave = new UsuarioSegundaClaveTO(usuarioId, 
                                                                                              ControllerBCI.obtieneDominio(sesionBci.getCanal().getCanalID()), 
                                                                                              sesionBci.getCanal().getCanalID());
                        
                        LlaveSegundaClaveTO llave = ControllerBCI.generarLlave(segundaClaveSS, 
                                                                               servicio, 
                                                                               usuarioSegundaClave, 
                                                                               campoDeLlaves, 
                                                                               "ONLINE");
                        
                        if (getLogger().isEnabledFor(Level.DEBUG)) {
                            getLogger().debug("[execute] to String llave " + llave.toString());
                            getLogger().debug("[execute] Estado de la llave : " + llave.isEstatus());
                        }
                        
                        parametrosEstrategiaSegundaClaveTO.setCamposDeLlave(campoDeLlaves);
                        parametrosEstrategiaSegundaClaveTO.setSessionBCI(null);
                        sesionBci.setParametrosEstrategiaSegundaClaveTO(parametrosEstrategiaSegundaClaveTO);
                        
                        sesionBci.setAttrib("transactionId", llave.getSerial());
                        request.setAttribute("frecuenciaEntrust", ControllerBCI.getFrecuenciaEntrust());
                        request.setAttribute("timeOutEntrust", ControllerBCI.getTimeOutEntrust());
                        
                        request.setAttribute(segundaClaveSS, segundaClaveSS);
                    }
                    catch(SeguridadException se) {
                        pagina = "fracaso";
                        if (getLogger().isEnabledFor(Level.ERROR)) {
                            getLogger().error(
                                "[execute][" + sesionBci.getCliente().getRut() + "] Warning, Exception: " + se.getMessage(), se);
                        }
                        request.setAttribute("javax.servlet.jsp.jspException", se);
                        return mapping.findForward("fracaso");
                    }
                    
                }
                else {
                    return mapping.findForward("fracaso");
                }
            }
            else if (segundaClaveSS.equals(ENTRUST_TOKEN)) {
                request.setAttribute(segundaClaveSS, segundaClaveSS);
            }
            else {
	            LlaveSegundaClaveTO llaveSegundaClave;
	            llaveSegundaClave = ControllerBCI
	                .generarLlavesDeSegundaClave(request, idName, servicio, campoDeLlaves);
	
	            if (llaveSegundaClave.isEstatus()) {
	                // Obteniendo data que contiene codigo safeSigner que luego se envia al formulario
	                data = llaveSegundaClave.getLlave().getClaveAlfanumerica();
	                sesionHttp.setAttribute("imagenqr", data);
	                request.setAttribute("data", data);
	            }
	            else {
	                if (getLogger().isDebugEnabled()) {
	                    getLogger().debug("[execute][" + rutCliente + "] "
	                        + "llaveSegundaClave==null o llaveSegundaClave.getLlave()==null"
	                        + " o llaveSegundaClave.getLlave().getClaveAlfanumerica()");
	                }
	                saveToken(request);
	
	            }
            }
            
            // validando monto carga

            double montoMaximoAutorizado = 0;
            montoMaximoAutorizado = helper.setMontosMaximosAutorizados(sesionBci, paramFile);
            if (getLogger().isInfoEnabled()) {
                getLogger().info("[execute][" + rutCliente + "] montoMaximoAutorizado:" + montoMaximoAutorizado);
            }

            int horaLimite = new Integer(TablaValores.getValor("recargaCelular.parametros", "horaLimite", "Desc"))
                .intValue();

            String idConvenio = TablaValores.getValor("recargaCelular.parametros", "Convenio", "Desc");

            // Valido que el monto acumulado diario no sea mayor al monto
            // m�ximo autorizado

            String fechaContable = formatoFechaContable.format(FechasUtil.diaHabilParaHoraLimite(Calendar
                .getInstance().getTime(), horaLimite));

            String rut = StringUtil.rellenaConCeros(rutCliente.substring(0, rutCliente.length() - 2), 8);

            // Valido que el monto acumulado diario no sea mayor al monto m�ximo autorizado
            if (getLogger().isInfoEnabled()) {
                getLogger().info("[execute][" + rutCliente + "] rut: [" + rut + "], fechaContableEnMov: ["
                    + fechaContable + "], idConvenio: [" + idConvenio + "]");
            }

            double montoRecargaDia = 0;
            montoRecargaDia = delegate.validarMontosMov(rut, fechaContable, idConvenio);

            if (getLogger().isDebugEnabled()) {
                getLogger().debug("[execute][" + rutCliente + "] monto acumulado :" + montoRecargaDia);
            }

            if ((montoRecargaDia + (new Double(montoCarga)).doubleValue()) > montoMaximoAutorizado) {
                getLogger().error("[execute][BCI_FINEX][" + rutCliente
                    + "] GeneralException: Cliente sobrepas� el l�mite diario.");
                throw new GeneralException("REC" + sesionBci.getCanal().getCanalID() + "-01");
            }
            // fin validaci�n monto carga
            request.setAttribute("monto_carga", montoCarga);

            String nombreCuenta = (String) recargaCelularesForm.get("nombre_cuenta");
            request.setAttribute("nombre_cuenta", nombreCuenta);

            String montoDisponible = (String) recargaCelularesForm.get("saldoDisponibleCte");
            request.setAttribute("montoDisponible", montoDisponible);

            double saldoDisponible = Double.parseDouble((String) recargaCelularesForm.get("saldoDisponibleCte"));
            double monto_Carga = Double.parseDouble((String) recargaCelularesForm.get("monto_carga"));
            if (getLogger().isDebugEnabled()) {
                getLogger().debug("[execute][" + rutCliente + "] nombreCuenta:" + nombreCuenta + ", montoDisponible: "
                    + montoDisponible + ", saldoDisponible: " + saldoDisponible + ", montoCarga: " + montoCarga);
            }
            String tieneSaldo = null;
            if (saldoDisponible >= monto_Carga) {
                tieneSaldo = "1";
            }
            else {
                tieneSaldo = "2";
            }
            request.setAttribute("tieneSaldo", tieneSaldo);

            IngresoTxVO[] recargas = null;

            try {
                recargas = delegate.consultaAliasRecargas(sesionBci.getCliente().getRut(), canal);
            }
            catch (Exception ex) {
                if (getLogger().isEnabledFor(Level.WARN)) {
                    getLogger().warn("[execute][" + rutCliente + "] Warning, Exception: " + ex.getMessage(), ex);
                }
            }

            if (recargas != null) {
                request.setAttribute("recargas", recargas);
            }

            pagina = "confirma";

        }
        catch (Exception e) {
            pagina = "fracaso";
            if (getLogger().isEnabledFor(Level.WARN)) {
                getLogger().warn(
                    "[execute][" + sesionBci.getCliente().getRut() + "] Warning, Exception: " + e.getMessage(), e);
            }
            request.setAttribute("javax.servlet.jsp.jspException", e);
        }
        if (getLogger().isInfoEnabled()) {
            getLogger().info("[execute][BCI_FINOK][" + sesionBci.getCliente().getRut()
                + "]  Retorna mapping.findForward(pagina); pagina [" + pagina + "]");
        }
        return mapping.findForward(pagina);
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
    
    /**
     * Metodo encargado de obtener el logger().
     * <p>
     *
     * Registro de versiones:<ul>
     * <li> 1.0 30/08/2017,  Marcelo Fuentes (SEnTRA) - Ricardo Carrasco (Ing. Soft. BCI): Versi�n Inicial.</li>
     * </ul>
     * <p>
     * @return el logger.
     * 
     * @since 1.14
     */
    private Logger getLogger() {
        if (logger == null) {
            logger = Logger.getLogger(this.getClass());
        }
        return logger;
    }
    
}
