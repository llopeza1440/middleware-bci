package wcorp.transantiago.ui.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import wcorp.env.WCorpConfig;
import wcorp.infraestructura.seguridad.autenticacion.util.SafeSignerUtil;
import wcorp.model.seguridad.CamposDeLlaveTO;
import wcorp.model.seguridad.ControllerBCI;
import wcorp.model.seguridad.EstadoSegundaClaveTO;
import wcorp.model.seguridad.LlaveSegundaClaveTO;
import wcorp.model.seguridad.ParametrosEstrategiaSegundaClaveTO;
import wcorp.model.seguridad.SessionBCI;
import wcorp.model.seguridad.UsuarioSegundaClaveTO;
import wcorp.transantiago.Constantes;
import wcorp.transantiago.ui.form.RecargaTarjetaBipForm;
import wcorp.util.TablaValores;

/**
 * <b>CONFIRMACION DE UNA RECARGA DE TARJETA BIP!</b>
 * <p>
 * Esta clase representa la confirmacion de una recarga de una tarheta Bip!
 * <p>
 * Se espera que sea usado en los canales: BCInet, TBanc y Nova.
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 02-02-2007, Enrique Villar(Neoris Chile)- versi�n inicial
 * <li>1.1 04/12/2012, Diego Urrutia Guerra(Imagemaker IT). Se agrega l�gica para identificar el canal,
 *                     para luego enviar mensaje a p�gina de respuesta</li>
 * <li>1.2 23-04-2013 Victor Hugo Enero (Orand) : Se agrega la generacion de llave previa para segunda clave
 * <li>1.3 30/08/2017 Marcelo Fuentes (SEnTRA) - Ricardo Carrasco (Ing. Soft. BCI): Se modifica m�todo 
 * {@link #execute(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)}. Ademas se agrega metodo
 * {@link #getLogger()} y las constantes {@link #TABLA_PARAMETROS_ENTRUST}, {@link #ENTRUST_TOKEN} y {@link #ENTRUST_SOFTTOKEN}.</li>
 * </ul>
 * <p>
 * <B>Todos los derechos reservados por Banco de Cr�dito e Inversiones.</B>
 * <p>
 */

public class ConfirmaTarjetaBipAction extends Action {

    /**
     * Tabla de par�metros de la aplicaci�n. 
     */
    private static final String TABLA_PARAMETROS_TRANSANTIAGO = "transantiago.parametros";
    
    /**
     * tabla de par�metros de entrust.
     */
    private static final String TABLA_PARAMETROS_ENTRUST = "entrust.parametros";
    
    /**
     * Tipo de Segunda Clave Softtoken Entrust.
     */
    private static final String ENTRUST_TOKEN = TablaValores.getValor(TABLA_PARAMETROS_ENTRUST, "nombreToken", "desc");
    
    /**
     * Tipo de Segunda Clave Softtoken Entrust.
     */
    private static final String ENTRUST_SOFTTOKEN = TablaValores.getValor(TABLA_PARAMETROS_ENTRUST, "nombreSoftToken", "desc");
    
    /** 
     * Logger del action.
     */
    private transient Logger logger = (Logger)Logger.getLogger(ConfirmaTarjetaBipAction.class);
    
    /**
     * <b>Confirma Recarga Tarjeta Bip</b><br>
     * 
     * <br>
     * Registro de Versiones:<ul>
     * <li>1.0 xx/xx/xxxx, Desconocido. (Desconocido): versi�n inicial.</li>
     * <li>1.1 30/08/2017  Marcelo Fuentes (SEnTRA) - Ricardo Carrasco (Ing. Soft. BCI): Se agrega logica que revisa 
     * si se requiere autenticaci�n Entrust Token o Entrust SoftToken. Ademas se normaliza log.<li>
     * </ul>
     * <br>
     * 
     * @param mapping Mapeo de forwards para la acci�n.
     * @param form    ActionForm con las variables de acci�n si corresponde.
     * @param request Petici�n HTTP que se est� procesando.
     * @param response Respuesta HTTP que se esta generando.
     * @return El forward correspondiente al formulario por defecto.
     * @throws Exception En caso de producirse un error.
     * @since 1.0
     * <P>
     */
    public ActionForward execute( ActionMapping mapping, ActionForm form, HttpServletRequest request,
            HttpServletResponse response ) throws Exception {
        
        HttpSession sesionHttp = request.getSession(false);
        SessionBCI sesionBci = (SessionBCI) sesionHttp.getAttribute(WCorpConfig.SESION_BCI);

        if ( sesionBci.getCliente() != null ){
            String canal = sesionBci.getCanal().getCanalID();
            String mensaje1 = null;
            String mensaje2 = null;
            
            mensaje1 = TablaValores.getValor(TABLA_PARAMETROS_TRANSANTIAGO, "MSG_REC_INFO_1", canal);
            if(getLogger().isDebugEnabled()){
                getLogger().debug("[execute].." + sesionBci.getRutUsuario() + "mensaje : " + mensaje1);
            }
            mensaje2 = TablaValores.getValor(TABLA_PARAMETROS_TRANSANTIAGO, "MSG_REC_INFO_2", "Desc");
            if(getLogger().isDebugEnabled()){
                getLogger().debug("[execute].." + sesionBci.getRutUsuario() + "mensaje : " + mensaje2);
            }
            request.setAttribute("mensaje_info_1", mensaje1);
            request.setAttribute("mensaje_info_2", mensaje2);
            
            
        }
        else{ // no se pudo obtener su session.
            if(getLogger().isEnabledFor(Level.ERROR)){
                getLogger().error("Session del usuario no se pudo Obtener");
            }
            this.grabaError(request, "errors.sin_session", "");
            return mapping.findForward("error");
        }

        RecargaTarjetaBipForm recargaForm = (RecargaTarjetaBipForm) form;
        

        String  nombresDeLLave= TablaValores.getValor(Constantes.PARAMSAFESIGNER, 
            Constantes.IDSERVICE, "datosLlave");
        
        String [] valoresDeLlave = {recargaForm.getNumeroTarjeta(), 
            recargaForm.getMontoCarga(), recargaForm.getCuenta()};

        SafeSignerUtil seteaCamposDeLlave= new SafeSignerUtil();
        CamposDeLlaveTO campoDeLlaves = seteaCamposDeLlave.seteaCamposDeLlave(nombresDeLLave, valoresDeLlave);
        
        ParametrosEstrategiaSegundaClaveTO parametrosEstrategiaSegundaClaveTO;
        parametrosEstrategiaSegundaClaveTO = new ParametrosEstrategiaSegundaClaveTO(Constantes.IDSERVICE, 
                                                                                    sesionBci.getCliente(), 
                                                                                    sesionBci.getCanal(), 
                                                                                    sesionBci);
        
        EstadoSegundaClaveTO estadoSegundaClaveTO = ControllerBCI.estadoSegundaClave(parametrosEstrategiaSegundaClaveTO);
        
        String segundaClaveSS = "";
        if (estadoSegundaClaveTO != null && estadoSegundaClaveTO.getIdSegundaClave() != null) {
            segundaClaveSS = estadoSegundaClaveTO.getIdSegundaClave().getNombre();
            segundaClaveSS = segundaClaveSS != null ? segundaClaveSS : "";
            
            if (getLogger().isEnabledFor(Level.DEBUG)) {
                getLogger().debug("[execute] " + segundaClaveSS + ", status:[" + estadoSegundaClaveTO.isEstatus() + "]");
            }
            
            if (!estadoSegundaClaveTO.isEstatus() && (segundaClaveSS.equals(ENTRUST_SOFTTOKEN) || segundaClaveSS.equals(ENTRUST_TOKEN))) {
                this.grabaError(request, "errors.noTransaccion", "");
                return mapping.findForward("fracaso");
            }
        }
        
        if (segundaClaveSS.equals(ENTRUST_SOFTTOKEN)) {
            String requiereLlave = TablaValores.getValor("Seguridad.parametros", "SegundaClaveRequiereLlave", ENTRUST_SOFTTOKEN);
            
            if (requiereLlave != null && requiereLlave.equalsIgnoreCase("true")) {
                String usuarioId = String.valueOf(sesionBci.getCliente().getRut()) + String.valueOf(sesionBci.getCliente().getDigito());
                UsuarioSegundaClaveTO usuarioSegundaClave = new UsuarioSegundaClaveTO(usuarioId, 
                                                                                      ControllerBCI.obtieneDominio(sesionBci.getCanal().getCanalID()), 
                                                                                      sesionBci.getCanal().getCanalID());
                
                parametrosEstrategiaSegundaClaveTO.setUsuarioSegundaClave(usuarioSegundaClave);
                parametrosEstrategiaSegundaClaveTO.setCamposDeLlave(campoDeLlaves);
                parametrosEstrategiaSegundaClaveTO.setSessionBCI(null);
                sesionBci.setParametrosEstrategiaSegundaClaveTO(parametrosEstrategiaSegundaClaveTO);
                
                request.setAttribute("frecuenciaEntrust", ControllerBCI.getFrecuenciaEntrust());
                request.setAttribute("timeOutEntrust", ControllerBCI.getTimeOutEntrust());
                
                request.setAttribute(segundaClaveSS, segundaClaveSS);
            }
            else {
                this.grabaError(request, "errors.noTransaccion", "");
                return mapping.findForward("error");
            }
        }
        else if (segundaClaveSS.equals(ENTRUST_TOKEN)) {
            request.setAttribute(segundaClaveSS, segundaClaveSS);
        }
        else {
	       LlaveSegundaClaveTO llaveSegundaClave;
	       llaveSegundaClave = ControllerBCI
	           .generarLlavesDeSegundaClave(request, Constantes.IDNAME, Constantes.IDSERVICE, campoDeLlaves);
	       
	       if (llaveSegundaClave.isEstatus()) {
	           getLogger().debug("[execute] Se genero la llave previa");
	           request.setAttribute("data", llaveSegundaClave.getLlave().getClaveAlfanumerica());
	           sesionHttp.setAttribute("imagenqr", llaveSegundaClave.getLlave().getClaveAlfanumerica());
	       }
	       else {
	           getLogger().debug("[execute] No se genero la llave previa");  
	           saveToken(request);
	
	       }
        }
        

        return mapping.findForward("paso2");
    }
    
    /**
     * M�todo de agregar un error de acuerdo al tipo de error (keyError)
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 04/12/2012, Diego Urrutia Guerra(Imagemaker IT)- versi�n inicial
     * </ul>
     * </p>
     * 
     * @param request
     * @param keyError key del error correspondiente al error del property de la aplicaccion(ver strut-config)
     * @param valueError un mensaje con el error si es necesario o "" si no se desea enviar nada.d
     */
    private void grabaError(HttpServletRequest request, String keyError, String valueError) {
        ActionErrors errors = new ActionErrors();
        ActionError error = new ActionError(keyError, valueError);
        errors.add("errorGlobal", error);
        super.saveErrors(request, errors);

    }
    
    /**
     * Metodo encargado de obtener el logger().
     * <p>
     *
     * Registro de versiones:<ul>
     * <li> 1.0 30/08/2017,  Marcelo Fuentes (SEnTRA) - Ricardo Carrasco (Ing. Soft. BCI): Versi�n Inicial.</li>
     * </ul>
     * <p>
     * @return el logger.
     * 
     * @since 1.3
     */
    private Logger getLogger() {
        if (logger == null) {
            logger = Logger.getLogger(this.getClass());
        }
        return logger;
    }
    
}
