package wcorp.model.seguridad;

import java.io.Serializable;

/**
 * <b>CLAVE NUMERICA</b>
 * <p>
 * Clase Clave Numerica de segunda clave
 * <p>
 * 
 * Registro de versiones:<ul>
 * 
 * <li>1.0 (11/03/2013, Veronica Cestari. (Orand)): version inicial.</li>
 * <li>2.0 10/07/2017 Luis L�pez Alamos (SEnTRA) - Ricardo Carrasco Caceres (Ing. Soft. BCI): Se agregan atributos {@link #nombreDispositivoAsociado} y [{@link #claveAlfanumericaMovil} encapsulado}</li>
 * 
 * </ul>
 * 
 * <p><b>Todos los derechos reservados por Banco de Cr�dito e Inversiones.</b></p>
 */
public class RepresentacionNumericaSegundaClaveTO implements RepresentacionSegundaClave,Serializable {

    /**
     * serial UID.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Clave numerica.
     */
    private long claveNumerica;
    
    /**
     * Clave binario.
     */
    private byte[] claveBinaria;
    
    /**
     * Clave Alfanumerico.
     */
    private String claveAlfanumerica;
    
    /**
     * Tipo de clave.
     */
    private int tipoDeClave;
    
    /**
     * Nombre de dispositivo asociado a la representacion de la llave.
     */
    private String nombreDispositivoAsociado;

    /**
     * Clave Alfanumerica para canales M�viles.
     */
    private String claveAlfanumericaMovil;

    
    public String getNombreDispositivoAsociado() {
        return nombreDispositivoAsociado;
    }

    public void setNombreDispositivoAsociado(String nombreDispositivoAsociado) {
        this.nombreDispositivoAsociado = nombreDispositivoAsociado;
    }

    public long getClaveNumerica() {
        return claveNumerica;
    }

    public byte[] getClaveBinaria() {
        return claveBinaria;
    }

    public String getClaveAlfanumerica() {
        return claveAlfanumerica;
    }

    public int getTipoDeClave() {
        return tipoDeClave;
    }

    public void setClaveNumerica(long claveNumerica) {
        this.claveNumerica = claveNumerica;
    }

    public void setClaveBinaria(byte[] claveBinaria) {
        this.claveBinaria = claveBinaria;
    }

    public void setClaveAlfanumerica(String claveAlfanumerica) {
        this.claveAlfanumerica = claveAlfanumerica;
    }

    public void setTipoDeClave(int tipoDeClave) {
        this.tipoDeClave = tipoDeClave;
    }

    public String getClaveAlfanumericaMovil() {
        return claveAlfanumericaMovil;
    }

    public void setClaveAlfanumericaMovil(String claveAlfanumericaMovil) {
        this.claveAlfanumericaMovil = claveAlfanumericaMovil;
    }

    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append("RepresentacionNumericaSegundaClaveTO: "
        + "claveNumerica[").append(claveNumerica);
        sb.append("], claveAlfanumerica[").append(claveAlfanumerica);
        sb.append("], tipoDeClave[").append(tipoDeClave);
        sb.append("], nombreDispositivoAsociado[").append(nombreDispositivoAsociado);
        sb.append("], claveAlfanumericaMovil[").append(claveAlfanumericaMovil);
        sb.append("]");
        return sb.toString();        
    }

}
