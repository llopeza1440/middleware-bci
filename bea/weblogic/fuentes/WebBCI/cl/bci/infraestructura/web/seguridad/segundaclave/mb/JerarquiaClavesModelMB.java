package cl.bci.infraestructura.web.seguridad.segundaclave.mb;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import wcorp.model.seguridad.EstrategiaAutenticacionFactory;
import wcorp.model.seguridad.EstrategiaEntrustSoftToken;
import wcorp.model.seguridad.EstrategiaSegundaClave;
import wcorp.model.seguridad.ParametrosEstrategiaSegundaClaveTO;
import wcorp.serv.seguridad.SeguridadException;
import wcorp.util.StringUtil;
import wcorp.util.TablaValores;

import cl.bci.infraestructura.web.seguridad.segundaclave.SegundaClaveUtil;
import cl.bci.infraestructura.web.seguridad.segundaclave.to.CamposDeLlaveTO;
import cl.bci.infraestructura.web.seguridad.segundaclave.to.EstadoSegundaClaveTO;
import cl.bci.infraestructura.web.seguridad.segundaclave.to.LlaveSegundaClaveTO;
import cl.bci.infraestructura.web.seguridad.segundaclave.to.SegundaClaveEnum;
import cl.bci.infraestructura.web.seguridad.segundaclave.to.UsuarioSegundaClaveTO;

/**
 * <b>JerarquiaClavesModelMB</b>.
 * <p>
 * Clase que permite obtener la jerarquia de claves de un cliente.
 * <p>
 * 
 * Registro de versiones:<ul>
 * 
 * <li>1.0 (11/03/2013, Victor Enero D. (Orand S.A.)): version inicial
 * <li>1.1 (08/10/2013, Rodrigo Briones O. (Bci), Victor Enero D. (Orand S.A.)): Se agrega m�todo 
 * 	@link #obtieneOpcionalidadSegundaClave(String).
 * <li>2.0 10/07/2017 Luis Lopez Alamos - Jaime Suazo Diaz (SEnTRA) - Ricardo Carrasco C�ceres (Ing. Soft. BCI):
 *  Se modifica m�todo @link {@link #generarLlave(SegundaClaveEnum, String, UsuarioSegundaClaveTO, CamposDeLlaveTO, String)}
 *  Se agregan las constantes {@link #TABLA_PARAMETROS_ENTRUST}, {@link #DESC}, {@link #ONLINE}, {@link #OFFLINE} y {@link #SOFT_TOKEN_ENTRUST}
 *  Se agrega m�todo @link {@link #consultarEstadoDesafioOperacion()} por proyecto Entrust.</li>
 * <li>2.1 25/08/2017 Luis Lopez Alamos - Ricardo Carrasco C�ceres (Ing. Soft. BCI): Se agregan las constantes {@link #TIENE_ENTRUST_HTK}, {@link #TIENE_ENTRUST_STK}
 * y {@link #SIN_ENTRUST}. Se agrega m�todo {@link #tieneEntrust} por proyecto mejoras TEF para Entrust.</li>
 * </ul>
 * 
 * <p><b>Todos los derechos reservados por Banco de Cr�dito e Inversiones.</b></p>
 */
@ManagedBean
@SessionScoped
public class JerarquiaClavesModelMB implements Serializable {

    /**
     * Serial.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Log.
     */
    private static Logger logger = (Logger) Logger.getLogger(JerarquiaClavesModelMB.class);

    /**
     * Tabla de parametros asociada.
     */
    private static final String TABLA_PARAMETROS = "SegundaClave.parametros";

    /**
     * Id de NO OK.
     */
    private static final int IDNOOK = -1;

    /**
     * Indica si la segunda clave es opcional.
     */
    private static final String REQUERIDO = "NO";
    
    /**
     * tabla de par�metros de entrust.
     */
    private static final String TABLA_PARAMETROS_ENTRUST = "entrust.parametros";
    
    /**
     * Constante utilizada para definir descripci�n de valores de llaves desde tablas de parametros. 
     */
    private static final String DESC = "desc";
    
    /**
     * tipo de segunda clave para SoftToken Entrust.
     */
    private static final String ONLINE = TablaValores.getValor(TABLA_PARAMETROS_ENTRUST, "modoOnline", DESC);
    
    /**
     * tipo de segunda clave para SoftToken Entrust.
     */
    private static final String OFFLINE = TablaValores.getValor(TABLA_PARAMETROS_ENTRUST, "modoOffline", DESC);
    
    /**
     * Tipo de Segunda Clave Softtoken Entrust.
     */
    private static final String SOFT_TOKEN_ENTRUST = TablaValores.getValor(TABLA_PARAMETROS_ENTRUST, "nombreSoftToken", DESC);
    
    /**
     * Constante que indica la inexistencia de autenticaci�n de Tipo SegundaClave Entrust. 
     */
    private static final char SIN_ENTRUST = 'N';
    
    /**
     * Constante que representa existencia de autenticaci�n de Tipo De SegundaClave Entrust Token.
     */
    private static final char TIENE_ENTRUST_STK = 'S';
    
    /**
     * Constante que representa existencia de autenticaci�n de Tipo De SegundaClave Entrust SoftToken.
     */
    private static final char TIENE_ENTRUST_HTK = 'H';
    
    /**
     * Estados Dispositivos.
     */
    private Map estadosDispositivos;

    /**
     * Codigo de Transacci�n perteneciente a Jerarquia ENTRUST SoftToken ONLINE.
     */
    private String transactionId;
    
    
    /**
     * Codigo de Transacci�n perteneciente a Jerarquia ENTRUST SoftToken ONLINE.
     */
    private String estadoTransaccionEntrust;
    
    /**
     * <p>
     * Obtiene metodos de autenticacion.
     * </p>
     * <ul>
     * <li>1.0 11/03/2013 Victor Enero D. (Orand S.A.): versi�n inicial.
     * </ul>
     * 
     * @param usuarioSegundaClave
     * usuario de segunda clave.
     * @param servicio
     * servicio.
     * @return String metodo autenticacion.
     * @throws SeguridadException
     * Seguridad Exception.
     */
    public String obtieneMetodoAutentificacion(String servicio, UsuarioSegundaClaveTO usuarioSegundaClave)
        throws SeguridadException {
        EstadoSegundaClaveTO estado = estadoSegundaClave(servicio, usuarioSegundaClave);
        return estado.getSegundaClaveEnum().getNombre();
    }

    /**
     * <p>
     * Obtiene jerarquia de dominio.
     * </p>
     * <ul>
     * <li>1.0 11/03/2013 Victor Enero D. (Orand S.A.): versi�n inicial.
     * </ul>
     * 
     * @param dominioId
     * id del dominio.
     * @return PrioridadJerarquiaSegundaClaveTO prioridad.
     */
    public String[] obtieneJerarquiaDominio(String dominioId) {
        logger.debug("[obtieneJerarquiaDominio] [" + dominioId + "]");
        String jerarquiaSegundaClave = TablaValores.getValor(TABLA_PARAMETROS, dominioId, "Jerarquias");
        return StringUtil.divide(jerarquiaSegundaClave, ",");

    }

    /**
     * <p>
     * Obtiene metodos por servicio.
     * </p>
     * <ul>
     * <li>1.0 11/03/2013 Victor Enero D. (Orand S.A.): versi�n inicial.
     * </ul>
     * 
     * @param servicio
     * servicio.
     * @return String metodos.
     */
    public String[] obtieneMetodosPorServicio(String servicio) {
        logger.debug("[obtieneMetodosPorServicio] [" + servicio + "]");
        String segundasClaves = TablaValores.getValor(TABLA_PARAMETROS, servicio, "SegundasClaves");
        return StringUtil.divide(segundasClaves, ",");
    }

    /**
     * <p>
     * Obtiene opcionalidad del uso de segunda clave para el servicio dado
     * </p>
     * <ul>
     * <li>1.0 08/10/2013 Rodrigo Briones O. (Bci), Victor Enero D. (Orand S.A.): versi�n inicial.
     * </ul>
     * 
     * @param servicio
     * servicio.
     * @return boolean esOpcional.
     */
    private boolean obtieneOpcionalidadSegundaClave(String servicio) {
    	if (logger.isDebugEnabled()) {
    		logger.debug("[obtieneOpcionalidadSegundaClave] [" + servicio + "]");
    	}
    	boolean esOpcional = false;
    	String valorOpcionalidad = TablaValores.getValor(TABLA_PARAMETROS, servicio, "esRequerido");

    	if (REQUERIDO.equals(valorOpcionalidad)){
    		esOpcional = true;
    	}
    	return esOpcional;
    }
    
    /**
     * Obtiene el estado de la segunda clave.
     * 
     * @param servicio
     * servicio.
     * @param usuarioSegundaClave
     * usuario segunda clave.
     * @return EstadoSegundaClaveTO estado de segunda clave.
     * @throws SeguridadException
     * excepcion de seguridad.
     */
    public EstadoSegundaClaveTO estadoSegundaClave(String servicio, UsuarioSegundaClaveTO usuarioSegundaClave)
        throws SeguridadException {

        logger.debug("[estadoSegundaClave] [" + usuarioSegundaClave.getUsuarioId() + "]");

        /*
         * prioridadJerarquiaSegundaClave representa una lista ordenada de segundas claves.
         */
        String[] dominios = obtieneJerarquiaDominio(usuarioSegundaClave.getDominioId());

        /*
         * Por cada segunda clave de la jerarquia, se verifica que la segunda clave este habilitada para el
         * servicio.
         */
        String[] segundasClavesParaServicio = obtieneMetodosPorServicio(servicio);

        if (estadosDispositivos == null) {
            estadosDispositivos = new HashMap();
        }

        if (dominios == null) {
            logger.debug("[estadoSegundaClave] No existe dominios");
            throw new SeguridadException("SAFESIGNER05");
        }

        if (segundasClavesParaServicio == null) {
            logger.debug("[estadoSegundaClave] [" + servicio + "] servicio no existe en la tabla de parametros");
            throw new SeguridadException("SAFESIGNER05");
        }

        for (int i = 0; i < dominios.length; i++) {
            for (int j = 0; j < segundasClavesParaServicio.length; j++) {
                if (logger.isInfoEnabled()) {
                    logger.info("[estadoSegundaClave] [" + usuarioSegundaClave.getUsuarioId() + "] dominio = "
                        + dominios[i] + " segundasClavesParaServicio = " + segundasClavesParaServicio[j]);
                }

                try {
                    if (dominios[i].equals(segundasClavesParaServicio[j])) {
                        SegundaClaveEnum segundaClave = SegundaClaveEnum
                            .obtieneSegundaClaveEnum(segundasClavesParaServicio[j]);

                        EstadoSegundaClaveTO estadoSegundaClave;
                        // preguntar por si no esta en el hashmap
                        if (!estadosDispositivos.containsKey(segundaClave.getNombre())) {

                            logger.debug("[estadoSegundaClave] estadosDispositivos no se encuentra en hash");
                            // verificar que el usuario tenga esta segunda clave activa
                            EstrategiaSegundaClave estrategiaSegundaClave = EstrategiaAutenticacionFactory
                                    .obtenerEstrategiaSegundaClave(SegundaClaveUtil
                                            .toPrioridadSegundaClaveIdTO(segundaClave));                            
                            ParametrosEstrategiaSegundaClaveTO parametrosConsulta = 
                                    new ParametrosEstrategiaSegundaClaveTO(
                                            SegundaClaveUtil.toUsuarioSegundaClaveTO(usuarioSegundaClave));
                            estadoSegundaClave = SegundaClaveUtil.toEstadoSegundaClaveTO(estrategiaSegundaClave
                                .consultarEstadoSegundaClave(parametrosConsulta));

                            if (logger.isDebugEnabled()) {
                                logger.debug("[estadoSegundaClave] [" + usuarioSegundaClave.getUsuarioId()
                                    + "] glosa = " + estadoSegundaClave.getGlosa() + " codigo = "
                                    + estadoSegundaClave.getIdCodigoEstado());
                            }

                            estadosDispositivos.put(segundaClave.getNombre(), estadoSegundaClave);

                        }
                        else {
                            logger.debug("[estadoSegundaClave] estadosDispositivos en hash");
                            estadoSegundaClave = (EstadoSegundaClaveTO) estadosDispositivos.get(segundaClave
                                .getNombre());
                        }

                        if (estadoSegundaClave.isEstatus()) {
                            logger.debug("[estadoSegundaClave] [" + usuarioSegundaClave.getUsuarioId()
                                + "] Estatus OK ");
                            return estadoSegundaClave;
                        }
                        else {
                            logger.debug("[estadoSegundaClave] [" + usuarioSegundaClave.getUsuarioId()
                                + "] Estatus NOK, pasando a siguiente prioridad ");
                        }
                    }
                }
                catch (Exception e) {
                    logger.error("[estadoSegundaClave] [" + e.getMessage());
                }
            }
        }

        logger.error("[estadoSegundaClave] [" + usuarioSegundaClave.getUsuarioId()
            + "] No hay segunda clave valida para servicio: " + servicio);

        EstadoSegundaClaveTO estado = new EstadoSegundaClaveTO();
        if (obtieneOpcionalidadSegundaClave(servicio)) {
        	if (logger.isDebugEnabled()) {
	        	logger.debug("[estadoSegundaClave] [" + usuarioSegundaClave.getUsuarioId()
	                    + "] Servicio configurado con opcionalidad de segunda clave: " + servicio);
        	}
            estado.setIdCodigoEstado(0);
            estado.setEstatus(true);
            estado.setSegundaClaveEnum(SegundaClaveEnum.SINCLAVE);
            return estado;
        } 
        
        estado.setIdCodigoEstado(IDNOOK);
        estado.setEstatus(false);
        return estado;
        
    }

    /**
     * Metodo que genera la llave de autenticacion de transaccion.
     * <p>
     * Registro de Versiones:
     * <ul>
     * <li>1.0 ??/??/?? Desconocido : Versi�n Inicial.</li>
     * <li>2.0 10/07/2017 Luis Lopez Alamos - Jaime Suazo Diaz (SEnTRA) - Ricardo Carrasco C�ceres (Ing. Soft. BCI): Se agrega parametro <code>modoTransaccion</code> 
     * con la finalidad de poder diferenciar cuando se debe generar una llave de tipo ONLINE/OFFLINE para autenticaci�n Entrust SoftToken.
     * Se normaliza formato log</li> 
     * </ul>
     * </p>
     * @param segundaClaveEnum id de segunda clave.
     * @param servicio servicio.
     * @param usuarioSegundaClave usuario de segunda clave.
     * @param campos campos.
     * @param modoTransaccion ONLINE/OFFLINE si vienen como Entrust SoftToken.
     * @return LlaveSegundaClaveTO llave de segunda clave.
     * @throws SeguridadException excepcion de seguridad.
     * @since 1.0
     */
    public LlaveSegundaClaveTO generarLlave(SegundaClaveEnum segundaClaveEnum, String servicio,
        UsuarioSegundaClaveTO usuarioSegundaClave, CamposDeLlaveTO campos, String modoTransaccion) throws SeguridadException {
        if (logger.isEnabledFor(Level.INFO)){
            logger.info("[generarLlave] [BCI_INI] [" + usuarioSegundaClave.getUsuarioId() + "] segundaclaveEnum: " + segundaClaveEnum + " servicio: " + servicio + " usuarioSegundaClave: " + usuarioSegundaClave.toString() + " camposDeLlave " + campos.toString() + " modoTransaccion " + modoTransaccion);
        }
        EstrategiaSegundaClave estrategiaSegundaClave = EstrategiaAutenticacionFactory
            .obtenerEstrategiaSegundaClave(SegundaClaveUtil.toPrioridadSegundaClaveIdTO(segundaClaveEnum));

        ParametrosEstrategiaSegundaClaveTO parametrosEstrategiaSegundaClaveTO = new ParametrosEstrategiaSegundaClaveTO(
           servicio, SegundaClaveUtil.toUsuarioSegundaClaveTO(usuarioSegundaClave),
            SegundaClaveUtil.toCamposDeLlaveTO(campos));

        if (logger.isEnabledFor(Level.DEBUG)) {
        logger.debug("[generarLlave] [" + usuarioSegundaClave.getUsuarioId() + "] Antes de generar llave");
        }
        
        if ((SOFT_TOKEN_ENTRUST.equalsIgnoreCase(segundaClaveEnum.getNombre()))
                && (modoTransaccion != null) 
                && ((modoTransaccion.equalsIgnoreCase(ONLINE) || modoTransaccion.equalsIgnoreCase(OFFLINE)))){
            if (logger.isEnabledFor(Level.INFO)){
                logger.info("[generarLlave] seteo el modo Transaccion " + modoTransaccion + " para Entrust SoftToken");
            }
            parametrosEstrategiaSegundaClaveTO.setTipoSegundaClave(modoTransaccion);   
                
        }
		 
		 if (logger.isEnabledFor(Level.INFO)){
                logger.info("[generarLlave][BCI_FINOK] Valor de parametrosEstrategiaSegundaClaveTO : " + parametrosEstrategiaSegundaClaveTO);
         }
			
        return SegundaClaveUtil.toLlaveSegundaClaveTO(estrategiaSegundaClave
            .generarLlave(parametrosEstrategiaSegundaClaveTO));
    }

	 /**
     * Permite saber si un usuario tiene configurado SafeSigner para
     * el servicio a usar.
     * 
     * @param servicio Servicio a consultar
     * @param usuarioSegundaClave Usuario conectado segunda clave.
     * @return EstadoSegundaClaveTO estado de segunda clave.
     * @throws SeguridadException excepcion de seguridad.
     */ 
    public boolean tieneSafeSigner(String servicio, 
    		UsuarioSegundaClaveTO usuarioSegundaClave) throws SeguridadException {
        boolean tieneSafeSigner = false;
        EstadoSegundaClaveTO estadoSafeSigner;
		estadoSafeSigner = estadoSegundaClave (servicio, usuarioSegundaClave);
        if( estadoSafeSigner != null ) {
	        if( SegundaClaveEnum.SAFESIGNER.equals( estadoSafeSigner.getSegundaClaveEnum() ) ) {
	        	tieneSafeSigner = estadoSafeSigner.isEstatus();
	        }
        }
        return tieneSafeSigner;
    }
    
    
    public void setTransactionId(String codigoTransaccion){
        this.transactionId = codigoTransaccion;
    }
    
    public String getTransactionId(){
        return this.transactionId;
    }
    
    /**
     * Consulta al pooling de curus por el estado actual de un desafio de tipo SoftToken Online para Autenticacion Entrust.
     * <p>
     * 
     * Registro de Versiones:
     * <ul>
     * <li>1.0 10/07/2017 Luis L�pez Alamos (SEnTRA) - Ricardo Carrasco C�ceres (Ing. Soft. BCI): Versi�n Inicial.</li>
     * </ul>
     * <P>
     * 
     * @return String con el resultado.
     * @since 2.0
     */
    public String consultarEstadoDesafioOperacion(){
        if (getLogger().isEnabledFor(Level.INFO)) {
        	getLogger().info("[consultarEstadoDesafioOperacion][BCI_INI]");
        }
        String resultado = null;
        try {
            EstrategiaEntrustSoftToken estrategiaConsultada = new EstrategiaEntrustSoftToken();
            resultado = estrategiaConsultada.consultarEstadoDesafio(this.getTransactionId());
            if (getLogger().isEnabledFor(Level.INFO)) {
            	getLogger().info("[consultarEstadoDesafioOperacion] [resultado =" + resultado + "] [BCI_FINOK]");
            }
        }
        catch (Exception e){
            if (getLogger().isEnabledFor(Level.WARN ) ) { 
            	getLogger().warn("[consultarEstadoDesafioOperacion][BCI_FINEX][Exception] Warning, Exception al obtener la consulta al pooling: " 
            			+ e.getMessage(), e); 
            }
        }
        
        estadoTransaccionEntrust = resultado;
        
        if (getLogger().isEnabledFor(Level.INFO)) {
        	getLogger().info("[consultarEstadoDesafioOperacion][BCI_FINOK] Valor de estado : " + this.estadoTransaccionEntrust);
        }
        return resultado;
    }
        
    public String getEstadoTransaccionEntrust(){
        return this.estadoTransaccionEntrust;
    }
            
    
    /**
     * Metodo para obtener el log de la clase.
     * 
     * Registro de versiones:
     * <ul>
     * <li>1.0 10/07/2017 Jaime Suazo. (SEnTRA) - Ricardo Carrasco (Ing. Soft. BCI): versi�n inicial.</li>
     * </ul>
     * 
     * @return Logger log de la clase.
     * @since 2.0
     */
    private Logger getLogger() {
        if (logger == null) {
            logger = Logger.getLogger(JerarquiaClavesModelMB.class);
        }
        return logger;
    }    
    
    
    /**
     * Consulta al pooling de curus por el estado actual de un desafio de tipo SoftToken Online para Autenticacion Entrust.
     * <p>
     * 
     * Registro de Versiones:
     * <ul>
     * <li>1.0 25/08/2017 Luis L�pez Alamos (SEnTRA) - Ricardo Carrasco C�ceres (Ing. Soft. BCI): Versi�n Inicial.</li>
     * </ul>
     * <P>
     * @param servicio a consultar.
     * @param usuarioSegundaClave con datos del usuario a consultar.
     * @throws SeguridadException en caso de algun error al consultar el estado de segundaClave.
     * @return char tieneEntrust ('N' : No tiene Entrust, 'S' : tiene EntrustSoftToken, 'H' : tiene EntrustF�sico).
     * @since 2.1
     */
    public char tieneEntrust(String servicio, UsuarioSegundaClaveTO usuarioSegundaClave) throws SeguridadException {
    	if (getLogger().isEnabledFor(Level.INFO)) {
        	getLogger().info("[tieneEntrust][BCI_INI]");
        }
        char tieneEntrust = SIN_ENTRUST;
        EstadoSegundaClaveTO estadoEntrust = estadoSegundaClave(servicio, usuarioSegundaClave);
        if( estadoEntrust != null ) {
	        if( SegundaClaveEnum.ENTRUST_SOFTTOKEN.equals( estadoEntrust.getSegundaClaveEnum() ) ) {
	        	if (estadoEntrust.isEstatus()) {
	        		tieneEntrust = TIENE_ENTRUST_STK;
	        	}
	        }
	        else if( SegundaClaveEnum.ENTRUST_TOKEN.equals( estadoEntrust.getSegundaClaveEnum() ) ) {
	        	if (estadoEntrust.isEstatus()) {
	        		tieneEntrust = TIENE_ENTRUST_HTK;
	        	}
	        }
        }
        if (getLogger().isEnabledFor(Level.INFO)) {
        	getLogger().info("[tieneEntrust][BCI_FINOK][tieneEntrust : " + tieneEntrust+" ]");
        }
        return tieneEntrust;
    }
}
