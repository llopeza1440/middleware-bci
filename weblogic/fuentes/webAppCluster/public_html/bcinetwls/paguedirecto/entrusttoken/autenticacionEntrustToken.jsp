<%@ page language="java" %>
<%@taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@taglib uri="/WEB-INF/fmt.tld" prefix="fmt"%>
<jsp:useBean id="sessionBci" scope="session" class="wcorp.model.seguridad.SessionBCI"></jsp:useBean>
<jsp:setProperty name="sessionBci" property="*" />
<fmt:setBundle basename="resources.messages-PagueDirecto"/>

<c:set var="urlBase" value="https://www.bci.cl"/>

<c:choose>
  <c:when test="${!empty requestScope.estadoPopUp}">
	<c:set var="estadoPopUp" value="${requestScope.estadoPopUp}"/>
  </c:when>
  <c:otherwise>
	<c:if test="${!empty param.estadoPopUp}">
		<c:set var="estadoPopUp" value="${param.estadoPopUp}"/>
	</c:if>
  </c:otherwise>
</c:choose>

<html class="no-js">
	<head>
		<title>Bci.cl/personas</title>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="description" content="">
		<meta name="viewport" content="width=device-width,initial-scale=1">
		<link href="/bcinetwls/common/css/frames.css" rel="stylesheet" type="text/css">
		<script type="text/javascript" src="/bcinetwls/common/js/multipass.js"></script>
	</head>

	<body onLoad="javascript:inicializar();" onUnload="salida('<c:out value="${urlBase}"/>/pagoswls/PagueDirecto?paso=2', '<c:out value="${urlBase}"/>/bcinetwls/paguedirecto/error_cancelar.jsp');">
		
		<div class="entrust_container opciones">
			<p class="elige"><fmt:message key="entrustToken.contenido.tituloMultipass"/></p>
			
			<form name="PagueDirectoForm" id="form" action="/servlet/paguedirecto" method="post" onSubmit="return aceptar()">
				<input type="hidden" name="canal" value="entrustToken">
				<c:if test="${estadoPopUp == 'false'}">
					  <INPUT TYPE=HIDDEN NAME="estadoPopUp" VALUE="<c:out value="estadoPopUp" />">
				</c:if>
				
				<div class="multipass">
					<div class="img"></div>
					<div class="ingreso">
						<div class="label" id="label"><fmt:message key="entrustToken.contenido.subtitulo"/></div>
						<input type="password" id="pass" name="claveToken" placeholder="<fmt:message key="entrustToken.contenido.placeholder"/>" maxlength="6">
						<div id="ver"></div>
					</div>
					<div class="paso"><fmt:message key="entrustToken.contenido.cantCaracteres"/></div>
				</div>
				<button id="aceptar" class="aceptar disabled" disabled><fmt:message key="entrustToken.boton.aceptar"/></button>

				<div id="loading"></div>

			</form>
		</div>
	</body>
</html>

<%--
/**************************************************************************************************************************
* Archivo:     autenticacionEntrustToken.jsp
* Descripci�n: Se utiliza para la autenticaci�n a trav�s del dispositivo fisico de Entrust.
* Path:        /bcinetwls/paguedirecto/entrusttoken
* " Todos los derechos reservados por Banco de Cr�dito e Inversiones."              
* Historia Cambios
* -------------------------
* versi�n       fecha         autor                                                       cambios
* =======   ==========  ==========================================================        =================================
* 1.0       02-08-2017  Marcelo Fuentes (SEnTRA) - Ricardo Carrasco (Ing. Soft. BCI)      versi�n inicial. Ser� modificada.
*
**************************************************************************************************************************/
--%>
