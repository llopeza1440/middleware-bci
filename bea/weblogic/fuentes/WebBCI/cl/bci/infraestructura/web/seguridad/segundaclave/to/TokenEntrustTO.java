package cl.bci.infraestructura.web.seguridad.segundaclave.to;

import java.io.Serializable;
import java.util.Date;

import cl.bci.infraestructura.web.seguridad.segundaclave.to.UsuarioSegundaClaveTO;

/**
 * TokenEntrustTO
 * <br>
 * Clase transportadora que representa la informacion de un token entrust, ya sea hard o softoken. 
 * Y tambi�n representa la informaci�n de la tabla TANDEM TBPSC02.
 * <br>
 * Registro de versiones:
 * 
 * <ul>
 *   <li>1.0 10/07/2017 Jaime Suazo (Sentra) - Ricardo Carrasco (Ing. Soft. BCI) : versi�n inicial.</li>
 * </ul>
 * <p>
 * <b>Todos los derechos reservados por Banco de Cr�dito e Inversiones.</b>
 * </p>
 */
public class TokenEntrustTO implements Serializable {

	/**
	 * numero serial de la clase. 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * codigo respuesta WS entrust.
	 */
	private int codigoRespuestaWs = 0;
	
	/**
	 * descripcion respuesta WS entrust.
	 */
	private String descripcionRespuestaWs = null;
	
	/**
	 * estado token - softoken. Importante, para entrust, dicho estado debe venir con mayusculas.
	 * ej: CURRENT, PENDING, HOLD, CANCEL. 
	 */
	private String estado = null;
		
	/**
	 * estado despliegue token - softoken. 
	 * ej: CURRENT, PENDING, HOLD, CANCEL. 
	 */
	private String estadoDespliegue = null;
	
	/**
	 * numero serial token - softoken.
	 */
	private String serialNumber = null;
	
	/**
	 * Atributo relacionado a campo PSC_FEC_MOD de tabla TANDEM.
	 */
	private Date fechaModificacion;
	
	/**
	 * tipo de dispositivo token - softoken. (HTK, SFT).
	 */
	private String tipoDispositivo = null;
	
	/**
	 * nombre dispositivo softoken.
	 */
	private String nombreDispositivo = null;
	
	/**
	 * datos de cliente entrust.
	 */
	private UsuarioSegundaClaveTO usuarioSegundaClave = null;
	
	/**
	 * Grupo del token.
	 */
	private String grupoToken = null;


	public int getCodigoRespuestaWs() {
		return codigoRespuestaWs;
	}

	public void setCodigoRespuestaWs(int codigoRespuestaWs) {
		this.codigoRespuestaWs = codigoRespuestaWs;
	}

	public String getDescripcionRespuestaWs() {
		return descripcionRespuestaWs;
	}

	public void setDescripcionRespuestaWs(String descripcionRespuestaWs) {
		this.descripcionRespuestaWs = descripcionRespuestaWs;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

	public String getTipoDispositivo() {
		return tipoDispositivo;
	}

	public void setTipoDispositivo(String tipoDispositivo) {
		this.tipoDispositivo = tipoDispositivo;
	}
	
	public Date getFechaModificacion() {
		return fechaModificacion;
	}

	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	public String getNombreDispositivo() {
		return nombreDispositivo;
	}

	public void setNombreDispositivo(String nombreDispositivo) {
		this.nombreDispositivo = nombreDispositivo;
	}

	public UsuarioSegundaClaveTO getUsuarioSegundaClave() {
		return usuarioSegundaClave;
	}

	public void setUsuarioSegundaClave(UsuarioSegundaClaveTO usuarioSegundaClave) {
		this.usuarioSegundaClave = usuarioSegundaClave;
	}

	public String getGrupoToken() {
		return grupoToken;
	}

	public void setGrupoToken(String grupoToken) {
		this.grupoToken = grupoToken;
	}

	public String getEstadoDespliegue() {
		return estadoDespliegue;
	}

	public void setEstadoDespliegue(String estadoDespliegue) {
		this.estadoDespliegue = estadoDespliegue;
	}

	public String toString() {
		StringBuffer builder = new StringBuffer();
		builder.append("TokenEntrustTO [codigoRespuestaWs=");
		builder.append(codigoRespuestaWs);
		builder.append(", descripcionRespuestaWs=");
		builder.append(descripcionRespuestaWs);
		builder.append(", estado=");
		builder.append(estado);
		builder.append(", estadoDespliegue=");
		builder.append(estadoDespliegue);
		builder.append(", serialNumber=");
		builder.append(serialNumber);
		builder.append(", fechaModificacion=");
		builder.append(fechaModificacion);
		builder.append(", tipoDispositivo=");
		builder.append(tipoDispositivo);
		builder.append(", nombreDispositivo=");
		builder.append(nombreDispositivo);
		builder.append(", grupoToken=");
		builder.append(grupoToken);		
		builder.append(", usuarioSegundaClave=");
		builder.append(usuarioSegundaClave);
		builder.append("]");
		return builder.toString();
	}
	
}
