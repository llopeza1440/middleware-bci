package wcorp.model.seguridad;

import java.io.Serializable;

import wcorp.util.RUTUtil;

/**
 * <b>Usuario SegundaClave</b>
 * <p>
 * Interface para segunda clave
 * <p>
 * 
 * Registro de versiones:<ul>
 * 
 * <li>1.0 (30/04/2013, Mauricio Palma. (Orand)): version inicial</li>
 * <li>2.0  10/07/2017 Jaime Suazo (Sentra) - Ricardo Carrasco (Ing. Soft. BCI) : Por proyecto cambio plataforma de autenticacion
 *          se agregan los atributos <code>{@link #grupoClienteEntrust}</code>, <code>{@link #nombreCliente}</code>, 
 *          se actualiza {@link #toString()} con los nuevos atributos, adem�s se agrega el metodo {@link #getRutEntrust()} 
 *          para obtener RUT formato producto Entrust.</li>  
 * </ul>
 * 
 * <p><b>Todos los derechos reservados por Banco de Cr�dito e Inversiones.</b></p>
 */
public class UsuarioSegundaClaveTO implements Serializable {
    
    /**
     * identificador de rut.
     */
    public static final int TIPO = 1;
    
    /**
     * identificador.
     */
    public static final int ID = 2;

    /**
     * N�mero de versi�n utilizado durante la serializaci�n.
     */
    private static final long serialVersionUID = 2L;
        
    /**
     * id de usuario.
     */
    private String usuarioId;
    
    /**
     * id del dominio.
     */
    private String dominioId;
    
    /**
     * id del canal.
     */
    private String canalId;

    /**
     * tipo.
     */
    private int tipo;
    
    /**
     * rut.
     */
    private long rut;
    
    /**
     * dv.
     */
    private char dv;
    
	/**
	 * grupo entrust donde se encuentra asociado el cliente.
	 */
	private String grupoClienteEntrust = null;

	/**
	 * Nombre del cliente.
	 */
	private String nombreCliente = null;
    
    /**
     * Constructor de la clase.
     * 
     * @param usuarioId identificador del usuario.
     * @param dominioId identificador del dominio.
     * @param canalId identificador del canal.
     */
    public UsuarioSegundaClaveTO(String usuarioId, String dominioId, String canalId) {
        this.usuarioId = usuarioId;
        this.dominioId = dominioId;
        this.canalId = canalId;
        
        if (RUTUtil.validaRut(usuarioId)) {
            tipo = TIPO;
            rut = RUTUtil.extraeRUT(usuarioId);
            dv = RUTUtil.extraeDigitoVerificador(usuarioId);
        }
        else {
            tipo = ID;
        }
    }

    public String getUsuarioId() {
        return usuarioId;
    }

    public String getDominioId() {
        return dominioId;
    }

    public String getCanalId() {
        return canalId;
    }

    public int getTipo() {
        return tipo;
    }

    public long getRut() {
        return rut;
    }

    public char getDv() {
        return dv;
    }

    public String getGrupoClienteEntrust() {
		return grupoClienteEntrust;
	}

	public void setGrupoClienteEntrust(String grupoClienteEntrust) {
		this.grupoClienteEntrust = grupoClienteEntrust;
	}

	public String getNombreCliente() {
		return nombreCliente;
	}

	public void setNombreCliente(String nombreCliente) {
		this.nombreCliente = nombreCliente;
	}

	public String getRutEntrust() {
		return this.getGrupoClienteEntrust() + "/" + this.getRut() + "-" + String.valueOf(this.getDv());
	}
	
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("UsuarioSegundaClaveTO: [usuarioId=").append(usuarioId);
		sb.append("], dominioId=[").append(dominioId);
		sb.append("], canalId=[").append(canalId);
		sb.append("], tipo=[").append(tipo);
		sb.append("], rut=[").append(rut);
		sb.append("], dv=[").append(dv);
		sb.append("], grupoClienteEntrust=[").append(grupoClienteEntrust);
		sb.append("], nombreCliente=[").append(nombreCliente);
		sb.append("]");	
		return sb.toString();
	}

}
