package wcorp.model.seguridad;

import java.io.Serializable;

/**
 * 
 * <b>RepresentacionAlfanumericaSegundaClaveTO</b>
 * <p>
 * Implementa la interfaz RepresentacionSegundaClave.
 * </p>
 * 
 * <p>Registro de versiones : </p>
 * 
 * <ul>
 * <li> 1.0 28/04/2013, Victor Hugo Enero (Orand): version inicial</li>
 * <li> 2.0 10/07/2017 Luis L�pez Alamos (SEnTRA) - Ricardo Carrasco Caceres (Ing. Soft. BCI): Se implementa serializaci�n</li>
 * </ul>
 * 
 * <p><b>Todos los derechos reservados por Banco de Cr�dito e Inversiones.</b></p>
 * 
 */
public class RepresentacionAlfanumericaSegundaClaveTO implements RepresentacionSegundaClave, Serializable {

    /**
     * constante para la serializaci�n.
     */
    private static final long serialVersionUID = 1L;
	
    /**
     * segunda clave.
     */
    private String segundaClave;

    /**
     * constructor de la clase.
     * 
     * @param segundaClave segunda clave.
     */
    public RepresentacionAlfanumericaSegundaClaveTO(String segundaClave) {
        this.segundaClave = segundaClave;
    }

    /**
     * tipo de clave.
     * 
     * @return int que representa el tipo de clave.
     */
    public int tipoDeClave() {
        return RepresentacionSegundaClave.CLAVE_ALFANUMERICA;
    }

    /**
     * obtiene clave numerica.
     * 
     * @return long que representa la clave numerica.
     */
    public long getClaveNumerica() {
        throw new UnsupportedOperationException(
                "RepresentacionAlfanumericaSegundaClaveTO soporta solo clave alfanumerica");
    }

    /**
     * obtiene clave binaria.
     * 
     * @return byte[] que representa la clave binaria.
     */
    public byte[] getClaveBinaria() {
        throw new UnsupportedOperationException(
            "RepresentacionAlfanumericaSegundaClaveTO soporta solo clave alfanumerica");
    }

    public String getClaveAlfanumerica() {
        return segundaClave;
    }

    public int getTipoDeClave() {
        return RepresentacionSegundaClave.CLAVE_ALFANUMERICA;
    }

    /**
     * setea el tipo de clave.
     * 
     * @param tipoDeClave tipo de clave.
     */
    public void setTipoDeClave(int tipoDeClave) {
        if (tipoDeClave != RepresentacionSegundaClave.CLAVE_ALFANUMERICA) {
            throw new UnsupportedOperationException(
                "RepresentacionAlfanumericaSegundaClaveTO soporta solo clave alfanumerica");
        }
    }

    /**
     * setea la clave numerica.
     * 
     * @param claveNumerica clave numerica.
     */
    public void setClaveNumerica(long claveNumerica) {
        throw new UnsupportedOperationException(
            "RepresentacionAlfanumericaSegundaClaveTO soporta solo clave alfanumerica");
    }

    /**
     * setea la clave binaria.
     * 
     * @param claveBinaria clave binaria.
     */
    public void setClaveBinaria(byte[] claveBinaria) {
        throw new UnsupportedOperationException(
            "RepresentacionAlfanumericaSegundaClaveTO soporta solo clave alfanumerica");
    }

    public void setClaveAlfanumerica(String claveAlfanumerica) {
        this.segundaClave = claveAlfanumerica;
    }
}
