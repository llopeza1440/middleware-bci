package wcorp.model.seguridad.to;

import java.io.Serializable;
import java.util.Date;

/**
 * OtpEntrustTO
 * <br>
 * Clase transportadora que representa la informacion de un OtpEntrustTO entrust.
 * <br>
 * Registro de versiones:
 * 
 * <ul>
 *   <li>1.0 10/07/2017 Sergio Flores (Sentra) - Ricardo Carrasco (Ing. Soft. BCI) : versi�n inicial.</li>
 * </ul>
 * <p>
 * <b>Todos los derechos reservados por Banco de Cr�dito e Inversiones.</b>
 * </p>
 */
public class OtpEntrustTO implements Serializable {

	/**
	 * numero serial de la clase. 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * codigo respuesta WS entrust.
	 */
	private String userId;
	/**
	 * codigo respuesta WS entrust.
	 */
	private String group;
	/**
	 * codigo respuesta WS entrust.
	 */
	private String repositoryId;
	/**
	 * codigo respuesta WS entrust.
	 */   
	private String otp;
	/**
	 * codigo respuesta WS entrust.
	 */       
	private Date createDate;
    
	/**
	 * codigo respuesta WS entrust.
	 */    
	private Date expireDate;
	/**
	 * codigo respuesta WS entrust.
	 */    
	private String transactionId;
	
	/**
	 * codigo respuesta WS entrust.
	 */
	private int codigoRespuestaWs = 0;
	
	/**
	 * descripcion respuesta WS entrust.
	 */
	private String descripcionRespuestaWs = null;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getGroup() {
		return group;
	}

	public void setGroup(String group) {
		this.group = group;
	}

	public String getRepositoryId() {
		return repositoryId;
	}

	public void setRepositoryId(String repositoryId) {
		this.repositoryId = repositoryId;
	}

	public String getOtp() {
		return otp;
	}

	public void setOtp(String otp) {
		this.otp = otp;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getExpireDate() {
		return expireDate;
	}

	public void setExpireDate(Date expireDate) {
		this.expireDate = expireDate;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public int getCodigoRespuestaWs() {
		return codigoRespuestaWs;
	}

	public void setCodigoRespuestaWs(int codigoRespuestaWs) {
		this.codigoRespuestaWs = codigoRespuestaWs;
	}

	public String getDescripcionRespuestaWs() {
		return descripcionRespuestaWs;
	}

	public void setDescripcionRespuestaWs(String descripcionRespuestaWs) {
		this.descripcionRespuestaWs = descripcionRespuestaWs;
	}

	public String toString() {
		StringBuffer buffer = new StringBuffer();
		buffer.append("OtpEntrustTO [userId=");
		buffer.append(userId);
		buffer.append(", group=");
		buffer.append(group);
		buffer.append(", repositoryId=");
		buffer.append(repositoryId);
		buffer.append(", otp=");
		buffer.append(otp);
		buffer.append(", createDate=");
		buffer.append(createDate);
		buffer.append(", expireDate=");
		buffer.append(expireDate);
		buffer.append(", transactionId=");
		buffer.append(transactionId);
		buffer.append(", codigoRespuestaWs=");
		buffer.append(codigoRespuestaWs);
		buffer.append(", descripcionRespuestaWs=");
		buffer.append(descripcionRespuestaWs);
		buffer.append("]");
		return buffer.toString();
	}
}
