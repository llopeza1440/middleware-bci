package cl.bci.infraestructura.web.seguridad.segundaclave.ws.to;

import java.io.Serializable;
import java.util.Date;

/**
 * DetalleTarjetaWSTO
 * <br>
 * Clase transportadora que representa la informacion de una tarjeta tdd entrust.
 * <br>
 * Registro de versiones:
 * 
 * <ul>
 *   <li>1.0 10/07/2017 Sergio Flores (Sentra) - Ricardo Carrasco (Ing. Soft. BCI) : versi�n inicial.</li>
 * </ul>
 * <p>
 * <b>Todos los derechos reservados por Banco de Cr�dito e Inversiones.</b>
 * </p>
 */
public class DetalleTarjetaWSTO implements Serializable{

    /** 
    * N�mero de versi�n utilizado durante la serializaci�n. 
    */
   private static final long serialVersionUID = 1L;


    /**
     * n�mero tarjeta.
     */
   private String numeroTarjeta;

    /**
     * Estado.
     */
   private String estado;

    /**
     * n�mero de la cuenta corriente.
     */
   private String numCuentaCorriente;

    /**
     * Feha de entrega.
     */
   private Date fechaInicio;

    /**
     * Fecha de expiracion.
     */
   private Date fecVencimiento;

	public String getNumeroTarjeta() {
		return numeroTarjeta;
	}

	public void setNumeroTarjeta(String numeroTarjeta) {
		this.numeroTarjeta = numeroTarjeta;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getNumCuentaCorriente() {
		return numCuentaCorriente;
	}

	public void setNumCuentaCorriente(String numCuentaCorriente) {
		this.numCuentaCorriente = numCuentaCorriente;
	}

	public Date getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public Date getFecVencimiento() {
		return fecVencimiento;
	}

	public void setFecVencimiento(Date fecVencimiento) {
		this.fecVencimiento = fecVencimiento;
	}

	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("DetalleTarjetaWSTO [numeroTarjeta=");
		sb.append(numeroTarjeta);
		sb.append(", estado=");
		sb.append(estado);
		sb.append(", numCuentaCorriente=");
		sb.append(numCuentaCorriente);
		sb.append(", fechaInicio=");
		sb.append(fechaInicio);
		sb.append(", fecVencimiento=");
		sb.append(fecVencimiento);
		sb.append("]");
		return sb.toString();
	}

  
}
