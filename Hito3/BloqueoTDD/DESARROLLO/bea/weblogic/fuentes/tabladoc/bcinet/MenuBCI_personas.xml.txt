Este archivo permite generar el men� en BCI cl.

opcion
------
Opciones de men�.

titulo
------
Nombre con el que aparece en el listado, el usuario puede hacer clic en �l.

url
---
URL a la cual se despacha al hacer clic en la descripci�n.

permisos
--------
Permisos que necesita el cliente para poder acceder a la opcion


[REGISTRODEVERSIONES]
==========================================

. Desconocido: version inicial.
. 29/05/2012 Gonzalo Bustamante V.(Sermaluc): se modifican las opciones del menu de accesos directos para Seguridad y Emergencias
. 27/08/2013 Juan Pablo Vega (TINet): Se modifican las url de las opciones
  "trans_fondos", "transf_prog" y "mis_dest". Ademas se corrige
  formato de la tabla
. 27/06/2014 Juan Pablo Vega (TINet): Se modifica la url de la nueva pagina de transferencias fondos.
. 30/06/2015 Oscar Nahuelpan (SEnTRA)- Pablo Paredes (Ing. Software BCI): Se agrega url para visualizacion de contratos cuenta prima pendientes (consultaFirma.jsf).
. 10/07/2015 Nelson ALvarado (SEnTRA)- CLaudia Lopez (Ing. Soft. BCI): Se agrega url para visualizacion de transferencia de fondos nuevo (transferencia-web.jsf).
. 25/11/2015 Felipe Briones M. (SEnTRA) - Marcelo Innocenti D. (Ing. Soft. BCI): Se realizan cambios en el menu:
.        "SMS Bci" cambia a la pestaña "Pagos y Servicios".
.        "Habilitación/Deshabilitación Tarjeta Débito en el extranjero" cambia al menú "Emergencias".
.        Opción "Planes" ahora se llama "Otras Cuentas".
.        Opción "Otras Cuentas" se posiciona entre "Linea de Sobregiro" y "Datos Personales".
.        Se elimina opcion "Mis Beneficios" y su contenido se agrega a opción "Mi Cuenta".
. 17/07/2015 Nicole Sanhueza H. (Sermaluc)  - Patricia Guzman (Ing. BCI): Se modifican urls de opciones "invertir_ffmm" y "valores_cuota_ffmm" por Migración del Core de Fondos Mutuos.
. 04/02/2016 Luis Lopez A. (SEnTRA) - Claudia Lopez P. (Ing. Soft. BCI): Se realizan cambios en el menu: Se elimina "Transferir Fondos en línea" struts.
. 22/09/2015 Claudia Núñez (Sermaluc) - Patricia Guzman (Ing. BCI): Se agrega opcion CORE para los menus de FFMM y se agrega nueva ruta para las opciones de "cartola_saldos_ffmm_core" y "cartola_mov_ffmm_core". 
. 31/05/2016 Luis Garrido (Sermaluc) - Ricardo Carrasco (Ing. Soft. BCI): Se fusiona opci�n de men� de certificados tributarios y dep�sitos a plazo,
  quedando las nuevas opciones "cert_tributarios_ffmm" y "cert_tributarios_dap".
. 24/10/2016 Maximiliano Ruiz G. (Ing. Soft. BCI): Se cambia link hacia nuevo Seguro Multiprotección.
. 31/01/2017 Luis López Alamos (SEnTRA) - Christian Moraga Roldán (Ing. Soft. BCI) : Se cambia descripción de Recarga Celular por Recarga Celular/TV.
==========================================