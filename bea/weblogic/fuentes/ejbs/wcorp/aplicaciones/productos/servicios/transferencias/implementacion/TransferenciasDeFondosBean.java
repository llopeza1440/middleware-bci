package wcorp.aplicaciones.productos.servicios.transferencias.implementacion;

import java.rmi.RemoteException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.SessionBean;
import javax.ejb.SessionContext;
import javax.naming.NamingException;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import wcorp.aplicaciones.atenciondecliente.ejecutivo.agenda.dao.ClienteDAOIbatis;
import wcorp.aplicaciones.atenciondecliente.ejecutivo.agenda.to.DatosBasicosDelClienteTO;
import wcorp.aplicaciones.productos.servicios.transferencias.detecciondefraudes.EvaluacionRiesgoTTFF;
import wcorp.aplicaciones.productos.servicios.transferencias.detecciondefraudes.EvaluacionRiesgoTTFFHome;
import wcorp.aplicaciones.productos.servicios.transferencias.detecciondefraudes.helper.EvaluacionRiesgoTTFFHelper;
import wcorp.aplicaciones.productos.servicios.transferencias.detecciondefraudes.to.ResultadoEvaluacionTTFFTO;
import wcorp.aplicaciones.productos.servicios.transferencias.implementacion.helper.MessageAuthenticationCodeHelper;
import wcorp.aplicaciones.productos.servicios.transferencias.nominas.vo.DetalleTransferenciaVO;
import wcorp.aplicaciones.productos.servicios.transferencias.nominas.vo.FiltroConsultarTransferenciasVO;
import wcorp.aplicaciones.productos.servicios.transferencias.to.DatosInicializacionTransferenciasTO;
import wcorp.aplicaciones.productos.servicios.transferencias.to.DatosTransferenciaTO;
import wcorp.aplicaciones.productos.servicios.transferencias.to.EGift;
import wcorp.aplicaciones.productos.servicios.transferencias.to.ResultadoValidacionTTFFTO;
import wcorp.aplicaciones.productos.servicios.transferencias.to.TransferenciaATercerosTO;
import wcorp.aplicaciones.productos.servicios.transferencias.util.EvaluadorTTFF;
import wcorp.aplicaciones.productos.servicios.transferencias.util.GeneradorComprobanteTransferenciaDeFondos;
import wcorp.bprocess.cuentas.TransferenciaException;
import wcorp.bprocess.cuentas.dao.ConsultaTransferenciasProgramadasDAO;
import wcorp.bprocess.cuentas.dao.ConsultaTransferenciasRecibidasDAO;
import wcorp.bprocess.cuentas.dao.CuentasDAO;
import wcorp.bprocess.cuentas.dao.DAOFactory;
import wcorp.bprocess.cuentas.dao.TTFFDAO;
import wcorp.bprocess.cuentas.dao.TransferenciasFondosDAO;
import wcorp.bprocess.cuentas.utils.comparador.ComparadorAliasTTFFD;
import wcorp.bprocess.cuentas.vo.AliasTTFFDVO;
import wcorp.bprocess.cuentas.vo.CCADirectaVO;
import wcorp.bprocess.cuentas.vo.FiltroConsultaTransferenciasRecibidasVO;
import wcorp.bprocess.cuentas.vo.TipoMandatoVO;
import wcorp.bprocess.cuentas.vo.TransferenciaDuplicadaVO;
import wcorp.bprocess.cuentas.vo.TransferenciasEntradaVO;
import wcorp.bprocess.cuentas.vo.TransferenciasRecibidasVO;
import wcorp.bprocess.cuentas.vo.TransferenciasSalidaVO;
import wcorp.env.BusinessException;
import wcorp.env.WCorpConfig;
import wcorp.model.actores.Cliente;
import wcorp.model.productos.CuentaAhorro;
import wcorp.model.productos.CuentaCorriente;
import wcorp.model.programacion.ProgramacionPK;
import wcorp.serv.ahorros.SaldoAhorro;
import wcorp.serv.bciexpress.AgregaTransfEncabDetalle;
import wcorp.serv.bciexpress.BCIExpressException;
import wcorp.serv.bciexpress.ResultAgregaTransferenciaEncabezadoDetalle;
import wcorp.serv.bciexpress.ResultCambiaEstadoTransferencia;
import wcorp.serv.clientes.DatosBasicosCliente;
import wcorp.serv.cuentas.CuentasException;
import wcorp.serv.cuentas.DetalleTransferencia;
import wcorp.serv.cuentas.LineaSobregiro;
import wcorp.serv.cuentas.RegistroTransferencia;
import wcorp.serv.cuentas.SaldoCtaCte;
import wcorp.serv.cuentas.ServiciosCuentas;
import wcorp.serv.cuentas.ServiciosCuentasHome;
import wcorp.serv.cuentas.TransferenciaFondos;
import wcorp.serv.programacion.ProgramacionServices;
import wcorp.serv.programacion.ProgramacionServicesHome;
import wcorp.serv.sipe.BancosAutorizados;
import wcorp.serv.sipe.ServiciosSipe;
import wcorp.serv.sipe.ServiciosSipeHome;
import wcorp.util.CCAException;
import wcorp.util.EnhancedServiceLocator;
import wcorp.util.EnhancedServiceLocatorException;
import wcorp.util.EnvioDeCorreo;
import wcorp.util.ErroresUtil;
import wcorp.util.FechasUtil;
import wcorp.util.Formatting;
import wcorp.util.GeneralException;
import wcorp.util.SistemaException;
import wcorp.util.StringUtil;
import wcorp.util.TablaValores;
import wcorp.util.TextosUtil;
import wcorp.util.com.JOLTPoolConnection;
import wcorp.util.com.TuxedoException;
import wcorp.util.mime.GeneradorDeComponentesMIME;
import wcorp.util.switchappsensibles.SwicthAplicacionesSensiblesHelper;
import wcorp.util.workstation.ServicioNoDisponibleException;
import wcorp.util.xml.Dato;
import wcorp.util.xml.XFMLBuffer;

import bea.jolt.pool.ApplicationException;
import bea.jolt.pool.DataSet;
import bea.jolt.pool.Result;
import bea.jolt.pool.servlet.ServletSessionPool;

import cl.bci.mensajeria.sdp.conector.ConectorServicioEnvioSDP;
import cl.bci.middleware.aplicacion.persistencia.ConectorServicioDePersistenciaDeDatos;

import com.schema.util.FileSettings;
import com.schema.util.dao.TuxedoUtils;

/**
 * Bean de Transferencias de Fondos (Negocio). El proceso actual de transferencias en l�nea utiliza un componente
 * llamado {@link wcorp.bprocess.cuentas.TransferenciasFondosBean#} el cual contiene la l�gica de negocio. Este
 * componente corresponde al tipo Bean de sesi�n con estado (Stateful), es decir, cada cliente implica la creaci�n
 * de un objeto (create()). N clientes implican N Beans de sesi�n con estado provocando los siguientes problemas:
 * <li> Memory leak (agotamiento de la memoria disponible), alrededor de 1000 instancias.
 * <li> Timeout, Esto se produce ya que no se pueden crear m�s instancias del mismo.
 * <p>
 * Por lo descrito anteriormente se cre� un nuevo Ejb de sesi�n sin estado (Stateless), cuyo fin es que el servidor
 * mantenga un pool de objetos disponibles, sea capaz de reutilizar objetos para m�ltiples clientes de forma
 * sucesiva o sincronizada. Se migrar�n (en una primera etapa) los m�todos de Transferencias en L�nea desde el Ejb
 * Stateful {@link wcorp.bprocess.cuentas.TransferenciasFondosBean#} al Ejb Stateless
 * {@link wcorp.aplicaciones.productos.servicios.transferencias.implementacion.TransferenciasDeFondosBean#}. Las
 * pr�ximas etapas a abordar son:
 * <li> Transferencias programadas, transferencias en l�nea de empresas.
 * <li> Tarjetas.
 * <li> L�nea sobregiro.
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 25/03/2009, Carlos Cerda Iglesias (SEnTRA): Versi�n Inicial.
 * <li>1.1 25/05/2009, Marianela Sabando M. (BCI): Se valida largo de comentario, nombre destino y e-mail de
 * destino en m�todo
 * {@link #ingresaTransferencia(Cliente, String, String, double, Date, String, Date, String, String, long, String,
 * String, String, String, String, String, String, String, String, String, String)}.
 * <li>1.2 01/06/2009, Carlos Cerda I. (SEnTRA): Se agrega m�todo:
 * {@link #registraTransaccionPendiente(Cliente, String, Exception, String, String, String, double, String, int)},
 * para reintentar transacciones sensibles (abonos, cargos, reversas, etc.) de forma batch y se agrega la llamada a
 * este, en el m�todo
 * {@link #transCtaCteOtroBancoLineaPer(String, String, String, String, double, String, String, long, char,
 * Cliente, String, String, String, String, String, String, String, String)}.
 * <li>1.4 01/06/2009, Carlos Cerda I.  (SEnTRA): Se realizan los siguientes cambios al ejb:
 * <ul>
 * <li>Se agrega m�todo:
 * {@link #registraTransaccionPendiente(long, char, String, String, double, String, String, Exception)}, para
 * reintentar transacciones sensibles (abonos, cargos, reversas, etc.) de forma batch y se agrega la llamada a
 * este, en los m�todos
 * {@link #transferenciaCtaCteOtroBancoEnLineaEmpresa(String, String, String, String, String, long, char, String,
 * String, String, String, String, String, String, Cliente, String)} y
 * {@link #transCtaCteOtroBancoLineaPer(String, String, String, String, double, String, String, long, char,
 *  Cliente, String, String, String, String, String, String, String, String)}.
 * 		<li>Se estandarizan logs seg�n normativa BCI.
 * 		<li>Se cambia instanciacion de los ejb's de JNDIConfig a EnhancedServiceLocator y adem�s se a�ade instancia al
 * ejb: {@link wcorp.serv.seguridad.ServiciosSeguridadBean}.
 * 		<li>Se rescata desde tabla de par�metros el c�digo del banco BCI.
 * <li>Se agrega l�gica de la autenticaci�n de los mensajes de transferencias de fondos MAC (Message
 * Authentication Code), con el fin de aumentar el nivel de seguridad e integridad en los mensajes de las
 * transacciones emitidas e intercambiadas entre el Operador y los bancos. Cualquier alteraci�n implica un cambio
 * en el valor de la MAC y por consecuencia genera un rechazo del mensaje. Esto se raliza a trav�s del helper
 * {@link wcorp.aplicaciones.productos.servicios.transferencias.implementacion.
 * helper.MessageAuthenticationCodeHelper#}
 * </ul>
 * <li>1.5 22/02/2010, Carlos Cerda I. (Orand): Se agrega par�metro traceNumber a los siguientes m�todos:
 * {@link #realizarTransferenciaCtaCteHaciaCtaCte(Cliente, String, String, String, double)},
 * {@link #registraTransaccionPendiente(Cliente, String, Exception, String, String, String, double, String, String,
 *  String, int)},
 * {@link #realizaReversaCargo(Cliente, String, String, String, double)} y
 * {@link #realizaReversaAbono(Cliente, String, String, double)}. El trace number es un identificador �nico de
 * TTFF que permite a todos los bancos y a la CCA poder individualizar una operaci�n. Cada banco genera este n�mero
 * (trace number) por cada transacci�n y no puede repetirse dentro de un periodo de alrededor de 48 hrs. Este
 * parametro se agrega con la finalidad de poder hacer las reversar que correspondan, ya que en el funcionamiento
 * actual, para las reversas, se consideran la fecha, el n�mero de cuenta y el monto, cuyos valores son muy f�ciles
 * de repetir, en especial cuando el cliente intenta en reiteradas ocaciones cuando la transacci�n falla, generando
 * as� reversas que no corresponden.
 * <li>1.6 25/03/2010, Jes�s Orellana. (Orand): Actualmente cuando se realiza una TTFF se hace un cargo en la
 * cuenta del cliente y luego un abono a la cuenta saco. Si algo resulta mal en la transacci�n como por ejemplo: el
 * banco de destino no est� disponible, la cuenta est� incorrecta, no existe el rut de destino, etc. se hace la
 * reversa en la cuenta saco y la reversa en la cuenta del cliente. La cuenta saco se utiliza para efectos
 * contables, es decir, dejar "cuadrado" en l�nea el movimiento que se realiza sobre la cuenta del cliente. Sin
 * embargo, como es una sola cuenta para todas las TTFF est� colapsando en fechas de gran demanda de la aplicaci�n
 * afectando toda la plataforma t�ndem ya que el disco 14 (en donde est� alojada esta cuenta) comienza a alarmar
 * por uso de CPU. Por esta raz�n lo que estamos haciendo ahora es sacar la cuenta saco del proceso y reemplazarla
 * por una cuenta contable que no tenga uno a uno los movimientos sino un gran abono al final del d�a. Para esto
 * utilizaremos un evento SEC que parametriza contabilidad y que mueve sobre la nueva cuenta contable. Tambi�n
 * funcionan bajo este modelo: las TTFF recibidas, los pagos en l�nea, etc..en resumen se hace un cargo "cojo" y al
 * final del d�a un abono por el total. La transacci�n de transferencia se har� en l�nea, solo la contabilidad se
 * har� al final del d�a tal como se hace para los dem�s productos. Contabilidad no tiene ninguna ventaja con tener
 * los movimientos uno a uno en esta cuenta saco ya que al final del d�a igual los suman para hacer la cuadratura
 * del d�a contable. Para la l�nea es una ventaja sacar esta cuenta ya que descongestionar� el proceso y nos
 * permitir� tener un mejor servicio de cara a cliente. Para conseguir lo anteriormente descrito se realizan las
 * siguientes modificaciones:
 * 									<li> Se saca la obtenci�n de la cuenta saco desde la tabla de par�metros en los m�todos:
 * {@link #transCtaCteOtroBancoLineaPer(String, String, String, String, double, String, String, long, char,
 * Cliente, String, String, String, String, String, String, String, String)}
 * y
 * {@link #transferenciaCtaCteOtroBancoEnLineaEmpresa(String, String, String, String, String, long, char, String,
 * String, String, String, String, String, String, Cliente, String)}.
 * <li> Se saca la cuenta saco como par�metro de entrada en la llamada al tuxedo CtaCteTransfGen en el m�todo:
 * {@link #realizarTransferenciaCtaCteHaciaCtaCte(Cliente, String, String, String, double)}.
 * <li> Se modifica la forma de recibir la respuesta ya que ahora no retorna la cuenta de destino como respuesta a
 * la consulta sino la fecha contable de la transacci�n en lo m�todos:
 * {@link #realizarTransferenciaCtaCteHaciaCtaCte(Cliente, String, String, String, double)} y
 * {@link #realizaReversaCargo(Cliente, String, String, String, double)}. El atributo fechaContable corresponde a
 * la nueva salida del programa tandem. Ahora se har� un cargo al cliente y es necesario saber la fecha en la cual
 * se carg� la cuenta y logearla ya que es �til para hacer seguimiento a ciertos casos de calidad de servicio.
 *									<li> Se elimina el m�todo realizaReversaAbono() y todas las invocaciones sobre �l.
 * <li>1.7 13/05/2010, Carlos Cerda I. (Orand): Se agrega nem�nico al DataSet que lleva los campos al Tandem en
 * los m�todos {@link #realizaReversaCargo(Cliente, String, String, String, double)} y
 * {@link #realizarTransferenciaCtaCteHaciaCtaCte(Cliente, String, String, String, double)}. Se agrega nem�nico a
 * la llamada al m�todo:
 * {@link wcorp.serv.cuentas.ServiciosCuentasBean#registraTransaccionesPendientes(long, char, String, String,
 * String, String, String, String, String, String, String, String, int, char, String, String, String)}
 * para el m�todo
 * {@link #registraTransaccionPendiente(Cliente, String, Exception, String, String, double, String, String, String,
 *  int)}.
 * Este nem�nico es necesario para que la contabilidad pueda ejecutar el evento SEC (batch) y hacer las cuadraturas
 * del producto.
 * <li>1.8 (25/10/2009, Pablo Carre�o (TINet)): Se agrega m�todo que realiza cargo en cuenta corriente del cliente
 * por concepto de cierre de l�nea de emergencia, Se agrega m�todo para obtener logger que se encarga de retornar
 *                                                una instancia del objeto de logs.
 * <li>1.9 06/07/2010 Carlos Cerda Iglesias (Orand Innovaci�n): Se a�ade m�todo
 * {@link #confirmarAliasTTFF(long, String)}, el cual confirma que el N�mero de verificaci�n ingresado por el
 * cliente es id�ntico al almacenado por el banco, esto para autorizaci�n del alias (cuenta frecuente).
 * <li>1.10 (03/08/2010, Marcelo Fuentes H. (SEnTRA)): Se agregan los siguientes m�todos:
 * <ul>
 * <li>{@link #transferenciaCtaCteBciEnLineaEmpresa()} que se utiliza para transferir fondos desde cuenta
 * corriente hacia cuenta corriente de mismo banco BCI.</li>
 * <li>{@link #cargoCtaCtePorTransferenciaOTB()} el cual se utiliza para realizar el cargo a una Cuenta BCI
 * Empresa.</li>
 * </ul>
 * </li>
 * <li>1.11 (12/11/2010, Marcelo Fuentes H. (SEnTRA)): Se modifican los m�todos
 * {@link #transferenciaCtaCteBciEnLineaEmpresa()} y {@link #cargoCtaCtePorTransferenciaOTB()} agregando formateo
 * de par�metros.</li>
 * <li>1.12 (08/02/2011, Marcelo Fuentes H. (SEnTRA)): Se modifica el m�todo
 * {@link #cargoCtaCtePorTransferenciaOTB()}.</li>
 * <li>1.13 09/03/2011 Magdalena D�az R. (Imagemaker IT): Se crea nuevo m�todo
 * {@link #modificarEstadoAliasTTFF(long, String, char)} Este m�todo modifica el estado del alias por el pasado
 * como par�metro.
 * <li>1.14 (10/03/2011, Freddy Monsalve M. (OS.ONE), Carlos Garrido G. (OS.ONE)): Se trae m�todo {@link
 * #agregaTransferenciaEncabezadoDetalleEnLinea(TransferenciaATercerosTO)} desde
 * {@link wcorp.bprocess.cuentas.TransferenciasFondosBean) } a la implementaci�n Stateless del mismo. Se implementa
 * con un TO.</li>
 * <li>1.15 (21/06/2010, Cristian Recabarren R. (Sermaluc)): Se agregan los siguiente m�todos:
 * 					{@link #consultaTransferenciasEnviadasEmpresa(long, char, Date, String, int, int)}
 * 					{@link #consultaTransferenciasDiferidasEnviadas(long, char, Date, String, int, int)}
 * 					{@link #consultaTransferenciasEmpresaEnviadas(long, char, Date, String, int, int)}
 * <li>1.15 13/05/2010 Eduardo Mascayano (TInet): Se modifica la l�gica de los siguientes m�todos para generar el
 * comprobante de la transferencia de fondos en HTML del cliente/destinatario .
 * <ul>
 *      <li>{@link #enviaEmail(DetalleTransferencia, boolean, Cliente, String)}
 *      <li>{@link #enviaMailCliente(DetalleTransferencia, Cliente, String)}
 * </ul>
 * <li>1.16 (04/08/2011, Manuel Hern�ndez A. (OS.ONE), Carlos Garrido G. (OS.ONE)): Se agrega el m�todo
 * {@link #transferenciaCtaCteOtroBancoEnLineaEmpresaConEnvioDeMail(DetalleTransferencia)}</li>
 * <li>1.17 (08/11/2011, Richard Salazar Freddy Painenao(OS.ONE)): Se agrega el m�todo
 * {@link #traerTransferencias},{@link #traerTransferenciaTotales},{@link# traerDetalleTransferencias}</li>
 * <li>1.17 29/09/2011 Carlos Cerda I. (Orand): Se agrega par�metro 'tipoOperacion' a la firma del m�todo:
 * {@link #enviarTrfOperador(Cliente, String, String, String, String, double, String, Date, String, String, long,
 * char, String, String, long, char, String, String, String, String, String)} y se llama con este nuevo par�metro
 * desde los m�todos:
 * <ul>
 *      <li>{@link #transferenciaCtaCteOtroBancoEnLineaPersonas(DetalleTransferencia)}
 * <li>{@link #transferenciaCtaCteOtroBancoEnLineaEmpresa(String, String, String, String, String, long,  char,
 * String, String, String, String, String, String, String, Cliente, String)}
 *      <li>{@link #transferenciaCtaCteOtroBancoEnLineaEmpresaConEnvioDeMail(DetalleTransferencia)}
 * </ul>
 * Aplica para los canales Bcinet, Nova y Tbanc a transferencias hacia terceros Bci u otros bancos. No aplica para
 * las transferencias programadas.
 * <li>1.18 20/10/2011 Leonardo Quevedo M. (Orand S.A.): Se cambian los modificadores de acceso de los m�todos
 * {@link #ingresaTransferencia(Cliente cliente, String tipoCuentaOrigen, String ctaOrigen,
 * double montoTransferencia, Date fecha, String estado,
 * String tipoCuentaDestino, String ctaDestino, long rutDestino, String codigoBanco, String canalID, String
 * descripcion, String codigoRechazo, String comentario, String nombreDestino, String eMailAlternativo, String
 * eMailBeneficiario, String usuario, String tipoTrans, String codAlias)},
 * {@link #registraFinTrf(Cliente cliente, String canal, long numeroOperacion, String folio, String numero,
 * Date fecha, String estado)},
 * de private a public. Dichos m�todos se utilizar�n en el contexto del proyecto Pago TC Servipag, a fin de
 * registrar el estado inicial de un pago de TC (ingresaTransferencia), asi como tambi�n actualizar dicho registro
 * despu�s de los posibles fallos de invocaci�n de transacci�n Tandem (B2740) y el servicio monetario de Nexus
 * (registraFinTrf).</li>
 * <li>2.0 02/02/2012 Pablo Romero C�ceres (SEnTRA): Se agrega nuevo m�todo
 * {@link #consultaTransferenciasRecibidasBancoBCIPorPeriodo} clon de
 *                                                  {@link #consultaTransferenciasRecibidasBancoBCI}
 * <li>2.1 07/02/2012 Carlos Cerda I. (Orand): Se agrega l�gica para insertar una consulta de transferencias de
 * fondos al sistema StoreAndForward en el m�todo
 * {@link #transferenciaCtaCteOtroBancoEnLineaPersonas(DetalleTransferencia)}, se lleva parte de la l�gica del
 * m�todo {@link #enviarTrfOperador(Cliente, String, String, String)} al m�todo
 * {@link #construyeXFML(Cliente, String, String, String, String, double, String, Date, String, String, long, char,
 * String, String, long, char, String, String, String, String, String)}. Se cambia la forma de llamar a los m�todos
 * anteriomente mencionados en los siguientes:
 * <ul>
 *      <li>{@link #transferenciaCtaCteOtroBancoEnLineaPersonas(DetalleTransferencia)}
 * <li>{@link #transferenciaCtaCteOtroBancoEnLineaEmpresa(String, String, String, String, String, long,  char,
 * String, String, String, String, String, String, String, Cliente, String)}
 *      <li>{@link #transferenciaCtaCteOtroBancoEnLineaEmpresaConEnvioDeMail(DetalleTransferencia)}
 * </ul>
 * <li>2.2 03/04/2010, Leonardo Espinoza (Orand): Se agrega l�gica al m�todo
 * {@link #transfiereCtaCteHaciaCtaCte(String, String, String, String, double, long, char, Cliente, String, String,
 *  String, String, String, String, String, String)}
 * que hace que las transferencias dentro del mismo banco obtengan y se registren con un "Trace Number" como lo
 * hacen las transferencias a otros bancos. Adem�s, se agrega l�gica de reintentos de "Store & Forward". Tambi�n,
 * se refactoriza extrayendo c�digo duplicado en el m�todo
 * {@link #registraTransaccionPendiente(Cliente, String, Exception, String, String, String, double, String, String,
 *  String, int, boolean)}
 * creando para ello el m�todo
 * {@link #registrarTrxPendientePorSrvCuentas(Cliente, String, String, String, String, double, String, String,
 * String, int, boolean)}.
 * <li>2.3 19/04/2012 Eduardo Villagr�n M. (Imagemaker): Se agrega el m�todo
 * 			{@link #actualizarAliasTransferencias(AliasTTFFDVO)}
 * <li>2.4 09/08/2012, Jaime Pezoa N��ez (BCI): Se modifica el m�todo {@link #transferenciaCtaCteBciEnLineaEmpresa()}
 * <li>2.5 27/07/2012 Jorge Leiva A. (TINet): La javadoc general de la clase es normalizada, es eliminado un
 * import no usado y se migran los m�todos:
 * {@link #obtenerLineaSobreGiro(String, long, char, String)},
 * {@link #listaUltimasTransferencias(long rut, char dig, String canal)}
 * {@link #transfiereCtaCteHaciaTdc(String ctaOrigen, String tipoCuentaOrigen,String ctaDestino, String
 * tipoCuentaDestino, double monto, long rutDestino, char digDestino, String comentario, String nombreDestino,
 * String eMailCliente, String eMailDestino, String tipoAutenticacion, String tipoTarj, String nemonico, Cliente
 * cliente, String canal)},
 * {@link #registraIngresoTrf(String tipoCuentaOrigen, String ctaOrigen, double monto,Date fechaCargo, String
 * estado, Date fechaAbono, String tipoCuentaDestino, String ctaDestino, long rutDestino, String codigoBanco,
 * String comentario, String nombreDestino, String eMailDestino, long rut, char dig, String canal)}
 * {@link #realizarTransferenciaCtaCteHaciaTdc(String ctaOrigen, String ctaDestino, double monto, String tipoTarj,
 * String nemonico, long rut, char dig, String canal)}
 * {@link #realizarTransferenciaCtaCteHaciaTdc(String ctaOrigen, String ctaDestino, double monto, String nemonico,
 * long rut, char dig, String canal)},
 * {@link #registraFinTrf(Cliente cliente, String canal, long numeroOperacion, String folio, String numero,
 * Date fechaCargo, Date fechaAbono, String estado)},
 * {@link #transfiereAhorroHaciaCtaCte(String ctaOrigen, String ctaDestino, String tipoCuentaDestino, double monto,
 * long rutDestino, char digDestino, String comentario, String nombreDestino, String eMailCliente,
 * String eMailDestino, String tipoAutenticacion, Cliente cliente, String canal)},
 * {@link #transfiereAhorroHaciaCtaCte(String ctaOrigen, String ctaDestino, String tipoCuentaDestino, double monto,
 * long rutDestino, char digDestino, String comentario, String nombreDestino, String eMailCliente,
 * String eMailDestino, String tipoAutenticacion, boolean excesoGiros, Cliente cliente, String canal)},
 * {@link #realizarTransferenciaAhorroHaciaCtaCte(String ctaOrigen, double monto, String ctaDestino, long rut,
 * char dig, String canal)},
 * {@link #realizarTransferenciaAhorroHaciaCtaCte(String ctaOrigen, double monto, String ctaDestino,
 * boolean excesoGiros, long rut, char dig, String canal)},
 * {@link #validarSaldoAhorro(String ctaOrigen, double monto, long rut, char dig, String canal)},
 * {@link #registraFinTrf(long numeroOperacion, String folio, String numero, Date fechaCargo, Date fechaAbono,
 * String estado, long rut, char dig, String canal)},
 * {@link #transfiereTdcHaciaCtaCte(String ctaOrigen, String tipoCuentaOrigen, String ctaDestino,
 * String tipoCuentaDestino, double monto, long rutDestino, char digDestino, String comentario,
 * String nombreDestino, String eMailCliente, String eMailDestino,String tipoAutenticacion, Cliente cliente,
 * String canal)},
 * {@link #transfiereTdcHaciaCtaCte(String ctaOrigen, String tipoCuentaOrigen,String ctaDestino,
 * String tipoCuentaDestino, double monto, long rutDestino, char digDestino, String comentario,
 * String nombreDestino, String eMailCliente, String eMailDestino, String tipoAutenticacion, String tipoTarj,
 * boolean validarCupo, double cupo, Cliente cliente, String canal)},
 * {@link #transfiereTdcHaciaCtaCte(String ctaOrigen, String tipoCuentaOrigen, String ctaDestino,
 * String tipoCuentaDestino, double monto, long rutDestino, char digDestino, String comentario,
 * String nombreDestino, String eMailCliente, String eMailDestino, String tipoAutenticacion, String tipoTarj,
 * Cliente cliente, String canal)},
 * {@link #transfiereTdcHaciaCtaCte(String ctaOrigen, String tipoCuentaOrigen, String ctaDestino,
 * String tipoCuentaDestino, double monto, long rutDestino, char digDestino, String comentario,
 * String nombreDestino, String eMailCliente, String eMailDestino, String tipoAutenticacion, String tipoTarj,
 * boolean validarCupo, double cupo, boolean validaCupoTarjetaEnLinea, Cliente cliente, String canal)},
 * {@link #realizarTransferenciaTdcHaciaCtaCte(String ctaOrigen, String ctaDestino, double monto,
 * boolean validaCupoTarjetaEnLinea, long rut, char dig, String canal)},
 * {@link #realizarTransferenciaTdcHaciaCtaCte(String ctaOrigen, String ctaDestino, double monto, String tipoTarj,
 * boolean validaCupoTarjetaEnLinea, long rut, char dig, String canal)},
 * {@link #transfiereCtaCteHaciaLSG(String ctaOrigen, String tipoCuentaOrigen, double monto, long rutDestino,
 * char digDestino, String comentario, String nombreDestino, String eMailCliente, String eMailDestino,
 * String tipoAutenticacion, Cliente cliente, String canal)},
 * {@link #realizarTransferenciaCtaCteHaciaLSG(String ctaOrigen, double monto, Cliente cliente, String canal)},
 * {@link #transfiereCtaCteHaciaCtaCte(String ctaOrigen, String tipoCuentaOrigen, String ctaDestino,
 * String tipoCuentaDestino, double monto, long rutDestino, char digDestino, String comentario,
 * String nombreDestino, String eMailCliente, String eMailDestino, String tipoAutenticacion, Cliente cliente,
 * String canal)},
 * {@link #transfiereCtaCteHaciaCtaCte(String ctaOrigen, String tipoCuentaOrigen, String ctaDestino,
 * String tipoCuentaDestino, double monto, long rutDestino, char digDestino, String comentario,
 * String nombreDestino, String eMailCliente, String eMailDestino, String tipoAutenticacion, boolean esProgramada,
 * Cliente cliente,String canal)},
 * {@link #realizarTransferenciaCtaCteHaciaCtaCte(String ctaOrigen, double monto, String ctaDestino, long rut,
 * char dig,String canal)},
 * {@link #transfiereCtaCteHaciaAhorro(String ctaOrigen, String tipoCuentaOrigen, String ctaDestino, double monto,
 * long rutDestino, char digDestino,
 * String comentario, String nombreDestino, String eMailCliente, String eMailDestino, String tipoAutenticacion,
 * Cliente cliente, String canal)},
 * {@link #transfiereCtaCteHaciaAhorro(String ctaOrigen, String tipoCuentaOrigen, String ctaDestino, double monto,
 * long rutDestino, char digDestino, String comentario, String nombreDestino, String eMailCliente,
 * String eMailDestino, String tipoAutenticacion, boolean esProgramada, Cliente cliente, String canal) },
 * {@link #realizarTransferenciaCtaCteHaciaAhorro(String ctaOrigen, double monto, String ctaDestino, long rut,
 * char dig, String canal)},
 * Se agregan las constantes {@link #INICIO_TRANSFERENCIA}, {@link #FORMATO_CUENTA}, {@link #FORMATO_RUT},
 * {@link #NOMBRE_LOG_CLIENTE}, {@link #CARACTER_TIPO_TARJETA}.
 *  <li>2.6 04/02/2013 Claudia Aracena Fuentes (SEnTRA) :
 *  Se modifica m�todo {@link #mail(String, String, String, Cliente, String)}.
 * <li>2.7 06/02/2013, Marcelo Fuentes H. (SEnTRA): Se modifican los m�todos
 * {@link #transferenciaCtaCteOtroBancoEnLineaEmpresa(String, String, String, String, String, long, char, String,
 * String, String, String, String, String, String, Cliente, String)},
 * {@link #transferenciaCtaCteOtroBancoEnLineaEmpresaConEnvioDeMail(DetalleTransferencia) } </li>
 * <li>2.8 28/09/2011 Carlos Cerda I. (Orand): Se a�ade l�gica de obtenci�n de tracenumbrer en el m�todo:
 *          {@link #transfiereCtaCteHaciaCtaCte(String, String, String, String,
 *                                              double, long, char, Cliente, String,
 *                                              String, String, String, String, String,
 *                                              String, String, String, String)}.
 *                                             Se agrega tracenumber, nemonicoAbono y nemonicoCargo a
 *                                             la forma del m�todo:
 *          {@link #realizarTransferenciaCtaCteHaciaCtaCte(Cliente, String, String, String, double,
 *                                              String, String, String)}.
 *                                             Se a�ade m�todo:
 *          {@link #transfiereCtaCteHaciaCtaCte(String, String, String, String, double, long, char,
 *                                              Cliente, String, String, String, String, String, String,
 *                                              String, String, String, String)}.
 * <li>2.9 25/05/2013 Mauricio Palma (Orand): Formato NNNN000000000 para nemonicoCargo y nemonicoAbono en caso
 * de TTFF mismo banco.
 * <li>2.10 03/07/2013 Eduardo Villagr�n Morales (Imagemaker): Se agrega el m�todo para validar si un alias es
 *          seguro {@link #validaAliasSeguro(AliasTTFFDVO)} de agregar.
 *          Se modifica el m�todo {@link #registrarAliasTTFF(AliasTTFFDVO)} para que haga uso del nuevo m�todo
 *          {@link #validaAliasSeguro(AliasTTFFDVO)} cuando se quiere agregar un nuevo alias, de esta forma
 *          se mueven y centralizan validaciones desde vistas hacia EJB.
 * <li>3.0 06/06/2013, Venancio Ar�nguiz P. (SEnTRA): Se modifican los m�todos
 * {@link #transferenciaCtaCteOtroBancoEnLineaEmpresa(String, String, String, String, String, long, char, String,
 * String, String, String, String, String, String, Cliente, String)},
 * {@link #transferenciaCtaCteOtroBancoEnLineaEmpresaConEnvioDeMail(DetalleTransferencia) }
 * {@link #cambiaEstadoTransferenciaEnLinea(String, char, String, String, String, String, String, String, String)}
 * </li>
 * <li>3.1 27/08/2013 Eduardo Villagr�n Morales (ImageMaker): Se agrega el m�todo
 *          {@link #grabarRegistroTransfDup()} para registrar autorizaci�n del cliente para TTFF
 *          duplicada.
 *          Se parametriza valor del archivo de configuraci�n de JNDI para contexto en constante JNDI_CONFIG.
 * <li>3.2 22/10/2013, M�nica Garc�s C. (Sermaluc): Se sobrecarga el metodo:
 * {@link #consultaTransferenciasRecibidasBancoBCI(long, char, Date, String, int, int, boolean )}
 * para obtener el nombre de los clientes que han realizado transferencias a una cuenta BCI</li>
 * <li>3.3 04/10/2013, Sebasti�n S�nchez S. (ADA Ltda.): Se sobrecarga m�todo
 *         <code>transCtaCteOtroBancoLineaPer</code> y se agrega l�gica para evaluar riesgo de fraude en
 *         transferencias a otros bancos. Se agrega un objeto de transferencia, el que contiene informaci�n para
 *         realizar la evaluaci�n de riesgo de fraude con Actimize.
 * <li>3.3 15/11/2013 Francisco Gonz�lez V. (SEnTRA): Se agrega m�todo {@link #transfiereCtaCteHaciaTdc(String,
 *                                                    String, String, String, double, long, char, String, String,
 *                                                    String, String, String, String, String, Cliente, String)
 *                                                    y se agrega par�metro cashPooling a m�todo
 *                                                    {@link #transfiereCtaCteHaciaTdc(String, String, String,
 *                                                    String, double, long, char, String, String, String, String,
 *                                                    String, String, String, Cliente, String, boolean)
 * <li>3.5 27/11/2013, Venancio Ar�nguiz P. (SEnTRA): Se modifican los m�todos
 * {@link #transferenciaCtaCteOtroBancoEnLineaEmpresa(String, String, String, String, String, long, char, String,
 * String, String, String, String, String, String, Cliente, String)}
 * {@link #transferenciaCtaCteOtroBancoEnLineaEmpresaConEnvioDeMail(DetalleTransferencia) }
 * </li>
 * <li>3.6 03/01/2014, Sebasti�n S�nchez S. (ADA Ltda.): Se modifica m�todo {@link #transCtaCteOtroBancoLineaPer(
 *         String, String, String, String, double, String, String, long, char, Cliente, String, String, String,
 *         String, String, String, String, String)}, y se destruye variable de instancia al final de dicho m�todo
 *         para que los clientes que reutilicen el EJB no encuentren informaci�n que no corresponde a su contexto.
 * <li>3.7 27/03/2014, Ivan Carvajal A. (BCI): Se agrega reintento al actualizar estado
 * <li>3.8 16/05/2014, Ivan Carvajal A. (BCI): Se cambia a log4j y se corrige transferencia con MAC para codigo 015 y 016
 * <li>3.9 Franco Jona Torres (Imagemaker S.A.): se agrega validacion y valores por defecto, si las variables "intentos" y "cambiaEstadoTTFF" son null
 *  en el metodo {@link #transferenciaCtaCteOtroBancoEnLineaEmpresa(String, String, String, String, String, long, char, String, String, String, String, String, String, String, Cliente, String)
 * <li>4.0 17/07/2014, Oliver Hidalgo Leal. (BCI): Se controla caida envio mail en metodo
	{@link #transferenciaCtaCteOtroBancoEnLineaEmpresaConEnvioDeMail(DetalleTransferencia) }</li>
 * </li>
 * <li>4.1 27/03/2014, Sebasti�n S�nchez S. (ADA Ltda.): Se agregan los siguientes m�todos para incluir 
 *         evaluaci�n de riesgo de fraude: <ul>
 *         <li>{@link #transfiereCtaCteHaciaAhorro(DetalleTransferencia)}</li>
 *         <li>{@link #transfiereCtaCteHaciaCtaCte(DetalleTransferencia)}</li>
 *         </ul></li>
 * <li>4.2 19/11/2014 Luis L�pez Alamos (SEnTRA): Se agregan los siguientes m�todos para el 
 * proyecto Regalo Virtual:
 * {@link #obtenerEgifts(String)}
 * {@link #envioMailEgift}
 * {@link #getLogger()}
 * 
 * Tambi�n se modifican los m�todos:
 * 
 *  {@link #transfiereCtaCteHaciaCtaCte}
 *  {@link #transfiereCtaCteHaciaAhorro}
 *  {@link #transCtaCteOtroBancoLineaPer}
 *  
 * y se agregan variables locales para el manejo de mensajes de tipo String
 * dentro de los m�todos mencionados anteriormente como nuevos.</li>
 *
 * <li>4.3 23/06/2014 Alvaro Fuentes (TINet), Juan Pablo Vega (TINet): se agregan los siguientes m�todos para el 
 * proyecto Transferencia el L�nea:
 * 
 *  {@link #obtenerTransferenciasDeFondos()}
 *  {@link #programar(DatosTransferenciaTO)}
 *  {@link #transferir(DatosTransferenciaTO)}
 *  {@link #iniciarTransferencia(long, char)}
 *  {@link #crearMapaBancosAutorizados(BancosAutorizados[])}
 *  {@link #ordenarBancosAutorizados(BancosAutorizados[])}
 *  {@link #filtrarAliasAutorizados(AliasTTFFDVO[], Map)}
 *  {@link #validarTransferencia(DatosTransferenciaTO)}
 *  {@link #validarEstadoNuevoAlias(DatosTransferenciaTO, Calendar)}
 *  {@link #validarMontoMaximo(double)}
 *  {@link #obtenerBancosConMontosRestringidos(String)}
 *  {@link #copiarTransferenciaDuplicada(DatosTransferenciaTO)}
 *  {@link #esCuentaCorrientePrima(String)}
 *  {@link #esCuentaDeAhorro(String))}
 *  {@link #convierteDatosTransferenciaADetalleTransferencia(DatosTransferenciaTO))}
 *  {@link #registrarNuevoAlias(DatosTransferenciaTO)}
 *  {@link #borrarNuevoAlias(DatosTransferenciaTO)}
 *  {@link #modificarNuevoAlias(DatosTransferenciaTO)}.
 *  
 * Se modifica el m�todo: {@link #logCliente(Cliente, String, String, String)}.
 * Se agregan los atributos:
 * 
 * {@link #TABLA_PARAMETRO_NEMONICO}
 * {@link #TABLA_PARAMETRO_TRANSFERENCIA_TERCEROS}
 * {@link #LARGO_RUT}
 * {@link #MILISEGUNDOS}
 * {@link #SESENTA}
 * {@link #HORAS_POR_DIA}
 * {@link #COD_ALIAS_NO_NUEVO}
 * {@link #MENSAJE_ERROR_MODIF_ALIAS}
 * </li>
 * <li>4.3 10/12/2014, Claudia Carrasco H. (SEnTRA): se modifica el siguiente metodo
 * {@link #transferenciaCtaCteOtroBancoEnLineaEmpresaConEnvioDeMail(DetalleTransferencia) }
 * {@link #transferenciaCtaCteOtroBancoEnLineaEmpresa(String, String, String, String, String, long,
			char, String, String, String, String, String, String, String, Cliente, String)}
 * Se evalua el retorno obtenido desde la CCA para determinar si el error es por causa del banco de destino, 
 * de ser asi se mostrara el mensaje la cliente indicando dicha situacion</li> 

 *  <li>4.3 01/12/2015 Luis L�pez Alamos (SEnTRA): Se agregan los siguientes m�todos para la 
 *  diferenciaci�n de envios de correos usando Egift:
 * {@link #envioMailEgiftOrigen}
 * {@link #envioMailEgiftDestino}
 * Por lo anterior se elimino el m�todo {@link #envioMailEgift}.
 * Por proyecto Fraudes se modifican los siguientes m�todos: 
 *  {@link #transfiereCtaCteHaciaCtaCte}
 *  {@link #transfiereCtaCteHaciaAhorro}
 *  {@link #transCtaCteOtroBancoLineaPer}
 * Se modifica m�todo {@link #iniciarTransferencia()}para manejar servicio regalos virtuales para 
 * portales que dependen de framework jsf.
 * y se crea m�todo {@link #isValidaDisponibilidadServicioFraude} para validar la disponibilidad 
 * del servicio de rechazo de transferencias por posibles fraudes.</li>
 * <li>4.4 10/07/2015 Luis Lopez(SEnTRA)-Claudia Lopez(Ing Soft. BCI.): Se modifica metodo
 * {@link #registrarNuevoAlias(DatosTransferenciaTO, boolean)}
 * {@link #transferir(DatosTransferenciaTO)}
 * {@link #programar(DatosTransferenciaTO)}
 * <li>4.4 06/08/2015 Carlos Cerda I. (Kubos): Debido al cambio en la firma del m�todo 
 * {@link MessageAuthenticationCodeHelper#validaMessageAuthenticationCode(String, String, String, String, String, 
 * String, String, String, String, String, String, String, String)} de boolean a int, se modifican los siguientes
 * m�todos:
 * {@link #transCtaCteOtroBancoLineaPer(String, String, String, String, double, String, String, long, char, 
 * Cliente, String, String, String, String, String, String, String, String)}
 * {@link #transferenciaCtaCteOtroBancoEnLineaEmpresa(String, String, String, String, String, long, char, String, 
 * String, String, String, String, String, String, Cliente, String)}
 * {@link #transferenciaCtaCteOtroBancoEnLineaEmpresaConEnvioDeMail(DetalleTransferencia)}
 * </li>
 * <li>4.5 04/11/2015 Luis Lopez Alamos (SEnTRA) - Claudia Lopez Pavlicevic (ing. Soft. BCI): Se modifican los metodos {@link #grabarRegistroTransfDup(TransferenciaDuplicadaVO)} 
 *  y {@link #copiarTransferenciaDuplicada(DatosTransferenciaTO)}. Adem�s se eliminan algunos atributos de la clase no utilizados</li>
 * <li>4.6 25/11/2015 Ernesto Vera C - Luis Lopez Alamos (SEnTRA)-Claudia Lopez(Ing Soft. BCI.): Se modifican metodos {@link #enviaEmail(DetalleTransferencia, boolean, Cliente, String) 
 *  y #enviaMailCliente(DetalleTransferencia, Cliente, String)}}. Se crea metodo {@link #formatearMontoMail(double)}</li>
 * <li>4.7 02/09/2016 Christoper Escudero (Imagemaker) - Minheli Mejias (BCI) : Se modifica metodo {@link #transferenciaCtaCteOtroBancoEnLineaEmpresa()} y {@link #transferenciaCtaCteOtroBancoEnLineaEmpresaConEnvioDeMail()}, en el momento de caer al catch
 * e ingresar al metodo #registraTransaccionPendiente se realiza un substring a la variable cuentaOrigen quitando los ceros a la izquierda del numero de cuenta.
 * <li>4.8 08/09/2016 Cristopher Freire C. (Imagemaker) - Pamela Inostroza(Ing. Soft. BCI): Se agrega metodo 
 * {@link #consultaTTFFDuplicada(long, Date, Date)} para traer los ttff duplicadas</li>
 * <li>4.9 05/12/2016 Susana Soto Macher (SEnTRA)-Juan Jos� Buend�a(Ing. Soft. BCI): Se corrige validaciones a Comentario, Nombre Destino e Email Beneficiario
 * para considerar correctamente espacios en blanco dentro de la cantidad m�xima de caracteres, en m�todo {@link #ingresaTransferencia(Cliente,String,String,double,Date,String,String,String
 * ,long,String,String,String,String,String,String,String,String,String,String,String)}
 * Se define constantes {@link #UNO},{@link #LARGO_EMAIL_BENEF},{@link #LARGO_COMENTARIO_TRF}, {@link #LARGO_STRING_DIEZ}, {@link #INICIO_CORTE}, {@link #POSICION_INICIAL}, 
 * {@link #CODIGO_ERROR_CUENTA_EXCEPTION} y {@link #CODIGO_ERROR_TUXEDO_EXCEPTION} para evitar uso de n�meros m�gicos en M�todo {@link #ingresaTransferencia(Cliente,String,String,double,Date,String,String,String
 * ,long,String,String,String,String,String,String,String,String,String,String,String)}
 * Por �ltimo, se reemplaza asignaci�n en duro por obtenci�n desde tabla de par�metros para el nombre JNDI del ejb ProgramacionServices.</li>
 * <li>5.0 30/11/2016 Andr�s Pardo T. (SEnTRA) - Marcelo Avenda�o. (Ing. Soft. BCI): Se incluyen constantes SERV_TUX_ABONO_CCA, ESTADO_RECHAZADO, COD_EST_REC y XFML_RESPONSE_PARAMETRO de transferencias CCA, se modifican los metodos:
 * {@link #transferenciaCtaCteOtroBancoEnLineaEmpresa(String, String, String, String, String, long, char, String, String, String, String, String, String, String, Cliente, String)}  Se incluye cambio de estado cuando la MAC es incorrecta o la respuesta de CCA no esta definida.
 * {@link #transferenciaCtaCteOtroBancoEnLineaEmpresaConEnvioDeMail(DetalleTransferencia)} Se incluye cambio de estado cuando la MAC es incorrecta o la respuesta de CCA no esta definida.
 * Se sobrecarga metodo {@link #enviarTrfOperador(Cliente, String, String, String)}. 
 * se eliminan imports no usados de EvaluadorDeFraudesTO y LogFile.
 * </li>
 * <li>5.1 16/05/2017, Mauricio Am�stica G. (Kubos) - Marcelo Arias (ing. Soft. BCI):
 * Se modifica {@link #transferir(DatosTransferenciaTO)} para agregar validaci�n de cuenta Mach desde EvaluadorTTFF.
 * </li>
 * <li>5.2 05/09/2017 Luis Lopez Alamos (SEnTRA) - Ricardo Carrasco C�ceres (Ing. Soft. BCI): Se modifica m�todo {@link #validaAliasSeguro(AliasTTFFDVO)}.</li> 
 * </ul>
 * <p>
 * <B>Todos los derechos reservados por Banco de Cr�dito e Inversiones.</B>
 * <p>
 */
public class TransferenciasDeFondosBean implements SessionBean {
  
    /**
     * Parametro Servicio tuxedo abono cca.
     */
    private static final String SERV_TUX_ABONO_CCA = "ccadirecta";
    
    /**
     * Parametro Codigo estado de rechazo.
     */
    private static final String COD_EST_REC = "W200";
    
    /**
     * Parametro estado de rechazo.
     */
    private static final String ESTADO_RECHAZADO = "REC";
    
    /**
     * Parametro para resultado de valor xfmlResponse.
     */
    private static final String XFML_RESPONSE_PARAMETRO = null;

    /**
     * UID de la versi�n.
     */
    private static final long serialVersionUID = 6L;

    /**
     * N�mero cero.
     */
    private final int cero = 0;

    /**
     * Constante cero inicio de corte.
     */
    private static final int INICIO_CORTE = 0;
    
    /**
     * Constante cero para indicar posici�n inicial.
     */
    private static final int POSICION_INICIAL = 0;
    
    /**
     * Constante cero para indicar error en cuentaException.
     */
    private static final int CODIGO_ERROR_CUENTA_EXCEPTION = 0;
	
    /**
     * Constante uno para indicar error en tuxedoException.
     */
    private static final int CODIGO_ERROR_TUXEDO_EXCEPTION = 1;
    
    /**
     * N�mero dos.
     */
    private final int dos = 2;

    /**
     * Constante con el valor de reintentos del StoreAndForward.
     */
    public static final String STORE_FORWARD_CONFIG_INTENTOS_POR_DEFECTO = "3";

    /**
     * Constante con el valor de cambio de estado para marcar en caso de caer a StoreAndForward.
     */
    public static final String TTFF_CONFIG_CambiaEstadoTTFF="SI";

    /**
     * Constante con el valor de decimalFormat.
     */
    public static final String FORMATO_DECIMAL="###,###.##";

    /**
     * Constante con el valor del archivo JNDIConfig.parametros.
     */
    private static final String JNDI_CONFIG = "JNDIConfig.parametros";

	/**
	 * Log para realizar debug
	 */
	private static Logger logger = Logger.getLogger(TransferenciasDeFondosBean.class);

	/**
	 * Archivo de par�metros (configuraci�n) de Transferencias de Fondos
	 */
	private static final String TTFF_CONFIG = "TransferenciaTerceros.parametros";

	/**
	 * Archivo de Par�metros para Transferencia de Fondos
	 */
	private static String TTFF_PARAMETROS = "TransferenciaDeFondosEmpresa.parametros";

	/**
	 * Archivo de par�metros (configuraci�n) de Store and Forward de transacciones sensibles.
	 */
	private static final String STORE_FORWARD_CONFIG = "StoreAndForward.parametros";

	/**
	 * Archivo de par�metros (configuraci�n) de los mnemonicos de las transacciones.
	 */
    private static final String NEMONICOS_CONFIG = "CodigoNemonicos.parametros";

	/**
	 * Variable que indica que para este caso se act�a con Instituci�n Financiera Originadora.
	 */
	private static final String ORIGEN = "IFO";

    /**
     * Identificador del comprobante que ser� enviado al cliente.
     */
    private static final String ID_COMP_CLI = "COMPROBANTE_TTFF_CLIENTE";

    /**
     * Identificador del comprobante que ser� enviado al destinatario banco Bci.
     */
    private static final String ID_COMP_DEST_BCI = "COMPROBANTE_TTFF_DESTINATARIO_BANCO";

    /**
     * Identificador del comprobante que ser� enviado al destinatario otros bancos.
     */
    private static final String ID_COMP_DEST_OTRO_BANCO = "COMPROBANTE_TTFF_DESTINATARIO_OTRO_BANCO";

	/**
     * Tabla de par�metros del generador de componentes MIME.
     */
    public static final String TABLA_GEN_COMP_MIME = "generadordecomponentesmime/generadorDeComponentesMIME.parametros";

    /**
     * Codigo del encabezado.
     */
    private static final String CODIGO_ENCABEZADO = "ENCA";

    /**
     * Codigo estado de la Transaccion.
     */
    private static final String CODIGO_TRANSACCION = "PRO";

    /**
     * C�digo que indica el exito de una operaci�n
     */
    private static final Object CODIGO_EXITO_TRANSFERENCIA = "000";


    /**
     * Largo que debe tener la cuenta corriente origen
     */
    private static final int LARGO_CUENTA = 12;

    /**
     * Largo maximo que puede teener el nombre completo.
     */
    private static final int LARGO_NOMBRE = 25;
	
    /**
     * Largo maximo que puede teener el email beneficiario completo.
     */
    private static final int LARGO_EMAIL_BENEF = 40;
	
    /**
     * Largo maximo que puede teener el comentario de una transferencia.
     */
    private static final int LARGO_COMENTARIO_TRF = 40;
	
    /**
     * Largo string de 10.
     */
    private static final int LARGO_STRING_DIEZ = 10;	

    /**
     * Largo del substring que no sera censurado.
     */
    private static final int LARGO_CARACTERES_SIN_CENSURA = 4;

    /**
     * Parametro mensaje inicio transferencia.
     */
    private static final String INICIO_TRANSFERENCIA = "INICIO TRANSFERENCIA";

    /**
     * Parametro formato la cuenta.
     */
    private static final String FORMATO_CUENTA = "00000000";

    /**
     * Parametro formato rut para logCliente.
     */
    private static final String FORMATO_RUT = "00000000";

    /**
     * Parametro que representa el inicio del nombre logCliente.
     */
    private static final String NOMBRE_LOG_CLIENTE = "transTerceros/TRF_";

    /**
     * Parametro primer caracter tipo tarjeta.
     */
    private static final char CARACTER_TIPO_TARJETA = 'M';

    /**
     * Llave para acceder a la tabla de codigos nemonicos.
     */
    private static final String TABLA_PARAMETRO_NEMONICO = "CodigoNemonicos.parametros";
    
    /**
     * Llave para acceder a la tabla de transferencia a terceros.
     */
    private static final String TABLA_PARAMETRO_TRANSFERENCIA_TERCEROS = "TransferenciaTerceros.parametros";
    
    /**
     * Largo del rut admitido para consultar programaciones.
     */
    private static final int LARGO_RUT = 10;
    
    /**
     * Milisegundos.
     */
    private static final int MILISEGUNDOS = 1000;
    
    /**
     * Sesenta.
     */
    private static final int SESENTA = 60;
    
    /**
     * Horas en un dia.
     */
    private static final int HORAS_POR_DIA = 24;    

    /**
     * C�digo de alias no nuevo.
     */
    private static final int COD_ALIAS_NO_NUEVO = 0;

    /**
     * Mensaje error al modificar el alias nuevo.
     */
    private static final String MENSAJE_ERROR_MODIF_ALIAS = "Cannot found registration";
    
	private ServiciosCuentas serviciosCuentas = null;

	private ServiciosSipe serviciosSipe;

	private JOLTPoolConnection joltPool = null;

	private TuxedoUtils tuxedo = new TuxedoUtils();

	private ServletSessionPool sesion = null;

	private String ejbName = this.getClass().getName();

	private String codigoBancoBCI = new String();

	/**
	 * DAO de Transferencias de Fondos
	 */
	TTFFDAO ttffDAO = null;

	/**
     *  N�mero que identifica al cana bci express empresa.
     */
    private static final Object CANAL_BCIEXPRESS_NEW = "230";

    /**
     * N�mero que identifica al canal BCINET para el env�o de correo.
     */
    private static final String CANAL_CORREO_BCI = "110";

    /**
     * Largo que debe tener un rut de Usuario en el BCI.
     */
    private static final int LARGO_RUT_CLIENTE = 8;


    /**
     * Parametro TDC indica el tipo de envio
     */

    private static final String TDC = "TDC";

    /**
     * Parametro CTA inidica Parametro de envio
     */

    private static final String CTA = "CTA";

    /**
     * Parametro TODAS inidica Parametro de envio
     */

    private static final String TODAS = "TODAS";

    /**
     * Parametro TODAS inidica Parametro REALIZADAS
     */

    private static final String REALIZADAS = "REA";

    /**
     * Parametro TODAS inidica Parametro RECIBIDAS
     */

    private static final String RECIBIDAS = "REC";

    /**
     * Parametro TODAS inidica Parametro Terceros
     */

    private static final String CONST_TERCEROS = "TER";
    /**
     * Parametro formato Fechas
     */

    private static final String DD_MM_YYYY = "dd/MM/yyyy";
    /**
     * Parametro Tipo de Productos
     */
    private static final String PRODCONT = "PRODCONT";

    /**
     * Objeto con informaci�n para transferencias.
     */
    private DatosTransferenciaTO datosTransferenciaTO;

    /**
     * Ejb de evaluaci�n de Riesgo de fraude en transferencias de fondos.
     */
    private EvaluacionRiesgoTTFF evaluacionRiesgoTTFF;

    /**
     * Parametro Codigo de rechazo
     */
    private static final String COD_REC = "84";

    /**
     * Parametro para recuperar codigo de error
     */
    private static final int COD_ERR_CCA = 2;


    /**
     * Descripci�n que menciona el servicio Regalo Virtual.
     */
    private static final String EGIFT = "Egift";
    
    /**
     * Descripci�n que identifica la categoria de una egift.
     */
    private static final String CATEGORIA = "categoria";
    
    /**
     * Simbolo utilizado para rescatar las egift desde tabla de parametros.
     */
    private static final String GUION_EGIFT = "_";
    
    /**
     * Descripci�n que identifica el c�digo de una egift.
     */
    private static final String CODIGO_EGIFT = "codigo";
    
    /**
     * Descripci�n que menciona la url de una imagen egift.
     */
    private static final String URL_EGIFT = "url";
    
    /**
     * Descripci�n de cantidad para recuperar Egift desde tabla de parametros.
     */
    private static final String CANT_EGIFT = "cantidad";
    
    /**
     * N�mero que identifica al canal TBanc para el env�o de correo.
     */
    private static final String CANAL_CORREO_TBANC = "100";
    
    /**
     * N�mero que identifica al canal Nova para el env�o de correo.
     */
    private static final String CANAL_CORREO_NOVA = "800";

    /**
     * Llave que identifica si servicio egift se encuentra habilitado.
     */
    private static final String EGIFT_ACTIVO = "verificaEgift";
    
    /**
     * Valor de llave que indica si servicio egift se encuentra habilitado.
     */
    private static final String VALOR_EGIFT = "valor";
    
    /**
     * Valor de llave que indica si servicio egift se encuentra habilitado.
     */
    private static final String EGIFT_HABILITADA = "SI";
    
    /**
     * Valor de llave que indica si servicio egift se encuentra habilitado.
     */
    private static final String EGIFT_NO_HABILITADA = "NO";
    
    /**
     * Parametro para representar la cantidad de valores que se eliminan de un rut.
     */
    private static final int COD_LARGO_RUT = 1;
    
    /**
     * Representa la cantidad de valores a cortar del numero de cuenta.
     */
    private static final int CANTIDAD_CARACTERES_A_ELIMINAR = 4;
    


	/**
	 * M�todo que inicializa Variables de Conecci�n DAO (para Alias).
	 *
	 * Se trae m�todo desde {@link wcorp.bprocess.cuentas.TransferenciasFondosBean#} a la implementaci�n Stateless del mismo.
	 * Se valida que solo obtenga la instancia del dao TTFFDAO, cuando la referencia no exista. Adem�s se cambia nivel de logeo de debug
	 * a info o error seg�n corresponda.
	 *
	 * <p>
	 * Registro de Versiones:<ul>
	 *
	 * <li>1.0 25/03/2009, Carlos Cerda I.  (SEnTRA): Versi�n inicial.
	 *
	 * </ul>
	 *
	 * @throws CreateException
	 * @return void
	 * @Since 1.0
	 * @see {@link wcorp.bprocess.cuentas.TransferenciasFondosBean#envInit()}
	 */
	private void envInit() throws CreateException {
		if(ttffDAO == null){
		try {
			logger.debug("[envInit]:: Instanciaci�n de TTFFDAO: ");
			String settingsFileName = "wcorp-settings.xml";
			String errorsFileName = "errors.properties";

			if (!WCorpConfig.getInstance().appInit(settingsFileName, errorsFileName)) {
					if(logger.isEnabledFor(Level.ERROR)){logger.error("[envInit]:: Problema en la Inicializacion de Variables de Ambiente de EJB");}
				throw new CreateException("Problema en la Inicializacion de Variables de Ambiente de EJB");
			}

			int daoType = Integer.parseInt(FileSettings.getValue(WCorpConfig.APP_SETTINGS, "/application-settings/ttff/dao-type"));
			DAOFactory daoFactory = null;
			daoFactory = (DAOFactory) DAOFactory.getDAOFactory(daoType);
			ttffDAO = daoFactory.getTTFFDAO();
		} catch (Exception ex) {
				if(logger.isEnabledFor(Level.ERROR)){logger.error("[envInit]:: Problema en la Instanciaci�n de TTFFDAO: "+ex.toString());}
			throw new CreateException(ex.getMessage());
		}
		}
	}

	/**
	 * M�todo que instancia un nuevo Bean.
	 *
	 * Se trae m�todo desde {@link wcorp.bprocess.cuentas.TransferenciasFondosBean#} a la implementaci�n Stateless del mismo,
	 * se cambia nivel de logeo de debug a info o error seg�n corresponda y se a�ade instancia a wcorp.serv.sipe.ServiciosSipeBean.
	 *
	 * Registro de versiones:<ul>
	 *
	 * <li>1.0 25/03/2009, Carlos Cerda I.  (SEnTRA): Versi�n inicial.
	 * </ul>
	 *
	 * @throws CreateException
	 * @throws RemoteException
	 * @return void
	 * @since 1.0
	 * @see {@link wcorp.bprocess.cuentas.TransferenciasFondosBean#ejbCreate()}
	 */
	public void ejbCreate()throws CreateException, RemoteException {
		logger.debug("[createEJB]:: En ejbCreate");
		try{
			ServiciosCuentasHome cuentasHome = (ServiciosCuentasHome)EnhancedServiceLocator.getInstance().getHome("wcorp.serv.cuentas.ServiciosCuentas", ServiciosCuentasHome.class);
			serviciosCuentas = cuentasHome.create();
			logger.debug("[createEJB]:: Cuentas INSTANCIADO");
			ServiciosSipeHome sipeHome = (ServiciosSipeHome)EnhancedServiceLocator.getInstance().getHome("wcorp.serv.sipe.ServiciosSipe", ServiciosSipeHome.class);
			serviciosSipe = sipeHome.create();
			logger.debug("[createEJB]:: sipe INSTANCIADO");
			joltPool = new JOLTPoolConnection();
			String poolName = joltPool.getSesion(this.getClass().getName()).getPoolName();
			tuxedo.setPoolName(poolName);
			logger.debug("[createEJB]:: JOLTPoolConnection INSTANCIADO");
                        this.codigoBancoBCI = TablaValores.getValor(TTFF_CONFIG, "codigoBCI", "valor");
		}catch(Exception e){
			if(logger.isEnabledFor(Level.ERROR)){logger.error("[createEJB]:: No se pudo recuperar instancia de "+e);}
			throw new CreateException("No se pudo recuperar instancia de "+ejbName);
		}
	}

	/**
	 * M�todos p�blicos
	 */

	public void ejbActivate() throws EJBException, RemoteException {
	}

	public void ejbPassivate() throws EJBException, RemoteException {
	}

	public void ejbRemove() throws EJBException, RemoteException {
	}

	public void setSessionContext(SessionContext sessionContext) throws EJBException, RemoteException {
	}
	/**
	 * M�todo que genera un log "personalizado" (por archivo TRF_RUT_DV.log)
	 *
	 * Se trae m�todo desde {@link wcorp.bprocess.cuentas.TransferenciasFondosBean#} a la implementaci�n Stateless del mismo Sin modificaciones.
	 *
	 * Registro de versiones:<ul>
	 *
	 * <li>1.0 25/03/2009, Carlos Cerda I. (SEnTRA): Versin inicial.
	 * <li>1.1 02/05/2014, Ivan Carvajal A. (BCI): Se cambia log a log4j.
	 *
	 * </ul>
	 *
	 *
	 * @param metodo
	 * @param ctaOrigen
	 * @param tipoCuentaOrigen
	 * @param ctaDestino
	 * @param tipoCuentaDestino
	 * @param rut
	 * @param dig
	 * @param canal
	 * @param montoTransferencia
	 * @param rutDestino
	 * @param dvDestino
	 * @param codigoBanco
	 * @param mensaje
	 *
	 * @return void
	 * @since 1.0
	 * @see {@link wcorp.bprocess.cuentas.TransferenciasFondosBean#logCliente(String,String,String,String,String,double,long,char,String,String)}
	 */
	private void logCliente(
			String metodo,
			String ctaOrigen,
			String tipoCuentaOrigen,
			String ctaDestino,
			String tipoCuentaDestino,
			long rut,
			char dig,
			String canal,
			double montoTransferencia,
			long rutDestino,
			char dvDestino,
			String codigoBanco,
			String mensaje) {

		String msg= "";

		msg = "CANAL [" + canal + "], ";
		msg += "MENSAJE  [" + mensaje + "], ";
		msg += "CTACARGO [" + ctaOrigen + "], ";
		msg += "TIPOCARGO[" + tipoCuentaOrigen + "], ";
		msg += "CTAABONO [" + ctaDestino + "], ";
		msg += "TIPOABONO[" + tipoCuentaDestino + "], ";
		msg += "MTOTRANS [" + montoTransferencia + "], ";
		msg += "RUTDEST  [" + rutDestino + "], ";
		msg += "DVDEST   [" + dvDestino + "], ";
		msg += "CODBANCO [" + codigoBanco + "]";

		if(logger.isInfoEnabled()){
			logger.info("[" + metodo + "]["+rut+"] " + msg);
		}


	}
	/**
	 * M�todo que genera un log "personalizado" (por archivo TRF_RUT_DV.log)</p>
	 *
	 * Se trae m�todo desde {@link wcorp.bprocess.cuentas.TransferenciasFondosBean#} a la implementaci�n Stateless del mismo y
	 * se a�aden a la firma del m�todo cliente y canal.
	 *
	 * Registro de versiones:<ul>
	 * <li>1.0 25/03/2009, Carlos Cerda I. (SEnTRA): Versin inicial.
	 * <li>1.1 02/05/2014, Ivan Carvajal A. (BCI): Se cambia log a log4j.
     * <li>2.0 30/06/2014 Juan Pablo Vega (TINet): se cambia logeo por el log4j.
	 * </ul>
	 *
	 *
	 * @param cliente
	 * @param canal
	 * @param metodo
	 * @param mensaje
	 *
	 * @return void
	 * @since 1.0
	 * @see {@link wcorp.bprocess.cuentas.TransferenciasFondosBean#logCliente(String,String)}
	 */
	private void logCliente(Cliente cliente, String canal, String metodo, String mensaje) {

		String msg= "";
        if (obtenerLogger().isEnabledFor(Level.DEBUG)) {
            msg = "METODO  [" + metodo + "], ";
		msg += "MENSAJE [" + mensaje + "]";

            obtenerLogger().debug(msg);
		}

	}
	/**
     * M�todo migrado desde {@link TransferenciasFondosBean} encargado de generar un log "personalizado". Se
     * agregan las constantes FORMATO_RUT y NOMBRE_LOG_CLIENTE las cuales reemplazan a los valores en duro
     * "00000000" y "transTerceros/TRF_" respectivamente.
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 27/07/2012 Jorge Leiva A. (TINet): Versin inicial.
     * <li>1.1 02/05/2014, Ivan Carvajal A. (BCI): Se cambia log a log4j.
     * </ul>
     * <p>
     *
     * @param metodo a loguear.
     * @param mensaje mensaje a loguear.
     * @param rut del cliente asociado.
     * @param dig digito verificador del cliente
     * @param canal asociado.
     * @since 2.4
     */
    private void logCliente(String metodo, String mensaje, long rut, char dig, String canal) {

        String msg = "";

        msg = "CANAL [" + canal + "], ";
        msg += "MENSAJE [" + mensaje + "]";

        if(logger.isInfoEnabled()){
			logger.info("[" + metodo + "]["+rut+"] " + msg);
		}

    }

    /**
     * M�todo migrado desde {@link TransferenciasFondosBean} encargado de generar un log "personalizado". Se
     * agregan las constantes FORMATO_RUT y NOMBRE_LOG_CLIENTE las cuales reemplazan a los valores en duro
     * "00000000" y "transTerceros/TRF_" respectivamente.
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 27/07/2012 Jorge Leiva A. (TINet): Versin inicial.
     * <li>1.1 02/05/2014, Ivan Carvajal A. (BCI): Se cambia log a log4j.
     * </ul>
     * <p>
     *
     * @param metodo a loguear.
     * @param mensaje mensaje a loguear.
     * @param e Excepcion a loguear
     * @param rut del cliente asociado.
     * @param dig digito verificador del cliente
     * @param canal asociado.
     * @since 2.4
     */
    private void logCliente(String metodo, String mensaje, Exception e, long rut, char dig, String canal) {

        String msg = "";
        msg = "CANAL [" + canal + "], ";
        msg += "MENSAJE [" + mensaje + "]";

        if (e instanceof GeneralException) {
            GeneralException ge = (GeneralException) e;
            msg += ", COD_EX [" + ge.getCodigo() + "],";
            msg += ", INFO_EX[" + ge.getInfoAdic() + "],";
            msg += ", FULL_EX[" + ge.getFullMessage() + "]";
        }

        logger.error("[" + metodo + "]["+rut+"] " + msg, e);

    }

    /**
     * M�todo migrado desde {@link TransferenciasFondosBean} encargado de generar un log "personalizado". Se
     * agregan las constantes FORMATO_RUT y NOMBRE_LOG_CLIENTE las cuales reemplazan a los valores en duro
     * "00000000" y "transTerceros/TRF_" respectivamente.
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 27/07/2012 Jorge Leiva A. (TINet): Versin inicial.
     * <li>1.1 02/05/2014, Ivan Carvajal A. (BCI): Se cambia log a log4j.
     * </ul><p>
     * @param metodo a loguear.
     * @param ctaOrigen cuenta corriente origen (donde se hace el cargo)
     * @param tipoCuentaOrigen tipo de cuenta corriente origen.
     * @param ctaDestino cuenta corriente destino (donde se hace la transferencia)
     * @param tipoCuentaDestino tipo de cuenta corriente destino.
     * @param montoTransferencia monto de la transferencia
     * @param rutDestino del cliente destino
     * @param dvDestino del cliente destino
     * @param codigoBanco codigo del banco asociado a hacer la trasnferencia
     * @param mensaje mensaje a loguear.
     * @param rut del cliente asociado.
     * @param dig digito verificador del cliente
     * @param canal asociado.
     * @since 2.4
     */
    private void logCliente(String metodo, String ctaOrigen, String tipoCuentaOrigen, String ctaDestino,
        String tipoCuentaDestino, double montoTransferencia, long rutDestino, char dvDestino, String codigoBanco,
        String mensaje, long rut, char dig, String canal) {

        String msg = "";

        msg = "CANAL [" + canal + "], ";
        msg += "MENSAJE  [" + mensaje + "], ";
        msg += "CTACARGO [" + ctaOrigen + "], ";
        msg += "TIPOCARGO[" + tipoCuentaOrigen + "], ";
        msg += "CTAABONO [" + ctaDestino + "], ";
        msg += "TIPOABONO[" + tipoCuentaDestino + "], ";
        msg += "MTOTRANS [" + montoTransferencia + "], ";
        msg += "RUTDEST  [" + rutDestino + "], ";
        msg += "DVDEST   [" + dvDestino + "], ";
        msg += "CODBANCO [" + codigoBanco + "]";

        if(logger.isInfoEnabled()){
			logger.info("[" + metodo + "]["+rut+"] " + msg);
        }

    }
    /**
	 * M�todo que obtiene el saldo de la cuenta corriente (o prima).
	 * Se trae m�todo desde {@link wcorp.bprocess.cuentas.TransferenciasFondosBean#} a la implementaci�n Stateless del mismo y
	 * se cambia nivel de logeo de debug a info o error seg�n corresponda.
	 *
	 * Registro de versiones:<ul>
	 *
	 * <li>1.0 25/03/2009, Carlos Cerda I. (SEnTRA): Versi�n inicial.
	 *
	 * </ul>
	 *
	 *
	 * @param cta Cuenta Corriente o Cuenta de Prima
	 * @return wcorp.serv.cuentas.SaldoCtaCte Saldo de la Cuenta
	 * @throws TransferenciaException
	 * @since 1.0
	 * @see {@link wcorp.bprocess.cuentas.TransferenciasFondosBean#obtenerSaldoCteCpr(String)}
	 */
	public SaldoCtaCte obtenerSaldoCteCpr(String cta) throws TransferenciaException{
		try{
			if(logger.isInfoEnabled()){logger.info("[obtenerSaldoCteCpr]:: SALDO CTECPR [" + cta + "]");}
			CuentaCorriente cuentaCorriente = new CuentaCorriente(cta);
			return cuentaCorriente.getSaldo();
		}catch(Exception e){
			String mensaje= "Cuenta=" + cta;
			if(logger.isEnabledFor(Level.ERROR)){logger.error("[obtenerSaldoCteCpr]:: SALDOCTECPR: "+ mensaje + e);}
			throw new TransferenciaException("TRF001");
		}
	}
	/**
	 * M�todo que obtiene el saldo de la cuenta de ahorro.
	 * Se trae m�todo desde {@link wcorp.bprocess.cuentas.TransferenciasFondosBean#} a la implementaci�n Stateless del mismo y
	 * se cambia nivel de logeo de debug a info o error seg�n corresponda.
	 *
	 * Registro de versiones:<ul>
	 *
	 * <li>1.0 25/03/2009, Carlos Cerda I. (SEnTRA): Versi�n inicial.
	 *
	 * </ul>
	 *
	 *
	 * @param cta Cuenta de Ahorro
	 * @return wcorp.serv.ahorros.SaldoAho Saldo de la Cuenta
	 * @throws TransferenciaException
	 * @since 1.0
	 * @see {@link wcorp.bprocess.cuentas.TransferenciasFondosBean#obtenerSaldoAhorro(String)}
	 */
	public SaldoAhorro obtenerSaldoAhorro(String cta) throws TransferenciaException {
		try {
			if(logger.isInfoEnabled()){logger.info("[obtenerSaldoAhorro]:: SALDO AHORRO [" + cta + "]");}
			CuentaAhorro cuentaAhorro= new CuentaAhorro(cta);
			return cuentaAhorro.getSaldo();
		} catch (Exception e) {
			String mensaje= "Cuenta=" + cta;
			if(logger.isEnabledFor(Level.ERROR)){logger.error("[obtenerSaldoAhorro]:: SALDOAHO" + mensaje + e);}
			throw new TransferenciaException("TRF001");
		}
	}
	/**
	 * M�todo que obtiene el monto transferido en el dia para una cuenta espec�fica.
	 * Se trae m�todo desde {@link wcorp.bprocess.cuentas.TransferenciasFondosBean#} a la implementaci�n Stateless del mismo y
	 * se cambia nivel de logeo de debug a error.
	 *
	 * Registro de versiones:<ul>
	 *
	 * <li>1.0 25/03/2009, Carlos Cerda I. (SEnTRA): Versi�n inicial.
	 *
	 * </ul>
	 *
	 *
	 * @param rut
	 * @param tipoCuentaOrigen
	 * @param cuentaOrigen
	 * @param fecha
	 * @return double
	 * @throws TransferenciaException
	 * @since 1.0
	 * @see {@link wcorp.bprocess.cuentas.TransferenciasFondosBean#montoTransferidoDia(String, String, Date)}
	 */
	public double montoTransferidoDia(long rut, String tipoCuentaOrigen, String cuentaOrigen, Date fecha) throws TransferenciaException {
		try {
			return serviciosCuentas.montoTotalTransferidoDia(
					rut,
					tipoCuentaOrigen,
					cuentaOrigen,
					fecha);
		} catch (Exception e) {
			String mensaje=
				"TipoCargo="
				+ tipoCuentaOrigen
				+ ", CuentaCargo="
				+ cuentaOrigen
				+ ", Fecha="
				+ (fecha != null ? fecha.toString() : "--/--/----");
			if(logger.isEnabledFor(Level.ERROR)){logger.error("[montoTransferidoDia]:: montoTransferidoDia" + mensaje + e);}
			throw new TransferenciaException("TRF001");
		}
	}
	/**
	 * M�todo que obtiene el monto programado en el d�a para una cuenta espec�fica.
	 * Se trae m�todo desde wcorp.bprocess.cuentas.TransferenciasFondosBean a la implementaci�n Stateless del mismo y
	 * se cambia nivel de logeo de debug a error.
	 *
	 * Registro de versiones:<ul>
	 *
	 * <li>1.0 25/03/2009, Carlos Cerda I. (SEnTRA): Versi�n inicial.
	 *
	 * </ul>
	 *
	 *
	 * @param rut
	 * @param tipoCuentaOrigen
	 * @param cuentaOrigen
	 * @param fecha
	 * @return double
	 * @throws TransferenciaException
	 * @since 1.0
	 * @see {@link wcorp.bprocess.cuentas.TransferenciasFondosBean#montoProgramadoDia(String, String, Date)}
	 */
	public double montoProgramadoDia(long rut, String tipoCuentaOrigen, String cuentaOrigen, Date fecha) throws TransferenciaException {
		try {
			return serviciosCuentas.montoTotalProgramadoDia(rut, tipoCuentaOrigen, cuentaOrigen, fecha);
		} catch (Exception e) {
			String mensaje=
				"TipoCargo="
				+ tipoCuentaOrigen
				+ ", CuentaCargo="
				+ cuentaOrigen
				+ ", Fecha="
				+ (fecha != null ? fecha.toString() : "--/--/----");
			if(logger.isEnabledFor(Level.ERROR)){logger.error("[montoProgramadoDia]:: montoProgramadoDia" + mensaje + e);}
			throw new TransferenciaException("TRF001");
		}
	}
	/**
	 * Consulta por una transferencia similar a la que se quiere realizar.
	 *
	 * <p>La validaci�n se hace en base al rut del cliente, monto de la ttff y al rut, cuenta y
	 * banco de destino.
	 *
	 * <p>Registro de versiones:<ul>
	 * <li>1.1 26/03/2009, Carlos Cerda I. (SEnTRA): Versi�n inicial.
	 * <li>1.2 03/09/2013, Eduardo Villagr�n Morales (Imagemaker): Se cambia llamada de tuxido a SPF.
	 *             Se cambia el sp llamado.
	 * </ul>
	 *
	 * @param consEntradaTransferencia objeto con los datos de la transferencia para comparar.
	 * @throws Exception
	 * @since 1.0
	 * @see {@link wcorp.bprocess.cuentas.TransferenciasFondosBean#ConsTransfIguales(TransferenciasEntradaVO)}
	 */
	public TransferenciasSalidaVO ConsTransfIguales(TransferenciasEntradaVO consEntradaTransferencia)
	        throws Exception {
		TransferenciasSalidaVO consSalidaTransferencia = new TransferenciasSalidaVO();
		try {
			if(logger.isDebugEnabled()){
			    logger.debug("[ConsTransfIguales] [" + consEntradaTransferencia.getRutOrigen()
			            + "] montoTTFF=<" + consEntradaTransferencia.getMtoTransferir()
			            + ">, rutCtaDst=<" + consEntradaTransferencia.getRutDestino()
			            + ">, ctaDst=<" + consEntradaTransferencia.getCtaDestino()
			            + ">, codBcoDst=<" + consEntradaTransferencia.getCodBancoDestino() + ">");
		    }

			HashMap paramEntrada = new HashMap();
			paramEntrada.put("rutCliente", StringUtil.rellenaPorLaIzquierda(
			        consEntradaTransferencia.getRutOrigen(),8 , '0'));
			paramEntrada.put("montoTTFF", Double.valueOf(String.valueOf(consEntradaTransferencia.getMtoTransferir())));
			paramEntrada.put("rutCuentaDestino", StringUtil.rellenaPorLaIzquierda(
			        consEntradaTransferencia.getRutDestino(), 8, '0'));
			paramEntrada.put("cuentaDestino", consEntradaTransferencia.getCtaDestino());
			paramEntrada.put("codigoBancoDestino", consEntradaTransferencia.getCodBancoDestino());

            ConectorServicioDePersistenciaDeDatos conector = new ConectorServicioDePersistenciaDeDatos(
                    TablaValores.getValor(JNDI_CONFIG, "cluster", "param"));

            List respuesta = conector.consultar("ctaster", "lstEstTraCli24Hrs",
                    paramEntrada);
            if (logger.isDebugEnabled()){
                logger.debug("[ConsTransfIguales] [" + consEntradaTransferencia.getRutOrigen()
                        + "] Consulta a lstEstTraCli24Hrs:" + respuesta);
            }

			if (respuesta != null && respuesta.size() > 0){
			    HashMap ttffDuplicada = (HashMap) respuesta.get(0);
			    if (logger.isDebugEnabled()){
	                logger.debug("[ConsTransfIguales] [" + consEntradaTransferencia.getRutOrigen()
	                        + "] �ltima TTFF igual:" + ttffDuplicada);
	            }
				consSalidaTransferencia.setFechaCreacion((String) ttffDuplicada.get("fechaCreacion").toString());
				consSalidaTransferencia.setTipoCtaDestino((String) ttffDuplicada.get("tipoCuentaDestino"));
				consSalidaTransferencia.setCtaDestino((String) ttffDuplicada.get("cuentaDestino"));
				consSalidaTransferencia.setRutDestino((String) ttffDuplicada.get("rutCuentaDestino"));
				consSalidaTransferencia.setCodBancoDestino((String) ttffDuplicada.get("codigoBancoDestino"));
				consSalidaTransferencia.setCanal((String) ttffDuplicada.get("canal"));
                consSalidaTransferencia.setNumOperacion(Long.valueOf(((String) ttffDuplicada.get("folSip")).trim()).longValue());
				consSalidaTransferencia.setNomBenef(
				        ttffDuplicada.get("nombreBeneficiario") != null
				        ? (String) ttffDuplicada.get("nombreBeneficiario"):"");
				consSalidaTransferencia.setGlosaError(
				        ttffDuplicada.get("glsErr") != null ? (String)ttffDuplicada.get("glsErr"):"");
				consSalidaTransferencia.setCodEstado(
				        ttffDuplicada.get("codEst") != null ? (String) ttffDuplicada.get("codEst"):"");
				consSalidaTransferencia.setIgualTran("S");
			}
			else if (respuesta != null && respuesta.size() == 0){
				if(logger.isDebugEnabled()){
				    logger.debug("[ConsTransfIguales]:: Transferencia no es igual " + consEntradaTransferencia.getRutOrigen());
				    }
				consSalidaTransferencia.setIgualTran("N");
			}
			else{
			    if (logger.isEnabledFor(Level.ERROR)){
			        logger.error("[ConsTransfIguales] Error al consultar SPD");
			    }
			    throw new GeneralException("TRF001");
			}
			return consSalidaTransferencia;
		}
		catch (Exception e) {
			String excep = ErroresUtil.extraeStackTrace(e);
			if(logger.isEnabledFor(Level.ERROR)){
			    logger.error("[ConsTransfIguales]:: Exception: " + e.getMessage() + excep);
		    }
			throw new GeneralException("TRF001");
		}
	}
	/**
	 * M�todo que obtiene la lista de alias de TTFF del cliente.
	 * Se trae m�todo desde {@linkwcorp.bprocess.cuentas.TransferenciasFondosBean#} a la implementaci�n Stateless del mismo y
	 * se cambia nivel de logeo de debug a info. Adem�s se a�ade llamada a m�todo envInit().
	 *
	 * Registro de Versiones:<ul>
	 *
	 * <li>1.0 25/03/2009, Carlos Cerda I. (SEnTRA): Versi�n inicial.
	 *
	 * </ul>
	 *
	 * @param rut
	 * @param dig
	 * @return Colection de AliasTTFFDVO
	 * @throws BusinessException
	 * @throws Exception
	 * @throws com.schema.util.GeneralException
	 * @since 1.0
	 * @see {@linkwcorp.bprocess.cuentas.TransferenciasFondosBean#getListaAliasTTFF()}
	 */
	public Collection getListaAliasTTFF(long rut, char dig) throws BusinessException, Exception, com.schema.util.GeneralException {
		envInit();
		AliasTTFFDVO aliasTTFFDvo = new AliasTTFFDVO();
		if(logger.isInfoEnabled()){logger.info("[getListaAliasTTFF]:: Recuperarando para: "+rut+dig);}
		aliasTTFFDvo.setRutCliente(String.valueOf(rut)+dig);
		return ttffDAO.getListaAliasTTFFByRutCliente(aliasTTFFDvo);

	}
	/**
	 * Registra un Alias (crea o Actualiza).
	 * Se trae m�todo desde {@link wcorp.bprocess.cuentas.TransferenciasFondosBean#} a la implementaci�n Stateless del mismo y
	 * se a�ade llamada a m�todo envInit().
	 *
	 *
	 * Registro de Versiones:<ul>
	 *
	 * <li>1.0 26/03/2009, Carlos Cerda I. (SEnTRA): Versi�n inicial.
     * <lu>1.1 03/07/2013, Eduardo Villagr�n Morales (Imagemaker): Se agrega llamada al m�todo
     *          {@link #validaAliasSeguro(AliasTTFFDVO)} cuando se quiere agregar un nuevo alias con la
     *          finalidad de validar si es segura o no su inserci�n en base a las reglas indicadas en dicho
     *          m�todo.
	 * </ul>
	 *
	 *
	 * @param AliasTTFFDVO
	 * @throws BusinessException
	 * @throws Exception
	 * @throws GeneralException
	 * @since 1.0
	 * @see {@link wcorp.bprocess.cuentas.TransferenciasFondosBean#registrarAliasTTFF(AliasTTFFDVO)}
	 */
    public long registrarAliasTTFF(AliasTTFFDVO aliasTTFFDvo) throws BusinessException, Exception,
            com.schema.util.GeneralException {
		envInit();
		if (aliasTTFFDvo.getCodigo()>0) {
			ttffDAO.actualizarAliasTTFF(aliasTTFFDvo);
			return aliasTTFFDvo.getCodigo();
		} else
            validaAliasSeguro(aliasTTFFDvo);
			return ttffDAO.registrarAliasTTFF(aliasTTFFDvo);
	}

	/**
	 * Elimina un Alias.
	 * Se trae m�todo desde {@link wcorp.bprocess.cuentas.TransferenciasFondosBean#} a la implementaci�n Stateless del mismo y
	 * se a�ade llamada a m�todo envInit().
	 *
	 * Registro de Versiones:<ul>
	 *
	 * <li>1.0 26/03/2009, Carlos Cerda I. (SEnTRA): Versi�n inicial.
	 *
	 * </ul>
	 *
	 *
	 * @param code: Clave Primaria de la tabla ali
	 * @throws BusinessException, Exception, GeneralException
	 * @see {@link wcorp.bprocess.cuentas.TransferenciasFondosBean#borrarAliasTTFF(long)}
	 */
	public void borrarAliasTTFF(long code) throws BusinessException, Exception, com.schema.util.GeneralException {
		envInit();
		AliasTTFFDVO aliasTTFFDvo = new AliasTTFFDVO();
		aliasTTFFDvo.setCodigo(code);
		ttffDAO.borrarAliasTTFF(aliasTTFFDvo);
	}

	/**
	 * M�todo que confirma que el N�mero de verificaci�n ingresado por el cliente es id�ntico al almacenado por el banco. El m�todo
	 * devuelve void ya que la objeto ({@link com.schema.util.dao.SybaseUtils}) que realiza la llamada al sp lanza excepciones de acuerdo al
	 * error ocurrido. Estas pueden ser de dos tipos: 'error de comunicaci�n' o el 'id ingresado no corresponde al alias seleccionado'.
	 *
	 * Registro de Versiones:<ul>
	 *
	 * <li>1.0 06/07/2010 Carlos Cerda Iglesias (Orand Innovaci�n): - Versi�n inicial
	 *
	 * </ul>
	 *
	 *
	 * @param code: Clave Primaria de la tabla ali
	 * @param id: identificador de confirmaci�n del alias
	 * @throws BusinessException, Exception, com.schema.util.GeneralException
	 * @since 1.8
	 */
	public void confirmarAliasTTFF(long code, String id) throws BusinessException, Exception, com.schema.util.GeneralException {
		envInit();
		AliasTTFFDVO aliasTTFFDvo = new AliasTTFFDVO();
		aliasTTFFDvo.setCodigo(code);
		aliasTTFFDvo.setIdValidacion(id);
		ttffDAO.confirmarAliasTTFF(aliasTTFFDvo);
	}

	/**
	 * Transferencias desde Ctas. Corrientes (Primas) hacia Corrientes (Primas).
	 *
	 * Se trae m�todo desde {@link wcorp.bprocess.cuentas.TransferenciasFondosBean#} a la implementaci�n Stateless del mismo y
	 * se modifica la excepci�n y se agrega par�metro boolean 'esProgramada' para soportar las Transferencias Programadas en L�nea.
	 *
	 * Registro de Versiones:<ul>
	 *
	 * <li>1.0  26/03/2009, Carlos Cerda I. (SEnTRA): Versi�n inicial.
         * <li>1.1  28/09/2011, Carlos Cerda I. (Orand): Se invoca al m�todo {@link #transfiereCtaCteHaciaCtaCte(
         * String, String, String, String, double, long, char, Cliente, String, String, String, String, String,
         * String, String, String, String, String)}, dejando en �l la logica de la transferencia.
         * Se le pasa mnem�nicos gen�ricos para el cargo y abono.
     * <li>1.2 31/03/2014, Sebasti�n S�nchez S. (ADA Ltda.): Se modifica m�todo para mantener compatibilidad con 
     *         el m�todo {@link #transfiereCtaCteHaciaCtaCte(DetalleTransferencia)}
	 *
	 * </ul>
	 *
	 *
	 * @param ctaOrigen
	 * @param tipoCuentaOrigen
	 * @param ctaDestino
	 * @param tipoCuentaDestino
	 * @param monto
	 * @param rutDestino
	 * @param digDestino
	 * @param cliente
	 * @param canal
	 * @param comentario
	 * @param nombreDestino
	 * @param eMailCliente
	 * @param eMailDestino
	 * @param tipoAutenticacion
	 * @param tipoTransferencia
	 * @param codigoAlias
	 * @return DetalleTransferencia
	 * @throws TransferenciaException
	 * @since 1.0
	 * @see {@link wcorp.bprocess.cuentas.TransferenciasFondosBean#transfiereCtaCteHaciaCtaCte(String, String, String, String, double, long, char, String, String, String, String, String)
	 */
	public DetalleTransferencia transfiereCtaCteHaciaCtaCte(
			String ctaOrigen,
			String tipoCuentaOrigen,
			String ctaDestino,
			String tipoCuentaDestino,
			double monto,
			long rutDestino,
			char digDestino,
			Cliente cliente,
			String canal,
			String comentario,
			String nombreDestino,
			String eMailCliente,
			String eMailDestino,
			String tipoAutenticacion,
			String tipoTransferencia,
			String codigoAlias) throws TransferenciaException {
        
        DetalleTransferencia detalleTransferencia = new DetalleTransferencia();
        detalleTransferencia.setCtaOrigen(ctaOrigen);
        detalleTransferencia.setTipoCuentaOrigen(tipoCuentaOrigen);
        detalleTransferencia.setCtaDestino(ctaDestino);
        detalleTransferencia.setTipoCuentaDestino(tipoCuentaDestino);
        detalleTransferencia.setMontoTransferencia(monto);
        detalleTransferencia.setRutDestino(rutDestino);
        detalleTransferencia.setDigDestino(digDestino);
        detalleTransferencia.setDatosDelCliente(cliente);
        detalleTransferencia.setCanalID(canal);
        detalleTransferencia.setComentario(comentario);
        detalleTransferencia.setNombreDestino(nombreDestino);
        detalleTransferencia.setEMailCliente(eMailCliente);
        detalleTransferencia.setEMailDestino(eMailDestino);
        detalleTransferencia.setTipoAutenticacion(tipoAutenticacion);
        detalleTransferencia.setTipoTransferencia(tipoTransferencia);
        detalleTransferencia.setCodigoAlias(codigoAlias);
        detalleTransferencia.setNemonicoCargo(TablaValores.getValor(NEMONICOS_CONFIG, "CargoTrfGenerico", "Desc"));
        detalleTransferencia.setNemonicoAbono(TablaValores.getValor(NEMONICOS_CONFIG, "AbonoTrfGenerico", "Desc"));
        return transfiereCtaCteHaciaCtaCte(detalleTransferencia);
	}

    /**
     * Transferencias desde Ctas. Corrientes (Primas) hacia Corrientes (Primas). Se crea m�todo para mantener 
     * compatibilidad con las clases que no utilizan objeto de transferencia para invocar al m�todo.
     *
     * Registro de Versiones:<ul>
     *
     * <li>1.0  01/07/2014, Sebasti�n S�nchez S.. (ADA Ltda.): Versi�n inicial.</li>
	 * </ul>
	 *
	 *
	 * @param ctaOrigen
	 * @param tipoCuentaOrigen
	 * @param ctaDestino
	 * @param tipoCuentaDestino
	 * @param monto
	 * @param rutDestino
	 * @param digDestino
	 * @param cliente
	 * @param canal
	 * @param comentario
	 * @param nombreDestino
	 * @param eMailCliente
	 * @param eMailDestino
	 * @param tipoAutenticacion
	 * @param tipoTransferencia
	 * @param codigoAlias
         * @param nemonicoCargo mnem�nico de la glosa de cargo.
         * @param nemonicoAbono mnem�nico de la glosa de abono.
	 * @return DetalleTransferencia
	 * @throws TransferenciaException
	 * @since 3.9
	 */
	public DetalleTransferencia transfiereCtaCteHaciaCtaCte(
			String ctaOrigen,
			String tipoCuentaOrigen,
			String ctaDestino,
			String tipoCuentaDestino,
			double monto,
			long rutDestino,
			char digDestino,
			Cliente cliente,
			String canal,
			String comentario,
			String nombreDestino,
			String eMailCliente,
			String eMailDestino,
			String tipoAutenticacion,
			String tipoTransferencia,
			String codigoAlias,
			String nemonicoCargo,
			String nemonicoAbono) throws TransferenciaException {
         DetalleTransferencia detalle = new DetalleTransferencia();
         detalle.setCtaOrigen(ctaOrigen);
         detalle.setTipoCuentaOrigen(tipoCuentaOrigen);
         detalle.setCtaDestino(ctaDestino);
         detalle.setTipoCuentaDestino(tipoCuentaDestino);
         detalle.setMontoTransferencia(monto);
         detalle.setRutDestino(rutDestino);
         detalle.setDigDestino(digDestino);
         detalle.setDatosDelCliente(cliente);
         detalle.setCanalID(canal);
         detalle.setComentario(comentario);
         detalle.setNombreDestino(nombreDestino);
         detalle.setEMailCliente(eMailCliente);
         detalle.setEMailDestino(eMailDestino);
         detalle.setTipoAutenticacion(tipoAutenticacion);
         detalle.setTipoTransferencia(tipoTransferencia);
         detalle.setCodigoAlias(codigoAlias);
         detalle.setNemonicoCargo(nemonicoCargo);
         detalle.setNemonicoAbono(nemonicoAbono);
         return transfiereCtaCteHaciaCtaCte(detalle);
    }

    /**
     * Transferencias desde Ctas. Corrientes (Primas) hacia Corrientes (Primas). Se le a�ade a la firma el nemonicoCargo y nemonicoAbono.
     *
     * Registro de Versiones:<ul>
     *
     * <li>1.0  28/09/2011, Carlos Cerda I. (Orand): Versi�n inicial.
     * <li>1.1  30/09/2010, Leonardo Espinoza (ORAND): Se agrega el uso de Trace Number como identificador de transacci�n, y se agrega la l�gica de registro al sistema
     *                      Store & Forward cuando la transacci�n no es exitosa. Se elimina flag montoValido, ya que al eliminar bloque try catch que envolvia a las demas acciones (validar monto, obtener tracenumber, etc)
     *                      dej� de ser necesario, ya que ahora se actualiza el estado de la transferencia a error en los catch de cada acci�n del m�todo.
     * <li>1.1  28/09/2011, Carlos Cerda I. (Orand): Se a�aden a la firma los mnem�nicos para el cargo y abono y se modifica el nombre de la transaccion de reversa a inyectar en S&F para trf mismo banco.
     * <li>1.3  20/03/2014, Sebasti�n S�nchez S. (ADA Ltda.): Se reemplazan parametros del m�todo por un objeto 
     *          de transferencia. Se agrega invocaci�n a evaluaci�n de riesgo en Actimize.
     * <li>1.4  19/11/2014, Luis L�pez Alamos (SEnTRA): Se agrega envio de correo para cuando viene seleccionado
     *          el servicio de regalo virtual.</li>
     * <li>1.5 01/12/2015, Luis L�pez Alamos (SEnTRA): Se agrega respuesta de evaluador de fraudes de 
     *          transferencias de fondos y se modifica m�todo de envio de correo para egifts.</li>
	 * </ul>
	 *
	 *
	 * @param ctaOrigen
	 * @param tipoCuentaOrigen
	 * @param ctaDestino
	 * @param tipoCuentaDestino
	 * @param monto
	 * @param rutDestino
	 * @param digDestino
	 * @param cliente
	 * @param canal
	 * @param comentario
	 * @param nombreDestino
	 * @param eMailCliente
	 * @param eMailDestino
	 * @param tipoAutenticacion
	 * @param tipoTransferencia
	 * @param codigoAlias
         * @param nemonicoCargo mnem�nico de la glosa de cargo.
         * @param nemonicoAbono mnem�nico de la glosa de abono.
	 * @return DetalleTransferencia
	 * @throws TransferenciaException
	 * @since 1.0
     * @see {@link wcorp.bprocess.cuentas.TransferenciasFondosBean#transfiereCtaCteHaciaCtaCte(String, String, String, String, double, long, char, String, String, String, String, String)
	 */
    public DetalleTransferencia transfiereCtaCteHaciaCtaCte( DetalleTransferencia detalle) throws TransferenciaException {
        
        Cliente cliente = detalle.getDatosDelCliente();
        String canal = detalle.getCanalID();
        
        logCliente(cliente, canal, "transfiereCtaCteHaciaCtaCte", "Inicio M�todo");
        
        String ctaOrigen = detalle.getCtaOrigen();
        String tipoCuentaOrigen = detalle.getTipoCuentaOrigen();
        String ctaDestino = detalle.getCtaDestino();
        String tipoCuentaDestino = detalle.getTipoCuentaDestino();
        double monto = detalle.getMontoTransferencia();
        long rutDestino = detalle.getRutDestino();
        char digDestino = detalle.getDigDestino();
        String comentario = detalle.getComentario();
        String nombreDestino = detalle.getNombreDestino();
        String eMailCliente = detalle.getEMailCliente();
        String eMailDestino = detalle.getEMailDestino();
        String tipoAutenticacion = detalle.getTipoAutenticacion();
        String tipoTransferencia = detalle.getTipoTransferencia();
        String codigoAlias = detalle.getCodigoAlias();
        String nemonicoCargo = detalle.getNemonicoCargo();
        String nemonicoAbono = detalle.getNemonicoAbono();
        
        if(nemonicoCargo == null){
            nemonicoCargo = TablaValores.getValor(NEMONICOS_CONFIG, "CargoTrfGenerico", "Desc");
        }
        if(nemonicoAbono == null){
            nemonicoAbono = TablaValores.getValor(NEMONICOS_CONFIG, "AbonoTrfGenerico", "Desc");
        }
        
		String mensaje= "INICIO TRANSFERENCIA";
		logCliente(
				"CTACTE_CTACTE",
				ctaOrigen,
				tipoCuentaOrigen,
				ctaDestino,
				tipoCuentaDestino,
				cliente.getRut(),
				cliente.getDigito(),
				canal,
				monto,
				rutDestino,
				digDestino,
				"016",
				mensaje);

		CuentaCorriente cuentaCorriente = null;
		SaldoCtaCte saldo = null;

		ctaOrigen = StringUtil.rellenaPorLaIzquierda(ctaOrigen, 8, '0');
		ctaDestino = StringUtil.rellenaPorLaIzquierda(ctaDestino, 8, '0');

        String producto = TablaValores.getValor(STORE_FORWARD_CONFIG, "TransferenciasPersonas", "producto");
        logCliente(cliente,canal,"transfiereCtaCteHaciaCtaCte", "Producto [" + producto + "]");
        int intentos = Integer.parseInt(TablaValores.getValor(STORE_FORWARD_CONFIG, "TransferenciasPersonas"
                        , "intentos"));
        logCliente(cliente,canal,"transfiereCtaCteHaciaCtaCte", "Re-Intentos [" + intentos + "]");

        /*
         * Determinar si la transferencia es programada
         */
		boolean esProgramada = false;

		if(tipoTransferencia.equalsIgnoreCase("PRG")){
			logCliente(cliente,canal,"transfiereCtaCteHaciaCtaCte", "Es Transferencia Programada...");
			esProgramada = true;
		}

        // fecha de hoy
                Date fecha = new Date();

        /*
         * Obtener saldo
         */
		try {
			cuentaCorriente = new CuentaCorriente(ctaOrigen);
			saldo = cuentaCorriente.getSaldo();
            logCliente(cliente,canal,"transfiereCtaCteHaciaCtaCte", "Se obtuvo saldo = " + saldo);
		}
		catch (Exception e) {
			mensaje=
				"SaldoCtaCte"
				+ ", CuentaOrigen =["
				+ ctaOrigen
				+ "]"
				+ ", CuentaDestino=["
				+ ctaDestino
				+ "]"
				+ ", Monto=["
				+ monto
				+ "]: ";
			logCliente(cliente,canal,"CTACTE_CTACTE", mensaje+e);
			if(esProgramada){
				if(e instanceof GeneralException){
					GeneralException ge = (GeneralException)e;
					throw new TransferenciaException(ge.getCodigo());
				}
			}
			throw new TransferenciaException("TRF011");
		}

        /*
         * Obtenemos el monto total, suma de las transferencias programadas para el dia de hoy
         */
		double montoProgramado= 0.0;

		if(!esProgramada){
			logCliente(cliente,canal,"CTACTE_CTACTE", "Obtenemos monto programado");
			try {
				montoProgramado = montoProgramadoDia(cliente.getRut(),tipoCuentaOrigen, ctaOrigen, new Date());
                logCliente(cliente,canal,"transfiereCtaCteHaciaCtaCte", "Se obtuvo montoProgramado = "
				+ montoProgramado);
			}
			catch (Exception e) {
				mensaje=
					"MontoProgramado"
					+ ", CuentaOrigen =["
					+ ctaOrigen
					+ "]"
					+ ", CuentaDestino=["
					+ ctaDestino
					+ "]"
					+ ", Monto=["
					+ monto
					+ "]: ";
				logCliente(cliente,canal,"CTACTE_CTACTE", mensaje+e);
				throw new TransferenciaException("TRF001");
			}
		}

        /*
         * 1er criterio de verificaci�n previa a realizar la transferencia: �la cuenta tiene cantidad disponible?
         */
		if (!validarSaldoCtaCte(cliente,canal,ctaOrigen, monto + montoProgramado, true)) {
			throw new TransferenciaException("0020");
		}
        logCliente(cliente,canal,"transfiereCtaCteHaciaCtaCte", "Pas� validaci�n de disponibilidad en cuenta ");

        /*
         * Primer tracking de la transferencia: "inicio"
         */
		RegistroTransferencia registro = null;

		try {
			registro = ingresaTransferencia(cliente,
					tipoCuentaOrigen,
					ctaOrigen,
					monto,
					fecha,
					TablaValores.getValor(TTFF_CONFIG, "estados", "ingreso"),
					tipoCuentaDestino,
					ctaDestino,
					rutDestino,
					codigoBancoBCI,
					canal,
					null,
					null,
					comentario,
					nombreDestino,
					null,
					eMailDestino,
					String.valueOf(rutDestino),
					tipoTransferencia,
					codigoAlias);
            logCliente(cliente,canal,"transfiereCtaCteHaciaCtaCte", "Realiz� 1er tracking [ingreso]");
		}
		catch (Exception e) {
			mensaje=
				"REG_ITF"
				+ ", CuentaOrigen =["
				+ ctaOrigen
				+ "]"
				+ ", CuentaDestino=["
				+ ctaDestino
				+ "]"
				+ ", Monto=["
				+ monto
				+ "]: ";
			logCliente(cliente,canal,"CTACTE_CTACTE", mensaje+e);
			throw new TransferenciaException("TRF001");
		}

                String traceNumber = new String();

        /*
         * Obtenci�n de tope banco
         */
			double topeBanco= 0;
			try {
				topeBanco = maximoTopeCuenta(
						cliente.getRut(),
						canal,
						tipoCuentaOrigen,
						rutDestino,
						tipoAutenticacion);
            logCliente(cliente,canal,"transfiereCtaCteHaciaCtaCte", "Se obtuvo tope-banco = " + topeBanco);
			}
			catch (Exception e) {
				logCliente(cliente,canal,"CTACTE_CTACTE", "Fallo por no poder obtener TOPE BANCO: "+e);
				registraFinTrf(
						cliente,
						canal,
						Long.parseLong(registro.numeroOperacion),
						null,
						null,
						fecha,
						TablaValores.getValor(TTFF_CONFIG, "estados", "finError"));
				throw new TransferenciaException("TRF001");
			}

        /*
         * Obtener monto total de las transacciones (no programadas) del d�a
         */
			double montoDia= 0;
			try {
				montoDia = montoTransferidoDia(cliente.getRut(),tipoCuentaOrigen, ctaOrigen, new Date());
        logCliente(cliente,canal,"transfiereCtaCteHaciaCtaCte", "Se obtuvo montoDia = " + montoDia);
			}
			catch (Exception e) {
				logCliente(cliente,canal,"CTACTE_CTACTE", "Fallo por no poder obtener MONTO TRANSFERIDO DIA: "+e);
				registraFinTrf(
						cliente,
						canal,
						Long.parseLong(registro.numeroOperacion),
						null,
						null,
						fecha,
						TablaValores.getValor(TTFF_CONFIG, "estados", "finError"));
				throw new TransferenciaException("TRF001");
			}

        /*
         * 2do criterio de verificaci�n previa a realizar la transferencia:
         * que la transferencia no haga exceder el tope-banco diario
         */
			if (cliente.getRut() != rutDestino) {
				if (monto > (topeBanco - montoDia - montoProgramado)) {
					mensaje=
						"ValidaMonto"
						+ ", Monto="
						+ monto
						+ ", TopeBanco="
						+ topeBanco
						+ ", montoDia="
						+ montoDia
						+ ", montoProgramado="
						+ montoProgramado;
					logCliente(cliente,canal,"CTACTE_CTACTE", mensaje);
            // Tracking de transferencia: "finError"
					registraFinTrf(
							cliente,
							canal,
							Long.parseLong(registro.numeroOperacion),
							null,
							null,
							fecha,
							TablaValores.getValor(TTFF_CONFIG, "estados", "finError"));
					throw new TransferenciaException("TRF042");
				}
			}
		logCliente(cliente,canal,"transfiereCtaCteHaciaCtaCte", "Pas� validaci�n de tope banco");

		/*
		 * Se obtiene el Trace Number
		 */
		try {
			traceNumber = getTraceNumber();
		}
		catch (Exception e){
			logCliente(cliente,canal,"transfiereCtaCteHaciaCtaCte",
			        "Exception al obtener Trace Number SE ABORTA!!!!: "+e.toString());
			throw new TransferenciaException("TRF001");
		}
		logCliente(cliente,canal,"transfiereCtaCteHaciaCtaCte", "traceNumber=" + traceNumber);

        ResultadoEvaluacionTTFFTO resultadoEvaluacion = 
                EvaluacionRiesgoTTFFHelper.evaluarTransferencia(detalle, traceNumber);

        if (resultadoEvaluacion!=null && isValidaDisponibilidadServicioFraude(canal)){
            if (ResultadoEvaluacionTTFFTO.DECLINE.equalsIgnoreCase(resultadoEvaluacion.getAccion())){
                logCliente(cliente,canal,"transfiereCtaCteHaciaCtaCte", "Rechazo de TTFF por posible fraude");
                throw new TransferenciaException("TRF0090");
            }
            logCliente(cliente,canal,"transfiereCtaCteHaciaCtaCte", "Continuo flujo normal de TTFF");
        }
        
        
		/*
		 * 2do tracking: pasa a estado "traceNumber"
		 */
		String estadoTra = TablaValores.getValor(TTFF_CONFIG, "estados", "traceNumber");
		boolean modificoEstado = registraFinTrf(
				cliente,
				canal,
				Long.parseLong(registro.numeroOperacion),
				traceNumber,
				null,
				fecha,
				estadoTra);
		logCliente(cliente,canal,"transfiereCtaCteHaciaCtaCte", "MODIFICO ESTADO=" + modificoEstado);
		if (modificoEstado) {
			logCliente(cliente,canal,"transfiereCtaCteHaciaCtaCte", "Se actualiz� a estado: "+estadoTra);
		}
		else {
			logCliente(cliente,canal,"transfiereCtaCteHaciaCtaCte", "No se actualiz� a estado: "+estadoTra);
		}

		/*
		 * Realiza la transferencia
		 */
			logCliente(cliente,canal,
					"CTACTE_CTACTE",
					"EfectuandoTransferencia, MontoDia="
					+ montoDia
					+ ", TopeBanco="
					+ topeBanco
					+ ", Programado="
					+ montoProgramado);

        try {
            realizarTransferenciaCtaCteHaciaCtaCte(
                    cliente,
                    canal,
                    ctaOrigen,
                    traceNumber,
                    monto,
                    ctaDestino,
                    nemonicoCargo,
                    nemonicoAbono);
 
            logCliente(cliente,canal,"transfiereCtaCteHaciaCtaCte",
                    "Realiz� transferencia [rut-cliente = \"" + cliente.getRut() + "\", canal = \""
                            + canal + "\", ctaOrigen = \"" + ctaOrigen + "\", traceNumber = \""
                            + traceNumber + "\", monto = \"" + monto + "\", ctaDestino = \"" + ctaDestino + "\"]");
        }
        catch (Exception e) {
            logCliente(cliente,canal,"transfiereCtaCteHaciaCtaCte",
                    "Exception al realizarTransferenciaCtaCteHaciaCtaCte: "+e.getMessage());
            boolean insertoOk = registraTransaccionPendiente(cliente,
		   canal,
                    e,
                    "B2819",
                    ctaOrigen,
                    ctaDestino,
                    monto,
                    traceNumber,
                    producto,
                    null,
                    intentos,
                    true,
                    null,
		    null,
		    null,
                    null,
                    nemonicoCargo,
                    nemonicoAbono);
            logCliente(cliente,canal,"transfiereCtaCteHaciaCtaCte", "registr� transaccion pendiente cargo: "
                    +insertoOk);

            throw new TransferenciaException("TRF001");
		}

        /*
         * Tracking de transferencia finalizada correctamente
         */
		registraFinTrf(
				cliente,
				canal,
				Long.parseLong(registro.numeroOperacion),
				null,
				null,
				fecha,
				TablaValores.getValor(TTFF_CONFIG, "estados", "fin"));

        /*
         * Genera y devuelve el detalle de la transferencia
         */
        
		detalle.ctaOrigen= ctaOrigen;
		detalle.tipoCuentaOrigen= tipoCuentaOrigen;
		detalle.ctaDestino= ctaDestino;
		detalle.tipoCuentaDestino= tipoCuentaDestino;
		detalle.montoTransferencia= monto;
		detalle.numeroOperacion= registro.numeroOperacion;
		detalle.codigoBancoDestino= codigoBancoBCI;
		detalle.codigoBancoOrigen= codigoBancoBCI;
		detalle.rut= cliente.getRut();
		detalle.dig= cliente.getDigito();
		detalle.rutDestino= rutDestino;
		detalle.digDestino= digDestino;
		detalle.nombreDestino= nombreDestino;
		detalle.comentario= comentario;
		detalle.canalID= canal;
		detalle.eMailCliente= eMailCliente;
		detalle.eMailDestino= eMailDestino;
		detalle.fechaTransferencia= fecha;
		detalle.fechaCargo= fecha;
		detalle.fechaAbono= fecha;
		logCliente(cliente,canal,"CTACTE_CTACTE", "Cliente seteado como atributo");
		detalle.nombreOrigen = cliente.getFullName();

		detalle.nombreBancoOrigen = TablaValores.getValor(TTFF_CONFIG, "codigoBCI", "desc_" + canal);
		detalle.nombreBancoDestino = TablaValores.getValor(TTFF_CONFIG, "codigoBCI", "desc_" + canal);
        detalle.setResultadoEvaluacionTTFFTO(resultadoEvaluacion);

        if(!esProgramada&&!detalle.getEsEgift()){
			logCliente(cliente,canal,"CTACTE_CTACTE", "No se viene de TTTFF Programadas, se envian los correos");
			enviaEmail(detalle, false, cliente, canal);
			enviaMailCliente(detalle, cliente, canal);
        }else if (detalle.getEsEgift()){
            envioMailEgiftOrigen(detalle, cliente, canal);
            envioMailEgiftDestino(detalle, cliente, canal);
		}
		logCliente(cliente,canal,"CTACTE_CTACTE", "FIN TRANSFERENCIA");
		return detalle;
	}

	/**
	 * Transfiere desde Cuenta Corriente (Prima) hacia Cuenta de Ahorro.
	 * Se crea m�todo para mantener compatibilidad con las clases que no utilizan objeto de transferencia para 
     * invocar al m�todo.
	 *
	 * Registro de Versiones:<ul>
	 *
	 * <li>1.0  01/07/2014, Sebasti�n S�nchez S. (ADA Ltda.): Versi�n inicial.
	 * </ul>
	 *
	 *
	 * @param ctaOrigen,
	 * @param tipoCuentaOrigen,
	 * @param ctaDestino,
	 * @param monto,
	 * @param rutDestino,
	 * @param digDestino,
	 * @param cliente,
	 * @param canal,
	 * @param comentario,
	 * @param nombreDestino,
	 * @param eMailCliente,
	 * @param eMailDestino,
	 * @param tipoAutenticacion,
	 * @param tipoTransferencia,
	 * @param codigoAlias
	 * @return DetalleTransferencia
	 * @throws TransferenciaException
	 * @since 3.9
	 */
	public DetalleTransferencia transfiereCtaCteHaciaAhorro(
			String ctaOrigen,
			String tipoCuentaOrigen,
			String ctaDestino,
			double monto,
			long rutDestino,
			char digDestino,
			Cliente cliente,
			String canal,
			String comentario,
			String nombreDestino,
			String eMailCliente,
			String eMailDestino,
			String tipoAutenticacion,
			String tipoTransferencia,
			String codigoAlias) throws TransferenciaException {

	    DetalleTransferencia detalle = new DetalleTransferencia();
	    detalle.setCtaOrigen(ctaOrigen);
	    detalle.setTipoCuentaOrigen(tipoCuentaOrigen);
	    detalle.setCtaDestino(ctaDestino);
	    detalle.setMontoTransferencia(monto);
	    detalle.setRutDestino(rutDestino);
	    detalle.setDigDestino(digDestino);
	    detalle.setDatosDelCliente(cliente);
	    detalle.setCanalID(canal);
	    detalle.setComentario(comentario);
	    detalle.setNombreDestino(nombreDestino);
	    detalle.setEMailCliente(eMailCliente);
	    detalle.setEMailDestino(eMailDestino);
	    detalle.setTipoAutenticacion(tipoAutenticacion);
	    detalle.setTipoTransferencia(tipoTransferencia);
	    detalle.setCodigoAlias(codigoAlias);
        return transfiereCtaCteHaciaAhorro(detalle);
    }


    /**
     * Transfiere desde Cuenta Corriente (Prima) hacia Cuenta de Ahorro.
	 * Se trae m�todo desde {@link wcorp.bprocess.cuentas.TransferenciasFondosBean#} a la implementaci�n Stateless del mismo y
	 * se modifica la excepci�n y se agrega par�metro boolean 'esProgramada' para soportar las Transferencias Programadas en L�nea.
	 *
	 * Registro de Versiones:<ul>
	 *
	 * <li>1.0  26/03/2009, Carlos Cerda I. (SEnTRA): Versi�n inicial.
     * <li>1.1  20/03/2014  Sebasti�n S�nchez S. (ADA Ltda.): Se reemplazan parametros del m�todo por un 
     *          objeto de transferencia. Se agrega invocaci�n a evaluaci�n de riesgo en Actimize. Se agrega 
     *          traceNumber. </li>
     * <li>1.2 19/11/2014 Luis L�pez Alamos (SEnTRA): Se agrega llamada a m�todo {@link #envioMailEgift} en caso
     *         de realizarse la transferencia con regalo virtual.</li>
     * <li>1.3 01/12/2015, Luis L�pez Alamos (SEnTRA): Se agrega respuesta de evaluador de fraudes de 
     *          transferencias de fondos y se modifica m�todo de envio de correo para egifts.</li>
	 * </ul>
	 *
	 *
     * @param detalle Informaci�n de la transferencia
	 * @return DetalleTransferencia
	 * @throws TransferenciaException
	 * @since 1.0
	 * @see {@link wcorp.bprocess.cuentas.TransferenciasFondosBean#transfiereCtaCteHaciaAhorro(String, String, String, double, long, char, String, String, String, String, String)}
	 */
    public DetalleTransferencia transfiereCtaCteHaciaAhorro( DetalleTransferencia detalle) throws TransferenciaException {
        
        Cliente cliente = detalle.getDatosDelCliente();
        String canal = detalle.getCanalID();
        
        logCliente(cliente, canal, "transfiereCtaCteHaciaAhorro", "Inicio M�todo");
        
        String ctaOrigen = detalle.getCtaOrigen();
        String tipoCuentaOrigen = detalle.getTipoCuentaOrigen();
        String ctaDestino = detalle.getCtaDestino();
        double monto = detalle.getMontoTransferencia();
        long rutDestino = detalle.getRutDestino();
        char digDestino = detalle.getDigDestino();
        String comentario = detalle.getComentario();
        String nombreDestino = detalle.getNombreDestino();
        String eMailCliente = detalle.getEMailCliente();
        String eMailDestino = detalle.getEMailDestino();
        String tipoAutenticacion = detalle.getTipoAutenticacion();
        String tipoTransferencia = detalle.getTipoTransferencia();
        String codigoAlias = detalle.getCodigoAlias();

		String tipoCuentaDestino = TablaValores.getValor(TTFF_CONFIG, "tiposCuenta", "ahorro");
		boolean esProgramada = false;

		if(tipoTransferencia.equalsIgnoreCase("PRG")){
			logCliente(cliente,canal,"transfiereCtaCteHaciaCtaCte", "Es Transferencia Programada...");
			esProgramada = true;
		}

		String mensaje= "INICIO TRANSFERENCIA";
		logCliente(
				"transfiereCtaCteHaciaAhorro",
				ctaOrigen,
				tipoCuentaOrigen,
				ctaDestino,
				tipoCuentaDestino,
				cliente.getRut(),
				cliente.getDigito(),
				canal,
				monto,
				rutDestino,
				digDestino,
				"016",
				mensaje);

		CuentaCorriente cuentaCorriente= null;
		SaldoCtaCte saldo= null;

		ctaOrigen = StringUtil.rellenaPorLaIzquierda(ctaOrigen, 8, '0');

		Date fecha = new Date();

		try {
			cuentaCorriente = new CuentaCorriente(ctaOrigen);
			saldo = cuentaCorriente.getSaldo();
		} catch (Exception e) {
			mensaje=
				"SaldoCtaCte"
				+ ", CuentaOrigen =["
				+ ctaOrigen
				+ "]"
				+ ", CuentaDestino=["
				+ ctaDestino
				+ "]"
				+ ", Monto=["
				+ monto
				+ "]: ";
			logCliente(cliente,canal,"transfiereCtaCteHaciaAhorro", mensaje+e);
			if(esProgramada){
				if(e instanceof GeneralException){
					GeneralException ge = (GeneralException)e;
					throw new TransferenciaException(ge.getCodigo());
				}
			}
			throw new TransferenciaException("TRF001");
		}

		double montoProgramado= 0.0;
		if(!esProgramada){
			logCliente(cliente,canal,"transfiereCtaCteHaciaAhorro", "Obtenemos monto programado");
			try {
				montoProgramado = montoProgramadoDia(cliente.getRut(),tipoCuentaOrigen, ctaOrigen, new Date());
			} catch (Exception e) {
				mensaje=
					"MontoProgramado"
					+ ", CuentaOrigen =["
					+ ctaOrigen
					+ "]"
					+ ", CuentaDestino=["
					+ ctaDestino
					+ "]"
					+ ", Monto=["
					+ monto
					+ "]: ";
				logCliente(cliente,canal,"transfiereCtaCteHaciaAhorro", mensaje+e);
				throw new TransferenciaException("TRF001");
			}
		}

		if (!validarSaldoCtaCte(cliente,canal,ctaOrigen, monto + montoProgramado,true)) {
			throw new TransferenciaException("0020");
		}

		RegistroTransferencia registro = null;
		try {
			registro = ingresaTransferencia(cliente,
					tipoCuentaOrigen,
					ctaOrigen,
					monto,
					fecha,
					TablaValores.getValor(TTFF_CONFIG, "estados", "ingreso"),
					tipoCuentaDestino,
					ctaDestino,
					rutDestino,
					codigoBancoBCI,
					canal,
					null,
					null,
					comentario,
					nombreDestino,
					null,
					eMailDestino,
					String.valueOf(rutDestino),
					tipoTransferencia,
					codigoAlias);
		} catch (Exception e) {
			mensaje=
				"REG_ITF"
				+ ", CuentaOrigen =["
				+ ctaOrigen
				+ "]"
				+ ", CuentaDestino=["
				+ ctaDestino
				+ "]"
				+ ", Monto=["
				+ monto
				+ "]: ";
			logCliente(cliente,canal,"transfiereCtaCteHaciaAhorro", mensaje+e);
			throw new TransferenciaException("TRF001");
		}

		boolean montoValido= true;
        ResultadoEvaluacionTTFFTO resultadoEvaluacion = null;
		try {
			double topeBanco= 0.0;
			try {
				topeBanco = maximoTopeCuenta(
						cliente.getRut(),
						canal,
						tipoCuentaOrigen,
						rutDestino,
						tipoAutenticacion);
			} catch (Exception e) {
				logCliente(cliente,canal,"transfiereCtaCteHaciaAhorro", "Fallo por no poder obtener TOPE BANCO: "+e);
				registraFinTrf(
						cliente,
						canal,
						Long.parseLong(registro.numeroOperacion),
						null,
						null,
						fecha,
						TablaValores.getValor(TTFF_CONFIG, "estados", "finError"));
				montoValido = false;
				throw new TransferenciaException("TRF001");
			}

			double montoDia= 0.0;
			try {
				montoDia= montoTransferidoDia(cliente.getRut(),tipoCuentaOrigen, ctaOrigen, new Date());
			} catch (Exception e) {
				logCliente(cliente,canal,"transfiereCtaCteHaciaAhorro", "Fallo por no poder obtener MONTO TRANSFERIDO DIA; "+e);
				registraFinTrf(
						cliente,
						canal,
						Long.parseLong(registro.numeroOperacion),
						null,
						null,
						fecha,
						TablaValores.getValor(TTFF_CONFIG, "estados", "finError"));
				montoValido = false;
				throw new TransferenciaException("TRF001");
			}

			if (cliente.getRut() != rutDestino) {
				if (monto > (topeBanco - montoDia - montoProgramado)) {
					mensaje=
						"ValidaMonto"
						+ ", Monto="
						+ monto
						+ ", TopeBanco="
						+ topeBanco
						+ ", montoDia="
						+ montoDia
						+ ", montoProgramado="
						+ montoProgramado;
					logCliente(cliente,canal,"transfiereCtaCteHaciaAhorro", mensaje);
					registraFinTrf(
							cliente,
							canal,
							Long.parseLong(registro.numeroOperacion),
							null,
							null,
							fecha,
							TablaValores.getValor(TTFF_CONFIG, "estados", "finError"));
					montoValido = false;
					throw new TransferenciaException("TRF042");
				}
			}
            
            String traceNumber = "";
            
            /*
             * Se obtiene el Trace Number para WFD.
             */
            try {
                traceNumber = getTraceNumber();
            }
            catch (Exception e) {
                logCliente(cliente, canal, "transfiereCtaCteHaciaAhorro",
                        "Exception al obtener Trace Number SE ABORTA!!!!: " + e.toString());
                throw new TransferenciaException("TRF001");
            }
            logCliente(cliente, canal, "transfiereCtaCteHaciaAhorro", "traceNumber=" + traceNumber);
            
            /*
             * 2do tracking: pasa a estado "traceNumber"
             */
            String estadoTra = TablaValores.getValor(TTFF_CONFIG, "estados", "traceNumber");
            boolean modificoEstado = registraFinTrf(cliente, canal, Long.parseLong(registro.numeroOperacion),
                    traceNumber, null, fecha, estadoTra);
            logCliente(cliente, canal, "transfiereCtaCteHaciaAhorro", "MODIFICO ESTADO=" + modificoEstado);
            if (modificoEstado) {
                logCliente(cliente, canal, "transfiereCtaCteHaciaAhorro", "Se actualiz� a estado: " + estadoTra);
            }
            else {
                logCliente(cliente, canal, "transfiereCtaCteHaciaAhorro", "No se actualiz� a estado: " + estadoTra);
            }
            
            resultadoEvaluacion = EvaluacionRiesgoTTFFHelper.evaluarTransferencia(detalle,
                    traceNumber);
            
            if (resultadoEvaluacion!=null && isValidaDisponibilidadServicioFraude(canal)){
                if (ResultadoEvaluacionTTFFTO.DECLINE.equalsIgnoreCase(resultadoEvaluacion.getAccion())){
                    logCliente(cliente,canal,"transfiereCtaCteHaciaAhorro", "Rechazo de TTFF por posible fraude");
                    throw new TransferenciaException("TRF0090");
                }
                logCliente(cliente,canal,"transfiereCtaCteHaciaAhorro", "Continuo flujo normal de TTFF");
            }
            
			logCliente(cliente,canal,"transfiereCtaCteHaciaAhorro","EfectuandoTransferencia, MontoDia="
					+ montoDia
					+ ", TopeBanco="
					+ topeBanco
					+ ", Programado="
					+ montoProgramado);
			realizarTransferenciaCtaCteHaciaAhorro(cliente,canal,ctaOrigen, monto, ctaDestino);
		} catch (TransferenciaException e) {
			if (montoValido)
				registraFinTrf(
						cliente,
						canal,
						Long.parseLong(registro.numeroOperacion),
						null,
						null,
						fecha,
						TablaValores.getValor(TTFF_CONFIG, "estados", "finError"));
			throw e;
		}

		registraFinTrf(
				cliente,
				canal,
				Long.parseLong(registro.numeroOperacion),
				null,
				null,
				fecha,
				TablaValores.getValor(TTFF_CONFIG, "estados", "fin"));

		detalle.ctaOrigen= ctaOrigen;
		detalle.tipoCuentaOrigen= tipoCuentaOrigen;
		detalle.ctaDestino= ctaDestino;
		detalle.tipoCuentaDestino= tipoCuentaDestino;
		detalle.montoTransferencia= monto;
		detalle.numeroOperacion= registro.numeroOperacion;
		detalle.codigoBancoDestino= codigoBancoBCI;
		detalle.codigoBancoOrigen= codigoBancoBCI;
		detalle.rut= cliente.getRut();
		detalle.dig= cliente.getDigito();
		detalle.rutDestino= rutDestino;
		detalle.digDestino= digDestino;
		detalle.nombreDestino= nombreDestino;
		detalle.comentario= comentario;
		detalle.canalID= canal;
		detalle.eMailCliente= eMailCliente; // Puede ser null
		detalle.eMailDestino= eMailDestino; // Puede ser null
		detalle.fechaTransferencia= fecha;
		detalle.fechaCargo= fecha;
		detalle.fechaAbono= fecha;

		logCliente(cliente,canal,"transfiereCtaCteHaciaAhorro", "Cliente seteado como atributo");
		detalle.nombreOrigen = cliente.getFullName();
		detalle.nombreBancoOrigen = TablaValores.getValor(TTFF_CONFIG, "codigoBCI", "desc_" + canal);
		detalle.nombreBancoDestino = TablaValores.getValor(TTFF_CONFIG, "codigoBCI", "desc_" + canal);
        detalle.setResultadoEvaluacionTTFFTO(resultadoEvaluacion);

        if(!esProgramada && !detalle.getEsEgift()){
			logCliente(cliente,canal,"transfiereCtaCteHaciaAhorro", "No se viene de TTTFF Programadas, se envian los correos");
			enviaEmail(detalle, false, cliente, canal);
			enviaMailCliente(detalle, cliente, canal);
        } else if (detalle.getEsEgift()){
            envioMailEgiftOrigen(detalle, cliente, canal);
            envioMailEgiftDestino(detalle, cliente, canal);
		}

		logCliente(cliente,canal,"transfiereCtaCteHaciaAhorro", "FIN TRANSFERENCIA");
		return detalle;
	}

    /**
     * Transferencia hacia otros bancos en l�nea para personas
     *
     * En este m�todo se debe recopilar todos los antecedentes necesarios para posteriormente efectuar la transferencia.
     * La idea es que cada caso que se valida es imprescindible para la transacci�n y si alguna de estas consultas falla
     * entonces el proceso se abortar� sin haber efectuado ning�n cargo, ni actualizaci�n alguna en las tablas correspondientes
     * minimizando la probabilidad de fallo de la transacci�n en su totalidad.
     *
     * Se trae m�todo desde {@link wcorp.bprocess.cuentas.TransferenciasFondosBean#} a la implementaci�n Stateless del mismo y
     * se modifica la excepci�n y se agrega variable boolean 'transferenciaProgramada' para soportar las Transferencias Programadas en L�nea.
     *
     * Registro de Versiones:<UL>
     *
     * <li>1.0 26/03/2009, Carlos Cerda I. (SEnTRA): Versi�n inicial.
     * <li>1.1 01/06/2009, Carlos Cerda I. (SEnTRA): Se agrega reversa en caso de excepci�n en el cargo al cliente. En caso de fallar esta reversa se
     *                                         reintentar� mas tarde.
     * <li>1.2 22/02/2010, Carlos Cerda I. (Orand): Se agrega par�metro tracenumber (identificador de la transacci�n) a la llamada a los m�todos: {@link #realizarTransferenciaCtaCteHaciaCtaCte(Cliente, String, String, String, double)},
     *                                      {@link #registraTransaccionPendiente(Cliente, String, Exception, String, String, String, double, String, String, String, int)}, {@link #realizaReversaCargo(Cliente, String, String, String, double)}
     *                                      y {@link #realizaReversaAbono(Cliente, String, String, double)}.
     * <li>1.3 29/03/2010, Jes�s Orellana. (Orand): Se modifica lo siguiente:
     *                                            <li> Se saca la obtenci�n de la cuenta saco desde la tabla de par�metros.
     *                                            <li> Se saca la cuenta saco como par�metro de entrada en  la llamada al tuxedo CtaCteTransfGen.
     *                                            <li> Se modifica la forma de recibir la respuesta ya que ahora no retorna la cuenta de destino como respuesta a la consulta sino la fecha contable de la transacci�n.
     *                                            <li> Se elimina el llamado al m�todo realizaReversaAbono().
     * <li>1.2 07/02/2012, Carlos Cerda I. (Orand): Se cambia la forma de llamar a los m�todos
     * {@link #enviarTrfOperador(Cliente, String, String, String)} y
     * {@link #construyeXFML(Cliente, String, String, String, String, double, String, Date, String, String, long,
     * char, String, String, long, char, String, String, String, String, String)} y se agrega l�gica para insertar
     * una consulta de transferencias de fondos al sistema StoreAndForward.
     * <li>1.5 18/10/2013 Sebasti�n S�nchez S. (ADA Ltda.): Se agrega l�gica para evaluar riesgo de fraude en
     *         la transferencia.
     * <li>1.6 03/01/2014 Sebasti�n S�nchez S. (ADA Ltda.): Se destruye variable de instancia al final del m�todo
     *         para que los clientes que reutilicen este EJB no encuentren informaci�n que no corresponde a su
     *         contexto.
     * <li>1.7 19/11/2014 Luis L�pez Alamos (SEnTRA): Se agrega llamada a m�todo {@link #envioMailEgift} en caso
     *         de realizarse la transferencia con regalo virtual.</li>
     * <li>1.8 01/12/2015, Luis L�pez Alamos (SEnTRA): Se agrega respuesta de evaluador de fraudes de 
     *          transferencias de fondos y se modifica m�todo de envio de correo para egifts.</li>        
     * <li>1.9 06/08/2015 Carlos Cerda I. (Kubos): Debido al cambio en la firma del m�todo 
     * {@link MessageAuthenticationCodeHelper#validaMessageAuthenticationCode(String, String, String, String, 
     * String, String, String, String, String, String, String, String, String)} de boolean a int, se modifica su 
     * llamado.      
     * <li>
     * </UL>
     *
     * @param ctaOrigen
     * @param tipoCuentaOrigen
     * @param ctaDestino
     * @param tipoCuentaDestino
     * @param monto
     * @param codBancoDestino
     * @param nombreBancoDestino
     * @param rutDestino
     * @param digDestino
     * @param nombreDestino
     * @param comentario
     * @param eMailCliente
     * @param eMailDestino
     * @param tipoAutenticacion
     * @param tipoTransferencia
     * @param codigoAlias
     * @return detalle
     * @throws TransferenciaException
     * @throws TuxedoException
     * @throws CCAException
     * @since 1.0
     * @see {@link wcorp.bprocess.cuentas.TransferenciasFondosBean#transCtaCteOtroBancoLineaPer(String, String, String, String, double, String, String, long, char, String, String, String, String, String, String, String)}
     */
    public DetalleTransferencia transCtaCteOtroBancoLineaPer(
            String ctaOrigen,
            String tipoCuentaOrigen,
            String ctaDestino,
            String tipoCuentaDestino,
            double monto,
            String codBancoDestino,
            String nombreBancoDestino,
            long rutDestino,
            char digDestino,
            Cliente cliente,
            String canal,
            String nombreDestino,
            String comentario,
            String eMailCliente,
            String eMailDestino,
            String tipoAutenticacion,
            String tipoTransferencia,
            String codigoAlias) throws TransferenciaException, TuxedoException, CCAException {

        try{

            logCliente(cliente, canal, "transCtaCteOtroBancoLineaPer", "transCtaCteOtroBancoLineaPer");
            String mensaje;
            boolean transferenciaProgramada = false;
            String producto = TablaValores.getValor(STORE_FORWARD_CONFIG, "TransferenciasPersonas", "producto");
            logCliente(cliente, canal, "transCtaCteOtroBancoLineaPer", "Producto [" + producto + "]");
            int intentos = Integer.parseInt(TablaValores.getValor(STORE_FORWARD_CONFIG, "TransferenciasPersonas",
                    "intentos"));
            logCliente(cliente, canal, "transCtaCteOtroBancoLineaPer", "Re-Intentos [" + intentos + "]");

            if (tipoTransferencia.equalsIgnoreCase("PRG")) {
                logCliente(cliente, canal, "transCtaCteOtroBancoLineaPer", "Es Transferencia Programada...");
                transferenciaProgramada = true;
            }

            CuentaCorriente cc = null;
            SaldoCtaCte saldo = null;
            try {
                logCliente(cliente, canal, "transCtaCteOtroBancoLineaPer", "Validando que la cuenta ["
                        + ctaOrigen + "] exista");
                cc = new CuentaCorriente(ctaOrigen);
                saldo = cc.getSaldo();
            }
            catch (Exception e) {
                mensaje =
                    "Problemas con la cuenta"
                    + ", CuentaOrigen =["
                    + ctaOrigen
                    + "]"
                    + ", CuentaDestino=["
                    + ctaDestino
                    + "]"
                    + ", Monto=["
                    + monto
                    + "]: ";
                logCliente(cliente,canal,"transCtaCteOtroBancoLineaPer", mensaje+e);
                if(transferenciaProgramada){
                    if(e instanceof GeneralException){
                        GeneralException ge = (GeneralException)e;
                        throw new TransferenciaException(ge.getCodigo());
                    }
                }
                throw new TransferenciaException("TRF011");
            }

            ctaOrigen = StringUtil.rellenaPorLaIzquierda(ctaOrigen, 8, '0');
            logCliente(cliente,canal,"transCtaCteOtroBancoLineaPer", "CTAORIGEN=" + ctaOrigen);

            /*
             * Determinamos el monto programado en el d�a
             */
            double montoProgramado= 0.0;
            if(!transferenciaProgramada){
                try {
                    montoProgramado = montoProgramadoDia(cliente.getRut(),tipoCuentaOrigen, ctaOrigen,
                            new Date());
                }
                catch (Exception e) {
                    mensaje=
                        "MontoProgramado"
                        + ", CuentaOrigen =["
                        + ctaOrigen
                        + "]"
                        + ", CuentaDestino=["
                        + ctaDestino
                        + "]"
                        + ", Monto=["
                        + monto
                        + "]: ";
                    logCliente(cliente,canal,"transCtaCteOtroBancoLineaPer", mensaje+e);
                    throw new TransferenciaException("TRF001");
                }
            }
            logCliente(cliente,canal,"transCtaCteOtroBancoLineaPer", "MONTOPRG=" + montoProgramado);
            /*
             * Tope del Banco
             */
            double topeBanco= 0;
            try {
                topeBanco = maximoTopeCuenta(cliente.getRut(), canal, tipoCuentaOrigen, -1, tipoAutenticacion);
            }
            catch (Exception e) {
                mensaje=
                    "TopeBanco"
                    + ", CuentaOrigen =["
                    + ctaOrigen
                    + "]"
                    + ", CuentaDestino=["
                    + ctaDestino
                    + "]"
                    + ", Monto=["
                    + monto
                    + "]: ";
                logCliente(cliente,canal,"transCtaCteOtroBancoLineaPer", mensaje+e);
                throw new TransferenciaException("TRF001");
            }
            logCliente(cliente,canal,"transCtaCteOtroBancoLineaPer", "TOPEBANCO=" + topeBanco);

            /*
             * Transferido en el d�a
             */
            double montoDia= 0;

            try {
                montoDia= montoTransferidoDia(cliente.getRut(),tipoCuentaOrigen, ctaOrigen, new Date());
            }
            catch (Exception e) {
                mensaje=
                    "TransferidoDia"
                    + ", CuentaOrigen =["
                    + ctaOrigen
                    + "]"
                    + ", CuentaDestino=["
                    + ctaDestino
                    + "]"
                    + ", Monto=["
                    + monto
                    + "]: ";
                logCliente(cliente,canal,"transCtaCteOtroBancoLineaPer", mensaje+e);
                throw new TransferenciaException("TRF001");
            }


            logCliente(cliente,canal,"transCtaCteOtroBancoLineaPer", "MONTODIA=" + montoDia);

            /*
             * Validamos saldo disponible
             */
            if (!validarSaldoCtaCte(cliente,canal,ctaOrigen,monto,true)) {
                throw new TransferenciaException("0020");
            }

            logCliente(cliente,canal,"transCtaCteOtroBancoLineaPer", "MONTO 1 VALIDADO");

            if (monto > (topeBanco - montoDia - montoProgramado)) {
                mensaje=
                    "Monto no V�lido. "
                    + ", Monto="
                    + monto
                    + ", TopeBanco="
                    + topeBanco
                    + ", montoDia="
                    + montoDia
                    + ", montoProgramado="
                    + montoProgramado;
                logCliente(cliente,canal,"transCtaCteOtroBancoLineaPer", mensaje);
                throw new TransferenciaException("TRF042");
            }

            logCliente(cliente,canal,"transCtaCteOtroBancoLineaPer", "MONTO 2 VALIDADO");

            Date fecha = null;
            String hora = new String();
            try {
                fecha = new Date();
                hora = new SimpleDateFormat("hhmmss").format(fecha);
                logCliente(cliente,canal,"transCtaCteOtroBancoLineaPer", "FECHA CARGO=" + fecha);
            }
            catch (Exception e) {
                logCliente(cliente,canal,"transCtaCteOtroBancoLineaPer",
                        "Exception en FECHA CARGO=" + e.getMessage());
            }

            String estadoTra = new String();

            /*
             * Guardamos el estado inicial ITF
             */

            RegistroTransferencia registro = null;
            try {
                registro = ingresaTransferencia(
                        cliente,
                        tipoCuentaOrigen,
                        ctaOrigen,
                        monto,
                        fecha,
                        TablaValores.getValor(TTFF_CONFIG, "estados", "ingreso"),
                        tipoCuentaDestino,
                        ctaDestino,
                        rutDestino,
                        codBancoDestino,
                        canal,
                        null,
                        null,
                        comentario,
                        nombreDestino,
                        null,
                        eMailDestino,
                        String.valueOf(rutDestino),
                        tipoTransferencia,
                        codigoAlias);

                if (registro == null || registro.numeroOperacion == null) {
                    logCliente(cliente,canal,"transCtaCteOtroBancoLineaPer", "Registro null");
                    throw new TransferenciaException("TRF001");
                }
            }
            catch (Exception e) {
                mensaje=
                    "REG_ITF"
                    + ", CuentaOrigen =["
                    + ctaOrigen
                    + "]"
                    + ", CuentaDestino=["
                    + ctaDestino
                    + "]"
                    + ", Monto=["
                    + monto
                    + "]: ";
                logCliente(cliente,canal,"transCtaCteOtroBancoLineaPer", mensaje+e);
                throw new TransferenciaException("TRF001");
            }

            /*
             * Se obtiene el Trace Number
             */
            String traceNumber = new String();
            try {
                traceNumber = getTraceNumber();
            }
            catch (Exception e) {
                logCliente(cliente, canal, "transCtaCteOtroBancoLineaPer",
                        "Exception al obtener Trace Number SE ABORTA!!!!: " + e.toString());
                throw new TransferenciaException("TRF001");
            }
            logCliente(cliente, canal, "transCtaCteOtroBancoLineaPer", "traceNumber=" + traceNumber);

            /*
             * Se llama a servicio de evaluaci�n de riesgo de fraude.
             */

            logCliente(cliente, canal, "transCtaCteOtroBancoLineaPer", "datosTransferenciaTO="
                    + datosTransferenciaTO);

            ResultadoEvaluacionTTFFTO resultadoEvaluacion = 
                    EvaluacionRiesgoTTFFHelper.evaluarTransferencia(this.datosTransferenciaTO, traceNumber);

            if (resultadoEvaluacion!=null && isValidaDisponibilidadServicioFraude(canal)){
                if (ResultadoEvaluacionTTFFTO.DECLINE.equalsIgnoreCase(resultadoEvaluacion.getAccion())){
                   logCliente(cliente,canal,"transCtaCteOtroBancoLineaPer", "Rechazo de TTFF por posible fraude");
                   throw new TransferenciaException("TRF0090");
                }
                logCliente(cliente,canal,"transCtaCteOtroBancoLineaPer", "Continuo flujo normal de TTFF");
            }
            
            /*
             * Se genera MAC
             */
            String macOrigen = new String();
            boolean operarMAC = new Boolean(TablaValores.getValor(TTFF_CONFIG, "MAC", "operar")).booleanValue();
            logCliente(cliente,canal,"transCtaCteOtroBancoLineaPer", "operarMAC=" + operarMAC);
            MessageAuthenticationCodeHelper messageAuthenticationCodeHelper = null;
            if(operarMAC){
                try {
                    messageAuthenticationCodeHelper = new MessageAuthenticationCodeHelper(ejbName);
                    macOrigen = messageAuthenticationCodeHelper.generaMessageAuthenticationCode(
                            String.valueOf(cliente.getRut())+cliente.getDigito(),
                            ctaOrigen,
                            codigoBancoBCI,
                            codBancoDestino,
                            String.valueOf(rutDestino)+digDestino,
                            ctaDestino,
                            String.valueOf((long)monto),
                            traceNumber,
                            null,
                            null,
                            ORIGEN);
                }
                catch (Exception e){
                    logCliente(cliente,canal,"transCtaCteOtroBancoLineaPer",
                            "Exception al generar MAC SE ABORTA!!!!: "+e.toString());
                    throw new TransferenciaException("TRF001");
                }
            }
            boolean modificoEstado = false;
            boolean insertoOk = false;
            boolean reversaBanco = false;

            estadoTra = TablaValores.getValor(TTFF_CONFIG, "estados", "traceNumber");
            modificoEstado = registraFinTrf(
                    cliente,
                    canal,
                    Long.parseLong(registro.numeroOperacion),
                    traceNumber,
                    null,
                    fecha,
                    estadoTra);
            logCliente(cliente,canal,"transCtaCteOtroBancoLineaPer", "MODIFICO ESTADO=" + modificoEstado);
            if (modificoEstado) {
                logCliente(cliente,canal,"transCtaCteOtroBancoLineaPer", "Se actualiz� a estado: "+estadoTra);
            }
            else {
                logCliente(cliente, canal, "transCtaCteOtroBancoLineaPer", "No se actualiz� a estado: "
                        + estadoTra);
            }

            try{
                realizarTransferenciaCtaCteHaciaCtaCte(cliente,canal,ctaOrigen,traceNumber,monto);
            }
            catch(Exception te){
                logCliente(cliente,canal,"transCtaCteOtroBancoLineaPer",
                        "Exception al realizarTransferenciaCtaCteHaciaCtaCte SE INTENTA REVERSA: "
                                +te.getMessage());
                try{
                    realizaReversaCargo(cliente,canal,ctaOrigen,traceNumber,monto);
                }
                catch(Exception ex){
                    logCliente(cliente,canal,"transCtaCteOtroBancoLineaPer",
                            "Exception reversa Cargo: "+ex.getMessage());
                    insertoOk = registraTransaccionPendiente(cliente,
                                    canal,
                                    ex,
                                    "B8690",
                                    ctaOrigen,
                                    null,
                                    monto,
                                    traceNumber,
                                    producto,
                                    null,
                                    intentos,
                                    false,
                                    null,
                                    null,
                                    null,
                                    null,
                                    null,
                                    null);
                    logCliente(cliente,canal,"transCtaCteOtroBancoLineaPer",
                            "registr� transaccion pendiente cargo: "+insertoOk);
                }

                throw new TransferenciaException("TRF001");
            }
            /*
             * Generamos detalle de la TRF
             */
            DetalleTransferencia detalle= new DetalleTransferencia();
            if(!transferenciaProgramada){
                detalle.ctaOrigen = ctaOrigen;
                detalle.tipoCuentaOrigen = tipoCuentaOrigen;
                detalle.ctaDestino = ctaDestino;
                detalle.tipoCuentaDestino = tipoCuentaDestino;
                detalle.montoTransferencia = monto;
                detalle.numeroOperacion = registro.numeroOperacion;
                detalle.codigoBancoDestino = codBancoDestino;
                detalle.codigoBancoOrigen = codigoBancoBCI;
                detalle.rut = cliente.getRut();
                detalle.dig = cliente.getDigito();
                detalle.rutDestino= rutDestino;
                detalle.digDestino= digDestino;
                detalle.nombreDestino= nombreDestino;
                detalle.comentario= comentario;
                detalle.canalID= canal;
                detalle.eMailCliente= eMailCliente;
                detalle.eMailDestino= eMailDestino;
                detalle.fechaTransferencia= fecha;
                detalle.folio= "";
                detalle.fechaCargo= fecha;
                detalle.fechaAbono= fecha;
                detalle.nombreOrigen= cliente.getFullName();
                detalle.nombreBancoOrigen=TablaValores.getValor(TTFF_CONFIG, "codigoBCI", "desc_" + canal);
                detalle.nombreBancoDestino= nombreBancoDestino;
                detalle.setResultadoEvaluacionTTFFTO(resultadoEvaluacion);
                if (this.datosTransferenciaTO!=null && this.datosTransferenciaTO.getEsEgift()){
                    detalle.egift=this.datosTransferenciaTO.getEgift();
                    detalle.esEgift=this.datosTransferenciaTO.getEsEgift();
                    
                }
            }
            CCADirectaVO detalleOperador = null;
            String xfml = "";

            try {
                xfml = construyeXFML(cliente,
                        canal,
                        null,
                        tipoCuentaOrigen,
                        tipoCuentaDestino,
                        monto,traceNumber,
                        fecha,
                        hora,
                        ctaOrigen,
                        cliente.getRut(),
                        cliente.getDigito(),
                        codBancoDestino,
                        ctaDestino,
                        rutDestino,
                        digDestino,
                        nombreDestino,
                        comentario,
                        registro.numeroOperacion,
                        macOrigen);

                detalleOperador = enviarTrfOperador(cliente, canal, macOrigen, xfml);
            }
            catch (Exception e) {
                logCliente(cliente,canal,"transferenciaCtaCteOtroBancoEnLineaPersonas",
                        "Exception en enviarTrfOperador: " + e);

                try {
                    logCliente(cliente,canal,"transferenciaCtaCteOtroBancoEnLineaPersonas",
                            "Consultamos Transferencia en el operador");

                    xfml = construyeXFML(cliente,
                            canal,
                            TablaValores.getValor(TTFF_CONFIG, "consultaTransferencia", "tipoOperacion"),
                            TablaValores.getValor(TTFF_CONFIG, "consultaTransferencia", "tipoCuentaOrigen"),
                            TablaValores.getValor(TTFF_CONFIG, "consultaTransferencia", "tipoCuentaDestino"),
                            monto,
                            traceNumber,
                            fecha,
                            hora,
                            ctaOrigen,
                            cliente.getRut(),
                            cliente.getDigito(),
                            codBancoDestino,
                            ctaDestino,
                            rutDestino,
                            digDestino,
                            nombreDestino,
                            comentario,
                            registro.numeroOperacion,
                            macOrigen);

                    detalleOperador = enviarTrfOperador(cliente, canal, macOrigen, xfml);
                }
                catch (Exception ex) {
                    logCliente(cliente,canal,"transferenciaCtaCteOtroBancoEnLineaPersonas",
                            "Exception en consultar transferencia al operador: " + ex);

                    producto = TablaValores.getValor(STORE_FORWARD_CONFIG, "ConsultaTransferencias", "producto");
                    logCliente(cliente,canal,"transferenciaCtaCteOtroBancoEnLineaPersonas",
                            "Producto [" + producto + "]");
                    intentos = Integer.parseInt(TablaValores.getValor(STORE_FORWARD_CONFIG,
                            "ConsultaTransferencias", "intentos"));
                    logCliente(cliente,canal,"transferenciaCtaCteOtroBancoEnLineaPersonas",
                            "Re-Intentos [" + intentos + "]");
                    insertoOk = registraTransaccionPendiente(cliente,
                            canal,
                            e,
                            "B8690",
                            ctaOrigen,
                            null,
                            monto,
                            traceNumber,
                            producto,
                            null,
                            intentos,
                            false,
                            registro.numeroOperacion,
                            detalle.getEMailCliente(),
                            detalle.getEMailDestino(),
                            xfml,
                            null,
                            null);

                    logCliente(cliente,canal,"transferenciaCtaCteOtroBancoEnLineaPersonas",
                            "registr� transaccion pendiente consulta: "+insertoOk);

                    estadoTra = TablaValores.getValor(TTFF_CONFIG, "estados", "storeAndForward");
                    modificoEstado = registraFinTrf(
                            cliente,
                            canal,
                            Long.parseLong(registro.numeroOperacion),
                            traceNumber,
                            null,
                            fecha,
                            estadoTra);
                    if (modificoEstado) {
                        logCliente(cliente,canal,"transferenciaCtaCteOtroBancoEnLineaPersonas",
                                "Se actualiz� a estado: "+estadoTra);
                    }
                    else {
                        logCliente(cliente,canal,"transferenciaCtaCteOtroBancoEnLineaPersonas",
                                "No se actualiz� a estado: "+estadoTra);
                    }
                    throw new TransferenciaException("TRF027-" + canal);
                }

                if((detalleOperador.getOperador().equals(TablaValores.getValor(TTFF_CONFIG,
                        "operadorSecundario", "valor")) &&  (TablaValores.getValor(TTFF_CONFIG,
                                "operadorSecundario", "codigos").indexOf(detalleOperador.getTipoRespuesta()) >= 0))
                                || (detalleOperador.getOperador().equals(TablaValores.getValor(TTFF_CONFIG,
                                        "operadorPrimario", "valor")) &&  (TablaValores.getValor(TTFF_CONFIG,
                                                "operadorPrimario", "codigos").indexOf(
                                                        detalleOperador.getTipoRespuesta()) >= 0))){

                    logCliente(cliente,canal,"transferenciaCtaCteOtroBancoEnLineaPersonas",
                            "Insertamos registro en S&F debido a la respuesta del operador.");

                    producto = TablaValores.getValor(STORE_FORWARD_CONFIG, "ConsultaTransferencias", "producto");
                    logCliente(cliente,canal,"enviarTrfOperador", "Producto [" + producto + "]");
                    intentos = Integer.parseInt(
                            TablaValores.getValor(STORE_FORWARD_CONFIG, "ConsultaTransferencias", "intentos"));
                    logCliente(cliente,canal,"enviarTrfOperador", "Re-Intentos [" + intentos + "]");
                    insertoOk = registraTransaccionPendiente(cliente,
                            canal,
                            e,
                            "B8690",
                            ctaOrigen,
                            null,
                            monto,
                            traceNumber,
                            producto,
                            null,
                            intentos,
                            false,
                            registro.numeroOperacion,
                            detalle.getEMailCliente(),
                            detalle.getEMailDestino(),
                            xfml,
                            null,
                            null);

                    logCliente(cliente,canal,"transferenciaCtaCteOtroBancoEnLineaPersonas",
                            "registr� transaccion pendiente consulta: "+insertoOk);

                    estadoTra = TablaValores.getValor(TTFF_CONFIG, "estados", "storeAndForward");
                    modificoEstado = registraFinTrf(cliente,
                            canal,
                            Long.parseLong(registro.numeroOperacion),
                            traceNumber,
                            null,
                            fecha,
                            estadoTra);
                    if (modificoEstado) {
                        logCliente(cliente,canal,"transferenciaCtaCteOtroBancoEnLineaPersonas",
                                "Se actualiz� a estado: "+estadoTra);
                    }
                    else {
                        logCliente(cliente,canal,"transferenciaCtaCteOtroBancoEnLineaPersonas",
                                "No se actualiz� a estado: "+estadoTra);
                    }

                    throw new TransferenciaException("TRF027-" + canal);
                }
            }

            String codigoOperador = new String();

            if (transferenciaProgramada){
                try{
                    String operador = detalleOperador.getOperador().trim().substring(0, 2);
                    codigoOperador = "P"+operador;
                }
                catch (Exception e) {
                    codigoOperador = tipoTransferencia;
                    logCliente(cliente,canal,"transCtaCteOtroBancoLineaPer",
                            "Exception en parseo de estado: "+tipoTransferencia);
                }
            }
            else{
                codigoOperador = detalleOperador.getOperador();
            }

            logCliente(cliente,canal,"transCtaCteOtroBancoLineaPer", "codigoOperador: "+codigoOperador);
            estadoTra = TablaValores.getValor(TTFF_CONFIG, "estados", "envioCCA");
            modificoEstado = actualizaTransferenciaLinea(cliente,
                    canal,
                    registro.numeroOperacion,
                    estadoTra,
                    detalleOperador.getTimesTampCCA(),
                    detalleOperador.getTipoRespuesta(),
                    codigoOperador);
            logCliente(cliente,canal,"transCtaCteOtroBancoLineaPer", "MODIFICO ESTADO=" + modificoEstado);
            if (modificoEstado) {
                logCliente(cliente,canal,"transCtaCteOtroBancoLineaPer", "Se actualiz� a estado: "+estadoTra);
            }
            else {
                logCliente(cliente, canal, "transCtaCteOtroBancoLineaPer", "No se actualiz� a estado: "
                        + estadoTra);
            }


            String respuesta = TablaValores.getValor(TTFF_CONFIG, "0"+detalleOperador.getTipoRespuesta(), "tipo");
            String message = TablaValores.getValor(TTFF_CONFIG, "0"+detalleOperador.getTipoRespuesta(), "mensaje");
            boolean validacionMAC = false;

            if(operarMAC && detalleOperador.getTipoMensaje().equals("0210")){
                logCliente(cliente,canal,"transCtaCteOtroBancoLineaPer", "Se valida MAC");
                try {
                    int validaMAC = messageAuthenticationCodeHelper.validaMessageAuthenticationCode(
                            String.valueOf(cliente.getRut())+cliente.getDigito(),
                            ctaOrigen,
                            codigoBancoBCI,
                            codBancoDestino,
                            String.valueOf(rutDestino)+digDestino,
                            ctaDestino,
                            String.valueOf((long)monto),
                            traceNumber,
                            detalleOperador.getTimesTampCCA(),
                            detalleOperador.getMacReceptor(),
                            detalleOperador.getTipoRespuesta(),
                            detalleOperador.getMacOperador(),
                            ORIGEN);
                    validacionMAC = (validaMAC == cero || validaMAC == dos) ? true : false;
                }
                catch (Exception e) {
                    logCliente(cliente,canal,"transCtaCteOtroBancoLineaPer",
                            "Exception en validacionMAC: "+e.toString());
                }
            }
            if(operarMAC && !detalleOperador.getTipoMensaje().equals("9200") && !validacionMAC){
                logCliente(cliente,canal,"transCtaCteOtroBancoLineaPer", "Se rechaza MAC");
                try {
                    rechazaMacOperador(
                            cliente,
                            canal,
                            tipoCuentaOrigen,
                            tipoCuentaDestino,
                            monto,
                            detalleOperador.getTimesTampCCA(),
                            traceNumber,
                            hora,
                            fecha,
                            detalleOperador.getTipoRespuesta(),
                            ctaOrigen,
                            cliente.getRut(),
                            cliente.getDigito(),
                            codBancoDestino,
                            ctaDestino,
                            rutDestino,
                            digDestino,
                            nombreDestino,
                            macOrigen);
                }
                catch (Exception e) {
                    logCliente(cliente,canal,"transCtaCteOtroBancoLineaPer",
                            "Se realiza reversa banco Exception en rechazaMacOperador: "+e.toString());
                    reversaBanco = realizaReversaBanco(cliente,
                            canal,
                            tipoCuentaOrigen,
                            tipoCuentaDestino,
                            monto,
                            traceNumber,
                            fecha,
                            hora,
                            ctaOrigen,
                            cliente.getRut(),
                            cliente.getDigito(),
                            codBancoDestino,
                            ctaDestino,
                            rutDestino,
                            digDestino,
                            nombreDestino,
                            comentario,
                            registro.numeroOperacion);
                    logCliente(cliente,canal,"transCtaCteOtroBancoLineaPer",
                            "realiz� reversa banco: "+reversaBanco);
                }
            }
            if((operarMAC && !validacionMAC) || detalleOperador.getTipoMensaje().equals("9200")
                    || respuesta == null){
                logCliente(cliente,canal,"transCtaCteOtroBancoLineaPer", "Posibles errores: ["+respuesta+"]["
                        +detalleOperador.getTipoMensaje()+"]["+validacionMAC+"]");
                estadoTra = TablaValores.getValor(TTFF_CONFIG, "estados", "reversaOK");
                modificoEstado = registraFinTrf(
                        cliente,
                        canal,
                        Long.parseLong(registro.numeroOperacion),
                        traceNumber,
                        null,
                        fecha,
                        estadoTra);
                if (modificoEstado) {
                    logCliente(cliente, canal, "transCtaCteOtroBancoLineaPer", "Se actualiz� a estado: "
                            + estadoTra);
                }
                else {
                    logCliente(cliente,canal,"transCtaCteOtroBancoLineaPer",
                            "No se actualiz� a estado: "+estadoTra);
                }

                try{
                    realizaReversaCargo(cliente,canal,ctaOrigen,traceNumber,monto);
                }
                catch(Exception ex){
                    logCliente(cliente,canal,"transCtaCteOtroBancoLineaPer",
                            "Exception reversa Cargo: "+ex.getMessage());
                    estadoTra = TablaValores.getValor(TTFF_CONFIG, "estados", "reversaNOK");
                    modificoEstado = registraFinTrf(
                            cliente,
                            canal,
                            Long.parseLong(registro.numeroOperacion),
                            traceNumber,
                            null,
                            fecha,
                            estadoTra);
                    if (modificoEstado) {
                        logCliente(cliente, canal, "transCtaCteOtroBancoLineaPer", "Se actualiz� a estado: "
                                + estadoTra);
                    }
                    else {
                        logCliente(cliente, canal, "transCtaCteOtroBancoLineaPer", "No se actualiz� a estado: "
                                + estadoTra);
                    }
                    insertoOk = registraTransaccionPendiente(cliente,
                            canal,
                            ex,
                            "B8690",
                            ctaOrigen,
                            null,
                            monto,
                            traceNumber,
                            producto,
                            null,
                            intentos,
                            false,
                            null,
                            null,
                            null,
                            null,
                            null,
                            null);
                    logCliente(cliente,canal,"transferenciaCtaCteOtroBancoEnLineaPersonas",
                            "registr� transaccion pendiente cargo: "+insertoOk);
                }

                throw new TransferenciaException("TRF001");
            }
            else if (respuesta.equals("OK")) {
                logCliente(cliente,canal,"transCtaCteOtroBancoLineaPer", "RESPUESTA OK");
                estadoTra = TablaValores.getValor(TTFF_CONFIG, "estados", "fin");
                modificoEstado = actualizaTransferenciaLinea(
                        cliente,
                        canal,
                        registro.numeroOperacion,
                        estadoTra,
                        detalleOperador.getTimesTampCCA(),
                        detalleOperador.getTipoRespuesta(),
                        codigoOperador);

                logCliente(cliente,canal,"transCtaCteOtroBancoLineaPer", "MODIFICO ESTADO=" + modificoEstado);
                if (modificoEstado) {
                    logCliente(cliente, canal, "transCtaCteOtroBancoLineaPer", "Se actualiz� a estado: "
                            + estadoTra);
                }
                else {
                    logCliente(cliente, canal, "transCtaCteOtroBancoLineaPer", "No se actualiz� a estado: "
                            + estadoTra);
                }
                if(!transferenciaProgramada&&!detalle.getEsEgift()){
                    try {
                        // Env�a correos
                        logCliente(cliente,canal,"transCtaCteOtroBancoLineaPer", "Enviando correos");
                        enviaEmail(detalle, true, cliente, canal);
                        enviaMailCliente(detalle, cliente, canal);
                        logCliente(cliente, canal, "transCtaCteOtroBancoLineaPer", "Correos enviados");
                    }
                    catch (Exception e) {
                        logCliente(cliente,canal,"transCtaCteOtroBancoLineaPer",
                                "FALLA ENVIO DE EMAIL: "+e.getMessage());
                    }
                } else if (detalle.getEsEgift()){
                    logCliente(cliente,canal,"transCtaCteOtroBancoLineaPer", "Envia correo Regalo Virtual");
                    envioMailEgiftOrigen(detalle, cliente, canal);
                    envioMailEgiftDestino(detalle, cliente, canal);
                    
                }
            }
            else if (!respuesta.equals("OK")){
                logCliente(cliente,canal,"transCtaCteOtroBancoLineaPer", "RESPUESTA NOK, Mensaje: "+message);
                estadoTra = TablaValores.getValor(TTFF_CONFIG, "estados", "reversaOK");
                modificoEstado = registraFinTrf(
                        cliente,
                        canal,
                        Long.parseLong(registro.numeroOperacion),
                        traceNumber,
                        null,
                        fecha,
                        estadoTra);
                if (modificoEstado) {
                    logCliente(cliente, canal, "transCtaCteOtroBancoLineaPer", "Se actualiz� a estado: "
                            + estadoTra);
                }
                else {
                    logCliente(cliente,canal,"transCtaCteOtroBancoLineaPer", "No se actualiz� a estado: "
                            +estadoTra);
                }
                try{
                    realizaReversaCargo(cliente,canal,ctaOrigen,traceNumber,monto);
                }
                catch(Exception ex){
                    logCliente(cliente,canal,"transCtaCteOtroBancoLineaPer", "Exception reversa Cargo: "
                            +ex.getMessage());
                    estadoTra = TablaValores.getValor(TTFF_CONFIG, "estados", "reversaNOK");
                    modificoEstado = registraFinTrf(
                            cliente,
                            canal,
                            Long.parseLong(registro.numeroOperacion),
                            traceNumber,
                            null,
                            fecha,
                            estadoTra);
                    if (modificoEstado) {
                        logCliente(cliente,canal,"transCtaCteOtroBancoLineaPer",
                                "Se actualiz� a estado: "+estadoTra);
                    }
                    else {
                        logCliente(cliente,canal,"transCtaCteOtroBancoLineaPer",
                                "No se actualiz� a estado: "+estadoTra);
                    }
                    insertoOk = registraTransaccionPendiente(cliente,
                            canal,
                            ex,
                            "B8690",
                            ctaOrigen,
                            null,
                            monto,
                            traceNumber,
                            producto,
                            null,
                            intentos,
                            false,
                            null,
                            null,
                            null,
                            null,
                            null,
                            null);
                    logCliente(cliente,canal,"transferenciaCtaCteOtroBancoEnLineaPersonas",
                            "registr� transaccion pendiente cargo: "+insertoOk);
                }

                logCliente(cliente,canal,"transCtaCteOtroBancoLineaPer",
                        "codigoError: "+detalleOperador.getTipoRespuesta()+ "Mensaje: "+message);

                if(respuesta.equals("NOKCLIENTE")){
                    
                    EvaluacionRiesgoTTFFHelper.actualizarEvaluacionTransferencia(datosTransferenciaTO, 
                            resultadoEvaluacion != null ? resultadoEvaluacion.getCodigoTransaccion() : "0", 
                            detalleOperador.getTipoRespuesta());
    
                    throw new TransferenciaException("TRF026",message);
                }
                else if(respuesta.equals("NOKSISTEMA")){
                    
                    EvaluacionRiesgoTTFFHelper.actualizarEvaluacionTransferencia(datosTransferenciaTO, 
                            resultadoEvaluacion != null ? resultadoEvaluacion.getCodigoTransaccion() : "0",
                                    detalleOperador.getTipoRespuesta());
                    
                    throw new CCAException("TRF025",detalleOperador.getTipoRespuesta(),detalle);
                }
                else{
                    throw new TransferenciaException("TRF001");
                }
            }
            logCliente(cliente,canal,"transCtaCteOtroBancoLineaPer", "DETALLE FINAL, TRF FINALIZADA");
            return detalle;
        }
        finally{
            /* Se destruye variable de instancia para que los clientes que reutilicen este EJB no encuentren
             * informaci�n que no les corresponde. */
            this.datosTransferenciaTO = null;
        }
    }


    /**
     * <p>M�todo que permite obtener una instancia del ejb EvaluacionRiesgoTTFF.</p>
     *
     * Registro de Versiones:<UL>
     *
     * <li>1.0 04/10/2013 Sebasti�n S�nchez S. (ADA Ltda.): versi�n inicial.</li>
     * <li>1.1 03/01/2014 Sebasti�n S�nchez S. (ADA Ltda.): Se corrige formato. </li>
     * </UL>
     *
     * @return EvaluacionRiesgoTTFF
     * @throws Exception
     * @since 3.3
     */
    private EvaluacionRiesgoTTFF getEjbEvaluacionRiesgo() throws Exception {
        if (evaluacionRiesgoTTFF == null) {
            try {
                EnhancedServiceLocator locator = EnhancedServiceLocator.getInstance();
                EvaluacionRiesgoTTFFHome evaluacionRiesgoHome = (EvaluacionRiesgoTTFFHome)
                        locator.getHome("wcorp.aplicaciones.productos.servicios.transferencias."
                                + "detecciondefraudes.EvaluacionRiesgoTTFF", EvaluacionRiesgoTTFFHome.class);
                evaluacionRiesgoTTFF = evaluacionRiesgoHome.create();
            }
            catch (Exception e) {
                if(logger.isEnabledFor(Level.ERROR)) {
                    logger.error("[getEjbEvaluacionRiesgo]:: Excepcion : " + e);
                }
                throw new Exception("Error al crear instancias del ejb EvaluacionRiesgoTTFF");
            }
        }
        return evaluacionRiesgoTTFF;
    }

    /**
     * Transferencia hacia otros bancos en l�nea para personas
     *
     * Se sobrecarga m�todo <code>transCtaCteOtroBancoLineaPer</code>.
     * Se cambian los par�metros de la firma por un objeto de transferencia. Este nuevo TO incluye informaci�n
     * para realizar la evaluaci�n de riesgo de fraude con Actimize.
     *
     * Registro de Versiones:<UL>
     *
     * <li>1.0 03/10/2013 Sebasti�n S�nchez S. (ADA Ltda.): versi�n inicial.</li>
     * </UL>
     *
     * @param paramDatosTransferenciaTO datos de transferencia de fondo
     * @return detalle
     * @throws TransferenciaException
     * @throws TuxedoException
     * @throws CCAException
     * @since 3.3
     */
    public DetalleTransferencia transCtaCteOtroBancoLineaPer(DatosTransferenciaTO paramDatosTransferenciaTO)
            throws TransferenciaException, TuxedoException, CCAException {

        this.datosTransferenciaTO = paramDatosTransferenciaTO;
        return this.transCtaCteOtroBancoLineaPer(
                datosTransferenciaTO.getCtaOrigen(),
                datosTransferenciaTO.getTipoCuentaOrigen(),
                datosTransferenciaTO.getCtaDestino(),
                datosTransferenciaTO.getTipoCuentaDestino(),
                datosTransferenciaTO.getMonto(),
                datosTransferenciaTO.getCodBancoDestino(),
                datosTransferenciaTO.getNombreBancoDestino(),
                datosTransferenciaTO.getRutDestino(),
                datosTransferenciaTO.getDigDestino(),
                datosTransferenciaTO.getCliente(),
                datosTransferenciaTO.getCanal(),
                datosTransferenciaTO.getNombreDestino(),
                datosTransferenciaTO.getComentario(),
                datosTransferenciaTO.getEmailCliente(),
                datosTransferenciaTO.getEmailDestino(),
                datosTransferenciaTO.getTipoAutenticacion(),
                datosTransferenciaTO.getTipoTransferencia(),
                datosTransferenciaTO.getCodigoAlias());
    }

	/**
	 * transferenciaCtaCteOtroBancoEnLineaEmpresa
	 *
	 * Transfiere fondos desde cuenta corriente Empresa hacia cuenta corriente de otro Banco en Linea.
	 * Se trae m�todo desde {@link wcorp.bprocess.cuentas.TransferenciasFondosBean#} a la implementaci�n Stateless del mismo y
	 * se optimiza el uso de logs.
	 *
	 * Registro de versiones:<ul>
	 *
	 * <li>1.0 14/05/2009, Carlos Cerda I. (SEnTRA): Versi�n inicial.
	 * <li>1.2 22/02/2010, Carlos Cerda I. (Orand): Se agrega par�metro tracenumber (identificador de la transacci�n) a la llamada a los m�todos: {@link #realizarTransferenciaCtaCteHaciaCtaCte(Cliente, String, String, String, double)},
	 *  									{@link #registraTransaccionPendiente(Cliente, String, Exception, String, String, String, double, String, String, String, int)}, {@link #realizaReversaCargo(Cliente, String, String, String, double)}
	 *  									y {@link #realizaReversaAbono(Cliente, String, String, double)}.
	 * <li>1.2 25/03/2010, Jes�s Orellana. (Orand): Se realizan las siguientes modificaciones:
	 * 												<li> Se saca la obtenci�n de la cuenta saco desde la tabla de par�metros.
	 * 												<li> Se sacan todas las llamadas al m�todo realizaReversaAbono().
     * <li>1.3 15/12/2011, Carlos Cerda I. (Orand): Se a�ade la l�gica de consultar el estado de la transferencia
     * en el operador en caso de una excepci�n.
     * <li>1.4 07/02/2012, Carlos Cerda I. (Orand): Se cambia la forma de llamar a los m�todos
     * {@link #enviarTrfOperador(Cliente, String, String, String)} y
     * {@link #construyeXFML(Cliente, String, String, String, String, double, String, Date, String, String, long,
     * char, String, String, long, char, String, String, String, String, String)}
	 *
     * <li>1.5 03/04/2012, Leonardo Espinoza (Orand): Se agregan los valores <b>null</b> y <b>false</b> a los nuevos par�metros <pre>ctaDestino<pre> (<b>String</b>) y <pre>esTTFFMismoBanco<pre> (<b>boolean</b>) al llamado del m�todo
     *                                      {@link #registraTransaccionPendiente(Cliente, String, Exception, String, String, String, double, String, String, String, int, boolean)}.
     * <li>1.6 06/02/2013, Marcelo Fuentes H. (SEnTRA): Se agrega l�gica para insertar una consulta de
     * transferencias de fondos al sistema StoreAndForward para cuando falla la consulta por un error en el env�o a
     * CCA. Adem�s se modifica el c�digo de excepci�n devuelto, del TRF001 al TRF057, para todos los casos antes
     * del env�o a la CCA. De esta forma si el c�digo devuelto es el TRF057 se debe eliminar la �ltima firma
     * para que el cliente pueda reintentar la transferencia. Adem�s se modifica el estado de la transferencia
     * antes de enviarla a CCA.</li>
     * <li>1.7 06/02/2013, Venancio Ar�nguiz. (SEnTRA): Se modifica la llamada al m�todo
     * {@link #cambiaEstadoTransferenciaEnLinea(String, char, String, String, String, String, String, String,
     *  String)} ya que se migr� a persistencia de datos. Adem�s se modifica logueo del m�todo.</li>
     * <li>1.8 27/11/2013, Venancio Ar�nguiz. (SEnTRA): Se modifica cambio de estado para los casos en que CCA
     * responda NOKSISTEMA o NOKCLIENTE dejando la transferencia con estado REC.</li>
     * <li>1.9 27/02/2014, Franco Jona Torres (Imagemaker S.A.): se agrega validacion y valores por defecto,
     * si las variables "intentos" y "cambiaEstadoTTFF" son null. 
     * <li>2.0 10/12/2014, Claudia Carrasco H. (SEnTRA): Se evalua el retorno obtenido desde la CCA para 
     * determinar si el error es por causa del banco de destino. De ser asi se mostrara el mensaje la cliente 
     * indicando dicha situacion.</li>  
     * <li>2.1 06/08/2015 Carlos Cerda I. (Kubos): Debido al cambio en la firma del m�todo 
     * {@link MessageAuthenticationCodeHelper#validaMessageAuthenticationCode(String, String, String, String, 
     * String, String, String, String, String, String, String, String, String)} de boolean a int, se modifica su 
     * llamado. 
	 * <li>2.2 02/09/2016 Christoper Escudero (Imagemaker) - Minheli Mejias (BCI) : Se modifica metodo 
	 * {@link #registraTransaccionPendiente()}, en el momento de caer al catch e ingresar al metodo 
	 * se realiza un substring a la variable cuentaOrigen quitando los 
	 * ceros a la izquierda del numero de cuenta.
     * <li>2.3 30/11/2016 Andr�s Pardo T. (SEnTRA) - Marcelo Avenda�o. (Ing. Soft. BCI): Se incluye cambio de estado cuando la MAC es incorrecta o la respuesta de CCA no esta definida.
	 * </ul>
	 *
	 * @param tipoCuentaOrigen  : Tipo de la cuenta de origen.
	 * @param cuentaOrigen      : Cuenta origen de la transferencia.
	 * @param cuentaDestino     : Cuenta destino de la transferencia.
	 * @param tipoCuentaDestino : Tipo de la cuenta destino.
	 * @param montoTransferir   : Monto a transferir.
	 * @param rutDestino        : Rut del destinatario de la transferencia.
	 * @param dvDestino         : D�gito verificador del destinatario de la transferencia.
	 * @param codigoBancoDestino: C�digo del Banco de Destino de la transferencia.
	 * @param numeroOperacion   : Identificador de la transferencia.
	 * @param comentario        : Comentario relacionado con la transferencia.
	 * @param nombreDestino     : Nombre del destinatario de la transferencia.
	 * @param rutUsuario        : Rut del usuario que realiza la transferencia.
	 * @param convenio          : C�digo del convenio en el cu�l se realiza la transferencia.
	 * @param servicio          : C�digo del servicio en el que se realiza la transferencia.
	 * @param rutGirador        : Rut del girador de la transferencia.
	 * @param dvGirador         : D�gito verificador del girador de la transferencia.
	 * @return wcorp.bprocess.cuentas.vo.CCADirectaVO : Detalle del resultado del envio de datos a la CCA.
	 *
	 * @throws TransferenciaException
	 * @since 1.0
	 * @see {@link wcorp.bprocess.cuentas.TransferenciasFondosBean#transferenciaCtaCteOtroBancoEnLineaEmpresa(String, String, String, String, String, long, char, String, String, String, String, String, String, String, long, char)}
	 */
	public CCADirectaVO transferenciaCtaCteOtroBancoEnLineaEmpresa(String tipoCuentaOrigen,
			String cuentaOrigen,
			String cuentaDestino,
			String tipoCuentaDestino,
			String montoTransferir,
			long rutDestino,
			char dvDestino,
			String codigoBancoDestino,
			String numeroOperacion,
			String comentario,
			String nombreDestino,
			String rutUsuario,
			String convenio,
			String servicio,
			Cliente cliente,
			String canal) throws TransferenciaException, RemoteException{

		logCliente(cliente,canal,"transferenciaCtaCteOtroBancoEnLineaEmpresa", "[ "+numeroOperacion+" ]---> Entrando a transferenciaCtaCteOtroBancoEnLineaEmpresa");
		logCliente(cliente,canal,"transferenciaCtaCteOtroBancoEnLineaEmpresa", "[ "+numeroOperacion+" ]tipoCuentaOrigen   ["+tipoCuentaOrigen+"]");
		logCliente(cliente,canal,"transferenciaCtaCteOtroBancoEnLineaEmpresa", "[ "+numeroOperacion+" ]tipoCuentaDestino  ["+tipoCuentaDestino+"]");
		logCliente(cliente,canal,"transferenciaCtaCteOtroBancoEnLineaEmpresa", "[ "+numeroOperacion+" ]cuentaOrigen       ["+cuentaOrigen+"]");
		logCliente(cliente,canal,"transferenciaCtaCteOtroBancoEnLineaEmpresa", "[ "+numeroOperacion+" ]rutGirador         ["+cliente.getRut()+"-"+cliente.getDigito()+"]");
		logCliente(cliente,canal,"transferenciaCtaCteOtroBancoEnLineaEmpresa", "[ "+numeroOperacion+" ]codigoBancoDestino ["+codigoBancoDestino+"]");
		logCliente(cliente,canal,"transferenciaCtaCteOtroBancoEnLineaEmpresa", "[ "+numeroOperacion+" ]cuentaDestino      ["+cuentaDestino+"]");
		logCliente(cliente,canal,"transferenciaCtaCteOtroBancoEnLineaEmpresa", "[ "+numeroOperacion+" ]rutDestino         ["+rutDestino +"-"+dvDestino+"]");
		logCliente(cliente,canal,"transferenciaCtaCteOtroBancoEnLineaEmpresa", "[ "+numeroOperacion+" ]nombreDestino      ["+nombreDestino+ "]");
		logCliente(cliente,canal,"transferenciaCtaCteOtroBancoEnLineaEmpresa", "[ "+numeroOperacion+" ]comentario         ["+comentario+"]");
		logCliente(cliente,canal,"transferenciaCtaCteOtroBancoEnLineaEmpresa", "[ "+numeroOperacion+" ]codigoServicio     ["+servicio+"]");
		logCliente(cliente,canal,"transferenciaCtaCteOtroBancoEnLineaEmpresa", "[ "+numeroOperacion+" ]numConvenio        ["+convenio+"]");
		logCliente(cliente,canal,"transferenciaCtaCteOtroBancoEnLineaEmpresa", "[ "+numeroOperacion+" ]rutUsu         	  ["+rutUsuario+"]");

		double monto = Double.parseDouble(montoTransferir);
		logCliente(cliente,canal,"transferenciaCtaCteOtroBancoEnLineaEmpresa", "[ "+numeroOperacion+" ]monto              ["+monto+"]");

		String producto = TablaValores.getValor(STORE_FORWARD_CONFIG, "TransferenciasEmpresas", "producto");
        logCliente(cliente,canal,"transferenciaCtaCteOtroBancoEnLineaEmpresa",
                "[ " + numeroOperacion + " ]Producto [" + producto + "]");

        String intentosParam = TablaValores.getValor(STORE_FORWARD_CONFIG, "TransferenciasEmpresas", "intentos");

        if (intentosParam == null || "".compareTo(intentosParam) == 0) {
        	intentosParam = STORE_FORWARD_CONFIG_INTENTOS_POR_DEFECTO;
        }

        int intentos = Integer.parseInt(intentosParam);
        logCliente(cliente,canal,"transferenciaCtaCteOtroBancoEnLineaEmpresa", "Re-Intentos [" + intentos + "]");

        String cambiaEstadoTTFFvar = TablaValores.getValor(TTFF_CONFIG, "CambiaEstadoTTFF", "Desc");

        if (cambiaEstadoTTFFvar == null || cambiaEstadoTTFFvar.equals("")) {
        	cambiaEstadoTTFFvar = TTFF_CONFIG_CambiaEstadoTTFF;
        }

        String cambiaEstadoTTFF = cambiaEstadoTTFFvar;
        logCliente(cliente, canal, "transferenciaCtaCteOtroBancoEnLineaEmpresa",
        		"[ " + numeroOperacion + " ]cambiaEstadoTTFF [" + cambiaEstadoTTFF + "]");

		/*
		 * Se obtiene la Fecha de Cargo
		 */

		Date fecha = null;
		String hora = new String();
		try {
			fecha = new Date();
			hora = new SimpleDateFormat("hhmmss").format(fecha);
			logCliente(cliente,canal,"transferenciaCtaCteOtroBancoEnLineaEmpresa", "[ "+numeroOperacion+" ]FECHA CARGO=" + fecha);
		} catch (Exception e) {
			logCliente(cliente,canal,"transferenciaCtaCteOtroBancoEnLineaEmpresa", "[ "+numeroOperacion+" ]Exception en FECHA CARGO=" + e.getMessage());
		}

		/*
		 * Se obtiene el Trace Number
		 */

		String traceNumber = new String();
		try {
			traceNumber = StringUtil.rellenaPorLaIzquierda(getTraceNumber(), 12, '0');
			logCliente(cliente,canal,"transferenciaCtaCteOtroBancoEnLineaEmpresa", "[ "+numeroOperacion+" ]traceNumber        [" + traceNumber        + "]");
		} catch (Exception e) {
            logCliente(cliente,canal,"transferenciaCtaCteOtroBancoEnLineaEmpresa", "[ " + numeroOperacion
                    +" ]Exception al obtener Trace Number SE ABORTA!!!!: " + ErroresUtil.extraeStackTrace(e));
            throw new TransferenciaException("TRF057");
		}
		logCliente(cliente,canal,"transferenciaCtaCteOtroBancoEnLineaEmpresa", "[ "+numeroOperacion+" ]traceNumber=" + traceNumber);
		/*
		 * Se genera MAC
		 */
		String macOrigen = new String();
		boolean operarMAC = new Boolean(TablaValores.getValor(TTFF_CONFIG, "MAC", "operar")).booleanValue();
        logCliente(cliente,canal,"transferenciaCtaCteOtroBancoEnLineaEmpresa",
                "[ " + numeroOperacion + " ]operarMAC=" + operarMAC);
		MessageAuthenticationCodeHelper messageAuthenticationCodeHelper = null;
		if(operarMAC){
			try {
				messageAuthenticationCodeHelper = new MessageAuthenticationCodeHelper(ejbName);
				macOrigen = messageAuthenticationCodeHelper.generaMessageAuthenticationCode(
						String.valueOf(cliente.getRut())+cliente.getDigito(),
						cuentaOrigen,
						codigoBancoBCI,
						codigoBancoDestino,
						String.valueOf(rutDestino)+dvDestino,
						cuentaDestino,
						String.valueOf((long)monto),
					traceNumber,
					null,
						null,
						ORIGEN);
			} catch (Exception e){
                logCliente(cliente,canal,"transferenciaCtaCteOtroBancoEnLineaEmpresa",
                        "[ " + numeroOperacion + " ]Exception al generar MAC SE ABORTA!!!!: "
                        + ErroresUtil.extraeStackTrace(e));
                throw new TransferenciaException("TRF057");
			}
		}
		/*
		 * Se Realiza el Cargo
		 */
		boolean insertoOk = false;
		boolean reversaBanco = false;

			try{
            logCliente(cliente, canal, "transferenciaCtaCteOtroBancoEnLineaEmpresa",
                       "[ " + numeroOperacion + " ]realizarTransferenciaCtaCteHaciaCtaCte: " + "cuentaOrigen["
                       + cuentaOrigen + "], traceNumber[" + traceNumber + "], monto[" + monto + "]");
			realizarTransferenciaCtaCteHaciaCtaCte(cliente, canal, cuentaOrigen, traceNumber, monto);
            logCliente(cliente, canal, "transferenciaCtaCteOtroBancoEnLineaEmpresa",
                       "[ " + numeroOperacion + " ]Cargo realizado correctamente.");
		}catch(Exception e){
            logCliente(cliente,canal,"transferenciaCtaCteOtroBancoEnLineaEmpresa", "[ " + numeroOperacion
                    + " ]Exception al realizarTransferenciaCtaCteHaciaCtaCte SE ABORTA!!!!: "
                    + ErroresUtil.extraeStackTrace(e));
			try{
				realizaReversaCargo(cliente,canal,cuentaOrigen,traceNumber,monto);
			}catch(Exception ex){
                logCliente(cliente, canal, "transferenciaCtaCteOtroBancoEnLineaEmpresa",
                        "[ " + numeroOperacion + " ]Exception reversa Cargo: " + ex.getMessage());
                try {
				insertoOk = registraTransaccionPendiente(cliente,
						canal,
						ex,
						"B8690",
						cuentaOrigen.substring(CANTIDAD_CARACTERES_A_ELIMINAR, LARGO_CUENTA),
                                                null,
						monto,
						traceNumber,
						producto,
						convenio,
                                                intentos,
                                                false,
                                                null,
                                                null,
                                                null,
                                                null,
                                                null,
                                                null);
                    logCliente(cliente, canal, "transferenciaCtaCteOtroBancoEnLineaEmpresa",
                               "[ " + numeroOperacion + " ]registr� transaccion pendiente cargo: " + insertoOk);
                }
                catch(Exception rtp) {
                    logCliente(cliente, canal, "transferenciaCtaCteOtroBancoEnLineaEmpresa",
                               "[ " + numeroOperacion + " ]Error al registrar transaccion pendiente cargo:"
                               + rtp.getMessage());
                }
            }
            throw new TransferenciaException("TRF057");
        }

        if(cambiaEstadoTTFF.equalsIgnoreCase("SI")){
            String estadoEncabezado = TablaValores.getValor(TTFF_CONFIG,
                                                            "CambiaEstadoTTFF", "NuevoEstadoEncabezado");
            logCliente(cliente, canal, "transferenciaCtaCteOtroBancoEnLineaEmpresa",
                    "[ " + numeroOperacion + " ] antes de cambiar estado a estadoEncabezado ["
                    + estadoEncabezado + "]");

            try {
                ResultCambiaEstadoTransferencia cambiaEstado = null;

                cambiaEstado = cambiaEstadoTransferenciaEnLinea(String.valueOf(cliente.getRut()),
                        convenio,
                        numeroOperacion,
                        estadoEncabezado,
                        "",
                        "");

                logCliente(cliente, canal, "transferenciaCtaCteOtroBancoEnLineaEmpresa",
                        "[ " + numeroOperacion + " ]cambio Estado : [" + cambiaEstado.getCim_status() + "]");
            } catch (Exception e) {
                    logCliente(cliente, canal, "transferenciaCtaCteOtroBancoEnLineaEmpresa",
                        "[ " + numeroOperacion + " ]Problemas al cambiar el estado["
                        + ErroresUtil.extraeStackTrace(e) + "]");
                try{
                    realizaReversaCargo(cliente,canal,cuentaOrigen,traceNumber,monto);
                }catch(Exception ex){
                    logCliente(cliente,canal,"transferenciaCtaCteOtroBancoEnLineaEmpresa",
                            "[ " + numeroOperacion + " ]Exception reversa Cargo: " + ex.getMessage());
                    try {
                        insertoOk = registraTransaccionPendiente(cliente,
                            canal,
                            ex,
                            "B8690",
                            cuentaOrigen.substring(CANTIDAD_CARACTERES_A_ELIMINAR, LARGO_CUENTA),
                            null,
                            monto,
                            traceNumber,
                            producto,
                            convenio,
                            intentos,
                            false,
                            null,
                            null,
                            null,
                            null,
                            null,
                            null);
                        logCliente(cliente, canal, "transferenciaCtaCteOtroBancoEnLineaEmpresa",
                                "[ " + numeroOperacion + " ]registr� transaccion pendiente cargo: " + insertoOk);
                    }
                    catch(Exception rtp) {
                        logCliente(cliente, canal, "transferenciaCtaCteOtroBancoEnLineaEmpresa",
                                "[ " + numeroOperacion + " ]Error al registrar transaccion pendiente cargo:"
                                + rtp.getMessage());
                    }
                }
                throw new TransferenciaException("TRF057");
			}
		}
		/*
		 * Se Realiza el Env�o a la CCA
		 */
        logCliente(cliente, canal, "transferenciaCtaCteOtroBancoEnLineaEmpresa",
                "[ " + numeroOperacion + " ]Se Realiza el Env�o a la CCA");

		CCADirectaVO detalleOperador = null;
        String xfml = "";
		try{
            logCliente(cliente,canal,"transferenciaCtaCteOtroBancoEnLineaEmpresa",
                    "[ "+numeroOperacion+" ]Llamando a enviarTrfCCA ");

            xfml = construyeXFML(cliente,
					canal,
                    null,
					tipoCuentaOrigen,
					tipoCuentaDestino,
					monto,
						traceNumber,
						fecha,
					hora,
					cuentaOrigen,
					cliente.getRut(),
					cliente.getDigito(),
					codigoBancoDestino,
					cuentaDestino,
					rutDestino,
					dvDestino,
					nombreDestino,
					comentario,
					numeroOperacion,
					macOrigen);

            detalleOperador = enviarTrfOperador(cliente, canal, macOrigen, xfml, SERV_TUX_ABONO_CCA);

            logCliente(cliente,canal,"transferenciaCtaCteOtroBancoEnLineaEmpresa",
                    "[ "+numeroOperacion+" ]Finalizando enviarTrfCCA ");
		}
        catch (Exception e) {
            logCliente(cliente,canal,"transferenciaCtaCteOtroBancoEnLineaEmpresa",
                    "[ "+numeroOperacion+" ]Exception en enviarTrfCCA: "+e);

			try{
                logCliente(cliente,canal,"transferenciaCtaCteOtroBancoEnLineaEmpresa",
                        "[ " + numeroOperacion + " ]Consultamos Transferencia en el operador");

                xfml = construyeXFML(cliente,
						canal,
                        TablaValores.getValor(TTFF_CONFIG, "consultaTransferencia", "tipoOperacion"),
                        TablaValores.getValor(TTFF_CONFIG, "consultaTransferencia", "tipoCuentaOrigen"),
                        TablaValores.getValor(TTFF_CONFIG, "consultaTransferencia", "tipoCuentaDestino"),
					monto,
						traceNumber,
						fecha,
					hora,
					cuentaOrigen,
					cliente.getRut(),
					cliente.getDigito(),
					codigoBancoDestino,
					cuentaDestino,
					rutDestino,
					dvDestino,
					nombreDestino,
					comentario,
                        numeroOperacion,
                        macOrigen);

                String servTuxedo = TablaValores.getValor(TTFF_CONFIG, "consultaTransferencia", "servicioTuxedo");
                logCliente(cliente,canal,"transferenciaCtaCteOtroBancoEnLineaEmpresa", "[ " + servTuxedo + " ]");
                if(servTuxedo == null || servTuxedo.trim().equals("")){
                    servTuxedo = SERV_TUX_ABONO_CCA;
                }
                detalleOperador = enviarTrfOperador(cliente, canal, macOrigen, xfml, servTuxedo);
            }
            catch(Exception ex) {
                logCliente(cliente,canal,"transferenciaCtaCteOtroBancoEnLineaEmpresa",
                        "[ " + numeroOperacion + " ]Exception en consultar transferencia al operador: "
                        + ErroresUtil.extraeStackTrace(ex));

                try {
                    producto = TablaValores.getValor(STORE_FORWARD_CONFIG, "ConsultaTransferenciasEmpresas",
                                                     "producto");
                    logCliente(cliente,canal,"transferenciaCtaCteOtroBancoEnLineaEmpresa",
                            "[ " + numeroOperacion + " ]Producto [" + producto + "]");
                    intentos = Integer.parseInt(TablaValores.getValor(STORE_FORWARD_CONFIG,
                                                "ConsultaTransferenciasEmpresas", "intentos"));
                    logCliente(cliente,canal,"transferenciaCtaCteOtroBancoEnLineaEmpresa",
                            "[ " + numeroOperacion + " ]Re-Intentos [" + intentos + "]");

                    insertoOk = registraTransaccionPendiente(cliente,
                            canal,
                            ex,
                            "B8690",
                            cuentaOrigen.substring(CANTIDAD_CARACTERES_A_ELIMINAR, LARGO_CUENTA),
                            null,
                            monto,
                            traceNumber,
                            producto,
                            convenio,
                            intentos,
                            false,
                            servicio,
                            rutUsuario,
                            null,
                            xfml,
                            null,
                            null);
                    logCliente(cliente, canal, "transferenciaCtaCteOtroBancoEnLineaEmpresa",
                            "[ " + numeroOperacion + " ]registr� transaccion pendiente consulta: " + insertoOk);
                }
                catch(Exception rtp) {
                    logCliente(cliente, canal, "transferenciaCtaCteOtroBancoEnLineaEmpresa",
                            "[ " + numeroOperacion + " ]Error al registrar transaccion pendiente consulta:"
                            + rtp.getMessage());
                }

                CCADirectaVO ccadirecta =  new CCADirectaVO();
                ccadirecta.setTipoRespuesta("");
                return ccadirecta;
            }
		}

		/*
		 * Se Obtiene Informaci�n de la Respuesta de la CCA
		 */

		String respuesta = TablaValores.getValor(TTFF_CONFIG, "0"+detalleOperador.getTipoRespuesta(), "tipo");

		boolean validacionMAC = false;

		if(operarMAC && detalleOperador.getTipoMensaje().equals("0210")){
            logCliente(cliente,canal,"transferenciaCtaCteOtroBancoEnLineaEmpresa", "[ "
                    +numeroOperacion+" ] Se valida MAC");
			try {
                int validaMAC = messageAuthenticationCodeHelper.validaMessageAuthenticationCode(
						String.valueOf(cliente.getRut())+cliente.getDigito(),
						cuentaOrigen,
						codigoBancoBCI,
						codigoBancoDestino,
						String.valueOf(rutDestino)+dvDestino,
						cuentaDestino,
						String.valueOf((long)monto),
						traceNumber,
						detalleOperador.getTimesTampCCA(),
						detalleOperador.getMacReceptor(),
						detalleOperador.getTipoRespuesta(),
						detalleOperador.getMacOperador(),
						ORIGEN);
                validacionMAC = (validaMAC == cero || validaMAC == dos) ? true : false;
            } 
            catch (Exception e) {
                logCliente(cliente,canal,"transferenciaCtaCteOtroBancoEnLineaEmpresa", "[ "
                        +numeroOperacion+" ] Exception en validacionMAC: "+e.toString());
			}
		}
		if(operarMAC && !detalleOperador.getTipoMensaje().equals("9200") && !validacionMAC){
			logCliente(cliente,canal,"transferenciaCtaCteOtroBancoEnLineaEmpresa", "[ "+numeroOperacion+" ] Se rechaza MAC");
			try {
				rechazaMacOperador(
						cliente,
						canal,
						tipoCuentaOrigen,
						tipoCuentaDestino,
						monto,
						detalleOperador.getTimesTampCCA(),
						traceNumber,
						hora,
						fecha,
						detalleOperador.getTipoRespuesta(),
						cuentaOrigen,
						cliente.getRut(),
						cliente.getDigito(),
						codigoBancoDestino,
						cuentaDestino,
						rutDestino,
						dvDestino,
						nombreDestino,
						macOrigen);
			} catch (Exception e) {
				logCliente(cliente,canal,"transferenciaCtaCteOtroBancoEnLineaEmpresa", "[ "+numeroOperacion+" ] Se realiza reversa banco Exception en rechazaMacOperador: "+e.toString());
				reversaBanco = realizaReversaBanco(cliente,
						canal,
						tipoCuentaOrigen,
						tipoCuentaDestino,
						monto,
						traceNumber,
						fecha,
						hora,
						cuentaOrigen,
						cliente.getRut(),
						cliente.getDigito(),
						codigoBancoDestino,
						cuentaDestino,
						rutDestino,
						dvDestino,
						nombreDestino,
						comentario,
						numeroOperacion);
				logCliente(cliente,canal,"transferenciaCtaCteOtroBancoEnLineaEmpresa", "[ "+numeroOperacion+" ] realiz� reversa banco: "+reversaBanco);
			}
		}
		if((operarMAC && !validacionMAC) || detalleOperador.getTipoMensaje().equals("9200") || respuesta == null){
			logCliente(cliente,canal,"transferenciaCtaCteOtroBancoEnLineaEmpresa", "[ "+numeroOperacion+" ] Posibles errores: ["+respuesta+"]["+detalleOperador.getTipoMensaje()+"]["+validacionMAC+"]");
			logCliente(cliente,canal,"transferenciaCtaCteOtroBancoEnLineaEmpresa", "[ "+numeroOperacion+" ] Realizando Reversa de Cargo");
			try{
				realizaReversaCargo(cliente,canal,cuentaOrigen,traceNumber,monto);
			}
			catch(Exception ex)
			{
				logCliente(cliente,canal,"transferenciaCtaCteOtroBancoEnLineaEmpresa", "[ "+numeroOperacion+" ]Exception reversa Cargo: "+ex.getMessage());
				insertoOk = registraTransaccionPendiente(cliente,
						canal,
						ex,
						"B8690",
						cuentaOrigen.substring(CANTIDAD_CARACTERES_A_ELIMINAR, LARGO_CUENTA),
                                                null,
						monto,
						traceNumber,
						producto,
						convenio,
                                                intentos,
                                                false,
                                                null,
                                                null,
                                                null,
                                                null,
                                                null,
                                                null);
                logCliente(cliente, canal, "transferenciaCtaCteOtroBancoEnLineaEmpresa",
                        "[ " + numeroOperacion + " ]registr� transaccion pendiente cargo: " + insertoOk);
				}

            String estado = ESTADO_RECHAZADO;
            String codEstado = COD_EST_REC;

            try {
                logCliente(cliente, canal, "transferenciaCtaCteOtroBancoEnLineaEmpresa", "[ " + numeroOperacion + " ]Cambia estado de la transferencia");
                ResultCambiaEstadoTransferencia resCet = cambiaEstadoTransferenciaEnLinea(String.valueOf(cliente.getRut()), convenio, numeroOperacion, estado, estado, codEstado);
                logCliente(cliente, canal, "transferenciaCtaCteOtroBancoEnLineaEmpresa", "[ " + numeroOperacion + " ]Despues de cambiar el estado de la transferencia");

                logCliente(cliente, canal, "transferenciaCtaCteOtroBancoEnLineaEmpresa", "[ " + numeroOperacion + " ]res_cet.getCim_status()=" + resCet.getCim_status());
                logCliente(cliente, canal, "transferenciaCtaCteOtroBancoEnLineaEmpresa", "[ " + numeroOperacion + " ]res_cet.getCim_mensaje()=" + resCet.getCim_mensaje());

            }
            catch (Exception ignored) {
                logCliente(cliente, canal, "transferenciaCtaCteOtroBancoEnLineaEmpresa", "[ " + numeroOperacion + " ] Error al actualizar rechazo," + ignored.getClass() + " : " + ignored.getMessage());
            }
            
			throw new TransferenciaException("TRF001");
		}
		else if (!respuesta.equals("OK"))
		{
            boolean reversaRegistrada = false;
            logCliente(cliente, canal, "transferenciaCtaCteOtroBancoEnLineaEmpresa", "[ " + numeroOperacion
                    + " ]RESPUESTA: " + respuesta);

            try{
                realizaReversaCargo(cliente, canal, cuentaOrigen, traceNumber, monto);
                reversaRegistrada = true;
            }
            catch(Exception ex)
            {
                logCliente(cliente, canal, "transferenciaCtaCteOtroBancoEnLineaEmpresa", "[ " + numeroOperacion
                        + " ]Exception reversa Cargo: " + ex.getMessage());
                try {
                    insertoOk = registraTransaccionPendiente(cliente,
                                                canal,
                                                ex,
                                                "B8690",
                                                cuentaOrigen.substring(CANTIDAD_CARACTERES_A_ELIMINAR, LARGO_CUENTA),
                                                null,
                                                monto,
                                                traceNumber,
                                                producto,
                                                convenio,
                                                intentos,
                                                false,
                                                null,
                                                null,
                                                null,
                                                null,
                                                null,
                                                null);
                    reversaRegistrada = true;

                    logCliente(cliente, canal, "transferenciaCtaCteOtroBancoEnLineaEmpresa",
                        "[ " + numeroOperacion + " ]registr� transaccion pendiente reversa Cargo: " + insertoOk);
                }
                catch(Exception rtp) {
                    logCliente(cliente, canal, "transferenciaCtaCteOtroBancoEnLineaEmpresa",
                        "[ " + numeroOperacion + " ]ERROR al registrar transaccion pendiente reversa Cargo: "
                        + rtp.getMessage());
                }
            }

            String estado = "REC";
            String codEstado = COD_REC
                + detalleOperador.getTipoRespuesta().substring(detalleOperador.getTipoRespuesta().length()
                        - COD_ERR_CCA);
            String tipoError = TablaValores.getValor(TTFF_CONFIG,
                                                         "0" + detalleOperador.getTipoRespuesta(), "tipo");

            logCliente(cliente, canal, "transferenciaCtaCteOtroBancoEnLineaEmpresa",
                    "[ " + numeroOperacion + " ]RESPUESTA: " + respuesta + ", Reversa registrada: "
                    + reversaRegistrada + ", tipoError: " + tipoError);

            if (( tipoError != null && tipoError.equalsIgnoreCase("NOKSISTEMA") ) && reversaRegistrada ) {
                estado = "PEN";
                codEstado = "0000";
            }

            try {
                logCliente(cliente, canal, "transferenciaCtaCteOtroBancoEnLineaEmpresa",
                        "[ " + numeroOperacion + " ]Cambiando estado a: " + estado + ", codEstado:" + codEstado);

                cambiaEstadoTransferenciaEnLinea(String.valueOf(cliente.getRut()),
                        convenio,
                        numeroOperacion,
                        estado,
                        estado,
                        codEstado);

                logCliente(cliente, canal, "transferenciaCtaCteOtroBancoEnLineaEmpresa",
                        "[ " + numeroOperacion + " ]Despues de cambiar estado a: " + estado);
            }
            catch (Exception e) {
                logCliente(cliente, canal, "transferenciaCtaCteOtroBancoEnLineaEmpresa",
                    "[ " + numeroOperacion + " ] : Error al cambiar el estado:" + ErroresUtil.extraeStackTrace(e));
                try {
                    String pdt = TablaValores.getValor(STORE_FORWARD_CONFIG,
                                                "ActualizaEstadoTransferenciasEmpresas", "producto");
                    logCliente(cliente,canal,"transferenciaCtaCteOtroBancoEnLineaEmpresa",
                            "[ " + numeroOperacion + " ]Producto [" + producto + "]");
                    intentos = Integer.parseInt(TablaValores.getValor(STORE_FORWARD_CONFIG,
                                                "ActualizaEstadoTransferenciasEmpresas", "intentos"));
                    logCliente(cliente,canal,"transferenciaCtaCteOtroBancoEnLineaEmpresa",
                            "[ " + numeroOperacion + " ]Re-Intentos [" + intentos + "]");

                    String xml = construyeXFML(cliente,
                            canal,
                            "",
                            estado,
                            codEstado,
                            monto,
                            traceNumber,
                            new Date(),
                            hora,
                            "",
                            0,
                            ' ',
                            "",
                            "",
                            0,
                            ' ',
                            "",
                            "",
                            numeroOperacion,
                            "");

                    insertoOk = registraTransaccionPendiente(cliente,
                            canal,
                            e,
                            "",
                            cuentaOrigen.substring(CANTIDAD_CARACTERES_A_ELIMINAR, LARGO_CUENTA),
                            null,
                            monto,
                            traceNumber,
                            pdt,
                            convenio,
                            intentos,
                            false,
                            servicio,
                            rutUsuario,
                            null,
                            xml,
                            null,
                            null);
                    logCliente(cliente, canal, "transferenciaCtaCteOtroBancoEnLineaEmpresa",
                        "[ " + numeroOperacion + " ] : registr� transaccion pendiente cambio estado: " + insertoOk);
                }
                catch(Exception rtp) {
                    logCliente(cliente, canal, "transferenciaCtaCteOtroBancoEnLineaEmpresa",
                        "[ " + numeroOperacion + " ] : Error al registrar transaccion pendiente cambio estado:"
                        + rtp.getMessage());
                }
            }

            if (( tipoError != null && tipoError.equalsIgnoreCase("NOKSISTEMA") ) && reversaRegistrada ) {
                logCliente(cliente, canal, "transferenciaCtaCteOtroBancoEnLineaEmpresa",
                    "[ " + numeroOperacion + " ] : Se devuelve excepcion que indica TTFF es reintentable");
                throw new TransferenciaException("TRF100");
            }
            else {
                logCliente(cliente, canal, "transferenciaCtaCteOtroBancoEnLineaEmpresa",
                    "[ " + numeroOperacion + " ] : La TTFF NO es reintentable");
            }
		}
		else if (respuesta.equals("OK"))
		{
			/* --------------> Inicio de Modificacion 29-07-2008*/

			String estadoTransaccion = "PRO";
			String codigoEstado = "0000";
			logCliente(cliente,canal,"transferenciaCtaCteOtroBancoEnLineaEmpresa", "[ "+numeroOperacion+" ]codigoEstado      ["+codigoEstado+"]");
			String ctaOrigenDoce;
			//boolean transfOK = true;
			String rutEmpresa = new DecimalFormat ("00000000").format(Integer.parseInt(String.valueOf(cliente.getRut())));
			logCliente(cliente,canal,"transferenciaCtaCteOtroBancoEnLineaEmpresa", "[ "+numeroOperacion+" ]rutEmpresa        ["+rutEmpresa+"]");

			if (cuentaOrigen.trim().length()>12) {
				ctaOrigenDoce = cuentaOrigen.trim().substring(cuentaOrigen.trim().length()-12,cuentaOrigen.trim().length());
			} else {
				ctaOrigenDoce = cuentaOrigen.trim();
			}

			int tope_doc = 12 - ctaOrigenDoce.length();
			for (int j = 0; j < tope_doc; j++) {
				ctaOrigenDoce = '0' + ctaOrigenDoce;
			}
			logCliente(cliente,canal,"transferenciaCtaCteOtroBancoEnLineaEmpresa", "[ "+numeroOperacion+" ]ctaOrigenDoce     ["+ctaOrigenDoce+"]");

            //Inicio de la Actualizacion del estado de la transferencia
			try {
                logCliente(cliente,canal,"transferenciaCtaCteOtroBancoEnLineaEmpresa", "[ " + numeroOperacion
                        + " ]Cambia estado de la transferencia");
				ResultCambiaEstadoTransferencia res_cet = cambiaEstadoTransferenciaEnLinea(rutEmpresa,
						convenio,
						numeroOperacion,
						estadoTransaccion,
                        estadoTransaccion,
                        codigoEstado);
                logCliente(cliente, canal, "transferenciaCtaCteOtroBancoEnLineaEmpresa", "[ "
                        + numeroOperacion + " ]Despues de cambiar el estado de la transferencia");

				logCliente(cliente,canal,"transferenciaCtaCteOtroBancoEnLineaEmpresa", "[ "+numeroOperacion+" ]res_cet.getCim_status()="+res_cet.getCim_status());
				logCliente(cliente,canal,"transferenciaCtaCteOtroBancoEnLineaEmpresa", "[ "+numeroOperacion+" ]res_cet.getCim_mensaje()="+res_cet.getCim_mensaje());

			} catch (Exception ignored) {
				logCliente(cliente,canal,"transferenciaCtaCteOtroBancoEnLineaEmpresa", "[ "+numeroOperacion+" ]"+ignored.getClass() + " : " + ignored.getMessage());
			}
            //Fin de la Actualizacion del estado de la transferencia

			//Inicio de la Actualizazion de los Montos Diarios
			logCliente(cliente,canal,"transferenciaCtaCteOtroBancoEnLineaEmpresa", "[ "+numeroOperacion+" ]Sin Problemas en Transferencia - Entonces se actualiza CEC");

			try {
				logCliente(cliente,canal,"transferenciaCtaCteOtroBancoEnLineaEmpresa", "[ "+numeroOperacion+" ]Cambio de Montos Diarios en CEC y MAC");

				String res_mtodia = cambiaMontosDiaEnLinea(rutEmpresa,
						rutUsuario,
						convenio,
						ctaOrigenDoce,
						numeroOperacion,
						String.valueOf(montoTransferir),
				"LIN");

				logCliente(cliente,canal,"transferenciaCtaCteOtroBancoEnLineaEmpresa", "[ "+numeroOperacion+" ]Despues de Cambiar Montos Diarios");
				if (res_mtodia.equals("NOK"))
					logCliente(cliente,canal,"transferenciaCtaCteOtroBancoEnLineaEmpresa", "[ "+numeroOperacion+" ]No rebaj� tabla montos Dia rutEmp=" + String.valueOf(cliente.getRut()) + "- identificacion=" + numeroOperacion + "- numConvenio=" + convenio + "- ctaOrigenDoce=" + ctaOrigenDoce);

			} catch (Exception ignored) {
				logCliente(cliente,canal,"transferenciaCtaCteOtroBancoEnLineaEmpresa", "[ "+numeroOperacion+" ]"+ignored.getClass() + " : " + ignored.getMessage());
			}
			//Fin de la Actualizazion de los Montos Diarios

			//Inicio de la Actualizazion del TRACENUMBER


			String fechaTimesTamp = new String();

			try {
				fechaTimesTamp = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(FechasUtil.convierteStringADate(detalleOperador.getTimesTampCCA(), new SimpleDateFormat( "yyMMddHHmmss" )));
			} catch (Exception e) {
				fechaTimesTamp = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(new Date());
			}

			logCliente(cliente,canal,"transferenciaCtaCteOtroBancoEnLineaEmpresa", "[ "+numeroOperacion+" ] fechaTimesTamp: "+fechaTimesTamp);

			try{
				logCliente(cliente,canal,"transferenciaCtaCteOtroBancoEnLineaEmpresa", "[ "+numeroOperacion+" ]Grabando TraceNumber y Fecha de TTFF");
				logCliente(cliente,canal,"transferenciaCtaCteOtroBancoEnLineaEmpresa", "[ "+numeroOperacion+" ]rutEmp            [" + StringUtil.rellenaPorLaIzquierda(String.valueOf(rutEmpresa), 8, '0') + "]");
				logCliente(cliente,canal,"transferenciaCtaCteOtroBancoEnLineaEmpresa", "[ "+numeroOperacion+" ]convenio            [" + StringUtil.rellenaPorLaIzquierda(convenio, 10, ' ') + "]");
				logCliente(cliente,canal,"transferenciaCtaCteOtroBancoEnLineaEmpresa", "[ "+numeroOperacion+" ]traceNumber         [" + detalleOperador.getTraceNumber() + "]");
				logCliente(cliente,canal,"transferenciaCtaCteOtroBancoEnLineaEmpresa", "[ "+numeroOperacion+" ]fechaTimesTamp    [" + fechaTimesTamp + "]");
				logCliente(cliente,canal,"transferenciaCtaCteOtroBancoEnLineaEmpresa", "[ "+numeroOperacion+" ]operador          [" + detalleOperador.getOperador() + "]");
				String respuestaActCont = actualizacionContable(StringUtil.rellenaPorLaIzquierda(String.valueOf(rutEmpresa), 8, '0'),
						StringUtil.rellenaPorLaIzquierda(convenio, 10, ' '),
						numeroOperacion,
						detalleOperador.getTraceNumber(),
						fechaTimesTamp,
						detalleOperador.getOperador());

				logCliente(cliente,canal,"transferenciaCtaCteOtroBancoEnLineaEmpresa","[ "+numeroOperacion+" ]Despues de guardar el TraceNumber y la Fecha en la BD : "+ respuestaActCont);
			}
			catch(Exception ignored){
				logCliente(cliente,canal,"transferenciaCtaCteOtroBancoEnLineaEmpresa", "[ "+numeroOperacion+" ]"+ignored.getClass() + " : " + ignored.getMessage());
			}
			//Fin de la Actualizazion del TRACENUMBER

			/* <-------------- Termino de Modificacion 29-07-2008*/
		}
		logCliente(cliente,canal,"transferenciaCtaCteOtroBancoEnLineaEmpresa", "[ "+numeroOperacion+" ]---> DETALLE FINAL, TRF FINALIZADA\n");
		return detalleOperador;
	}

	/**
	 * cambiaEstadoTransferenciaEnLinea
	 *
	 * M�todo que permite cambiar el estado de la una transferencia en linea.
	 * Se trae m�todo desde {@link wcorp.bprocess.cuentas.TransferenciasFondosBean#} a la implementaci�n Stateless del mismo y
	 * se cambia nivel de logeo de debug a info o error seg�n corresponda.
	 *
	 *
	 * Registro de versiones:<ul>
	 *
	 * <li>1.0 14/05/2009, Carlos Cerda I. (SEnTRA): Versi�n inicial.
     * <li>1.1 06/06/2013, Venancio Ar�nguiz. (SEnTRA): Se migra llamada de tuxedo a persistencia de datos y se
     *                     actualizan parametros.</li>
	 * </ul>
	 *
	 *
	 * @param rut               : Rut de la empresa de la transferencia.
	 * @param numConvenio       : N�mero de convenio.
	 * @param indicTraspaso     : Indicador de la transferencia.
     * @param estadoEncabezado  : Nuevo estado para el encabezado de la transferencia.
     * @param estadoDetalle     : Nuevo estado para el detalle de la transferencia.
     * @param codigoEstadoDet   : C�digo del estado del detalle la transferencia.
	 *
	 * @return ResultCambiaEstadoTransferencia : Detalle del resultado del cambio de estado.
	 *
     * @throws GeneralException Clase de Manejo de Errores para Aplicaciones BCI.
	 *
	 * @since 1.0
	 * @see {@link wcorp.bprocess.cuentas.TransferenciasFondosBean#cambiaEstadoTransferenciaEnLinea(String, char, String, String, String, String, String, String, String)
	 */
	private ResultCambiaEstadoTransferencia cambiaEstadoTransferenciaEnLinea(String rut,
			String numConvenio,
			String indicTraspaso,
            String estadoEncabezado,
            String estadoDetalle,
            String codigoEstadoDet) throws GeneralException {

        ResultCambiaEstadoTransferencia  resultado = null;

        if (logger.isDebugEnabled()) {
            logger.debug("[cambiaEstadoTransferenciaEnLinea][" + rut
                    + "] INGRESA A CAMBIA ESTADO TRANSFERENCIA EN LINEA");
            logger.debug("[cambiaEstadoTransferenciaEnLinea] rutEmpresa: [" + rut + "]");
            logger.debug("[cambiaEstadoTransferenciaEnLinea] numeroConvenio: [" + numConvenio + "]");
            logger.debug("[cambiaEstadoTransferenciaEnLinea] numTransferencia: ["+ indicTraspaso + "]");
            logger.debug("[cambiaEstadoTransferenciaEnLinea] estadoEncabezado: [" + estadoEncabezado + "]");
            logger.debug("[cambiaEstadoTransferenciaEnLinea] estadoDetalle: [" + estadoDetalle + "]");
            logger.debug("[cambiaEstadoTransferenciaEnLinea] codEstadoDetalle: [" + codigoEstadoDet + "]");
		}

		try {
            TransferenciasFondosDAO transferenciasFondosDAO = new TransferenciasFondosDAO();
            resultado = transferenciasFondosDAO.cambiaEstadoTransferenciaEnLinea(rut, numConvenio, indicTraspaso,
                            estadoEncabezado, estadoDetalle, codigoEstadoDet);

			if(logger.isDebugEnabled()){
                logger.debug("[cambiaEstadoTransferenciaEnLinea]["+ indicTraspaso
                        + "] Cambio de estado realizado OK");
			}
				}
        catch (Exception e) {
            if (logger.isEnabledFor(Level.ERROR)) {
                logger.error("[cambiaEstadoTransferenciaEnLinea]["+ indicTraspaso + "][" + rut
                        + "] Exception: " + ErroresUtil.extraeStackTrace(e));
					}
            throw new GeneralException("ESPECIAL", e.getMessage());
				}

        return resultado;
	}

	/**
	 * cambiaMontosDiaEnLinea
	 *
	 * Actualiza los montos dia de una transferencia en linea correspondientes a la tabla MAC y CEC.
	 * Si la transferencia esta ok (indicador=OK) se actualiza el monto dia de la CEC si existe un
	 * apoderado que tiene monto maximo 0 (no tiene limitaciones).
	 * Si la transferencia falla (indicador=NOK) se rebaja el monto dia a todos los apoderados que
	 * pudieron firmar.
	 *
	 * Se trae m�todo desde {@link wcorp.bprocess.cuentas.TransferenciasFondosBean#} a la implementaci�n Stateless del mismo y
	 * se cambia nivel de logeo de debug a info o error seg�n corresponda.
	 *
	 *
	 * Registro de versiones:<ul>
	 *
	 * <li>1.0 14/05/2009, Carlos Cerda I. (SEnTRA): Versi�n inicial.
	 *
	 *
	 * </ul>
	 *
	 * @param rutEmp             : Rut de la empresa de la transferencia
	 * @param rutUsu             : Rut de la del usuario que realiza la transferencia
	 * @param numConvenio        : N�mero de convenio de la empresa para la transferencia
	 * @param cuenta             : Cuenta corriente de la empresa desde donde se efecuta la transferencia
	 * @param numeroAutorizacion : N�mero de Autorizaci�n de la transferencia
	 * @param monto              : Monto correspondinte a la transferencia
	 * @param estadoPago         : Estado de la transferencia
	 *
	 * @return String : Resultado de la transferencia
	 *
	 * @throws GeneralException
	 * @throws TuxedoException
	 * @throws TransferenciaException
	 *
	 * @since 1.0
	 * @see {@link wcorp.bprocess.cuentas.TransferenciasFondosBean#cambiaMontosDiaEnLinea(String, String, String, String, String, String, String)
	 */
	private String cambiaMontosDiaEnLinea(String rutEmpresa,
			String rutUsuario,
			String numConvenio,
			String cuenta,
			String numeroAutorizacion,
			String monto,
			String estadoPago) throws TuxedoException, GeneralException, TransferenciaException {

		if(logger.isDebugEnabled()){logger.debug("[cambiaMontosDiaEnLinea]["+rutUsuario+"]-->Ingresando a cambiaMontosDiaEnLinea(....)'");}

		ServletSessionPool sesion     = joltPool.getSesion(ejbName);
		DataSet            parametros = new DataSet();
		Result             resultado  = null;

		try
		{
			if(logger.isInfoEnabled()){logger.info("[cambiaMontosDiaEnLinea]["+rutUsuario+"] Empresa: '" + rutEmpresa + "'");}
			parametros.setValue("rutEmpresa", rutEmpresa);

			if(logger.isInfoEnabled()){logger.info("[cambiaMontosDiaEnLinea]["+rutUsuario+"] Usuario: '" + rutUsuario + "'");}
			parametros.setValue("rutUsuario", rutUsuario);

			if(logger.isInfoEnabled()){logger.info("[cambiaMontosDiaEnLinea]["+rutUsuario+"] Convenio: '" + numConvenio + "'");}
			parametros.setValue("numConvenio", numConvenio);

			if(logger.isInfoEnabled()){logger.info("[cambiaMontosDiaEnLinea]["+rutUsuario+"] Cuenta: '" + cuenta+ "'");}
			parametros.setValue("cuenta", cuenta);

			if(logger.isInfoEnabled()){logger.info("[cambiaMontosDiaEnLinea]["+rutUsuario+"] Autorizacion: '" + numeroAutorizacion + "'");}
			parametros.setValue("numeroAutorizacion", numeroAutorizacion);

			if(logger.isInfoEnabled()){logger.info("[cambiaMontosDiaEnLinea]["+rutUsuario+"] Monto: '" + monto + "'");}
			parametros.setValue("monto", monto);

			if(logger.isInfoEnabled()){logger.info("[cambiaMontosDiaEnLinea]["+rutUsuario+"] estadoPago: '" + estadoPago + "'");}
			parametros.setValue("estadoPago", estadoPago);

		}
		catch (Exception e)
		{
			if(logger.isEnabledFor(Level.ERROR)){logger.error("[cambiaMontosDiaEnLinea]["+rutUsuario+"] Ha ocurrido un error colocando parametros.  El error es " + e);}
		}

		if(logger.isDebugEnabled()){logger.debug("[cambiaMontosDiaEnLinea]["+rutUsuario+"] call UpdMtoDiaApoLin");}

		try
		{

			resultado = sesion.call("UpdMtoDiaApoLin", parametros, null);
			if(logger.isDebugEnabled()){logger.debug("[cambiaMontosDiaEnLinea]["+rutUsuario+"] retornando UpdMtoDiaApoLin");}
			return (String) resultado.getValue("indicador", 0, null);

		}
		catch (ApplicationException e)
		{
			resultado = e.getResult();

			if (resultado.getApplicationCode() == 0)
			{
				if(logger.isEnabledFor(Level.ERROR)){logger.error("[cambiaMontosDiaEnLinea]["+rutUsuario+"] Error Aplicativo: "+e.getMessage());}
				throw new GeneralException("TRF001");
			}
			else
			{
				if(logger.isEnabledFor(Level.ERROR)){logger.error("[cambiaMontosDiaEnLinea]["+rutUsuario+"] Error Tuxedo: "+e.getMessage());}
				throw new TuxedoException("TRF001");
			}
		}
		catch (Exception e)
		{
			if(logger.isEnabledFor(Level.ERROR)){logger.error("[cambiaMontosDiaEnLinea]["+rutUsuario+"] Exception: "+e.toString());}
			throw new TransferenciaException("TRF001");
		}
	}

	/**
	 * actualizacionContable
	 *
	 * Registra la infomaci�n contable de la transferencia en linea.
	 * Se trae m�todo desde {@link wcorp.bprocess.cuentas.TransferenciasFondosBean#} a la implementaci�n Stateless del mismo y
	 * se cambia nivel de logeo de debug a info o error seg�n corresponda.
	 *
	 *
	 * Registro de versiones:<ul>
	 *
	 * <li>1.0 14/05/2009, Carlos Cerda I. (SEnTRA): Versi�n inicial.
	 *
	 * </ul>
	 *
	 * @param rutEmpresa  : Rut de la empresa de la transferencia.
	 * @param numConvenio : N�mero del convenio
	 * @param id          : Identificaci�n de la transferencia realizada.
	 * @param firma       : Firma que devuelve la CCA al realizar la transferencia.
	 * @param fecha       : Fecha en que la transferencia se realiz�.
	 * @param operador    : Nombre del Operador que atendi� la transferencia.
	 *                      Los posibles valores son: CCA o RDB (Redbanc).
	 *
	 * @return String : C�digo del resultado de la actualizaci�n.
	 *
	 * @throws GeneralException
	 * @throws TuxedoException
	 * @throws TransferenciaException
	 *
	 * @since 1.0
	 * @see {@link wcorp.bprocess.cuentas.TransferenciasFondosBean#actualizacionContable(String, String, String, String, String, String)
	 */
	private String actualizacionContable(String rutEmpresa,
			String numConvenio,
			String id,
			String firma,
			String fecha,
			String operador) throws TuxedoException, GeneralException, TransferenciaException {

		if(logger.isDebugEnabled()){logger.debug("[actualizacionContable]-->Ingresando a actualizacionContable(....)");}

		ServletSessionPool sesion     = joltPool.getSesion(ejbName);
		DataSet            parametros = new DataSet();
		Result             resultado  = null;

		try
		{
			if(logger.isInfoEnabled()){logger.info("[actualizacionContable] Empresa: '" + rutEmpresa + "'");}
			parametros.setValue("rutEmpresa", rutEmpresa);

			if(logger.isInfoEnabled()){logger.info("[actualizacionContable] Convenio: '" + numConvenio + "'");}
			parametros.setValue("numConvenio", numConvenio);

			if(logger.isInfoEnabled()){logger.info("[actualizacionContable] Identificacion: '" + id + "'");}
			parametros.setValue("identificacion", id);

			if(logger.isInfoEnabled()){logger.info("[actualizacionContable] Firma: '" + firma + "'");}
			parametros.setValue("firmaTRF_4", firma);

			if(logger.isInfoEnabled()){logger.info("[actualizacionContable] Fecha: '" + fecha + "'");}
			parametros.setValue("fechaMov", fecha);

			if(logger.isInfoEnabled()){logger.info("[actualizacionContable] Operador: '" + operador + "'");}
			parametros.setValue("operador", operador);
		}
		catch (Exception e)
		{
			if(logger.isEnabledFor(Level.ERROR)){logger.error("[actualizacionContable] Ha ocurrido un error colocando parametros.  El error es " + e);}
		}

		if(logger.isDebugEnabled()){logger.debug("[actualizacionContable] call CtaUpdTraLin");}

		try
		{
			resultado = sesion.call("CtaUpdTraLin", parametros, null);
			if(logger.isDebugEnabled()){logger.debug("[actualizacionContable] despues de CtaUpdTraLin");}
			return (String) resultado.getValue("codRetorno", 0, null);
		}
		catch (ApplicationException e)
		{
			resultado = e.getResult();

			if (resultado.getApplicationCode() == 0)
			{
				if(logger.isEnabledFor(Level.ERROR)){logger.error("[actualizacionContable] Error Aplicativo: "+e.getMessage());}
				throw new GeneralException("TRF001");
			}
			else
			{
				if(logger.isEnabledFor(Level.ERROR)){logger.error("[actualizacionContable] Error Tuxedo: "+ e.getMessage());}
				throw new TuxedoException("TRF001");
			}
		}
		catch (Exception e)
		{
			if(logger.isEnabledFor(Level.ERROR)){logger.error("[actualizacionContable] Exception " + e.getMessage());}
			throw new TransferenciaException("TRF001");
		}
	}

	/**
	 * M�todo que Valida que el saldo disponible en la cuenta permita realizar la
	 * transferencia por el monto especificado.
	 *
	 * Se trae m�todo desde {@link wcorp.bprocess.cuentas.TransferenciasFondosBean#} a la implementaci�n Stateless del mismo sin modificaciones.
	 *
	 * Registro de Versiones:<ul>
	 *
	 * <li>1.0 14/05/2009, Carlos Cerda I. (SEnTRA): Versi�n inicial.
	 *
	 * </ul>
	 *
	 *
	 * @param ctaOrigen Cuenta corriente de origen
	 * @param monto Monto a transferir
	 * @param consideraLDC Cuando se paga la Linea de Sobre Giro NO debemos considerar
	 *                     el saldo disponible en la LDC para sumarlo al saldo de la CTACTE.
	 * @return true si el saldo permite la transferencia. false en caso contrario.
	 * @throws TransferenciaException
	 * @since 1.0
	 * @see {@link wcorp.bprocess.cuentas.TransferenciasFondosBean#validarSaldoCtaCte(String,double,boolean)
	 */
	private boolean validarSaldoCtaCte(Cliente cliente, String canal, String ctaOrigen, double monto, boolean consideraLDC) throws TransferenciaException {
		CuentaCorriente cuentaCorriente= null;
		SaldoCtaCte saldo= null;

		try {
			cuentaCorriente = new CuentaCorriente(ctaOrigen);
			saldo= cuentaCorriente.getSaldo();
			double saldoLSG= 0;

			if (consideraLDC) {
				saldoLSG= saldo.sobregiroDisp;
			}

			String msg= "";
			msg= "CuentaOrigen=" + ctaOrigen;
			msg += ", SaldoDisponible=" + saldo.saldoDisponible;
			msg += ", SaldoLSG=" + saldoLSG;
			msg += ", Monto=" + monto;

			logCliente(cliente,canal,"validarSaldoCtaCte", msg);

			if ((saldo.saldoDisponible + saldoLSG) < monto) {
				return false;
			}

			return true;
		} catch (Exception e) {
			String mensaje= "CuentaOrigen=" + ctaOrigen + ", Monto=" + monto + ": ";
			logCliente(cliente,canal,"validarSaldoCtaCte", mensaje+e);
			TransferenciaException te = new TransferenciaException("TRANSFERENCIA", "Error calculando saldo");
			te.setException(e);
			throw te;
		}
	}
	/**
	 * Registra el ingreso de la transferencia
	 *
	 * Metodo que registra una Transferencia en L�nea en la Tabla tra, BD ctaster, con los parametros
	 * ingresados por el cliente en la capa de presentaci�n.
	 *
	 * Se trae m�todo desde {@link wcorp.serv.cuentas.ServiciosCuentasBean#} (ya que esta se encuentra deprecada),
	 * y se la aplican los cambios necesarios para que reciba dos par�metros: tipoTrans, que corresponde al tipo de
	 * transferencia (Otros Bancos, en L�nea, etc.) y codAlias, que corresponde al identificador del alias (objeto
	 * en el que se setean los par�metros ingresados por el cliente en la transferencia).
	 *
	 * Registro de Versiones:<UL>
	 *
	 * <li>1.0 26/03/2009, Carlos Cerda I. (SEnTRA)-Desconocido(Ing. Soft. BCI): Versi�n inicial.
	 * <li>1.1 25/05/2009, Marianela Sabando M. (BCI)-Marianela Sabando M.(Ing. Soft. BCI): Se valida largo de comentario, nombre destino y e-mail de destino, para no superar largo m�ximo de la base de datos.
	 * <li>1.2 11/01/2012, Leonardo Quevedo M. (Orand S.A.)-Desconocido(Ing. Soft. BCI): Se cambia el modificador de acceso (de private a public) a fin de registrar el
	 * estado inicial de un pago de TC (proyecto Pago TC Servipag).</li>
	 * <li>1.3 05/12/2016 Susana Soto Macher (SEnTRA)-Juan Jos� Buend�a(Ing. Soft. BCI): Se corrige validaciones a Comentario, Nombre Destino e Email Beneficiario
	 * para considerar correctamente espacios en blanco dentro de la cantidad m�xima de caracteres. Adem�s se cambiaron valores en duro por constantes</li> 
	 * </UL>
	 * 
	 * @param cliente Cliente que se transfiere.
	 * @param tipoCuentaOrigen Tipo cuenta.
	 * @param ctaOrigen Cuenta origen.
	 * @param montoTransferencia Monto de la Transferencia.
	 * @param fecha fecha de cargo.
	 * @param estado Estado de la transferencia.
	 * @param tipoCuentaDestino Tipo cuenta de destino.
	 * @param ctaDestino Cuenta de Destino.
	 * @param rutDestino Rut del destinatario.
	 * @param codigoBanco C�digo Banco.
	 * @param canalID C�digo del canal.
	 * @param descripcion Descripci�n.
	 * @param codigoRechazo C�digo de rechazo.
	 * @param comentario Comentario.
	 * @param nombreDestino Nombre destinatario.
	 * @param eMailAlternativo Email alternativo.
	 * @param eMailBeneficiario Email beneficiario.
	 * @param usuario usuario.
	 * @param tipoTrans Tipo de transferencia.
	 * @param codAlias  C�digo del destinatario.
	 *
	 * @return RegistroTransferencia
	 * @throws CuentasException excepci�n para cuentas. 
	 * @throws TuxedoException excepci�n para tuxedo.
	 * @throws RemoteException excepci�n de remote.
	 * @since 1.0
	 * @see {@link wcorp.serv.cuentas.ServiciosCuentasBean#ingresaTransferencia(long, String, String, double, Date, String, Date, String, String, long, String, String, String, String, String, String, String, String, String)}
	 */
    public RegistroTransferencia ingresaTransferencia(Cliente cliente,
			String tipoCuentaOrigen,
			String ctaOrigen,
			double montoTransferencia,
			Date fecha,
			String estado,
			String tipoCuentaDestino,
			String ctaDestino,
			long rutDestino,
			String codigoBanco,
			String canalID,
			String descripcion,
			String codigoRechazo,
			String comentario,
			String nombreDestino,
			String eMailAlternativo,
			String eMailBeneficiario,
			String usuario,
			String tipoTrans,
			String codAlias) throws CuentasException, TuxedoException, RemoteException{

		sesion = joltPool.getSesion(ejbName);
		DataSet parametros = new DataSet();

		parametros.setValue("rut", String.valueOf(cliente.getRut()));
		parametros.setValue("tipoCuentaOrigen", tipoCuentaOrigen);
		parametros.setValue("ctaOrigen", ctaOrigen.length() > LARGO_STRING_DIEZ ? ctaOrigen.substring( ctaOrigen.length()-LARGO_STRING_DIEZ) : ctaOrigen );
		parametros.setValue("montoTransfer", String.valueOf(montoTransferencia));
		parametros.setValue("estado", estado);
		parametros.setValue("tipoCuentaDestino", tipoCuentaDestino);
		parametros.setValue("ctaDestino", ctaDestino);
		parametros.setValue("rutDestin", String.valueOf(rutDestino));
		parametros.setValue("codBanco", codigoBanco);
		parametros.setValue("canal", canalID);
		parametros.setValue("usuario", String.valueOf(cliente.getRut()));
		parametros.setValue("tipo", tipoTrans);
		parametros.setValue("alias", codAlias);

		if (descripcion != null) {
			parametros.setValue("descripcion", descripcion);
			logCliente(cliente,canalID,"ingresaTransferencia", "descripcion: "+descripcion);
		}
		if (fecha != null) {
			parametros.setValue("fecCargo", new SimpleDateFormat("yyyyMMdd").format(fecha));
			logCliente(cliente,canalID,"ingresaTransferencia", "fechaCargo: "+new SimpleDateFormat("yyyyMMdd").format(fecha));
			parametros.setValue("fecAbono", new SimpleDateFormat("yyyyMMdd").format(fecha));
			logCliente(cliente,canalID,"ingresaTransferencia", "fechaAbono: "+new SimpleDateFormat("yyyyMMdd").format(fecha));
		}

		if (codigoRechazo != null) {
			parametros.setValue("codRechazo", codigoRechazo);
			logCliente(cliente,canalID,"ingresaTransferencia", "codigoRechazo: "+codigoRechazo);
		}
		if (comentario != null) {
			comentario = comentario.trim();
			if(comentario.length() > LARGO_COMENTARIO_TRF){
				comentario = comentario.substring(INICIO_CORTE, LARGO_COMENTARIO_TRF);
			}
			parametros.setValue("comentario", comentario);
			logCliente(cliente,canalID,"ingresaTransferencia", "comentario: "+comentario);
		}
		if (nombreDestino != null) {
			nombreDestino = nombreDestino.trim();
			if (nombreDestino.length() > LARGO_NOMBRE){
				nombreDestino = nombreDestino.substring(INICIO_CORTE, LARGO_NOMBRE);
			}
				parametros.setValue("nombreDestin", nombreDestino);
			logCliente(cliente,canalID,"ingresaTransferencia", "nombreDestino: "+nombreDestino);
		}
		if (eMailAlternativo != null) {
			parametros.setValue("emailAlter", eMailAlternativo);
			logCliente(cliente,canalID,"ingresaTransferencia", "eMailAlternativo: "+eMailAlternativo);
		}

		logCliente(cliente,canalID,"ingresaTransferencia", "GUARDANDO PARA RUT=" + cliente.getRut() + ", eMailBeneficiario=[" + eMailBeneficiario + "], monto=" + montoTransferencia + ", ctaDestino=" + ctaDestino);

		if (eMailBeneficiario != null) {
			eMailBeneficiario = eMailBeneficiario.trim();
			if (eMailBeneficiario.length() > LARGO_EMAIL_BENEF){
				eMailBeneficiario = eMailBeneficiario.substring(INICIO_CORTE,LARGO_EMAIL_BENEF);
			}
			parametros.setValue("emailBenef", eMailBeneficiario);
			logCliente(cliente,canalID,"ingresaTransferencia", "eMailBeneficiario: ["+eMailBeneficiario+"]");

		}


		Result resultado = null;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");

		try {
			logCliente(cliente,canalID,"ingresaTransferencia", "INVOCANDO CtaCteIngTraLin");
			resultado = sesion.call("CtaCteIngTraLin", parametros, null);
			logCliente(cliente,canalID,"ingresaTransferencia", "CtaCteIngTraLin, INVOCADO");

			RegistroTransferencia reg = new RegistroTransferencia();
			reg.numeroOperacion = (String)resultado.getValue("numOperacion", POSICION_INICIAL, null);
			logCliente(cliente,canalID,"ingresaTransferencia", "numOperacion: "+reg.numeroOperacion);
			String fecIng = (String)resultado.getValue("fechaIngreso", POSICION_INICIAL, null);
			reg.fechaIngreso = sdf.parse(fecIng != null && !fecIng.trim().equals("")? fecIng : "19700101", new ParsePosition(0));
			logCliente(cliente,canalID,"ingresaTransferencia", "fechaIngreso: "+reg.fechaIngreso);
			return reg;
		} 
		catch (ApplicationException e) {
			logCliente(cliente,canalID,"ingresaTransferencia", "ERROR CtaCteIngresTraLin (" + e + ")");
			// Errores Aplicativos
			resultado = e.getResult();
			if (resultado.getApplicationCode() == CODIGO_ERROR_CUENTA_EXCEPTION) {
				//Error de Negocio Colocaciones
				throw new CuentasException((String)resultado.getValue("codigoError", POSICION_INICIAL, "DESC"),
						(String)resultado.getValue("descripcionError", POSICION_INICIAL, "[Sin Informacion]"));
			} 
			else {
				// Error Tuxedo
				throw new TuxedoException((String)(resultado.getApplicationCode() == CODIGO_ERROR_TUXEDO_EXCEPTION ? resultado.getValue("codigoError", POSICION_INICIAL, "DESC") : "TUX"));
			}
		}
	}
	/**
	 * Obtiene el tope dado el tipo de cuenta, el rut y el canal.
	 * Se trae m�todo desde {@link wcorp.bprocess.cuentas.TransferenciasFondosBean#} a la implementaci�n Stateless del mismo y
	 * se cambia nivel de logeo de debug a info.
	 *
	 * Registro de Versiones:<UL>
	 *
	 * <li>1.0 26/03/2009, Carlos Cerda I. (SEnTRA): Versi�n inicial.
	 *
	 * @param rut
	 * @param canal
	 * @param tipoCuenta
	 * @param rutDestino
	 * @param tipoAutenticacion
	 * @return double
	 * @throws TransferenciaException
	 *
	 * @since 1.0
	 * @see {@link wcorp.bprocess.cuentas.TransferenciasFondosBean#maximoTopeCuenta(long,String,String,long,String)}
	 */
	private double maximoTopeCuenta(
			long rut,
			String canal,
			String tipoCuenta,
			long rutDestino,
			String tipoAutenticacion) throws TransferenciaException {

		String tope;

		if (rutDestino == rut) {
			if (tipoAutenticacion.equalsIgnoreCase("Passcode")) {
				if (tipoCuenta.equalsIgnoreCase("CCT")) {
					tope = TablaValores.getValor(TTFF_CONFIG, canal, "topeMPCCCTPASS");
					if(logger.isInfoEnabled()){logger.info("[maximoTopeCuenta]:: Passcode topeMPCCCTPASS: " + tope);}
					return Double.valueOf(tope).doubleValue();
				}

				if (tipoCuenta.equalsIgnoreCase("CPR")) {
					tope = TablaValores.getValor(TTFF_CONFIG, canal, "topeMPCCPRPASS");
					if(logger.isInfoEnabled()){logger.info("[maximoTopeCuenta]:: Passcode topeMPCCPRPASS: " + tope);}
					return Double.valueOf(tope).doubleValue();
				}
			} else if (tipoAutenticacion.equalsIgnoreCase("Token")) {
				if (tipoCuenta.equalsIgnoreCase("CCT")) {
					tope = TablaValores.getValor(TTFF_CONFIG, canal, "topeMPCCCTTOK");
					if(logger.isInfoEnabled()){logger.info("[maximoTopeCuenta]:: Token topeMPCCCTTOK: " + tope);}
					return Double.valueOf(tope).doubleValue();
				}
				if (tipoCuenta.equalsIgnoreCase("CPR")) {
					tope = TablaValores.getValor(TTFF_CONFIG, canal, "topeMPCCPRTOK");
					if(logger.isInfoEnabled()){logger.info("[maximoTopeCuenta]:: Token topeMPCCPRTOK: " + tope);}
					return Double.valueOf(tope).doubleValue();
				}
			} else {
				if (tipoCuenta.equalsIgnoreCase("CCT")) {
					tope = TablaValores.getValor(TTFF_CONFIG, canal, "topeMPCCCT");
					if(logger.isInfoEnabled()){logger.info("[maximoTopeCuenta]:: no tengo PassCode ni Tokentope topeMPCCCT: " + tope);}
					return Double.valueOf(tope).doubleValue();
				}

				if (tipoCuenta.equalsIgnoreCase("CPR")) {
					tope = TablaValores.getValor(TTFF_CONFIG, canal, "topeMPCCPR");
					if(logger.isInfoEnabled()){logger.info("[maximoTopeCuenta]:: no tengo PassCode ni Token topeMPCCPR: " + tope);}
					return Double.valueOf(tope).doubleValue();
				}
			}
		} else {
			if (tipoAutenticacion.equalsIgnoreCase("Passcode")) {
				// Cliente tiene PassCode
				if (tipoCuenta.equalsIgnoreCase("CCT")) {
					tope = TablaValores.getValor(TTFF_CONFIG, canal, "topeCCTPASS");
					if(logger.isInfoEnabled()){logger.info("[maximoTopeCuenta]:: (Hacia terceros)Passcode topeCCTPASS: " + tope);}
					return Double.valueOf(tope).doubleValue();
				}

				if (tipoCuenta.equalsIgnoreCase("CPR")) {
					tope = TablaValores.getValor(TTFF_CONFIG, canal, "topeCPRPASS");
					if(logger.isInfoEnabled()){logger.info("[maximoTopeCuenta]:: (Hacia terceros)Passcode topeCPRPASS: " + tope);}
					return Double.valueOf(tope).doubleValue();
				}
			} else if (tipoAutenticacion.equalsIgnoreCase("Token")) {
				if (tipoCuenta.equalsIgnoreCase("CCT")) {
					tope = TablaValores.getValor(TTFF_CONFIG, canal, "topeLINEACCTTOKEN");
					if(logger.isInfoEnabled()){logger.info("[maximoTopeCuenta]:: (Hacia terceros)Token topeLINEACCTTOKEN: " + tope);}
					return Double.valueOf(tope).doubleValue();
				}

				if (tipoCuenta.equalsIgnoreCase("CPR")) {
					tope = TablaValores.getValor(TTFF_CONFIG, canal, "topeLINEACPRTOKEN");
					if(logger.isInfoEnabled()){logger.info("[maximoTopeCuenta]:: (Hacia terceros)Token topeLINEACPRTOKEN: " + tope);}
					return Double.valueOf(tope).doubleValue();
				}
			} else {
				if (tipoCuenta.equalsIgnoreCase("CCT")) {
					tope = TablaValores.getValor(TTFF_CONFIG, canal, "topeCCT");
					if(logger.isInfoEnabled()){logger.info("[maximoTopeCuenta]:: (Hacia terceros) Ni Token Ni pasccode topeCCT: " + tope);}
					return Double.valueOf(tope).doubleValue();
				}

				if (tipoCuenta.equalsIgnoreCase("CPR")) {
					tope = TablaValores.getValor(TTFF_CONFIG, canal, "topeCPR");
					if(logger.isInfoEnabled()){logger.info("[maximoTopeCuenta]:: (Hacia terceros) Ni Token Ni pasccode topeCPR: " + tope);}
					return Double.valueOf(tope).doubleValue();
				}
			}
		}
		return 0;
	}
	/**
	 * M�todo que forma el cuerpo del Email de destino a ser enviado.
	 *
	 * Se trae m�todo desde {@link wcorp.bprocess.cuentas.TransferenciasFondosBean#} a la implementaci�n Stateless del mismo,
	 * se cambia nivel de logeo de debug a info y se agrega a la firma del m�todo el cliente y canal.
	 *
	 * Registro de Versiones:<ul>
	 *
	 * <li>1.0 26/03/2009, Carlos Cerda I. (SEnTRA): Versi�n inicial.
     * <li>2.0 13/05/2010 Eduardo Mascayano (TInet), Hernan Rodriguez (TInet): Se modifica la l�gica para generar el comprobante/correo en HTML y Texto plano.
     * Seg�n regla de negocio, se implementa s�lo para los canales Bcinet, Nova y Tbanc. agrega opcion modoCorreoHTML para utilizar modo envio correo HTML actual
	 * o antiguo modo de envio de correo.
	 * <li>2.1 (04-08-2011 Manuel Hern�ndez A. (OS.ONE), Carlos Garrido G. (OS.ONE)) : Se agrega condici�n con respecto al canal de origen, que cuando el canal
     * de origen sea bci express new (230) se utilizar� el canal por defecto (110) para confeccionar el cuerpo del email a enviar. Se sigue la recomendaci�n de {@link wcorp.util.mime.formatos.FormatoXSLT#CANAL_POR_DEFECTO}
     * que a fecha de hoy dice "Por regla de negocio, Bcinet ser� el canal por defecto a utilizar en la generaci�n del componente en caso de que el canal especificado a�n no est� implementado."</li>
         * <li>2.2 25/11/2015 Ernesto Vera(SEnTRA)-Claudia Lopez(Ing Soft. BCI.): Se agregan lineas para formateo con punto del valopr de la variable detalle.getMontoTransferencia().</li>
	 * </ul>
	 *
	 *
	 * @param detalle Objeto con la informaci�n de la transferencia realizada.
	 * @param otroBanco boolean que indica si la transferencia es realizado a otro banco
	 * @param cliente
	 * @param canal
	 * @return void
	 *
	 * @since 1.0
	 * @see {@link wcorp.bprocess.cuentas.TransferenciasFondosBean#enviaEmail(DetalleTransferencia,boolean) }
	 */
	private void enviaEmail(DetalleTransferencia detalle, boolean otroBanco, Cliente cliente, String canal) {
		
		if (getLogger().isInfoEnabled()) {
    		getLogger().info("[enviaEmail][BCI_INI][" + cliente.getRut() + "]");  
    	}
		
		logCliente(cliente,canal,"MAILDESTINO", "Email=" + detalle.eMailDestino + ", otroBanco=" + otroBanco);
		
		
		
		if (detalle.eMailDestino == null || detalle.eMailDestino.trim().equals("")){
			if (getLogger().isInfoEnabled()) {
	    		getLogger().info("[enviaEmail][FIN_OK][" + cliente.getRut() + "]");  
	    	}
			return;
		}

		String subject= "Aviso de Transferencia de Fondos.";
		String msg= "";


        String modoCorreoHTML = TablaValores.getValor(TABLA_GEN_COMP_MIME, "MODALIDAD_CORREO_RECARGA_CELULAR", "MODO_HTML");

        String montoString =  formatearMontoMail(detalle.montoTransferencia);


        if ((modoCorreoHTML == null) || (modoCorreoHTML.equalsIgnoreCase("NO"))) {
		if (otroBanco) {
			msg += "Estimado(a) " + detalle.getNombreDestino() + "\n";
			msg += "\n";
			msg
			+= TextosUtil.parrafo(
					"De acuerdo con lo instruido por nuestro cliente "
					+ detalle.getNombreOrigen()
					+ ", le informamos que con fecha "
					+ new SimpleDateFormat("dd/MM/yyyy").format(detalle.getFechaTransferencia())
					+ " se ha realizado una transferencia de fondos hacia su cuenta del banco "
					+ detalle.getNombreBancoDestino().trim()
					+ " nro. #"
					+ detalle.ctaDestino
					+ ".");
			msg += "\n";
			msg += "El detalle de la operacion es el siguiente:\n";
			msg += "\n";
			msg += "    Monto transferido               : $"
			    + montoString
				+ "\n";
			msg += "    Titular de la cuenta de origen  : "
				+ ((detalle.getNombreOrigen() != null) ? detalle.getNombreOrigen() : "")
				+ "\n";
			msg += "    Banco de origen                 : "
				+ ((detalle.getNombreBancoOrigen() != null)
						? detalle.getNombreBancoOrigen().trim()
								: "")
								+ "\n";
			msg += "    Comentario para el destinatario : "
				+ ((detalle.getComentario() != null) ? detalle.getComentario().trim() : "")
				+ "\n";
			msg += "    Numero de la operacion          : "
				+ ((detalle.getNumeroOperacion() != null) ? detalle.getNumeroOperacion() : "")
				+ "\n";
			msg += "    Fecha de abono                  : "
				+ ((detalle.getFechaAbono() != null) ? new SimpleDateFormat("dd/MM/yyyy").format(detalle.getFechaAbono()) : "")
				+ "\n";
			msg += "\n";
			msg += "Atte,\n";
			msg += (detalle.canalID.equals("100") ? "TBanc" : "BCI - Banco Credito Inversiones")
			+ "\n";
			msg += "\n";
			msg += "Importante:\n";
			msg
			+= TextosUtil.parrafo("Este mail es generado de manera automatica, por favor no responda a este mensaje. Ante cualquier duda, contactese con nuestro cliente.");
			msg += "\n";
			msg += "P.D.: Los tildes han sido omitidos en forma intencional.\n";
			msg += "\n";

		} else { // transferencia a tercero BCI/TBanc
			msg += "Estimado(a) " + detalle.getNombreDestino() + "\n";
			msg += "\n";
			msg
			+= TextosUtil.parrafo(
					"De acuerdo con lo instruido por nuestro cliente "
					+ detalle.getNombreOrigen()
					+ ", le informamos que con fecha "
					+ new SimpleDateFormat("dd/MM/yyyy").format(detalle.getFechaTransferencia())
					+ " se ha realizado una transferencia de fondos hacia su cuenta"
					+ " nro. #"
					+ detalle.ctaDestino
					+ ".");
			msg += "\n";
			msg += "El detalle de la operacion es el siguiente:\n";
			msg += "\n";
			msg += "    Monto transferido               : $"
				+ montoString
				+ "\n";
			msg += "    Titular de la cuenta de origen  : "
				+ ((detalle.getNombreOrigen() != null) ? detalle.getNombreOrigen() : "")
				+ "\n";
			msg += "    Banco de origen                 : "
				+ ((detalle.getNombreBancoOrigen() != null)
						? detalle.getNombreBancoOrigen().trim()
								: "")
								+ "\n";
			msg += "    Comentario para el destinatario : "
				+ ((detalle.getComentario() != null) ? detalle.getComentario().trim() : "")
				+ "\n";
			msg += "    Numero de la operacion          : "
				+ ((detalle.getNumeroOperacion() != null) ? detalle.getNumeroOperacion() : "")
				+ "\n";
			msg += "    Fecha de abono                  : "
				+ ((detalle.getFechaAbono() != null) ? new SimpleDateFormat("dd/MM/yyyy").format(detalle.getFechaAbono()) : "")
				+ "\n";
			msg += "\n";
			msg += "Atte,\n";
			msg += (detalle.canalID.equals("100") ? "TBanc" : "BCI - Banco Credito Inversiones")
			+ "\n";
			msg += "\n";
			msg += "Importante:\n";
			msg
			+= TextosUtil.parrafo(
					"Este mail es generado de manera automatica, por favor no responda a este mensaje. Ante cualquier duda, contactese al "
					+ (detalle.canalID.equals("100") ? "600 524 24 24" : "600 824 24 24")
					+ " o con nuestro cliente.");
			msg += "\n";
			msg += "P.D.: Los tildes han sido omitidos en forma intencional.\n";
			msg += "\n";

		}


        } else {

        String tipoComprobante = "";
        tipoComprobante = (otroBanco) ? ID_COMP_DEST_OTRO_BANCO : ID_COMP_DEST_BCI;

        GeneradorComprobanteTransferenciaDeFondos componente = new GeneradorComprobanteTransferenciaDeFondos();
        componente.setNombreCliente(detalle.getNombreOrigen());
        componente.setBancoOrigen(detalle.getNombreBancoOrigen());

        componente.setNombreDestinatario(detalle.getNombreDestino());
        componente.setBancoDestino(detalle.getNombreBancoDestino());
        componente.setCuentaDestino(detalle.getCtaDestino());

        componente.setNumeroOperacion(detalle.getNumeroOperacion());
        componente.setFechaTransferencia(detalle.getFechaTransferencia());
        componente.setFechaAbono(detalle.getFechaAbono());
        componente.setMonto(detalle.getMontoTransferencia());
        componente.setComentario(detalle.getComentario());
        componente.setTipoComprobante(tipoComprobante);

        if (logger.isDebugEnabled()) {
            logger.debug("[enviaEmail] componente.toString() [" + componente.toString() + "]");
        }

        Map parametros = new HashMap();
        if(canal.equals(CANAL_BCIEXPRESS_NEW))
        {
            canal=CANAL_CORREO_BCI;
        }
        parametros.put("canal", canal);
        parametros.put("otroBanco", (otroBanco) ? "S" : "N");
        GeneradorDeComponentesMIME generador = new GeneradorDeComponentesMIME();
        generador.setComponente(componente);
        generador.agregarFormatoHtml(tipoComprobante, canal);
        Map correos = generador.generarComponentes(parametros);
        String correoTexto = (String) correos.get("XSLT_" + tipoComprobante + "_TXT_" + canal);
        if (correoTexto != null) {
            msg = correoTexto;
            String correoHtml = (String) correos.get("XSLT_" + tipoComprobante + "_HTML_" + canal);
            if (correoHtml != null) {
                msg = "MIME" + correoTexto + "|T&H|" + correoHtml;
            }
            else {
                logger.warn("No se enviara correo en formato HTML");
            }
        }

		}

		if (msg != "") {
            logCliente(cliente, canal, "MAILDESTINO", msg);
            mail(detalle.eMailDestino, msg, subject, cliente, canal);
            logCliente(cliente, canal, "MAILDESTINO", "Mail enviado");
        }else{
            if (getLogger().isInfoEnabled()) {
            	getLogger().info("[enviaEmail][BCI_FINOK] Error al generar el contenido HTML/Texto Plano del mail");
            }
            
            logger.warn("[enviaEmail] Error al generar el contenido HTML/Texto Plano del mail");
        }

	}

	/**
	 * M�todo que forma el cuerpo del Email del cliente a ser enviado.
	 *
	 * Se trae m�todo desde {@link wcorp.bprocess.cuentas.TransferenciasFondosBean#} a la implementaci�n Stateless del mismo y
	 * se agrega a la firma del m�todo el cliente y canal.
	 *
	 * Registro de Versiones:<ul>
	 *
	 * <li>1.0 26/03/2009, Carlos Cerda I. (SEnTRA): Versi�n inicial.
	 * <li>2.0 13/05/2010 Eduardo Mascayano (TInet), Hernan Rodriguez (TInet): Se modifica la l�gica para generar el comprobante/correo en HTML y Texto plano.
     * Seg�n regla de negocio, se implementa s�lo para los canales Bcinet, Nova y Tbanc. agrega opcion envio correo html y envio
	 * de correo texto plano.
         * <li>2.1 25/11/2015 Ernesto Vera(SEnTRA)-Claudia Lopez(Ing Soft. BCI.): Se agregan lineas para formateo con punto del valopr de la variable detalle.getMontoTransferencia().</li>
	 * </ul>
	 *
	 *
	 * @param detalle Objeto con la informaci�n de la transferencia realizada.
	 * @param cliente
	 * @param canal
	 * @return void
	 *
	 * @since 1.0
	 * @see {@link wcorp.bprocess.cuentas.TransferenciasFondosBean#enviaMailCliente(DetalleTransferencia detalle)}
	 */
	private void enviaMailCliente(DetalleTransferencia detalle, Cliente cliente, String canal) {
		logCliente(cliente,canal,"MAILCLIENTE", "EmailCliente=" + detalle.eMailCliente);

		if (detalle.eMailCliente == null || detalle.eMailCliente.trim().equals(""))
			return;

		logCliente(cliente,canal,"MAILCLIENTE canal ID", detalle.canalID);

		String subject= "";
		String msg= "";

        String montoString =  formatearMontoMail(detalle.getMontoTransferencia());
        
		if (detalle.canalID.equals("150") || detalle.canalID.equals("151")){

			logCliente(cliente,canal,"MAILCLIENTE detalle.tipoCuentaOrigen [", detalle.tipoCuentaOrigen);
			logCliente(cliente,canal,"MAILCLIENTE detalle.tipoCuentaOrigen [", detalle.tipoCuentaDestino);


			if (detalle.tipoCuentaOrigen.trim().equals("CCT") && detalle.tipoCuentaDestino.trim().equals("TDC")){

				subject= "Aviso de Pago Tarjeta de Credito.";

				msg += "Estimado(a) " + detalle.getNombreOrigen() + "\n";
				msg += "\n";

				msg
				+= TextosUtil.parrafo(
						"Con fecha "
						+ new SimpleDateFormat("dd/MM/yyyy").format(detalle.getFechaTransferencia())
						+ " se ha realizado un pago de su tarjeta de credito desde "
						+ "su cuenta nro. #"
						+ StringUtil.rellenaPorLaIzquierda(detalle.getCtaOrigen(), 8, '0')
						+ ". El detalle de esta operacion es el siguiente:");
				msg += "\n";

				msg += "    Monto transferido        : $"
					+ montoString
					+ "\n";

				msg += "    Nombre del destinatario  : "
					+ ((detalle.getNombreDestino() != null) ? detalle.getNombreDestino() : "")
					+ "\n";

				msg += "    Numero de tarjeta        : XXXX-XXXX-XXXX-"
					+ ((detalle.getCtaDestino() != null) ? detalle.getCtaDestino().substring(detalle.getCtaDestino().length() - 4) : "")
					+ "\n";


				msg += "    Numero de la operacion   : "
					+ ((detalle.getNumeroOperacion() != null) ? detalle.getNumeroOperacion() : "")
					+ "\n";

				msg += "\n";
				msg += "Atte,\n";
				msg += "BCI - Banco Credito Inversiones" + "\n";
				msg += "\n";
				msg += "Importante:\n";
				msg += TextosUtil.parrafo(
						"Este mail es generado de manera automatica, por favor no responda"
						+ " a este mensaje. Ante cualquier duda, contactese al 600 824 24 24.");
				msg += "\n";
				msg += "P.D.: Los tildes han sido omitidos en forma intencional.\n";
				msg += "\n";

			}else if (detalle.tipoCuentaOrigen.trim().equals("TDC") && detalle.tipoCuentaDestino.trim().equals("CCT")) {

				subject= "Aviso de Avance Tarjeta de Credito.";

				msg += "Estimado(a) " + detalle.getNombreOrigen() + "\n";
				msg += "\n";

				msg
				+= TextosUtil.parrafo(
						"Con fecha "
						+ new SimpleDateFormat("dd/MM/yyyy").format(detalle.getFechaTransferencia())
						+ " se ha realizado un avance en efectivo desde "
						+ "su cuenta nro. #XXXX-XXXX-XXXX-"
						+ detalle.getCtaOrigen().substring(detalle.getCtaOrigen().length() - 4)
						+ ". El detalle de esta operacion es el siguiente:");
				msg += "\n";

				msg += "    Monto transferido        : $"
					+ montoString
					+ "\n";

				msg += "    Nombre del destinatario  : "
					+ ((detalle.getNombreDestino() != null) ? detalle.getNombreDestino() : "")
					+ "\n";

				msg += "    Cuenta de destino        : "
					+ ((detalle.getCtaDestino() != null) ? detalle.getCtaDestino() : "")
					+ "\n";

				msg += "    Numero de la operacion   : "
					+ ((detalle.getNumeroOperacion() != null) ? detalle.getNumeroOperacion() : "")
					+ "\n";
				msg += "\n";
				msg += "Atte,\n";
				msg += "BCI - Banco Credito Inversiones" + "\n";
				msg += "\n";
				msg += "Importante:\n";
				msg += TextosUtil.parrafo(
						"Este mail es generado de manera automatica, por favor no responda"
						+ " a este mensaje. Ante cualquier duda, contactese al 600 824 24 24.");
				msg += "\n";
				msg += "P.D.: Los tildes han sido omitidos en forma intencional.\n";
				msg += "\n";

			}

		}else{


        String modoCorreoHTML = TablaValores.getValor(TABLA_GEN_COMP_MIME, "MODALIDAD_CORREO_RECARGA_CELULAR", "MODO_HTML");

        if ((modoCorreoHTML == null) || (modoCorreoHTML.equalsIgnoreCase("NO"))) {

			subject= "Aviso de Transferencia de Fondos.";

			msg += "Estimado(a) " + detalle.getNombreOrigen() + "\n";
			msg += "\n";

			msg
			+= TextosUtil.parrafo(
					"Con fecha "
					+ new SimpleDateFormat("dd/MM/yyyy").format(detalle.getFechaTransferencia())
					+ " se ha realizado una transferencia de fondos desde "
					+ (detalle.canalID.equals("100") ? "t" : "s")
					+ "u cuenta nro. #"
					+ StringUtil.rellenaPorLaIzquierda(detalle.getCtaOrigen(), 8, '0')
					+ ". El detalle de esta operacion es el siguiente:");
			msg += "\n";
			msg += "    Monto transferido        : $"
				+ montoString
				+ "\n";
			msg += "    Nombre del destinatario  : "
				+ ((detalle.getNombreDestino() != null) ? detalle.getNombreDestino() : "")
				+ "\n";
			msg += "    Banco de destino         : "
				+ ((detalle.getCodigoBancoDestino() != null && detalle.getNombreBancoDestino() != null)
						? detalle.getNombreBancoDestino().trim()
								: "")
								+ "\n";
			msg += "    Cuenta de destino        : "
				+ ((detalle.getCtaDestino() != null) ? detalle.getCtaDestino() : "")
				+ "\n";
			msg += "    Comentario transferencia : "
				+ ((detalle.getComentario() != null) ? detalle.getComentario() : "")
				+ "\n";
			msg += "    Numero de la operacion   : "
				+ ((detalle.getNumeroOperacion() != null) ? detalle.getNumeroOperacion() : "")
				+ "\n";
			msg += "    Fecha de abono           : "
				+ ((detalle.getFechaAbono() != null) ? new SimpleDateFormat("dd/MM/yyyy").format(detalle.getFechaAbono()) : "")
				+ "\n";
			msg += "\n";

			msg += "Atte,\n";
			msg += (detalle.canalID.equals("100") ? "TBanc" : "BCI - Banco Credito Inversiones") + "\n";
			msg += "\n";
			msg += "Importante:\n";
			msg
			+= TextosUtil.parrafo(
					"Este mail es generado de manera automatica, por favor no responda"
					+ (detalle.canalID.equals("100") ? "s" : "")
					+ " a este mensaje. Ante cualquier duda, contact"
					+ (detalle.canalID.equals("100") ? "ate" : "ese")
					+ " al "
					+ (detalle.canalID.equals("100") ? "600 524 24 24" : "600 824 24 24")
					+ ".");
			msg += "\n";
			msg += "P.D.: Los tildes han sido omitidos en forma intencional.\n";
			msg += "\n";

} else {


			subject= "Aviso de Transferencia de Fondos.";
            GeneradorComprobanteTransferenciaDeFondos componente = new GeneradorComprobanteTransferenciaDeFondos();
            componente.setNombreCliente(detalle.getNombreOrigen());
            componente.setCuentaOrigen(detalle.getCtaOrigen());

            componente.setNombreDestinatario(detalle.getNombreDestino());
            componente.setBancoDestino(detalle.getNombreBancoDestino());
            componente.setCuentaDestino(detalle.getCtaDestino());
            componente.setCorreoDestinatario(detalle.getEMailDestino());

            componente.setNumeroOperacion(detalle.getNumeroOperacion());
            componente.setFechaTransferencia(detalle.getFechaTransferencia());
            componente.setFechaAbono(detalle.getFechaAbono());
            componente.setMonto(detalle.getMontoTransferencia());
            componente.setComentario(detalle.getComentario());
            componente.setTipoComprobante(ID_COMP_CLI);

            Map parametros = new HashMap();
            parametros.put("canal", canal);
            GeneradorDeComponentesMIME generador = new GeneradorDeComponentesMIME();
            generador.setComponente(componente);
            generador.agregarFormatoHtml(ID_COMP_CLI, canal);
            Map correos = generador.generarComponentes(parametros);
            String correoTexto = (String) correos.get("XSLT_" + ID_COMP_CLI + "_TXT_" + canal);
            if (correoTexto != null) {
                msg = correoTexto;
                String correoHtml = (String) correos.get("XSLT_" + ID_COMP_CLI + "_HTML_" + canal);
                if (correoHtml != null) {
                    msg = "MIME" + correoTexto + "|T&H|" + correoHtml;
                }
                else {
                    logger.warn("[enviaMailCliente]No se enviara correo en formato HTML");
                }
            }
        }

		}



        if (msg != "") {
            logCliente(cliente, canal, "MAILCLIENTE", msg);
            mail(detalle.eMailCliente, msg, subject, cliente, canal);
            logCliente(cliente, canal, "MAILCLIENTE", "Mail enviado");
        }
        else {
            logger.warn("[enviaMailCliente] Error al generar el contenido HTML/Texto Plano del mail");
        }


	}
	/**
	 * M�todo que realiza el env�o de Email.
	 * Se trae m�todo desde {@link wcorp.bprocess.cuentas.TransferenciasFondosBean#} a la implementaci�n Stateless del mismo y
	 * se agrega a la firma del m�toso el cliente y canal.
	 *
	 * Registro de Versiones:<ul>
	 *
	 * <li>1.0 26/03/2009, Carlos Cerda I. (SEnTRA): Versi�n inicial.
	 *
	 * <li>2.0 04/02/2013, Claudia Aracena F. (SEnTRA) :
	 *  Se agrega la l�gica para enviar correo por medio de latinia, desde configuracion
	 *  en tabla de par�metros.</li>
	 * </ul>
	 *
	 *
	 * @param eMail email
	 * @param msg  mensaje
	 * @param subject subject del mensaje
	 * @param cliente  cliente
	 * @param canal   canal
	 * @return void  retorna
	 *
	 * @since 1.0
	 * @see {@link wcorp.bprocess.cuentas.TransferenciasFondosBean#mail(String eMail, String msg, String subject)}
	 */
	private void mail(String eMail, String msg, String subject, Cliente cliente, String canal) {

		String contenidoPlano = "";
		String contenidoHTML = "";
    	int delimitador = 4;

		logCliente(cliente,canal,"mail", "Enviando mail a: "+eMail+" canal: "+canal);

		String from = TablaValores.getValor(TTFF_CONFIG, "correo", "mailFrom");
		String tipoCorreo = TablaValores.getValor(TTFF_CONFIG, "tipoCorreo", "latinia");
		boolean latinia = (new Boolean(tipoCorreo)).booleanValue();
		int resultado;

		if (!latinia) {
			try
			{
			resultado = EnvioDeCorreo.simple(null, from, eMail, subject, msg, null);
			logCliente(cliente,canal,"mail", "Resultado del envio de mail: "+resultado);
			}
			catch (Exception e)
			{
			String mensaje=
				"MailFrom  ["
					+ from
					+ "]"
					+ ", EMail   ["
					+ eMail
					+ "]"
					+ ", Subject ["
					+ subject
					+ "]: ";
				logCliente(cliente,canal,"mail", mensaje+e);
			}
		}
		else
		{
			try
			{
				String loginEnterprise = TablaValores.getValor(TTFF_CONFIG, "loginEnterprise"+canal, "banco");
				String refContract = TablaValores.getValor(TTFF_CONFIG, "contract"+canal, "refContract");

				if(msg != null && msg.length() > delimitador){
					msg = msg.substring(delimitador,msg.length());
					String[] contendioMail = StringUtil.divideConString(msg, "|T&H|");
					if(contendioMail != null && contendioMail.length >= 2){
						contenidoPlano = contendioMail[0];
						contenidoHTML = contendioMail[1];
					} else{
						contenidoPlano = msg;
						contenidoHTML = msg;
					}
				} else{
					contenidoPlano = msg;
					contenidoHTML = msg;
				}

				Map paramsMail = new HashMap();

				paramsMail.put("to", eMail);
				paramsMail.put("subject", subject);
				paramsMail.put("body", contenidoPlano);
				paramsMail.put("bodyHtml", contenidoHTML);

				String contexto = TablaValores.getValor("JNDIConfig.parametros",  "cluster", "param");


				ConectorServicioEnvioSDP instance = new ConectorServicioEnvioSDP(contexto);

				int result = instance.sendMail( loginEnterprise, refContract,     paramsMail);
				logCliente(cliente,canal,"mail", "Resultado del envio de mail por latinia: "+result);

			}catch (Exception e)
			{
				String mensaje2=
					"MailFrom  ["
				+ TablaValores.getValor(TTFF_CONFIG, "correo", "mailFrom")
				+ "]"
				+ ", EMail   ["
				+ eMail
				+ "]"
				+ ", Subject ["
				+ subject
				+ "]: ";
				logCliente(cliente,canal,"mail latinia", mensaje2+e);

			}
		}
	}
	/**
	 * M�todo que actualiza el registro de la transferencia realizada.
	 * Se trae m�todo desde {@link wcorp.bprocess.cuentas.TransferenciasFondosBean#} a la implementaci�n Stateless del mismo y
	 * se agrega a la firma del m�toso el cliente y canal.
	 *
	 * Registro de Versiones:<ul>
	 *
	 * <li>1.0 26/03/2009, Carlos Cerda I. (SEnTRA): Versi�n inicial.
     * <li>1.18 11/01/2012, Leonardo Quevedo M. (Orand S.A.): Se cambia el modificador de acceso (de private a public) a fin de
     * actualizar el registro de transferencia realizada despu�s de los posibles fallos de invocaci�n de transacci�n Tandem (B2740)
     * y el servicio monetario de Nexus (proyecto Pago TC Servipag).</li>
	 *
	 * </ul>
	 *
	 *
	 * @param cliente
	 * @param canal
	 * @param numeroOperacion
	 * @param folio
	 * @param numero
	 * @param fechaCargo
	 * @param fechaAbono
	 * @param canal
	 * @return estado
	 *
	 * @since 1.0
	 * @see {@link wcorp.bprocess.cuentas.TransferenciasFondosBean#registraFinTrf(long,String,String,Date,Date,String)}
	 */
    public boolean registraFinTrf(Cliente cliente,
			String canal,
			long numeroOperacion,
			String folio,
			String numero,
			Date fecha,
			String estado) {
		try {
			String mensaje= "";
			mensaje =
				"NumeroOperacion="
				+ numeroOperacion
				+ ", Folio="
				+ folio
				+ ", Numero="
				+ numero
				+ ", FechaCargo="
				+ (fecha != null ? fecha.toString() : "--/--/----")
				+ ", FechaAbono="
				+ (fecha != null ? fecha.toString() : "--/--/----")
				+ ", Estado="
				+ estado
				+ ", Usuario="
				+ cliente.getRut();

			logCliente(cliente,canal,"registraFinTrf", mensaje);

			sesion = joltPool.getSesion(ejbName);
			DataSet parametros = new DataSet();

			parametros.setValue("numOperacion", String.valueOf(numeroOperacion));
			parametros.setValue("estado", estado);
			parametros.setValue("usuario", String.valueOf(cliente.getRut()));

			if (folio != null && !folio.trim().equals("")){
				parametros.setValue("folio", folio.trim());
			}
			if (numero != null && !numero.trim().equals("")){
				parametros.setValue("numero", numero.trim());
			}
			if (fecha != null){
				parametros.setValue("fecCargo", new SimpleDateFormat("yyyyMMdd").format(fecha));
				parametros.setValue("fecAbono", new SimpleDateFormat("yyyyMMdd").format(fecha));
			}

			sesion.call("CtaCteModEstTra", parametros, null);
			logCliente(cliente,canal,"registraFinTrf", "CtaCteModEstTra invocado");
			return true;
		} catch (ApplicationException e) {
			String mensaje=
				"NUM_OPE="
				+ numeroOperacion
				+ ", FOLIO="
				+ folio
				+ ", NUMERO="
				+ numero
				+ ", FECCARGO="
				+ (fecha != null ? fecha.toString() : "--/--/----")
				+ ", FECABONO="
				+ (fecha != null ? fecha.toString() : "--/--/----")
				+ ", ESTADO="
				+ estado + ": ";
			logCliente(cliente,canal,"registraFinTrf", mensaje+e.getResult());
			return false;
		}catch (Exception e) {
			String mensaje=
				"NUM_OPE="
				+ numeroOperacion
				+ ", FOLIO="
				+ folio
				+ ", NUMERO="
				+ numero
				+ ", FECCARGO="
				+ (fecha != null ? fecha.toString() : "--/--/----")
				+ ", FECABONO="
				+ (fecha != null ? fecha.toString() : "--/--/----")
				+ ", ESTADO="
				+ estado + ": ";
			logCliente(cliente,canal,"registraFinTrf", mensaje+e);
			return false;
		}
	}
	/**
	 * Transfiere fondos (monto) desde una cuenta origen (cargo) a una destino (abono). Utilizado para Transferencias
	 * entre mis propias cuentas, hacia ahorros, hacia otros bancos, 3ros bci.
	 *
	 * Se trae m�todo desde {@link wcorp.bprocess.cuentas.TransferenciasFondosBean#} a la implementaci�n Stateless del mismo y
	 * se agrega a la firma del m�toso el cliente y canal.
	 *
	 * Registro de Versiones:<ul>
	 *
	 * <li>1.0 26/03/2009, Carlos Cerda I. (SEnTRA): Versi�n inicial.
         * <li>1.1 03/04/2012, Leonardo Espinoza (ORAND): Se agrega el par�metro traceNumber (identificador de transacci�n) el cual es traspasado al m�todo
         *                     {@link wcorp.model.productos.CuentaCorriente#transfiereFondos(String, double, String)} que realiza la transferencia.
         * <li>1.2 28/09/2011, Carlos Cerda I. (Orand): Se agrega el par�metro traceNumber
         *                     (identificador de transacci�n), nemonicoCargo y nemonicoAbono,
         *                     los cuales son traspasados al m�todo:
         *                     {@link wcorp.model.productos.CuentaCorriente#transfiereFondosGlosa(String, String,
         *                                                                  double, String, String)}
         * que realiza la transferencia.
	 *
	 * </ul>
	 *
	 *
	 * @param cliente
	 * @param canal
	 * @param ctaOrigen Cuenta de Cargo (Cuenta Corriente o Prima)
	 * @param traceNumber Identificador �nico de Transacci�n.
	 * @param monto Monto de la transferencia (sin comision en caso de ser otro banco)
	 * @param ctaDestino Cuenta de Abono (Corriente, Prima, Ahorro, Otro Banco (CCT))
         * @param nemonicoCargo mnem�nico de la glosa de cargo.
         * @param nemonicoAbono mnem�nico de la glosa de abono.
	 * @return wcorp.serv.cuentas.TransferenciaFondos Detalle de la transferencia (monto y cuenta destino)
	 * @throws TransferenciaException excepci�n de la aplicaci�n.
     *
	 * @since 1.0
	 * @see {@link wcorp.bprocess.cuentas.TransferenciasFondosBean#realizarTransferenciaCtaCteHaciaCtaCte(String,double,String)}
	 */
	private wcorp.serv.cuentas.TransferenciaFondos realizarTransferenciaCtaCteHaciaCtaCte(
			Cliente cliente,
			String canal,
			String ctaOrigen,
			String traceNumber,
			double monto,
			String ctaDestino,
			String nemonicoCargo,
			String nemonicoAbono) throws TransferenciaException {
		try {
		    logCliente(cliente,canal,"realizarTransferenciaCtaCteHaciaCtaCte","ctaOrigen=" + ctaOrigen
		            + ", ctaDestino=" + ctaDestino + ", monto=" + monto + ", traceNumber=" + traceNumber
		            + ", nemonicoCargo=" + nemonicoCargo + ", nemonicoAbono=" + nemonicoAbono);

			ctaOrigen = StringUtil.rellenaPorLaIzquierda(ctaOrigen, 8, '0');
			ctaDestino = StringUtil.rellenaPorLaIzquierda(ctaDestino, 8, '0');

			CuentaCorriente cuentaCorriente = new CuentaCorriente(ctaOrigen);
            return cuentaCorriente.transfiereFondosGlosa(
                    ctaDestino,
                    traceNumber,
                    monto,
                    nemonicoCargo,
                    nemonicoAbono);
        }
        catch (Exception e) {
			String mensaje=
				"CuentaOrigen="
				+ ctaOrigen
				+ ", CuentaDestino="
				+ ctaDestino
				+ ", MontoTrf="
							+ monto+": "
							+ ", traceNumber="
							+ traceNumber+" ";
			logCliente(cliente,canal,"realizarTransferenciaCtaCteHaciaCtaCte", mensaje+e);
            if(e instanceof RemoteException){
	        throw new TransferenciaException("TUX");
		    }
		    else{
			throw new TransferenciaException("TRF001");
		}
	}
    }
	/**
	 * Transfiere fondos (monto) desde una cuenta origen (cargo) a una destino (abono).
	 * Utilizado para Transferencias entre mis propias cuentas, hacia ahorros, 3ros bci.
	 *
	 * Se trae m�todo desde {@link wcorp.bprocess.cuentas.TransferenciasFondosBean#} a la implementaci�n Stateless del mismo y
	 * se agrega a la firma del m�toso el cliente y canal.
	 *
	 * Registro de versiones:<ul>
	 *
	 * <li>1.0 26/03/2009, Carlos Cerda I. (SEnTRA): Versi�n inicial.
	 *
	 * </ul>
	 *
	 *
	 * @param cliente
	 * @param canal
	 * @param ctaOrigen Cuenta de Cargo (Cuenta Corriente o Prima)
	 * @param ctaDestino Cuenta de Abono (Corriente, Prima, Ahorro, Otro Banco (CCT))
	 * @param monto Monto de la transferencia (sin comision en caso de ser otro banco)
	 * @return wcorp.serv.cuentas.TransferenciaFondos Detalle de la transferencia (monto y cuenta destino)
	 * @throws TransferenciaException
	 *
	 * @since 1.0
	 * @see {@link wcorp.bprocess.cuentas.TransferenciasFondosBean#realizarTransferenciaCtaCteHaciaAhorro(String,double,String)}
	 */
	private wcorp.serv.ahorros.TransferenciaFondos realizarTransferenciaCtaCteHaciaAhorro(
			Cliente cliente,
			String canal,
			String ctaOrigen,
			double monto,
			String ctaDestino) throws TransferenciaException {
		try {
			logCliente(cliente,canal,"realizarTransferenciaCtaCteHaciaAhorro","ctaOrigen=" + ctaOrigen + ", ctaDestino=" + ctaDestino + ", monto=" + monto);
			ctaOrigen = StringUtil.rellenaPorLaIzquierda(ctaOrigen, 8, '0');
			CuentaAhorro cuentaAhorro = new CuentaAhorro(ctaDestino);
			return cuentaAhorro.depositaDesdeCtaCte(ctaOrigen, monto);
		}catch (GeneralException ge) {
			String mensaje=
				"CuentaOrigen="
				+ ctaOrigen
				+ ", CuentaDestino="
				+ ctaDestino
				+ ", MontoTrf="
				+ monto +": ";
			logCliente(cliente,canal,"realizarTransferenciaCtaCteHaciaAhorro", mensaje+ge);
			if (ge.getCodigo().trim().equalsIgnoreCase("0029")){
				throw new TransferenciaException("TRF0027");
			}
			else{
				throw new TransferenciaException("TRF001");
			}
		} catch (Exception e) {
			String mensaje=
				"CuentaOrigen="
				+ ctaOrigen
				+ ", CuentaDestino="
				+ ctaDestino
				+ ", MontoTrf="
				+ monto+": ";
			logCliente(cliente,canal,"realizarTransferenciaCtaCteHaciaAhorro", mensaje+e);
			throw new TransferenciaException("TRF001");
		}
	}
	/**
	 * M�todo que permite obtener un Trace Number (n�mero que identificar� la transferencia).
	 *
	 * Se trae m�todo desde {@link wcorp.bprocess.cuentas.TransferenciasFondosBean#} a la implementaci�n Stateless del mismo sin modificaciones.
	 *
	 * Registro de versiones:<ul>
	 *
	 * <li>1.0 26/03/2009, Carlos Cerda I. (SEnTRA): Versi�n inicial.
	 *
	 * </ul>
	 *
	 * @return String con n�mero de Trace.
	 * @throws TuxedoException
	 * @throws GeneralException
	 *
	 * @since 1.0
	 * @see {@link wcorp.bprocess.cuentas.TransferenciasFondosBean#getTraceNumber()}
	 */
	private String getTraceNumber() throws TuxedoException, GeneralException{
		sesion = joltPool.getSesion(ejbName);
		DataSet parametros = new DataSet();
		parametros.setValue("codigo", "TFL");
		Result resultado = null;
		String traceNumber = "";
		try{
			resultado = sesion.call("GetSemillaTFL", parametros, null);
			traceNumber = (String)resultado.getValue("secuencia", 0, null);
		} catch (ApplicationException e) {
			resultado= e.getResult();
			if (resultado.getApplicationCode() == 0) {
				throw new GeneralException("TRF001");
			} else {
				throw new TuxedoException("TRF001");
			}
		}
		return traceNumber;
	}

	/**
	 * Transfiere fondos (monto) desde una cuenta origen (cargo) a una destino (abono).
	 *
	 *  La cuenta de abono corresponde a una cuenta saco, en la cual se acumulan en Tandem todas las
	 *  Transferencias en l�nea.
	 * Se trae m�todo desde {@link wcorp.bprocess.cuentas.TransferenciasFondosBean#} a la implementaci�n Stateless del mismo y
	 * se agrega a la firma del m�toso el cliente y canal.
	 *
	 * Registro de versiones:<ul>
	 *
	 * <li>1.0 26/03/2009, Carlos Cerda I. (SEnTRA): Versi�n inicial.
	 * <li>1.1 22/02/2010, Carlos Cerda I. (Orand): Se agrega par�metro tracenumber (identificador de la transacci�n) a la firma del m�todo.
	 * <li>1.2 25/03/2010, Jes�s Orellana. (Orand): Se realizan las siguientes modificaciones:
	 * 												<li> Se saca la obtenci�n de la cuenta saco desde la tabla de par�metros.
	 * 												<li> Se saca la cuenta saco como par�metro de entrada en  la llamada al tuxedo CtaCteTransfGen.
	 * 												<li> Se modifica la forma de recibir la respuesta ya que ahora no retorna la cuenta de destino como respuesta a la consulta sino la fecha contable de la transacci�n.
	 * <li>1.3 13/05/2010, Carlos Cerda I. (Orand): Se agrega nem�nico al DataSet que lleva los campos al Tandem.
	 *
	 * </ul>
	 *
	 *
	 * @param cliente
	 * @param canal
	 * @param ctaOrigen
	 * @param tracenumber
	 * @param monto
	 * @return wcorp.serv.cuentas.TransferenciaFondos Detalle de la transferencia (monto y cuenta destino).
	 * @throws TuxedoException
	 * @throws CuentasException
	 * @throws TransferenciaException.
	 *
	 * @since 1.0
	 * @see {@link wcorp.bprocess.cuentas.TransferenciasFondosBean#realizarTransferenciaCtaCteHaciaCtaCte(String,double)}
	 */
	private wcorp.serv.cuentas.TransferenciaFondos realizarTransferenciaCtaCteHaciaCtaCte(Cliente cliente,String canal,String ctaOrigen,String traceNumber,double monto) throws CuentasException, TuxedoException, TransferenciaException{

		sesion = joltPool.getSesion(ejbName);
		long ctaOrigenTandem = 0;
		try{
			ctaOrigenTandem = Long.parseLong(ctaOrigen);
		}catch(Exception e){
			throw new wcorp.serv.cuentas.CuentasException("TRF001");
		}
		DataSet parametros = new DataSet();
		parametros.setValue("ctaOrigen", StringUtil.rellenaPorLaIzquierda(String.valueOf(ctaOrigenTandem), 8, '0'));
		parametros.setValue("codigo", TablaValores.getValor(TTFF_CONFIG, "nemonicoTransferencias", "desc"));
		parametros.setValue("monto", String.valueOf((long)monto));
		parametros.setValue("identificacion", traceNumber);

		logCliente(cliente,canal,"realizarTransferenciaCtaCteHaciaCtaCte", "ctaOrigen: "+StringUtil.rellenaPorLaIzquierda(String.valueOf(ctaOrigenTandem), 8, '0'));
		logCliente(cliente,canal,"realizarTransferenciaCtaCteHaciaCtaCte", "monto: "+String.valueOf((long)monto));
		logCliente(cliente,canal,"realizarTransferenciaCtaCteHaciaCtaCte", "traceNumber: "+traceNumber);

		TransferenciaFondos data = null;
		Result resultado=null;

		try {
			logCliente(cliente,canal,"realizarTransferenciaCtaCteHaciaCtaCte","Se  hace el llamado a CtaCteTransfGen");
			resultado = sesion.call("CtaCteTransfGen", parametros, null);
			logCliente(cliente,canal,"realizarTransferenciaCtaCteHaciaCtaCte","Se llena el objeto con el resultado de CtaCteTransfGen");
			data = new TransferenciaFondos();
			data.setFechaContable(FechasUtil.convierteStringADate(String.valueOf(Long.parseLong(((String)resultado.getValue("fechaContable",0, new SimpleDateFormat("yyyyMMdd").format(new Date()))).trim())), new SimpleDateFormat("yyyyMMdd")));
			logCliente(cliente,canal,"realizarTransferenciaCtaCteHaciaCtaCte","fechaContable:" + data.getFechaContable());
		} catch (ApplicationException e) {
			logCliente(cliente,canal,"realizarTransferenciaCtaCteHaciaCtaCte","Excepcion en realizarTransferenciaCtaCteHaciaCtaCte: ApplicationCode["+e.getApplicationCode()+"] Result["+e.getResult()+"]");
			resultado = e.getResult();
			if (resultado.getApplicationCode()==0) {
				throw new wcorp.serv.cuentas.CuentasException
				((String)resultado.getValue("codigoError",0,"DESC"),
						(String)resultado.getValue("descripcionError",0,"[Sin Informacion]"));
			} else {
				throw new TuxedoException
				((String)(resultado.getApplicationCode()==1 ? resultado.getValue("codigoError",0,"DESC") : "TUX"),
						(String)resultado.getValue("descripcionError",0,"[Sin Informacion]"));
			}
		}
		return data;
	}
	/**
	 * M�todo que permite la comunicaci�n con el Adapter CCA-Redbanc para las Transferencias en L�nea
	 * a otros bancos.
	 *
     * Se trae m�todo desde {@link wcorp.bprocess.cuentas.TransferenciasFondosBean#} a la implementaci�n Stateless
     * del mismo y se agrega a la firma del m�toso el cliente y canal.
	 *
	 *
	 * Registro de versiones:<ul>
	 *
	 * <li>1.0 26/03/2009, Carlos Cerda I. (SEnTRA): Versi�n inicial.
	 *
     *
     * <li>1.1 15/12/2011, Carlos Cerda I. (Orand): Se agrega par�metro 'tipoOperacion' a la firma del m�todo y
     *                                     se agrega a la mesajer�a que viaja hacia el operador
     *                                     (solo en caso que sea distinto de null).
     * <li>1.2 07/02/2012, Carlos Cerda I. (Orand): Se lleva la l�gica de creaci�n del archivo xfml al m�todo:
     * {@link #construyeXFML(Cliente, String, String, String, String, double, String, Date, String, String, long,
     * char, String, String, long, char, String, String, String, String, String)}.
     * <li>1.3 30/11/2016 Andr�s Pardo T. (SEnTRA) - Marcelo Avenda�o. (Ing. Soft. BCI): Se sobrecarga metodo moviendo todo el codigo al metodo creado.
     *
	 * </ul>
	 *
	 *
     * @param cliente objeto con datos b�sicos del cliente.
     * @param canal canal por donde ingres� el cliente.
     * @param macOrigen message authentication code, c�digo con el que se autentica el mensaje.
     * @param xfmlRequest mensaje.
	 * @return CCADirectaVO, con informaci�n enviada por el operador.
     * @throws Exception excepci�n de la aplicaci�n.
	 *
	 * @since 1.0
     * @see {@link wcorp.bprocess.cuentas.TransferenciasFondosBean#enviarTrfCCA(String,String,double,String,Date,
     * String,long,char,String,String,long,char,String,String,String)}
	 */

    private CCADirectaVO enviarTrfOperador(Cliente cliente, String canal,String macOrigen, String xfmlRequest)
            throws Exception{
        return enviarTrfOperador(cliente, canal, macOrigen, xfmlRequest, SERV_TUX_ABONO_CCA);
    }
    
    /**
     * Metodo sobrecargado que incluye nuevo parametro que indica el servicio tuxedo a ejecutar.
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 30/11/2016 Andr�s Pardo T. (SEnTRA) - Marcelo Avenda�o. (Ing. Soft. BCI): versi�n inicial.
     * </ul>
     * <p>
     * 
     * @param cliente objeto con datos b�sicos del cliente.
     * @param canal canal por donde ingres� el cliente.
     * @param macOrigen message authentication code, c�digo con el que se autentica el mensaje.
     * @param xfmlRequest mensaje.
     * @param servTuxedo identificador de servicio tuxedo.
     * @return CCADirectaVO, con informaci�n enviada por el operador.
     * @throws Exception excepci�n de la aplicaci�n.
     * @since 4.9
     */
    private CCADirectaVO enviarTrfOperador(Cliente cliente, String canal,String macOrigen, String xfmlRequest, String servTuxedo)
            throws Exception{


	sesion = joltPool.getSesion(ejbName);
	Result resultado = null;
	DataSet parametros = new DataSet();
	parametros.setValue("in_buffer", xfmlRequest);

	XFMLBuffer bufferResponse = null;

	try {
            logCliente(cliente,canal,"enviarTrfOperador", "Invocaremos Jolt " + servTuxedo);
            resultado = sesion.call(servTuxedo, parametros, null);
            logCliente(cliente,canal,"enviarTrfOperador", "Obtenemos los datos del Jolt");
			String xfmlResponse = (String)resultado.getValue("out_buffer",0, null);
            logCliente(cliente,canal,"enviarTrfOperador", xfmlResponse);
			bufferResponse = new XFMLBuffer(xfmlResponse);
            logCliente(cliente,canal,"enviarTrfOperador", "Construimos XFMLBuffer");

        }
        catch (ApplicationException e) {
        	resultado= e.getResult();
                logCliente(cliente,canal,"enviarTrfOperador", "ApplicationException: ApplicationCode["
                        +e.getApplicationCode()+"] " + "Result["+e.getResult()+"]");
        	throw e;
            }

	CCADirectaVO resultadoCCA = new CCADirectaVO();

	Dato tipoMensaje = bufferResponse.getDato("tipoMensaje");
	resultadoCCA.setTipoMensaje(tipoMensaje.getValor());
        logCliente(cliente,canal,"enviarTrfOperador", "TipoMensaje: "+resultadoCCA.getTipoMensaje());
        Dato timestampCCA = bufferResponse.getDato("timestampCCA");
        resultadoCCA.setTimesTampCCA(timestampCCA.getValor());
        logCliente(cliente,canal,"enviarTrfOperador", "TimesTampCCA: "+resultadoCCA.getTimesTampCCA());
        Dato traceNumber = bufferResponse.getDato("traceNumber");
        resultadoCCA.setTraceNumber(traceNumber.getValor());
        logCliente(cliente,canal,"enviarTrfOperador", "TraceNumber: "+resultadoCCA.getTraceNumber());
        Dato codigoAutorizacion = bufferResponse.getDato("codigoAutorizacion");
        if(codigoAutorizacion == null){
        	resultadoCCA.setCodigoAutorizacion("");
        } else{
        resultadoCCA.setCodigoAutorizacion(codigoAutorizacion.getValor());
        }
        logCliente(cliente,canal,"enviarTrfOperador", "CodigoAutorizacion: "
                + resultadoCCA.getCodigoAutorizacion());
        Dato tipoRespuesta = bufferResponse.getDato("tipoRespuesta");
        if(tipoRespuesta == null){
        	resultadoCCA.setTipoRespuesta("");
        } else{
        resultadoCCA.setTipoRespuesta(tipoRespuesta.getValor());
        }
        logCliente(cliente,canal,"enviarTrfOperador", "TipoRespuesta: "+resultadoCCA.getTipoRespuesta());
        Dato operador = bufferResponse.getDato("operador");
        resultadoCCA.setOperador(operador.getValor());
        logCliente(cliente,canal,"enviarTrfOperador", "operador: "+resultadoCCA.getOperador());
		if (!macOrigen.trim().equals("") && !tipoMensaje.getValor().equals("9200")){
		    Dato macOperador = bufferResponse.getDato("macCCA");
		    resultadoCCA.setMacOperador(macOperador.getValor());
            logCliente(cliente,canal,"enviarTrfOperador", "MacOperador: "+ resultadoCCA.getMacOperador());
			Dato macReceptor = bufferResponse.getDato("macReceptor");
			if(macReceptor == null){
				resultadoCCA.setMacReceptor(TablaValores.getValor(TTFF_CONFIG, "MAC", "clave"));
                logCliente(cliente,canal,"enviarTrfOperador", "macReceptor: "+ resultadoCCA.getMacReceptor());
            }
            else{
			    resultadoCCA.setMacReceptor(macReceptor.getValor());
                    logCliente(cliente,canal,"enviarTrfOperador", "macReceptor: "+ resultadoCCA.getMacReceptor());
		    }
		}

		return resultadoCCA;
	}
	/**
	 * M�todo que permite la comunicaci�n con el Adapter CCA-Redbanc para las Transferencias en L�nea
	 * a otros bancos.
	 *
	 * Se trae m�todo desde {@link wcorp.bprocess.cuentas.TransferenciasFondosBean#} a la implementaci�n Stateless del mismo y
	 * se agrega a la firma del m�toso el cliente y canal.
	 *
	 *
	 * Registro de versiones:<ul>
	 *
	 * <li>1.0 26/03/2009, Carlos Cerda I. (SEnTRA): Versi�n inicial.
	 *
	 * </ul>
	 *
	 *
	 * @param cliente
	 * @param canal
	 * @param tipoCuentaOrigen: Tipo de cuenta de origen (CCT, CPR)
	 * @param tipoCuentaDestino: Tipo de cuenta de destino (CCT, CPR)
	 * @param monto: monto de la transferencia
	 * @param traNumber: identificador �nico de la transferencia
	 * @param fechaCargo: fecha de cargo de la transferencia
	 * @param ctaOrigen: cuenta del cliente
	 * @param rut: rut del cliente
	 * @param dv: digito verificador del cliente
	 * @param codBancoDestino: c�digo del banco de destino (001, etc)
	 * @param ctaDestino: cuenta de abono
	 * @param rutDestino: rut de abono
	 * @param digDestino: digito verificador de abono
	 * @param nombreDestino: nombre de la persona de destino del abono
	 * @param comentario: comentario para el env�o de Email
	 * @param numeroOperacion: n�mero que devuelve el sp que inseta en la BD tra
	 * @return CCADirectaVO, con informaci�n enviada por el operador.
	 * @throws TuxedoException
	 * @throws GeneralException
	 *
	 * @since 1.0
	 * @see {@link wcorp.bprocess.cuentas.TransferenciasFondosBean#enviarTrfCCA(String,String,double,String,Date,String,long,char,String,String,long,char,String,String,String)}
	 */

	private void rechazaMacOperador(
			Cliente cliente,
			String canal,
			String tipoCuentaOrigen,
			String tipoCuentaDestino,
			double monto,
			String timesTamp,
			String traceNumber,
			String hora,
			Date fecha,
			String tipoRespuesta,
			String cuentaOrigen,
			long rut,
			char dv,
			String codBancoDestino,
			String cuentaDestino,
			long rutDestino,
			char digDestino,
			String nombreDestino,
			String macReceptor) throws Exception{

		logCliente(cliente,canal,"rechazaMacOperador", "Modo XFMLBuffer");
		try {

			XFMLBuffer xfmlBuffer = new XFMLBuffer();
			xfmlBuffer.setSincrono(false);
			String tipCuentaOrigen = TablaValores.getValor(TTFF_CONFIG, "idTiposCuentaLinea", tipoCuentaOrigen);
			logCliente(cliente,canal,"rechazaMacOperador", "tipoCuentaOrigen: "+tipCuentaOrigen);
			String tipCuentaDestino = TablaValores.getValor(TTFF_CONFIG, "idTiposCuentaLinea", tipoCuentaDestino);
			logCliente(cliente,canal,"rechazaMacOperador", "tipoCuentaDestino: "+tipCuentaDestino);

			xfmlBuffer.addDato(new Dato("productoOrigen", tipCuentaOrigen));
			xfmlBuffer.addDato(new Dato("productoDestino", tipCuentaDestino));

			long montoTrans = (long)monto;

			xfmlBuffer.addDato(new Dato("monto", (StringUtil.rellenaPorLaDerecha((StringUtil.rellenaPorLaIzquierda(String.valueOf(montoTrans), 10, '0')), 12, '0'))));
			logCliente(cliente,canal,"rechazaMacOperador", "montoTrans: "+montoTrans);
			xfmlBuffer.addDato(new Dato("timestampCCA", timesTamp));
			logCliente(cliente,canal,"rechazaMacOperador", "timesTamp: "+timesTamp);
			xfmlBuffer.addDato(new Dato("traceNumber", StringUtil.rellenaPorLaIzquierda(traceNumber, 12, '0')));
			logCliente(cliente,canal,"rechazaMacOperador", "traceNumber: "+traceNumber);
			xfmlBuffer.addDato(new Dato("hora", hora));
			logCliente(cliente,canal,"rechazaMacOperador", "hora: "+hora);
			xfmlBuffer.addDato(new Dato("fecha", (new SimpleDateFormat("yyMMdd").format(fecha))));
			logCliente(cliente,canal,"rechazaMacOperador", "fecha: "+(new SimpleDateFormat("yyMMdd").format(fecha)));

			String codAutorizacion = TablaValores.getValor(TTFF_CONFIG, "codAutorizacion", "valor");
			if (codAutorizacion == null){
				codAutorizacion="000000";
			}
			xfmlBuffer.addDato(new Dato("codigoAutorizacion", codAutorizacion));
			logCliente(cliente,canal,"rechazaMacOperador", "codigoAutorizacion: "+codAutorizacion);

			xfmlBuffer.addDato(new Dato("tipoRespuesta", tipoRespuesta));
			logCliente(cliente,canal,"rechazaMacOperador", "tipoRespuesta: "+tipoRespuesta);


			xfmlBuffer.addDato(new Dato("moneda", (TablaValores.getValor(TTFF_CONFIG, "tipoMoneda", "Peso"))));
			logCliente(cliente,canal,"rechazaMacOperador", "moneda: "+(TablaValores.getValor(TTFF_CONFIG, "tipoMoneda", "Peso")));
			xfmlBuffer.addDato(new Dato("bancoOrigen", (StringUtil.rellenaPorLaIzquierda(codigoBancoBCI, 4, '0'))));
			logCliente(cliente,canal,"rechazaMacOperador", "bancoOrigen: "+(StringUtil.rellenaPorLaIzquierda(codigoBancoBCI, 4, '0')));
			xfmlBuffer.addDato(new Dato("cuentaOrigen", (StringUtil.rellenaPorLaIzquierda(cuentaOrigen, 20, '0'))));
			logCliente(cliente,canal,"rechazaMacOperador", "cuentaOrigen: "+(StringUtil.rellenaPorLaIzquierda(cuentaOrigen, 20, '0')));

			String rutGir = StringUtil.rellenaPorLaIzquierda((String.valueOf(rut)+dv), 12, '0');
			xfmlBuffer.addDato(new Dato("rutGirador", rutGir));
			logCliente(cliente,canal,"enviarTrfCCA", "rutGir: "+rutGir);

			String nombreCliente = cliente.getFullName().length() > 100 ? cliente.getFullName().substring(0, 100) : cliente.getFullName();
			xfmlBuffer.addDato(new Dato("nombreGirador", nombreCliente));
			logCliente(cliente,canal,"rechazaMacOperador", "nombreCliente: "+nombreCliente);
			xfmlBuffer.addDato(new Dato("bancoDestino", (StringUtil.rellenaPorLaIzquierda(codBancoDestino, 4, '0'))));
			logCliente(cliente,canal,"rechazaMacOperador", "bancoDestino: "+(StringUtil.rellenaPorLaIzquierda(codBancoDestino, 4, '0')));
			xfmlBuffer.addDato(new Dato("cuentaDestino", (StringUtil.rellenaPorLaIzquierda(cuentaDestino, 20, '0'))));
			logCliente(cliente,canal,"rechazaMacOperador", "cuentaDestino: "+(StringUtil.rellenaPorLaIzquierda(cuentaDestino, 20, '0')));

			String rutDest = StringUtil.rellenaPorLaIzquierda((String.valueOf(rutDestino)+digDestino), 12, '0');
			xfmlBuffer.addDato(new Dato("rutDestinatario", rutDest));
			logCliente(cliente,canal,"rechazaMacOperador", "rutDest: "+rutDest);
			if (nombreDestino != null && !nombreDestino.trim().equals("")){
				String nomDestino = nombreDestino.length() > 100 ? nombreDestino.substring(0, 100) : nombreDestino;
				xfmlBuffer.addDato(new Dato("nombreDestinatario", nomDestino));
				logCliente(cliente,canal,"rechazaMacOperador", "nomDestino: "+nomDestino);
			}else{
				xfmlBuffer.addDato(new Dato("nombreDestinatario", " "));
				logCliente(cliente,canal,"rechazaMacOperador", "nomDestino: ");
			}

			if (!macReceptor.trim().equals("")){
				xfmlBuffer.addDato(new Dato("macReceptor", macReceptor.trim()));
			String largoMac = TablaValores.getValor(TTFF_CONFIG, "MAC", "largo");
			xfmlBuffer.addDato(new Dato("largoMacReceptor", largoMac));
			xfmlBuffer.addDato(new Dato("largoMacCCA", largoMac));
			}else{
				xfmlBuffer.addDato(new Dato("largoMacReceptor", "0"));
				xfmlBuffer.addDato(new Dato("largoMacCCA", "0"));
			}

			String xfmlRequest = xfmlBuffer.getXFMLString();
			logCliente(cliente,canal,"rechazaMacOperador", xfmlRequest);

			sesion = joltPool.getSesion(ejbName);
			DataSet parametros = new DataSet();
			parametros.setValue("in_buffer", xfmlRequest);


			logCliente(cliente,canal,"rechazaMacOperador", "Invocaremos Jolt cca9210");
			sesion.call("cca9210", parametros, null);
			logCliente(cliente,canal,"rechazaMacOperador", "Retornamos de Invocaci�n Jolt cca9210");
			return;
		} catch (Exception e) {
			if(e instanceof ApplicationException){
				ApplicationException ae = (ApplicationException)e;
				logCliente(cliente,canal,"rechazaMacOperador", "ApplicationException: ApplicationCode["+ae.getApplicationCode()+"] Result["+ae.getResult()+"]");
			}else{
				logCliente(cliente,canal,"rechazaMacOperador", "Exception: "+e);
			}
			throw e;
		}

	}
	/**
	 * M�todo que realiza la reversa de la transferencia en L�nea.
	 * Esta reversa se realiza sobre la cuenta origen (cargo).
	 *
	 *  La cuenta de abono corresponde a una cuenta saco, en la cual se acumulan en Tandem todas las
	 *  Transferencias en l�nea.
	 * Se trae m�todo desde {@link wcorp.bprocess.cuentas.TransferenciasFondosBean#} a la implementaci�n Stateless del mismo y
	 * se agrega a la firma del m�toso el cliente y canal.
	 *
	 * Registro de versiones:<ul>
	 *
	 * <li>1.0 26/03/2009, Carlos Cerda I. (SEnTRA): Versi�n inicial.
	 * <li>1.1 22/02/2010, Carlos Cerda I. (Orand): Se agrega par�metro traceNumber (identificador de la transacci�n) a la firma del m�todo.
	 * <li>1.1 22/02/2010, Jes�s Orellana. (Orand): Se realizan las siguientes modificaciones:
	 * 												<li> Se saca la obtenci�n de la cuenta saco desde la tabla de par�metros.
	 * 												<li> Se saca la cuenta saco como par�metro de entrada en  la llamada al tuxedo CtaCteRevTrnCar.
	 *												<li> Se modifica la forma de recibir la respuesta ya que ahora no retorna la cuenta de destino como respuesta a la consulta sino la fecha contable de la transacci�n.
	 * <li>1.3 13/05/2010, Carlos Cerda I. (Orand): Se agrega nem�nico al DataSet que lleva los campos al Tandem.
	 * </ul>
	 *
	 * @param cliente
	 * @param canal
	 * @param ctaOrigen
	 * @param traceNumber
	 * @param monto
	 * @return wcorp.serv.cuentas.TransferenciaFondos Detalle de la transferencia (monto y cuenta destino).
	 * @throws TuxedoException
	 * @throws CuentasException
	 * @throws TransferenciaException.
	 *
	 * @since 1.0
	 * @see {@link wcorp.bprocess.cuentas.TransferenciasFondosBean#realizaReversaCargo(String,double)}
	 */
	private wcorp.serv.cuentas.TransferenciaFondos realizaReversaCargo(Cliente cliente, String canal,String ctaOrigen,String traceNumber,double monto) throws CuentasException, TuxedoException, TransferenciaException{

		sesion = joltPool.getSesion(ejbName);
		long ctaOrigenTandem = 0;
		try{
			ctaOrigenTandem = Long.parseLong(ctaOrigen);
		}catch(Exception e){
			throw new CuentasException("TRF001");
		}
		DataSet parametros = new DataSet();
		parametros.setValue("ctaOrigen", StringUtil.rellenaPorLaIzquierda(String.valueOf(ctaOrigenTandem), 8, '0'));
		parametros.setValue("codigo", TablaValores.getValor(TTFF_CONFIG, "nemonicoTransferencias", "desc"));
		parametros.setValue("monto", String.valueOf((long)monto));
		parametros.setValue("identificacion", traceNumber);

		logCliente(cliente,canal,"realizaReversaCargo", "ctaOrigen: "+StringUtil.rellenaPorLaIzquierda(String.valueOf(ctaOrigenTandem), 8, '0'));
		logCliente(cliente,canal,"realizaReversaCargo", "monto: "+String.valueOf((long)monto));
		logCliente(cliente,canal,"realizaReversaCargo", "traceNumber: "+traceNumber);

		TransferenciaFondos data = null;
		Result resultado=null;

		try {
			logCliente(cliente,canal,"realizaReversaCargo","Se  hace el llamado a CtaCteRevTrnCar");
			resultado = sesion.call("CtaCteRevTrnCar", parametros, null);
			logCliente(cliente,canal,"realizaReversaCargo","Se llena el objeto con el resultado de CtaCteRevTrnCar");
			data = new TransferenciaFondos();
			data.setFechaContable(FechasUtil.convierteStringADate(String.valueOf(Long.parseLong(((String)resultado.getValue("fechaContable",0, new SimpleDateFormat("yyyyMMdd").format(new Date()))).trim())), new SimpleDateFormat("yyyyMMdd")));
			logCliente(cliente,canal,"realizaReversaCargo","fechaContable:" + data.getFechaContable());
		} catch (ApplicationException e) {
			logCliente(cliente,canal,"realizaReversaCargo","Excepcion en realizarTransferenciaCtaCteHaciaCtaCte: ApplicationCode["+e.getApplicationCode()+"] Result["+e.getResult()+"]");
			// Errores Aplicativos
			resultado = e.getResult();
			if (resultado.getApplicationCode()==0) { //Error de Negocio Cuentas
				throw new CuentasException
				((String)resultado.getValue("codigoError",0,"DESC"),
						(String)resultado.getValue("descripcionError",0,"[Sin Informacion]"));
			} else { // Error Tuxedo
				throw new TuxedoException
				((String)(resultado.getApplicationCode()==1 ? resultado.getValue("codigoError",0,"DESC") : "TUX"),
						(String)resultado.getValue("descripcionError",0,"[Sin Informacion]"));
			}
		}
		return data;
	}


	/**
	 * <p>M�todo que actualiza la transferencia en l�nea</p>
	 *
	 * Metodo que actualiza una Transferencia en L�nea en la Tabla tra, BD ctaster, con los parametros
	 * devueltos por la CCA (Centro de Compensaci�n Automatizada), que es el ente responsable de la comunicaci�n
	 * entre bancos y que hace posible las transferencias en l�nea.
	 *
	 * Se trae m�todo desde {@link wcorp.bprocess.cuentas.TransferenciasFondosBean#} a la implementaci�n Stateless del mismo y
	 * se agrega a la firma del m�toso el cliente y canal.
	 *
	 * Registro de Versiones:<ul>
	 *
	 * <li>1.0 26/03/2009, Carlos Cerda I. (SEnTRA): Versi�n inicial.
	 *
	 * </ul>
	 *
	 * @param cliente
	 * @param canal
	 * @param numOperacion
	 * @param estado
	 * @param fechaMov
	 * @param codRespuesta
	 * @param operador
	 * @return boolean
	 * @throws TuxedoException
	 *
	 * @since 1.0
	 * @see {@link wcorp.bprocess.cuentas.TransferenciasFondosBean#actualizaTransferenciaLinea(String,String,String,String,String)}
	 */
	private boolean actualizaTransferenciaLinea(Cliente cliente, String canal, String numOperacion, String estado, String fechaMov, String codRespuesta, String operador) throws TuxedoException {

		boolean actualizo = true;
		sesion = joltPool.getSesion(ejbName);
		DataSet parametros = new DataSet();

		String fecha = fechaMov.substring(0, 6);
		String horaSinFor = fechaMov.substring(6, fechaMov.length());
		String hora = horaSinFor.substring(0, 2)+":"+horaSinFor.substring(2, 4)+":"+horaSinFor.substring(4, 6);
		String fechaFinal = fecha+" "+hora;

		logCliente(cliente,canal,"actualizaTransferenciaLinea", "numOperacion: "+numOperacion);
		logCliente(cliente,canal,"actualizaTransferenciaLinea", "estado: "+estado);
		logCliente(cliente,canal,"actualizaTransferenciaLinea", "fechaMov: "+"["+fechaFinal+"]");
		logCliente(cliente,canal,"actualizaTransferenciaLinea", "codRespuesta: "+codRespuesta);
		logCliente(cliente,canal,"actualizaTransferenciaLinea", "operador: "+operador);

		parametros.setValue("numOperacion", numOperacion);
		parametros.setValue("estado", estado);
		parametros.setValue("fechaMov", fechaFinal);
		parametros.setValue("codRespuesta", codRespuesta);
		parametros.setValue("operador", operador);

		try {
			logCliente(cliente,canal,"actualizaTransferenciaLinea", "Llamamos al servicio ActTraRespCca");
			sesion.call("ActTraRespCca", parametros, null);
			logCliente(cliente,canal,"actualizaTransferenciaLinea", "volvimos de ActTraRespCca");
		} catch (Exception e) {
			logCliente(cliente,canal,"actualizaTransferenciaLinea", "ERROR ActTraRespCca (" + e.toString() + ")");
			actualizo = false;
		}
		return actualizo;
	}

	 /**
	 * <P>M�todo encargado de consultar las transferencias de fondos recibidas por parte del banco BCI
	 * y en un intervalo de tiempo dado permitiendo realizar consultas por bloque, es decir
	 * el par�metro de entrada "cantidad de registros" indica cuantos registros se desean recibir del total y
	 * el par�metro bloque en que "p�gina de resultado" se est� con respecto al total.</P>
	 *
	 * Registro de versiones:<UL>
	 *
	 * <LI>1.0 06/12/2008, Alejandro Barra F. (SEnTRA): versi�n inicial.</LI>
	 * <UL>
	 * @param rut Rut del cliente.
	 * @param dv Digito Verificador del cliente.
	 * @param periodo mes que se desea consultar.
	 * @param cuenta cuenta del cliente que se desea consultar.
	 * @param bloque bloque que se esta consultando.
	 * @param cantidadRegistros cantidad de registros a consultar.
	 * @return transferencias enviadas al cliente
	 * @throws Exception
	 * @since 7.12
	 */
    public TransferenciasRecibidasVO[] consultaTransferenciasRecibidasBancoBCI(long rut, char dv, Date periodo, String cuenta, int bloque, int cantidadRegistros) throws Exception {
    	logger.debug("[consultaTransferenciasRecibidasBancoBCI] :: inicio del metodo");
        TransferenciasRecibidasVO[] transferenciasRecibidasVO = null;
        try{
            TransferenciasFondosDAO transferenciasFondosDAO = new TransferenciasFondosDAO();
            if(logger.isDebugEnabled()){logger.debug("[consultaTransferenciasRecibidasBancoBCI] :: transferenciasFondosDAO " + transferenciasFondosDAO);}
            transferenciasRecibidasVO = transferenciasFondosDAO.consultaTransferenciasRecibidasBancoBCI(rut, dv, periodo, cuenta, bloque, cantidadRegistros);
            if(logger.isDebugEnabled()){logger.debug("[consultaTransferenciasRecibidasBancoBCI] :: transferenciasRecibidasVO " + transferenciasRecibidasVO);}
        }catch(Exception e){
        	if(logger.isEnabledFor(Level.ERROR)){logger.error("[consultaTransferenciasRecibidasCliente]:: Problemas al realizar la consulta, Exception["+e+"]");}
            throw e;
        }
        return transferenciasRecibidasVO;
    }

    /**
     * <P>M�todo encargado de consultar las transferencias de fondos recibidas por parte del banco BCI
     * y en un intervalo de tiempo definido por el flagPeriodo permitiendo realizar consultas por bloque, es decir
     * el par�metro de entrada "cantidad de registros" indica cuantos registros se desean recibir del total y
     * el par�metro bloque en que "p�gina de resultado" se est� con respecto al total.</P>
     *
     * Registro de versiones:<UL>
     *
     * <LI>1.0 02/02/2012, Pablo Romero C�ceres (SEnTRA): versi�n inicial.</LI>
     * </UL>
     * @param rut Rut del cliente.
     * @param dv Digito Verificador del cliente.
     * @param periodo mes que se desea consultar.
     * @param cuenta cuenta del cliente que se desea consultar.
     * @param bloque bloque que se esta consultando.
     * @param cantidadRegistros cantidad de registros a consultar.
     * @param flagPeriodo Periodo en que se har� la consulta, <b>D</b> para un d�a, <b>S</b> para una semana,
     *                      <b>M</b> para un mes.
     * @return transferencias enviadas al cliente
     * @throws Exception Errores.
     * @since 2.5
     */
    public TransferenciasRecibidasVO[] consultaTransferenciasRecibidasBancoBCIPorPeriodo(long rut, char dv,
            Date periodo, String cuenta, int bloque, int cantidadRegistros, char flagPeriodo) throws Exception {
        logger.debug("[consultaTransferenciasRecibidasBancoBCIPorPeriodo] :: inicio del metodo");
        TransferenciasRecibidasVO[] transferenciasRecibidasVO = null;
        try{
            TransferenciasFondosDAO transferenciasFondosDAO = new TransferenciasFondosDAO();
            transferenciasRecibidasVO = transferenciasFondosDAO.consultaTransferenciasRecibidasBancoBCIPorPeriodo(
                    rut, dv, periodo, cuenta, bloque, cantidadRegistros, flagPeriodo);
            if(logger.isDebugEnabled()){
                logger.debug("[consultaTransferenciasRecibidasBancoBCIPorPeriodo] :: transferenciasRecibidasVO "
                        + transferenciasRecibidasVO);
            }
        }catch(Exception e){
            if(logger.isEnabledFor(Level.ERROR)){
                logger.error("[consultaTransferenciasRecibidasBancoBCIPorPeriodo] Exception", e);
            }
            throw e;
        }
        return transferenciasRecibidasVO;
    }



	 /**
	 * <P>M�todo encargado de consultar las transferencias de fondos recibidas por el cliente para una cuenta
	 * de destino en particular y en un intervalo de tiempo dado permitiendo realizar consultas por bloque, es
	 * decir el par�metro de entrada "cantidad de registros" indica cuantos registros se desean recibir del total
	 * y el par�metro bloque en que "p�gina de resultado" se est� con respecto al total.</P>
	 *
	 * Registro de versiones:<UL>
	 *
	 * <LI>1.0 11/03/2009, Claudia N��ez. (SEnTRA): versi�n inicial.M�todo migrado desde TransferenciasFondosBean.</LI>
	 * <UL>
	 * @param rut Rut del cliente.
	 * @param dv Digito Verificador del cliente.
	 * @param periodo mes que se desea consultar.
	 * @param cuenta cuenta del cliente que se desea consultar.
	 * @param bloque bloque que se esta consultando.
	 * @param cantidadRegistros cantidad de registros a consultar.
	 * @return transferencias enviadas al cliente
	 * @throws Exception
	 * @since 8.0
	 */

	public TransferenciasRecibidasVO[] consultaTransferenciasRecibidasCliente(long rut, char dv, Date periodo, String cuenta, int bloque, int cantidadRegistros) throws Exception {
		TransferenciasRecibidasVO[] transferenciasRecibidasVO = null;
		try{
			TransferenciasFondosDAO transferenciasFondosDAO = new TransferenciasFondosDAO();
			transferenciasRecibidasVO = transferenciasFondosDAO.consultaTransferenciasRecibidasCliente(rut, dv, periodo, cuenta, bloque, cantidadRegistros);
		}catch(Exception e){
			logger.debug("[consultaTransferenciasRecibidasCliente]:: Problemas al realizar la consulta, Exception["+e+"]");
			throw e;
		}
		return transferenciasRecibidasVO;
	}

	/**
	 * <P>M�todo encargado de consultar las transferencias de fondos realizadas por el cliente para una cuenta
	 * de origen en particular y en un intervalo de tiempo dado permitiendo realizar consultas por bloque, es decir
	 * el par�metro de entrada "cantidad de registros" indica cuantos registros se desean recibir del total y
	 * el par�metro bloque en que "p�gina de resultado" se est� con respecto al total.</P>
	 * Registro de versiones:<UL>
	 *
	 * <LI>1.0 11/03/2009, Claudia N��ez. (SEnTRA): versi�n inicial. M�todo migrado desde TransferenciasFondosBean.</LI>
	 * <UL>
	 * @param rut Rut del cliente.
	 * @param dv Digito Verificador del cliente.
	 * @param periodo mes que se desea consultar.
	 * @param bloque bloque que se esta consultando.
	 * @param cuenta cuenta del cliente que se desea consultar.
	 * @param cantidadRegistros cantidad de registros a consultar.
	 * @return transferencias realizadas por el cliente
	 * @throws Exception
	 * @since 8.0
	 */
	public TransferenciasRecibidasVO[] consultaTransferenciasEnviadasCliente(long rut, char dv, Date periodo, String cuenta, int bloque, int cantidadRegistros)
	throws RemoteException, GeneralException, Exception{
		TransferenciasRecibidasVO[] transferenciasRecibidasVO = null;
		try{
			TransferenciasFondosDAO transferenciasFondosDAO = new TransferenciasFondosDAO();
			transferenciasRecibidasVO = transferenciasFondosDAO.consultaTransferenciasEnviadasCliente(rut, dv, periodo, cuenta, bloque, cantidadRegistros);
		}catch(Exception e){
			logger.debug("[consultaTransferenciasEnviadasCliente]:: Problemas al realizar la consulta, Exception["+e+"]");
			throw e;

		}

		return transferenciasRecibidasVO;

	}

	/**
     * <p>
     * M�todo que obtiene desde servicio DAO
     * wcorp.bprocess.cuentas.dao.ConsultaTransferenciasRecibidasDAO
     * las transferencias en l�nea recibidas.  Este m�todo es invocado cuando
     * el usuario selecciona una cuenta corriente o todas para consultar las
     * transferencias recibidas.
     * </p>
     *
     * <p>Registro de versiones:</p>
     * <ul>
     *   <LI>1.0 27/03/2009, Graciela Estay. (BCI): versi�n inicial. M�todo migrado desde TransferenciasFondosBean.</LI>
     * </ul>
     * @param filtro
     *              objeto de tipo FiltroConsultaTransferenciasRecibidasVO que
     *              contiene el n�mero de cuenta corriente, fecha inicial y
     *              fecha de t�rmino para consultar las transferencias recibidas
     *              en l�nea en un rango de fechas.
     * @throws BCIExpressException
     * @throws RuntimeException
     * @throws RemoteException
     * @return ConsultaTransferenciasRecibidasVO
     *         arreglo contenedor de la informaci�n de las transferencias en
     *         l�nea recibidas.
     * @since 7.2
     */
    public TransferenciasRecibidasVO[] consultaTransferenciasEnLineaPorCuentaCorrienteRecibidas(
            FiltroConsultaTransferenciasRecibidasVO filtro) throws BCIExpressException, RuntimeException, RemoteException {
        ConsultaTransferenciasRecibidasDAO transferenciaDAO = new ConsultaTransferenciasRecibidasDAO(ejbName);
        TransferenciasRecibidasVO[] transferenciasLinea = transferenciaDAO.consultaTransferenciasEnLineaPorCuentaCorrienteRecibidas(filtro);

        logger.debug("[consultaTransferenciasEnLineaPorCuentaCorrienteRecibidas]: Contenido Arreglo \n"+ StringUtil.contenidoDe(transferenciasLinea) + "\n");

        return transferenciasLinea;
    }

		/**
	 * M�todo que recibe el tipo de proceso que realizara la aplicaci�n de Transferencias Programadas en L�nea,
	 * desde el Web Service: ProgramacionLinea.
	 *
	 * Se trae m�todo desde wcorp.bprocess.cuentas.TransferenciasFondosBean a la implementaci�n Stateless del mismo y
	 * se cambia nivel de logeo de debug a info o error seg�n corresponda.
	 *
	 * Registro de versiones:<ul>
	 *
	 * <li>1.0 26/03/2009, Carlos Cerda I. (SEnTRA):
	 * </ul>
	 *
	 * @param tipoProceso
	 * @return String
	 * @throws Exception
	 *
	 * @since 1.0
	 * @see {@link wcorp.bprocess.cuentas.TransferenciasFondosBean#getTransferenciasProgramadas(String)}
	 */

	public String getTransferenciasProgramadas(String tipoProceso) throws Exception {

		logger.debug("[getTransferenciasProgramadas] En getTransferenciasProgramadas");
		if(logger.isDebugEnabled()){logger.debug("[getTransferenciasProgramadas] tipoProceso: "+tipoProceso);}
		String continuo = "Si";

		if((tipoProceso != null) && (!tipoProceso.trim().equals("")) && (tipoProceso.trim().equals("proceso") || tipoProceso.trim().equals("reProceso"))){
			try {
				continuo = procesoTransferenciasProgramadas(tipoProceso);
			} catch (Exception e) {
				if(logger.isEnabledFor(Level.ERROR)){logger.error("[getTransferenciasProgramadas] Exception: "+e.toString());}
			}
		}else{
			if(logger.isEnabledFor(Level.ERROR)){logger.error("[getTransferenciasProgramadas] Exception: tipoProceso indefinido["+tipoProceso+"]");}
			throw new Exception("tipoProceso indefinido");
		}
		if (logger.isInfoEnabled()){logger.info("[getTransferenciasProgramadas] continuo: "+continuo);}
		return continuo.trim();
	}

	/**
	 * M�todo que obtiene las Transferencias Programadas, las realiza en l�nea y las actualiza seg�n corresponda: procesada, cuando se realice bien,
	 * pendiente, para cuando existan excepciones y noProcesada, para cuando falle en el reproceso (las transferencias que fallen se reprocesar�n una vez).
	 *
	 * Se trae m�todo desde {@link wcorp.bprocess.cuentas.TransferenciasFondosBean#} a la implementaci�n Stateless del mismo y
	 * se cambia nivel de logeo de debug a info o error seg�n corresponda.
	 *
	 * Registro de versiones:<ul>
	 *
	 * <li>1.0 26/03/2009, Carlos Cerda I. (SEnTRA): Versi�n inicial.
	 * <li>1.1 27/03/2014, Ivan Carvajal A. (BCI): Se agrega reintento al actualizar estado
	 *
	 * </ul>
	 *
	 * @param tipoProceso
	 * @return String
	 * @throws Exception
	 *
	 * @since 1.0
	 * @see {@link wcorp.bprocess.cuentas.TransferenciasFondosBean#procesoTransferenciasProgramadas(String)}
	 */

	private String procesoTransferenciasProgramadas(String tipoProceso) throws Exception{
		HashMap salida = null;
		List resultado  = null;
		int actualizo = -1;
		String continuo;
		String estadoTransferencia = new String();
		ConsultaTransferenciasProgramadasDAO consultaTransferenciasProgramadasDAO = null;
		String estado = TablaValores.getValor(TTFF_CONFIG, "estadosAcualizacion", "pendiente");
		String descripcion = new String();
		if(logger.isDebugEnabled()){logger.debug("[procesoTransferenciasProgramadas] En procesoTransferenciasProgramadas");}
		if (logger.isInfoEnabled()){logger.info("[procesoTransferenciasProgramadas] tipoProceso: "+tipoProceso);}
		if(tipoProceso.equals("proceso")){
			estadoTransferencia = TablaValores.getValor(TTFF_CONFIG, "estadosAcualizacion", "proceso");
		}else if(tipoProceso.equals("reProceso")){
			estadoTransferencia = TablaValores.getValor(TTFF_CONFIG, "estadosAcualizacion", "reProceso");
		}
		if (logger.isInfoEnabled()){logger.info("[procesoTransferenciasProgramadas] estadoTransferencia: "+estadoTransferencia);}
		try {
			consultaTransferenciasProgramadasDAO = new ConsultaTransferenciasProgramadasDAO();
			if(logger.isDebugEnabled()){logger.debug("[procesoTransferenciasProgramadas] nos vamos a obtieneTransferenciasProgramadas");}
			resultado = consultaTransferenciasProgramadasDAO.obtieneTransferenciasProgramadas(estadoTransferencia);
			if (logger.isInfoEnabled()){logger.info("[procesoTransferenciasProgramadas] numero de programaciones: "+(resultado != null ? resultado.size() : 0));}
		} catch (Exception e) {
			if(logger.isEnabledFor(Level.ERROR)){logger.error("[procesoTransferenciasProgramadas] Exception en consultaTransferenciasProgramadasDAO: "+e.toString());}
			throw new Exception();
		}

		if(resultado != null && resultado.size() > 0){
			if (logger.isInfoEnabled()){logger.info("[procesoTransferenciasProgramadas] numero de programadas: "+ resultado.size());}
			continuo = "Si";
			for (int indice = 0; indice < resultado.size(); indice++) {

				salida = (HashMap) resultado.get(indice);
				String ctaOrigen = (String)salida.get("ctaOrigen");
				if (logger.isInfoEnabled()){logger.info("[procesoTransferenciasProgramadas] ctaOrigen: "+ctaOrigen);}
				String tipoCuentaOrigen = (String)salida.get("tipoCuentaOrigen");
				if (logger.isInfoEnabled()){logger.info("[procesoTransferenciasProgramadas] tipoCuentaOrigen: "+tipoCuentaOrigen);}
				String ctaDestino = (String)salida.get("ctaDestino");
				if (logger.isInfoEnabled()){logger.info("[procesoTransferenciasProgramadas] ctaDestino: "+ctaDestino);}
				String tipoCuentaDestino = (String)salida.get("tipoCuentaDestino");
				if (logger.isInfoEnabled()){logger.info("[procesoTransferenciasProgramadas] tipoCuentaDestino: "+tipoCuentaDestino);}
				Double monto = (Double)salida.get("monto");
				if (logger.isInfoEnabled()){logger.info("[procesoTransferenciasProgramadas] monto: "+monto.doubleValue());}
				Integer codBancoDestino = (Integer)salida.get("codBancoDestino");
				if (logger.isInfoEnabled()){logger.info("[procesoTransferenciasProgramadas] codBancoDestino: "+codBancoDestino.intValue());}
				String rutDestino = (String)salida.get("rutDestino");
				if (logger.isInfoEnabled()){logger.info("[procesoTransferenciasProgramadas] rutDestino: "+rutDestino);}
				String nombreDestino = (String)salida.get("nombreDestino");
				if (logger.isInfoEnabled()){logger.info("[procesoTransferenciasProgramadas] nombreDestino: "+nombreDestino);}
				String comentario = (String)salida.get("comentario");
				if (logger.isInfoEnabled()){logger.info("[procesoTransferenciasProgramadas] comentario: "+comentario);}
				String eMailCliente = (String)salida.get("eMailCliente");
				if (logger.isInfoEnabled()){logger.info("[procesoTransferenciasProgramadas] eMailCliente "+eMailCliente);}
				String eMailDestino = (String)salida.get("eMailDestino");
				if (logger.isInfoEnabled()){logger.info("[procesoTransferenciasProgramadas] eMailDestino: "+eMailDestino);}
				String rutCliente = (String)salida.get("rutOrigen");
				if (logger.isInfoEnabled()){logger.info("[procesoTransferenciasProgramadas] rutCliente: "+rutCliente);}
				String periodoProceso = (String)salida.get("periodoProceso");
				if (logger.isInfoEnabled()){logger.info("[procesoTransferenciasProgramadas] periodoProceso: "+periodoProceso);}
				Integer id = (Integer)salida.get("idProgramadas");
				if (logger.isInfoEnabled()){logger.info("[procesoTransferenciasProgramadas] id: "+id.intValue());}

				try {
					enviarTransferenciaProgramada(ctaOrigen.trim(), tipoCuentaOrigen, ctaDestino.trim(), tipoCuentaDestino, monto.doubleValue(), String.valueOf(codBancoDestino.intValue()), rutDestino, nombreDestino, comentario, eMailCliente, eMailDestino, rutCliente);
				} catch (Exception e) {
					if(e instanceof GeneralException){
						GeneralException ge = (GeneralException)e;
						descripcion = ge.getInfoAdic();
					}else{
						descripcion = TablaValores.getValor(TTFF_CONFIG, "estadosAcualizacion", "fallo");
					}
					if(logger.isEnabledFor(Level.ERROR)){logger.error("[procesoTransferenciasProgramadas] descripcion: "+descripcion);}
					if (tipoProceso.equals("reProceso")){
						estado = TablaValores.getValor(TTFF_CONFIG, "estadosAcualizacion", "noProcesar");
					}
					try {
						actualizo = consultaTransferenciasProgramadasDAO.actualizarEstadoTransferenciaProgramada(periodoProceso, id, estado, descripcion);
						if (logger.isInfoEnabled()){
							logger.info("[procesoTransferenciasProgramadas] se actualizo registro ["+actualizo+"] con estado: "+estado);
						}
					} catch (Exception ex) {
						logger.error("[procesoTransferenciasProgramadas] Exception en actualizarEstadoTransferenciaProgramada: "
							+ "Pendiente. Se reintenta actualizacion");
						try {
							actualizo = consultaTransferenciasProgramadasDAO.actualizarEstadoTransferenciaProgramada(
                                periodoProceso, id, estado, descripcion);
							if (logger.isInfoEnabled()){
								logger.info("[procesoTransferenciasProgramadas] Reintento: se actualizo registro ["
							        + actualizo + "] con estado: " + estado);
							}
						} catch (Exception exc) {
							logger.error("[procesoTransferenciasProgramadas] Reintento: Exception en "
								+ "actualizarEstadoTransferenciaProgramada: Pendiente");
						}
					}
					continue;
				}
				try {
					actualizo = consultaTransferenciasProgramadasDAO.actualizarEstadoTransferenciaProgramada(periodoProceso, id, TablaValores.getValor(TTFF_CONFIG, "estadosAcualizacion", "procesada"), TablaValores.getValor(TTFF_CONFIG, "estadosAcualizacion", "ok"));
					if (logger.isInfoEnabled()){
						logger.info("[procesoTransferenciasProgramadas] se actualizo registro ["+actualizo+"] con estado: Procesada");
					}
				} catch (Exception e) {
					logger.error("[procesoTransferenciasProgramadas] Exception en actualizarEstadoTransferenciaProgramada: "
						+ "Procesada. Se reintenta actualizacion");
					try {
						actualizo = consultaTransferenciasProgramadasDAO.actualizarEstadoTransferenciaProgramada(
                            periodoProceso, id, TablaValores.getValor(TTFF_CONFIG, "estadosAcualizacion", "procesada")
                            , TablaValores.getValor(TTFF_CONFIG, "estadosAcualizacion", "ok"));
						if(logger.isEnabledFor(Level.WARN)){
							logger.warn("[procesoTransferenciasProgramadas] Reintento: se actualizo registro ["
						        + actualizo + "] con estado: Procesada");
						}
					} catch (Exception ex) {
						logger.error("[procesoTransferenciasProgramadas] Reintento: Exception en "
							+ "actualizarEstadoTransferenciaProgramada: Procesada");
					}
				}
			}
		}else{
			if(logger.isEnabledFor(Level.ERROR)){logger.error("[procesoTransferenciasProgramadas] resultado null o con ceros no continuamos");}
			continuo = "No";
		}
		return continuo;
	}

	/**
	 * M�todo que realiza la conexi�n con el metodo transCtaCteOtroBancoLineaPer() de este mismo bean
	 * para que las transferencias programadas se hagan en l�nea.
	 *
	 * <p>Se utiliza logica ya existente de conexi�n con operador CCA o Redbanc.</p>
	 *
	 * Se trae m�todo desde {@link wcorp.bprocess.cuentas.TransferenciasFondosBean#} a la implementaci�n Stateless del mismo y
	 * se cambia nivel de logeo de debug a info o error seg�n corresponda.
	 *
	 * Registro de versiones:<ul>
	 *
	 * <li>1.0 26/03/2009, Carlos Cerda I. (SEnTRA): Versi�n inicial.
	 * <li>1.1 28/09/2011, Carlos Cerda I. (Orand): Se agragan atributos: nemonicoCargo y nemonicoAbono, con los cuales se realiza una transferencia de fondos.
	 *
	 * </ul>
	 *
	 * @param ctaOrigen,
	 * @param tipoCuentaOrigen,
	 * @param ctaDestino,
	 * @param tipoCuentaDestino,
	 * @param monto,
	 * @param codBancoDestino,
	 * @param rutDestino,
	 * @param nombreDestino,
	 * @param comentario,
	 * @param eMailCliente,
	 * @param eMailDestino,
	 * @param rutCliente
	 * @return void
	 * @throws Exception
	 *
	 * @since 1.0
	 * @see {@link wcorp.bprocess.cuentas.TransferenciasFondosBean#enviarTransferenciaProgramada(String,String,String,String,double,String,String,String,String,String,String,String) }
	 */

	private void enviarTransferenciaProgramada(String ctaOrigen,
			String tipoCuentaOrigen,
			String ctaDestino,
			String tipoCuentaDestino,
			double monto,
			String codBancoDestino,
			String rutDestino,
			String nombreDestino,
			String comentario,
			String eMailCliente,
			String eMailDestino,
			String rutCliente) throws Exception{

		String nomBanco = "";
		long rutDestin;
		char dvDestin;
		BancosAutorizados bcosAutorizados[] = null;
		if (logger.isInfoEnabled()){
			logger.info("[enviarTransferenciaProgramada] CtaOrigen: ["+ctaOrigen+"]");
			logger.info("[enviarTransferenciaProgramada] TipoCuentaOrigen: ["+tipoCuentaOrigen+"]");
			logger.info("[enviarTransferenciaProgramada] CtaDestino: ["+ctaDestino+"]");
			logger.info("[enviarTransferenciaProgramada] TipoCuentaDestino: ["+tipoCuentaDestino+"]");
			logger.info("[enviarTransferenciaProgramada] Monto: ["+monto+"]");
			logger.info("[enviarTransferenciaProgramada] NombreDestino: ["+nombreDestino+"]");
			logger.info("[enviarTransferenciaProgramada] Comentario: ["+comentario+"]");
			logger.info("[enviarTransferenciaProgramada] EMailCliente: ["+eMailCliente+"]");
			logger.info("[enviarTransferenciaProgramada] EMailDestino: ["+eMailDestino+"]");
		}
		String tipoAutenticacion = TablaValores.getValor(TTFF_CONFIG, "trfPrg", "tipoAuten");
		if (logger.isInfoEnabled()){logger.info("[enviarTransferenciaProgramada] tipoAutenticacion: ["+tipoAutenticacion+"]");}
		String tipotransferencia = TablaValores.getValor(TTFF_CONFIG, "trfPrg", "tipoTrans");
		if (logger.isInfoEnabled()){logger.info("[enviarTransferenciaProgramada] tipotransferencia: ["+tipotransferencia+"]");}
		String codigoAlias = TablaValores.getValor(TTFF_CONFIG, "trfPrg", "codAlias");
		if (logger.isInfoEnabled()){logger.info("[enviarTransferenciaProgramada] codigoAlias: ["+codigoAlias+"]");}
		String canal = TablaValores.getValor(TTFF_CONFIG, "trfPrg", "canalPrg");
		if (logger.isInfoEnabled()){logger.info("[enviarTransferenciaProgramada] canal: ["+canal+"]");}
		String codigoBCI = String.valueOf(Integer.parseInt(codigoBancoBCI));
		if (logger.isInfoEnabled()){logger.info("[enviarTransferenciaProgramada] codigoBCI: ["+codigoBCI+"]");}
		boolean esBCI = (codigoBCI.equals(codBancoDestino));
		if (logger.isInfoEnabled()){logger.info("[enviarTransferenciaProgramada] esBCI: ["+esBCI+"]");}
		String nemonicoCargo = TablaValores.getValor(NEMONICOS_CONFIG, "CargoTrfProgramada", "Desc");
		if (logger.isInfoEnabled()){logger.info("[enviarTransferenciaProgramada] nemonicoCargo: ["+nemonicoCargo+"]");}
		String nemonicoAbono = TablaValores.getValor(NEMONICOS_CONFIG, "AbonoTrfProgramada", "Desc");
		if (logger.isInfoEnabled()){logger.info("[enviarTransferenciaProgramada] nemonicoAbono: ["+nemonicoAbono+"]");}

		Cliente cliente = null;
		long rut;
		char dig;

		try{
			if (logger.isInfoEnabled()){logger.info("[enviarTransferenciaProgramada] RutDestino sin formato: ["+rutDestino+"]");}

			rutDestin = Long.parseLong(rutDestino.substring(0,(rutDestino.length()-1)));
			if (logger.isInfoEnabled()){logger.info("[enviarTransferenciaProgramada] RutDestino: ["+rutDestin+"]");}
			dvDestin = (rutDestino.substring((rutDestino.length()-1),(rutDestino.length()))).charAt(0);
			if (logger.isInfoEnabled()){logger.info("[enviarTransferenciaProgramada] dvDestino: ["+dvDestin+"]");}

			rut = Long.parseLong(rutCliente.substring(0,(rutCliente.length()-1)));
			if (logger.isInfoEnabled()){logger.info("[enviarTransferenciaProgramada] RutOrigen: ["+rut+"]");}
			dig = (rutCliente.substring((rutCliente.length()-1),(rutCliente.length()))).charAt(0);
			if (logger.isInfoEnabled()){logger.info("[enviarTransferenciaProgramada] dvOrigen: ["+dig+"]");}

			cliente = new Cliente(rut, dig);
			cliente.setDatosBasicos();
			logger.debug("[enviarTransferenciaProgramada] Cliente Creado");
			DatosBasicosCliente datosBasicosCliente = cliente.getDatosBasicosBCI();
			logger.debug("[enviarTransferenciaProgramada] Procesamos datos basicos del cliente");
			String nombreCompleto = datosBasicosCliente.getNombre().trim()+" "+datosBasicosCliente.getApellidoPaterno().trim()+" "+datosBasicosCliente.getApellidoMaterno().trim();
			if (logger.isInfoEnabled()){logger.info("[enviarTransferenciaProgramada] nombreCompleto: ["+nombreCompleto+"]");}

			if(!esBCI){
				logger.debug("[enviarTransferenciaProgramada] Buscamos bancos autorizados");
				bcosAutorizados = serviciosSipe.getBancosAutorizadosLinea();
				if (bcosAutorizados != null) {
					if (logger.isInfoEnabled()){logger.info("[enviarTransferenciaProgramada] CodBancoDestino: ["+codBancoDestino+"]");}
					for (int i = 0; i < bcosAutorizados.length; i++){
						if (codBancoDestino.equals(bcosAutorizados[i].codBanco)){
							nomBanco = bcosAutorizados[i].getNombre();
							if (logger.isInfoEnabled()){logger.info("[enviarTransferenciaProgramada] nomBanco: ["+nomBanco+"]");}
						}
					}
				}
			}
		} catch (Exception e) {
			if(logger.isEnabledFor(Level.ERROR)){logger.error("[enviarTransferenciaProgramada] Exception en obtener codigo de bancos Autorizados: "+e.toString());}
			throw new Exception();
		}
		if(esBCI){
			logger.debug("[enviarTransferenciaProgramada] Transfieriendo a Cuenta BCI");
			if((tipoCuentaOrigen.equals("CCT") || tipoCuentaOrigen.equals("CPR")) && (tipoCuentaDestino.equals("CCT") || tipoCuentaDestino.equals("CPR"))){
				logger.debug("[enviarTransferenciaProgramada] CCT/CPR -> CCT/CPR");
				try {
					transfiereCtaCteHaciaCtaCte(
							ctaOrigen,
							tipoCuentaOrigen,
							ctaDestino,
							tipoCuentaDestino,
							monto,
							rutDestin,
							dvDestin,
							cliente,
							canal,
							comentario,
							nombreDestino,
							eMailCliente,
							eMailDestino,
							tipoAutenticacion,
							tipotransferencia,
							codigoAlias,
							nemonicoCargo,
							nemonicoAbono);
				} catch (Exception e) {
					if(logger.isEnabledFor(Level.ERROR)){logger.error("[enviarTransferenciaProgramada] Exception en transfiereCtaCteHaciaCtaCte: "+e.toString());}
					throw e;
				}
			}else if((tipoCuentaOrigen.equals("CCT") || tipoCuentaOrigen.equals("CPR")) && (tipoCuentaDestino.equals("AHO") || tipoCuentaDestino.equals("LDE"))){
				logger.debug("[enviarTransferenciaProgramada] CCT/CPR -> AHO/LDE");
				try {
					transfiereCtaCteHaciaAhorro(
							ctaOrigen,
							tipoCuentaOrigen,
							ctaDestino,
							monto,
							rutDestin,
							dvDestin,
							cliente,
							canal,
							comentario,
							nombreDestino,
							eMailCliente,
							eMailDestino,
							tipoAutenticacion,
							tipotransferencia,
							codigoAlias);
				} catch (Exception e) {
					if(logger.isEnabledFor(Level.ERROR)){logger.error("[enviarTransferenciaProgramada] Exception en transfiereCtaCteHaciaAhorro: "+e.toString());}
					throw e;
				}
			}else{
				if(logger.isEnabledFor(Level.ERROR)){logger.error("[enviarTransferenciaProgramada] Tipo de transferencia indefinida");}
				throw new Exception("Tipo de transferencia indefinida");
			}
		}else{
			logger.debug("[enviarTransferenciaProgramada] Transfieriendo a Otros Bancos");
			try {
				transCtaCteOtroBancoLineaPer(
						ctaOrigen,
						tipoCuentaOrigen,
						ctaDestino,
						tipoCuentaDestino,
						monto,
						StringUtil.rellenaPorLaIzquierda(codBancoDestino,3,'0'),
						nomBanco,
						rutDestin,
						dvDestin,
						cliente,
						canal,
						nombreDestino,
						comentario,
						eMailCliente,
						eMailDestino,
						tipoAutenticacion,
						tipotransferencia,
						codigoAlias);
			} catch (Exception e) {
				if(logger.isEnabledFor(Level.ERROR)){logger.error("[enviarTransferenciaProgramada] Exception en transCtaCteOtroBancoLineaPer: "+e.toString());}
				throw e;
			}
		}
	}

	/**
     * M�todo que realiza la decisi�n de almacenar o no la transacci�n sensible (en base a la excepci�n lanzada por la aplicaci�n),
	 * la cual se procesar� posteriormente en modo batch.
	 *
	 *
	 * Registro de versiones:<ul>
	 *
	 * <li>1.0 01/06/2009, Carlos Cerda I. (SEnTRA): versi�n inicial.
	 * <li>1.1 22/02/2010, Carlos Cerda I. (Orand): Se agrega par�metro traceNumber (identificador de la transacci�n) a la firma del m�todo.
	 * <li>1.2 25/03/2010, Jes�s Orellana. (Orand): Se quita par�metro de entrada ctaDestino y los par�metros ctaDestino enviados
	 * 												al m�todo registraTransaccionesPendientes().
	 * <li>1.3 13/05/2010, Carlos Cerda I. (Orand): Se agrega nem�nico a la llamada al m�todo: {@link wcorp.serv.cuentas.ServiciosCuentasBean#registraTransaccionesPendientes(long, char, String, String, String, String, String, String, String, String, String, String, int, char, String, String, String)}.
     * <li>1.4 13/10/2010, Leonardo Espinoza (Orand): Se acomoda la llamada al m�todo {@link wcorp.serv.cuentas.ServiciosCuentasBean#registraTransaccionesPendientes(long, char, String, String, String, String, String, String, String, String, String, String, int, char, String, String, String)}
     *                                      a la situaci�n de Transferencias dentro del mismo banco. Para ello se agregaron dos argumentos: un string con la cuenta de destino y un boolean para indicar si es una llamada bajo este caso.
     * <li>1.5 07/02/2012 Carlos Cerda I. (Orand): Se agrega par�metro 'xfml' a la firma del m�todo para poder
     * reinsistir las consultas al operador (estas necesitan la mensaje�a completa).
	 * </ul>
	 *
	 * @param cliente, objeto Cliente desde el cual se obtiene el rut y dv del cliente
	 * @param canal, canal del cliente
	 * @param ex, excepci�n lanzada por la aplicaci�n
	 * @param trx, transacci�n
	 * @param ctaOrigen,	cuenta de origen
	 * @param ctaDestino, cuenta de destino
	 * @param monto
	 * @param traceNumber, identificador de la transacci�n
	 * @param producto, producto (TTFF, Abonos, TDC, etc.)
	 * @param convenio, caso empresas
	 * @param intentos, cantidad de iteraciones que realizar� el ejecutor de las transacciones pendientes
         * @param esTTFFMismoBanco, indica si la transacci�n a registrar corresponde a una dentro del mismo banco
         * @param xfml mensajer�a hacia el otro banco.
         * @param nemonicoAbono nemonico de cargo
         * @param nemonicoCargo nemonico de abono
	 *
	 * @return void
	 *
	 * @since 1.0
	 */
	private boolean registraTransaccionPendiente(Cliente cliente,
			String canal,
			Exception ex,
			String trx,
			String ctaOrigen,
                        String ctaDestino,
			double monto,
			String traceNumber,
			String producto,
			String convenio,
                        int intentos,
                        boolean esTTFFMismoBanco,
                        String numeroOperacion,
                        String emailCliente,
                        String emailDestino,
                        String xfml,
                        String nemonicoCargo,
                        String nemonicoAbono){
        boolean resultadoRegTrxPend = false;

		if(ex instanceof GeneralException){
			logCliente(cliente,canal,"registraTransaccionPendiente", "En GeneralException");
			GeneralException ge = (GeneralException)ex;
            boolean errorFound = false;
			try {
				logCliente(cliente,canal,"registraTransaccionPendiente", "Buscamos c�digo: "+ge.getCodigo());
				errorFound = serviciosCuentas.buscarCodigoError("TransferenciasPersonas", ge.getCodigo());
			} catch (Exception e) {
				logCliente(cliente,canal,"registraTransaccionPendiente", "Exception en buscarCodigoError: "+e.getMessage());
				return false;
			}
			logCliente(cliente,canal,"registraTransaccionPendiente", "C�digo encontrado: "+errorFound);
			if(errorFound){
                resultadoRegTrxPend = registrarTrxPendientePorSrvCuentas(
                        cliente, canal, trx, ctaOrigen, ctaDestino, monto,
                        traceNumber, producto, convenio, intentos, esTTFFMismoBanco,
                        numeroOperacion, emailCliente, emailDestino, xfml, nemonicoCargo, nemonicoAbono);
			}
		}else{
			logCliente(cliente,canal,"registraTransaccionPendiente", "Otras Exception's");
            resultadoRegTrxPend = registrarTrxPendientePorSrvCuentas(
                    cliente, canal, trx, ctaOrigen, ctaDestino, monto,
                    traceNumber, producto, convenio, intentos, esTTFFMismoBanco,
                    numeroOperacion, emailCliente, emailDestino, xfml, nemonicoCargo, nemonicoAbono);
        }
        return resultadoRegTrxPend;
    }

    /**
     * M�todo que registra la transacci�n pendiente a trav�s del Bean de ServicioCuentas. Aparte de este objetivo principal,
     * tambi�n se encarga de configurar bien los 6 parametros gen�ricos para los casos de Transacciones dentro del mismo banco como hacia fuera.
     *
     * Registro de versiones:<ul>
     *
     * <li>1.0 28/10/2010, Leonardo Espinoza (Orand): Versi�n inicial.
     * <li>1.1 07/02/2012 Carlos Cerda I. (Orand): Se agrega par�metro 'xfml' a la firma del m�todo para poder
     * reinsistir las consultas al operador (estas necesitan la mensaje�a completa).
     * </ul>
     *
     * @param cliente
     * @param canal
     * @param trx
     * @param ctaOrigen
     * @param ctaDestino
     * @param monto
     * @param traceNumber
     * @param producto
     * @param convenio
     * @param intentos
     * @param esTTFFMismoBanco
     * @param xfml mensajer�a hacia el otro banco.
     * @return un bolean indicando si la transacci�n pendiente fue exitosamente registrada
     * @since 1.8
     */
    private boolean registrarTrxPendientePorSrvCuentas(Cliente cliente,
            String canal,
            String trx,
            String ctaOrigen,
            String ctaDestino,
            double monto,
            String traceNumber,
            String producto,
            String convenio,
            int intentos,
            boolean esTTFFMismoBanco,
            String numeroOperacion,
            String emailCliente,
            String emailDestino,
            String xfml,
            String nemonicoCargo,
            String nemonicoAbono) {
			try {
                String prefijoCuentas = TablaValores.getValor(TTFF_CONFIG, "producto", "Tandem");
                String param2 = null, param4 = null, param6 = null;
                if (esTTFFMismoBanco) {
                    param2 = StringUtil.rellenaPorLaIzquierda(nemonicoCargo.substring(0, 4), 13, '0');
                    param4 = prefijoCuentas+ctaDestino;
                    param6 = StringUtil.rellenaPorLaIzquierda(nemonicoAbono.substring(0, 4), 13, '0');
                } else {
                    param2 = TablaValores.getValor(TTFF_CONFIG, "nemonicoTransferencias", "desc");
                    param4 = null;
                    param6 = null;
                }

				serviciosCuentas.registraTransaccionesPendientes(cliente.getRut(),
						cliente.getDigito(),
						canal,
						producto,
						convenio,
						trx,
                        prefijoCuentas+ctaOrigen,
                        param2,
						StringUtil.rellenaPorLaDerecha(
StringUtil.rellenaPorLaIzquierda(String.valueOf((long)monto), 11, '0'), 13, '0'),
                        param4,
						StringUtil.rellenaPorLaIzquierda(traceNumber, 13, '0'),
                        param6,
						intentos,
						'P',
                        numeroOperacion,
                        emailCliente,
                        emailDestino,
                        xfml);

			} catch (Exception e) {
            logCliente(cliente,canal,"registraTransaccionPendiente", "Exception: "+e.toString());
				return false;
			}
		return true;
	}

	/**
	 * M�todo que permite la comunicaci�n con el Adapter CCA para las Reversas en L�nea
	 * a otros bancos.
	 *
	 *
	 * Registro de versiones:<ul>
	 *
	 * <li>1.0 10/07/2009, Carlos Cerda I. (SEnTRA): Versi�n inicial.
     * <li>1.1 30/11/2016 Andr�s Pardo T. (SEnTRA) - Marcelo Avenda�o. (Ing. Soft. BCI): Se asigna parametro XFML_RESPONSE_PARAMETRO a referencia null.
	 *
	 * </ul>
	 *
	 *
	 * @param cliente
	 * @param canal
	 * @param tipoCuentaOrigen: Tipo de cuenta de origen (CCT, CPR)
	 * @param tipoCuentaDestino: Tipo de cuenta de destino (CCT, CPR)
	 * @param monto: monto de la transferencia
	 * @param traNumber: identificador �nico de la transferencia
	 * @param fechaCargo: fecha de cargo de la transferencia
	 * @param ctaOrigen: cuenta del cliente
	 * @param rut: rut del cliente
	 * @param dv: digito verificador del cliente
	 * @param codBancoDestino: c�digo del banco de destino (001, etc)
	 * @param ctaDestino: cuenta de abono
	 * @param rutDestino: rut de abono
	 * @param digDestino: digito verificador de abono
	 * @param nombreDestino: nombre de la persona de destino del abono
	 * @param comentario: comentario para el env�o de Email
	 * @param numeroOperacion: n�mero que devuelve el sp que inseta en la BD tra
	 * @return CCADirectaVO, con informaci�n enviada por el operador.
	 * @throws TuxedoException
	 * @throws GeneralException
	 *
	 * @since 1.0
	 */

	private boolean realizaReversaBanco(
			Cliente cliente,
			String canal,
			String tipoCuentaOrigen,
			String tipoCuentaDestino,
			double monto,
			String traceNumber,
			Date fecha,
			String hora,
			String ctaOrigen,
			long rut,
			char dv,
			String codBancoDestino,
			String ctaDestino,
			long rutDestino,
			char digDestino,
			String nombreDestino,
			String comentario,
			String numeroOperacion) {

		try {
			logCliente(cliente,canal,"realizaReversaBanco", "Modo XFMLBuffer realizaReversaBanco");

			XFMLBuffer xfmlBuffer = new XFMLBuffer();

			String tipCuentaOrigen = TablaValores.getValor(TTFF_CONFIG, "idTiposCuentaLinea", tipoCuentaOrigen);
			logCliente(cliente,canal,"realizaReversaBanco", "tipoCuentaOrigen: "+tipCuentaOrigen);
			String tipCuentaDestino = TablaValores.getValor(TTFF_CONFIG, "idTiposCuentaLinea", tipoCuentaDestino);
			logCliente(cliente,canal,"realizaReversaBanco", "tipoCuentaDestino: "+tipCuentaDestino);

			xfmlBuffer.addDato(new Dato("productoOrigen", tipCuentaOrigen));
			xfmlBuffer.addDato(new Dato("productoDestino", tipCuentaDestino));

			long montoTrans = (long)monto;

			xfmlBuffer.addDato(new Dato("monto", (StringUtil.rellenaPorLaDerecha((StringUtil.rellenaPorLaIzquierda(String.valueOf(montoTrans), 10, '0')), 12, '0'))));
			logCliente(cliente,canal,"realizaReversaBanco", "montoTrans: "+montoTrans);
			logCliente(cliente,canal,"realizaReversaBanco", "monto: "+(StringUtil.rellenaPorLaDerecha((StringUtil.rellenaPorLaIzquierda(String.valueOf(montoTrans), 10, '0')), 12, '0')));
			xfmlBuffer.addDato(new Dato("traceNumber", StringUtil.rellenaPorLaIzquierda(traceNumber, 12, '0')));
			logCliente(cliente,canal,"realizaReversaBanco", "traceNumber: "+traceNumber);
			xfmlBuffer.addDato(new Dato("hora",hora));
			logCliente(cliente,canal,"realizaReversaBanco", "hora: "+hora);
			xfmlBuffer.addDato(new Dato("fecha", (new SimpleDateFormat("yyMMdd").format(fecha))));
			logCliente(cliente,canal,"realizaReversaBanco", "fecha: "+(new SimpleDateFormat("yyMMdd").format(fecha)));

			xfmlBuffer.addDato(new Dato("motivoReversa", TablaValores.getValor(TTFF_CONFIG, "motivoReversa", "valor")));
			logCliente(cliente,canal,"realizaReversaBanco", "motivoReversa: "+TablaValores.getValor(TTFF_CONFIG, "motivoReversa", "valor"));

			xfmlBuffer.addDato(new Dato("moneda", (TablaValores.getValor(TTFF_CONFIG, "tipoMoneda", "Peso"))));
			logCliente(cliente,canal,"realizaReversaBanco", "moneda: "+(TablaValores.getValor(TTFF_CONFIG, "tipoMoneda", "Peso")));
			xfmlBuffer.addDato(new Dato("bancoOrigen", (StringUtil.rellenaPorLaIzquierda(codigoBancoBCI, 4, '0'))));
			logCliente(cliente,canal,"realizaReversaBanco", "bancoOrigen: "+(StringUtil.rellenaPorLaIzquierda(codigoBancoBCI, 4, '0')));
			xfmlBuffer.addDato(new Dato("cuentaOrigen", (StringUtil.rellenaPorLaIzquierda(ctaOrigen, 20, '0'))));
			logCliente(cliente,canal,"realizaReversaBanco", "cuentaOrigen: "+(StringUtil.rellenaPorLaIzquierda(ctaOrigen, 20, '0')));

			String rutGir = StringUtil.rellenaPorLaIzquierda((String.valueOf(rut)+dv), 12, '0');
			xfmlBuffer.addDato(new Dato("rutGirador", rutGir));
			logCliente(cliente,canal,"realizaReversaBanco", "rutGir: "+rutGir);

			String nombreCliente = cliente.getFullName().length() > 100 ? cliente.getFullName().substring(0, 100) : cliente.getFullName();
			logCliente(cliente,canal,"realizaReversaBanco", "nombreCliente if: "+nombreCliente);

			xfmlBuffer.addDato(new Dato("nombreGirador", nombreCliente));
			xfmlBuffer.addDato(new Dato("bancoDestino", (StringUtil.rellenaPorLaIzquierda(codBancoDestino, 4, '0'))));
			logCliente(cliente,canal,"realizaReversaBanco", "bancoDestino: "+(StringUtil.rellenaPorLaIzquierda(codBancoDestino, 4, '0')));
			xfmlBuffer.addDato(new Dato("cuentaDestino", (StringUtil.rellenaPorLaIzquierda(ctaDestino, 20, '0'))));
			logCliente(cliente,canal,"realizaReversaBanco", "cuentaDestino: "+(StringUtil.rellenaPorLaIzquierda(ctaDestino, 20, '0')));

			String rutDest = StringUtil.rellenaPorLaIzquierda((String.valueOf(rutDestino)+digDestino), 12, '0');
			xfmlBuffer.addDato(new Dato("rutDestinatario", rutDest));
			logCliente(cliente,canal,"realizaReversaBanco", "rutDest: "+rutDest);
			if (nombreDestino != null && !nombreDestino.trim().equals("")){
				String nomDestino = nombreDestino.length() > 100 ? nombreDestino.substring(0, 100) : nombreDestino;
				xfmlBuffer.addDato(new Dato("nombreDestinatario", nomDestino));
				logCliente(cliente,canal,"realizaReversaBanco", "nomDestino: "+nomDestino);
			}else{
				xfmlBuffer.addDato(new Dato("nombreDestinatario", " "));
				logCliente(cliente,canal,"realizaReversaBanco", "nomDestino: ");
			}
			if (comentario != null){
				xfmlBuffer.addDato(new Dato("referencia", comentario));
			}else
				xfmlBuffer.addDato(new Dato("referencia", " "));
			logCliente(cliente,canal,"realizaReversaBanco", "comentario: "+comentario);
			xfmlBuffer.addDato(new Dato("nroOperacion", numeroOperacion));
			logCliente(cliente,canal,"realizaReversaBanco", "numeroOperacion: "+numeroOperacion);

			String xfmlRequest = xfmlBuffer.getXFMLString();
			logCliente(cliente,canal,"realizaReversaBanco", xfmlRequest);

			sesion = joltPool.getSesion(ejbName);
			Result resultado = null;
			DataSet parametros = new DataSet();
			parametros.setValue("in_buffer", xfmlRequest);

			XFMLBuffer bufferResponse = null;

			logCliente(cliente,canal,"realizaReversaBanco", "Invocaremos Jolt ccareversa");
			resultado = sesion.call("ccareversa", parametros, null);
			logCliente(cliente,canal,"realizaReversaBanco", "Obtenemos los datos del Jolt");
            String xfmlResponse = (String)resultado.getValue("out_buffer",0, XFML_RESPONSE_PARAMETRO);
			logCliente(cliente,canal,"realizaReversaBanco", xfmlResponse);
			bufferResponse = new XFMLBuffer(xfmlResponse);
			logCliente(cliente,canal,"realizaReversaBanco", "Construimos XFMLBuffer");
			String tipoRespuesta = (bufferResponse.getDato("tipoRespuesta")).getValor();
			logCliente(cliente,canal,"realizaReversaBanco", "TipoRespuesta: "+tipoRespuesta);
			if(!tipoRespuesta.equals("000")){
				return false;
			}
		} catch (Exception e) {
			logCliente(cliente,canal,"realizaReversaBanco", "Exception al invocar jolt ccareversa: "+e.toString());
			return false;
		}
		return true;
	}

 /**
     * M�todo que carga un monto a la cuentaCorriente por motivo del Cierre de Linea de Emergencia, este monto
     * corresponde al pago del cierre.
     * <p>
     * Registro de versiones:
     * <ul>
     *     <li>1.0 15/10/2009 Pablo Carre�o (TINet): Versi�n inicial.
     * </ul>
     * <p>
     *
     * @param cuentaCorrienteCierre Cuenta en la que se generara un cargo.
     * @param rut Identificaci�n del cliente.
     * @param monto Monto que se cargara en la cuenta corriente.
     * @throws TransferenciaException En caso de error al realizar el cargo de la l�nea de cr�dito.
     * @since 1.8
     */
    public void cargaCuentaCorrientePorPago(String cuentaCorrienteCierre, long rut, double monto) throws TransferenciaException {
        CuentaCorriente cuentaCorriente;
        try {
            cuentaCorriente = new CuentaCorriente(cuentaCorrienteCierre);
            cuentaCorriente.cargaCuentaCorrientePorPago(cuentaCorrienteCierre, rut, monto);
        }
        catch (RemoteException e) {
            if (this.obtenerLogger().isEnabledFor(Level.ERROR)) {
                this.obtenerLogger().error(
                    "Error al cargar la cuenta", e);
            }
            throw new SistemaException("ESPECIAL", e);
        }
        catch (NamingException e) {
            if (this.obtenerLogger().isEnabledFor(Level.ERROR)) {
                this.obtenerLogger().error(
                    "Error al cargar la cuenta", e);
            }
            throw new SistemaException("ESPECIAL", e);
        }
        catch (CreateException e) {
            if (this.obtenerLogger().isEnabledFor(Level.ERROR)) {
                this.obtenerLogger().error(
                    "Error al cargar la cuenta", e);
            }
            throw new SistemaException("ESPECIAL", e);
        }
        catch (ServicioNoDisponibleException e) {
            if (this.obtenerLogger().isEnabledFor(Level.ERROR)) {
                this.obtenerLogger().error(
                    "Error al cargar la cuenta", e);
            }
            throw new SistemaException("ESPECIAL", e);
        }

    }

    /**
     * Permite validar y obtener logger si este no se encuentra creado.
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 29/09/2009 Pablo Carre�o (TINet): Versi�n inicial.
     * </ul>
     * <p>
     *
     * @return Logger Objeto de log.
     * @since 1.8
     */
    private Logger obtenerLogger() {
        if (logger == null) {
            logger = Logger.getLogger(TransferenciasDeFondosBean.class);
        }
        return logger;
    }

	/**
	 * <p> Transfiere fondos desde cuenta corriente hacia cuenta corriente de mismo banco BCI.
	 * Este m�todo ser� utilizado para realizar transferencias desde cuentas corrientes empresas, ya que la l�gica de negocio es diferente
	 * a la de realizar transferencias desde cuentas corrientes personas {@link #transfiereCtaCteHaciaCtaCte()}; como por ejemplo para el
	 * caso de personas se registra la transferencia en la BD ctaster.
	 * Debido a lo indicado anteriormente se prefiri� crear este nuevo m�todo que realiza s�lo la operaci�n de cargo-abono.</p>
	 *
	 * Registro de versiones:<ul>
	 * <li>1.0 (03/08/2010, Marcelo Fuentes H. (SEnTRA)): Versi�n inicial.</li>
	 * <li>1.1 (12/11/2010, Marcelo Fuentes H. (SEnTRA)): Se formatean los par�metros serial y serial2, que ser�n enviados al servicio tuxedo,
	 *                                                    con ceros a la izquierda.
     * <li>1.2 (09/08/2012, Jaime Pezoa N��ez (BCI)): Se formatean los par�metros ctaCargo y ctaAbono, que ser�n enviados al servicio tuxedo,
     *                                                    con ceros a la izquierda.
	 *  </ul>
	 *
	 * @param ctaCargo Cuenta Bci a la cual se le efectuar� el cargo
	 * @param serial c�digo param�trico, utilizado internamente en la transacci�n
	 * @param monto el monto a transferir
	 * @param ctaAbono Cuenta Bci a la cual se le efectuar� el abono
	 * @param serial2 c�digo param�trico, utilizado internamente en la transacci�n
	 * @param glosa valor para identificar una transferencia realizada
	 * @return boolean con el resultado de la transacci�n
	 * @throws TransferenciaException, RemoteException
	 *
	 * @since 1.9
	 */
	public boolean transferenciaCtaCteBciEnLineaEmpresa(
			String ctaCargo,
			String serial,
			String monto,
			String ctaAbono,
			String serial2,
			String glosa) throws TuxedoException {

		boolean actualizo = true;
		sesion = joltPool.getSesion(ejbName);
		DataSet parametros = new DataSet();
		Result resultado = null;

		if(logger.isDebugEnabled()){
			logger.debug("[transferenciaCtaCteBciEnLineaEmpresa]:: Cuenta Cargo ["+ ctaCargo +"]");
			logger.debug("[transferenciaCtaCteBciEnLineaEmpresa]:: Serial ["+ serial +"]");
			logger.debug("[transferenciaCtaCteBciEnLineaEmpresa]:: monto ["+ monto +"]");
			logger.debug("[transferenciaCtaCteBciEnLineaEmpresa]:: Cuenta Abono ["+ ctaAbono +"]");
			logger.debug("[transferenciaCtaCteBciEnLineaEmpresa]:: Serial 2 ["+ serial2 +"]");
			logger.debug("[transferenciaCtaCteBciEnLineaEmpresa]:: Glosa ["+ glosa +"]");
		}

        parametros.setValue("ctaCargo", StringUtil.completaPorLaIzquierda(ctaCargo, 8, '0'));
		parametros.setValue("serial", StringUtil.completaPorLaIzquierda(serial, 13, '0'));
		parametros.setValue("monto",monto);
        parametros.setValue("ctaAbono", StringUtil.completaPorLaIzquierda(ctaAbono, 8, '0'));
		parametros.setValue("serial2", StringUtil.completaPorLaIzquierda(serial2, 13, '0'));
		parametros.setValue("glosa", StringUtil.completaPorLaIzquierda(glosa, 13, '0'));

		try {
			if(logger.isDebugEnabled()){logger.debug("[transferenciaCtaCteBciEnLineaEmpresa]: Llamamos al servicio CtaCteTTFF");}
			resultado = sesion.call("CtaCteTTFF", parametros, null);
			if(logger.isDebugEnabled()){logger.debug("[transferenciaCtaCteBciEnLineaEmpresa]: Volvimos de CtaCteTTFF");}
			String ctaAbono_resp = (String)resultado.getValue("ctaAbono",0, null);
			if(logger.isDebugEnabled()){logger.debug("[transferenciaCtaCteBciEnLineaEmpresa]: ctaAbono respuesta ["+ctaAbono_resp+"]");}
			String monto_resp = (String)resultado.getValue("monto",0, null);
	        if(logger.isDebugEnabled()){logger.debug("[transferenciaCtaCteBciEnLineaEmpresa]: monto respuesta ["+monto_resp+"]");}

		} catch (Exception e) {
			if(logger.isDebugEnabled()){logger.debug("[transferenciaCtaCteBciEnLineaEmpresa]: Error [CtaCteTTFF] (" + ErroresUtil.extraeStackTrace(e) + ")");}
			actualizo = false;
		}
		return actualizo;
	}

     /**
     * <p> Hace el cargo a una cuenta BCI empresa. Este m�todo ser� utilizado para realizar cargos por transferencias desde cuentas
     * BCI Empresas a cuentas de otros bancos. La transacci�n utilizada es gen�rica lo que permite utilizar un determinado nem�nico para as�
     * diferenciar los cargos por transferencias realizadas de forma masiva de los individuales, todo lo anterior para la
     * generaci�n de la glosa variable en la cartola.</p>
     *
     *
     * Registro de versiones:<ul>
     * <li>1.0 (03/08/2010, Marcelo Fuentes H. (SEnTRA)): Versi�n inicial.
     * <li>1.1 (12/11/2010, Marcelo Fuentes H. (SEnTRA)): Se formatea el par�metro serial, que ser� enviado al servicio tuxedo,
     *                                                    con ceros a la izquierda.
     * <li>2.0 (08/02/2011, Marcelo Fuentes H. (SEnTRA)): Se modifica la firma del m�todo agregando nuevo par�metro traceNumber el cual
     *                                                    ahora es requerido por la transacci�n.
     * </ul>
     *
     * @param cuenta Cuenta Bci a la cual se le efectuar� el cargo
     * @param serial c�digo param�trico, utilizado internamente en la transacci�n
     * @param monto el monto a cargar
     * @param glosa valor para identificar una transferencia realizada
     * @param traceNumber identificador �nico de TTFF que permite a todos los bancos y a la CCA poder individualizar una operaci�n
     * @return Date fecha en la que se concret� el cargo si este se realiza, sino retorna null.
     * @throws TransferenciaException, RemoteException
     *
     * @since 1.9
     */
	public Date cargoCtaCtePorTransferenciaOTB(
			String cuenta,
			String serial,
			String monto,
			String glosa,
			String traceNumber) throws TuxedoException {

		sesion = joltPool.getSesion(ejbName);
		DataSet parametros = new DataSet();
		Result resultado = null;
		Date fecha_respuesta=null;
		SimpleDateFormat formatoDelTexto = new SimpleDateFormat("yyyyMMdd");

		if(logger.isDebugEnabled()){
			logger.debug("[cargoCtaCtePorTransferenciaOTB]:: cuenta ["+ cuenta +"]");
			logger.debug("[cargoCtaCtePorTransferenciaOTB]:: serial ["+ serial +"]");
			logger.debug("[cargoCtaCtePorTransferenciaOTB]:: monto ["+ monto +"]");
			logger.debug("[cargoCtaCtePorTransferenciaOTB]:: glosa ["+ glosa +"]");
			logger.debug("[cargoCtaCtePorTransferenciaOTB]:: traceNumber ["+ traceNumber +"]");
		}

		String prefijoCuenta=TablaValores.getValor(TTFF_PARAMETROS, "prefijoCuenta", "Desc");
		cuenta=prefijoCuenta+cuenta;
		String decimalesMonto=TablaValores.getValor(TTFF_PARAMETROS, "decimalesMonto", "Desc");
		monto=monto+decimalesMonto;

		parametros.setValue("cuenta", cuenta);
		parametros.setValue("serial", StringUtil.completaPorLaIzquierda(serial, 13, '0'));
		parametros.setValue("monto", monto);
		parametros.setValue("identificacion", traceNumber);
		parametros.setValue("glosa", StringUtil.completaPorLaIzquierda(glosa, 13, '0'));

		try {
			if(logger.isDebugEnabled()){logger.debug("[cargoCtaCtePorTransferenciaOTB]: Llamamos al servicio CtaCteCarOTB");}
			resultado = sesion.call("CtaCteCarOTB", parametros, null);
			if(logger.isDebugEnabled()){logger.debug("[cargoCtaCtePorTransferenciaOTB]: volvimos de CtaCteCarOTB");}
			String fecha_resp = (String)resultado.getValue("fecha",0, null);
			fecha_respuesta=formatoDelTexto.parse(fecha_resp);
			if(logger.isDebugEnabled()){logger.debug("[cargoCtaCtePorTransferenciaOTB]: fecha_respuesta ["+fecha_respuesta+"]");}

		}catch(Exception e){
			if(logger.isDebugEnabled()){logger.debug("[cargoCtaCtePorTransferenciaOTB]: ERROR [CtaCteCarOTB] (" + ErroresUtil.extraeStackTrace(e) + ")");}
		}

		return fecha_respuesta;
	}

/**
     * M�todo que confirma que el N�mero de verificaci�n ingresado por el cliente es id�ntico al almacenado por el banco. El m�todo
     * devuelve void ya que la objeto ({@link com.schema.util.dao.SybaseUtils}) que realiza la llamada al sp lanza excepciones de acuerdo al
     * error ocurrido. Estas pueden ser de dos tipos: 'error de comunicaci�n' o el 'id ingresado no corresponde al alias seleccionado'.
	 * El estado se modifica por el entregado como par�metro.
     *
     * Registro de Versiones:<ul>
     *
     * <li>1.0 08/03/2011 Magdalena D�az R.(Imagemaker IT): - Versi�n inicial
     *
     * </ul>
     *
     *
     * @param code: Clave Primaria de la tabla ali
     * @param id: identificador de confirmaci�n del alias
	 * @param estado: estado del alias de Transferencia
     * @throws BusinessException, Exception, com.schema.util.GeneralException
     * @since 1.13
     */
    public void modificarEstadoAliasTTFF(long code, String id, char estado) throws BusinessException, Exception, com.schema.util.GeneralException {
        envInit();
        AliasTTFFDVO aliasTTFFDvo = new AliasTTFFDVO();
        aliasTTFFDvo.setCodigo(code);
        aliasTTFFDvo.setIdValidacion(id);
        aliasTTFFDvo.setEstado(estado);
        ttffDAO.modificarEstadoAliasTTFF(aliasTTFFDvo);
    }

 /**
     * <p>Agrega la infomaci�n del encabezado y respectivo detalle de la transferencia en linea. Se trae m�todo desde {@link wcorp.bprocess.cuentas.TransferenciasFondosBean#} a la implementaci�n Stateless del mismo. Se reemplazan los par�metros por un TO.</p>
     * Registro de versiones:
     * <ul>
     * <li>1.0  (16-02-2011 Freddy Monsalve M. (OS.ONE), Carlos Garrido G. (OS.ONE)) : versi�n inicial.</li>
     * </ul>
     * @param empaqueTransferencia TO que encapsula los datos de necesarios para registrar el encabezado y detalle de una transferencia en l�nea.
     * @return ResultAgregaTransferenciaEncabezadoDetalle : Detalle de la inserci�n del encabezado y detalle de la transferencia.
     * @throws TuxedoException
     * @throws BCIExpressException
     * @throws GeneralException
     * @since 1.14
     */
    public ResultAgregaTransferenciaEncabezadoDetalle agregaTransferenciaEncabezadoDetalleEnLinea(TransferenciaATercerosTO empaqueTransferencia) throws TuxedoException, BCIExpressException,
            GeneralException {

        ServletSessionPool sesion = joltPool.getSesion(ejbName);
        DataSet parametros = new DataSet();
        Result resultado = null;

        try {

            String rutUsuario = StringUtil.completaPorLaIzquierda(
                    Long.toString(empaqueTransferencia.getRutUsuario()), 8, '0');
            char dvUsuario = empaqueTransferencia.getDigitoVerifUsu();
            String rutEmpresa = StringUtil.completaPorLaIzquierda(
                    Long.toString(empaqueTransferencia.getRutEmpresa()), 8, '0');
            char dvEmpresa = empaqueTransferencia.getDigitoVerifEmp();
            String rutDestin = StringUtil.completaPorLaIzquierda(
                    Long.toString(empaqueTransferencia.getRutDestin()), 8, '0');
            char dVerificadorDestinatario = empaqueTransferencia.getDigitoVerificador();
            String[] claves = empaqueTransferencia.getClaves();

            logger.debug("rutUsuario: '" + rutUsuario + "'");
            parametros.setValue("rutUsuario", rutUsuario);

            logger.debug("digitoVerifUsu: '" + String.valueOf(dvUsuario) + "'");
            parametros.setValue("digitoVerifUsu", String.valueOf(dvUsuario));

            logger.debug("rutEmpresa: '" + rutEmpresa + "'");
            parametros.setValue("rutEmpresa", rutEmpresa);

            logger.debug("digitoVerifEmp: '" + String.valueOf(dvEmpresa) + "'");
            parametros.setValue("digitoVerifEmp", String.valueOf(dvEmpresa));

            logger.debug("numConvenio: '" + empaqueTransferencia.getNumConvenio() + "'");
            parametros.setValue("numConvenio", empaqueTransferencia.getNumConvenio());

            logger.debug("ctaOrigen: '" + empaqueTransferencia.getCtaOrigen() + "'");
            parametros.setValue("ctaOrigen", empaqueTransferencia.getCtaOrigen());

            logger.debug("glosa: '" + empaqueTransferencia.getGlosa() + "'");
            parametros.setValue("glosa", empaqueTransferencia.getGlosa());

            logger.debug("ctaDestino: '" + empaqueTransferencia.getCtaDestino() + "'");
            parametros.setValue("ctaDestino", empaqueTransferencia.getCtaDestino());

            logger.debug("montoTransfer: '" + Long.toString(empaqueTransferencia.getMontoTransfer()) + "'");
            parametros.setValue("montoTransfer", Long.toString(empaqueTransferencia.getMontoTransfer()));

            logger.debug("tipoPago: '" + empaqueTransferencia.getTipoPago() + "'");
            parametros.setValue("tipoPago", empaqueTransferencia.getTipoPago());

            logger.debug("glosaEvento: '" + empaqueTransferencia.getGlosaEvento() + "'");
            parametros.setValue("glosaEvento", empaqueTransferencia.getGlosaEvento());

            logger.debug("glosaMov: '" + empaqueTransferencia.getGlosaMov() + "'");
            parametros.setValue("glosaMov", empaqueTransferencia.getGlosaMov());

            logger.debug("codBanco1: '" + empaqueTransferencia.getCodBanco1() + "'");
            parametros.setValue("codBanco1", empaqueTransferencia.getCodBanco1());

            logger.debug("rutDestin: '" + rutDestin + "'");
            parametros.setValue("rutDestin", rutDestin);

            logger.debug("digitoVerificador: '" + String.valueOf(dVerificadorDestinatario) + "'");
            parametros.setValue("digitoVerificador", String.valueOf(dVerificadorDestinatario));

            logger.debug("nombreDestin: '" + empaqueTransferencia.getNombreDestin() + "'");
            parametros.setValue("nombreDestin", empaqueTransferencia.getNombreDestin());

            logger.debug("codigoServicio: '" + empaqueTransferencia.getCodigoServicio() + "'");
            parametros.setValue("codigoServicio", empaqueTransferencia.getCodigoServicio());

            logger.debug("identificador: '" + empaqueTransferencia.getIdentificador() + "'");
            parametros.setValue("identificador", empaqueTransferencia.getIdentificador());

            logger.debug("cantidadFirmas: '" + empaqueTransferencia.getCantidadFirmas() + "'");
            parametros.setValue("numero", String.valueOf(empaqueTransferencia.getCantidadFirmas()));

            for (int i = 0; i < claves.length; i++) {
                int suma = i + 1;
                logger.debug("clave" + suma + "     [" + claves[i] + "]");
                if (claves[i] == null)
                    claves[i] = "";
                parametros.setValue("firmaTRF_" + suma, claves[i].trim());
            }

            logger.debug("email: '" + empaqueTransferencia.getEmail() + "'");
            parametros.setValue("email", empaqueTransferencia.getEmail());

            logger.debug("tmp: '" + empaqueTransferencia.getTmp() + "'");
            parametros.setValue("item", empaqueTransferencia.getTmp());

            logger.debug("tipoTrf: '" + empaqueTransferencia.getTipoTrf() + "'");
            parametros.setValue("tipo", empaqueTransferencia.getTipoTrf());

        }
        catch (Exception e) {
            logger.debug("Ha ocurrido un error colocando parametros.  El error es " + e);
        }

        logger.debug("call CtaIngTraLinEncDet");

        try {
            logger.debug("Banco: '" + parametros.get("codBanco1") + "'");
            logger.debug("Servicio: '" + parametros.get("codigoServicio") + "'");
            logger.debug("Cta Destino: '" + parametros.get("ctaDestino") + "'");
            logger.debug("Cta Origen: '" + parametros.get("ctaOrigen") + "'");
            logger.debug("DV Empresa: '" + parametros.get("digitoVerifEmp") + "'");
            logger.debug("DV Usuario: '" + parametros.get("digitoVerifUsu") + "'");
            logger.debug("Digito Verificador: '" + parametros.get("digitoVerificador") + "'");
            logger.debug("Glosa: '" + parametros.get("glosa") + "'");
            logger.debug("Monto: '" + parametros.get("montoTransfer") + "'");
            logger.debug("N Destino: '" + parametros.get("nombreDestin") + "'");
            logger.debug("Convenio: '" + parametros.get("numConvenio") + "'");

            logger.debug("Rut Destino: '" + parametros.get("rutDestin") + "'");
            logger.debug("Rut Empresa: '" + parametros.get("rutEmpresa") + "'");
            logger.debug("Rut Usuario: '" + parametros.get("rutUsuario") + "'");
            logger.debug("Tipo Tranferencia: '" + parametros.get("tipo") + "'");

            resultado = sesion.call("CtaIngTraLinEncDet", parametros, null);

            logger.debug("llenando ResultAgregaTransferenciaEncabezadoDetalleEnLinea");
            logger.debug("Inicializa cant = 0 ");

            int cant = 0;

            try {
                if (resultado.getValue("cantidad", 0, null) != null) {
                    cant = Integer.parseInt(((String) resultado.getValue("cantidad", 0, "0")).trim(), 10);
                }
            }
            catch (Exception ignored) {
            }

            logger.debug("cant : " + Integer.toString(cant));

            ResultAgregaTransferenciaEncabezadoDetalle data = new ResultAgregaTransferenciaEncabezadoDetalle(cant);

            for (int i = 0; i < cant; i++) {

                try {
                    if (resultado.getValue("identificacion", i, null) == null
                            || ((String) resultado.getValue("identificacion", i, null)).trim().equals("")) {
                        break;
                    }
                }
                catch (Exception e) {
                    break;
                }

                logger.debug("agrTraEncDet.identificacion[" + Integer.toString(i) + "]: '"
                        + (String) resultado.getValue("identificacion", i, null) + "' (String)");

            }

            logger.debug("llenando 'cim_status' ...");
            data.setCim_status((String) resultado.getValue("codRespuesta", 0, null));

            logger.debug("llenando 'cim_mensaje' ...");
            data.setCim_mensaje((String) resultado.getValue("mensaje", 0, null));

            for (int i = 0; i < cant; i++) {

                try {
                    if (resultado.getValue("identificacion", i, null) == null
                            || ((String) resultado.getValue("identificacion", i, null)).trim().equals("")) {
                        logger.debug("saliendo del llenado de agrTraEncDet");
                        break;
                    }
                }
                catch (Exception e) {
                    logger.debug("saliendo del llenado de agrTraEncDet (e)");
                    break;
                }

                logger.debug("llenando agrTraEncDet[" + Integer.toString(i) + "] ...");
                data.setAgregaTransfEncabDetalle(
                        new AgregaTransfEncabDetalle((String) resultado.getValue("identificacion", i, null)), i);
            }

            return data;

        }
        catch (ApplicationException e) {

            resultado = e.getResult();

            if (resultado.getApplicationCode() == 0) {
                throw new BCIExpressException((String) resultado.getValue("codigoError", 0, "DESC"),
                        (String) resultado.getValue("descripcionError", 0, "[Sin Informacion]"));
            }
            else {
                throw new wcorp.util.com.TuxedoException((String) (resultado.getApplicationCode() == 1
                        ? resultado.getValue("codigoError", 0, "DESC")
                        : "TUX"), (String) resultado.getValue("descripcionError", 0, "[Sin Informacion]"));
            }
        }
        catch (Exception e) {
            logger.debug("Exception " + e.getMessage());
            throw new BCIExpressException("ESPECIAL", e.getMessage());
        }
    }

	/**
	 * <P>M�todo encargado de consultar las transferencias de fondos realizadas por el cliente Empresa para una cuenta
	 * de origen en particular o para todas las cuentas asociadas al cliente y en un intervalo de tiempo dado permitiendo
	 * realizar consultas por bloque, es decir el par�metro de entrada "cantidad de registros" indica cuantos registros se desean recibir del total y
	 * el par�metro bloque en que "p�gina de resultado" se est� con respecto al total.</P>
     * <p>
	 *  <b>Registro de versiones: </b>
	 *  <ul>
	 *   		<li>1.0 (27/05/2010, Cristian Recabarren R. (Sermaluc)): versi�n inicial.</li>
	 *  </ul>
	 * </p>
	 * @param rut Rut del cliente.
	 * @param dv Digito Verificador del cliente.
	 * @param periodo mes que se desea consultar.
	 * @param bloque bloque que se esta consultando.
	 * @param cuenta cuenta del cliente que se desea consultar.
	 * @param cantidadRegistros cantidad de registros a consultar.
	 * @return transferencias realizadas por el cliente empresa
	 * @throws Exception
	 * @since 1.15
	 */
	public TransferenciasRecibidasVO[] consultaTransferenciasEnviadasEmpresa(long rut, char dv, Date periodo, String cuenta, int bloque, int cantidadRegistros)
	throws RemoteException, GeneralException, Exception{
		TransferenciasRecibidasVO[] transferenciasEmpresaVO = null;
		try{
			TransferenciasFondosDAO transferenciasFondosDAO = new TransferenciasFondosDAO();
			transferenciasEmpresaVO = transferenciasFondosDAO.consultaTransferenciasEnviadasEmpresa(rut, dv, periodo, cuenta, bloque, cantidadRegistros);
		}catch(Exception e){
			logger.debug("[consultaTransferenciasEnviadasEmpresa]:: Problemas al realizar la consulta, Exception["+e+"]");
			throw e;

		}

		return transferenciasEmpresaVO;

	}


	/**
	 * <P>M�todo encargado de consultar las transferencias de fondos realizadas por el cliente y que esten diferidas (Transferencia diferida es
	 * aquella transferencias que se ejecuta el d�a 1, pero los fondos se abonan el d�a 2) para una cuenta de origen en particular o para todas las
	 * cuentas asociadas al cliente y en un intervalo de tiempo dado permitiendo realizar consultas por bloque, es decir el par�metro de entrada
	 * "cantidad de registros" indica cuantos registros se desean recibir del total y el par�metro bloque en que "p�gina de resultado" se est�
	 * con respecto al total.</P>
     * <p>
	 *  <b>Registro de versiones: </b>
	 *  <ul>
	 *   		<li>1.0 (02/06/2010, Cristian Recabarren R. (Sermaluc)): versi�n inicial.</li>
	 *  </ul>
	 * </p>
	 * @param rut Rut del cliente.
	 * @param dv Digito Verificador del cliente.
	 * @param periodo mes que se desea consultar.
	 * @param bloque bloque que se esta consultando.
	 * @param cuenta cuenta del cliente que se desea consultar.
	 * @param cantidadRegistros cantidad de registros a consultar.
	 * @return transferencias diferidas realizadas por el cliente
	 * @throws Exception
	 * @since 1.15
	 */
	public TransferenciasRecibidasVO[] consultaTransferenciasDiferidasEnviadas(long rut, char dv, Date periodo, String cuenta, int bloque, int cantidadRegistros)
	throws RemoteException, GeneralException, Exception{
		TransferenciasRecibidasVO[] transferenciasDiferidasVO = null;
		try{
			TransferenciasFondosDAO transferenciasFondosDAO = new TransferenciasFondosDAO();
			transferenciasDiferidasVO = transferenciasFondosDAO.consultaTransferenciasDiferidasEnviadas(rut, dv, periodo, cuenta, bloque, cantidadRegistros);
		}catch(Exception e){
			logger.debug("[consultaTransferenciasDiferidasEnviadas]:: Problemas al realizar la consulta, Exception["+e+"]");
			throw e;

		}

		return transferenciasDiferidasVO;

	}

	/**
	 * <P>M�todo encargado de consultar las transferencias de fondos realizadas por el cliente Empresa para una cuenta
	 * de origen en particular o para todas las cuentas asociadas al cliente y en un intervalo de tiempo dado permitiendo
	 * realizar consultas por bloque, es decir el par�metro de entrada "cantidad de registros" indica cuantos registros se desean recibir del total y
	 * el par�metro bloque en que "p�gina de resultado" se est� con respecto al total.</P>
     * <p>
	 *  <b>Registro de versiones: </b>
	 *  <ul>
	 *   		<li>1.0 (07/06/2010, Cristian Recabarren R. (Sermaluc)): versi�n inicial.</li>
	 *  </ul>
	 * </p>
	 * @param rut Rut del cliente.
	 * @param dv Digito Verificador del cliente.
	 * @param periodo mes que se desea consultar.
	 * @param bloque bloque que se esta consultando.
	 * @param cuenta cuenta del cliente que se desea consultar.
	 * @param cantidadRegistros cantidad de registros a consultar.
	 * @return transferencias realizadas por el cliente empresa
	 * @throws Exception
	 * @since 1.15
	 */
	public TransferenciasRecibidasVO[] consultaTransferenciasEmpresaEnviadas(long rut, char dv, Date periodo, String cuenta, int bloque, int cantidadRegistros)
	throws RemoteException, GeneralException, Exception{
		TransferenciasRecibidasVO[] transferenciasEmpresasEnviadasVO = null;
		try{
			TransferenciasFondosDAO transferenciasFondosDAO = new TransferenciasFondosDAO();
			transferenciasEmpresasEnviadasVO = transferenciasFondosDAO.consultaTransferenciasEmpresaEnviadas(rut, dv, periodo, cuenta, bloque, cantidadRegistros);
		}catch(Exception e){
			logger.debug("[consultaTransferenciasDiferidasEnviadas]:: Problemas al realizar la consulta, Exception["+e+"]");
			throw e;

		}

		return transferenciasEmpresasEnviadasVO;

	}

    /**
     * <p>Se clona el m�todo {@link #transferenciaCtaCteOtroBancoEnLineaEmpresa(String, String, String, String, String, long, char, String, String, String, String, String, String, String, Cliente, String)} para agregar la implementaci�n de env�o de correo electr�nico y mensaje para el destinatario de la transferencia. Se reemplazan los n par�metros por un TO.
     * Transfiere fondos desde cuenta corriente Empresa hacia cuenta corriente de otro Banco en Linea.</p>
     * Registro de versiones:
     * <ul>
     * <li>1.0  (04-08-2011 Manuel Hern�ndez A. (OS.ONE), Carlos Garrido G. (OS.ONE)) : versi�n inicial.</li>
     * <li>1.1 15/12/2011, Carlos Cerda I. (Orand): Se a�ade la l�gica de consultar el estado de la transferencia
     * en el operador en caso de una excepci�n.
     * <li>1.2 07/02/2012, Carlos Cerda I. (Orand): Se cambia la forma de llamar a los m�todos
     * {@link #enviarTrfOperador(Cliente, String, String, String)} y
     * {@link #construyeXFML(Cliente, String, String, String, String, double, String, Date, String, String, long,
     * char, String, String, long, char, String, String, String, String, String)}
     * <li>1.3 06/02/2013, Marcelo Fuentes H. (SEnTRA): Se agrega l�gica para insertar una consulta de
     * transferencias de fondos al sistema StoreAndForward para cuando falla la consulta por un error en el env�o a
     * CCA. Adem�s se modifica el c�digo de excepci�n devuelto, del TRF001 al TRF057, para todos los casos antes
     * del env�o a la CCA. De esta forma si el c�digo devuelto es el TRF057 se debe eliminar la �ltima firma
     * para que el cliente pueda reintentar la transferencia. Adem�s se modifica el estado de la transferencia
     * antes de enviarla a CCA.</li>
     * <li>1.4 06/02/2013, Venancio Ar�nguiz. (SEnTRA): Se modifica la llamada al m�todo
     * {@link #cambiaEstadoTransferenciaEnLinea(String, char, String, String, String, String, String, String,
     *  String)} ya que se migr� a persistencia de datos. Adem�s se modifica logueo del m�todo.</li>
     * <li>1.5 27/11/2013, Venancio Ar�nguiz. (SEnTRA): Se modifica cambio de estado para los casos en que CCA
     * responda NOKSISTEMA o NOKCLIENTE dejando la transferencia con estado REC.</li>
     * <li>1.6 17/07/2014, Oliver Hidalgo Leal. (BCI): Se controla caida en el envio mail.</li>
     * <li>1.7 10/12/2014, Claudia Carrasco H. (SEnTRA): Se evalua el retorno obtenido desde la CCA para 
     * determinar si el error es por causa del banco de destino. De ser asi se mostrara el mensaje al cliente 
     * indicando dicha situacion.</li> 
     * <li>1,8 06/08/2015 Carlos Cerda I. (Kubos): Debido al cambio en la firma del m�todo 
     * {@link MessageAuthenticationCodeHelper#validaMessageAuthenticationCode(String, String, String, String, 
     * String, String, String, String, String, String, String, String, String)} de boolean a int, se modifica su 
     * llamado.
     * <li>1.9 02/09/2016 Christoper Escudero (Imagemaker) - Minheli Mejias (BCI) : Se modifica metodo 
	 * {@link #registraTransaccionPendiente()}, en el momento de caer al catch e ingresar al metodo 
	 * se realiza un substring a la variable cuentaOrigen quitando los 
	 * ceros a la izquierda del numero de cuenta. 
     * <li>1.10 30/11/2016 Andr�s Pardo T. (SEnTRA) - Marcelo Avenda�o. (Ing. Soft. BCI): Se incluye cambio de estado cuando la MAC es incorrecta o la respuesta de CCA no esta definida.
	 * </ul>
     * @param detalleTransferencia    : TO con los datos de la transferencia.
     * @return CCADirectaVO           : Detalle del resultado del envio de datos al centro de compensaci�n de datos (CCA).
     * @throws TransferenciaException
     * @throws RemoteException
     * @since 1.16
     * @see {@link #transferenciaCtaCteOtroBancoEnLineaEmpresa(String, String, String, String, String, long, char, String, String, String, String, String, String, String, Cliente, String)}
     */
    public CCADirectaVO transferenciaCtaCteOtroBancoEnLineaEmpresaConEnvioDeMail(DetalleTransferencia detalleTransferencia) throws TransferenciaException, RemoteException{

        Cliente cliente = detalleTransferencia.getDatosDelCliente();
        String canal = detalleTransferencia.getCanalID();
        if (logger.isDebugEnabled() ) {
            logger.debug("detalle Transferencia: " + detalleTransferencia.toString());
        }

        double monto = detalleTransferencia.getMontoTransferencia();
        String montoTransferir = String.valueOf(monto);
        String numeroOperacion = detalleTransferencia.getNumeroOperacion();
        String cuentaOrigen= detalleTransferencia.getCtaOrigen();
        String codigoBancoDestino = detalleTransferencia.getCodigoBancoDestino();
        char dvDestino = detalleTransferencia.getDigDestino();
        String cuentaDestino = detalleTransferencia.getCtaDestino();
        long rutDestino = detalleTransferencia.getRutDestino();
        String convenio = detalleTransferencia.getNumeroConvenio();
        String tipoCuentaOrigen = detalleTransferencia.getTipoCuentaOrigen();
        String tipoCuentaDestino = detalleTransferencia.getTipoCuentaDestino();
        String nombreDestino = detalleTransferencia.getNombreDestino();
        String comentario = detalleTransferencia.getComentario();
        long rutUsuario = detalleTransferencia.getRutUsuario();

        String cambiaEstadoTTFF = TablaValores.getValor(TTFF_CONFIG, "CambiaEstadoTTFF", "Desc");
        if (logger.isInfoEnabled() ) {
            logger.info("MAILDESTINO Cliente:" + cliente.getFullRut() + "" +
                    " Canal:" + canal +
                    "[transferenciaCtaCteOtroBancoEnLineaEmpresaConEnvioDeMail]" +
                    "[ " + numeroOperacion + " ]cambiaEstadoTTFF [" + cambiaEstadoTTFF + "]");
        }

        if (logger.isInfoEnabled() ) {
            logger.info(
                " MAILDESTINO Cliente:"+cliente.getFullRut()+"" +
                " Canal:"+canal+
                "[transferenciaCtaCteOtroBancoEnLineaEmpresaConEnvioDeMail]"+
                "[ "+numeroOperacion +" ]monto              ["+monto+"]");
        }

        String producto = TablaValores.getValor(STORE_FORWARD_CONFIG, "TransferenciasEmpresas", "producto");

        if (logger.isInfoEnabled() ) {
            logger.info(
                " MAILDESTINO Cliente:"+cliente.getFullRut()+"" +
                " Canal:"+canal+
                "[transferenciaCtaCteOtroBancoEnLineaEmpresaConEnvioDeMail]"+
                            "[ " + numeroOperacion + " ]Producto [" + producto + "]");
        }

        int intentos = Integer.parseInt(TablaValores.getValor(STORE_FORWARD_CONFIG, "TransferenciasEmpresas", "intentos"));

        if (logger.isInfoEnabled() ) {
            logger.info(
                " MAILDESTINO Cliente:"+cliente.getFullRut()+"" +
                " Canal:"+canal+
                "[transferenciaCtaCteOtroBancoEnLineaEmpresaConEnvioDeMail]"+
                            "[ "+ numeroOperacion + " ]Re-Intentos [" + intentos + "]");
        }

        if (logger.isInfoEnabled() ) {
            logger.info("[transferenciaCtaCteOtroBancoEnLineaEmpresaConEnvioDeMail]"
                + "[ "+ numeroOperacion + " ]Se obtiene la Fecha de Cargo");
        }

        Date fecha = null;
        String hora = new String();
        try {
            fecha = new Date();
            hora = new SimpleDateFormat("hhmmss").format(fecha);
            if (logger.isInfoEnabled() ) {
                logger.info(
                    " MAILDESTINO Cliente:"+cliente.getFullRut()+"" +
                    " Canal:"+canal+
                    "[transferenciaCtaCteOtroBancoEnLineaEmpresaConEnvioDeMail]"+
                    "[ "+numeroOperacion+" ]FECHA CARGO=" + fecha);
            }
        } catch (Exception e) {
            if (logger.isInfoEnabled() ) {
                logger.info(
                    " MAILDESTINO Cliente:"+cliente.getFullRut()+"" +
                    " Canal:"+canal+
                    "[transferenciaCtaCteOtroBancoEnLineaEmpresaConEnvioDeMail]"+
                    "[ "+numeroOperacion+" ]Exception en FECHA CARGO=" + ErroresUtil.extraeStackTrace(e));
            }
        }

        if (logger.isInfoEnabled() ) {
            logger.info("[transferenciaCtaCteOtroBancoEnLineaEmpresaConEnvioDeMail]"
                    + "[ "+ numeroOperacion + " ]Se obtiene el Trace Number");
        }
        String traceNumber = new String();
        try {
            traceNumber = StringUtil.rellenaPorLaIzquierda(getTraceNumber(), 12, '0');
            if (logger.isInfoEnabled() ) {
                logger.info(
                    " MAILDESTINO Cliente:"+cliente.getFullRut()+"" +
                    " Canal:"+canal+
                    "[transferenciaCtaCteOtroBancoEnLineaEmpresaConEnvioDeMail]"+
                    "[ "+numeroOperacion+" ]traceNumber        [" + traceNumber + "]");
            }
        } catch (Exception e) {
            if (logger.isInfoEnabled() ) {
                logger.info(
                    " MAILDESTINO Cliente:"+cliente.getFullRut()+"" +
                    " Canal:"+canal+
                    "[transferenciaCtaCteOtroBancoEnLineaEmpresaConEnvioDeMail]"+
                    "[ "+numeroOperacion+" ]Exception al obtener Trace Number SE ABORTA!!!!: "+ ErroresUtil.extraeStackTrace(e));
            }
            throw new TransferenciaException("TRF057");
        }
        if (logger.isInfoEnabled() ) {
            logger.info(
                " MAILDESTINO Cliente:"+cliente.getFullRut()+"" +
                " Canal:"+canal+
                "[transferenciaCtaCteOtroBancoEnLineaEmpresaConEnvioDeMail]"+
                "[ "+numeroOperacion+" ]traceNumber=" + traceNumber);
        }

        if (logger.isInfoEnabled() ) {
            logger.info("[transferenciaCtaCteOtroBancoEnLineaEmpresaConEnvioDeMail]"
                    + "[ " + numeroOperacion + " ]Se genera MAC");
        }

        String macOrigen = new String();
        boolean operarMAC = new Boolean(TablaValores.getValor(TTFF_CONFIG, "MAC", "operar")).booleanValue();
        if (logger.isInfoEnabled() ) {
            logger.info(
                " MAILDESTINO Cliente:"+cliente.getFullRut()+"" +
                " Canal:"+canal+
                "[transferenciaCtaCteOtroBancoEnLineaEmpresaConEnvioDeMail]"+
                    "[ " + numeroOperacion + " ]operarMAC=" + operarMAC);
        }
        MessageAuthenticationCodeHelper messageAuthenticationCodeHelper = null;
        if(operarMAC){
            try {
                messageAuthenticationCodeHelper = new MessageAuthenticationCodeHelper(ejbName);
                macOrigen = messageAuthenticationCodeHelper.generaMessageAuthenticationCode(
                        String.valueOf(cliente.getRut())+cliente.getDigito(),
                        cuentaOrigen,
                        codigoBancoBCI,
                        codigoBancoDestino,
                        String.valueOf(rutDestino)+dvDestino,
                        cuentaDestino,
                        String.valueOf((long)monto),
                        traceNumber,
                        null,
                        null,
                        ORIGEN);
            } catch (Exception e){
                if (logger.isInfoEnabled() ) {
                    logger.info(
                        " MAILDESTINO Cliente:"+cliente.getFullRut()+"" +
                        " Canal:"+canal+
                        "[transferenciaCtaCteOtroBancoEnLineaEmpresaConEnvioDeMail]"+
                                    "[ " + numeroOperacion + " ]operarMAC=" + operarMAC);
                }
                if (logger.isInfoEnabled() ) {
                    logger.info(
                        " MAILDESTINO Cliente:"+cliente.getFullRut()+"" +
                        " Canal:"+canal+
                        "[transferenciaCtaCteOtroBancoEnLineaEmpresaConEnvioDeMail]"+
                                    "[ " + numeroOperacion + " ]Exception al generar MAC SE ABORTA!!!!: "
                        + ErroresUtil.extraeStackTrace(e));
                }
                throw new TransferenciaException("TRF057");
            }
        }

        if (logger.isInfoEnabled() ) {
            logger.info("[transferenciaCtaCteOtroBancoEnLineaEmpresaConEnvioDeMail]"
                    + "[ " + numeroOperacion + " ]Se Realizar� el Cargo");
        }

        boolean insertoOk = false;
        boolean reversaBanco = false;

            try {
                if(logger.isInfoEnabled())
                {
                logger.info("realizarTransferenciaCtaCteHaciaCtaCte[ " + numeroOperacion + " ]:cliente["
                    + cliente.toString() + "], canal[" + canal + "], cuentaOrigen[" + cuentaOrigen
                    + "], traceNumber[" + traceNumber + "], monto[" + monto + "]");
                }
    			realizarTransferenciaCtaCteHaciaCtaCte(cliente, canal, cuentaOrigen, traceNumber, monto);
            if (logger.isInfoEnabled() ) {
                logger.info("[transferenciaCtaCteOtroBancoEnLineaEmpresaConEnvioDeMail]"
                        + "[ " + numeroOperacion + " ]Cargo realizado correctamente.");
            }
        }catch(Exception e){
            logger.info("Error al realizar el Cargo");
            if (logger.isEnabledFor(Level.ERROR) ) {
                logger.error(
                    " MAILDESTINO Cliente:"+cliente.getFullRut()+"" +
                    " Canal:"+canal+
                    "[transferenciaCtaCteOtroBancoEnLineaEmpresaConEnvioDeMail]"+
                    "[ "+numeroOperacion+" ]Exception al realizarTransferenciaCtaCteHaciaCtaCte SE ABORTA!!!!: "+e.toString());
            }
            try{
                realizaReversaCargo(cliente,canal,cuentaOrigen,traceNumber,monto);
            }catch(Exception ex){
                if (logger.isInfoEnabled() ) {
                    logger.info(
                        " MAILDESTINO Cliente:"+cliente.getFullRut()+"" +
                        " Canal:"+canal+
                        "[transferenciaCtaCteOtroBancoEnLineaEmpresaConEnvioDeMail]"+
                            "[ " + numeroOperacion + " ]Exception reversa Cargo: "
                            + ErroresUtil.extraeStackTrace(ex));
                }
                try {
                insertoOk = registraTransaccionPendiente(cliente,
                        canal,
                        ex,
                        TablaValores.getValor(TTFF_CONFIG, "Trx", "valor"),
                        cuentaOrigen.substring(CANTIDAD_CARACTERES_A_ELIMINAR, LARGO_CUENTA),
                        null,
                        monto,
                        traceNumber,
                        producto,
                        convenio,
                        intentos,
                        false,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null);
                if (logger.isInfoEnabled() ) {
                        logger.info("MAILDESTINO Cliente:" + cliente.getFullRut() + "" + " Canal:" + canal
                                    + "[transferenciaCtaCteOtroBancoEnLineaEmpresaConEnvioDeMail]"
                                    + "[ " + numeroOperacion + " ]registr� transaccion pendiente cargo: "
                                    + insertoOk);
                    }
                }
                catch(Exception rtp) {
                    if (logger.isEnabledFor(Level.ERROR) ) {
                        logger.error("Cliente:" + cliente.getFullRut() + "" + " Canal:" + canal
                                     + "[transferenciaCtaCteOtroBancoEnLineaEmpresaConEnvioDeMail]"
                                     + "[ " + numeroOperacion + " ]Error al registrar transaccion pendiente cargo:"
                                     + rtp.getMessage());
                    }
                }
            }
            throw new TransferenciaException("TRF057");
        }

        if(cambiaEstadoTTFF.equalsIgnoreCase("SI")){
            String estadoEncabezado = TablaValores.getValor(TTFF_CONFIG,
                                                            "CambiaEstadoTTFF", "NuevoEstadoEncabezado");
            if (logger.isInfoEnabled() ) {
                logger.info("Cliente:" + cliente.getFullRut() + " Canal:" + canal
                        + "[transferenciaCtaCteOtroBancoEnLineaEmpresaConEnvioDeMail]"
                        + "[ " + numeroOperacion + " ] antes de cambiar estado a estadoEncabezado ["
                        + estadoEncabezado + "]");
            }

            try {
                ResultCambiaEstadoTransferencia cambiaEstado = null;
                cambiaEstado = cambiaEstadoTransferenciaEnLinea(String.valueOf(cliente.getRut()),
                        convenio,
                        numeroOperacion,
                        estadoEncabezado,
                        "",
                        "");

                if (logger.isInfoEnabled() ) {
                    logger.info("Cliente:" + cliente.getFullRut() + "" + " Canal:" + canal +
                            "[transferenciaCtaCteOtroBancoEnLineaEmpresaConEnvioDeMail]" +
                            "[ " + numeroOperacion + " ]cambio Estado : [" + cambiaEstado.getCim_status() + "]");
                }
            } catch (Exception e) {
                if (logger.isEnabledFor(Level.ERROR)) {
                    logger.error("Cliente:" + cliente.getFullRut() + "" + " Canal:" + canal +
                                "[transferenciaCtaCteOtroBancoEnLineaEmpresaConEnvioDeMail]" +
                            "[ " + numeroOperacion + " ]Problemas al cambiar el estado["
                            + ErroresUtil.extraeStackTrace(e) + "]");
                }
                try{
                    realizaReversaCargo(cliente,canal,cuentaOrigen,traceNumber,monto);
                }catch(Exception ex){
                    if (logger.isInfoEnabled() ) {
                    logger.info(
                        " MAILDESTINO Cliente:"+cliente.getFullRut()+"" +
                        " Canal:"+canal+
                        "[transferenciaCtaCteOtroBancoEnLineaEmpresaConEnvioDeMail]"+
                                        "[ " + numeroOperacion + " ]Exception reversa Cargo: "
                                        + ErroresUtil.extraeStackTrace(ex));
                    }
                    try {
                        insertoOk = registraTransaccionPendiente(cliente,
                            canal,
                            ex,
                            TablaValores.getValor(TTFF_CONFIG, "Trx", "valor"),
                            cuentaOrigen.substring(CANTIDAD_CARACTERES_A_ELIMINAR, LARGO_CUENTA),
                            null,
                            monto,
                            traceNumber,
                            producto,
                            convenio,
                            intentos,
                            false,
                            null,
                            null,
                            null,
                            null,
                            null,
                            null);
                        if (logger.isInfoEnabled() ) {
                            logger.info("MAILDESTINO Cliente:" + cliente.getFullRut() + "" + " Canal:" + canal
                                        + "[transferenciaCtaCteOtroBancoEnLineaEmpresaConEnvioDeMail]"
                                        + "[ " + numeroOperacion + " ]registr� transaccion pendiente cargo: "
                                        + insertoOk);
                        }
                    }
                    catch(Exception rtp) {
                        if (logger.isEnabledFor(Level.ERROR) ) {
                            logger.error("Cliente:" + cliente.getFullRut() + "" + " Canal:" + canal
                                         + "[transferenciaCtaCteOtroBancoEnLineaEmpresaConEnvioDeMail]"
                                         + "[ " + numeroOperacion + " ]Error al registrar transaccion pendiente "
                                         + "cargo:" + rtp.getMessage());
                        }
                    }
                }
                throw new TransferenciaException("TRF057");
            }
        }

        if (logger.isInfoEnabled() ) {
            logger.info("[transferenciaCtaCteOtroBancoEnLineaEmpresaConEnvioDeMail]"
                    + "[ " + numeroOperacion + " ]Se Realiza el Env�o a la CCA");
        }

        CCADirectaVO detalleOperador = null;
        String xfml = "";
        try{
            if (logger.isInfoEnabled() ) {
                logger.info("Se enviar� al CCA la transferencia: enviarTrfOperador (...) "
                        +"macOrigen["+macOrigen+"] traceNumber["+traceNumber+"] id["+numeroOperacion+"]");
            }

            xfml = construyeXFML(cliente,
                    canal,
                    null,
                    tipoCuentaOrigen,
                    tipoCuentaDestino,
                    monto,
                        traceNumber,
                        fecha,
                    hora,
                    cuentaOrigen,
                    cliente.getRut(),
                    cliente.getDigito(),
                    codigoBancoDestino,
                    cuentaDestino,
                    rutDestino,
                    dvDestino,
                    nombreDestino,
                    comentario,
                    numeroOperacion,
                    macOrigen);

            detalleOperador = enviarTrfOperador(cliente, canal, macOrigen, xfml, SERV_TUX_ABONO_CCA);

            if (logger.isInfoEnabled() ) {
                logger.info("[transferenciaCtaCteOtroBancoEnLineaEmpresaConEnvioDeMail]"
                        + "[ " + numeroOperacion + " ]El env�o de la ttff al CCA fu� exitoso");
            }
        }
        catch (Exception e) {
            if (logger.isInfoEnabled() ) {
                logger.info("Fall� env�oal CCA la transferencia: enviarTrfOperador (...) "
                        +"macOrigen["+macOrigen+"] traceNumber["+traceNumber+"] id["+numeroOperacion+"]");
            }

            try{
                xfml = construyeXFML(cliente,
                        canal,
                        TablaValores.getValor(TTFF_CONFIG, "consultaTransferencia", "tipoOperacion"),
                        TablaValores.getValor(TTFF_CONFIG, "consultaTransferencia", "tipoCuentaOrigen"),
                        TablaValores.getValor(TTFF_CONFIG, "consultaTransferencia", "tipoCuentaDestino"),
                    monto,
                        traceNumber,
                        fecha,
                    hora,
                    cuentaOrigen,
                    cliente.getRut(),
                    cliente.getDigito(),
                    codigoBancoDestino,
                    cuentaDestino,
                    rutDestino,
                    dvDestino,
                    nombreDestino,
                    comentario,
                        numeroOperacion,
                        macOrigen);

                String servTuxedo = TablaValores.getValor(TTFF_CONFIG, "consultaTransferencia", "servicioTuxedo");
                if (logger.isInfoEnabled() ) {
                    logger.info("enviarTrfOperador" + servTuxedo);
                }
                if(servTuxedo == null || servTuxedo.trim().equals("")){
                    servTuxedo = SERV_TUX_ABONO_CCA;
                }
                detalleOperador = enviarTrfOperador(cliente, canal, macOrigen, xfml, servTuxedo);
            }
            catch(Exception ex) {
            if (logger.isInfoEnabled() ) {
                    logger.info("Fall� al consultar la transferencia: enviarTrfOperador (...) "
                            +"macOrigen["+macOrigen+"] traceNumber["+traceNumber+"] id["+numeroOperacion+"]");
            }

                try {
                    producto = TablaValores.getValor(STORE_FORWARD_CONFIG, "ConsultaTransferenciasEmpresas",
                                                     "producto");
                    if (logger.isInfoEnabled() ) {
                        logger.info("transferenciaCtaCteOtroBancoEnLineaEmpresaConEnvioDeMail"
                                    + "[ " + numeroOperacion + " ]Producto [" + producto + "]");
                    }
                    intentos = Integer.parseInt(TablaValores.getValor(STORE_FORWARD_CONFIG,
                                                "ConsultaTransferenciasEmpresas", "intentos"));
                    if (logger.isInfoEnabled() ) {
                        logger.info("transferenciaCtaCteOtroBancoEnLineaEmpresaConEnvioDeMail"
                                    + "[ " + numeroOperacion + " ]Re-Intentos [" + intentos + "]");
                    }

                    insertoOk = registraTransaccionPendiente(cliente,
                            canal,
                            ex,
                            TablaValores.getValor(TTFF_CONFIG, "Trx", "valor"),
                            cuentaOrigen.substring(CANTIDAD_CARACTERES_A_ELIMINAR, LARGO_CUENTA),
                            null,
                            monto,
                            traceNumber,
                            producto,
                            convenio,
                            intentos,
                            false,
                            detalleTransferencia.getCodigoServicio(),
                            String.valueOf(detalleTransferencia.getRutUsuario()),
                            detalleTransferencia.getEMailDestino(),
                            xfml,
                            null,
                            null);
                    if (logger.isInfoEnabled() ) {
                        logger.info("transferenciaCtaCteOtroBancoEnLineaEmpresaConEnvioDeMail"
                                   + "[ " + numeroOperacion + " ]registr� transaccion pendiente consulta: "
                                   + insertoOk);
                    }
                }
                catch(Exception rtp) {
                    if (logger.isEnabledFor(Level.ERROR) ) {
                        logger.error("transferenciaCtaCteOtroBancoEnLineaEmpresaConEnvioDeMail"
                                     + "[ " + numeroOperacion + " ]Error al registrar transaccion pendiente "
                                     + "consulta:" + rtp.getMessage());
                    }
                }
                CCADirectaVO ccadirecta =  new CCADirectaVO();
                ccadirecta.setTipoRespuesta("");
                return ccadirecta;
            }
        }

        if (logger.isInfoEnabled() ) {
            logger.info("transferenciaCtaCteOtroBancoEnLineaEmpresaConEnvioDeMail"
                    + "[ " + numeroOperacion + " ]Se Obtiene Informaci�n de la Respuesta de la CCA");
        }

        String respuesta = TablaValores.getValor(TTFF_CONFIG, "0"+detalleOperador.getTipoRespuesta(), "tipo");

        if (logger.isInfoEnabled() ) {
            logger.info("transferenciaCtaCteOtroBancoEnLineaEmpresaConEnvioDeMail"
                    + "[ " + numeroOperacion + " ]Respuesta desde el CCA, tipo["
                    + detalleOperador.getTipoRespuesta() + "] respuesta[" + respuesta + "]");
        }

        if(logger.isDebugEnabled())
        {
            logger.info("transferenciaCtaCteOtroBancoEnLineaEmpresaConEnvioDeMail"
                    + "[ " + numeroOperacion + " ]Respuesta full desde el CCA:" + detalleOperador.toString());
        }

        boolean validacionMAC = false;

        if(operarMAC && detalleOperador.getTipoMensaje().equals("0210")){
            if (logger.isInfoEnabled() ) {
                logger.info(
                    " MAILDESTINO Cliente:"+cliente.getFullRut()+"" +
                    " Canal:"+canal+
                    "[transferenciaCtaCteOtroBancoEnLineaEmpresaConEnvioDeMail]"+
                    "[ "+numeroOperacion+" ] Se valida MAC");
            }
            try {
                logger.info("Se validar� la MAC");
                int validaMAC = messageAuthenticationCodeHelper.validaMessageAuthenticationCode(
                        String.valueOf(cliente.getRut())+cliente.getDigito(),
                        cuentaOrigen,
                        codigoBancoBCI,
                        codigoBancoDestino,
                        String.valueOf(rutDestino)+dvDestino,
                        cuentaDestino,
                        String.valueOf((long)monto),
                        traceNumber,
                        detalleOperador.getTimesTampCCA(),
                        detalleOperador.getMacReceptor(),
                        detalleOperador.getTipoRespuesta(),
                        detalleOperador.getMacOperador(),
                        ORIGEN);
                validacionMAC = (validaMAC == cero || validaMAC == dos) ? true : false;
                logger.info("Validaci�n MAC exitosa.");
            } 
            catch (Exception e) {
                if (logger.isEnabledFor(Level.ERROR)) {
                    logger.error("transferenciaCtaCteOtroBancoEnLineaEmpresaConEnvioDeMail"
                            + "[ " + numeroOperacion + " ]Error al validar la MAC: " + e.toString());
                }
            }
        }
        if(operarMAC && !detalleOperador.getTipoMensaje().equals("9200") && !validacionMAC){
            if (logger.isInfoEnabled() ) {
                logger.info(
                    " MAILDESTINO Cliente:"+cliente.getFullRut()+"" +
                    " Canal:"+canal+
                    "[transferenciaCtaCteOtroBancoEnLineaEmpresaConEnvioDeMail]"+
                    "[ "+numeroOperacion+" ] Se rechaza MAC");
            }
            try {
                rechazaMacOperador(
                        cliente,
                        canal,
                        tipoCuentaOrigen,
                        tipoCuentaDestino,
                        monto,
                        detalleOperador.getTimesTampCCA(),
                        traceNumber,
                        hora,
                        fecha,
                        detalleOperador.getTipoRespuesta(),
                        cuentaOrigen,
                        cliente.getRut(),
                        cliente.getDigito(),
                        codigoBancoDestino,
                        cuentaDestino,
                        rutDestino,
                        dvDestino,
                        nombreDestino,
                        macOrigen);
            } catch (Exception e) {
                if (logger.isInfoEnabled() ) {
                    logger.info(
                        " MAILDESTINO Cliente:"+cliente.getFullRut()+"" +
                        " Canal:"+canal+
                        "[transferenciaCtaCteOtroBancoEnLineaEmpresaConEnvioDeMail]"+
                        "[ "+numeroOperacion+" ] Se realiza reversa banco Exception en rechazaMacOperador: "+e.toString());
                }
                reversaBanco = realizaReversaBanco(cliente,
                        canal,
                        tipoCuentaOrigen,
                        tipoCuentaDestino,
                        monto,
                        traceNumber,
                        fecha,
                        hora,
                        cuentaOrigen,
                        cliente.getRut(),
                        cliente.getDigito(),
                        codigoBancoDestino,
                        cuentaDestino,
                        rutDestino,
                        dvDestino,
                        nombreDestino,
                        comentario,
                        numeroOperacion);
                if (logger.isInfoEnabled() ) {
                    logger.info(
                        " MAILDESTINO Cliente:"+cliente.getFullRut()+"" +
                        " Canal:"+canal+
                        "[transferenciaCtaCteOtroBancoEnLineaEmpresaConEnvioDeMail]"+
                        "[ "+numeroOperacion+" ] realiz� reversa banco: "+reversaBanco);
                }
            }
        }
        if((operarMAC && !validacionMAC) || detalleOperador.getTipoMensaje().equals("9200") || respuesta == null){
            if (logger.isInfoEnabled() ) {
                logger.info(
                    " MAILDESTINO Cliente:"+cliente.getFullRut()+"" +
                    " Canal:"+canal+
                    "[transferenciaCtaCteOtroBancoEnLineaEmpresaConEnvioDeMail]"+
                    "[ "+numeroOperacion+" ] Posibles errores: ["+respuesta+"]["+detalleOperador.getTipoMensaje()+"]["+validacionMAC+"]");
            }
            if (logger.isInfoEnabled() ) {
                logger.info(
                    " MAILDESTINO Cliente:"+cliente.getFullRut()+"" +
                    " Canal:"+canal+
                    "[transferenciaCtaCteOtroBancoEnLineaEmpresaConEnvioDeMail]"+
                    "[ "+numeroOperacion+" ] Realizando Reversa de Cargo");
            }
            try{
                realizaReversaCargo(cliente,canal,cuentaOrigen,traceNumber,monto);
            }
            catch(Exception ex)
            {
                if (logger.isInfoEnabled() ) {
                    logger.info(
                        " MAILDESTINO Cliente:"+cliente.getFullRut()+"" +
                        " Canal:"+canal+
                        "[transferenciaCtaCteOtroBancoEnLineaEmpresaConEnvioDeMail]"+
                        "[ "+numeroOperacion+" ]Exception reversa Cargo: "+ ErroresUtil.extraeStackTrace(ex));
                }
                insertoOk = registraTransaccionPendiente(cliente,
                        canal,
                        ex,
                        TablaValores.getValor(TTFF_CONFIG, "Trx", "valor"),
                        cuentaOrigen.substring(CANTIDAD_CARACTERES_A_ELIMINAR, LARGO_CUENTA),
                        null,
                        monto,
                        traceNumber,
                        producto,
                        convenio,
                        intentos,
                        false,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null);
                if (logger.isInfoEnabled() ) {
                    logger.info(
                        " MAILDESTINO Cliente:"+cliente.getFullRut()+"" +
                        " Canal:"+canal+
                        "[transferenciaCtaCteOtroBancoEnLineaEmpresaConEnvioDeMail]" +
                        "[ " + numeroOperacion + " ]registr� transaccion pendiente cargo: " + insertoOk);
                }
            }

            String estado = ESTADO_RECHAZADO;
            String codEstado = COD_EST_REC;

            try {
                logCliente(cliente, canal, "transferenciaCtaCteOtroBancoEnLineaEmpresa", "[ " + numeroOperacion + " ]Cambia estado de la transferencia");
                ResultCambiaEstadoTransferencia resCet = cambiaEstadoTransferenciaEnLinea(String.valueOf(cliente.getRut()), convenio, numeroOperacion, estado, estado, codEstado);
                logCliente(cliente, canal, "transferenciaCtaCteOtroBancoEnLineaEmpresa", "[ " + numeroOperacion + " ]Despues de cambiar el estado de la transferencia");

                logCliente(cliente, canal, "transferenciaCtaCteOtroBancoEnLineaEmpresa", "[ " + numeroOperacion + " ]res_cet.getCim_status()=" + resCet.getCim_status());
                logCliente(cliente, canal, "transferenciaCtaCteOtroBancoEnLineaEmpresa", "[ " + numeroOperacion + " ]res_cet.getCim_mensaje()=" + resCet.getCim_mensaje());

            }
            catch (Exception ignored) {
                logCliente(cliente, canal, "transferenciaCtaCteOtroBancoEnLineaEmpresa", "[ " + numeroOperacion + " ] Error al actualizar rechazo, " + ignored.getClass() + " : " + ignored.getMessage());
            }

            throw new TransferenciaException("TRF001");
        }
        else if (!respuesta.equals("OK"))
        {
            boolean reversaRegistrada = false;
            if (logger.isInfoEnabled() ) {
                logger.info(" MAILDESTINO Cliente:" + cliente.getFullRut() + " Canal:" + canal
                    + "[transferenciaCtaCteOtroBancoEnLineaEmpresaConEnvioDeMail]"
                    + "[ " + numeroOperacion + " ]RESPUESTA: " + respuesta);
            }

            try{
                realizaReversaCargo(cliente,canal,cuentaOrigen,traceNumber,monto);
                reversaRegistrada = true;
            }
            catch(Exception ex)
            {
                if (logger.isInfoEnabled() ) {
                    logger.info( " MAILDESTINO Cliente:" + cliente.getFullRut() + " Canal:" + canal
                        + "[transferenciaCtaCteOtroBancoEnLineaEmpresaConEnvioDeMail]" + "[ "
                        + numeroOperacion + " ]Exception reversa Cargo: " + ErroresUtil.extraeStackTrace(ex));
                }
                try {
                    insertoOk = registraTransaccionPendiente(cliente,
                        canal,
                        ex,
                        TablaValores.getValor(TTFF_CONFIG, "Trx", "valor"),
                        cuentaOrigen.substring(CANTIDAD_CARACTERES_A_ELIMINAR, LARGO_CUENTA),
                        null,
                        monto,
                        traceNumber,
                        producto,
                        convenio,
                        intentos,
                        false,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null);

                    reversaRegistrada = true;

                    if (logger.isInfoEnabled() ) {
                        logger.info( " MAILDESTINO Cliente:" + cliente.getFullRut() +  " Canal:" + canal
                            + "[transferenciaCtaCteOtroBancoEnLineaEmpresaConEnvioDeMail]" + "[ "
                            + numeroOperacion + " ]registr� transaccion pendiente reversa Cargo: " + insertoOk);
                    }
                }
                catch(Exception rtp) {
                    if (logger.isEnabledFor(Level.ERROR)) {
                        logger.error("MAILDESTINO Cliente:" + cliente.getFullRut() + " Canal:" + canal
                            + "[transferenciaCtaCteOtroBancoEnLineaEmpresaConEnvioDeMail]" + "[ "
                            + numeroOperacion + " ] : Error al registrar transaccion pendiente reversa Cargo:"
                            + rtp.getMessage());
                    }

                }
            }

            String estado = "REC";
            String codEstado = COD_REC
                + detalleOperador.getTipoRespuesta().substring(detalleOperador.getTipoRespuesta().length()
                        - COD_ERR_CCA);
            String tipoError = TablaValores.getValor(TTFF_CONFIG,
                                                         "0" + detalleOperador.getTipoRespuesta(), "tipo");
            if (logger.isInfoEnabled() ) {
                logger.info("MAILDESTINO Cliente:" + cliente.getFullRut() + " Canal:" + canal
                    + "[transferenciaCtaCteOtroBancoEnLineaEmpresaConEnvioDeMail]"
                    + "[ " + numeroOperacion + " ]RESPUESTA:" + respuesta + ", Reversa registrada:"
                    + reversaRegistrada + ", tipoError:" + tipoError);
            }

            if (( tipoError != null && tipoError.equalsIgnoreCase("NOKSISTEMA") ) && reversaRegistrada ) {
                estado = "PEN";
                codEstado = "0000";
            }

            try {
                if (logger.isInfoEnabled() ) {
                    logger.info("MAILDESTINO Cliente:" + cliente.getFullRut() + " Canal:" + canal
                        + "[transferenciaCtaCteOtroBancoEnLineaEmpresaConEnvioDeMail]" + "[ "
                        + numeroOperacion + " ]Cambiando estado a: " + estado + ", codEstado:"+ codEstado);
                }

                cambiaEstadoTransferenciaEnLinea(String.valueOf(cliente.getRut()),
                        convenio,
                        numeroOperacion,
                        estado,
                        estado,
                        codEstado);

                if (logger.isInfoEnabled() ) {
                    logger.info("MAILDESTINO Cliente:" + cliente.getFullRut() + " Canal:" + canal
                        + "[transferenciaCtaCteOtroBancoEnLineaEmpresaConEnvioDeMail]"
                        + "[ " + numeroOperacion + " ] : Despues de cambiar estado a: " + estado);
                }
            }
            catch (Exception e) {
                if (logger.isInfoEnabled() ) {
                    logger.info("MAILDESTINO Cliente:" + cliente.getFullRut() + " Canal:" + canal
                        + "[transferenciaCtaCteOtroBancoEnLineaEmpresaConEnvioDeMail]" + "[ "
                        + numeroOperacion + " ] : Error al cambiar el estado:" + e.getMessage());
                }
                try {
                    String pdt = TablaValores.getValor(STORE_FORWARD_CONFIG,
                        "ActualizaEstadoTransferenciasEmpresas", "producto");
                    intentos = Integer.parseInt(TablaValores.getValor(STORE_FORWARD_CONFIG,
                                        "ActualizaEstadoTransferenciasEmpresas", "intentos"));

                    if (logger.isDebugEnabled() ) {
                        logger.debug("MAILDESTINO Cliente:" + cliente.getFullRut() + " Canal:" + canal
                            + "[transferenciaCtaCteOtroBancoEnLineaEmpresaConEnvioDeMail]"
                            + "[ " + numeroOperacion + " ] Producto:[" + producto + "], Re-Intentos:["
                            + intentos + "]");
                    }

                    String xml = construyeXFML(cliente,
                        canal,
                        "",
                        estado,
                        codEstado,
                        monto,
                        traceNumber,
                        new Date(),
                        hora,
                        "",
                        0,
                        ' ',
                        "",
                        "",
                        0,
                        ' ',
                        "",
                        "",
                        numeroOperacion,
                        "");

                    insertoOk = registraTransaccionPendiente(cliente,
                        canal,
                        e,
                        "",
                        cuentaOrigen.substring(CANTIDAD_CARACTERES_A_ELIMINAR, LARGO_CUENTA),
                        null,
                        monto,
                        traceNumber,
                        pdt,
                        convenio,
                        intentos,
                        false,
                        detalleTransferencia.getCodigoServicio(),
                        null,
                        null,
                        xml,
                        null,
                        null);

                    if (logger.isInfoEnabled() ) {
                        logger.info("MAILDESTINO Cliente:" + cliente.getFullRut() + " Canal:" + canal
                            + "[transferenciaCtaCteOtroBancoEnLineaEmpresaConEnvioDeMail]"
                            + "[ " + numeroOperacion + " ] : registr� transaccion pendiente cambio estado: "
                            + insertoOk);
                    }
                }
                catch(Exception rtp) {
                    if (logger.isEnabledFor(Level.ERROR)) {
                        logger.error("MAILDESTINO Cliente:" + cliente.getFullRut() + " Canal:" + canal
                            + "[transferenciaCtaCteOtroBancoEnLineaEmpresaConEnvioDeMail]" + "[ "
                            + numeroOperacion + " ] : Error al registrar transaccion pendiente cambio estado:"
                            + rtp.getMessage());
                    }
                }
            }

            if (( tipoError != null && tipoError.equalsIgnoreCase("NOKSISTEMA") ) && reversaRegistrada ) {
                if (logger.isInfoEnabled()) {
                    logger.info("MAILDESTINO Cliente:" + cliente.getFullRut() + " Canal:" + canal
                        + "[transferenciaCtaCteOtroBancoEnLineaEmpresaConEnvioDeMail]" + "[ "
                        + numeroOperacion + " ] : Se devuelve excepcion que indica TTFF es reintentable");
                }
                throw new TransferenciaException("TRF100");
            }
            else {
                if (logger.isInfoEnabled()) {
                    logger.info("MAILDESTINO Cliente:" + cliente.getFullRut() + "" + " Canal:" + canal
                        + "[transferenciaCtaCteOtroBancoEnLineaEmpresaConEnvioDeMail]"
                        + "[ " + numeroOperacion + " ] : La TTFF NO es reintentable");
                }
            }
        }
        else if (respuesta.equals("OK"))
        {
            /* --------------> Inicio de Modificacion 29-07-2008*/
            if (logger.isInfoEnabled() ) {
                logger.info("[transferenciaCtaCteOtroBancoEnLineaEmpresaConEnvioDeMail]"
                        + "[ " + numeroOperacion + " ]La respuesta de la CCA fu� OK, "
                        + "se procede a actualizar el estado de la ttff");
            }
            String estadoTransaccion = "PRO";
            String codigoEstado = "0000";
            if (logger.isInfoEnabled() ) {
                logger.info(
                    " MAILDESTINO Cliente:"+cliente.getFullRut()+"" +
                    " Canal:"+canal+
                    "[transferenciaCtaCteOtroBancoEnLineaEmpresaConEnvioDeMail]"+
                    "[ "+numeroOperacion+" ]codigoEstado      ["+codigoEstado+"]");
            }
            String ctaOrigenDoce;
            String rutEmpresa = new DecimalFormat ("00000000").format(Integer.parseInt(String.valueOf(cliente.getRut())));
            if (logger.isInfoEnabled() ) {
                logger.info(
                    " MAILDESTINO Cliente:"+cliente.getFullRut()+"" +
                    " Canal:"+canal+
                    "[transferenciaCtaCteOtroBancoEnLineaEmpresaConEnvioDeMail]"+
                    "[ "+numeroOperacion+" ]rutEmpresa        ["+rutEmpresa+"]");
            }
                ctaOrigenDoce = StringUtil.rellenaConCeros(cuentaOrigen.trim(), LARGO_CUENTA);
            if (logger.isInfoEnabled() ) {
                logger.info(
                    " MAILDESTINO Cliente:"+cliente.getFullRut()+"" +
                    " Canal:"+canal+
                    "[transferenciaCtaCteOtroBancoEnLineaEmpresaConEnvioDeMail]"+
                    "[ "+numeroOperacion+" ]ctaOrigenDoce     ["+ctaOrigenDoce+"]");
            }

            if (logger.isInfoEnabled() ) {
                logger.info("[transferenciaCtaCteOtroBancoEnLineaEmpresaConEnvioDeMail]"
                        + "[ " + numeroOperacion + " ]Inicio de la Actualizacion del estado de la transferencia");
            }
            try {
                if (logger.isInfoEnabled() ) {
                    logger.info(
                        " MAILDESTINO Cliente:"+cliente.getFullRut()+"" +
                        " Canal:"+canal+
                        "[transferenciaCtaCteOtroBancoEnLineaEmpresaConEnvioDeMail]"+
                                    "[ " + numeroOperacion + " ]Se cambiar� el estado de la transferencia.");
                }
                ResultCambiaEstadoTransferencia res_cet = cambiaEstadoTransferenciaEnLinea(rutEmpresa,
                        convenio,
                        numeroOperacion,
                        estadoTransaccion,
                        estadoTransaccion,
                        codigoEstado);

                if (logger.isInfoEnabled() ) {
                    logger.info(
                        " MAILDESTINO Cliente:"+cliente.getFullRut()+"" +
                        " Canal:"+canal+
                        "[transferenciaCtaCteOtroBancoEnLineaEmpresaConEnvioDeMail]"+
                                    "[ " + numeroOperacion + " ]Se cambi� el estado de la transferencia OK.");
                }
                if (logger.isInfoEnabled() ) {
                    logger.info(
                        " MAILDESTINO Cliente:"+cliente.getFullRut()+"" +
                        " Canal:"+canal+
                        "[transferenciaCtaCteOtroBancoEnLineaEmpresaConEnvioDeMail]"+
                        "[ "+numeroOperacion+" ]res_cet.getCim_status()="+res_cet.getCim_status());
                }
                if (logger.isInfoEnabled() ) {
                    logger.info(
                        " MAILDESTINO Cliente:"+cliente.getFullRut()+"" +
                        " Canal:"+canal+
                        "[transferenciaCtaCteOtroBancoEnLineaEmpresaConEnvioDeMail]"+
                        "[ "+numeroOperacion+" ]res_cet.getCim_mensaje()="+res_cet.getCim_mensaje());
                }

            } catch (Exception ignored) {
                if (logger.isEnabledFor(Level.ERROR)) {
                    logger.error(
                        " MAILDESTINO Cliente:"+cliente.getFullRut()+"" +
                        " Canal:"+canal+
                        "[transferenciaCtaCteOtroBancoEnLineaEmpresaConEnvioDeMail]"+
                        "[ "+numeroOperacion+" ] :" + ErroresUtil.extraeStackTrace(ignored));
                }
            }

            if (logger.isInfoEnabled() ) {
                logger.info("[transferenciaCtaCteOtroBancoEnLineaEmpresaConEnvioDeMail]"
                        + "[ " + numeroOperacion + " ]Fin de la Actualizacion del estado de la transferencia. "
                        + "Inicio de la Actualizacion de los Montos Diarios");
            }

            if (logger.isInfoEnabled() ) {
                logger.info(
                    " MAILDESTINO Cliente:"+cliente.getFullRut()+"" +
                    " Canal:"+canal+
                    "[transferenciaCtaCteOtroBancoEnLineaEmpresaConEnvioDeMail]"+
                    "[ "+numeroOperacion+" ]Sin Problemas en Transferencia - Entonces se actualiza CEC");
            }

            try {
                if (logger.isInfoEnabled() ) {
                    logger.info(
                        " MAILDESTINO Cliente:"+cliente.getFullRut()+"" +
                        " Canal:"+canal+
                        "[transferenciaCtaCteOtroBancoEnLineaEmpresaConEnvioDeMail]"+
                        "[ "+numeroOperacion+" ]Cambio de Montos Diarios en CEC y MAC");
                }
                if(logger.isDebugEnabled())
                {
                    logger.debug("[transferenciaCtaCteOtroBancoEnLineaEmpresaConEnvioDeMail]"
                            + "[ " + numeroOperacion + " ]call cambiaMontosDiaEnLinea: rutEmpresa[" + rutEmpresa
                            + "],rutUsuario[" + rutUsuario + "],convenio[" + convenio + "],ctaOrigenDoce["
                            + ctaOrigenDoce + "],numeroOperacion[" + numeroOperacion + "],montoTransferir["
                            + montoTransferir + "]");
                }
                String res_mtodia = cambiaMontosDiaEnLinea(rutEmpresa,
                        StringUtil.rellenaConCeros(String.valueOf(rutUsuario), LARGO_RUT_CLIENTE),
                        convenio,
                        ctaOrigenDoce,
                        numeroOperacion,
                        String.valueOf(montoTransferir),
                        "LIN");
                if (logger.isInfoEnabled() ) {
                    logger.info(
                        " MAILDESTINO Cliente:"+cliente.getFullRut()+"" +
                        " Canal:"+canal+
                        "[transferenciaCtaCteOtroBancoEnLineaEmpresaConEnvioDeMail]"+
                        "[ "+numeroOperacion+" ]Despues de Cambiar Montos Diarios");
                }
                if (res_mtodia.equals("NOK"))
                {   logger.info("Error en cambiaMontosDiaEnLinea(...)");
                    if (logger.isEnabledFor(Level.ERROR)) {
                        logger.error(
                            " MAILDESTINO Cliente:"+cliente.getFullRut()+"" +
                            " Canal:"+canal+
                            "[transferenciaCtaCteOtroBancoEnLineaEmpresaConEnvioDeMail]"+
                            "[ "+numeroOperacion+" ]No rebaj� tabla montos Dia rutEmp=" + String.valueOf(cliente.getRut()) +
                            "- identificacion=" + numeroOperacion +
                            "- numConvenio=" + convenio +
                            "- ctaOrigenDoce=" + ctaOrigenDoce);
                    }
                }
            } catch (Exception ignored) {
                logger.info("Error en cambiaMontosDiaEnLinea(...)");
                if (logger.isEnabledFor(Level.ERROR) ) {
                    logger.error(
                        " MAILDESTINO Cliente:"+cliente.getFullRut()+"" +
                        " Canal:"+canal+
                        "[transferenciaCtaCteOtroBancoEnLineaEmpresaConEnvioDeMail]"+
                        "[ "+numeroOperacion+" ]"+ignored.getClass() + " : " + ErroresUtil.extraeStackTrace(ignored));
                }
            }

            if (logger.isInfoEnabled() ) {
                logger.info("[transferenciaCtaCteOtroBancoEnLineaEmpresaConEnvioDeMail]"
                        + "[ " + numeroOperacion + " ]Fin de la Actualizazion de los Montos Diarios. "
                        + "Inicio de la Actualizazion del TRACENUMBER");
            }

            String fechaTimesTamp = new String();

            try {
                fechaTimesTamp = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(FechasUtil.convierteStringADate(detalleOperador.getTimesTampCCA(), new SimpleDateFormat( "yyMMddHHmmss" )));
            } catch (Exception e) {
                fechaTimesTamp = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(new Date());
            }
            if (logger.isInfoEnabled() ) {
                logger.info(
                    " MAILDESTINO Cliente:"+cliente.getFullRut()+"" +
                    " Canal:"+canal+
                    "[transferenciaCtaCteOtroBancoEnLineaEmpresaConEnvioDeMail]"+
                    "[ "+numeroOperacion+" ] fechaTimesTamp: "+fechaTimesTamp);
            }
            try{
                if (logger.isInfoEnabled() ) {
                    logger.info(
                        " MAILDESTINO Cliente:"+cliente.getFullRut()+"" +
                        " Canal:"+canal+
                        "[transferenciaCtaCteOtroBancoEnLineaEmpresaConEnvioDeMail]"+
                        "[ "+numeroOperacion+" ]Grabando TraceNumber y Fecha de TTFF");
                }
                if (logger.isInfoEnabled() ) {
                    logger.info(
                        " MAILDESTINO Cliente:"+cliente.getFullRut()+"" +
                        " Canal:"+canal+
                        "[transferenciaCtaCteOtroBancoEnLineaEmpresaConEnvioDeMail]"+
                        "[ "+numeroOperacion+" ]rutEmp            [" + StringUtil.rellenaPorLaIzquierda(String.valueOf(rutEmpresa), 8, '0') + "]");
                }
                if (logger.isInfoEnabled() ) {
                    logger.info(
                        " MAILDESTINO Cliente:"+cliente.getFullRut()+"" +
                        " Canal:"+canal+
                        "[transferenciaCtaCteOtroBancoEnLineaEmpresaConEnvioDeMail]"+
                        "[ "+numeroOperacion+" ]convenio            [" + StringUtil.rellenaPorLaIzquierda(convenio, 10, ' ') + "]");
                }
                if (logger.isInfoEnabled() ) {
                    logger.info(
                        " MAILDESTINO Cliente:"+cliente.getFullRut()+"" +
                        " Canal:"+canal+
                        "[transferenciaCtaCteOtroBancoEnLineaEmpresaConEnvioDeMail]"+
                        "[ "+numeroOperacion+" ]traceNumber         [" + detalleOperador.getTraceNumber() + "]");
                }
                if (logger.isInfoEnabled() ) {
                    logger.info(
                        " MAILDESTINO Cliente:"+cliente.getFullRut()+"" +
                        " Canal:"+canal+
                        "[transferenciaCtaCteOtroBancoEnLineaEmpresaConEnvioDeMail]"+
                        "[ "+numeroOperacion+" ]fechaTimesTamp    [" + fechaTimesTamp + "]");
                }
                if (logger.isInfoEnabled() ) {
                    logger.info(
                        " MAILDESTINO Cliente:"+cliente.getFullRut()+"" +
                        " Canal:"+canal+
                        "[transferenciaCtaCteOtroBancoEnLineaEmpresaConEnvioDeMail]"+
                        "[ "+numeroOperacion+" ]operador          [" + detalleOperador.getOperador() + "]");
                }
                String respuestaActCont = actualizacionContable(StringUtil.rellenaPorLaIzquierda(String.valueOf(rutEmpresa), 8, '0'),
                        StringUtil.rellenaPorLaIzquierda(convenio, 10, ' '),
                        numeroOperacion,
                        detalleOperador.getTraceNumber(),
                        fechaTimesTamp,
                        detalleOperador.getOperador());
                if (logger.isInfoEnabled() ) {
                    logger.info(
                        " MAILDESTINO Cliente:"+cliente.getFullRut()+"" +
                        " Canal:"+canal+
                        "[transferenciaCtaCteOtroBancoEnLineaEmpresaConEnvioDeMail]"+
                        "[ "+numeroOperacion+" ]Despues de guardar el TraceNumber y la Fecha en la BD : "+ respuestaActCont);
                }
            }
            catch(Exception ignored){
                if (logger.isInfoEnabled() ) {
                    logger.info(
                        " MAILDESTINO Cliente:"+cliente.getFullRut()+"" +
                        " Canal:"+canal+
                        "[transferenciaCtaCteOtroBancoEnLineaEmpresaConEnvioDeMail]"+
                        "[ "+numeroOperacion+" ]"+ignored.getClass() + " : "+ ErroresUtil.extraeStackTrace(ignored));
                }
            }

            if (logger.isInfoEnabled() ) {
                logger.info("[transferenciaCtaCteOtroBancoEnLineaEmpresaConEnvioDeMail]"
                        + "[ " + numeroOperacion + " ]Fin de la Actualizazion del TRACENUMBER");
            }

        }
        if (logger.isInfoEnabled() ) {
            logger.info(
                " MAILDESTINO Cliente:"+cliente.getFullRut()+"" +
                " Canal:"+canal+
                "[transferenciaCtaCteOtroBancoEnLineaEmpresaConEnvioDeMail]"+
                "[ "+numeroOperacion+" ]---> DETALLE FINAL, TRF FINALIZADA\n");
        }
        String codigo = detalleOperador.getTipoRespuesta();
        if(codigo.equals(CODIGO_EXITO_TRANSFERENCIA)) {
            if (logger.isInfoEnabled() ) {
                logger.info("[transferenciaCtaCteOtroBancoEnLineaEmpresaConEnvioDeMail]"
                        + "[ " + numeroOperacion + " ]Se enviar� email al destinatario. ");
            }
            detalleTransferencia.setRut(cliente.getRut());
            detalleTransferencia.setDig(cliente.getDigito());
            detalleTransferencia.setNombreOrigen(cliente.getFullName().trim());
            detalleTransferencia.setEMailCliente(cliente.getEmail().trim());
            detalleTransferencia.setNombreBancoOrigen(TablaValores.getValor(TTFF_CONFIG, "codigoBCI", "desc_" + detalleTransferencia.getCanalID()));
            detalleTransferencia.setCodigoBancoOrigen(TablaValores.getValor(TTFF_CONFIG, "codigoBCI", "valor"));
            detalleTransferencia.setFechaAbono(fecha);
            detalleTransferencia.setFechaCargo(fecha);
            detalleTransferencia.setFechaTransferencia(fecha);
            if (logger.isInfoEnabled() ) {
                logger.info("[transferenciaCtaCteOtroBancoEnLineaEmpresaConEnvioDeMail]"
                        + "[ " + numeroOperacion + " ]Se prepararon los datos para el env�o de email: "
                        + detalleTransferencia.toString());
            }
	    try{
		enviaEmail(detalleTransferencia, true, cliente, canal);
            }catch(Exception ignored){
				if (logger.isInfoEnabled() ) {
                    logger.info(
                            " MAILDESTINO ERROR ENVIO MAIL - Cliente:"+cliente.getFullRut()+"" +
                            " Canal:"+canal
                            +ignored.getClass() + " : "+ ErroresUtil.extraeStackTrace(ignored));
                }	
            }
            if (logger.isInfoEnabled() ) {
                logger.info("[transferenciaCtaCteOtroBancoEnLineaEmpresaConEnvioDeMail]"
                        + "[ " + numeroOperacion + " ]Email probablemente enviado.");
            }
        }
        else
        {
            if (logger.isInfoEnabled() ) {
                logger.info("[transferenciaCtaCteOtroBancoEnLineaEmpresaConEnvioDeMail]"
                        + "[ " + numeroOperacion + " ]No se enviar� email destinatario, c�digo respuesta["
                        + codigo + "]");
            }
            if(logger.isDebugEnabled())
            {
                logger.debug("[transferenciaCtaCteOtroBancoEnLineaEmpresaConEnvioDeMail]"
                        + "[ " + numeroOperacion + " ]No se enviar� email destinatario, detalles:"
                        + detalleOperador.toString());
            }
        }
        return detalleOperador;
    }

    /**
     * M�todo que permite consultar Transferencias a terceros
     * Registro de versiones:<ul>
     * <li>1.0 08/11/2011,Richard Salazar Freddy Painenao (Osone):Versi�n inicial.
     * </ul>
     * @param filtro
     * @param periodoInicial fecha inicio
     * @param periodoFinal fecha Final
     * @param bloque inicio paginacion
     * @param cantidadRegistros devueltos en paginacion
     * @param tipoTransferencia entre poductos contratados o transferencias
     * @param tipoMovimiento este puede ser realizado o recibidas o todas
     * @param hoy boolean si corresponde si es true corresponde a transferencias ocurridas hoy
     * @throws RemoteException
     * @throws BCIExpressException
     * @throws Exception
     * @since 1.17
     *
     *
     */

    public DetalleTransferenciaVO[] traerTransferencias(FiltroConsultarTransferenciasVO filtro,Date periodoInicial,Date periodoFinal, int bloque, int cantidadRegistros,String tipoTransferencia, String tipoMovimiento, boolean hoy,String nCuenta) throws TransferenciaException, RemoteException, BCIExpressException{

        DetalleTransferenciaVO[] detalleTransferenciaVO = null;
        try{
            TransferenciasFondosDAO transferenciasFondosDAO = new TransferenciasFondosDAO();
            logger.debug("[TransferenciasFondosBean - traerTransferencias]");
            if (!hoy &&(tipoTransferencia.equals(CONST_TERCEROS))){
                logger.debug("[TransferenciasFondosBean - traerTransferencias]");
                return  transferenciasFondosDAO.traerTransferencias(filtro,periodoInicial,periodoFinal,tipoTransferencia, tipoMovimiento, bloque, cantidadRegistros,nCuenta);
            }else if (tipoTransferencia.equals(PRODCONT)&& !hoy){
                logger.debug("[TransferenciasFondosBean - traerTransferencias]");
                return  transferenciasFondosDAO.traerTransferencias(filtro,periodoInicial,periodoFinal,tipoTransferencia, tipoMovimiento, bloque, cantidadRegistros,nCuenta);
            }else if (tipoTransferencia.equals(CONST_TERCEROS)&&(tipoMovimiento.equals(RECIBIDAS))&& hoy) {
                logger.debug("[TransferenciasFondosBean - traerTransferencias]");
                return null;
            }else if (tipoTransferencia.equals(CONST_TERCEROS)&&(tipoMovimiento.equals(REALIZADAS))&& hoy) {
                logger.debug("[TransferenciasFondosBean - traerTransferencias]");
                return  transferenciasFondosDAO.traerTransferenciasRealizadaHoy(filtro,periodoInicial,periodoFinal,nCuenta);
            }else if (tipoTransferencia.equals(CONST_TERCEROS)&&(tipoMovimiento.equals(TODAS))&& hoy) {
                logger.debug("[TransferenciasFondosBean - traerTransferencias]");
                return  transferenciasFondosDAO.traerTransferenciasTercerosTodosHoy(filtro,periodoInicial,periodoFinal,nCuenta);
            }else if (tipoTransferencia.equals(PRODCONT)&&(tipoMovimiento.equals(TODAS))&& hoy){
                logger.debug("[TransferenciasFondosBean - traerTransferencias]");
                return  transferenciasFondosDAO.traerTransferenciasProdTodosHoy(filtro,periodoInicial,periodoFinal,nCuenta);
            }

        }catch (Exception e) {
            if(logger.isEnabledFor(Level.FATAL)){logger.fatal("[TransferenciasFondosBean - traerTransferencias] Error " + e.getMessage());}
            throw new BCIExpressException("ESPECIAL", e.getMessage());



        }
        return detalleTransferenciaVO;




    }



    /**
     * M�todo que permite consultar  Detalle Transferencias a terceros
     *
     * Registro de versiones:<ul>
     * <li>1.0 08/11/2011,Richard Salazar Freddy Painenao (Osone): Versi�n inicial.
     * </ul>
     * @param filtro
     * @param tipoTransferencia entre poductos contrata o transferencias
     * @param tipoBusqueda este puede ser realizado o recibidas o todas
     * @param numeroDetalle de la transferencia
     * @param hoy variable boolena que indica si la fecha buscada es hoy.
     * @throws RemoteException
     * @throws BCIExpressException
     * @throws Exception
     * @since 1.17
     *
     *
     */
    public DetalleTransferenciaVO[] traerDetalleTransferencias(FiltroConsultarTransferenciasVO filtro,String tipoTransferencia, String tipoBusqueda, String numeroDetalle, boolean hoy,String nDetalleTrx) throws TransferenciaException, RemoteException, BCIExpressException{

        DetalleTransferenciaVO[] detalleTransferenciaVO = null;
        try{
            TransferenciasFondosDAO transferenciasFondosDAO = new TransferenciasFondosDAO();
            logger.debug("[TransferenciasFondosBean - traerDetalleTransferencias]");

            if ((tipoTransferencia.equals(CONST_TERCEROS )|| tipoTransferencia.equals(PRODCONT))&& !hoy ){
                logger.debug("[TransferenciasFondosBean - Terceros Productos Contratados distinto de Hoy]");
                return  transferenciasFondosDAO.traerDetalleTransferencias(filtro,numeroDetalle,nDetalleTrx);
            }else if (tipoTransferencia.equals(PRODCONT)&& hoy){
                if (tipoBusqueda.equals(CTA)){
                    logger.debug("[TransferenciasFondosBean -  Productos Contratados Hoy CTA]");
                    return  transferenciasFondosDAO.traerDetalleCTA(filtro,numeroDetalle,nDetalleTrx);
                }else if (tipoBusqueda.equals(TDC)){
                    logger.debug("[TransferenciasFondosBean -  Productos Contratados Hoy TDC]");
                    return  transferenciasFondosDAO.traerDetalleTDC(filtro,numeroDetalle,nDetalleTrx);
                }
            }else if((tipoTransferencia.equals(CONST_TERCEROS)&& tipoBusqueda.equals(REALIZADAS))){
                if (hoy){
                    logger.debug("[TransferenciasFondosBean - Terceros Realizadas Hoy]");
                    return  transferenciasFondosDAO.traerDetalleTercerosHoy(filtro,numeroDetalle,nDetalleTrx);
                }
            }
        }catch (Exception e) {
            if(logger.isEnabledFor(Level.FATAL)){logger.fatal("[TransferenciasFondosBean - traerTransferencias] Error " + e.getMessage());}

            throw new BCIExpressException("ESPECIAL", e.getMessage());


        }
        return detalleTransferenciaVO;


    }




    /**
     * M�todo que permite consultar Transferencias a terceros
     * Registro de versiones:<ul>
     * <li>1.0 08/11/2011,Richard Salazar Freddy Painenao (Osone):Versi�n inicial.
     * </ul>
     * @param filtro
     * @param periodoInicial fecha inicio
     * @param periodoFinal fecha Final
     * @param bloque inicio paginacion
     * @param cantidadRegistros devueltos en paginacion
     * @param tipoTransferencia entre poductos contratados o transferencias
     * @param tipoMovimiento este puede ser realizado o recibidas o todas
     * @param hoy boolean si corresponde si es true corresponde a transferencias ocurridas hoy
     * @throws RemoteException
     * @throws Exception
     * @since 1.17
     *
     *
     */

    public DetalleTransferenciaVO[] traerTransferenciaTotales(FiltroConsultarTransferenciasVO filtro,Date periodoInicial,Date periodoFinal, String tipoTransferencia, String tipoMovimiento,String numeroCuenta) throws TransferenciaException, RemoteException, BCIExpressException{

        try{
            TransferenciasFondosDAO transferenciasFondosDAO = new TransferenciasFondosDAO();
            logger.debug("[TransferenciasFondosBean - traerTransferenciasTotales]");
            return  transferenciasFondosDAO.traeTotalTransferencias(filtro,periodoInicial,periodoFinal,tipoTransferencia, tipoMovimiento,numeroCuenta);

        }catch (Exception e) {
            if(logger.isEnabledFor(Level.FATAL)){logger.fatal("[TransferenciasFondosBean - traerTransferenciasTotales] Error " + e.getMessage());}

            throw new BCIExpressException("ESPECIAL", e.getMessage());


        }




    }

    /**
     * M�todo que construye el xml con los datos a enviar al operador.
     *
     *
     * Registro de versiones:<ul>
     *
     * <li>1.0 06/02/2012, Carlos Cerda I. (Orand): Versi�n inicial.
     *
     * </ul>
     *
     *
     * @param cliente objeto con los datos del cliente.
     * @param canal canal.
     * @param tipoOperacion tipo de operacion (30, 40)
     * @param tipoCuentaOrigen Tipo de cuenta de origen (CCT, CPR)
     * @param tipoCuentaDestino Tipo de cuenta de destino (CCT, CPR)
     * @param monto monto de la transferencia
     * @param traNumber identificador �nico de la transferencia
     * @param fecha fecha de cargo de la transferencia
     * @param hora hora de cargo de la transferencia
     * @param ctaOrigen cuenta del cliente
     * @param rut rut del cliente
     * @param dv digito verificador del cliente
     * @param codBancoDestino c�digo del banco de destino (001, etc)
     * @param ctaDestino cuenta de abono
     * @param rutDestino rut de abono
     * @param digDestino digito verificador de abono
     * @param nombreDestino nombre de la persona de destino del abono
     * @param comentario comentario para el env�o de Email
     * @param numeroOperacion n�mero que devuelve el sp que inseta en la BD tra
     * @param macOrigen mac de origen.
     * @return String, con el xfml.
     *
     * @since 1.0
     */
    private String construyeXFML(Cliente cliente,
            String canal,
            String tipoOperacion,
            String tipoCuentaOrigen,
            String tipoCuentaDestino,
            double monto,
            String traNumber,
            Date fecha,
            String hora,
            String ctaOrigen,
            long rut,
            char dv,
            String codBancoDestino,
            String ctaDestino,
            long rutDestino,
            char digDestino,
            String nombreDestino,
            String comentario,
            String numeroOperacion,
            String macOrigen){

        logCliente(cliente,canal,"enviarTrfOperador", "Modo XFMLBuffer");

        XFMLBuffer xfmlBuffer = new XFMLBuffer();
        String tipCuentaOrigen = new String();
        String tipCuentaDestino = new String();

        logCliente(cliente,canal,"enviarTrfOperador", "tipoOperacion: "+tipoOperacion);
        if(tipoOperacion != null){
            xfmlBuffer.addDato(new Dato("tipoOperacion", tipoOperacion));
            tipCuentaOrigen = tipoCuentaOrigen;
            tipCuentaDestino = tipoCuentaDestino;
        }
        else{
            tipCuentaOrigen = TablaValores.getValor(TTFF_CONFIG, "idTiposCuentaLinea", tipoCuentaOrigen);
            tipCuentaDestino = TablaValores.getValor(TTFF_CONFIG, "idTiposCuentaLinea", tipoCuentaDestino);
        }

        logCliente(cliente,canal,"enviarTrfOperador", "tipoCuentaOrigen: "+tipCuentaOrigen);
        logCliente(cliente,canal,"enviarTrfOperador", "tipoCuentaDestino: "+tipCuentaDestino);

        xfmlBuffer.addDato(new Dato("productoOrigen", tipCuentaOrigen));
        xfmlBuffer.addDato(new Dato("productoDestino", tipCuentaDestino));

        long montoTrans = (long)monto;
        final int largoRellenoMontoDerecha = 12;
        final int largoRellenoMontoIzquierda = 10;
        final int largoRellenoBanco = 4;
        final int largoRellenoCuenta = 20;
        final int largoNombre = 100;
        char relleno = '0';

        xfmlBuffer.addDato(new Dato("monto", (StringUtil.rellenaPorLaDerecha((StringUtil.rellenaPorLaIzquierda(
                String.valueOf(montoTrans), largoRellenoMontoIzquierda, relleno)),
                largoRellenoMontoDerecha, relleno))));
        logCliente(cliente,canal,"enviarTrfOperador", "montoTrans: "+montoTrans);
        logCliente(cliente,canal,"enviarTrfOperador", "monto: "+ (StringUtil.rellenaPorLaDerecha(
                (StringUtil.rellenaPorLaIzquierda(String.valueOf(montoTrans),
                        largoRellenoMontoIzquierda, relleno)), largoRellenoMontoDerecha, relleno)));
        xfmlBuffer.addDato(new Dato("traceNumber",
                StringUtil.rellenaPorLaIzquierda(traNumber, largoRellenoMontoDerecha, relleno)));
        logCliente(cliente,canal,"enviarTrfOperador", "traceNumber: "+traNumber);
        xfmlBuffer.addDato(new Dato("hora", hora));
        logCliente(cliente,canal,"enviarTrfOperador", "hora: "+hora);
        xfmlBuffer.addDato(new Dato("fecha", (new SimpleDateFormat("yyMMdd").format(fecha))));
        logCliente(cliente,canal,"enviarTrfOperador", "fecha: "+(new SimpleDateFormat("yyMMdd").format(fecha)));
        xfmlBuffer.addDato(new Dato("moneda", (TablaValores.getValor(TTFF_CONFIG, "tipoMoneda", "Peso"))));
        logCliente(cliente,canal,"enviarTrfOperador", "moneda: "
                + (TablaValores.getValor(TTFF_CONFIG, "tipoMoneda", "Peso")));
        xfmlBuffer.addDato(new Dato("bancoOrigen",
                (StringUtil.rellenaPorLaIzquierda(codigoBancoBCI, largoRellenoBanco, relleno))));
        logCliente(cliente,canal,"enviarTrfOperador", "bancoOrigen: "
                + (StringUtil.rellenaPorLaIzquierda(codigoBancoBCI, largoRellenoBanco, relleno)));
        xfmlBuffer.addDato(new Dato("cuentaOrigen",
                (StringUtil.rellenaPorLaIzquierda(ctaOrigen, largoRellenoCuenta, relleno))));
        logCliente(cliente,canal,"enviarTrfOperador", "cuentaOrigen: "
                + (StringUtil.rellenaPorLaIzquierda(ctaOrigen, largoRellenoCuenta, relleno)));

        String rutGir = StringUtil.rellenaPorLaIzquierda(
                (String.valueOf(rut)+dv), largoRellenoMontoDerecha, relleno);
        xfmlBuffer.addDato(new Dato("rutGirador", rutGir));
        logCliente(cliente,canal,"enviarTrfOperador", "rutGir: "+rutGir);

        String nombreCliente = cliente.getFullName().length() > largoNombre
                ? cliente.getFullName().substring(0, largoNombre) : cliente.getFullName();
                logCliente(cliente,canal,"enviarTrfOperador", "nombreCliente if: "+nombreCliente);

                xfmlBuffer.addDato(new Dato("nombreGirador", nombreCliente));
                xfmlBuffer.addDato(new Dato("bancoDestino",
                        (StringUtil.rellenaPorLaIzquierda(codBancoDestino, largoRellenoBanco, relleno))));
                logCliente(cliente,canal,"enviarTrfOperador", "bancoDestino: "
                        + (StringUtil.rellenaPorLaIzquierda(codBancoDestino, largoRellenoBanco, relleno)));
                xfmlBuffer.addDato(new Dato("cuentaDestino",
                        (StringUtil.rellenaPorLaIzquierda(ctaDestino, largoRellenoCuenta, relleno))));
                logCliente(cliente,canal,"enviarTrfOperador", "cuentaDestino: "
                        + (StringUtil.rellenaPorLaIzquierda(ctaDestino, largoRellenoCuenta, relleno)));

                String rutDest = StringUtil.rellenaPorLaIzquierda(
                        (String.valueOf(rutDestino)+digDestino), largoRellenoMontoDerecha, relleno);
                xfmlBuffer.addDato(new Dato("rutDestinatario", rutDest));
                logCliente(cliente,canal,"enviarTrfOperador", "rutDest: "+rutDest);
                if (nombreDestino != null && !nombreDestino.trim().equals("")){
                    String nomDestino = nombreDestino.length() > largoNombre
                            ? nombreDestino.substring(0, largoNombre) : nombreDestino;
                            xfmlBuffer.addDato(new Dato("nombreDestinatario", nomDestino));
                            logCliente(cliente,canal,"enviarTrfOperador", "nomDestino: "+nomDestino);
                }
                else{
                    xfmlBuffer.addDato(new Dato("nombreDestinatario", " "));
                    logCliente(cliente,canal,"enviarTrfOperador", "nomDestino: ");
                }
                if (comentario != null){
                    xfmlBuffer.addDato(new Dato("referencia", comentario));
                }
                else{
                    xfmlBuffer.addDato(new Dato("referencia", " "));
                }
                logCliente(cliente,canal,"enviarTrfOperador", "comentario: "+comentario);
                xfmlBuffer.addDato(new Dato("nroOperacion", numeroOperacion));
                logCliente(cliente,canal,"enviarTrfOperador", "numeroOperacion: "+numeroOperacion);


                if (!macOrigen.trim().equals("")){
                    xfmlBuffer.addDato(new Dato("macOrigen", macOrigen.trim()));
                    String largoMac = TablaValores.getValor(TTFF_CONFIG, "MAC", "largo");
                    xfmlBuffer.addDato(new Dato("largoMacOrigen", largoMac));
                    xfmlBuffer.addDato(new Dato("largoMacCCA", largoMac));
                    xfmlBuffer.addDato(new Dato("macCCA", TablaValores.getValor(TTFF_CONFIG, "MAC", "clave")));
                }
                else{
                    xfmlBuffer.addDato(new Dato("largoMacOrigen", "0"));
                    xfmlBuffer.addDato(new Dato("largoMacCCA", "0"));
                }

                String xfmlRequest = xfmlBuffer.getXFMLString();
                logCliente(cliente,canal,"enviarTrfOperador", xfmlRequest);
                return xfmlRequest;
    }

     /**
     * Actualiza los datos de un alias de transferencias.
     *
     * <p>Registro de versiones:
     * <ul>
     * <li> 1.0 Eduardo Villagr�n Morales (Imagemaker): Versi�n inicial</li>
     * </ul>
     *
     * @param alias AliasTTFFDVO con los datos para actualizar
     * @throws BusinessException
     * @throws com.schema.util.GeneralException
     * @throws Exception
     * @since 2.3
     */
	public void actualizarAliasTransferencias(AliasTTFFDVO alias) throws BusinessException, Exception,
		com.schema.util.GeneralException{
		if (logger.isDebugEnabled()){
			logger.debug("[TransferenciasFondosBean] Actualizando alias TTFF " + alias.getCodigo());
		}
		ttffDAO.actualizarAliasTTFF(alias);
		if (logger.isDebugEnabled()){
			logger.debug("[TransferenciasFondosBean] Actualizado alias TTFF " + alias.getCodigo());
		}
	}
    /**
     * M�todo migrado desde {@link TransferenciasFondosBean}, encargo de obtener el saldo de la linea de
     * sobregiro. Se agregan los par�metros long rut, char dig y String canal.
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 27/07/2012 Jorge Leiva A. (TINet): Versi�n inicial.
     * </ul>
     * <p>
     *
     * @param cta cuenta asociada al cliente.
     * @param rut rut del cliente.
     * @param dig digito verficador.
     * @param canal identificador del canal.
     * @return LineaSobreGiro Saldo de la linea de sobregiro.
     * @throws TransferenciaException excepci�n.
     * @throws RemoteException excepci�n.
     * @since 2.4
     */
    public LineaSobregiro obtenerLineaSobreGiro(String cta, long rut, char dig, String canal)
        throws TransferenciaException, RemoteException {
        try {
            logCliente("SALDOLDC", "SALDO LDC [" + cta + "]", rut, dig, canal);
            CuentaCorriente cuentaCorriente = new CuentaCorriente(cta);
            return cuentaCorriente.getLineaSobregiro();
        }
        catch (Exception e) {
            String mensaje = "Cuenta=" + cta;
            logCliente("SALDOLDC", mensaje, e, rut, dig, canal);
            if (logger.isEnabledFor(Level.ERROR)) {
                logger.error("[obtenerLineaSobreGiro]", e);
            }
            throw new TransferenciaException("TRF001");
        }
    }

    /**
     * M�todo migrado desde {@link TransferenciasFondosBean}, encargado de obtener el tipo de mandato del cliente.
     * A: Abierto, C: Cerrado y N: sin mandato.
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 27/07/2012 Jorge Leiva A. (TINet): Versi�n inicial.
     * </ul>
     * <p>
     *
     * @param rut rut del cliente.
     * @param tipoCta tipo de cuenta.
     * @param cta cuenta asociada.
     * @return TipoMandatoVO[] con los arreglos del mandato.
     * @since 2.4
     */
    public TipoMandatoVO[] obtieneTipoMandato(String rut, String tipoCta, String cta) {
        try {
            CuentasDAO cuentasDAO = new CuentasDAO();
            return cuentasDAO.obtieneTipoMandato(rut, tipoCta, cta);
        }
        catch (TuxedoException e) {
            if (logger.isEnabledFor(Level.ERROR)) {
                logger.error("[obtieneTipoMandato] Excepcion", e);
            }
        }
        catch (GeneralException e) {
            if (logger.isEnabledFor(Level.ERROR)) {
                logger.error("[obtieneTipoMandato] Excepcion", e);
            }
        }
        catch (CreateException e) {
            if (logger.isEnabledFor(Level.ERROR)) {
                logger.error("[obtieneTipoMandato] Excepcion", e);
            }
        }
        return null;
    }

    /**
     * M�todo migrado desde {@link TransferenciasFondosBean}, encargado de obtener el listado de las �ltimas 10
     * transferencias. Se agregan los par�metros long rut, char dig y String canal.
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 27/07/2012 Jorge Leiva A. (TINet): Versi�n inicial.
     * </ul>
     * <p>
     *
     * @param rut rut del cliente.
     * @param dig digito verificador.
     * @param canal identificador del canal.
     * @return DetalleTransferencia[] listado de las �ltimas 10 transferencias.
     * @throws TransferenciaException Excepci�n.
     * @since 2.4
     */
    public DetalleTransferencia[] listaUltimasTransferencias(long rut, char dig, String canal)
        throws TransferenciaException {
        try {
            return serviciosCuentas.listaUltimasTransferencias(rut);
        }
        catch (Exception e) {
            logCliente("listaUltimasTransferencias", "", e, rut, dig, canal);
            if (logger.isEnabledFor(Level.ERROR)) {
                logger.error("[listaUltimasTransferencias] Excepcion", e);
            }
            throw new TransferenciaException("TRF001");
        }
    }

    /**
     * M�todo agregado para mantener compatibilidad ya que se agrega parametro para validaci�n de cuentas
     * cash pooling.
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 15/11/2013 Francisco Gonz�lez V. (TINet): Versi�n inicial.</li>
     * </ul>
     * </p>
     *
     * @param ctaOrigen cuenta origen.
     * @param tipoCuentaOrigen tipo de cuenta origen.
     * @param ctaDestino cuenta destino.
     * @param tipoCuentaDestino tipo cuenta destino.
     * @param monto monto a transferir.
     * @param rutDestino rut del destinatario.
     * @param digDestino digito verificador del destinatario.
     * @param comentario comentario.
     * @param nombreDestino nombre del destinatario.
     * @param eMailCliente email del cliente.
     * @param eMailDestino email del destinatario.
     * @param tipoAutenticacion tipo de autentificaci�n.
     * @param tipoTarj tipo de tarjeta.
     * @param nemonico nemonico.
     * @param cliente objeto con los datos del cliente.
     * @param canal identificador del canal.
     * @param cashPooling indica si la cta cte pertenece a grupo cash pooling.
     * @return DetalleTransferencia detalle de la transferencia realizada.
     * @throws TransferenciaException Excepci�n.
     * @since 3.0
     */
    public DetalleTransferencia transfiereCtaCteHaciaTdc(String ctaOrigen, String tipoCuentaOrigen,
            String ctaDestino, String tipoCuentaDestino, double monto, long rutDestino, char digDestino,
            String comentario, String nombreDestino, String eMailCliente, String eMailDestino,
            String tipoAutenticacion, String tipoTarj, String nemonico, Cliente cliente, String canal)
            throws TransferenciaException {

        return transfiereCtaCteHaciaTdc(ctaOrigen, tipoCuentaOrigen, ctaDestino, tipoCuentaDestino, monto,
                rutDestino, digDestino, comentario, nombreDestino, eMailCliente, eMailDestino, tipoAutenticacion,
                tipoTarj, nemonico, cliente, canal, false);
    }

    /**
     * M�todo migrado desde {@link TransferenciasFondosBean}, encargado de obtener el detalle de la transferencia
     * realizada entre una cuenta corriente y una tarjeta. Se agregan los par�metros Cliente cliente y String
     * canal. Se agrega la constante FORMATO_CUENTA y INICIO_TRANSFERENCIA que reemplazan los valores en duro
     * "00000000" y "INICIO TRANSFERENCIA".
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 27/07/2012 Jorge Leiva A. (TINet): Versi�n inicial.
     * <li>1.1 15/11/2013 Francisco Gonz�lez V (SEnTRA): Se agrega parametro cashPooling para saltar validaci�n
     *                                                  de saldo, para evitar todo impacto de este cambio se agrega
     *                                                  m�todo {@link #transfiereCtaCteHaciaTdc(String, String,
     *                                                  String, String, double, long, char, String, String, String,
     *                                                  String, String, String, String, Cliente, String)}</li>
     * </ul>
     * <p>
     *
     * @param ctaOrigen cuenta origen.
     * @param tipoCuentaOrigen tipo de cuenta origen.
     * @param ctaDestino cuenta destino.
     * @param tipoCuentaDestino tipo cuenta destino.
     * @param monto monto a transferir.
     * @param rutDestino rut del destinatario.
     * @param digDestino digito verificador del destinatario.
     * @param comentario comentario.
     * @param nombreDestino nombre del destinatario.
     * @param eMailCliente email del cliente.
     * @param eMailDestino email del destinatario.
     * @param tipoAutenticacion tipo de autentificaci�n.
     * @param tipoTarj tipo de tarjeta.
     * @param nemonico nemonico.
     * @param cliente objeto con los datos del cliente.
     * @param canal identificador del canal.
     * @param cashPooling indica si la cta cte pertenece a grupo cash pooling.
     * @return DetalleTransferencia detalle de la transferencia realizada.
     * @throws TransferenciaException Excepci�n.
     * @since 2.4
     */
    public DetalleTransferencia transfiereCtaCteHaciaTdc(String ctaOrigen, String tipoCuentaOrigen,
        String ctaDestino, String tipoCuentaDestino, double monto, long rutDestino, char digDestino,
        String comentario, String nombreDestino, String eMailCliente, String eMailDestino,
        String tipoAutenticacion, String tipoTarj, String nemonico, Cliente cliente, String canal,
        boolean cashPooling) throws TransferenciaException {
        String mensaje = INICIO_TRANSFERENCIA;

        long rut = cliente.getRut();
        char dig = cliente.getDigito();

        logCliente("CTACTE_TDC", ctaOrigen, tipoCuentaOrigen, ctaDestino, tipoCuentaDestino, monto, rutDestino,
            digDestino, "016", mensaje, rut, dig, canal);

        String codBanco = TablaValores.getValor(TTFF_CONFIG, "codigoBCI", "valor");

        ctaOrigen = Formatting.number(Long.parseLong(ctaOrigen), FORMATO_CUENTA);

        Date fechaCargo = new Date();
        Date fechaAbono = new Date();

        try {
            new CuentaCorriente(ctaOrigen).getSaldo();
        }
        catch (Exception e) {
            mensaje = "SaldoCtaCte" + ", CuentaOrigen =[" + ctaOrigen + "]" + ", CuentaDestino=[" + ctaDestino
                + "]" + ", Monto=[" + monto + "]";
            logCliente("CTACTE_TDC", mensaje, e, rut, dig, canal);
            if (logger.isEnabledFor(Level.ERROR)) {
                logger.error("[transfiereCtaCteHaciaTdc] Excepcion", e);
            }
            throw new TransferenciaException("TRF011");
        }

        double montoProgramado = 0.0;
        try {
            montoProgramado = montoProgramadoDia(rut, tipoCuentaOrigen, ctaOrigen, new Date());
        }
        catch (Exception e) {
            mensaje = "MontoProgramado" + ", CuentaOrigen =[" + ctaOrigen + "]" + ", CuentaDestino=[" + ctaDestino
                + "]" + ", Monto=[" + monto + "]";
            logCliente("CTACTE_TDC", mensaje, e, rut, dig, canal);
            if (logger.isEnabledFor(Level.ERROR)) {
                logger.error("[transfiereCtaCteHaciaTdc] montoProgramado Excepcion", e);
            }
            throw new TransferenciaException("TRF001");
        }

        // Debemos validar el monto a transferir + el monto programado
        // independiente de que la programaci�n haya sido efectuada ya.
        if(!cashPooling){
        if (!validarSaldoCtaCte(cliente, canal, ctaOrigen, monto + montoProgramado, true)) {
            throw new TransferenciaException("0020");
        }
        }

        /*
         * Registramos la transferencia y obtenemos de ella el numero de operaci�n y la fecha de cargo
         */
        RegistroTransferencia reg = null;
        try {
            String estadoIngreso = TablaValores.getValor(TTFF_CONFIG, "estados", "ingreso");

            reg = registraIngresoTrf(tipoCuentaOrigen, ctaOrigen, monto, fechaCargo, estadoIngreso, fechaAbono,
                tipoCuentaDestino, ctaDestino, rutDestino, codBanco, comentario, nombreDestino, eMailDestino, rut,
                dig, canal);
        }
        catch (Exception e) {
            mensaje = "REG_ITF" + ", CuentaOrigen =[" + ctaOrigen + "]" + ", CuentaDestino=[" + ctaDestino + "]"
                + ", Monto=[" + monto + "]";
            logCliente("CTACTE_TDC", mensaje, e, rut, dig, canal);
            if (logger.isEnabledFor(Level.ERROR)) {
                logger.error("[transfiereCtaCteHaciaTdc] registraIngresoTrf Excepcion", e);
            }
            throw new TransferenciaException("TRF011");
        }

        boolean montoValido = true;
        try {
            double topeBanco = 0;
            try {
                topeBanco = maximoTopeCuenta(rut, canal, tipoCuentaOrigen, rutDestino, tipoAutenticacion);
            }
            catch (Exception e) {
                logCliente("CTACTE_TDC", "Fallo por no poder obtener TOPE BANCO", e, rut, dig, canal);
                registraFinTrf(cliente, canal, Long.parseLong(reg.numeroOperacion), null, null, fechaCargo,
                    fechaAbono, TablaValores.getValor(TTFF_CONFIG, "estados", "finError"));
                montoValido = false;
                if (logger.isEnabledFor(Level.ERROR)) {
                    logger.error("[transfiereCtaCteHaciaTdc] Excepcion General ", e);
                }
                throw new TransferenciaException("TRF001");
            }

            double montoDia = 0;
            try {
                montoDia = montoTransferidoDia(rut, tipoCuentaOrigen, ctaOrigen, new Date());
            }
            catch (Exception e) {
                logCliente("CTACTE_TDC", "Fallo por no poder obtener MONTO TRANSFERIDO DIA", e, rut, dig, canal);
                registraFinTrf(cliente, canal, Long.parseLong(reg.numeroOperacion), null, null, fechaCargo,
                    fechaAbono, TablaValores.getValor(TTFF_CONFIG, "estados", "finError"));
                montoValido = false;
                if (logger.isEnabledFor(Level.ERROR)) {
                    logger.error("[transfiereCtaCteHaciaTdc] Excepcion General ", e);
                }
                throw new TransferenciaException("TRF001");
            }

            if (rut != rutDestino) {
                if (monto > (topeBanco - montoDia - montoProgramado)) {
                    mensaje = "ValidaMonto" + ", Monto=" + monto + ", TopeBanco=" + topeBanco + ", montoDia="
                        + montoDia + ", montoProgramado=" + montoProgramado;
                    logCliente("CTACTE_TDC", mensaje, rut, dig, canal);
                    registraFinTrf(cliente, canal, Long.parseLong(reg.numeroOperacion), null, null, fechaCargo,
                        fechaAbono, TablaValores.getValor(TTFF_CONFIG, "estados", "finError"));
                    montoValido = false;
                    throw new TransferenciaException("TRF042");
                }
            }
            logCliente("TDC_CTACTE", "EfectuandoTransferencia, MontoDia=" + montoDia + ", TopeBanco=" + topeBanco
                + ", Programado=" + montoProgramado, cliente.getRut(), cliente.getDigito(), canal);
            if (tipoTarj != null) {
                realizarTransferenciaCtaCteHaciaTdc(ctaOrigen, ctaDestino, monto, tipoTarj, nemonico, rut, dig,
                    canal);
            }
            else {
                realizarTransferenciaCtaCteHaciaTdc(ctaOrigen, ctaDestino, monto, nemonico, rut, dig, canal);
            }
        }
        catch (TransferenciaException e) {
            if (montoValido) {
                registraFinTrf(cliente, canal, Long.parseLong(reg.numeroOperacion), null, null, fechaCargo,
                    fechaAbono, TablaValores.getValor(TTFF_CONFIG, "estados", "finError"));
            }
            if (logger.isEnabledFor(Level.ERROR)) {
                logger.error("[transfiereCtaCteHaciaTdc] TransferenciaException General ", e);
            }
            throw e;
        }

        /*
         * Se debe tener presente que tanto la fecha de Cargo como la fecha de Abono debieran ser las contables que
         * tiene tandem. El problema es que la transacci�n B0671 NO retorna dicha fecha y no existe alguna forma de
         * obtenerla. Por lo tanto se decidi� mientras manejar la fecha del proceso como fechas de Cargo y Abono
         * (4to y 5to par�metro)
         */
        registraFinTrf(cliente, canal, Long.parseLong(reg.numeroOperacion), null, null, fechaCargo, fechaAbono,
            TablaValores.getValor(TTFF_CONFIG, "estados", "fin"));

        DetalleTransferencia detalle = new DetalleTransferencia();
        detalle.ctaOrigen = ctaOrigen;
        detalle.tipoCuentaOrigen = tipoCuentaOrigen;
        detalle.ctaDestino = ctaDestino;
        detalle.tipoCuentaDestino = tipoCuentaDestino;
        detalle.montoTransferencia = monto;
        detalle.numeroOperacion = reg.numeroOperacion;
        detalle.codigoBancoDestino = codBanco;
        detalle.codigoBancoOrigen = codBanco;
        detalle.rut = cliente.getRut();
        detalle.dig = cliente.getDigito();
        detalle.rutDestino = rutDestino;
        detalle.digDestino = digDestino;
        detalle.nombreDestino = nombreDestino;
        detalle.comentario = comentario;
        detalle.canalID = canal;
        detalle.eMailCliente = eMailCliente; // Puede ser null
        detalle.eMailDestino = eMailDestino; // Puede ser null
        detalle.fechaTransferencia = fechaCargo;

        detalle.fechaCargo = fechaCargo;
        detalle.fechaAbono = fechaAbono;

        detalle.nombreOrigen = cliente.getFullName();
        detalle.nombreBancoOrigen = TablaValores.getValor(TTFF_CONFIG, "codigoBCI", "desc_" + canal);
        detalle.nombreBancoDestino = TablaValores.getValor(TTFF_CONFIG, "codigoBCI", "desc_" + canal);

        enviaEmail(detalle, false, cliente, canal);
        enviaMailCliente(detalle, cliente, canal);

        logCliente("CTACTE_TDC", "FIN TRANSFERENCIA", rut, dig, canal);
        return detalle;
    }

    /**
     * M�todo migrado desde {@link TransferenciasFondosBean}, encargo de registrar el ingreso de la transferencia.
     * Se agregan los par�metros long rut, char dig y String canal.
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 27/07/2012 Jorge Leiva A. (TINet): Versi�n inicial.
     * </ul>
     * <p>
     *
     * @param tipoCuentaOrigen tipo cuenta origen.
     * @param ctaOrigen cuenta origen.
     * @param monto monto a transferir.
     * @param fechaCargo fecha del cargo.
     * @param estado estado.
     * @param fechaAbono fecha abono.
     * @param tipoCuentaDestino tipo de cuenta destino.
     * @param ctaDestino cuenta destino.
     * @param rutDestino rut del destinatario.
     * @param codigoBanco c�digo del banco.
     * @param comentario comentario.
     * @param nombreDestino nombre del destinatario.
     * @param eMailDestino email del destinatario.
     * @param rut rut del cliente.
     * @param dig digito verificador.
     * @param canal identificador del canal.
     * @return RegistroTransferencia transferencia realizada.
     * @throws CuentasException excepci�n.
     * @throws TuxedoException excepci�n del servicio tuxedo.
     * @throws RemoteException excepci�n.
     * @since 2.4
     */
    private RegistroTransferencia registraIngresoTrf(String tipoCuentaOrigen, String ctaOrigen, double monto,
        Date fechaCargo, String estado, Date fechaAbono, String tipoCuentaDestino, String ctaDestino,
        long rutDestino, String codigoBanco, String comentario, String nombreDestino, String eMailDestino,
        long rut, char dig, String canal) throws CuentasException, TuxedoException, RemoteException {
        if (nombreDestino != null) {
            if (nombreDestino.length() > LARGO_NOMBRE)
                nombreDestino = nombreDestino.substring(0, LARGO_NOMBRE - 1);
        }

        String msg = "";

        msg = "tipoCuentaOrigen=" + tipoCuentaOrigen;
        msg += ", ctaOrigen=" + ctaOrigen;
        msg += ", monto=" + monto;
        msg += ", fechaCargo=" + (fechaCargo != null ? fechaCargo.toString() : null);
        msg += ", estado=" + estado;
        msg += ", fechaAbono=" + (fechaAbono != null ? fechaAbono.toString() : null);
        msg += ", tipoCuentaDestino=" + tipoCuentaDestino;
        msg += ", rutDestino=" + rutDestino;
        msg += ", codigoBanco=" + codigoBanco;
        msg += ", comentario=" + comentario;
        msg += ", nombreDestino=" + nombreDestino;
        msg += ", eMailDestino=" + eMailDestino;
        logCliente("INGRESAESTADO", msg, rut, dig, canal);

        RegistroTransferencia reg = serviciosCuentas.ingresaTransferencia(rut, tipoCuentaOrigen, ctaOrigen, monto,
            fechaCargo, estado, fechaAbono, tipoCuentaDestino, ctaDestino, rutDestino, codigoBanco, canal, null,
            null, comentario, nombreDestino, null, eMailDestino, String.valueOf(rutDestino));
        return reg;
    }

    /**
     * M�todo migrado desde {@link TransferenciasFondosBean}, encargado de realzar la transferencias entre una
     * cuenta corriente y una tarjeta de cr�dito. Se agregan los par�metros long rut, char dig y String canal.
     * Se agrega la constante CARACTER_TIPO_TARJETA que reemplaza el valor en duro 'M'.
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 27/07/2012 Jorge Leiva A. (TINet): Versi�n inicial.
     * </ul>
     * <p>
     *
     * @param ctaOrigen cuenta origen.
     * @param ctaDestino cuenta destino.
     * @param monto monto a transferir.
     * @param tipoTarj tipo de tarjeta.
     * @param nemonico nemonico.
     * @param rut rut del cliente.
     * @param dig digito verificador.
     * @param canal identificador del canal.
     * @return TransferenciaFondos transferencia realizada.
     * @throws TransferenciaException excepci�n.
     * @since 2.4
     */
    private TransferenciaFondos realizarTransferenciaCtaCteHaciaTdc(String ctaOrigen, String ctaDestino,
        double monto, String tipoTarj, String nemonico, long rut, char dig, String canal)
        throws TransferenciaException {

        logger.debug("ctaDestino::" + ctaDestino + "::tipoTarj::" + tipoTarj);
        tipoTarj = TablaValores.getValor(TTFF_CONFIG, "tipoTarj", tipoTarj);

        if ((tipoTarj != null) && (tipoTarj.length() > 0) && (tipoTarj.charAt(0) == CARACTER_TIPO_TARJETA)) {
            try {
                logCliente("PAGOMASTER", "ctaOrigen=" + ctaOrigen + ", ctaDestino=" + ctaDestino + ", monto="
                    + monto, rut, dig, canal);
                return serviciosCuentas.pagoMasterCtaCte(ctaOrigen, ctaDestino, monto, nemonico);
            }
            catch (Exception e) {
                String mensaje = "CuentaOrigen=" + ctaOrigen + ", CuentaDestino=" + ctaDestino + ", MontoTrf="
                    + monto;
                logCliente("PAGOMASTER", mensaje, e, rut, dig, canal);
                if (logger.isEnabledFor(Level.ERROR)) {
                    logger.error("[realizarTransferenciaCtaCteHaciaTdc] pagoMasterCtaCte ", e);
                }
                throw new TransferenciaException("TRF001");
            }
        }
        else {
            try {
                logCliente("PAGOVISA", "ctaOrigen=" + ctaOrigen + ", ctaDestino=" + ctaDestino + ", monto="
                    + monto + ", nemonico=" + nemonico, rut, dig, canal);
                return serviciosCuentas.pagoVisaCtaCte(ctaOrigen, ctaDestino, monto, nemonico);
            }
            catch (Exception e) {
                String mensaje = "CuentaOrigen=" + ctaOrigen + ", CuentaDestino=" + ctaDestino + ", MontoTrf="
                    + monto;
                logCliente("PAGOVISA", mensaje, e, rut, dig, canal);
                if (logger.isEnabledFor(Level.ERROR)) {
                    logger.error("[realizarTransferenciaCtaCteHaciaTdc] pagoVisaCtaCte ", e);
                }
                throw new TransferenciaException("TRF001");
            }
        } // fin else
    }

    /**
     * M�todo migrado desde {@link TransferenciasFondosBean}, encargado de realzar la transferencias entre una
     * cuenta corriente y una tarjeta de cr�dito. Se agregan los par�metros long rut, char dig y String canal.
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 27/07/2012 Jorge Leiva A. (TINet): Versi�n inicial.
     * </ul>
     * <p>
     *
     * @param ctaOrigen cuenta de origen.
     * @param ctaDestino cuenta de destino.
     * @param monto monto a transferir.
     * @param nemonico nemonico.
     * @param rut rut del cliente.
     * @param dig digito verificador.
     * @param canal identificador del canal.
     * @return TransferenciaFondos transferencia realizada.
     * @throws TransferenciaException excepci�n.
     * @since 2.4
     */
    private TransferenciaFondos realizarTransferenciaCtaCteHaciaTdc(String ctaOrigen, String ctaDestino,
        double monto, String nemonico, long rut, char dig, String canal) throws TransferenciaException {
        try {
            logCliente("PAGOVISA", "ctaOrigen=" + ctaOrigen + ", ctaDestino=" + ctaDestino + ", monto=" + monto,
                rut, dig, canal);
            return serviciosCuentas.pagoVisaCtaCte(ctaOrigen, ctaDestino, monto, nemonico);
        }
        catch (Exception e) {
            String mensaje = "CuentaOrigen=" + ctaOrigen + ", CuentaDestino=" + ctaDestino + ", MontoTrf=" + monto;
            logCliente("PAGOVISA", mensaje, e, rut, dig, canal);
            if (logger.isEnabledFor(Level.ERROR)) {
                logger.error("[realizarTransferenciaCtaCteHaciaTdc] pagoVisaCtaCte ", e);
            }
            throw new TransferenciaException("TRF001");
        }
    }

    /**
     * M�todo migrado desde {@link TransferenciasFondosBean}, encargado de registrar una transferencia.
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 27/07/2012 Jorge Leiva A. (TINet): Versi�n inicial.
     * </ul>
     * <p>
     *
     * @param cliente objeto con los datos del cliente.
     * @param canal identificador del canal.
     * @param numeroOperacion n�mero de la operaci�n.
     * @param folio folio.
     * @param numero n�mero.
     * @param fechaCargo fecha del cargo.
     * @param fechaAbono fecha del abono.
     * @param estado estado.
     * @return boolena
     * @since 2.4
     */
    private boolean registraFinTrf(Cliente cliente, String canal, long numeroOperacion, String folio,
        String numero, Date fechaCargo, Date fechaAbono, String estado) {
        long rut = cliente.getRut();
        char dig = cliente.getDigito();
        try {
            String mensaje = "";
            mensaje = "NumeroOperacion=" + numeroOperacion + ", Folio=" + folio + ", Numero=" + numero
                + ", FechaCargo=" + (fechaCargo != null ? fechaCargo.toString() : "--/--/----") + ", FechaAbono="
                + (fechaAbono != null ? fechaAbono.toString() : "--/--/----") + ", Estado=" + estado;

            logCliente("MODIFICAESTADO", mensaje, rut, dig, canal);
            serviciosCuentas.modificaEstadoTransferencia(numeroOperacion, folio, numero, fechaCargo, fechaAbono,
                estado, String.valueOf(rut));
            return true;
        }
        catch (Exception e) {
            String mensaje = "NUM_OPE=" + numeroOperacion + ", FOLIO=" + folio + ", NUMERO=" + numero
                + ", FECCARGO=" + (fechaCargo != null ? fechaCargo.toString() : "--/--/----") + ", FECABONO="
                + (fechaAbono != null ? fechaAbono.toString() : "--/--/----") + ", ESTADO=" + estado;
            logCliente("MODIFICAESTADO", mensaje, e, rut, dig, canal);
            if (logger.isEnabledFor(Level.ERROR)) {
                logger.error("[registraFinTrf] modificaEstadoTransferencia ", e);
            }
            return false;
        }
    }

    /**
     * M�todo migrado desde {@link TransferenciasFondosBean}, encargado de transferir fondos desde una cuenta de
     * ahorros a una cuenta corriente. Se agregan los parametros Cliente cliente y String canal.
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 27/07/2012 Jorge Leiva A. (TINet): Versi�n inicial.
     * </ul>
     * <p>
     *
     * @param ctaOrigen cuenta de origen.
     * @param ctaDestino cuenta de destino.
     * @param tipoCuentaDestino tipo de cuenta de destino.
     * @param monto monto a transferir.
     * @param rutDestino rut del destinatario.
     * @param digDestino digito verificador del destinatario.
     * @param comentario comentario.
     * @param nombreDestino nombre del destinatario.
     * @param eMailCliente email de cliente.
     * @param eMailDestino email del destinatario.
     * @param tipoAutenticacion tipo de autentificaci�n.
     * @param cliente objeto con los datos del cliente.
     * @param canal identificador del canal.
     * @return DetalleTransferencia detalle de la transferencia realizada.
     * @throws TransferenciaException excepci�n.
     * @since 2.4
     */
    public DetalleTransferencia transfiereAhorroHaciaCtaCte(String ctaOrigen, String ctaDestino,
        String tipoCuentaDestino, double monto, long rutDestino, char digDestino, String comentario,
        String nombreDestino, String eMailCliente, String eMailDestino, String tipoAutenticacion, Cliente cliente,
        String canal) throws TransferenciaException {

        return transfiereAhorroHaciaCtaCte(ctaOrigen, ctaDestino, tipoCuentaDestino, monto, rutDestino,
            digDestino, comentario, nombreDestino, eMailCliente, eMailDestino, tipoAutenticacion, false, cliente,
            canal);
    }

    /**
     * M�todo migrado desde {@link TransferenciasFondosBean}, encargado de transferir fondos desde una cuenta de
     * ahorros a una cuenta corriente. Se agregan los parametros Cliente cliente y String canal. Se agrega la
     * constante INICIO_TRANSFERENCIA que reemplaza el valor en duro "INICIO TRANSFERENCIA". Se agrega la constante
     * FORMATO_CUENTA que reemplaza el valor en duro "00000000".
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 27/07/2012 Jorge Leiva A. (TINet): Versi�n inicial.
     * </ul>
     * <p>
     *
     * @param ctaOrigen cuenta de origen.
     * @param ctaDestino cuenta de destino.
     * @param tipoCuentaDestino tipo de cuenta de destino.
     * @param monto monto a transferir.
     * @param rutDestino rut del destinatario.
     * @param digDestino digito verificador del destinatario.
     * @param comentario comentario.
     * @param nombreDestino nombre del destinatario.
     * @param eMailCliente email del cliente.
     * @param eMailDestino email del destinatario.
     * @param tipoAutenticacion tipo de autentificaci�n.
     * @param excesoGiros exceso de giros.
     * @param cliente objeto con los datos del cliente.
     * @param canal identificador de canal.
     * @return DetalleTransferencia detalle de la transferencia realizada.
     * @throws TransferenciaException excepci�n.
     * @since 2.4
     */
    public DetalleTransferencia transfiereAhorroHaciaCtaCte(String ctaOrigen, String ctaDestino,
        String tipoCuentaDestino, double monto, long rutDestino, char digDestino, String comentario,
        String nombreDestino, String eMailCliente, String eMailDestino, String tipoAutenticacion,
        boolean excesoGiros, Cliente cliente, String canal) throws TransferenciaException {
        String mensaje = INICIO_TRANSFERENCIA;
        long rut = cliente.getRut();
        char dig = cliente.getDigito();
        logCliente("AHO_CTACTE", ctaOrigen, "AHO", ctaDestino, tipoCuentaDestino, monto, rutDestino, digDestino,
            "016", mensaje, rut, dig, canal);
        String codBanco = TablaValores.getValor(TTFF_CONFIG, "codigoBCI", "valor");
        String tipoCuentaOrigen = TablaValores.getValor(TTFF_CONFIG, "tiposCuenta", "ahorro");

        Date fechaCargo = new Date();
        Date fechaAbono = new Date();

        ctaDestino = Formatting.number(Long.parseLong(ctaDestino), FORMATO_CUENTA);

        // Validar saldo
        try {
            new CuentaAhorro(ctaOrigen).getSaldo();
        }
        catch (Exception e) {
            mensaje = "SaldoCtaCte" + ", CuentaOrigen =[" + ctaOrigen + "]" + ", CuentaDestino=[" + ctaDestino
                + "]" + ", Monto=[" + monto + "]";
            logCliente("AHO_CTACTE", mensaje, e, rut, dig, canal);

            TransferenciaException te = new TransferenciaException("TRF011");
            te.setException(e);
            if (logger.isEnabledFor(Level.ERROR)) {
                logger.error("[transfiereAhorroHaciaCtaCte] new CuentaAhorro ", e);
            }
            throw te;
        }

        double montoProgramado = 0.0;
        try {
            montoProgramado = montoProgramadoDia(rut, tipoCuentaOrigen, ctaOrigen, new Date());
        }
        catch (Exception e) {
            mensaje = "MontoProgramado" + ", CuentaOrigen =[" + ctaOrigen + "]" + ", CuentaDestino=[" + ctaDestino
                + "]" + ", Monto=[" + monto + "]";
            logCliente("AHO_CTACTE", mensaje, e, rut, dig, canal);
            if (logger.isEnabledFor(Level.ERROR)) {
                logger.error("[transfiereAhorroHaciaCtaCte]  montoProgramadoDia", e);
            }
            throw new TransferenciaException("TRF001");
        }

        // Debemos validar el monto a transferir + el monto programado
        // independiente de que la programaci�n haya sido efectuada ya.
        if (!validarSaldoAhorro(ctaOrigen, monto + montoProgramado, rut, dig, canal)) {
            throw new TransferenciaException("0020");
        }

        /*
         * Registramos la transferencia y obtenemos de ella el numero de operaci�n y la fecha de cargo
         */
        RegistroTransferencia reg = null;
        try {
            reg = registraIngresoTrf(tipoCuentaOrigen, ctaOrigen, monto, fechaCargo, TablaValores.getValor(
                TTFF_CONFIG, "estados", "ingreso"), fechaAbono, tipoCuentaDestino, ctaDestino, rutDestino,
                codBanco, comentario, nombreDestino, eMailDestino, rut, dig, canal);
        }
        catch (Exception e) {
            mensaje = "REG_ITF" + ", CuentaOrigen =[" + ctaOrigen + "]" + ", CuentaDestino=[" + ctaDestino + "]"
                + ", Monto=[" + monto + "]";
            logCliente("AHO_CTACTE", mensaje, e, rut, dig, canal);
            if (logger.isEnabledFor(Level.ERROR)) {
                logger.error("[transfiereAhorroHaciaCtaCte]  registraIngresoTrf", e);
            }
            throw new TransferenciaException("TRF001");
        }

        boolean montoValido = true;
        try {
            double topeBanco = 0.0;
            try {
                topeBanco = maximoTopeCuenta(rut, canal, tipoCuentaOrigen, rutDestino, tipoAutenticacion);
}
            catch (Exception e) {
                logCliente("AHO_CTACTE", "Fallo por no poder obtener TOPE BANCO", e, rut, dig, canal);
                registraFinTrf(Long.parseLong(reg.numeroOperacion), null, null, fechaCargo, fechaAbono,
                    TablaValores.getValor(TTFF_CONFIG, "estados", "finError"), rut, dig, canal);
                montoValido = false;
                if (logger.isEnabledFor(Level.ERROR)) {
                    logger.error("[transfiereAhorroHaciaCtaCte]  Excepcion General ", e);
                }
                throw new TransferenciaException("TRF001");
            }

            double montoDia = 0.0;
            try {
                montoDia = montoTransferidoDia(rut, tipoCuentaOrigen, ctaOrigen, new Date());
            }
            catch (Exception e) {
                logCliente("AHO_CTACTE", "Fallo por no poder obtener MONTO TRANSFERIDO DIA", e, rut, dig, canal);
                registraFinTrf(Long.parseLong(reg.numeroOperacion), null, null, fechaCargo, fechaAbono,
                    TablaValores.getValor(TTFF_CONFIG, "estados", "finError"), rut, dig, canal);
                montoValido = false;
                if (logger.isEnabledFor(Level.ERROR)) {
                    logger.error("[transfiereAhorroHaciaCtaCte]  Excepcion General ", e);
                }
                throw new TransferenciaException("TRF001");
            }

            if (rut != rutDestino) {
                if (monto > (topeBanco - montoDia - montoProgramado)) {
                    mensaje = "Monto no v�lido" + ", Monto=" + monto + ", TopeBanco=" + topeBanco + ", montoDia="
                        + montoDia + ", montoProgramado=" + montoProgramado;
                    logCliente("AHO_CTACTE", mensaje, rut, dig, canal);
                    registraFinTrf(Long.parseLong(reg.numeroOperacion), null, null, fechaCargo, fechaAbono,
                        TablaValores.getValor(TTFF_CONFIG, "estados", "finError"), rut, dig, canal);
                    montoValido = false;
                    throw new TransferenciaException("TRF042");
                }
            }
            logCliente("AHO_CTACTE", "EfectuandoTransferencia, MontoDia=" + montoDia + ", TopeBanco=" + topeBanco
                + ", Programado=" + montoProgramado, rut, dig, canal);
            realizarTransferenciaAhorroHaciaCtaCte(ctaOrigen, monto, ctaDestino, excesoGiros, rut, dig, canal);
        }
        catch (TransferenciaException e) {
            if (montoValido) {
                registraFinTrf(Long.parseLong(reg.numeroOperacion), null, null, fechaCargo, fechaAbono,
                    TablaValores.getValor(TTFF_CONFIG, "estados", "finError"), rut, dig, canal);
            }
            if (logger.isEnabledFor(Level.ERROR)) {
                logger.error("[transfiereAhorroHaciaCtaCte]  Excepcion General ", e);
            }
            throw e;
        }

        /*
         * Se debe tener presente que tanto la fecha de Cargo como la fecha de Abono debieran ser las contables que
         * tiene tandem. El problema es que la transacci�n B0671 NO retorna dicha fecha y no existe alguna forma de
         * obtenerla. Por lo tanto se decidi� mientras manejar la fecha del proceso como fechas de Cargo y Abono
         * (4to y 5to par�metro)
         */
        registraFinTrf(Long.parseLong(reg.numeroOperacion), null, null, fechaCargo, fechaAbono, TablaValores
            .getValor(TTFF_CONFIG, "estados", "fin"), rut, dig, canal);

        DetalleTransferencia detalle = new DetalleTransferencia();
        detalle.ctaOrigen = ctaOrigen;
        detalle.tipoCuentaOrigen = tipoCuentaOrigen;
        detalle.ctaDestino = ctaDestino;
        detalle.tipoCuentaDestino = tipoCuentaDestino;
        detalle.montoTransferencia = monto;
        detalle.numeroOperacion = reg.numeroOperacion;
        detalle.codigoBancoDestino = codBanco;
        detalle.codigoBancoOrigen = codBanco;
        detalle.rut = rut;
        detalle.dig = dig;
        detalle.rutDestino = rutDestino;
        detalle.digDestino = digDestino;
        detalle.nombreDestino = nombreDestino;
        detalle.comentario = comentario;
        detalle.canalID = canal;
        detalle.eMailCliente = eMailCliente; // Puede ser null
        detalle.eMailDestino = eMailDestino; // Puede ser null
        detalle.fechaTransferencia = fechaCargo;

        detalle.fechaCargo = fechaCargo;
        detalle.fechaAbono = fechaAbono;

        detalle.nombreOrigen = cliente.getFullName();
        detalle.nombreBancoOrigen = TablaValores.getValor(TTFF_CONFIG, "codigoBCI", "desc_" + canal);
        detalle.nombreBancoDestino = TablaValores.getValor(TTFF_CONFIG, "codigoBCI", "desc_" + canal);

        enviaEmail(detalle, false, cliente, canal);
        enviaMailCliente(detalle, cliente, canal);

        logCliente("AHO_CTACTE", "FIN TRANSFERENCIA", rut, dig, canal);
        return detalle;
    }

    /**
     * M�todo migrado desde {@link TransferenciasFondosBean}, encargado de realizar la transferencia de fondos
     * entre una cuenta de ahorro y una cuenta corriente. Se agregan los par�metros long rut, char dig y String
     * canal. Se agrega la constante FORMATO_CUENTA que reemplaza el valor en duro "00000000".
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 27/07/2012 Jorge Leiva A. (TINet): Versi�n inicial.
     * </ul>
     * <p>
     *
     * @param ctaOrigen cuenta de origen.
     * @param monto monto a transferir.
     * @param ctaDestino cuenta de destino.
     * @param excesoGiros exceso de giros.
     * @param rut rut del cliente.
     * @param dig digito verificador.
     * @param canal identificador del canal.
     * @return TransferenciaFondos transferencia realizada.
     * @throws TransferenciaException excepci�n.
     * @since 2.4
     */
    private wcorp.serv.ahorros.TransferenciaFondos realizarTransferenciaAhorroHaciaCtaCte(String ctaOrigen,
        double monto, String ctaDestino, boolean excesoGiros, long rut, char dig, String canal)
        throws TransferenciaException {
        try {
            logCliente("GIROAHO", "ctaOrigen=" + ctaOrigen + ", ctaDestino=" + ctaDestino + ", monto=" + monto,
                rut, dig, canal);
            ctaDestino = Formatting.number(Long.parseLong(ctaDestino), FORMATO_CUENTA);
            CuentaAhorro cta = new CuentaAhorro(ctaOrigen);
            return cta.giraHaciaCtaCte(ctaDestino, monto, excesoGiros);
        }
        catch (Exception e) {
            String mensaje = "CuentaOrigen=" + ctaOrigen + ", CuentaDestino=" + ctaDestino + ", MontoTrf=" + monto;
            logCliente("GIROAHO", mensaje, e, rut, dig, canal);
            if (logger.isEnabledFor(Level.ERROR)) {
                logger.error("[realizarTransferenciaAhorroHaciaCtaCte]  giraHaciaCtaCte ", e);
            }
            throw new TransferenciaException("TRF001");
        }
    }

    /**
     * M�todo migrado desde {@link TransferenciasFondosBean}, encargado de validar el saldo de la cuenta ahorro.
     * Se agregan los par�metros long rut, char dig y String canal.
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 27/07/2012 Jorge Leiva A. (TINet): Versi�n inicial.
     * </ul>
     * <p>
     *
     * @param ctaOrigen cuenta de origen.
     * @param monto monto a transferir.
     * @param rut rut del cliente.
     * @param dig digito verificador.
     * @param canal identificador del canal.
     * @return boolean
     * @throws TransferenciaException excepci�n.
     * @since 2.4
     */
    public boolean validarSaldoAhorro(String ctaOrigen, double monto, long rut, char dig, String canal)
        throws TransferenciaException {
        CuentaAhorro cc = null;
        SaldoAhorro saldo = null;
        try {
            cc = new CuentaAhorro(ctaOrigen);
            saldo = cc.getSaldo();

            String msg = "";
            msg = "CuentaOrigen=" + ctaOrigen;
            msg += ", SaldoDisponible=" + saldo.saldoDisponible;
            msg += ", Monto=" + monto;

            logCliente("VALIDASALDOAHO", msg, rut, dig, canal);

            if ((saldo.saldoDisponible) < monto)
                return false;

            return true;
        }
        catch (Exception e) {
            String mensaje = "CuentaOrigen=" + ctaOrigen + ", Monto=" + monto;
            logCliente("VALIDASALDOAHO", mensaje, e, rut, dig, canal);

            TransferenciaException te = new TransferenciaException("TRANSFERENCIA", "Error calculando saldo");
            te.setException(e);
            if (logger.isEnabledFor(Level.ERROR)) {
                logger.error("[validarSaldoAhorro]  getSaldo ", e);
            }
            throw te;
        }
    }

    /**
     * M�todo migrado desde {@link TransferenciasFondosBean}, encargado de registrar una transferencia. Se agregan
     * los par�metros long rut, char dig y String canal.
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 27/07/2012 Jorge Leiva A. (TINet): Versi�n inicial.
     * </ul>
     * <p>
     *
     * @param numeroOperacion n�mero de operaci�n.
     * @param folio folio.
     * @param numero n�mero.
     * @param fechaCargo fecha del cargo.
     * @param fechaAbono fecha del abono.
     * @param estado estado.
     * @param rut rut del cliente.
     * @param dig digito verificador.
     * @param canal identificador del canal.
     * @return boolean
     * @since 2.4
     */
    private boolean registraFinTrf(long numeroOperacion, String folio, String numero, Date fechaCargo,
        Date fechaAbono, String estado, long rut, char dig, String canal) {
        try {
            String mensaje = "";
            mensaje = "NumeroOperacion=" + numeroOperacion + ", Folio=" + folio + ", Numero=" + numero
                + ", FechaCargo=" + (fechaCargo != null ? fechaCargo.toString() : "--/--/----") + ", FechaAbono="
                + (fechaAbono != null ? fechaAbono.toString() : "--/--/----") + ", Estado=" + estado;

            logCliente("MODIFICAESTADO", mensaje, rut, dig, canal);
            serviciosCuentas.modificaEstadoTransferencia(numeroOperacion, folio, numero, fechaCargo, fechaAbono,
                estado, String.valueOf(rut));
            return true;
        }
        catch (Exception e) {
            String mensaje = "NUM_OPE=" + numeroOperacion + ", FOLIO=" + folio + ", NUMERO=" + numero
                + ", FECCARGO=" + (fechaCargo != null ? fechaCargo.toString() : "--/--/----") + ", FECABONO="
                + (fechaAbono != null ? fechaAbono.toString() : "--/--/----") + ", ESTADO=" + estado;
            logCliente("MODIFICAESTADO", mensaje, e, rut, dig, canal);
            if (logger.isEnabledFor(Level.ERROR)) {
                logger.error("[registraFinTrf]  modificaEstadoTransferencia ", e);
            }
            return false;
        }
    }

    /**
     * M�todo migrado desde {@link TransferenciasFondosBean}, encargado de realizar la transferencia de fondos
     * entre una tarjeta de cr�dito y una cuenta corriente. Se agregan los par�metros Cliente cliente y String
     * canal.
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 27/07/2012 Jorge Leiva A. (TINet): Versi�n inicial.
     * </ul>
     * <p>
     *
     * @param ctaOrigen cuenta origen.
     * @param tipoCuentaOrigen tipo cuenta origen.
     * @param ctaDestino cuenta destino.
     * @param tipoCuentaDestino tipo de cuenta de destino.
     * @param monto monto a transferir.
     * @param rutDestino rut del destinatario.
     * @param digDestino digito verificador del destinatario.
     * @param comentario comentario.
     * @param nombreDestino nombre del destinatario.
     * @param eMailCliente email del cliente.
     * @param eMailDestino email del destinatario.
     * @param tipoAutenticacion tipo de autentificaci�n.
     * @param cliente objeto con los datos del cliente.
     * @param canal identificador del canal.
     * @return DetalleTransferencia detalle de la transferencia realizada.
     * @throws TransferenciaException excepci�n.
     * @since 2.4
     */
    public DetalleTransferencia transfiereTdcHaciaCtaCte(String ctaOrigen, String tipoCuentaOrigen,
        String ctaDestino, String tipoCuentaDestino, double monto, long rutDestino, char digDestino,
        String comentario, String nombreDestino, String eMailCliente, String eMailDestino,
        String tipoAutenticacion, Cliente cliente, String canal) throws TransferenciaException {
        return transfiereTdcHaciaCtaCte(ctaOrigen, tipoCuentaOrigen, ctaDestino, tipoCuentaDestino, monto,
            rutDestino, digDestino, comentario, nombreDestino, eMailCliente, eMailDestino, tipoAutenticacion,
            null, cliente, canal);
    }

    /**
     * M�todo migrado desde {@link TransferenciasFondosBean}, encargado de realizar la transferencia de fondos
     * entre una tarjeta de cr�dito y una cuenta corriente. Se agregan los par�metros Cliente cliente y String
     * canal.
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 27/07/2012 Jorge Leiva A. (TINet): Versi�n inicial.
     * </ul>
     * <p>
     *
     * @param ctaOrigen cuenta origen.
     * @param tipoCuentaOrigen tipo de cuenta origen.
     * @param ctaDestino cuenta destino.
     * @param tipoCuentaDestino tipo de cuenta destino.
     * @param monto monto a transferir.
     * @param rutDestino rut destinatario.
     * @param digDestino digito verificador destinatario.
     * @param comentario comentario.
     * @param nombreDestino nombre del destinatario.
     * @param eMailCliente email del cliente.
     * @param eMailDestino email del destinatario.
     * @param tipoAutenticacion tipo de autentificaci�n.
     * @param tipoTarj tipo de tarjeta.
     * @param validarCupo valida cupo.
     * @param cupo cupo.
     * @param cliente objeto con los datos del cliente.
     * @param canal identificador del canal.
     * @return DetalleTranferencia detalles de la transferencia realizada.
     * @throws TransferenciaException excepci�n.
     * @since 2.4
     */
    public DetalleTransferencia transfiereTdcHaciaCtaCte(String ctaOrigen, String tipoCuentaOrigen,
        String ctaDestino, String tipoCuentaDestino, double monto, long rutDestino, char digDestino,
        String comentario, String nombreDestino, String eMailCliente, String eMailDestino,
        String tipoAutenticacion, String tipoTarj, boolean validarCupo, double cupo, Cliente cliente, String canal)
        throws TransferenciaException {

        return transfiereTdcHaciaCtaCte(ctaOrigen, tipoCuentaOrigen, ctaDestino, tipoCuentaDestino, monto,
            rutDestino, digDestino, comentario, nombreDestino, eMailCliente, eMailDestino, tipoAutenticacion,
            tipoTarj, false, 0, true, cliente, canal);
    }

    /**
     * M�todo migrado desde {@link TransferenciasFondosBean}, encargado de realizar la transferencia de fondos
     * entre una tarjeta de cr�dito y una cuenta corriente. Se agregan los par�metros Cliente cliente y String
     * canal.
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 27/07/2012 Jorge Leiva A. (TINet): Versi�n inicial.
     * </ul>
     * <p>
     *
     * @param ctaOrigen cuenta origen.
     * @param tipoCuentaOrigen tipo de cuenta origen.
     * @param ctaDestino cuenta de destino.
     * @param tipoCuentaDestino tipo de cuenta destino.
     * @param monto monto a transferir.
     * @param rutDestino rut del destinatario.
     * @param digDestino digito verificador del destinarario.
     * @param comentario comentario.
     * @param nombreDestino nombre del destinatario.
     * @param eMailCliente email del cliente.
     * @param eMailDestino email del destinatario.
     * @param tipoAutenticacion tipo de autentificaci�n.
     * @param tipoTarj tipo de tarjeta.
     * @param cliente objeto con los datos del cliente.
     * @param canal identificador del canal.
     * @return DetalleTransferencia detalle de la transferencia realizada.
     * @throws TransferenciaException excepci�n.
     * @since 2.4
     */
    public DetalleTransferencia transfiereTdcHaciaCtaCte(String ctaOrigen, String tipoCuentaOrigen,
        String ctaDestino, String tipoCuentaDestino, double monto, long rutDestino, char digDestino,
        String comentario, String nombreDestino, String eMailCliente, String eMailDestino,
        String tipoAutenticacion, String tipoTarj, Cliente cliente, String canal) throws TransferenciaException {
        return transfiereTdcHaciaCtaCte(ctaOrigen, tipoCuentaOrigen, ctaDestino, tipoCuentaDestino, monto,
            rutDestino, digDestino, comentario, nombreDestino, eMailCliente, eMailDestino, tipoAutenticacion,
            tipoTarj, false, 0, cliente, canal);
    }

    /**
     * M�todo migrado desde {@link TransferenciasFondosBean}, encargado de realizar la transferencia de fondos
     * entre una tarjeta de cr�dito y una cuenta corriente. Se agregan los par�metros Cliente cliente y String
     * canal. Se agrega la constante INICIO_TRANSFERENCIA que reemplaza el valor en duro "INICIO TRANSFERENCIA".
     * Se agrega la constante FORMATO_CUENTA que reemplaza el valor en duro "00000000".
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 27/07/2012 Jorge Leiva A. (TINet): Versi�n inicial.
     * </ul>
     * <p>
     *
     * @param ctaOrigen cuenta origen.
     * @param tipoCuentaOrigen tipo cuenta origen.
     * @param ctaDestino cuenta destino.
     * @param tipoCuentaDestino tipo cuenta destino.
     * @param monto monto a transferir.
     * @param rutDestino rut del destinatario.
     * @param digDestino digito verificador del destinatario.
     * @param comentario comentario.
     * @param nombreDestino nombre del destinatario.
     * @param eMailCliente email del cliente.
     * @param eMailDestino email del destinatario.
     * @param tipoAutenticacion tipo de autentificacio�n.
     * @param tipoTarj tipo de tarjeta.
     * @param validarCupo valida cupo.
     * @param cupo cupo.
     * @param validaCupoTarjetaEnLinea valida cupo en tarjeta en l�nea.
     * @param cliente objeto con los datos del cliente.
     * @param canal identificador del canal.
     * @return DetalleTransferencia detalle de la transferencia realizada.
     * @throws TransferenciaException excepci�n.
     * @since 2.4
     */
    public DetalleTransferencia transfiereTdcHaciaCtaCte(String ctaOrigen, String tipoCuentaOrigen,
        String ctaDestino, String tipoCuentaDestino, double monto, long rutDestino, char digDestino,
        String comentario, String nombreDestino, String eMailCliente, String eMailDestino,
        String tipoAutenticacion, String tipoTarj, boolean validarCupo, double cupo,
        boolean validaCupoTarjetaEnLinea, Cliente cliente, String canal) throws TransferenciaException {
        String mensaje = INICIO_TRANSFERENCIA;
        String logCtaOrigen = StringUtil.censura(ctaOrigen, ' ', LARGO_CARACTERES_SIN_CENSURA, false, false);

        long rut = cliente.getRut();
        char dig = cliente.getDigito();

        logCliente("TDC_CTACTE", logCtaOrigen, tipoCuentaOrigen, ctaDestino, tipoCuentaDestino, monto, rutDestino,
            digDestino, "016 (Origen)", mensaje, rut, dig, canal);

        String codBanco = TablaValores.getValor(TTFF_CONFIG, "codigoBCI", "valor");

        Date fechaCargo = new Date();
        Date fechaAbono = new Date();

        ctaDestino = Formatting.number(Long.parseLong(ctaDestino), FORMATO_CUENTA);

        double montoProgramado = 0.0;
        try {
            montoProgramado = montoProgramadoDia(rut, tipoCuentaOrigen, ctaOrigen, new Date());
        }
        catch (Exception e) {
            mensaje = "MontoProgramado" + ", CuentaOrigen =[" + logCtaOrigen + "]" + ", CuentaDestino=["
                + ctaDestino + "]" + ", Monto=[" + monto + "]";
            logCliente("TDC_CTECPR", mensaje, e, rut, dig, canal);
            if (logger.isEnabledFor(Level.ERROR)) {
                logger.error("[transfiereTdcHaciaCtaCte]  montoProgramadoDia ", e);
            }
            throw new TransferenciaException("TRF001");
        }

        logCliente("TDC", "validar cupo..::" + validarCupo, rut, dig, canal);
        logCliente("TDC", "cupo..........::" + cupo, rut, dig, canal);
        logCliente("TDC", "monto.........::" + monto, rut, dig, canal);
        logCliente("TDC", "montoProgramado..::" + montoProgramado, rut, dig, canal);

        if (validarCupo) {
            // Debemos validar el monto a transferir + el monto programado
            // independiente de que la programaci�n haya sido efectuada ya.
            logCliente("TDC", "validando cupo ....", rut, dig, canal);
            logCliente("TDC", "cupo < monto + montoProgramado....", rut, dig, canal);
            if (cupo < monto + montoProgramado) {
                throw new TransferenciaException("0020");
            }
        }
        /*
         * Registramos la transferencia y obtenemos de ella el numero de operaci�n y la fecha de cargo
         */
        RegistroTransferencia reg = null;
        try {
            String estadoIngreso = TablaValores.getValor(TTFF_CONFIG, "estados", "ingreso");

            reg = registraIngresoTrf(tipoCuentaOrigen, ctaOrigen, monto, fechaCargo, estadoIngreso, fechaAbono,
                tipoCuentaDestino, ctaDestino, rutDestino, codBanco, comentario, nombreDestino, eMailDestino, rut,
                dig, canal);
        }
        catch (Exception e) {
            mensaje = "REG_ITF" + ", CuentaOrigen =[" + logCtaOrigen + "]" + ", CuentaDestino=[" + ctaDestino
                + "]" + ", Monto=[" + monto + "]";
            logCliente("TDC_CTECPR", mensaje, e, rut, dig, canal);
            if (logger.isEnabledFor(Level.ERROR)) {
                logger.error("[transfiereTdcHaciaCtaCte]  registraIngresoTrf ", e);
            }
            throw new TransferenciaException("TRF001");
        }

        boolean montoValido = true;
        try {
            double topeBanco = 0;
            try {
                topeBanco = maximoTopeCuenta(rut, canal, tipoCuentaOrigen, rutDestino, tipoAutenticacion);
            }
            catch (Exception e) {
                logCliente("TDC_CTACTE", "Fallo por no poder obtener TOPE BANCO", e, rut, dig, canal);
                registraFinTrf(Long.parseLong(reg.numeroOperacion), null, null, fechaCargo, fechaAbono,
                    TablaValores.getValor(TTFF_CONFIG, "estados", "finError"), rut, dig, canal);
                montoValido = false;
                if (logger.isEnabledFor(Level.ERROR)) {
                    logger.error("[transfiereTdcHaciaCtaCte]  Excepcion General  ", e);
                }
                throw new TransferenciaException("TRF001");
            }

            double montoDia = 0;
            try {
                montoDia = montoTransferidoDia(rut, tipoCuentaOrigen, ctaOrigen, new Date());
            }
            catch (Exception e) {
                logCliente("TDC_CTACTE", "Fallo por no poder obtener MONTO TRANSFERIDO DIA", e, rut, dig, canal);
                registraFinTrf(Long.parseLong(reg.numeroOperacion), null, null, fechaCargo, fechaAbono,
                    TablaValores.getValor(TTFF_CONFIG, "estados", "finError"), rut, dig, canal);
                montoValido = false;
                if (logger.isEnabledFor(Level.ERROR)) {
                    logger.error("[transfiereTdcHaciaCtaCte]  Excepcion General  ", e);
                }
                throw new TransferenciaException("TRF001");
            }

            if (rut != rutDestino) {
                if (monto > (topeBanco - montoDia - montoProgramado)) {
                    mensaje = "ValidaMonto" + ", Monto=" + monto + ", TopeBanco=" + topeBanco + ", montoDia="
                        + montoDia + ", montoProgramado=" + montoProgramado;
                    logCliente("TDC_CTACTE", mensaje, rut, dig, canal);
                    registraFinTrf(Long.parseLong(reg.numeroOperacion), null, null, fechaCargo, fechaAbono,
                        TablaValores.getValor(TTFF_CONFIG, "estados", "finError"), rut, dig, canal);
                    montoValido = false;
                    throw new TransferenciaException("TRF042");
                }
            }
            logCliente("TDC_CTACTE", "EfectuandoTransferencia, MontoDia=" + montoDia + ", TopeBanco=" + topeBanco
                + ", Programado=" + montoProgramado, rut, dig, canal);
            if (tipoTarj != null) {
                realizarTransferenciaTdcHaciaCtaCte(ctaOrigen, ctaDestino, monto, tipoTarj,
                    validaCupoTarjetaEnLinea, rut, dig, canal);
            }
            else {
                realizarTransferenciaTdcHaciaCtaCte(ctaOrigen, ctaDestino, monto, validaCupoTarjetaEnLinea, rut,
                    dig, canal);
            }
        }
        catch (TransferenciaException e) {
            if (montoValido) {
                registraFinTrf(Long.parseLong(reg.numeroOperacion), null, null, fechaCargo, fechaAbono,
                    TablaValores.getValor(TTFF_CONFIG, "estados", "finError"), rut, dig, canal);
            }
            if (logger.isEnabledFor(Level.ERROR)) {
                logger.error("[transfiereTdcHaciaCtaCte]  Excepcion General  ", e);
            }
            throw e;
        }

        /*
         * Se debe tener presente que tanto la fecha de Cargo como la fecha de Abono debieran ser las contables que
         * tiene tandem. El problema es que la transacci�n B0671 NO retorna dicha fecha y no existe alguna forma de
         * obtenerla. Por lo tanto se decidi� mientras manejar la fecha del proceso como fechas de Cargo y Abono
         * (4to y 5to par�metro)
         */
        registraFinTrf(Long.parseLong(reg.numeroOperacion), null, null, fechaCargo, fechaAbono, TablaValores
            .getValor(TTFF_CONFIG, "estados", "fin"), rut, dig, canal);

        DetalleTransferencia detalle = new DetalleTransferencia();
        detalle.ctaOrigen = ctaOrigen;
        detalle.tipoCuentaOrigen = tipoCuentaOrigen;
        detalle.ctaDestino = ctaDestino;
        detalle.tipoCuentaDestino = tipoCuentaDestino;
        detalle.montoTransferencia = monto;
        detalle.numeroOperacion = reg.numeroOperacion;
        detalle.codigoBancoDestino = codBanco;
        detalle.codigoBancoOrigen = codBanco;
        detalle.rut = rut;
        detalle.dig = dig;
        detalle.rutDestino = rutDestino;
        detalle.digDestino = digDestino;
        detalle.nombreDestino = nombreDestino;
        detalle.comentario = comentario;
        detalle.canalID = canal;
        detalle.eMailCliente = eMailCliente; // Puede ser null
        detalle.eMailDestino = eMailDestino; // Puede ser null
        detalle.fechaTransferencia = fechaCargo;

        detalle.fechaCargo = fechaCargo;
        detalle.fechaAbono = fechaAbono;

        detalle.nombreOrigen = cliente.getFullName();
        detalle.nombreBancoOrigen = TablaValores.getValor(TTFF_CONFIG, "codigoBCI", "desc_" + canal);
        detalle.nombreBancoDestino = TablaValores.getValor(TTFF_CONFIG, "codigoBCI", "desc_" + canal);

        enviaEmail(detalle, false, cliente, canal);
        enviaMailCliente(detalle, cliente, canal);

        logCliente("TDC_CTACTE", "FIN TRANSFERENCIA", rut, dig, canal);
        return detalle;
    }

    /**
     * M�todo migrado desde {@link TransferenciasFondosBean}, encargado de realizar la transferencia de fondos
     * entre una tarjeta de cr�dito y una cuenta corriente. Se agregan los par�metros long rut, char dig y String
     * canal.
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 27/07/2012 Jorge Leiva A. (TINet): Versi�n inicial.
     * </ul>
     * <p>
     *
     * @param ctaOrigen cuenta origen.
     * @param ctaDestino cuenta destino.
     * @param monto monto a transferir.
     * @param validaCupoTarjetaEnLinea valida el cupo de tarjeta en l�nea.
     * @param rut rut del cliente.
     * @param dig digito verificador.
     * @param canal identificador del canal.
     * @return TransferenciaFondos transferencia realizada.
     * @throws TransferenciaException excepci�n.
     * @since 2.4
     */
    private wcorp.serv.cuentas.TransferenciaFondos realizarTransferenciaTdcHaciaCtaCte(String ctaOrigen,
        String ctaDestino, double monto, boolean validaCupoTarjetaEnLinea, long rut, char dig, String canal)
        throws TransferenciaException {

        String logCtaOrigen = StringUtil.censura(ctaOrigen, ' ', LARGO_CARACTERES_SIN_CENSURA, false, false);

        try {
            logCliente("AVANCEVISA", "ctaOrigen=" + logCtaOrigen + ", ctaDestino=" + ctaDestino + ", monto="
                + monto + ", validaCupoTandem=" + validaCupoTarjetaEnLinea, rut, dig, canal);
            return serviciosCuentas.avanceTCreditoCtaCte(ctaOrigen, ctaDestino, monto, validaCupoTarjetaEnLinea);
        }
        catch (Exception e) {
            String mensaje = "CuentaOrigen=" + logCtaOrigen + ", CuentaDestino=" + ctaDestino + ", MontoTrf="
                + monto;
            logCliente("AVANCEVISA", mensaje, e, rut, dig, canal);
            if (logger.isEnabledFor(Level.ERROR)) {
                logger.error("[realizarTransferenciaTdcHaciaCtaCte]  avanceTCreditoCtaCtel  ", e);
            }
            throw new TransferenciaException("TRF001");
        }
    }

    /**
     * M�todo migrado desde {@link TransferenciasFondosBean}, encargado de realizar la transferencia de fondos
     * entre una tarjeta de cr�dito y una cuenta corriente. Se agregan los par�metros long rut, char dig y String
     * canal. Se agrega la constante CARACTER_TIPO_TARJETA que reemplaza el valor en duro 'M'.
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 27/07/2012 Jorge Leiva A. (TINet): Versi�n inicial.
     * </ul>
     * <p>
     *
     * @param ctaOrigen cuenta origen.
     * @param ctaDestino cuenta destino.
     * @param monto monto a transferir.
     * @param tipoTarj tipo de tarjeta.
     * @param validaCupoTarjetaEnLinea valida cupo tarjeta en l�nea.
     * @param rut rut del cliente.
     * @param dig digito verificador.
     * @param canal identificador del canal.
     * @return TransferenciaFondos transferencia realizada.
     * @throws TransferenciaException excepci�n
     * @since 2.4
     */
    private wcorp.serv.cuentas.TransferenciaFondos realizarTransferenciaTdcHaciaCtaCte(String ctaOrigen,
        String ctaDestino, double monto, String tipoTarj, boolean validaCupoTarjetaEnLinea, long rut, char dig,
        String canal) throws TransferenciaException {
        String logCtaOrigen = StringUtil.censura(ctaOrigen, ' ', LARGO_CARACTERES_SIN_CENSURA, false, false);
        logger.debug("ctaOrigen::" + logCtaOrigen + "::tipoTarj::" + tipoTarj);
        tipoTarj = TablaValores.getValor(TTFF_CONFIG, "tipoTarj", tipoTarj);

        if ((tipoTarj != null) && (tipoTarj.length() > 0) && (tipoTarj.charAt(0) == CARACTER_TIPO_TARJETA)) {
            try {
                logCliente("AVANCEMASTER", "ctaOrigen=" + logCtaOrigen + ", ctaDestino=" + ctaDestino + ", monto="
                    + monto, rut, dig, canal);
                return serviciosCuentas.avanceTCreditoCtaCte(ctaOrigen, ctaDestino, monto,
                    validaCupoTarjetaEnLinea);
            }
            catch (Exception e) {
                String mensaje = "CuentaOrigen=" + logCtaOrigen + ", CuentaDestino=" + ctaDestino + ", MontoTrf="
                    + monto;
                logCliente("AVANCEMASTER", mensaje, e, rut, dig, canal);
                if (logger.isEnabledFor(Level.ERROR)) {
                    logger.error("[realizarTransferenciaTdcHaciaCtaCte]  avanceTCreditoCtaCte  ", e);
                }
                throw new TransferenciaException("TRF001");
            }
        }
        else {
            try {
                logCliente("AVANCEVISA", "ctaOrigen=" + logCtaOrigen + ", ctaDestino=" + ctaDestino + ", monto="
                    + monto, rut, dig, canal);
                return serviciosCuentas.avanceTCreditoCtaCte(ctaOrigen, ctaDestino, monto,
                    validaCupoTarjetaEnLinea);
            }
            catch (Exception e) {
                String mensaje = "CuentaOrigen=" + logCtaOrigen + ", CuentaDestino=" + ctaDestino + ", MontoTrf="
                    + monto;
                logCliente("AVANCEVISA", mensaje, e, rut, dig, canal);
                if (logger.isEnabledFor(Level.ERROR)) {
                    logger.error("[realizarTransferenciaTdcHaciaCtaCte]  avanceTCreditoCtaCte  ", e);
                }
                throw new TransferenciaException("TRF001");
            }
        } // fin else

    }

    /**
     * M�todo migrado desde {@link TransferenciasFondosBean}, encargo de realizar el abono a la l�nea de
     * sobregiro. Se agregan los par�metros Cliente cliente y String canal. Se agrega la constante
     * INICIO_TRANSFERENCIA que reemplaza el valor en duro "INICIO TRANSFERENCIA". Se agrega la constante
     * FORMATO_CUENTA que reemplaza el valor en duro "00000000".
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 27/07/2012 Jorge Leiva A. (TINet): Versi�n inicial.
     * </ul>
     * <p>
     *
     * @param ctaOrigen cuenta origen.
     * @param tipoCuentaOrigen tipo de cuenta origen.
     * @param monto monto a transferir.
     * @param rutDestino rut del destinatario.
     * @param digDestino digito verficador del destinatario.
     * @param comentario comentario.
     * @param nombreDestino nombre del destinatario.
     * @param eMailCliente email del cliente.
     * @param eMailDestino email del destinatario.
     * @param tipoAutenticacion tipo de autentificaci�n.
     * @param cliente objeto con los datos del cliente.
     * @param canal identificador del canal.
     * @return DetalleTransferencia detalles de la transferencia realizada.
     * @throws TransferenciaException excepci�n.
     * @since 2.4
     */
    public DetalleTransferencia transfiereCtaCteHaciaLSG(String ctaOrigen, String tipoCuentaOrigen, double monto,
        long rutDestino, char digDestino, String comentario, String nombreDestino, String eMailCliente,
        String eMailDestino, String tipoAutenticacion, Cliente cliente, String canal)
        throws TransferenciaException {
        String mensaje = INICIO_TRANSFERENCIA;
        logCliente("CTACTE_LDC", ctaOrigen, tipoCuentaOrigen, ctaOrigen, "LDC", monto, rutDestino, digDestino,
            "016", mensaje, cliente.getRut(), cliente.getDigito(), canal);
        String codBanco = TablaValores.getValor(TTFF_CONFIG, "codigoBCI", "valor");
        String ctaDestino = ctaOrigen;

        ctaOrigen = Formatting.number(Integer.parseInt(ctaOrigen), FORMATO_CUENTA);
        ctaDestino = Formatting.number(Integer.parseInt(ctaDestino), FORMATO_CUENTA);
        String tipoCuentaDestino = "LDC";

        Date fechaCargo = new Date();
        Date fechaAbono = new Date();

        // Validar saldo
        try {
            new CuentaCorriente(ctaOrigen).getSaldo();
        }
        catch (Exception e) {
            mensaje = "SaldoCtaCte" + ", CuentaOrigen =[" + ctaOrigen + "]" + ", Monto=[" + monto + "]";
            logCliente("CTACTE_LDC", mensaje, e, cliente.getRut(), cliente.getDigito(), canal);
            if (logger.isEnabledFor(Level.ERROR)) {
                logger.error("[transfiereCtaCteHaciaLSG]  new CuentaCorriente  ", e);
            }
            throw new TransferenciaException("TRF001");
        }

        double montoProgramado = 0.0;
        try {
            montoProgramado = montoProgramadoDia(cliente.getRut(), tipoCuentaOrigen, ctaOrigen, new Date());
        }
        catch (Exception e) {
            mensaje = "MontoProgramado" + ", CuentaOrigen =[" + ctaOrigen + "]" + ", Monto=[" + monto + "]";
            logCliente("CTACTE_LDC", mensaje, e, cliente.getRut(), cliente.getDigito(), canal);
            if (logger.isEnabledFor(Level.ERROR)) {
                logger.error("[transfiereCtaCteHaciaLSG]  montoProgramadoDia  ", e);
            }
            throw new TransferenciaException("TRF001");
        }

        // Debemos validar el monto a transferir + el monto programado
        // independiente de que la programaci�n haya sido efectuada ya.
        if (!validarSaldoCtaCte(cliente, canal, ctaOrigen, monto + montoProgramado, false)) {
            throw new TransferenciaException("0020");
        }

        /*
         * Registramos la transferencia y obtenemos de ella el numero de operaci�n y la fecha de cargo
         */
        RegistroTransferencia reg = null;

        try {
            String estadoIngreso = TablaValores.getValor(TTFF_CONFIG, "estados", "ingreso");

            reg = registraIngresoTrf(tipoCuentaOrigen, ctaOrigen, monto, fechaCargo, estadoIngreso, fechaAbono,
                tipoCuentaDestino, ctaDestino, rutDestino, codBanco, comentario, nombreDestino, eMailDestino,
                cliente.getRut(), cliente.getDigito(), canal);
        }
        catch (Exception e) {
            mensaje = "REG_ITF" + ", CuentaOrigen =[" + ctaOrigen + "]" + ", Monto=[" + monto + "]";
            logCliente("CTACTE_LDC", mensaje, e, cliente.getRut(), cliente.getDigito(), canal);
            if (logger.isEnabledFor(Level.ERROR)) {
                logger.error("[transfiereCtaCteHaciaLSG]  registraIngresoTrf  ", e);
            }
            throw new TransferenciaException("TRF001");
        }

        boolean montoValido = true;
        try {
            double topeBanco = 0.0;
            try {
                topeBanco = maximoTopeCuenta(cliente.getRut(), canal, tipoCuentaOrigen, rutDestino,
                    tipoAutenticacion);
            }
            catch (Exception e) {
                logCliente("CTACTE_LDC", "Fallo por no poder obtener TOPE BANCO", e, cliente.getRut(), cliente
                    .getDigito(), canal);
                registraFinTrf(Long.parseLong(reg.numeroOperacion), null, null, fechaCargo, fechaAbono,
                    TablaValores.getValor(TTFF_CONFIG, "estados", "finError"), cliente.getRut(), cliente
                        .getDigito(), canal);
                montoValido = false;
                if (logger.isEnabledFor(Level.ERROR)) {
                    logger.error("[transfiereCtaCteHaciaLSG]  Excepcion General ", e);
                }
                throw new TransferenciaException("TRF001");
            }

            double montoDia = 0.0;
            try {
                montoDia = montoTransferidoDia(cliente.getRut(), tipoCuentaOrigen, ctaOrigen, new Date());
            }
            catch (Exception e) {
                logCliente("CTACTE_LDC", "Fallo por no poder obtener MONTO TRANSFERIDO DIA", e, cliente.getRut(),
                    cliente.getDigito(), canal);
                registraFinTrf(Long.parseLong(reg.numeroOperacion), null, null, fechaCargo, fechaAbono,
                    TablaValores.getValor(TTFF_CONFIG, "estados", "finError"), cliente.getRut(), cliente
                        .getDigito(), canal);
                montoValido = false;
                if (logger.isEnabledFor(Level.ERROR)) {
                    logger.error("[transfiereCtaCteHaciaLSG]  Excepcion General ", e);
                }
                throw new TransferenciaException("TRF001");
            }

            if (cliente.getRut() != rutDestino) {
                if (monto > (topeBanco - montoDia - montoProgramado)) {
                    mensaje = "ValidaMonto" + ", Monto=" + monto + ", TopeBanco=" + topeBanco + ", montoDia="
                        + montoDia + ", montoProgramado=" + montoProgramado;
                    logCliente("CTACTE_LDC", mensaje, cliente.getRut(), cliente.getDigito(), canal);
                    registraFinTrf(Long.parseLong(reg.numeroOperacion), null, null, fechaCargo, fechaAbono,
                        TablaValores.getValor(TTFF_CONFIG, "estados", "finError"), cliente.getRut(), cliente
                            .getDigito(), canal);
                    montoValido = false;
                    throw new TransferenciaException("TRF042");
                }
            }
            logCliente("CTACTE_LDC", "EfectuandoTransferencia, MontoDia=" + montoDia + ", TopeBanco=" + topeBanco
                + ", Programado=" + montoProgramado, cliente.getRut(), cliente.getDigito(), canal);
            realizarTransferenciaCtaCteHaciaLSG(ctaOrigen, monto, cliente, canal);
        }
        catch (TransferenciaException e) {
            if (montoValido) {
                registraFinTrf(Long.parseLong(reg.numeroOperacion), null, null, fechaCargo, fechaAbono,
                    TablaValores.getValor(TTFF_CONFIG, "estados", "finError"), cliente.getRut(), cliente
                        .getDigito(), canal);
            }
            if (logger.isEnabledFor(Level.ERROR)) {
                logger.error("[transfiereCtaCteHaciaLSG]  Excepcion General ", e);
            }
            throw e;
        }

        /*
         * Se debe tener presente que tanto la fecha de Cargo como la fecha de Abono debieran ser las contables que
         * tiene tandem. El problema es que la transacci�n B0671 NO retorna dicha fecha y no existe alguna forma de
         * obtenerla. Por lo tanto se decidi� mientras manejar la fecha del proceso como fechas de Cargo y Abono
         * (4to y 5to par�metro)
         */

        registraFinTrf(Long.parseLong(reg.numeroOperacion), null, null, fechaCargo, fechaAbono, TablaValores
            .getValor(TTFF_CONFIG, "estados", "fin"), cliente.getRut(), cliente.getDigito(), canal);

        DetalleTransferencia detalle = new DetalleTransferencia();
        detalle.ctaOrigen = ctaOrigen;
        detalle.tipoCuentaOrigen = tipoCuentaOrigen;
        detalle.ctaDestino = ctaDestino;
        detalle.tipoCuentaDestino = tipoCuentaDestino;
        detalle.montoTransferencia = monto;
        detalle.numeroOperacion = reg.numeroOperacion;
        detalle.codigoBancoDestino = codBanco;
        detalle.codigoBancoOrigen = codBanco;
        detalle.rut = cliente.getRut();
        detalle.dig = cliente.getDigito();
        detalle.rutDestino = rutDestino;
        detalle.digDestino = digDestino;
        detalle.nombreDestino = nombreDestino;
        detalle.comentario = comentario;
        detalle.canalID = canal;
        detalle.eMailCliente = eMailCliente; // Puede ser null
        detalle.eMailDestino = eMailDestino; // Puede ser null
        detalle.fechaTransferencia = fechaCargo;

        detalle.fechaCargo = fechaCargo;
        detalle.fechaAbono = fechaAbono;

        detalle.nombreOrigen = cliente.getFullName();
        detalle.nombreBancoOrigen = TablaValores.getValor(TTFF_CONFIG, "codigoBCI", "desc_" + canal);
        detalle.nombreBancoDestino = TablaValores.getValor(TTFF_CONFIG, "codigoBCI", "desc_" + canal);

        enviaEmail(detalle, false, cliente, canal);
        enviaMailCliente(detalle, cliente, canal);

        logCliente("CTACTE_LDC", "FIN TRANSFERENCIA", cliente.getRut(), cliente.getDigito(), canal);
        return detalle;
    }

    /**
     * M�todo migrado desde {@link TransferenciasFondosBean}, encargado de realizar el abono desde la cuenta
     * corriente a la l�nea de sobregiro. Se agregan los par�metros Cliente cliente y String canal. Se agrega la
     * constante FORMATO_CUENTA que reemplaza el valor en duro "00000000".
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 27/07/2012 Jorge Leiva A. (TINet): Versi�n inicial.
     * </ul>
     * <p>
     *
     * @param ctaOrigen cuenta origen.
     * @param monto monto a transferir.
     * @param cliente objeto con los datos del cliente.
     * @param canal identificador del canal.
     * @return AbonoLineaSobr abono realizado.
     * @throws TransferenciaException excepci�n.
     * @since 2.4
     */
    private wcorp.serv.cuentas.AbonoLineaSobr realizarTransferenciaCtaCteHaciaLSG(String ctaOrigen, double monto,
        Cliente cliente, String canal) throws TransferenciaException {
        try {
            logCliente("ABONALDC", "ctaOrigen=" + ctaOrigen + ", monto=" + monto, cliente.getRut(), cliente
                .getDigito(), canal);
            ctaOrigen = Formatting.number(Integer.parseInt(ctaOrigen), FORMATO_CUENTA);

            CuentaCorriente cc = new CuentaCorriente(ctaOrigen);
            return cc.abonaLineaSobregiro(monto, cliente.getRut());
        }
        catch (Exception e) {
            String mensaje = "CuentaOrigen=" + ctaOrigen + ", MontoTrf=" + monto;
            logCliente("ABONALDC", mensaje, e, cliente.getRut(), cliente.getDigito(), canal);
            if (logger.isEnabledFor(Level.ERROR)) {
                logger.error("[realizarTransferenciaCtaCteHaciaLSG]  abonaLineaSobregiro ", e);
            }
            throw new TransferenciaException("TRF001");
        }
    }

    /**
     * M�todo migrado desde {@link TransferenciasFondosBean}, encargado de realizar la transferencia entre una
     * cuenta corriente y otra cuenta corriente. Se agregan los par�metros Cliente cliente y String canal.
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 27/07/2012 Jorge Leiva A. (TINet): Versi�n inicial.
     * </ul>
     * <p>
     *
     * @param ctaOrigen cuenta de origen.
     * @param tipoCuentaOrigen tipo de cuenta de origen.
     * @param ctaDestino cuenta de destino.
     * @param tipoCuentaDestino tipo de cuenta destino.
     * @param monto monto a transferir.
     * @param rutDestino rut del destinatario.
     * @param digDestino digito verificador del destinatario.
     * @param comentario comentario.
     * @param nombreDestino nombre del destinarario.
     * @param eMailCliente email del cliente.
     * @param eMailDestino email del destinatario.
     * @param tipoAutenticacion tipo de autentificaci�n.
     * @param cliente objeto con los datos del cliente.
     * @param canal identificador del canal.
     * @return DetalleTransferencia detalle de la transferencia realizada.
     * @throws TransferenciaException excepci�n.
     */
    public DetalleTransferencia transfiereCtaCteHaciaCtaCte(String ctaOrigen, String tipoCuentaOrigen,
        String ctaDestino, String tipoCuentaDestino, double monto, long rutDestino, char digDestino,
        String comentario, String nombreDestino, String eMailCliente, String eMailDestino,
        String tipoAutenticacion, Cliente cliente, String canal) throws TransferenciaException {

        return transfiereCtaCteHaciaCtaCte(ctaOrigen, tipoCuentaOrigen, ctaDestino, tipoCuentaDestino, monto,
            rutDestino, digDestino, comentario, nombreDestino, eMailCliente, eMailDestino, tipoAutenticacion,
            false, cliente, canal);
    }

    /**
     * M�todo migrado desde TransferenciasFondosBean, encargado de obtener el detalle de una transferencias de
     * fondos entre una cuenta corriente y otra cuenta corriente. Se agregaron los par�metros Cliente cliente y
     * String Canal. Se agrega la constante INICIO_TRANSFERENCIA que reemplaza el valor en duro
     * "INICIO TRANSFERENCIA". Se agrega la constante FORMATO_CUENTA que reemplaza el valor en duro "00000000".
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 27/07/2012 Jorge Leiva A. (TINet): Versi�n inicial.
     * </ul>
     * <p>
     *
     * @param ctaOrigen cuenta de origen.
     * @param tipoCuentaOrigen tipo de cuenta origen.
     * @param ctaDestino cuenta de destino.
     * @param tipoCuentaDestino tipo de cuenta destino.
     * @param monto monto a transferir.
     * @param rutDestino rut del destinatario.
     * @param digDestino digito verificador del destinatario.
     * @param comentario comentario.
     * @param nombreDestino nombre del destinatario.
     * @param eMailCliente email del cliente.
     * @param eMailDestino email del destinatario.
     * @param tipoAutenticacion tipo de autentificaci�n.
     * @param esProgramada transferencia programada.
     * @param cliente objeto con los datos del cliente.
     * @param canal identificador del canal.
     * @return DetalleTransferencias representa el detalle de la transferencia realizada.
     * @throws TransferenciaException excepci�n.
     * @since 2.4
     */
    private DetalleTransferencia transfiereCtaCteHaciaCtaCte(String ctaOrigen, String tipoCuentaOrigen,
        String ctaDestino, String tipoCuentaDestino, double monto, long rutDestino, char digDestino,
        String comentario, String nombreDestino, String eMailCliente, String eMailDestino,
        String tipoAutenticacion, boolean esProgramada, Cliente cliente, String canal)
        throws TransferenciaException {
        String mensaje = INICIO_TRANSFERENCIA;
        logCliente("CTACTE_CTACTE", ctaOrigen, tipoCuentaOrigen, ctaDestino, tipoCuentaDestino, monto, rutDestino,
            digDestino, "016", mensaje, cliente.getRut(), cliente.getDigito(), canal);

        String codBanco = TablaValores.getValor(TTFF_CONFIG, "codigoBCI", "valor");

        ctaOrigen = Formatting.number(Integer.parseInt(ctaOrigen), FORMATO_CUENTA);
        ctaDestino = Formatting.number(Integer.parseInt(ctaDestino), FORMATO_CUENTA);

        Date fechaCargo = new Date();
        Date fechaAbono = new Date();

        // Validamos saldo
        try {
            new CuentaCorriente(ctaOrigen).getSaldo();
        }
        catch (Exception e) {
            mensaje = "SaldoCtaCte" + ", CuentaOrigen =[" + ctaOrigen + "]" + ", CuentaDestino=[" + ctaDestino
                + "]" + ", Monto=[" + monto + "]";
            logCliente("CTACTE_CTACTE", mensaje, e, cliente.getRut(), cliente.getDigito(), canal);
            if (logger.isEnabledFor(Level.ERROR)) {
                logger.error("[transfiereCtaCteHaciaCtaCte]  General ", e);
            }
            if (esProgramada) {
                if (e instanceof GeneralException) {
                    GeneralException ge = (GeneralException) e;
                    throw new TransferenciaException(ge.getCodigo());
                }
            }
            throw new TransferenciaException("TRF011");
        }

        double montoProgramado = 0.0;

        if (!esProgramada) {
            logCliente("CTACTE_CTACTE", "Obtenemos monto programado", cliente.getRut(), cliente.getDigito(), canal);
            try {
                montoProgramado = montoProgramadoDia(cliente.getRut(), tipoCuentaOrigen, ctaOrigen, new Date());
            }
            catch (Exception e) {
                mensaje = "MontoProgramado" + ", CuentaOrigen =[" + ctaOrigen + "]" + ", CuentaDestino=["
                    + ctaDestino + "]" + ", Monto=[" + monto + "]";
                logCliente("CTACTE_CTACTE", mensaje, e, cliente.getRut(), cliente.getDigito(), canal);
                if (logger.isEnabledFor(Level.ERROR)) {
                    logger.error("[transfiereCtaCteHaciaCtaCte]  General ", e);
                }
                throw new TransferenciaException("TRF001");
            }
        }

        // Debemos validar el monto a transferir + el monto programado
        // independiente de que la programaci�n haya sido efectuada ya.
        if (!validarSaldoCtaCte(cliente, canal, ctaOrigen, monto + montoProgramado, true)) {
            throw new TransferenciaException("0020");
        }

        /*
         * Registramos la transferencia y obtenemos de ella el numero de operaci�n y la fecha de cargo
         */
        RegistroTransferencia reg = null;

        try {
            String estadoIngreso = TablaValores.getValor(TTFF_CONFIG, "estados", "ingreso");

            reg = registraIngresoTrf(tipoCuentaOrigen, ctaOrigen, monto, fechaCargo, estadoIngreso, fechaAbono,
                tipoCuentaDestino, ctaDestino, rutDestino, codBanco, comentario, nombreDestino, eMailDestino,
                cliente.getRut(), cliente.getDigito(), canal);
        }
        catch (Exception e) {
            mensaje = "REG_ITF" + ", CuentaOrigen =[" + ctaOrigen + "]" + ", CuentaDestino=[" + ctaDestino + "]"
                + ", Monto=[" + monto + "]";
            logCliente("CTACTE_CTACTE", mensaje, e, cliente.getRut(), cliente.getDigito(), canal);
            if (logger.isEnabledFor(Level.ERROR)) {
                logger.error("[transfiereCtaCteHaciaCtaCte]  General ", e);
            }
            throw new TransferenciaException("TRF001");
        }

        boolean montoValido = true;
        try {
            double topeBanco = 0;
            try {
                topeBanco = maximoTopeCuenta(cliente.getRut(), canal, tipoCuentaOrigen, rutDestino,
                    tipoAutenticacion);
            }
            catch (Exception e) {
                logCliente("CTACTE_CTACTE", "Fallo por no poder obtener TOPE BANCO", e, cliente.getRut(), cliente
                    .getDigito(), canal);
                registraFinTrf(Long.parseLong(reg.numeroOperacion), null, null, fechaCargo, fechaAbono,
                    TablaValores.getValor(TTFF_CONFIG, "estados", "finError"), cliente.getRut(), cliente
                        .getDigito(), canal);
                montoValido = false;
                registraFinTrf(Long.parseLong(reg.numeroOperacion), null, null, fechaCargo, fechaAbono,
                    TablaValores.getValor(TTFF_CONFIG, "estados", "finError"), cliente.getRut(), cliente
                        .getDigito(), canal);
                if (logger.isEnabledFor(Level.ERROR)) {
                    logger.error("[transfiereCtaCteHaciaCtaCte]  Excepcion General  ", e);
                }
                throw new TransferenciaException("TRF001");
            }

            double montoDia = 0;
            try {
                montoDia = montoTransferidoDia(cliente.getRut(), tipoCuentaOrigen, ctaOrigen, new Date());
            }
            catch (Exception e) {
                logCliente("CTACTE_CTACTE", "Fallo por no poder obtener MONTO TRANSFERIDO DIA", e, cliente
                    .getRut(), cliente.getDigito(), canal);
                registraFinTrf(Long.parseLong(reg.numeroOperacion), null, null, fechaCargo, fechaAbono,
                    TablaValores.getValor(TTFF_CONFIG, "estados", "finError"), cliente.getRut(), cliente
                        .getDigito(), canal);
                montoValido = false;
                if (logger.isEnabledFor(Level.ERROR)) {
                    logger.error("[transfiereCtaCteHaciaCtaCte]  Excepcion General  ", e);
                }
                throw new TransferenciaException("TRF001");
            }

            if (cliente.getRut() != rutDestino) {
                if (monto > (topeBanco - montoDia - montoProgramado)) {
                    mensaje = "ValidaMonto" + ", Monto=" + monto + ", TopeBanco=" + topeBanco + ", montoDia="
                        + montoDia + ", montoProgramado=" + montoProgramado;
                    logCliente("CTACTE_CTACTE", mensaje, cliente.getRut(), cliente.getDigito(), canal);
                    registraFinTrf(Long.parseLong(reg.numeroOperacion), null, null, fechaCargo, fechaAbono,
                        TablaValores.getValor(TTFF_CONFIG, "estados", "finError"), cliente.getRut(), cliente
                            .getDigito(), canal);
                    montoValido = false;
                    throw new TransferenciaException("TRF042");
                }
            }

            logCliente("CTACTE_CTACTE", "EfectuandoTransferencia, MontoDia=" + montoDia + ", TopeBanco="
                + topeBanco + ", Programado=" + montoProgramado, cliente.getRut(), cliente.getDigito(), canal);
            realizarTransferenciaCtaCteHaciaCtaCte(ctaOrigen, monto, ctaDestino, cliente.getRut(), cliente
                .getDigito(), canal);
        }
        catch (TransferenciaException e) {
            if (montoValido) {
                registraFinTrf(Long.parseLong(reg.numeroOperacion), null, null, fechaCargo, fechaAbono,
                    TablaValores.getValor(TTFF_CONFIG, "estados", "finError"), cliente.getRut(), cliente
                        .getDigito(), canal);
            }
            if (logger.isEnabledFor(Level.ERROR)) {
                logger.error("[transfiereCtaCteHaciaCtaCte]  Excepcion General  ", e);
            }
            throw e;
        }

        /*
         * Se debe tener presente que tanto la fecha de Cargo como la fecha de Abono debieran ser las contables que
         * tiene tandem. El problema es que la transacci�n B0671 NO retorna dicha fecha y no existe alguna forma de
         * obtenerla. Por lo tanto se decidi� mientras manejar la fecha del proceso como fechas de Cargo y Abono
         * (4to y 5to par�metro)
         */

        registraFinTrf(Long.parseLong(reg.numeroOperacion), null, null, fechaCargo, fechaAbono, TablaValores
            .getValor(TTFF_CONFIG, "estados", "fin"), cliente.getRut(), cliente.getDigito(), canal);

        DetalleTransferencia detalle = new DetalleTransferencia();
        detalle.ctaOrigen = ctaOrigen;
        detalle.tipoCuentaOrigen = tipoCuentaOrigen;
        detalle.ctaDestino = ctaDestino;
        detalle.tipoCuentaDestino = tipoCuentaDestino;
        detalle.montoTransferencia = monto;
        detalle.numeroOperacion = reg.numeroOperacion;
        detalle.codigoBancoDestino = codBanco;
        detalle.codigoBancoOrigen = codBanco;
        detalle.rut = cliente.getRut();
        detalle.dig = cliente.getDigito();
        detalle.rutDestino = rutDestino;
        detalle.digDestino = digDestino;
        detalle.nombreDestino = nombreDestino;
        detalle.comentario = comentario;
        detalle.canalID = canal;
        detalle.eMailCliente = eMailCliente; // Puede ser null
        detalle.eMailDestino = eMailDestino; // Puede ser null
        detalle.fechaTransferencia = fechaCargo;

        detalle.fechaCargo = fechaCargo;
        detalle.fechaAbono = fechaAbono;
        logCliente("CTACTE_CTACTE", "Cliente seteado como atributo", cliente.getRut(), cliente.getDigito(), canal);
        detalle.nombreOrigen = cliente.getFullName();
        detalle.nombreBancoOrigen = TablaValores.getValor(TTFF_CONFIG, "codigoBCI", "desc_" + canal);
        detalle.nombreBancoDestino = TablaValores.getValor(TTFF_CONFIG, "codigoBCI", "desc_" + canal);

        logCliente("CTACTE_CTACTE", "No se viene de TTTFF Programadas, se envian los correos", cliente.getRut(),
            cliente.getDigito(), canal);
        enviaEmail(detalle, false, cliente, canal);
        enviaMailCliente(detalle, cliente, canal);
        logCliente("CTACTE_CTACTE", "FIN TRANSFERENCIA", cliente.getRut(), cliente.getDigito(), canal);
        return detalle;
    }

    /**
     * M�todo migrado desde TransferenciasFondosBean, encargado de transferir fondos de una cuenta origen (cargo) a
     * una cuenta destino (abono). Se agregan los par�metros long rut, char dig y String canal. Se agrega la
     * constante FORMATO_CUENTA que reemplaza el valor en duro "00000000".
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 27/07/2012 Jorge Leiva A. (TINet): Versi�n inicial.
     * </ul>
     * <p>
     *
     * @param ctaOrigen cuenta de origen.
     * @param monto monto a transferir.
     * @param ctaDestino cuenta de destino.
     * @param rut rut del cliente.
     * @param dig digito verificador.
     * @param canal identificador del canal.
     * @return TransferenciaFondos detalle de la transferencia.
     * @throws TransferenciaException excepci�n.
     * @since 2.4
     */
    private TransferenciaFondos realizarTransferenciaCtaCteHaciaCtaCte(String ctaOrigen, double monto,
        String ctaDestino, long rut, char dig, String canal) throws TransferenciaException {
        try {
            logCliente("CTECPR_CTECPR", "ctaOrigen=" + ctaOrigen + ", ctaDestino=" + ctaDestino + ", monto="
                + monto, rut, dig, canal);
            ctaOrigen = Formatting.number(Integer.parseInt(ctaOrigen), FORMATO_CUENTA);
            ctaDestino = Formatting.number(Integer.parseInt(ctaDestino), FORMATO_CUENTA);

            CuentaCorriente cc = new CuentaCorriente(ctaOrigen);
            return cc.transfiereFondos(ctaDestino, monto);
        }
        catch (Exception e) {
            String mensaje = "CuentaOrigen=" + ctaOrigen + ", CuentaDestino=" + ctaDestino + ", MontoTrf=" + monto;
            logCliente("CTECPR_CTECPR", mensaje, e, rut, dig, canal);
            if (logger.isEnabledFor(Level.ERROR)) {
                logger.error("[realizarTransferenciaCtaCteHaciaCtaCte]  transfiereFondos  ", e);
}
            throw new TransferenciaException("TRF001");
        }
    }

    /**
     * M�todo migrado desde {@link TransferenciasFondosBean}, encargado de obtener el detalle de una
     * transferencias entre una cuenta corriente y una cuenta de ahorro.
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 27/07/2012 Jorge Leiva A. (TINet): Versi�n inicial.
     * </ul>
     * <p>
     *
     * @param ctaOrigen cuenta de origen.
     * @param tipoCuentaOrigen tipo de cuenta origen.
     * @param ctaDestino cuenta de destino.
     * @param monto monto a transferir.
     * @param rutDestino rut del destinatario.
     * @param digDestino digito verificador del destinatario.
     * @param comentario comentario.
     * @param nombreDestino nombre del destinatario.
     * @param eMailCliente email del cliente.
     * @param eMailDestino email del destinatario.
     * @param tipoAutenticacion tipo de autentificaci�n.
     * @param cliente objeto con los datos del cliente.
     * @param canal identificador del canal.
     * @return DetalleTransferencia detalle de la transferncia realizada.
     * @throws TransferenciaException excepci�n.
     * @since 2.4
     */
    public DetalleTransferencia transfiereCtaCteHaciaAhorro(String ctaOrigen, String tipoCuentaOrigen,
        String ctaDestino, double monto, long rutDestino, char digDestino, String comentario,
        String nombreDestino, String eMailCliente, String eMailDestino, String tipoAutenticacion, Cliente cliente,
        String canal) throws TransferenciaException {
        return transfiereCtaCteHaciaAhorro(ctaOrigen, tipoCuentaOrigen, ctaDestino, monto, rutDestino, digDestino,
            comentario, nombreDestino, eMailCliente, eMailDestino, tipoAutenticacion, false, cliente, canal);
    }

    /**
     * M�todo migrado desde TransferenciasFondosBean, encargado de obtener el detalle de transferencias entre una
     * cuenta corriente y una cuenta de ahorro. Se agregaron los par�metros Cliente cliente y String canal. Se
     * agrega la constante INICIO_TRANSFERENCIA que reemplaza el valor en duro "INICIO TRANSFERENCIA". Se agrega la
     * constante FORMATO_CUENTA que reemplaza el valor en duro "00000000".
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 27/07/2012 Jorge Leiva A. (TINet): Versi�n inicial.
     * </ul>
     * <p>
     *
     * @param ctaOrigen cuenta de origen.
     * @param tipoCuentaOrigen tipo de cuenta origen.
     * @param ctaDestino cuenta de destino.
     * @param monto monto a transferir.
     * @param rutDestino rut del destinatario.
     * @param digDestino digito verificador del destinatario.
     * @param comentario comentario.
     * @param nombreDestino nombre del destinatario.
     * @param eMailCliente email del cliente.
     * @param eMailDestino email del destinatario.
     * @param tipoAutenticacion tipo de autentificaci�n.
     * @param esProgramada transferencia programada.
     * @param cliente objeto con los datos del cliente.
     * @param canal identificador del canal.
     * @return DetalleTransferncia detalle de la transferencia realizada.
     * @throws TransferenciaException excepci�n.
     * @since 2.4
     */
    private DetalleTransferencia transfiereCtaCteHaciaAhorro(String ctaOrigen, String tipoCuentaOrigen,
        String ctaDestino, double monto, long rutDestino, char digDestino, String comentario,
        String nombreDestino, String eMailCliente, String eMailDestino, String tipoAutenticacion,
        boolean esProgramada, Cliente cliente, String canal) throws TransferenciaException {
        String mensaje = INICIO_TRANSFERENCIA;
        logCliente("CTACTE_AHO", ctaOrigen, tipoCuentaOrigen, ctaDestino, "AHO", monto, rutDestino, digDestino,
            "016", mensaje, cliente.getRut(), cliente.getDigito(), canal);
        String codBanco = TablaValores.getValor(TTFF_CONFIG, "codigoBCI", "valor");
        String tipoCuentaDestino = TablaValores.getValor(TTFF_CONFIG, "tiposCuenta", "ahorro");

        ctaOrigen = Formatting.number(Integer.parseInt(ctaOrigen), FORMATO_CUENTA);

        Date fechaCargo = new Date();
        Date fechaAbono = new Date();

        // Validar saldo
        try {
            new CuentaCorriente(ctaOrigen).getSaldo();
        }
        catch (Exception e) {
            mensaje = "SaldoCtaCte" + ", CuentaOrigen =[" + ctaOrigen + "]" + ", CuentaDestino=[" + ctaDestino
                + "]" + ", Monto=[" + monto + "]";
            logCliente("CTACTE_AHO", mensaje, e, cliente.getRut(), cliente.getDigito(), canal);
            if (logger.isEnabledFor(Level.ERROR)) {
                logger.error("[transfiereCtaCteHaciaAhorro]  getSaldo  ", e);
            }
            if (esProgramada) {
                if (e instanceof GeneralException) {
                    GeneralException ge = (GeneralException) e;
                    throw new TransferenciaException(ge.getCodigo());
                }
            }
            throw new TransferenciaException("TRF001");
        }

        double montoProgramado = 0.0;
        if (!esProgramada) {
            logCliente("CTACTE_AHO", "Obtenemos monto programado", cliente.getRut(), cliente.getDigito(), canal);
            try {
                montoProgramado = montoProgramadoDia(cliente.getRut(), tipoCuentaOrigen, ctaOrigen, new Date());
            }
            catch (Exception e) {
                mensaje = "MontoProgramado" + ", CuentaOrigen =[" + ctaOrigen + "]" + ", CuentaDestino=["
                    + ctaDestino + "]" + ", Monto=[" + monto + "]";
                logCliente("CTACTE_AHO", mensaje, e, cliente.getRut(), cliente.getDigito(), canal);
                if (logger.isEnabledFor(Level.ERROR)) {
                    logger.error("[transfiereCtaCteHaciaAhorro]  montoProgramadoDia ", e);
                }
                throw new TransferenciaException("TRF001");
            }
        }
        // Debemos validar el monto a transferir + el monto programado
        // independiente de que la programaci�n haya sido efectuada ya.
        if (!validarSaldoCtaCte(cliente, canal, ctaOrigen, monto + montoProgramado, true)) {
            throw new TransferenciaException("0020");
        }

        /*
         * Registramos la transferencia y obtenemos de ella el numero de operaci�n y la fecha de cargo
         */
        RegistroTransferencia reg = null;
        try {
            reg = registraIngresoTrf(tipoCuentaOrigen, ctaOrigen, monto, fechaCargo, TablaValores.getValor(
                TTFF_CONFIG, "estados", "ingreso"), fechaAbono, tipoCuentaDestino, ctaDestino, rutDestino,
                codBanco, comentario, nombreDestino, eMailDestino, cliente.getRut(), cliente.getDigito(), canal);
        }
        catch (Exception e) {
            mensaje = "REG_ITF" + ", CuentaOrigen =[" + ctaOrigen + "]" + ", CuentaDestino=[" + ctaDestino + "]"
                + ", Monto=[" + monto + "]";
            logCliente("CTACTE_AHO", mensaje, e, cliente.getRut(), cliente.getDigito(), canal);
            if (logger.isEnabledFor(Level.ERROR)) {
                logger.error("[transfiereCtaCteHaciaAhorro]  registraIngresoTrf ", e);
            }
            throw new TransferenciaException("TRF001");
        }

        boolean montoValido = true;
        try {
            double topeBanco = 0.0;
            try {
                topeBanco = maximoTopeCuenta(cliente.getRut(), canal, tipoCuentaOrigen, rutDestino,
                    tipoAutenticacion);
            }
            catch (Exception e) {
                logCliente("CTACTE_AHO", "Fallo por no poder obtener TOPE BANCO", e, cliente.getRut(), cliente
                    .getDigito(), canal);
                registraFinTrf(Long.parseLong(reg.numeroOperacion), null, null, fechaCargo, fechaAbono,
                    TablaValores.getValor(TTFF_CONFIG, "estados", "finError"), cliente.getRut(), cliente
                        .getDigito(), canal);
                montoValido = false;
                if (logger.isEnabledFor(Level.ERROR)) {
                    logger.error("[transfiereCtaCteHaciaAhorro]  Excepcion General  ", e);
                }
                throw new TransferenciaException("TRF001");
            }

            double montoDia = 0.0;
            try {
                montoDia = montoTransferidoDia(cliente.getRut(), tipoCuentaOrigen, ctaOrigen, new Date());
            }
            catch (Exception e) {
                logCliente("CTACTE_AHO", "Fallo por no poder obtener MONTO TRANSFERIDO DIA", e, cliente.getRut(),
                    cliente.getDigito(), canal);
                registraFinTrf(Long.parseLong(reg.numeroOperacion), null, null, fechaCargo, fechaAbono,
                    TablaValores.getValor(TTFF_CONFIG, "estados", "finError"), cliente.getRut(), cliente
                        .getDigito(), canal);
                montoValido = false;
                if (logger.isEnabledFor(Level.ERROR)) {
                    logger.error("[transfiereCtaCteHaciaAhorro]  Excepcion General  ", e);
                }
                throw new TransferenciaException("TRF001");
            }

            if (cliente.getRut() != rutDestino) {
                if (monto > (topeBanco - montoDia - montoProgramado)) {
                    mensaje = "ValidaMonto" + ", Monto=" + monto + ", TopeBanco=" + topeBanco + ", montoDia="
                        + montoDia + ", montoProgramado=" + montoProgramado;
                    logCliente("CTACTE_AHO", mensaje, cliente.getRut(), cliente.getDigito(), canal);
                    registraFinTrf(Long.parseLong(reg.numeroOperacion), null, null, fechaCargo, fechaAbono,
                        TablaValores.getValor(TTFF_CONFIG, "estados", "finError"), cliente.getRut(), cliente
                            .getDigito(), canal);
                    montoValido = false;
                    throw new TransferenciaException("TRF042");
                }
            }
            logCliente("CTACTE_AHO", "EfectuandoTransferencia, MontoDia=" + montoDia + ", TopeBanco=" + topeBanco
                + ", Programado=" + montoProgramado, cliente.getRut(), cliente.getDigito(), canal);
            realizarTransferenciaCtaCteHaciaAhorro(ctaOrigen, monto, ctaDestino, cliente.getRut(), cliente
                .getDigito(), canal);
        }
        catch (TransferenciaException e) {
            if (montoValido) {
                registraFinTrf(Long.parseLong(reg.numeroOperacion), null, null, fechaCargo, fechaAbono,
                    TablaValores.getValor(TTFF_CONFIG, "estados", "finError"), cliente.getRut(), cliente
                        .getDigito(), canal);
            }
            if (logger.isEnabledFor(Level.ERROR)) {
                logger.error("[transfiereCtaCteHaciaAhorro]  Excepcion General  ", e);
            }
            throw e;
        }

        /*
         * Se debe tener presente que tanto la fecha de Cargo como la fecha de Abono debieran ser las contables que
         * tiene tandem. El problema es que la transacci�n B0671 NO retorna dicha fecha y no existe alguna forma de
         * obtenerla. Por lo tanto se decidi� mientras manejar la fecha del proceso como fechas de Cargo y Abono
         * (4to y 5to par�metro)
         */

        registraFinTrf(Long.parseLong(reg.numeroOperacion), null, null, fechaCargo, fechaAbono, TablaValores
            .getValor(TTFF_CONFIG, "estados", "fin"), cliente.getRut(), cliente.getDigito(), canal);

        DetalleTransferencia detalle = new DetalleTransferencia();
        detalle.ctaOrigen = ctaOrigen;
        detalle.tipoCuentaOrigen = tipoCuentaOrigen;
        detalle.ctaDestino = ctaDestino;
        detalle.tipoCuentaDestino = tipoCuentaDestino;
        detalle.montoTransferencia = monto;
        detalle.numeroOperacion = reg.numeroOperacion;
        detalle.codigoBancoDestino = codBanco;
        detalle.codigoBancoOrigen = codBanco;
        detalle.rut = cliente.getRut();
        detalle.dig = cliente.getDigito();
        detalle.rutDestino = rutDestino;
        detalle.digDestino = digDestino;
        detalle.nombreDestino = nombreDestino;
        detalle.comentario = comentario;
        detalle.canalID = canal;
        detalle.eMailCliente = eMailCliente; // Puede ser null
        detalle.eMailDestino = eMailDestino; // Puede ser null
        detalle.fechaTransferencia = fechaCargo;

        detalle.fechaCargo = fechaCargo;
        detalle.fechaAbono = fechaAbono;

        logCliente("CTACTE_CTACTE", "Cliente seteado como atributo", cliente.getRut(), cliente.getDigito(), canal);
        detalle.nombreOrigen = cliente.getFullName();


        detalle.nombreBancoOrigen = TablaValores.getValor(TTFF_CONFIG, "codigoBCI", "desc_" + canal);
        detalle.nombreBancoDestino = TablaValores.getValor(TTFF_CONFIG, "codigoBCI", "desc_" + canal);


        logCliente("CTACTE_CTACTE", "No se viene de TTTFF Programadas, se envian los correos", cliente.getRut(),
            cliente.getDigito(), canal);
        enviaEmail(detalle, false, cliente, canal);
        enviaMailCliente(detalle, cliente, canal);

        logCliente("CTACTE_AHO", "FIN TRANSFERENCIA", cliente.getRut(), cliente.getDigito(), canal);
        return detalle;
    }

    /**
     * M�todo migrado desde {@link TransferenciasFondosBean}, encargado de transferir fondos desde una cuenta
     * origen a una cuenta destino. Se agregaron los par�metros long rut, char dig y String canal. Se agrega la
     * constante FORMATO_CUENTA que reemplaza el valor en duro "00000000".
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 27/07/2012 Jorge Leiva A. (TINet): Versi�n inicial.
     * </ul>
     * <p>
     *
     * @param ctaOrigen cuenta de origen.
     * @param monto monto a transferir.
     * @param ctaDestino cuenta de destino.
     * @param rut rut del cliente.
     * @param dig digito verificador.
     * @param canal identificador del canal.
     * @return TransferenciaFondos detalle de la transferencia.
     * @throws TransferenciaException excepci�n.
     */
    private wcorp.serv.ahorros.TransferenciaFondos realizarTransferenciaCtaCteHaciaAhorro(String ctaOrigen,
        double monto, String ctaDestino, long rut, char dig, String canal) throws TransferenciaException {
        try {
            logCliente("DEPOSITAHO", "ctaOrigen=" + ctaOrigen + ", ctaDestino=" + ctaDestino + ", monto=" + monto,
                rut, dig, canal);
            ctaOrigen = Formatting.number(Long.parseLong(ctaOrigen), FORMATO_CUENTA);
            CuentaAhorro cta = new CuentaAhorro(ctaDestino);
            return cta.depositaDesdeCtaCte(ctaOrigen, monto);
        }
        catch (GeneralException ge) {
            String mensaje = "CuentaOrigen=" + ctaOrigen + ", CuentaDestino=" + ctaDestino + ", MontoTrf=" + monto;
            logCliente("DEPOSITAHO", mensaje, ge, rut, dig, canal);
            if (logger.isEnabledFor(Level.ERROR)) {
                logger.error("[realizarTransferenciaCtaCteHaciaAhorro]  Excepcion General  ", ge);
            }
            if (ge.getCodigo().trim().equalsIgnoreCase("0029")) {
                throw new TransferenciaException("TRF0027");
            }
            else {
                throw new TransferenciaException("TRF001");
            }
        }
        catch (Exception e) {
            String mensaje = "CuentaOrigen=" + ctaOrigen + ", CuentaDestino=" + ctaDestino + ", MontoTrf=" + monto;
            logCliente("DEPOSITAHO", mensaje, e, rut, dig, canal);
            if (logger.isEnabledFor(Level.ERROR)) {
                logger.error("[realizarTransferenciaCtaCteHaciaAhorro]  Excepcion General  ", e);
            }
            throw new TransferenciaException("TRF001");
        }
    }

    /**
     * M�todo que valida si un alias es seguro para crear.
     *
     * <p>El m�todo valida que el alias a crear no viola alguna de las siguientes reglas:
     * <ol>
     *      <li>Nombre de alias no debe existir en alias del cliente</li>
     *      <li>Tr�o rut destino, cuenta destino, banco destino no debe existir en alias del cliente</li>
     *      <li>Tr�o rut destino, cuenta destino, banco destino no debe existir en alias del cliente
     *          eliminados en las �ltimas N horas, donde N es un valor entregado v�a tabla de par�metros.</li>
     * </ol>
     *
     * <p>Registro de versiones:
     * <ul>
     * <li>1.0 03/07/2013 Eduardo Villagr�n Morales (Imagemaker): Versi�n inicial.<li>
     * <li>1.1 05/09/2017 Luis Lopez Alamos (SEnTRA) - Ricardo Carrasco C�ceres (Ing. Soft. BCI): Se agrega l�gica que permite obviar la restricci�n de creaci�n de 24 horas,
     *  siempre y cuando el atributo que viene del alias es falso. De ser as�, se toma en consideraci�n el valor de la ventana sin restricci�n de tabla de parametros.</li> 
     * </ul>
     *
     * @since 2.10
     * @param alias El alias a validar.
     * @throws GeneralException se lanza �sta para cualquier otra excepci�n.
     *
     */
    private void validaAliasSeguro(AliasTTFFDVO alias) throws GeneralException{
    	if (logger.isEnabledFor(Level.INFO)){
            logger.info("[validaAliasSeguro][BCI_INI][Alias : " + alias.toString() + "]");
        }
        try{
        	
            envInit();
            int ventana = Integer.parseInt(TablaValores.getValor(TTFF_CONFIG, "ventanaAliasEliminado", "horas"));
            if (alias!=null && !alias.isRestriccionRegla24Horas()) {
            	logger.debug("[validaAliasSeguro] No se aplicar� restricci�n para creaci�n");
            	ventana = Integer.parseInt(TablaValores.getValor(TTFF_CONFIG, "ventanaAliasEliminadoSinRestriccion", "horas")==null ? String.valueOf(ventana) : TablaValores.getValor(TTFF_CONFIG, "ventanaAliasEliminadoSinRestriccion", "horas"));
            }
            if (logger.isDebugEnabled()){
                logger.debug("[validaAliasSeguro] Ventana :" + ventana);
            }
            int respuesta = ttffDAO.validaAliasSeguro(alias, ventana);

            switch (respuesta){
                case 0:
                	if (logger.isEnabledFor(Level.INFO)){
                        logger.info("[validaAliasSeguro][BCI_FINOK]");
                    }
                    return;
                case 1:
                    if (logger.isDebugEnabled()){
                        logger.debug("[validaAliasSeguro] alias ya existe.");
                    }
                    throw new GeneralException("TRF001");
                default:
                    if (logger.isDebugEnabled()){
                        logger.debug("[validaAliasSeguro] tr�o cta, bco y rut destino existe.");
                    }
                    throw new GeneralException("TRF0" + respuesta);
            }
        }
        catch (GeneralException exp){
            if (logger.isDebugEnabled()){
                logger.debug("[validaAliasSeguro] Validaci�n no pasada.", exp);
            }
            throw exp;
        }
        catch (Exception exp){
            if (logger.isDebugEnabled()){
                logger.debug("[validaAliasSeguro] Error General", exp);
            }
            throw new GeneralException("TRF001");
        }
    }

    /**
     * Permite ingresar un registro en la tabla de duplicados de TTFF.
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 27/08/2013 Eduardo Villagr�n Morales. (ImageMaker): Versi�n inicial.
     * <li>1.1 04/11/2015 Luis Lopez Alamos (SEnTRA) - Claudia Lopez Pavlicevic (ing. Soft. BCI): Se elimina formateo de rutAutorizador y de Cuenta de origen.</li>
     * </ul>
     *
     * @since 3.1
     * @param duplicada VO con datos de transferencias duplicadas.
     * @return si se ingres� el registro.
     * @throws Exception excepci�n al ingresar duplicado.
     */
    public boolean grabarRegistroTransfDup(TransferenciaDuplicadaVO duplicada) throws Exception{
        boolean retorno = true;
        if(logger.isInfoEnabled()){
            logger.info("[grabarRegistroTransfDup] rut[" + duplicada.getRutAutorizador()
                    + "], rutOriginador[" + duplicada.getRutOrigen() + "], ctaOrigen["
                    + duplicada.getCuentaOrigen() + "], monto[" + duplicada.getMontoTransferencia()
                    + "], canal[" + duplicada.getCanal() + "], convenio [" + duplicada.getConvenio()
                    + "], tracenumber [" + duplicada.getTraceNumber() + "], fechaAnterior["
                    + duplicada.getFechaAnterior() + "], fechaActual[" + duplicada.getFechaActual()
                    + "] rutDestino [" + duplicada.getRutDestino() + "], ctaDestino["
                    + duplicada.getCuentaDestino() + "], bancoDestino["
                    + duplicada.getCodigoBancoDestino() + "]");
        }

        try{
            HashMap paramEntrada = new HashMap();

            Calendar fechaAnt = Calendar.getInstance();
            fechaAnt.setTime(duplicada.getFechaAnterior());

            Calendar fechaAct = Calendar.getInstance();
            fechaAct.setTime(duplicada.getFechaActual());

            paramEntrada.put("rutAutorizador", duplicada.getRutAutorizador());
            paramEntrada.put("rutCuentaOrigen", duplicada.getRutOrigen());
            paramEntrada.put("cuentaOrigen", duplicada.getCuentaOrigen());
            paramEntrada.put("montoTTFF", Double.valueOf(String.valueOf(duplicada.getMontoTransferencia())));
            paramEntrada.put("canal", Integer.valueOf(String.valueOf(duplicada.getCanal())));
            paramEntrada.put("convenio", duplicada.getConvenio() == null ? "" : duplicada.getConvenio());
            paramEntrada.put("traceNumber", duplicada.getTraceNumber());
            paramEntrada.put("fechaAnterior", fechaAnt);
            paramEntrada.put("fechaActual", fechaAct);
            paramEntrada.put("rutCuentaDestino", duplicada.getRutDestino());
            paramEntrada.put("cuentaDestino", duplicada.getCuentaDestino());
            paramEntrada.put("codigoBancoDestino", duplicada.getCodigoBancoDestino());

            String contexto = TablaValores.getValor(JNDI_CONFIG, "cluster", "param");
            ConectorServicioDePersistenciaDeDatos conector
                = new ConectorServicioDePersistenciaDeDatos(contexto);

            int respuesta = ((Integer) conector.ejecutar("ctaster", "insEveDup", paramEntrada)).intValue();

            if(logger.isInfoEnabled()){
                logger.info("[grabarRegistroTransfDup][" + duplicada.getRutAutorizador()
                        + "] Respuesta: [" + respuesta + "]");
            }

            if (respuesta != 0) {
                retorno = false;
            }

            if(logger.isInfoEnabled()){
                logger.info("[grabarRegistroTransfDup][" + duplicada.getRutAutorizador()
                        +"] Retorno [" + retorno + "]");
            }
            return retorno;

        }
        catch(Exception e){
            if(logger.isEnabledFor(Level.WARN)){
                logger.warn("[grabarRegistroTransfDup][" + duplicada.getRutAutorizador()
                        +"] Error en insertar registro duplicado: "
                        +  e.getMessage(), e);
            }
            if(logger.isInfoEnabled()){
                logger.info("[grabarRegistroTransfDup][" + duplicada.getRutAutorizador()
                        + "] Retorna false por Exception.");
            }
            return false;
        }
    }

    /**
    * <p>M�todo encargado de consultar las transferencias de fondos recibidas por parte del banco BCI
    * y en un intervalo de tiempo dado permitiendo realizar consultas por bloque, es decir
    * el par�metro de entrada "cantidad de registros" indica cuantos registros se desean recibir del total y
    * el par�metro bloque en que "p�gina de resultado" se est� con respecto al total. Adem�s se incluye
    * b�squeda de informaci�n adicional de la transferencia.</p>
    *
    * Registro de versiones:<ul>
    *
    * <li>1.0 22/10/2013, M�nica Garc�s C. (Sermaluc): Versi�n Inicial.</li>
    * <ul>
    * @param rut Rut del cliente.
    * @param dv Digito Verificador del cliente.
    * @param periodo mes que se desea consultar.
    * @param cuenta cuenta del cliente que se desea consultar.
    * @param bloque bloque que se esta consultando.
    * @param cantidadRegistros cantidad de registros a consultar.
    * @param  obtenerNombres indicador para realizar busqueda de nombres.
    * @return transferencias enviadas al cliente
    * @throws Exception
    * @since 3.2
    */
   public TransferenciasRecibidasVO[] consultaTransferenciasRecibidasBancoBCI(long rut, char dv, Date periodo, String cuenta, int bloque, int cantidadRegistros, boolean obtenerNombres) throws Exception {
       logger.debug("[consultaTransferenciasRecibidasBancoBCI] :: inicio del metodo");
       TransferenciasRecibidasVO[] transferenciasRecibidasVO = null;
       try{
           TransferenciasFondosDAO transferenciasFondosDAO = new TransferenciasFondosDAO();
           if(logger.isDebugEnabled()){logger.debug("[consultaTransferenciasRecibidasBancoBCI] :: transferenciasFondosDAO " + transferenciasFondosDAO);}
           transferenciasRecibidasVO = transferenciasFondosDAO.consultaTransferenciasRecibidasBancoBCI(rut, dv, periodo, cuenta, bloque, cantidadRegistros);
           if(logger.isDebugEnabled()){logger.debug("[consultaTransferenciasRecibidasBancoBCI] :: transferenciasRecibidasVO " + transferenciasRecibidasVO);}
            if (logger.isDebugEnabled()) {
                logger
                    .debug("[consultaTransferenciasRecibidasBancoBCI] :: obtenerNombres "
                        + obtenerNombres);
            }
           if(obtenerNombres == true){
                if (transferenciasRecibidasVO != null && transferenciasRecibidasVO.length > 0) {

                    if (logger.isDebugEnabled()) {
                        logger
                            .debug("[consultaTransferenciasRecibidasBancoBCI] :: transferenciasRecibidasVO :: "
                                + transferenciasRecibidasVO.length);
                    }

               ClienteDAOIbatis clienteDAOIbatis = new ClienteDAOIbatis();
               Set ruts = new HashSet();

               long[] rutTransferencias = new long[transferenciasRecibidasVO.length];

               for(int i=0; i<transferenciasRecibidasVO.length; i++){
                        rutTransferencias[i] = Long.parseLong(transferenciasRecibidasVO[i]
                            .getRutOriginadorTransferencia());

                        if (logger.isDebugEnabled()) {
                            logger
                                .debug("[consultaTransferenciasRecibidasBancoBCI] :: rutTransferencias :: "
                                    + rutTransferencias[i]);
                        }
}

               for(int j=0; j<rutTransferencias.length; j++){
                   ruts.add(new Long(rutTransferencias[j]));
               }

               Map nombres = clienteDAOIbatis.consultaNombreClientes(ruts);

               for(int i=0; i<transferenciasRecibidasVO.length; i++){
                        long rutOriginador = Long.parseLong(transferenciasRecibidasVO[i]
                            .getRutOriginadorTransferencia());
                        if (logger.isDebugEnabled()) {
                            logger
                                .debug("[consultaTransferenciasRecibidasBancoBCI] :: rutOriginador :: "
                                    + rutOriginador);
                        }
                        DatosBasicosDelClienteTO nombreOriginador = (DatosBasicosDelClienteTO) nombres
                            .get(new Long(rutOriginador));
                        if (nombreOriginador == null) {
                            transferenciasRecibidasVO[i].setNombreOriginadorTransferencia("");
                        }
                        else {
                        transferenciasRecibidasVO[i]
                            .setNombreOriginadorTransferencia(nombreOriginador.getNombre());

                            if (logger.isDebugEnabled()) {
                                logger
                                    .debug("[consultaTransferenciasRecibidasBancoBCI] :: nombreOriginador :: "
                                        + nombreOriginador);
                    }
               }
                    }
               }
           }
        }
       catch(Exception e){
           if(logger.isEnabledFor(Level.ERROR)){logger.error("[consultaTransferenciasRecibidasCliente]:: Problemas al realizar la consulta, Exception["+e+"]");}
           throw e;
       }
       if (logger.isDebugEnabled()) {
           logger
               .debug("[consultaTransferenciasRecibidasBancoBCI] :: transferenciasRecibidasVO :: "
                   + transferenciasRecibidasVO.length);
           logger
           .debug("[consultaTransferenciasRecibidasBancoBCI] :: transferenciasRecibidasVO :: "
               + transferenciasRecibidasVO.toString());
       }
       return transferenciasRecibidasVO;
   }

   
   /**
    * 
    * M�todo obtenerEgifts.
    * Este m�todo permite llenar un arreglo de objetos con imagenes y categorias para presentar en el 
    * servicio Regalo Virtual, que inicialmente depende de las Transferencias de Fondos.
    * <p>
    * Registro de versiones:<ul>
    * 
    * <li>1.0 19/11/2014 Luis L�pez Alamos (SEnTRA): Versi�n inicial.
    * </ul><p>
    * @param canal de peticion.
    * @return EGIFT[].
    * @since 4.2
    */
   
   public EGift[] obtenerEgifts(String canal) throws Exception {
       
       if (getLogger().isEnabledFor(Level.INFO)){
           getLogger().info("[obtenerEgifts] ["+ canal +"] [BCI_INI]");
       }
       
       String canalDeConsulta = EGIFT + canal;
       
       try{
           int totalImagenes = Integer.valueOf(
               TablaValores.getValor(TTFF_CONFIG, canalDeConsulta,
                       CANT_EGIFT)).intValue();
       
           if (getLogger().isEnabledFor(Level.INFO)){
               getLogger().info("[obtenerEgifts] carga de canal : "+ canalDeConsulta 
                       + " un total de : " + totalImagenes +" Imagenes");
           }
       
           EGift[] arregloEgifts = new EGift[totalImagenes];
       
           for (int pagCnt = 1; pagCnt <= totalImagenes; pagCnt++) {
               String categoria = TablaValores.getValor(TTFF_CONFIG,
                       canalDeConsulta + GUION_EGIFT + pagCnt, CATEGORIA);
               String codigo = TablaValores.getValor(TTFF_CONFIG,
                       canalDeConsulta + GUION_EGIFT + pagCnt, CODIGO_EGIFT);
               String url = TablaValores.getValor(TTFF_CONFIG,
                       canalDeConsulta + GUION_EGIFT + pagCnt, URL_EGIFT);
               arregloEgifts[pagCnt-1] = new EGift();
               arregloEgifts[pagCnt-1].setCategoria(categoria);
               arregloEgifts[pagCnt-1].setCodigo(codigo);
               arregloEgifts[pagCnt-1].setUrlImagen(url);
           }   
           
           if (getLogger().isEnabledFor(Level.INFO)){
               getLogger().info("[obtenerEgifts] ["+ canal +"] [BCI_FINOK]");
           }   
           return arregloEgifts;
          
       }
       catch (Exception e){
           if (getLogger().isEnabledFor(Level.ERROR)){
               getLogger().error("[obtenerEgifts] ["+ canal +"] [BCI_FINEX] cargarEgits =<"
               		+ e.getMessage() + ">",e);
           }
           throw new Exception("Error en obtener Egifts " + e.getMessage());
       }
       
   }
   
   /**
    * <p>M�todo que retorna la variable logger de la clase.</p>
    * 
    * Registro de Versiones:
    * <ul>
    * <li>1.0 24/11/2014 Luis L�pez Alamos. (SEnTRA): versi�n inicial.</li>
    * </ul>
    * 
    * @return Logger Variable logger de la clase.
    * @since 4.2
    */       
   public Logger getLogger(){
       if (logger == null){
           logger = Logger.getLogger(this.getClass());
       }
       return logger;
   }
   
      
   /**
    * M�todo que forma el cuerpo del Email del cliente Origen a ser enviado.    *
    * Registro de Versiones:<ul>
    *
    * <li>1.0 01/12/2015, Luis L�pez Alamos (SEnTRA): Versi�n inicial.</li>
    * </ul>
    *
    *
    * @param detalle Objeto con la informaci�n de la transferencia realizada.
    * @param cliente parametro de cliente.
    * @param canal parametro de canal.
    *
    * @since 4.3
    */
   private void envioMailEgiftOrigen(DetalleTransferencia detalle, Cliente cliente, String canal) {

       if (getLogger().isEnabledFor(Level.INFO)){
           getLogger().info("[envioMailEgiftOrigen]: " + cliente.getRut() + "[BCI_INI]");
       }
       
       if (detalle.eMailCliente == null || detalle.eMailCliente.trim().equals(""))
           return;
       
       String empresa = TablaValores.getValor(TTFF_CONFIG, "EmpresaEgift", "desc");
       String asuntoEmisor = TablaValores.getValor(TTFF_CONFIG, "asuntoEgiftEmisor", "desc");
       String asuntoDestinatario = TablaValores.getValor(TTFF_CONFIG, "asuntoEgiftDestinatario", "desc");
       String contrato = TablaValores.getValor(TTFF_CONFIG, "contratoEgift", "desc");
       String refTemplate = "";
       String enviado = TablaValores.getValor(TTFF_CONFIG, "enviado", "desc");
       String a = TablaValores.getValor(TTFF_CONFIG, "a", "desc");
       String contexto = TablaValores.getValor(JNDI_CONFIG, "cluster", "param");
               
       if (detalle.canalID.equals(CANAL_CORREO_BCI)){
           refTemplate = TablaValores.getValor(TTFF_CONFIG, "TEMPLATEBCI", "desc") ;
       }
       if (detalle.canalID.equals(CANAL_CORREO_TBANC)){
           refTemplate = TablaValores.getValor(TTFF_CONFIG, "TEMPLATETBANC", "desc") ;
       }
       if (detalle.canalID.equals(CANAL_CORREO_NOVA)){
           refTemplate = TablaValores.getValor(TTFF_CONFIG, "TEMPLATENOVA", "desc") ;
       }
       
       try{
           ConectorServicioEnvioSDP csesdp = new ConectorServicioEnvioSDP(contexto);
           
           Map parametrosMailEmisor = new HashMap();
           parametrosMailEmisor.put("fromName:",empresa);
           parametrosMailEmisor.put("to",detalle.eMailCliente);
           parametrosMailEmisor.put("subject", asuntoEmisor);
           parametrosMailEmisor.put("body", " ");
           parametrosMailEmisor.put("mainContent", "body");
           
           Date fecha = new Date();
           String montoOperacion = "$" + Formatting.number(detalle.getMontoTransferencia(),"#,##0"
                                   , new Locale("es","CL"));
           String rutModificado = Formatting.number(detalle.getRutDestino(),"#,##0", new Locale("es","CL"));
           String rutDestinatario = rutModificado + "-" + String.valueOf(detalle.getDigDestino()); 
           String fechaOperacion = new SimpleDateFormat("dd/MM/yyyy").format(fecha);
           String horaOperacion =  new SimpleDateFormat("HH:mm:ss").format(fecha);
           
           String descripcionCuenta = "";
           
           if (("CCT").equalsIgnoreCase(detalle.getTipoCuentaDestino())){
               descripcionCuenta = TablaValores.getValor(TTFF_CONFIG, "CCT", "desc");
           }
           else if (("CPR").equalsIgnoreCase(detalle.getTipoCuentaDestino())) {
               descripcionCuenta = TablaValores.getValor(TTFF_CONFIG, "CPR", "desc");
           }
           else if (("LDE").equalsIgnoreCase(detalle.getTipoCuentaDestino())) {
               descripcionCuenta = TablaValores.getValor(TTFF_CONFIG, "LDE", "desc");
           }
           else {
               descripcionCuenta = TablaValores.getValor(TTFF_CONFIG, "Otra", "desc");
           }
           
           
           Map parametrosTemplateEmisor = new HashMap();
           parametrosTemplateEmisor.put("imagen-egift", detalle.getEgift().getUrlImagen());
           parametrosTemplateEmisor.put("texto-egift", detalle.getEgift().getMensaje());
           parametrosTemplateEmisor.put("nombre-cliente-1", detalle.getNombreOrigen());
           parametrosTemplateEmisor.put("hora-operacion", horaOperacion);
           parametrosTemplateEmisor.put("enviado-recibido", enviado);
           parametrosTemplateEmisor.put("a-de", a);
           parametrosTemplateEmisor.put("nombre-cliente-2", detalle.getNombreDestino());
           parametrosTemplateEmisor.put("monto-operacion", montoOperacion);
           parametrosTemplateEmisor.put("fecha-operacion", fechaOperacion);
           parametrosTemplateEmisor.put("subject-egift", asuntoEmisor);
           parametrosTemplateEmisor.put("nro-operacion", detalle.getNumeroOperacion());
           parametrosTemplateEmisor.put("rut-destinatario", rutDestinatario);
           parametrosTemplateEmisor.put("banco-destinatario", detalle.getNombreBancoDestino());
           parametrosTemplateEmisor.put("tipo-cta-destinatario", descripcionCuenta);
           parametrosTemplateEmisor.put("email-destinatario", detalle.getEMailDestino());
           parametrosTemplateEmisor.put("nombre-destinatario", detalle.getNombreDestino());
           
           if (getLogger().isEnabledFor(Level.INFO)){
               getLogger().info("[envioMailEgiftOrigen]: Datos cliente : " + detalle.toString());
           }
           
           int resultadoEnvioEmisor = csesdp.sendMail(empresa, contrato, refTemplate, 
                    parametrosMailEmisor, parametrosTemplateEmisor);
           
           if (getLogger().isEnabledFor(Level.INFO)){
               getLogger().info("[envioMailEgiftOrigen] resultadoEnvio Emisor: " 
                                + resultadoEnvioEmisor);
            }
            
           
           }
           catch  (Exception e) {
            String mensaje2=
                   "MailFrom/to  ["
               + TablaValores.getValor(TTFF_CONFIG, "correo", "mailFrom")
               + "]"
               + ", Subject ["
               + asuntoDestinatario
               + "]: ";
               logCliente(cliente,canal,"mail latinia Egift", mensaje2+e);
       }
       
       if (getLogger().isEnabledFor(Level.INFO)){
           getLogger().info("[envioMailEgiftOrigen] " 
                            + cliente.getRut() + "[BCI_FINOK]");
        }
           
   }
   
   
   /**
    * M�todo que forma el cuerpo del Email del cliente Destino a ser enviado.    *
    * Registro de Versiones:<ul>
    *
    * <li>1.0 01/12/2015, Luis L�pez Alamos (SEnTRA): Versi�n inicial.</li>
    * </ul>
    *
    *
    * @param detalle Objeto con la informaci�n de la transferencia realizada.
    * @param cliente parametro cliente bci.
    * @param canal parametro canal.
    *
    * @since 4.3
    */
   private void envioMailEgiftDestino(DetalleTransferencia detalle, Cliente cliente, String canal) {

       if (getLogger().isEnabledFor(Level.INFO)){
           getLogger().info("[envioMailEgiftDestino]: " + cliente.getRut() + "[BCI_INI]");
       }
       
       if (detalle.eMailDestino == null || detalle.eMailDestino.trim().equals(""))
           return;
       
       String empresa = TablaValores.getValor(TTFF_CONFIG, "EmpresaEgift", "desc");
       String asuntoDestinatario = TablaValores.getValor(TTFF_CONFIG, "asuntoEgiftDestinatario", "desc");
       String contrato = TablaValores.getValor(TTFF_CONFIG, "contratoEgift", "desc");
       String refTemplate = "";
       String recibido = TablaValores.getValor(TTFF_CONFIG, "recibido", "desc");
       String de = TablaValores.getValor(TTFF_CONFIG, "de", "desc");
       String contexto = TablaValores.getValor(JNDI_CONFIG, "cluster", "param");
               
       if (detalle.canalID.equals(CANAL_CORREO_BCI)){
           refTemplate = TablaValores.getValor(TTFF_CONFIG, "TEMPLATEBCI", "desc") ;
       }
       if (detalle.canalID.equals(CANAL_CORREO_TBANC)){
           refTemplate = TablaValores.getValor(TTFF_CONFIG, "TEMPLATETBANC", "desc") ;
       }
       if (detalle.canalID.equals(CANAL_CORREO_NOVA)){
           refTemplate = TablaValores.getValor(TTFF_CONFIG, "TEMPLATENOVA", "desc") ;
       }
       
       try{
           ConectorServicioEnvioSDP csesdp = new ConectorServicioEnvioSDP(contexto);
           
           Map parametrosMailDestinatario = new HashMap();
           parametrosMailDestinatario.put("fromName:",empresa);
           parametrosMailDestinatario.put("to",detalle.eMailDestino);
           parametrosMailDestinatario.put("subject", asuntoDestinatario);
           parametrosMailDestinatario.put("body", " ");
           parametrosMailDestinatario.put("mainContent", "body");
           
           Date fecha = new Date();
           String montoOperacion = "$" + Formatting.number(detalle.getMontoTransferencia(),"#,##0"
                                   , new Locale("es","CL"));
           String rutModificado = Formatting.number(detalle.getRutDestino(),"#,##0", new Locale("es","CL"));
           String rutDestinatario = rutModificado + "-" + String.valueOf(detalle.getDigDestino()); 
           String fechaOperacion = new SimpleDateFormat("dd/MM/yyyy").format(fecha);
           String horaOperacion =  new SimpleDateFormat("HH:mm:ss").format(fecha);
           
           String descripcionCuenta = "";
           
           if (("CCT").equalsIgnoreCase(detalle.getTipoCuentaDestino())){
               descripcionCuenta = TablaValores.getValor(TTFF_CONFIG, "CCT", "desc");
           }
           else if (("CPR").equalsIgnoreCase(detalle.getTipoCuentaDestino())) {
               descripcionCuenta = TablaValores.getValor(TTFF_CONFIG, "CPR", "desc");
           }
           else if (("LDE").equalsIgnoreCase(detalle.getTipoCuentaDestino())) {
               descripcionCuenta = TablaValores.getValor(TTFF_CONFIG, "LDE", "desc");
           }
           else {
               descripcionCuenta = TablaValores.getValor(TTFF_CONFIG, "Otra", "desc");
           }
           
           Map parametrosTemplateDestinatario = new HashMap();
           parametrosTemplateDestinatario.put("imagen-egift", detalle.getEgift().getUrlImagen());
           parametrosTemplateDestinatario.put("texto-egift", detalle.getEgift().getMensaje());
           parametrosTemplateDestinatario.put("nombre-cliente-1", detalle.getNombreDestino());
           parametrosTemplateDestinatario.put("hora-operacion", horaOperacion);
           parametrosTemplateDestinatario.put("enviado-recibido", recibido);
           parametrosTemplateDestinatario.put("a-de", de);
           parametrosTemplateDestinatario.put("nombre-cliente-2", detalle.getNombreOrigen());
           parametrosTemplateDestinatario.put("monto-operacion", montoOperacion);
           parametrosTemplateDestinatario.put("fecha-operacion", fechaOperacion);
           parametrosTemplateDestinatario.put("subject-egift", asuntoDestinatario);
           parametrosTemplateDestinatario.put("nro-operacion", detalle.getNumeroOperacion());
           parametrosTemplateDestinatario.put("rut-destinatario", rutDestinatario);
           parametrosTemplateDestinatario.put("banco-destinatario", detalle.getNombreBancoDestino());
           parametrosTemplateDestinatario.put("tipo-cta-destinatario", descripcionCuenta);
           parametrosTemplateDestinatario.put("email-destinatario", detalle.getEMailDestino());
           parametrosTemplateDestinatario.put("nombre-destinatario", detalle.getNombreDestino());
           
           if (getLogger().isEnabledFor(Level.INFO)){
               getLogger().info("[envioMailEgiftDestino]: Datos cliente : " + detalle.toString());
           }
           int resultadoEnvioDestinatario = csesdp.sendMail(empresa, contrato, refTemplate, 
                   parametrosMailDestinatario, parametrosTemplateDestinatario);
                       
           if (getLogger().isEnabledFor(Level.INFO)){
                getLogger().info("[envioMailEgiftDestino] resultadoEnvio Destinatario: " 
                            + resultadoEnvioDestinatario);
            }
           
           }
           catch  (Exception e) {
            String mensaje2=
                   "MailFrom/to  ["
               + TablaValores.getValor(TTFF_CONFIG, "correo", "mailFrom")
               + "]"
               + ", Subject ["
               + asuntoDestinatario
               + "]: ";
               logCliente(cliente,canal,"mail latinia Egift", mensaje2+e);
       }
       
       if (getLogger().isEnabledFor(Level.INFO)){
           getLogger().info("[envioMailEgiftDestino] " 
                       + cliente.getRut() + "[BCI_FINOK]");
       }
           
   }
   
   /**
    * M�todo que valida que el servicio de fraude de transferencia de fondos se encuentre operativo. <br>
    * Registro de Versiones:
    * <ul>
    * <li>1.0 11/12/2014 Luis L�pez Alamos (SEnTRA): versi�n inicial
    * </ul>
    * <br>
    * @param canal parametro canal.
    * @return Devuelve si el servicio esta disponible.
    * @since 4.3
    */
   public boolean isValidaDisponibilidadServicioFraude(String canal) {
       if (getLogger().isEnabledFor(Level.INFO)){
           getLogger().info("[isValidaDisponibilidadServicioFraude][BCI_INI]");
       }    
       boolean disponibilidad = true;
       
       if (getLogger().isEnabledFor(Level.INFO)){
           getLogger().info("[isValidaDisponibilidadServicioFraude] canal de ingreso : " + canal);
       }
       try {
           SwicthAplicacionesSensiblesHelper.validaDisponibilidadServicio(canal,
                   SwicthAplicacionesSensiblesHelper.PRODUCTO_EVALUACION_ACTIMIZE_TTFF);
       }
       catch (GeneralException e) {
           if (getLogger().isEnabledFor(Level.ERROR)){
               getLogger().error("[isValidaDisponibilidadServicio][GeneralException] "
                       + "Error al validar servicio" + e.getMessage(),e);
           }
           disponibilidad = false;
       }
       
       if (getLogger().isEnabledFor(Level.INFO)){
           getLogger().info("[isValidaDisponibilidadServicioFraude][BCI_FINOK]");
       }    
       return disponibilidad;
   }


   /**
    * 
     * M�todo iniciarTransferencia.
     * <p>
     * Registro de versiones:<ul>
     * 
     * <li>1.0 23/06/2014 Alvaro Fuentes O. (TINet): Versi�n inicial.
     * <li>1.1 12/01/2015 Luis L�pez Alamos. (SEnTRA): Se agrega par�metro canal, para que cuando vaya a cargar
     *      la lista de regalos virtuales, s�lo recupere las imagenes permitidas para el canal de petici�n.
     *      Tambien se agrega condicion de habilitacion de servicio, que depende de Tabla de parametros definida
     *      Se regularizan ciertos logs de acuerdo a normativa vigente</li>
     * </ul><p>
     * @param rut rut.
     * @param dv dv.
     * @param canal canal web.
     * @return datos transferencia.
     * @throws Exception en caso de error.
     * @since 4.3
     */
    public DatosInicializacionTransferenciasTO iniciarTransferencia(long rut, char dv, String canal)
            throws Exception {
        
        if (getLogger().isInfoEnabled()) {
            getLogger().info("[iniciarTransferencia] [BCI_INI] Inicio " + rut);
        }
        
        DatosInicializacionTransferenciasTO datosInicializacionTransferenciasTO = 
            new DatosInicializacionTransferenciasTO();
        String validaEgift = TablaValores.getValor(TABLA_PARAMETRO_TRANSFERENCIA_TERCEROS, EGIFT_ACTIVO+canal,
                VALOR_EGIFT) != null ? TablaValores.getValor(TABLA_PARAMETRO_TRANSFERENCIA_TERCEROS,
                        EGIFT_ACTIVO+canal, VALOR_EGIFT) : EGIFT_NO_HABILITADA;
           if (getLogger().isEnabledFor(Level.DEBUG)){
               getLogger().debug("[iniciarTransferencia]["+rut+"] egift habilitada : " + validaEgift);
           }
                
        if (EGIFT_HABILITADA.equalsIgnoreCase(validaEgift)){
        datosInicializacionTransferenciasTO.seteGift(obtenerEgifts(canal));
        }
        Collection listado = getListaAliasTTFF(rut, dv);
        
        if (getLogger().isInfoEnabled()) {
            getLogger()
                .info(
                    "[iniciarTransferencia][" + rut + "] listado ");
        }
        BancosAutorizados[] bancosAutorizados = serviciosSipe.getBancosAutorizadosLinea();
        
        if (getLogger().isInfoEnabled()) {
            getLogger().info(
                "[iniciarTransferencia][" + rut + "] bancosAutorizados ["
                    + StringUtil.contenidoDe(bancosAutorizados) + "]");
        }        
        AliasTTFFDVO[] aliasRegistrados = (AliasTTFFDVO[]) listado.toArray(new AliasTTFFDVO[listado.size()]);
        
        if (getLogger().isInfoEnabled()) {
            getLogger().info(
                "[iniciarTransferencia][" + rut + "] aliasRegistrados ");
        }
        
        datosInicializacionTransferenciasTO.setAliasRegistrados(aliasRegistrados);
        bancosAutorizados = ordenarBancosAutorizados(bancosAutorizados);
        Map mapaBancosAutorizados = crearMapaBancosAutorizados(bancosAutorizados);
        
        if (getLogger().isInfoEnabled()) {
            getLogger().info(
                "[iniciarTransferencia][" + rut + "] mapaBancosAutorizados ");
        }
        
        datosInicializacionTransferenciasTO.setBancosAutorizados(bancosAutorizados);
        AliasTTFFDVO[] aliasAutorizados = filtrarAliasAutorizados(aliasRegistrados, mapaBancosAutorizados);
        
        if (getLogger().isInfoEnabled()) {
            getLogger().info(
                "[iniciarTransferencia][" + rut + "] aliasAutorizados ");
        }
        
        datosInicializacionTransferenciasTO.setAliasAutorizados(aliasAutorizados);
        Calendar fechaAnt = Calendar.getInstance();
        fechaAnt.add(Calendar.DATE, -1);
        datosInicializacionTransferenciasTO.setFechaProgramacionPropuesta(fechaAnt);
        
        if (getLogger().isInfoEnabled()) {
            getLogger().info(
                "[iniciarTransferencia][" + rut + "][BCI_FINOK]");
        }
        return datosInicializacionTransferenciasTO;
    }

    /**
     * 
     * M�todo crearMapaBancosAutorizados.
     * <p>
     * Registro de versiones:<ul>
     * 
     * <li>1.0 23/06/2014 Alvaro Fuentes O. (TINet): Versi�n inicial.
     * </ul><p>
     * @param bancosAutorizados bancos autorizados.
     * @return mapaBancosAutorizados.
     * @since 4.3
     */
    private HashMap crearMapaBancosAutorizados(BancosAutorizados[] bancosAutorizados) {
        if (obtenerLogger().isInfoEnabled()) {
            obtenerLogger().info("[crearMapaBancosAutorizados] [BCI_INI] Inicio");
        }        
        HashMap mapaBancosAutorizados = new HashMap();
        for (int i = 0; i < bancosAutorizados.length; i++) {
            mapaBancosAutorizados.put(Integer.valueOf(bancosAutorizados[i].getCodBanco()),
                bancosAutorizados[i].getNombre());
        }
        
        if (obtenerLogger().isInfoEnabled()) {
            obtenerLogger().info(
                "[crearMapaBancosAutorizados][BCI_FINOK]");
        }
        return mapaBancosAutorizados;
    }

   /**
    * 
    * M�todo ordenarBancosAutorizados.
    * <p>
    * Registro de versiones:<ul>
    * 
    * <li>1.0 23/06/2014 Alvaro Fuentes O. (TINet): Versi�n inicial.
    * </ul><p>
    * @param bancosAutorizados bancos autorizados.
    * @return listado bancos.
    * @since 4.3
    */
    private BancosAutorizados[] ordenarBancosAutorizados(BancosAutorizados[] bancosAutorizados) {
        if (obtenerLogger().isInfoEnabled()) {
            obtenerLogger().info("[ordenarBancosAutorizados] [BCI_INI] Inicio");
        }
        int recorreBancos;
        for (recorreBancos = 0; recorreBancos < bancosAutorizados.length; recorreBancos++) {
            if (bancosAutorizados[recorreBancos].getCodBanco().equals("016")) {
                BancosAutorizados tmpBanco = bancosAutorizados[recorreBancos];
                bancosAutorizados[recorreBancos] = bancosAutorizados[0];
                bancosAutorizados[0] = tmpBanco;
                recorreBancos = bancosAutorizados.length;
            }
        }
        if (obtenerLogger().isInfoEnabled()) {
            obtenerLogger().info("[ordenarBancosAutorizados][BCI_FINOK]");
        }
        return bancosAutorizados;
    }

    /**
     * 
     * M�todo filtrarAliasAutorizados.
     * <p>
     * Registro de versiones:<ul>
     * 
     * <li>1.0 23/06/2014 Juan Pablo Vega (TINet): Versi�n inicial.
     * </ul><p>
     * @param aliasRegistrados alias registrados.
     * @param mapaBancosAutorizados mapa bancos autorizados.
     * @return AliasTTFFDVO.
     * @since 4.3
     */
    private AliasTTFFDVO[] filtrarAliasAutorizados(AliasTTFFDVO[] aliasRegistrados, Map mapaBancosAutorizados) {
        
        if (obtenerLogger().isInfoEnabled()) {
            obtenerLogger().info("[filtrarAliasAutorizados] [BCI_INI] Inicio");
        }        
        List aliasAgregados = new ArrayList();
        int recorreAlias;
        for (recorreAlias = 0; recorreAlias < aliasRegistrados.length; recorreAlias++) {                        
            if (aliasRegistrados[recorreAlias].isAutorizado()
                && mapaBancosAutorizados.containsKey(new Integer(aliasRegistrados[recorreAlias].
                    getBancoCodigo()))) {
                aliasRegistrados[recorreAlias].setNombreBanco(String.valueOf(mapaBancosAutorizados
                        .get(new Integer(aliasRegistrados[recorreAlias].getBancoCodigo()))));
                aliasAgregados.add(aliasRegistrados[recorreAlias]);

            }
        }
        
        Collections.sort(aliasAgregados, new ComparadorAliasTTFFD());
        AliasTTFFDVO[] aliasAutorizados = (AliasTTFFDVO[]) aliasAgregados.toArray(new AliasTTFFDVO[aliasAgregados
            .size()]);
              
        return aliasAutorizados;
    }



    /**
     * 
     * M�todo validarTransferencia.
     * <p>
     * Registro de versiones:<ul>
     * 
     * <li>1.0 23/06/2014 Juan Pablo Vega (TINet): Versi�n inicial.
     * </ul><p>
     * @param transferencia transferencia.
     * @return resultado validacion.
     * @since 4.3
     */
    public ResultadoValidacionTTFFTO validarTransferencia(DatosTransferenciaTO transferencia) {
        if (obtenerLogger().isInfoEnabled()) {
            obtenerLogger().info("[validarTransferencia] [BCI_INI] Inicio " +transferencia.getCliente().getRut());
            obtenerLogger().info("[validarTransferencia][transferencia:[" + transferencia + "]");
        }
        EvaluadorTTFF evaluadorTTFF = new EvaluadorTTFF();
        ResultadoValidacionTTFFTO resultadoValidacion = evaluadorTTFF.validar(transferencia);
        if (obtenerLogger().isInfoEnabled()) {
            obtenerLogger().info(
                "[validarTransferencia] [" + transferencia.getCliente().getRut()
                    + "][BCI_FINOK] resultadoValidacion [" + resultadoValidacion + "]");
        }
        return resultadoValidacion;
    }

    /**
     * 
     * M�todo transferir.
     * <p>
     * Registro de versiones:<ul>
     * 
     * <li>1.0 23/06/2014 Alvaro Fuentes O. (TINet): Versi�n inicial.</li>
     * <li>1.1 10/07/2015 Luis Lopez(SEnTRA)-Claudia Lopez(Ing. Soft BCI): Se agrega al atributo para crear el alias.</li>
	 * <li>1.2 26/04/2017 Mauricio Am�stica G. (Kubos) - Marcelo Arias (ing. Soft. BCI): Se agrega validaci�n de 
	 * cuentas Mach </li>
     * </ul><p>
     * @param datosTransferencia datos transferencia.
     * @return detalle transferencia.
     * @throws TransferenciaDeFondosException en caso de error.
     * @since 4.3
     */
    public DetalleTransferencia transferir(DatosTransferenciaTO datosTransferencia)
        throws TransferenciaDeFondosException {
        
        if(obtenerLogger().isEnabledFor(Level.INFO)) {
            obtenerLogger().info("[transferir] [BCI_INI] Inicio");
        }
        if (obtenerLogger().isEnabledFor(Level.DEBUG)) {
            obtenerLogger().debug("[transferir][" + datosTransferencia.getCliente().getRut() + "] datosTransferencia:["
                + datosTransferencia + "]");
        }
        boolean aliasNuevo = false;
        DetalleTransferencia detalleTransferencia = null;
        ResultadoValidacionTTFFTO resultado = validarTransferencia(datosTransferencia);        
        boolean tieneErrores = resultado.isTieneErrores();
        
        if (obtenerLogger().isEnabledFor(Level.DEBUG)) {
            obtenerLogger().debug("[transferir][" + datosTransferencia.getCliente().getRut() + "] resultado:[" + resultado
                + "]");
        }
        
        if (!tieneErrores) {
            if (obtenerLogger().isEnabledFor(Level.DEBUG)) {
                obtenerLogger().debug("[transferir][" + datosTransferencia.getCliente().getRut() + "] Validaci�n sin errores");
                obtenerLogger().debug("[transferir][" + datosTransferencia.getCliente().getRut() + "] datosTransferencia.getAlias().getCodigo(): " + datosTransferencia.getAlias().getCodigo());
                obtenerLogger().debug("[transferir][" + datosTransferencia.getCliente().getRut() + "] datosTransferencia.getAlias().getAlias(): " + datosTransferencia.getAlias().getAlias());
            }
            
            if (datosTransferencia.getAlias().getCodigo() < COD_ALIAS_NO_NUEVO
                && datosTransferencia.getAlias().getAlias() != null) {
                aliasNuevo = true;
                registrarNuevoAlias(datosTransferencia,true);    
            }
            
            try {
                if (datosTransferencia.isOkDuplicada()) {
                    if (obtenerLogger().isEnabledFor(Level.DEBUG)) {
                        obtenerLogger().debug("[transferir][" + datosTransferencia.getCliente().getRut() + "] Realizando grabarRegistroTransfDup");
                    }

                    grabarRegistroTransfDup(copiarTransferenciaDuplicada(datosTransferencia));
                }
                if (obtenerLogger().isEnabledFor(Level.DEBUG)) {
                    obtenerLogger().debug(
                        "[transferir][" + datosTransferencia.getCliente().getRut()
                            + "] Realizando validarEstadoNuevoAlias");
                }
            }
            catch (Exception e) {
                if (obtenerLogger().isEnabledFor(Level.ERROR)) {
                    obtenerLogger().error("[transferir][" + datosTransferencia.getCliente().getRut() + "][BCI_FINEX] [Exception] "
                        + "Error al ejecutar la transferencia a otro banco");
                }
                throw new TransferenciaDeFondosException("TRF001");
            }
            EvaluadorTTFF evaluadorTTFF = new EvaluadorTTFF();
            boolean esDestinoBCI = evaluadorTTFF.esCuentaDestinoBci(datosTransferencia);

            if (esCuentaCorrientePrima(datosTransferencia.getTipoCuentaOrigen())
					&& esCuentaCorrientePrima(datosTransferencia.getTipoCuentaDestino()) && esDestinoBCI
					&& !evaluadorTTFF.isMach(datosTransferencia.getAlias().getNumeroCuenta())) {
                try {

                    String tipoTransferencia = TablaValores.getValor(TABLA_PARAMETRO_TRANSFERENCIA_TERCEROS,
                        "tipoTransferencia", "mismoBanco");
                    
                    if (obtenerLogger().isEnabledFor(Level.DEBUG)) {
                        obtenerLogger().debug("[transferir][" + datosTransferencia.getCliente().getRut() + "] Realizando TEF a CCT/CPR BCI");
                        obtenerLogger().debug("[transferir][" + datosTransferencia.getCliente().getRut() + "] tipoTransferencia:[" + tipoTransferencia + "]");
                    }

                    String nemCargo;
                    String nemAbono;
                    if (evaluadorTTFF.esCuentaDestinoTercerosBci(datosTransferencia)) {

                        nemCargo = TablaValores.getValor(TABLA_PARAMETRO_NEMONICO, "CargoTrfTercerosBci", "Desc");
                        nemAbono = TablaValores.getValor(TABLA_PARAMETRO_NEMONICO, "AbonoTrfTercerosBci", "Desc");
                        obtenerLogger().info("[transferir] Realizando ttff a cta cte BCI (terceros BCI)");
                    }
                    else {

                        nemCargo = TablaValores.getValor(TABLA_PARAMETRO_NEMONICO, "CargoTrfEntreCuenta", "Desc");
                        nemAbono = TablaValores.getValor(TABLA_PARAMETRO_NEMONICO, "AbonoTrfEntreCuenta", "Desc");
                        obtenerLogger().info("[transferir] Realizando ttff a cta cte BCI (mismas cuentas)");
                    }

                    if (obtenerLogger().isEnabledFor(Level.DEBUG)) {
                        obtenerLogger().debug("[transferir][" + datosTransferencia.getCliente().getRut() + "] nemCargo:[" + nemCargo + "]");
                        obtenerLogger().debug("[transferir][" + datosTransferencia.getCliente().getRut() + "] nemAbono:[" + nemAbono + "]");
                    }                    

                    DetalleTransferencia detalle = new DetalleTransferencia();
                    detalle = convierteDatosTransferenciaADetalleTransferencia(datosTransferencia);
                    detalle.setTipoTransferencia(tipoTransferencia);
                    detalle.setNemonicoCargo(nemCargo);
                    detalle.setNemonicoAbono(nemAbono);
                    if (obtenerLogger().isEnabledFor(Level.DEBUG)) {
                        obtenerLogger().debug("[transferir][" + datosTransferencia.getCliente().getRut()  + "] detalle:[" + detalle + "]");
                    }                      
                    detalleTransferencia = transfiereCtaCteHaciaCtaCte(detalle);
                }
                catch (TransferenciaException e) {
                    if(obtenerLogger().isEnabledFor(Level.ERROR)) {
                        obtenerLogger().error("[transferir][" + datosTransferencia.getCliente().getRut() + "][TransferenciaException] Error al llamar a " + "transfiereCtaCteHaciaCtaCte: " + e.getMessage(), e);
                        obtenerLogger().error("[transferir][" + datosTransferencia.getCliente().getRut() + "][TransferenciaException] aliasNuevo " + aliasNuevo);
                            }

                    if (aliasNuevo) {
                        borrarNuevoAlias(datosTransferencia);
                        }
                    
                    if (e.getCodigo() != null && !e.getCodigo().trim().equals("")) {
                            if (obtenerLogger().isEnabledFor(Level.ERROR)) {
                            obtenerLogger().error("[transferir][" + datosTransferencia.getCliente().getRut() + "][TransferenciaException] " + "e.getInfoAdic()[" + e.getInfoAdic() + "]");
                            obtenerLogger().error("[transferir][" + datosTransferencia.getCliente().getRut() + "][BCI_FINEX] [TransferenciaException] " + "Se lanza TransferenciaDeFondosException con InfoAdic");
                        }
                        throw new TransferenciaDeFondosException(e.getCodigo(), e.getInfoAdic());
                    }
                    else {
                        if (obtenerLogger().isEnabledFor(Level.ERROR)) {
                            obtenerLogger().error("[transferir][" + datosTransferencia.getCliente().getRut() + "][BCI_FINEX] [TransferenciaException] " + "Se lanza TransferenciaDeFondosException con TRF001");
                        }
                        throw new TransferenciaDeFondosException("TRF001");
                    }
                }
            }
            else if (esCuentaCorrientePrima(datosTransferencia.getTipoCuentaOrigen())
					&& esCuentaDeAhorro(datosTransferencia.getTipoCuentaDestino()) && esDestinoBCI
					&& !evaluadorTTFF.isMach(datosTransferencia.getAlias().getNumeroCuenta())) {
                try {
                    String tipoTransferencia = TablaValores.getValor(TABLA_PARAMETRO_TRANSFERENCIA_TERCEROS,
                        "tipoTransferencia", "mismoBanco");
                    
                    if (obtenerLogger().isEnabledFor(Level.DEBUG)) {
                        obtenerLogger().debug("[transferir][" + datosTransferencia.getCliente().getRut() + "] Realizando TEF a AHO BCI");
                        obtenerLogger().debug("[transferir][" + datosTransferencia.getCliente().getRut() + "] tipoTransferencia:[" + tipoTransferencia + "]");
                    }                    

                    DetalleTransferencia detalle = new DetalleTransferencia();
                    detalle = convierteDatosTransferenciaADetalleTransferencia(datosTransferencia);
                    detalle.setTipoTransferencia(tipoTransferencia);
                    
                    if (obtenerLogger().isEnabledFor(Level.DEBUG)) {
                        obtenerLogger().debug("[transferir][" + datosTransferencia.getCliente().getRut() + "] detalle:[" + detalle + "]");
                    }                    
                    detalleTransferencia = transfiereCtaCteHaciaAhorro(detalle);
                }
                catch (TransferenciaException e) {
                    if(obtenerLogger().isEnabledFor(Level.ERROR)) {
                        obtenerLogger().error("[transferir][" + datosTransferencia.getCliente().getRut() + "][TransferenciaException] Error al llamar a " + "transfiereCtaCteHaciaAhorro: " + e.getMessage(), e);
                        obtenerLogger().error("[transferir][" + datosTransferencia.getCliente().getRut() + "][TransferenciaException] aliasNuevo " + aliasNuevo);
                            }                            

                    if (aliasNuevo) {
                        borrarNuevoAlias(datosTransferencia);
                        }
                                        
                    if (e.getCodigo() != null && !e.getCodigo().trim().equals("")) {
                            if (obtenerLogger().isEnabledFor(Level.ERROR)) {
                            obtenerLogger().error("[transferir][" + datosTransferencia.getCliente().getRut() + "][TransferenciaException] " + "e.getInfoAdic()[" + e.getInfoAdic() + "]");
                            obtenerLogger().error("[transferir][" + datosTransferencia.getCliente().getRut() + "][BCI_FINEX] [TransferenciaException] " + "Se lanza TransferenciaDeFondosException con InfoAdic");
                        }
                        throw new TransferenciaDeFondosException(e.getCodigo(), e.getInfoAdic());
                    }
                    else {
                        if (obtenerLogger().isEnabledFor(Level.ERROR)) {
                            obtenerLogger().error("[transferir][" + datosTransferencia.getCliente().getRut() + "][BCI_FINEX] [TransferenciaException] " + "Se lanza TransferenciaDeFondosException con TRF001");
                        }                        
                        throw new TransferenciaDeFondosException("TRF001");
                    }
                }
            }
            else {
                try {
                    String tipoTransferencia = TablaValores.getValor(TABLA_PARAMETRO_TRANSFERENCIA_TERCEROS,
                        "tipoTransferencia", "linea");
                    
                    if (obtenerLogger().isEnabledFor(Level.DEBUG)) {
                        obtenerLogger().debug("[transferir][" + datosTransferencia.getCliente().getRut() + "] Realizando TEF a Otros Bancos");
                        obtenerLogger().debug("[transferir][" + datosTransferencia.getCliente().getRut() + "] tipoTransferencia:[" + tipoTransferencia + "]");
                    }

                    datosTransferencia.setTipoTransferencia(tipoTransferencia);
                    detalleTransferencia = transCtaCteOtroBancoLineaPer(datosTransferencia);
                }
                catch (TransferenciaException e) {
                    if(obtenerLogger().isEnabledFor(Level.ERROR)) {
                        obtenerLogger().error("[transferir][" + datosTransferencia.getCliente().getRut() + "][TransferenciaException] Error al llamar a " + "transCtaCteOtroBancoLineaPer: " + e.getMessage(), e);
                        obtenerLogger().error("[transferir][" + datosTransferencia.getCliente().getRut() + "][TransferenciaException] aliasNuevo " + aliasNuevo);
                            }                            
                    
                    if (aliasNuevo) {
                        borrarNuevoAlias(datosTransferencia);
                        }
                    
                    if (e.getCodigo() != null && !e.getCodigo().trim().equals("")) {
                            if (obtenerLogger().isEnabledFor(Level.ERROR)) {
                            obtenerLogger().error("[transferir][" + datosTransferencia.getCliente().getRut() + "][TransferenciaException] " + "e.getInfoAdic()[" + e.getInfoAdic() + "]");
                            obtenerLogger().error("[transferir][" + datosTransferencia.getCliente().getRut() + "][BCI_FINEX] [TransferenciaException] " + "Se lanza TransferenciaDeFondosException con InfoAdic");
                        }
                        throw new TransferenciaDeFondosException(e.getCodigo(), e.getInfoAdic());
                    }
                    else {
                        if (obtenerLogger().isEnabledFor(Level.ERROR)) {
                            obtenerLogger().error("[transferir][" + datosTransferencia.getCliente().getRut() + "][BCI_FINEX] [TransferenciaException] " + "Se lanza TransferenciaDeFondosException con TRF001");
                        }                        
                        throw new TransferenciaDeFondosException("TRF001");
                    }
                }
                catch (TuxedoException e) {
                    if(obtenerLogger().isEnabledFor(Level.ERROR)) {
                        obtenerLogger().error("[transferir][" + datosTransferencia.getCliente().getRut() + "][TuxedoException] Error al llamar a " + "transCtaCteOtroBancoLineaPer: " + e.getMessage(), e);
                        obtenerLogger().error("[transferir][" + datosTransferencia.getCliente().getRut() + "][TuxedoException] aliasNuevo " + aliasNuevo);
                    }
                    
                    if (aliasNuevo) {
                        borrarNuevoAlias(datosTransferencia);
                    }                       
                    throw new TransferenciaDeFondosException("TRF001");
                }
                catch (CCAException e) {
                    if(obtenerLogger().isEnabledFor(Level.ERROR)) {
                        obtenerLogger().error("[transferir][" + datosTransferencia.getCliente().getRut() + "][CCAException] Error al llamar a " + "transCtaCteOtroBancoLineaPer: " + e.getMessage(), e);
                        obtenerLogger().error("[transferir][" + datosTransferencia.getCliente().getRut() + "][CCAException] aliasNuevo " + aliasNuevo);
                    }
                    if (aliasNuevo) {
                        borrarNuevoAlias(datosTransferencia);
                    }
                    if (e.getCodigo().equals("TRF025")) {
                            if (obtenerLogger().isEnabledFor(Level.ERROR)) {
                                obtenerLogger().error(
                                    "[transferir][" + datosTransferencia.getCliente().getRut()
                                    + "][BCI_FINEX][CCAException] " + "Se captura y lanza TRF025");
                            }                            
                        throw new TransferenciaDeFondosException("TRF025");

                        }
                    else if (e.getCodigo() != null && !e.getCodigo().trim().equals("")) {
                            if (obtenerLogger().isEnabledFor(Level.ERROR)) {
                            obtenerLogger().error("[transferir][" + datosTransferencia.getCliente().getRut() + "][CCAException] " + "e.getInfoAdic()[" + e.getInfoAdic() + "]");
                            obtenerLogger().error("[transferir][" + datosTransferencia.getCliente().getRut() + "][BCI_FINEX][CCAException] " + "Se lanza TransferenciaDeFondosException con InfoAdic");
                        }
                        throw new TransferenciaDeFondosException(e.getCodigo(), e.getInfoAdic());
                    }
                    else {
                        if (obtenerLogger().isEnabledFor(Level.ERROR)) {
                            obtenerLogger().error("[transferir][" + datosTransferencia.getCliente().getRut() + "][BCI_FINEX][CCAException] " + "Se lanza TransferenciaDeFondosException con TRF001");
                        }
                        throw new TransferenciaDeFondosException("TRF001");
                    }
                }
            }
        }
        else {
            if (obtenerLogger().isEnabledFor(Level.ERROR)) {
                obtenerLogger().error("[transferir][" + datosTransferencia.getCliente().getRut() + "][CCAException] Tiene errores de validacion. " + resultado);
            }
            throw new TransferenciaDeFondosException("TRF001", resultado);
        }

        char estadoAliasNuevo = TablaValores.getValor(TABLA_PARAMETRO_TRANSFERENCIA_TERCEROS, "estadoAlias", "nuevo").charAt(0);
        double montoMaximoAlias = (TablaValores.getValor(TABLA_PARAMETRO_TRANSFERENCIA_TERCEROS, "montoMaximoAlias", "valor") == null) ? 0 : new Double(TablaValores.getValor(TABLA_PARAMETRO_TRANSFERENCIA_TERCEROS, "montoMaximoAlias", "valor"))
                .doubleValue();
        EvaluadorTTFF evaluador = new EvaluadorTTFF();

        if (obtenerLogger().isEnabledFor(Level.DEBUG)) {
            obtenerLogger().debug("[transferir][" + datosTransferencia.getCliente().getRut() + "] estadoAliasNuevo: " + estadoAliasNuevo);
            obtenerLogger().debug("[transferir][" + datosTransferencia.getCliente().getRut() + "] montoMaximoAlias: " + montoMaximoAlias);
            obtenerLogger().debug("[transferir][" + datosTransferencia.getCliente().getRut() + "] datosTransferencia.getAlias().getFechaIngreso(): " + datosTransferencia.getAlias().getFechaIngreso());
            obtenerLogger().debug("[transferir][" + datosTransferencia.getCliente().getRut() + "] datosTransferencia.getAlias().getBancoCodigo(): " + datosTransferencia.getAlias().getBancoCodigo());
            obtenerLogger().debug("[transferir][" + datosTransferencia.getCliente().getRut() + "] datosTransferencia.getAlias().getEstado(): " + datosTransferencia.getAlias().getEstado());
            obtenerLogger().debug("[transferir][" + datosTransferencia.getCliente().getRut() + "] datosTransferencia.getMonto(): " + datosTransferencia.getMonto());
        }
        
        if (evaluador.esFechaIngresoRestringida(datosTransferencia.getAlias().getFechaIngreso())
            && evaluador.esBancoRestringido(datosTransferencia.getAlias().getBancoCodigo())
            && datosTransferencia.getAlias().getEstado() == estadoAliasNuevo
            && datosTransferencia.getMonto() <= montoMaximoAlias) {

            modificarNuevoAlias(datosTransferencia);
        }
        
        if (obtenerLogger().isEnabledFor(Level.INFO)) {
            obtenerLogger().info("[transferir][" + datosTransferencia.getCliente().getRut() + "][BCI_FINOK] detalleTransferencia: " + detalleTransferencia);
        }       
        return detalleTransferencia;
    }

    /**
     * 
     * M�todo validarEstadoNuevoAlias.
     * <p>
     * Registro de versiones:<ul>
     * 
     * <li>1.0 24/06/2014 Alvaro Fuentes O. (TINet): Versi�n inicial.
     * </ul><p>
     * @param datosTransferencia datos de la transferencia.
     * @param fechaActual fecha actual.
     * @throws BusinessException en caso de error.
     * @throws Exception en caso de error.
     * @throws com.schema.util.GeneralException en caso de error.
     * @since 4.3
     */
    private void validarEstadoNuevoAlias(DatosTransferenciaTO datosTransferencia, Calendar fechaActual)
        throws BusinessException, Exception, com.schema.util.GeneralException {

        if(obtenerLogger().isInfoEnabled()) {
            obtenerLogger().info("[validarEstadoNuevoAlias] [BCI_INI] Inicio");
        }
        
        if (obtenerLogger().isInfoEnabled()) {
            obtenerLogger().info("[validarEstadoNuevoAlias][" + datosTransferencia.getCliente().getRut()
                + "] datosTransferencia:[" + datosTransferencia + "]");
            obtenerLogger().info("[validarEstadoNuevoAlias][" + datosTransferencia.getCliente().getRut()
                + "] datosTransferencia.getAlias():[" + datosTransferencia.getAlias() + "]");
        }
        
        if (datosTransferencia.getAlias().getEstado() == 'N') {
            
            Date fechaAlias = new Date(datosTransferencia.getAlias().getFechaIngreso().getTime());
            long diferenciaHoras = fechaActual.getTime().getTime() - fechaAlias.getTime();
            diferenciaHoras = (((diferenciaHoras / MILISEGUNDOS) / SESENTA) / SESENTA);
            
            if (obtenerLogger().isInfoEnabled()) {
                obtenerLogger().info("[validarEstadoNuevoAlias][" + datosTransferencia.getCliente().getRut()
                    + "] diferenciaHoras:[" + diferenciaHoras + "]");
            }              
            
            if ((diferenciaHoras < HORAS_POR_DIA)
                && obtenerBancosConMontosRestringidos(datosTransferencia.getCodBancoDestino())
                && validarMontoMaximo(datosTransferencia.getMonto())) {
                
                if (obtenerLogger().isDebugEnabled()) {
                    obtenerLogger().debug(
                        "[transferir][" + datosTransferencia.getCliente().getRut()
                            + "] Modificando estado del Alias");
                }              
                modificarEstadoAliasTTFF(datosTransferencia.getAlias().getCodigo(), null, 'A');
            }
        }
        
        if (obtenerLogger().isInfoEnabled()) {
            obtenerLogger().info("[validarEstadoNuevoAlias][BCI_FINOK]");
        }       
    }
    
    /**
     * 
     * M�todo validarMontoMaximo.
     * <p>
     * Registro de versiones:<ul>
     * 
     * <li>1.0 23/06/2014 Juan Pablo Vega (TINet): Versi�n inicial.
     * </ul><p>
     * @param montoTransferencia monto transferencia.
     * @return true or false.
     * @since 4.3
     */
    private boolean validarMontoMaximo(double montoTransferencia) {
        
        if(obtenerLogger().isInfoEnabled()) {
            obtenerLogger().info("[validarMontoMaximo] [BCI_INI] Inicio");
        }        
        String montoTabla = TablaValores.getValor(TABLA_PARAMETRO_TRANSFERENCIA_TERCEROS, "montoMaximoAlias",
            "valor");
        
        if (obtenerLogger().isInfoEnabled()) {
            obtenerLogger().info("[validarMontoMaximo] montoTabla:[" + montoTabla + "]");
        }
        if (!StringUtil.esVacio(montoTabla) && !StringUtil.esAlfanumerico(montoTabla)) {
            double montoValor = Double.parseDouble(montoTabla);
            if (montoTransferencia <= montoValor) {
                if (obtenerLogger().isInfoEnabled()) {
                    obtenerLogger().info("[validarMontoMaximo] [BCI_FINOK] [true]");
                }                
                return true;
            }
        }
        
        if (obtenerLogger().isInfoEnabled()) {
            obtenerLogger().info("[validarMontoMaximo] [BCI_FINOK] [false]");
        }
        return false;
    }

    /**
     * 
     * M�todo obtenerBancosConMontosRestringidos.
     * <p>
     * Registro de versiones:<ul>
     * 
     * <li>1.0 23/06/2014 Juan Pablo Vega (TINet): Versi�n inicial.
     * </ul><p>
     * @param codigoBancoDestino codigo banco destino.
     * @return true or false.
     * @since 4.3
     */
    private boolean obtenerBancosConMontosRestringidos(String codigoBancoDestino) {
        
        if(obtenerLogger().isInfoEnabled()) {
            obtenerLogger().info("[obtenerBancosConMontosRestringidos] [BCI_INI] Inicio");
        }        
        String[] bancosMontoRestringido = StringUtil
            .divide(
                TablaValores.getValor(TABLA_PARAMETRO_TRANSFERENCIA_TERCEROS, "bancoMontoRestringido", "codigos"),
                ",");
        if (bancosMontoRestringido != null) {
            
            if (obtenerLogger().isInfoEnabled()) {
                obtenerLogger().info("[obtenerBancosConMontosRestringidos] Cant. bancos con monto restringidos :["
                    + bancosMontoRestringido.length + "]");
            }            
            for (int i = 0; i < bancosMontoRestringido.length; i++) {
                if (bancosMontoRestringido[i].equals(codigoBancoDestino)) {
                    if (obtenerLogger().isInfoEnabled()) {
                        obtenerLogger().info("[obtenerBancosConMontosRestringidos] [BCI_FINOK] [true]");
                    }
                    return true;
                }
            }
        }
        
        if (obtenerLogger().isInfoEnabled()) {
            obtenerLogger().info("[obtenerBancosConMontosRestringidos] [BCI_FINOK] [false]");
        }      
        return false;
    }

    /**
     * 
     * M�todo copiarTransferenciaDuplicada.
     * <p>
     * Registro de versiones:<ul>
     * 
     * <li>1.0 23/06/2014 Alvaro Fuentes O. (TINet): Versi�n inicial.</li>
     * <li>1.1 04/11/2015 Luis Lopez Alamos (SEnTRA) - Claudia Lopez Pavlicevic (ing. Soft. BCI): Se elimina concatenacion de digito verificador para atributo de metodo rutCliente.</li>
     * </ul><p>
     * @param datos datos de la transferencia a duplicar.
     * @return transferencia duplicada.
     * @throws ParseException error de parseo 
     * @since 4.3
     */
    private TransferenciaDuplicadaVO copiarTransferenciaDuplicada(
        DatosTransferenciaTO datos) throws ParseException {
        
        if (obtenerLogger().isInfoEnabled()) {
            obtenerLogger().info("[copiarTransferenciaDuplicada] [BCI_INI] Inicio");
            obtenerLogger().info("[copiarTransferenciaDuplicada][" + datos.getCliente().getRut() + "] datos: [" + datos
                + "]");
        }
        TransferenciasSalidaVO datosDuplicacion = datos.getDatosDuplicacion();
        if (obtenerLogger().isDebugEnabled()) {
            obtenerLogger().debug("[copiarTransferenciaDuplicada][" + datos.getCliente().getRut() + "] datos entrada:" 
                + StringUtil.contenidoDe(datosDuplicacion));
        }
        
        TransferenciaDuplicadaVO transferenciaDuplicada = new TransferenciaDuplicadaVO();
        transferenciaDuplicada.setCanal(Integer.parseInt(datosDuplicacion.getCanal().trim()));
        transferenciaDuplicada.setCodigoBancoDestino(datosDuplicacion.getCodBancoDestino());
        transferenciaDuplicada.setCuentaDestino(datosDuplicacion.getCtaDestino().trim());
        transferenciaDuplicada.setCuentaOrigen(datos.getCtaOrigen().trim());
        String strFechaAnterior = datosDuplicacion.getFechaCreacion().trim();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date fechaAnterior = format.parse(strFechaAnterior);
        transferenciaDuplicada.setFechaActual(new Date());
        transferenciaDuplicada.setFechaAnterior(fechaAnterior);
        transferenciaDuplicada.setMontoTransferencia(datos.getMonto());
        transferenciaDuplicada.setRutDestino(datosDuplicacion.getRutDestino().trim());
        String rutCliente = String.valueOf(datos.getCliente().getRut());
        transferenciaDuplicada.setRutOrigen(rutCliente);
        transferenciaDuplicada.setRutAutorizador(rutCliente);
        long traceNumber = datosDuplicacion.getNumOperacion();
        transferenciaDuplicada.setTraceNumber(String.valueOf(traceNumber));
                
        if (obtenerLogger().isInfoEnabled()) {
            obtenerLogger().info("[copiarTransferenciaDuplicada] [" + datos.getCliente().getRut()
                + "[BCI_FINOK] transferenciaDuplicada [" + StringUtil.contenidoDe(transferenciaDuplicada) + "]");
        }       
        return transferenciaDuplicada;
    }
    
    
    /**
     * 
     * M�todo programar.
     * <p>
     * Registro de versiones:<ul>
     * 
     * <li>1.0 23/06/2014 Juan Pablo Vega (TINet): Versi�n inicial.
     * <li>1.1 10/07/2015 Luis Lopez(SEnTRA)-Claudia Lopez(Ing. Soft BCI): Se agrega al atributo para crear el alias y se modifica metodo.</li>
     * </ul><p>
     * @param transferencia datos de la transferencia.
     * @return ProgramacionPK.
     * @throws TransferenciaDeFondosException en caso de error.
     * @since 4.3
     */
    public ProgramacionPK programar(DatosTransferenciaTO transferencia) throws TransferenciaDeFondosException {
        
        if (obtenerLogger().isEnabledFor(Level.INFO)) {
            obtenerLogger().info("[programar] [BCI_INI] Inicio");
            obtenerLogger().info("[programar][" + transferencia.getCliente().getRut() + "] transferencia: ["
                + transferencia + "]");
        }
        
        ProgramacionPK result = null;
        EvaluadorTTFF evaluadorTTFF = new EvaluadorTTFF();
        if (obtenerLogger().isEnabledFor(Level.DEBUG)) {
            obtenerLogger().debug("[programar] [" + transferencia.getCliente().getRut()
                + "] Antes de invocar al EvaluadorTTFF...");
        }
        ResultadoValidacionTTFFTO resultado = evaluadorTTFF.validar(transferencia);
        if (resultado.isTieneErrores()) {
            if (obtenerLogger().isEnabledFor(Level.ERROR)) {
                obtenerLogger().error("[programar] [" + transferencia.getCliente().getRut() + "] [BCI_FINEX]  "
                    + "Evaluador respondi� con errores de validaci�n.");
            }           
            throw new TransferenciaDeFondosException("TRF001", resultado);
        }
        else {
            try {
                if (obtenerLogger().isEnabledFor(Level.DEBUG)) {
                    obtenerLogger().debug("[programar] [" + transferencia.getCliente().getRut()+"] Antes de agregar programaci�n...");
                }           
                result = obtenerTransferenciasDeFondos().agregarProgramacion(transferencia.getProgramacion());

                if (obtenerLogger().isEnabledFor(Level.DEBUG)) {
                    obtenerLogger().debug("[programar] [" + transferencia.getCliente().getRut() + "] transferencia.getAlias().getCodigo(): " + transferencia.getAlias().getCodigo());
                    obtenerLogger().debug("[programar] [" + transferencia.getCliente().getRut() + "] transferencia.getAlias().getAlias(): " + transferencia.getAlias().getAlias());
            }
                
                if (transferencia.getAlias().getCodigo() < COD_ALIAS_NO_NUEVO
                    && transferencia.getAlias().getAlias() != null) {
                    registrarNuevoAlias(transferencia,false);
                }
            }
            catch (Exception e) {
                if (obtenerLogger().isEnabledFor(Level.ERROR)) {
                    obtenerLogger().error("[programar][" + transferencia.getCliente().getRut() + "][BCI_FINEX][Exception] Error al agregar programaci�n en el ejb de ProgramacionServices");
                }
                throw new TransferenciaDeFondosException("ERROR-RemoteException");
            }
        }
        
        if (obtenerLogger().isEnabledFor(Level.INFO)) {
            obtenerLogger().info("[programar] [" + transferencia.getCliente().getRut() + "[BCI_FINOK] result [" + result + "]");
        }
        return result;
    }

    /**
     * 
     * M�todo obtenerTransferenciasDeFondos.
     * <p>
     * Registro de versiones:<ul>
     * 
     * <li>1.0 23/06/2014 Juan Pablo Vega (TINet) - Desconocido (Ing. Soft. BCI): Versi�n inicial.
     * <li>1.1 26/12/2016 Susana Soto Macher (SEnTRA)-Juan Jos� Buend�a(Ing. Soft. BCI): Se reemplaza asignaci�n en duro 
     * por obtenci�n desde tabla de par�metros para el nombre JNDI del ejb ProgramacionServices.</li>
     * </ul><p>
     * @return instancia del ejb ProgramacionServices.
     * @throws TransferenciaDeFondosException en caso de error.
     * @since 4.3
     */
    private ProgramacionServices obtenerTransferenciasDeFondos() throws TransferenciaDeFondosException {
        
        if(obtenerLogger().isInfoEnabled()) {
            obtenerLogger().info("[obtenerTransferenciasDeFondos] [BCI_INI] Inicio");
        }        
        ProgramacionServices programacionServices = null;
        ProgramacionServicesHome programacionServicesHome = null;
        String jndi = TablaValores.getValor(TTFF_CONFIG, "jndiProgramacionServices", "valor");

        EnhancedServiceLocator locator;
        try {
            if (obtenerLogger().isDebugEnabled()) {
                obtenerLogger().debug("[obtenerTransferenciasDeFondos] Antes de instanciar ejb ProgramacionServices");
            }
            locator = EnhancedServiceLocator.getInstance();
            programacionServicesHome = (ProgramacionServicesHome) locator.getGenericService(jndi,
                ProgramacionServicesHome.class);
            programacionServices = programacionServicesHome.create();
        }
        catch (EnhancedServiceLocatorException e) {
            if (obtenerLogger().isEnabledFor(Level.ERROR)) {
                obtenerLogger().error("[obtenerTransferenciasDeFondos] [BCI_FINEX] [EnhancedServiceLocatorException] "
                    + "Error al instanciar el ejb de ProgramacionServices");
            }
            throw new TransferenciaDeFondosException("TRF001");
        }
        catch (RemoteException e) {
            if (obtenerLogger().isEnabledFor(Level.ERROR)) {
                obtenerLogger().error("[obtenerTransferenciasDeFondos] [BCI_FINEX] [RemoteException] "
                    + "Error al instanciar el ejb de ProgramacionServices");
            }
            throw new TransferenciaDeFondosException("TRF001");
        }
        catch (CreateException e) {
            if (obtenerLogger().isEnabledFor(Level.ERROR)) {
                obtenerLogger().error("[obtenerTransferenciasDeFondos] [BCI_FINEX] [CreateException] "
                    + "Error al instanciar el ejb de ProgramacionServices");
            }
            throw new TransferenciaDeFondosException("TRF001");
        }
        
        if (obtenerLogger().isInfoEnabled()) {
            obtenerLogger().info("[validarMontoTransferenciaLinea][BCI_FINOK] programacionServices: "
                + programacionServices);
        }
        return programacionServices;
    }
        
    /**
     * Devuelve si la cuenta es corriente o prima. <br>
    * Registro de Versiones:
    * <ul>
     * <li>1.0 23/06/2014 Alvaro Fuentes O. (TINet): Versi�n inicial.
    * </ul>
     * <br>
    * 
     * @param tipoCuenta tipo de cuenta.
     * @return boolean si la cuenta es corriente o prima.
	 * @since 4.3
    */       
    private boolean esCuentaCorrientePrima(String tipoCuenta) {
        if(obtenerLogger().isInfoEnabled()) {
            obtenerLogger().info("[esCuentaCorrientePrima] [BCI_INI] Inicio");
        }
        if (tipoCuenta.trim().equalsIgnoreCase("CCT") || tipoCuenta.trim().equalsIgnoreCase("CPR")){          
            if (obtenerLogger().isInfoEnabled()) {
                obtenerLogger().info("[esCuentaCorrientePrima] [" + tipoCuenta + "][BCI_FINOK] regla [true]");
            }
            return true;
        }
        if (obtenerLogger().isInfoEnabled()) {
            obtenerLogger().info("[esCuentaCorrientePrima] [" + tipoCuenta + "][BCI_FINOK] regla [false]");
        }         
        return false;
    }

    /**
     * Devuelve si la cuenta es ahorro o ahorro electronico?nico. <br>
     * Registro de Versiones:
     * <ul>
     * <li>1.0 23/06/2014 Alvaro Fuentes O. (TINet): Versi�n inicial.
     * </ul>
     * <br>
     * 
     * @param tipoCuenta tipo de cuenta.
     * @return boolean si la cuenta es ahorro o ahorro electronico.
	 * @since 4.3
     */
    private static boolean esCuentaDeAhorro(String tipoCuenta) {
        if(logger.isInfoEnabled()) {
            logger.info("[esCuentaDeAhorro] [BCI_INI] Inicio");
        }
        if (tipoCuenta.trim().equalsIgnoreCase("LDE") || tipoCuenta.trim().equalsIgnoreCase("AHO")){
            if (logger.isInfoEnabled()) {
                logger.info("[esCuentaDeAhorro] [" + tipoCuenta + "][BCI_FINOK] regla [true]");
            }
            return true;
        }
        if (logger.isInfoEnabled()) {
            logger.info("[esCuentaDeAhorro] [" + tipoCuenta + "][BCI_FINOK] regla [false]");
        }
        return false;
    }
    

    /**
     * Adapter que obtiene el TO que contiene los datos necesarios para realizar la evaluaci�n de riesgo de fraude
     * con Actimize. <br>
     * Registro de Versiones:
     * <ul>
     * <li>1.0 11/11/2014 Eduardo Mascayano (TINet): Versi�n inicial.
     * <li>1.1 10/01/2015 Luis L�pez Alamos (SEnTRA): Se agrega condici�n para cuando viene egift seleccionada.</li>
     * </ul>
     * <br>
     * 
     * @param datosTransferencia Objeto que contienen los datos para realizar la evaluaci�n de fraudes.
     * @return Objeto que contiene la respuesta de la transferencia de fondos.
     * @since 4.3
     */
    private DetalleTransferencia convierteDatosTransferenciaADetalleTransferencia(
        DatosTransferenciaTO datosTransferencia) {

        if (obtenerLogger().isInfoEnabled()) {
            obtenerLogger().info("[convierteDatosTransferenciaADetalleTransferencia] [BCI_INI] Inicio " + datosTransferencia.getCliente().getRut());
            obtenerLogger().info("[convierteDatosTransferenciaADetalleTransferencia][datosTransferencia:[" + datosTransferencia + "]");
        }
        DetalleTransferencia detalleTransferencia = new DetalleTransferencia();

        detalleTransferencia.setCtaOrigen(datosTransferencia.getCtaOrigen());
        detalleTransferencia.setTipoCuentaOrigen(datosTransferencia.getTipoCuentaOrigen());
        detalleTransferencia.setCtaDestino(datosTransferencia.getCtaDestino());
        detalleTransferencia.setTipoCuentaDestino(datosTransferencia.getTipoCuentaDestino());
        detalleTransferencia.setMontoTransferencia(datosTransferencia.getMonto());
        detalleTransferencia.setRutDestino(datosTransferencia.getRutDestino());
        detalleTransferencia.setDigDestino(datosTransferencia.getDigDestino());
        detalleTransferencia.setDatosDelCliente(datosTransferencia.getCliente());
        detalleTransferencia.setCanalID(datosTransferencia.getCanal());
        detalleTransferencia.setComentario(datosTransferencia.getComentario());
        detalleTransferencia.setNombreDestino(datosTransferencia.getNombreDestino());
        detalleTransferencia.setEMailCliente(datosTransferencia.getEmailCliente());
        detalleTransferencia.setEMailDestino(datosTransferencia.getEmailDestino());
        detalleTransferencia.setTipoAutenticacion(datosTransferencia.getTipoAutenticacion());
        detalleTransferencia.setCodigoAlias(datosTransferencia.getCodigoAlias());
        detalleTransferencia.setAlias(datosTransferencia.getAlias());
        detalleTransferencia.setCodigoBancoDestino(datosTransferencia.getCodBancoDestino());
        detalleTransferencia.setNombreBancoDestino(datosTransferencia.getNombreBancoDestino());
        detalleTransferencia.setEvaluadorDeFraudesTO(datosTransferencia.getEvaluadorDeFraudesTO());
        if (datosTransferencia.getEsEgift()){
            if (obtenerLogger().isInfoEnabled()) {
                obtenerLogger().info("[convierteDatosTransferenciaADetalleTransferencia] ["
                    +  " se pasa parametro regalo virtual]");
            }
            detalleTransferencia.setEsEgift(true);
            detalleTransferencia.setEgift(datosTransferencia.getEgift());
        }

        if (obtenerLogger().isInfoEnabled()) {
            obtenerLogger().info("[convierteDatosTransferenciaADetalleTransferencia] ["
                + datosTransferencia.getCliente().getRut() + "[BCI_FINOK] detalleTransferencia ["
                + detalleTransferencia + "]");
        }
        return detalleTransferencia;
    }
    
    /**
     * Registra el alias cuando es nuevo.
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 24/06/2014 Eduardo Mascayano (TINet): Versi�n inicial.</li>
     * <li>1.1 10/07/2015 Luis Lopez(SEnTRA)-Claudia Lopez(Ing. Soft. BCI): Se Modifica metodo para unificar la creaci�n o mantencion de los alias y
     *                              se elimina el metodo registrarAlias. </li>
     * </ul>
     * <p>
     * 
     * @param datosTransferencia datos de la transferencia.
     * @since 4.3
     */
    private void registrarNuevoAlias(DatosTransferenciaTO datosTransferencia, boolean Nuevo) {
        if (obtenerLogger().isEnabledFor(Level.INFO)) {
            obtenerLogger().info("[registrarNuevoAlias] [BCI_INI] Inicio");
        }
        if (obtenerLogger().isEnabledFor(Level.INFO)) {
            obtenerLogger().info(
                "[registrarNuevoAlias][" + datosTransferencia.getCliente().getRut() + "] datosTransferencia:["
                    + datosTransferencia + "]");
        }
        long codigoAlias = -1;
        try {
            if (obtenerLogger().isEnabledFor(Level.DEBUG)) {
                obtenerLogger().debug(
                    "[registrarNuevoAlias][" + datosTransferencia.getCliente().getRut()
                        + "] Guardamos el nuevo alias:[" + datosTransferencia.getAlias() + "]");
            }
            if(Nuevo){
            codigoAlias = registrarAliasTTFF(datosTransferencia.getAlias());
            datosTransferencia.getAlias().setCodigo(codigoAlias);
            }else {
                registrarAliasTTFF(datosTransferencia.getAlias());
            }
        }
        catch (Exception e) {
            if (obtenerLogger().isEnabledFor(Level.ERROR)) {
                obtenerLogger().error(
                    "[registrarNuevoAlias][Exception][" + datosTransferencia.getCliente().getRut()
                        + "]e"
                        + "Excepcion al registrar el alias ["
                            + datosTransferencia.getAlias().getCodigo() + "]" + e.getMessage(), e);
            }
        }
        if (obtenerLogger().isEnabledFor(Level.DEBUG)) {
            obtenerLogger().debug("[registrarNuevoAlias][" + datosTransferencia.getCliente().getRut() + "] Codigo Alias: " + datosTransferencia.getAlias().getCodigo());
            obtenerLogger().debug("[registrarNuevoAlias][" + datosTransferencia.getCliente().getRut() + "] Nombre alias: " + datosTransferencia.getAlias().getNombre());
            obtenerLogger().debug("[registrarNuevoAlias][" + datosTransferencia.getCliente().getRut() + "] Nombre alias reemplazado: " + StringUtil.escapaCaracteresHTML(datosTransferencia.getAlias().getNombre()));
            obtenerLogger().debug("[registrarNuevoAlias][" + datosTransferencia.getCliente().getRut() + "] Tipo de cuenta: " + datosTransferencia.getTipoCuentaDestino());
            }

        if(obtenerLogger().isEnabledFor(Level.INFO)){
            obtenerLogger().info("[registrarNuevoAlias][" + datosTransferencia.getCliente().getRut() + "][BCI_FINOK]");
        }
    }   
    
    
    /**
     * Borra el alias cuando es nuevo.
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 24/06/2014 Eduardo Mascayano (TINet): Versi�n inicial.
     * </ul>
     * <p>
     * 
     * @param datosTransferencia datos de la transferencia.
     * @since 4.3
     */
    private void borrarNuevoAlias(DatosTransferenciaTO datosTransferencia) {

        if (obtenerLogger().isInfoEnabled()) {
            obtenerLogger().info("[borrarNuevoAlias] [BCI_INI] Inicio");
            obtenerLogger().info(
                "[borrarNuevoAlias][" + datosTransferencia.getCliente().getRut()
                    + "] datosTransferencia.getAlias().getCodigo() [" + datosTransferencia.getAlias().getCodigo()
                    + "]");
        }
        try {
            borrarAliasTTFF(datosTransferencia.getAlias().getCodigo());
            if (obtenerLogger().isInfoEnabled()) {
                obtenerLogger().info(
                    "[borrarNuevoAlias][" + datosTransferencia.getCliente().getRut() + "] se borr� el alias ["
                        + datosTransferencia.getAlias().getCodigo() + "]");
            }
        }
        catch (Exception e) {
            if (obtenerLogger().isEnabledFor(Level.WARN)) {
                obtenerLogger().warn(
                    "[borrarNuevoAlias][" + datosTransferencia.getCliente().getRut()
                        + "] Exception al borrar el alias [" + datosTransferencia.getAlias().getCodigo() + "]"
                        + e.getMessage(), e);
            }
        }

    }
    
    /**
     * Modifica el alias cuando es nuevo.
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 24/06/2014 Eduardo Mascayano (TINet): Versi�n inicial.
     * </ul>
     * <p>
     * 
     * @param datosTransferencia datos de la transferencia.
     * @throws TransferenciaDeFondosException Al ocurrir un error al modificar el estado del alias.
     * @since 4.3
     */
    private void modificarNuevoAlias(DatosTransferenciaTO datosTransferencia)
        throws TransferenciaDeFondosException {

        if (obtenerLogger().isInfoEnabled()) {
            obtenerLogger().info("[modificarNuevoAlias] [BCI_INI] Inicio");
        }
        try {
            char estadoAliasAutorizado = TablaValores.getValor(TABLA_PARAMETRO_TRANSFERENCIA_TERCEROS,
                "estadoAlias", "autorizado").charAt(0);
            if (obtenerLogger().isInfoEnabled()) {
                obtenerLogger().info(
                    "[modificarNuevoAlias][" + datosTransferencia.getCliente().getRut()
                        + "] datosTransferencia.getAlias().getCodigo() ["
                        + datosTransferencia.getAlias().getCodigo() + "]");
                obtenerLogger().info(
                    "[modificarNuevoAlias][" + datosTransferencia.getCliente().getRut()
                        + "] estadoAliasAutorizado [" + estadoAliasAutorizado + "]");
            }
            modificarEstadoAliasTTFF(datosTransferencia.getAlias().getCodigo(), null, estadoAliasAutorizado);
        }
        catch (Exception e) {
            if (obtenerLogger().isEnabledFor(Level.ERROR)) {
                obtenerLogger().error(
                    "[modificarNuevoAlias][" + datosTransferencia.getCliente().getRut()
                        + "][Exception] Error al modificar estado alias" + e.getMessage(), e);
            }
            if (e.getMessage().indexOf(MENSAJE_ERROR_MODIF_ALIAS) != -1) {
                if (obtenerLogger().isEnabledFor(Level.WARN)) {
                    obtenerLogger().warn(
                        "[modificarNuevoAlias][" + datosTransferencia.getCliente().getRut()
                            + "] Cannot found registration");
                }

            }
            if (obtenerLogger().isEnabledFor(Level.ERROR)) {
                obtenerLogger().error(
                    "[modificarNuevoAlias][" + datosTransferencia.getCliente().getRut()
                        + "][BCI_FINEX][Exception] " + "Se lanza TransferenciaDeFondosException con TRF001");
            }
            throw new TransferenciaDeFondosException("TRF001");
        }
    }
    
    
    /**
     * Metodo que obliga a retornar un monto con formato de puntos decimales.
     * Se crea de esta forma, ya que, por problemas de Locale en ciertas plataformas no se genera el monto de tef como corresponde.
     * Crea un m�todo replaceAll para utilizarlo en versiones menores a java 4.
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 05/01/2016 Luis Lopez Alamos(SEnTRA)-Claudia Lopez(Ing Soft. BCI.): Versi�n inicial.
     * </ul>
     * <p>
     * 
     * @param double monto de la transferencia.
     * @since 4.6
     */
    
    private String formatearMontoMail(double monto){
        if (obtenerLogger().isInfoEnabled()) {
            obtenerLogger().info("[formatearMonto] [BCI_INI] Inicio con valor : " + monto);
        }
        DecimalFormat formateador = new DecimalFormat(FORMATO_DECIMAL);
        String montoFormateado = formateador.format(monto);
        String montoDevuelto = montoFormateado;
        
        int idx = montoFormateado.lastIndexOf(",");
                if (idx != -1){
            StringBuffer sb = new StringBuffer(montoFormateado);
            sb.replace(idx, idx+",".length(), ".");
            while ((idx=montoFormateado.lastIndexOf(",", --idx))!= -1){
              sb.replace(idx, idx+",".length(), ".");
                }
            montoDevuelto = sb.toString();
            }
        if (obtenerLogger().isInfoEnabled()) {
            obtenerLogger().info("[formatearMonto] [BCI_FINOK] Salgo del metodo con valor : " + montoDevuelto);
        }
        return montoDevuelto;
    }
    
    
    /**
 	 * M�todo que obtiene Consulta TTFF duplicadas.
 	 * <p>
 	 * Registro de versiones:
 	 * <ul>
 	 * <li>1.0 08/09/2016 Cristopher Freire C. (ImageMaker) - Pamela Inostroza(Ing. Soft. BCI): Version Inicial.</li>
 	 * </ul>
 	 * <p>
 	 *
 	 * @return TranferenciasFondosDuplicadas objeto con los datos de la consulta.
     * @param rutCliente rut de cliente.
     * @param fechadesde fecha desde TTFF.
     * @param fechahasta fecha hasta TTFF.
 	 * @throws Exception  por si hay un error.
 	 * @since 4.7
 	 */
    public TranferenciasFondosDuplicadas[] consultaTTFFDuplicada(long rutCliente,Date fechadesde,Date fechahasta)throws Exception{
    	if(getLogger().isInfoEnabled()) {
    		  getLogger().info("[consultaTTFFDuplicada][BCI_INI]rutCliente["+rutCliente+"],fechadesde["+fechadesde+"],fechahasta["+fechahasta+"]");
  		}
    	try{

	    	TransferenciasFondosDAO transferenciasFondosDAO = new TransferenciasFondosDAO();
	    	TranferenciasFondosDuplicadas[] transDup = transferenciasFondosDAO.consultaTTFFDuplicada(rutCliente, fechadesde, fechahasta);
	    	if(getLogger().isInfoEnabled()) {
	    	    getLogger().info("[consultaTTFFDuplicada][BCI_FINOK]"
	    	            + "resultadoTO[" + transDup + "]");
	    	}
	    	return transDup;
		}
    	catch (Exception e) {
			if(logger.isEnabledFor(Level.ERROR)) {
				logger.error("[consultaTTFFDuplicada][BCI_FINEX][CLASE_EXCEPTION]"
			            + "[Error en obtener tranferencias duplicadas] mensaje[" + e.getMessage() + "]", e);
			}
			throw new Exception("Error " + e.getMessage());
		}
    }
    
}

