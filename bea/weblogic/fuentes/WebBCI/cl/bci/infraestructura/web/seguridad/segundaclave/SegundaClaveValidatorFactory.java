package cl.bci.infraestructura.web.seguridad.segundaclave;

import javax.faces.validator.Validator;

import org.apache.log4j.Logger;

import cl.bci.infraestructura.web.seguridad.segundaclave.to.EstadoSegundaClaveTO;
import cl.bci.infraestructura.web.seguridad.segundaclave.to.SegundaClaveEnum;
import cl.bci.infraestructura.web.seguridad.segundaclave.validator.SegundaClaveETokenMultiBrowserValidator;
import cl.bci.infraestructura.web.seguridad.segundaclave.validator.SegundaClaveETokenValidator;
import cl.bci.infraestructura.web.seguridad.segundaclave.validator.SegundaClaveEntrustSoftTokenValidator;
import cl.bci.infraestructura.web.seguridad.segundaclave.validator.SegundaClaveEntrustTokenValidator;
import cl.bci.infraestructura.web.seguridad.segundaclave.validator.SegundaClaveInternetValidator;
import cl.bci.infraestructura.web.seguridad.segundaclave.validator.SegundaClaveMultipassValidator;
import cl.bci.infraestructura.web.seguridad.segundaclave.validator.SegundaClaveSafeSignerValidator;

/**
 * Factory de validators para el componente.
 * 
 * Registro de versiones:
 * <ul>
 * <li>1.0 (01/05/2013 Mauricio Palma. (Orand S.A.)): versi�n inicial.</li>
 * <li>1.1 03/09/2013 Pablo Romero C�ceres (SEnTRA): Se incorpora retorno de instancia de la estrategia EToken. 
 * <li> 1.2 12/12/2016 Darlyn Delgado P�rez (SEnTRA) - Daniel Araya Ureta (Ing. Soft. BCI): Se modifica m�todo {@link #crear(EstadoSegundaClaveTO)}.</li>
 * <li> 1.3 10/07/2017 Luis L�pez Alamos (SEnTRA) - Ricardo Carrasco C�ceres (Ing. Soft. BCI): Se modifica m�todo {@link #crear(EstadoSegundaClaveTO)}.</li>
 * </ul>
 * <p>
 * <b>Todos los derechos reservados por Banco de Cr�dito e Inversiones.</b>
 * </p>
 */
public class SegundaClaveValidatorFactory {

    /**
     * Logger de la clase.
     */
    private static Logger logger = (Logger) Logger.getLogger(SegundaClaveValidatorFactory.class);

    /**
     * Crea validator.
     * <p>
     * <b>Registro de versiones:</b>
     * <ul> 
     * <li> 1.0 xx/xx/xxxx Desconocido - Desconocido (Ing. Soft. BCI): Versi�n inicial.</li>
     * <li> 1.1 12/12/2016 Darlyn Delgado (SEnTRA) - Daniel Araya Ureta (Ing. Soft. BCI): Se agrega nueva estrategia EtokenMultiBrowser y se agrega documentacion version inicial.</li>
     * <li> 1.2 10/07/2017 Luis L�pez Alamos (SEnTRA) - Ricardo Carrasco C�ceres (Ing. Soft. BCI): Se agrega validator para Estrategia EntrustToken y EntrustSoftToken.</li>
     * </ul>
     * <p>
     * 
     * @param estadoSegundaClave estado de segunda clave.
     * @return Validator validador.
     * @since 1.0
     */
    public static Validator crear(EstadoSegundaClaveTO estadoSegundaClave) {

        if (estadoSegundaClave == null) {
        	logger.error("[crear] estadoSegundaClave no puede ser null");
            throw new IllegalArgumentException("estadoSegundaClave no puede ser null");
        }
        
        if (SegundaClaveEnum.ENTRUST_SOFTTOKEN.equals(estadoSegundaClave.getSegundaClaveEnum())) {
            return new SegundaClaveEntrustSoftTokenValidator();
        } 
        else if (SegundaClaveEnum.ENTRUST_TOKEN.equals(estadoSegundaClave.getSegundaClaveEnum())) {
            return new SegundaClaveEntrustTokenValidator();
        } 
        else if (SegundaClaveEnum.MULTIPASS.equals(estadoSegundaClave.getSegundaClaveEnum())) {
            return new SegundaClaveMultipassValidator();
        } 
        else if (SegundaClaveEnum.SAFESIGNER.equals(estadoSegundaClave.getSegundaClaveEnum())) {
            return new SegundaClaveSafeSignerValidator();
        } 
        else if (SegundaClaveEnum.INTERNET.equals(estadoSegundaClave.getSegundaClaveEnum())) {
            return new SegundaClaveInternetValidator();
        } 
        else if (SegundaClaveEnum.ETOKEN.equals(estadoSegundaClave.getSegundaClaveEnum())) {
            return new SegundaClaveETokenValidator();
        } 
        else if (SegundaClaveEnum.ETOKENMULTIBROWSER.equals(estadoSegundaClave.getSegundaClaveEnum())) {
            return new SegundaClaveETokenMultiBrowserValidator();
        } 
        else {
        	logger.error("[crear] ["+estadoSegundaClave+"] Tipo de segunda clave no implementado");
            throw new UnsupportedOperationException(
                    "Tipo de segunda clave no implementado: " 
                    + estadoSegundaClave.getSegundaClaveEnum().getNombre());
        }
    }
}

