var intervaloConsultaTransaccion = null;
var estadoConsulta = null;
var valorInicial = 0;
var repeticionesMaximas = 0;
var tiempoDeConsulta = 0;

/*Se utiliza cuando se carga la pagina de softoken-online*/
function inicializaValores(pRepMax, pTpoCon){
  repeticionesMaximas = pRepMax;
  tiempoDeConsulta = pTpoCon;
  if (intervaloConsultaTransaccion == null){
    intervaloConsultaTransaccion = setInterval(function(){consultaTransaccion();}, tiempoDeConsulta)
  }
}

function limpiaIntervalo(){
  clearInterval(intervaloConsultaTransaccion);
}

function consultaTransaccion(){
  var form = document.getElementById("form");
  consultaTransaccionAjax(form);
}

function consultaTransaccionGenerico(form){
  consultaTransaccionAjax(form);
}

function consultaTransaccionAjax(form){
  $.ajax({ 
    type: "POST",
    dataType: "text",
    url: "/seguridadwls/segundaclave/ConsultarEstadoDesafioSegundaClave.do?metodo=consultarEstadoDesafioOperacion",
    success: function(data){
      estadoConsulta = data;
    }
  });
  
  if (estadoConsulta == 'CONFIRM'){
    clearInterval(intervaloConsultaTransaccion);
    form.submit();
  }
  
  valorInicial++;
  if ((estadoConsulta == 'CANCEL' ) || (estadoConsulta == 'CONCERN' ) || (estadoConsulta == 'TIME_OUT' ) || (valorInicial == repeticionesMaximas)){
    clearInterval(intervaloConsultaTransaccion);
    resuelveMensajeRespuesta();
  }
}

function obtenerDesafioOnline(){
  var form = document.getElementById("form");
  $.ajax({ 
    type: "POST",
    dataType: "text",
    url: "/seguridadwls/segundaclave/ConsultarEstadoDesafioSegundaClave.do?metodo=obtenerDesafioOnline",
    success: function(data){
      estadoConsulta = data;
    }
  });
}

function generarQRActionWebPay(){
  var form = document.getElementById("form");
  form.submit();
}

function generarQRActionPD(){
  var form = document.getElementById("form");
  form.modoAutenticacion.value="OFFLINE";
  form.submit();
}

function generarQRActionPDMonex(){
  var form = document.getElementById("form");
  form.metodo.value="generarLlaveQR";
  form.submit();
}

/*Se utiliza cuando se usa softoken-online como un div incrustado en otra pagina*/
function inicializaValoresGenerico(pRepMax, pTpoCon, pForm){
  if(pForm == null){
    pForm = document.getElementById("form");
  }
  
  repeticionesMaximas = pRepMax;
  tiempoDeConsulta = pTpoCon;
  if (intervaloConsultaTransaccion == null){
    intervaloConsultaTransaccion = setInterval(function(){consultaTransaccionGenerico(pForm);}, tiempoDeConsulta)
  }
}

/*Se utiliza para cargar la pagina de softoken-offline-qr en un iframe*/
function generarQRActionGenerico(){
  var layerOnline = document.getElementById("layerOnline");
  var layerOffline = document.getElementById("layerOffline");
  var loading_gen = document.getElementById("loading_gen");
  loading_gen.style.display='block';
  layerOffline.style.display='block';
  layerOnline.style.display='none';
  iframe = $('#iframe_offline');
  iframe.attr('src',"/seguridadwls/segundaclave/ConsultarEstadoDesafioSegundaClave.do?metodo=generarLlaveQR");
}

function ocultaImgLoading(){
  var loading_gen = document.getElementById("loading_gen");
  loading_gen.style.display='none';
}

function resize_offline(){
  iframe = $('#iframe_offline');
  if(iframe != null){
    alto = iframe.contents().find("html").height() + 45;
    iframe.height(alto);
  }
}

function resize_iframeContenido(){
  iframe = window.parent.document.getElementById('iframeContenido');
  if(iframe != null){
    alto = parseInt(iframe.style.height.replace(/px$/,""), 0) + 110;
    iframe.style.height = alto + 'px';
  }
}

function resuelveMensajeRespuesta(){
  var layerOnline = document.getElementById("layerOnline");
  var errorLayerOnline = document.getElementById("errorLayerOnline");
  if(layerOnline != null && errorLayerOnline != null){
    layerOnline.style.display = 'none';
    errorLayerOnline.style.display = 'block';
  }
}
