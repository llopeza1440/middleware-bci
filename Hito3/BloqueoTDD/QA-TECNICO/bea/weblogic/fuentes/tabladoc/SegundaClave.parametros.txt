Esta tabla contiene los valores de diferentes parametros a usarse en Segunda Clave

PERSONA: Es Bci-Personas.
Jerarquias: Son las jerarquias de segunda clave.
EMPRESA: Es Bci-Empresas.
INTRANET: Es Bci-Intranet.
ACCESIBLE: Es Bci-Accesible.

recargacelular: Codigo de operacion para recarga celular.
CargaTarjetaBip: Codigo de operacion para recarga tarjeta bip.
Transferencia: Codigo de operacion para tranaferencia de fondos.
PagoCuenta: Codigo de operacion para pago de cuenta.
TransferenciasMxUnoAUno: Codigo de operacion para transferencia moneda uno a uno.

GestorTransferencias: Codigo para transferencia de fondos.
TransferenciasProgramadasActivar: Codigo activar transferencia programadas.
TransferenciasProgramadasDesactivar: Codigo Desactivar transferencia programadas.
TransferenciasProgramadasEliminar: Codigo Elimnar transferencia programadas.

SolicitudProrrogaFactoring: Codigo para Prorroga de documentos

AumentoCupoLSGMovil: Codigo para aumento de cupo m�vil.

TarjetaVirtualMovil: Codigo para tarjeta virtual m�vil.

FirmaMovilPNOL: C�digo para Firma m�vil PNOL.

AutorizacionBeneficiariosMovil: C�digo para Firma m�vil destinatarios PYME.

Se agregan llaves para Solicitud Cr�dito Hipotecario en Viaje CHIP SolicitudCreditoHipotecario:
ActualizarRentaPrevired, CondicionSeguro y DpsViajeCHIP.

Se agrega llave Safesigner en ActivarTDC. 

ResolucionConvenioDxP : Codigos para Convenios D x P

Articulo85: Se agrega llave para clave de acceso multipass.

CurseCreditoComercial: c�digo para clave de cr�dito comercial.

[REGISTRODEVERSIONES]
==========================================
 
1.  30/04/2013 Victor Hugo Enero (ORAND): version inicial.
2.  30/10/2013 Victor Agurto Osorio (Imagemaker): agregar TransferenciasMxUnoAUno.
3.  13/11/2013 Pedro Carmona Escobar (SEnTRA): Se define la jerarqu�a para dominio EMPRESA. Adem�s, se define
                                    la jerarquia de SegundasClaves particular para el servicio AutorizacionNominaDeCheques.
4.  13/01/2014 Rodrigo Briones Ortega (BCI): Se elimina jerarqu�a Safesigner.
5.  31/08/2013 Christian Cu�llar B. (TINet): Se agregan las claves 
GestorTransferencias,TransferenciasProgramadasActivar,TransferenciasProgramadasDesactivar,
TransferenciasProgramadasEliminar estas representan nuevas claves para transferencias de fondos.
6.  16/01/2014 Luis Silva (TINet): Se agregan las claves
    AutorizacionPagoImpuestosEmpresaMovil, AutorizacionMultiempresaPagosMovil,
    FirmaMultiempresaPagosMovil y FirmaMultiempresaPagosMovilCCT.
7.  02/04/2014 Pedro Quintanilla Q. (ADA Ltda.): Se agregan las claves SolicitudProrrogaFactoring.    
8.  16-04-2014 Rafael Pizarro (TINet): Se agrega la clave TransferenciasEmpresasMovilCuentasPropias.
9.  04/07/2014 Ignacio Gonz�lez (TInet): Se agrega la clave AumentoCupoLSGMovil.
10. 21/07/2014 Ignacio Gonz�lez (TINet): Se agrega la clave TarjetaVirtualMovil.

11. 28/08/2014 Pedro Carmona Escobar (SEnTRA): Se define nueva llave 'FirmaLbtr'
12. 08/10/2014 Rodolfo Kafack Ghinelli (SEnTRA): Se define nueva llave CambioClavePrimeraVez.
13. 05/12/2014 Pablo romero (SEnTRA): Se define nuevas llaves para Pnol AutorizacionBeneficiarios,ActivacionNominaBeneficiarios,FirmaNominasEnLinea,EliminaPNOL.
14. 14/11/2014 Ignacio Gonz�lez (TINet): Se agrega la clave 'PaseDiarioMovil'.
15. 03/01/2015 Eric Mart�nez (TINet): Se define nueva llave para PNOL FirmaMovilPNOL.
16. 09/01/2015 Oscar Nahuelpan(SEnTRA): Se define nueva llave 'FirmarSeguroAutomotriz'.
17. 18/02/2015 Andr�s Mor�n Ortiz (SEnTRA): Se modifica jerarquia de servicio FirmaLbtr.
18. 16/05/2015 Carlos Cerda I. (Kubos): version inicial.
19. 09/04/2015 Felipe Briones (SEnTRA): Se agrega la llave 'FirmarPagoSoma'.
20. 17/03/2015 Felipe Briones M. (SEnTRA): Se define nueva llave AumentoCupoLSGPyme.19. 13/05/2015 Oscar Nahuelan. (SEnTRA): Se agrega AumentoCupoLSGPyme para Integraci�n/Certificaci�n
21. 06/05/2015 Luis L�pez Alamos(SEnTRA) - Claudia L�pez P (ing Soft. BCI): Se define nueva llave 'FirmarNominaCuentaPrima'.
22. 17/03/2015 Felipe Briones M. (SEnTRA): Se define nueva llave AumentoCupoLSGPyme.
23. 13/05/2015 Oscar Nahuelan. (SEnTRA): Se agrega AumentoCupoLSGPyme para Integraci�n/Certificaci�n
24. 03/08/2015 Sergio Cuevas Diaz. (SEnTRA) - Ma. Jose Romero (Ing. Soft. BCI): Se agrega llave FirmaTransferenciasAltosMontos.
25. 13/08/2015 Paola Avalos (BCI): Se elimina la clave Internet para Firma de echeck
26. 18/08/2015 Jorge Lara (BCI): Se agrega la clave eToken para AutorizacionBeneficiarios y ActivacionNominaBeneficiarios
27. 20/08/2015 Manuel Esc�rate (BEE): Se agregan las siguientes llaves AvanceMultilinea, FirmarMultilinea, RenovacionMultilinea
28. 10/07/2015 Luis Silva (TINet) Se agrega clave
29. 05/05/2015 Jaime Suazo(SEnTRA) - Patricia Guzman (Ing. Soft. BCI): Se define nueva llave 'FirmaAutorizacionMtoMaxTtffDia'.
30. 24/08/2015 Victor Caroca. (SEnTRA) - Felipe Poblete (ing. Soft. BCI): Se agrega el Campo.
31. 30/01/2015 Ignacio Gonz�lez(TINet) - Robinson Hidalgo (Ing. Soft. BCI): Se define nueva llave 'AutorizacionBeneficiariosMovil'
32. 10/03/2016 Carlos Cerda I. (Kubos) - Pablo Rompentin (Ing. Soft. BCI): Se agrega llave: 'EnrolamientoPagameIndustria'.
33. 29/03/2016 Angelo Vel�squez V (SEnTRA) - Jorge Lara D�az (Ing. Soft. BCI) : Se agrega llave 'AutorizacionNominasPAC'.
34. 26/05/2016 Germ�n Pizarro (SEnTRA) - Alexis Per�z (Ing. Soft. BCI): se agrega el codigo FirmarAumentoCupo			
35. 09/08/2016 Gonzalo Cofr� G. (SEnTRA) - Daniel Araya (Ing. Soft. BCI): Se agregan llaves para proyecto de transferencias entre cuentas propias empresa y pyme.
36  30/09/2016 Gonzalo Villagr�n (SEnTRA) - Jos� Palma (TInet) - Francisco Gonz�lez (SEnTra) - Robinson Hidalgo (Ing. Soft. BCI) : Se agregan llaves para solicitud de cr�dito hipotecario, viaje CHIP.
37. 24/08/2016 Victor Enero. (Orand) - Pablo Rompentin (Ing. Soft. BCI): Se agrega llave: 'canjedepuntos'.
38. 26/01/2017 Pamela Inostroza M (BCI): Se agrega en ActivarTDC como primera jerarquia Safesigner, y se mantiene multipass como segunda jerarquia.
39. 03/02/2017 Rodrigo Baeza O. (IAT Ltda) - Mauricio Monsalve (Ing. Soft. BCI): Se agregan variable de para el manejo de la segunda clave convenio DxP BciNova.
40. 16/01/2017 Manuel Esc�rate (BEE) - Felipe Ojeda (Ing. Soft. BCI): Se agrega llave Articulo85.
41. 13/02/2017 Luis Silva (TINet) - Juan Bustos (Ing. SOft. BCI): Se agrega servicio ActivacionNotificaciones,
    adem�s de configurarlo para poder obtener el m�todo de segunda clave v�a servicio REST.
42. 17/05/2017 Paola Avalos(BCI) - Se agrega eToken a la Firma de N�minas en Linea.
43. 10/07/2017 Luis L�pez Alamos (SEnTRA) - Ricardo Carrasco C�ceres (Ing. Soft. BCI): Se agregan nuevas llaves de jerarquia EntrustSoftToken y EntrustToken.
44. 06/09/2017 Andr�s Silva H. (SEnTRA) - Ricardo Carrasco C�ceres (Ing. Soft. BCI): Se agregan nuevas llaves de jerarquia EntrustSoftToken y EntrustToken para los servicios:ActualizacionMisDatos
45. 23/10/2017 Andr�s Silva H. (SEnTRA) - Ricardo Carrasco C�ceres (Ing. Soft. BCI): Se agregan nuevas llaves de jerarquia EntrustSoftToken y EntrustToken para el servicio:SeguridadTarjetaDebito
==========================================
