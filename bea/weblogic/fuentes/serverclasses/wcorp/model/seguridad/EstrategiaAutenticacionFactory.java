package wcorp.model.seguridad;

import wcorp.serv.seguridad.SeguridadException;

/**
 * 
 * <b>EstrategiaAutenticacionFactory</b>
 * <p>
 * Estructura para safesigner que permite instanciar estrategia Multipass o SafeSigner. 
 * </p>
 * 
 * Registro de versiones:<ul> 
 * 
 * <li> 1.0 (05/06/2013, Victor Hugo Enero (Orand)): versi�n inicial</li>
 * <li> 1.1 02/09/2013 Pablo Romero C�ceres (SEnTRA): Se modifica m�todo 
 *                                                    {@link #obtenerEstrategiaSegundaClave} para agregar retorno 
 *                                                    de la nueva estrategia EToken</li>
 * <li> 1.2 12/12/2016 Darlyn Delgado P�rez (SEnTRA) - Daniel Araya Ureta (Ing. Soft. BCI): Se modifica m�todo {@link #obtenerEstrategiaSegundaClave(PrioridadSegundaClaveIdTO)}.</li>
 * <li> 1.3 10/07/2017 Luis L�pez Alamos (SEnTRA) - Ricardo Carrasco C�ceres (Ing. Soft. BCI): Se modifica m�todo {@link #obtenerEstrategiaSegundaClave(PrioridadSegundaClaveIdTO)}.</li>
 * </ul>
 * 
 * <p><b>Todos los derechos reservados por Banco de Cr�dito e Inversiones.</b></p>
 * 
 */
public class EstrategiaAutenticacionFactory {

    /**
     * Obtiene estrategia de segunda clave.
     * <p>
     * <b>Registro de versiones:</b>
     * <ul>  
     * <li> 1.0 xx/xx/xxxx Desconocido - Desconocido (Ing. Soft. BCI): Versi�n inicial.</li>
     * <li> 1.1 12/12/2016 Darlyn Delgado (SEnTRA) - Daniel Araya Ureta (Ing. Soft. BCI): Se agrega nueva estrategia EtokenMultiBrowser y se agrega documentacion version inicial.</li>
     * <li> 1.2 10/07/2017 Luis L�pez Alamos (SEnTRA) - Ricardo Carrasco C�ceres (Ing. Soft. BCI): Se agregan nuevas estrategias <code>ENTRUST_SOFTTOKEN</code> y <code>ENTRUST_TOKEN</code> </li>
     * </ul>
     * <p> 
     * @param segundaClaveId id de segunda clave.
     * @return EstrategiaSegundaClave estrategia de segunda clave.
     * @throws SeguridadException excepcion de seguridad.
     * @since 1.0
     */
    public static EstrategiaSegundaClave obtenerEstrategiaSegundaClave(PrioridadSegundaClaveIdTO segundaClaveId)
        throws SeguridadException {
        
        if (PrioridadSegundaClaveIdTO.ENTRUST_SOFTTOKEN.equals(segundaClaveId)) {
            return new EstrategiaEntrustSoftToken();
        }
        else if (PrioridadSegundaClaveIdTO.ENTRUST_TOKEN.equals(segundaClaveId)) {
            return new EstrategiaEntrustToken();
        }
        else if (PrioridadSegundaClaveIdTO.SAFESIGNER.equals(segundaClaveId)) {
            return new EstrategiaSafeSigner();
        }
        else if (PrioridadSegundaClaveIdTO.MULTIPASS.equals(segundaClaveId)) {
            return new EstrategiaMultipass();
        }
        else if (PrioridadSegundaClaveIdTO.INTERNET.equals(segundaClaveId)) {
            return new EstrategiaClaveInternet();
        }
        else if (PrioridadSegundaClaveIdTO.ETOKEN.equals(segundaClaveId)) {
            return new EstrategiaEToken();
        }
        else if (PrioridadSegundaClaveIdTO.ETOKENMULTIBROWSER.equals(segundaClaveId)) {
            return new EstrategiaEtokenMultiBrowser();
        }
        else {
            throw new UnsupportedOperationException("Segunda clave no soportada: " + segundaClaveId.getNombre());
        }
    }
}

