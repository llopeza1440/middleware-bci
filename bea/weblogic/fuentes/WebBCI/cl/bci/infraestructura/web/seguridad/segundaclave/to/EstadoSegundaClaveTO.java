package cl.bci.infraestructura.web.seguridad.segundaclave.to;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;


import cl.bci.infraestructura.web.seguridad.segundaclave.to.TokenEntrustTO;


/**
 * 
 * <b>EstadoSegundaClaveTO</b>
 * <p>
 * Estructura para safesigner que permite obtener el estatus de safesigner
 * para un cliente del banco. 
 * </p>
 * 
 * Registro de versiones:<ul> 
 * 
 * <li> 1.0 (05/06/2013, Victor Hugo Enero (Orand)): versi�n inicial</li>
 * <li>1.1 23/06/2014, Venancio Aranguiz (SEnTRA) : Se agrega la propiedad
 * metodosAutenticacion.</li>
 * <li>1.2 22/07/2016 Felipe Briones M. (SEnTRA) - Victor Ortiz (Ing. Soft. BCI): Se agregan las constantes: {@link #CODIGO_AUTORIZADO},
 * {@link #CODIGO_ACTIVO}. {@link #CODIGO_BLOQUEADO},
 * {@link #CODIGO_ACTIVO_EXCESO_REINTENTOS}, {@link #CODIGO_VALOR_INVALIDO} y
 * {@link #CODIGO_ASIGNADO}. Se agregan los atributos: {@link #tipoSegundaClave}, {@link #serial}, {@link #fechaInicio}, {@link #modo}, {@link #claveNueva},
 * {@link #codigoCambio} {@link #fechaModificacion} con sus correspondientes m�todos get y set. Se agrega el metodo
 * {@link #codigoEstadoToken()}. Por �ltimo se modifica m�todo {@link #toString()} para reflejar la incorporaci�n de atributos.</li>
 * <li>1.3 10/07/2017 Jaime Suazo. (SEnTRA) - Ricardo Carrasco C�ceres (Ing. Soft. BCI): Por proyecto cambio plataforma autenticacion 
 *         se agrega atributo {@link #listaDispositivos} con sus respectivos m�todos setter y getter. Se modifica {@link #toString()}</li>
 * </ul>
 * 
 * <p><b>Todos los derechos reservados por Banco de Cr�dito e Inversiones.</b></p>
 * 
 */

public class EstadoSegundaClaveTO implements Serializable{

    /**
     * serial UID.
     */
    private static final long serialVersionUID = 1L;
    
    /**
     * Codigo autorizado.
    */
    private static final int CODIGO_AUTORIZADO = 2;

    /**
     * Codigo activo.
     */
    private static final int CODIGO_ACTIVO = 3;

    /**
     * Codigo Bloqueado.
     */
    private static final int CODIGO_BLOQUEADO = 4;

    /**
     * Codigo Exceso de reintentos.
     */
    private static final int CODIGO_ACTIVO_EXCESO_REINTENTOS = 5;

    /**
     * Codigo para valor invalido.
     */
    private static final int CODIGO_VALOR_INVALIDO = -1;

    /**
     * Codigo clave asignada.
     */
    private static final int CODIGO_ASIGNADO = 1;
    
    /**
     * Id Codigo de Estado.
     */
    private int idCodigoEstado;
    
    /**
     * Estatus OK o NOK.
     */
    private boolean estatus;
    
    /**
     * Glosa del estado.
     */
    private String glosa;
    
    /**
     * Informacion Anexa.
     */
    private String informacionAnexa;
    
    /**
     * Tipo de segunda clave.
     */
    private SegundaClaveEnum segundaClaveEnum;
    
    /**
     * Metodos de autenticacion.
     */
    private Hashtable metodosAutenticacion;
    
    /**
     * Codigo que indica el tipo de segunda clave SafeSigner - Multipass.
     */
    private String tipoSegundaClave;

    /**
     * Serial de la segunda clave si es que posee numero ingresado. Este valor equivale al dato EstadoToken.serial_number.
     */
    private String serial;
    
    /**
     * Fecha de inicio dia, mes, a�o. Este valor equivale al dato EstadoToken.token_assigned_date.
     */
    private Date fechaInicio;
    
    /**
     * Modo de la segunda clave con el valor true o false. Este valor equivale al dato EstadoToken.newPin.
     */
    private boolean modo;
    
    /**
     * Clave nueva en donde se permite ingreso solo numeros. Este valor equivale a dato EstadoToken.estatusPin.
     */
    private int claveNueva;
    
    /**
     * Codigo cambio de clave que permite ingreso solo numeros. Este valor equivale a dato EstadoToken.nextCodeStatus.
     */
    private int codigoCambio;
    
    /**
     * Atributo que almacena una lista de dispositivos del tipo TokenEntrustTO.
     */
    private TokenEntrustTO[] listaDispositivos;
    
    /**
     * Fecha de modificacion dia, mes, a�o.
     */
    private Date fechaModificacion;
    
    public int getIdCodigoEstado() {
        return idCodigoEstado;
    }

    public void setIdCodigoEstado(int idCodigoEstado) {
        this.idCodigoEstado = idCodigoEstado;
    }

    public boolean isEstatus() {
        return estatus;
    }

    public void setEstatus(boolean estatus) {
        this.estatus = estatus;
    }

    public String getGlosa() {
        return glosa;
    }

    public void setGlosa(String glosa) {
        this.glosa = glosa;
    }

    public String getInformacionAnexa() {
        return informacionAnexa;
    }

    public void setInformacionAnexa(String informacionAnexa) {
        this.informacionAnexa = informacionAnexa;
    }

    public SegundaClaveEnum getSegundaClaveEnum() {
        return segundaClaveEnum;
    }

    public void setSegundaClaveEnum(SegundaClaveEnum segundaClaveEnum) {
        this.segundaClaveEnum = segundaClaveEnum;
    }

    public Hashtable getMetodosAutenticacion() {
        return metodosAutenticacion;
    }
    
    public void setMetodosAutenticacion(Hashtable metodosAutenticacion) {
        this.metodosAutenticacion = metodosAutenticacion;
    }
    
    public String getTipoSegundaClave() {
        return tipoSegundaClave;
    }

    public void setTipoSegundaClave(String tipoSegundaClave) {
        this.tipoSegundaClave = tipoSegundaClave;
    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public boolean isModo() {
        return modo;
    }

    public void setModo(boolean modo) {
        this.modo = modo;
    }

    public int getClaveNueva() {
        return claveNueva;
    }

    public void setClaveNueva(int claveNueva) {
        this.claveNueva = claveNueva;
    }

    public int getCodigoCambio() {
        return codigoCambio;
    }

    public void setCodigoCambio(int codigoCambio) {
        this.codigoCambio = codigoCambio;
    }

    public Date getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }
    
    /**
     * Retorna el c�digo de estado de la segunda clave.
     * Este metodo tiene las mismas reglas que wcorp.serv.seguridad.EstadoToken.
     * Registro de versiones:
     * <ul>
     * <li>1.0 22/07/2016 Felipe Briones M. (SEnTRA) - Victor Ortiz (Ing. Soft. BCI): versi�n inicial.</li>
     * </ul>
     * <p>
     * @return int C�digo de estado de la segunda clave.
     * @since 1.2
     */
    public int codigoEstadoToken() {
        /*
         * Token asignado al usuario pero no tiene pin y est� bloqueado.
         */
        if ( ( !estatus ) && modo && ( claveNueva == 0 ) && ( codigoCambio == 0 ) ) {
            return CODIGO_ASIGNADO;
        }
        /*
         * Condiciones de TOKEN AUTORIZADO No puede autentificarse porque el
         * token est� bloqueado pero PIN est� asignado
         */
        if ( estatus && modo && ( claveNueva == 0 ) && ( codigoCambio == 0 ) ) {
            return CODIGO_AUTORIZADO;
        }
        /*
         * Condiciones de TOKEN ACTIVO, Puede autentificarse, sincronizarse y
         * todas las operaciones
         */
        if ( estatus && ( !modo ) && ( claveNueva == 1 ) && ( codigoCambio == 0 ) ) {
            return CODIGO_ACTIVO;
        }
        /*
         * Condiciones de TOKEN BLOQUEADO
         */
        if ( ( !estatus ) && ( !modo ) && ( claveNueva == 1 ) && ( codigoCambio == 0 ) ) {
            return CODIGO_BLOQUEADO;
        }
        /*
         * Condiciones de TOKEN Habilitado pero que ha excedido el n�mero de
         * intentos fallidos
         */
        if ( estatus && ( !modo ) && ( claveNueva == 1 ) && ( codigoCambio == 1 ) ) {
            return CODIGO_ACTIVO_EXCESO_REINTENTOS;
        }
        /*
         * Token "inexistente" netamente ocupado para mantener la "firma" del m�todo original.
         */
        if ( ( !estatus ) && ( !modo ) && ( claveNueva == 0 ) && ( codigoCambio == 0 ) ) {
            return CODIGO_VALOR_INVALIDO;
        }
        return CODIGO_VALOR_INVALIDO;
    }
    
    public TokenEntrustTO[] getListaDispositivos() {
        return listaDispositivos;
    }

    public void setListaDispositivos(TokenEntrustTO[] listaDispositivos) {
        this.listaDispositivos = listaDispositivos;
    }
    

    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append("EstadoSegundaClaveTO [idCodigoEstado=");
        sb.append(idCodigoEstado);
        sb.append(", estatus=");
        sb.append(estatus);
        sb.append(", glosa=");
        sb.append(glosa);
        sb.append(", informacionAnexa=");
        sb.append(informacionAnexa);
        sb.append(", segundaClaveEnum=");
        sb.append(segundaClaveEnum);
        sb.append(", metodosAutenticacion=");
        sb.append(metodosAutenticacion);
        sb.append(", tipoSegundaClave=");
        sb.append(tipoSegundaClave);
        sb.append(", serial=");
        sb.append(serial);
        sb.append(", fechaInicio=");
        sb.append(fechaInicio);
        sb.append(", modo=");
        sb.append(modo);
        sb.append(", claveNueva=");
        sb.append(claveNueva);
        sb.append(", codigoCambio=");
        sb.append(codigoCambio);
        sb.append(", fechaModificacion=");
        sb.append(fechaModificacion);
        sb.append(", listaDispositivos=");
        sb.append(listaDispositivos == null ? "null": listaDispositivos.toString());
        sb.append("]");
        return sb.toString();
    }
    
    
    
}
