<%@ page import="wcorp.util.TablaValores"%>
<%@ page import="wcorp.util.HTMLUtil"%>
<%@ taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles" %>
<%@ taglib uri="/WEB-INF/struts-nested.tld" prefix="nested" %>
<%@ taglib uri="/WEB-INF/struts-template.tld" prefix="template" %>
<%@taglib prefix="html"  uri="/WEB-INF/struts-html.tld"%>
<%@taglib prefix="bean"  uri="/WEB-INF/struts-bean.tld"%>
<%@taglib prefix="logic" uri="/WEB-INF/struts-logic.tld"%>
<%@taglib prefix="fmt"	 uri="http://java.sun.com/jstl/fmt"%>
<%@taglib prefix="c"	 uri="http://java.sun.com/jstl/core"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<fmt:setLocale value="es-cl"/>
<html:html>
<head>
<title>bip!</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link href="style/definitiva.css" rel="stylesheet" type="text/css" />
<script src="scripts/common.js" type="text/javascript"></script>
<script src="scripts/cuentas.js" type="text/javascript"></script>
<script src="scripts/mochikit.js" type="text/javascript"></script>
<script src="scripts/ui.js" type="text/javascript"></script>
<script src="scripts/funciones.js" type="text/javascript"></script>
<script src="scripts/New.js" type="text/javascript"></script>
<script src="scripts/fat.js" type="text/javascript"></script>
<script src="scripts/funcionesRecargaTransantiago.js" type="text/javascript"></script>


<script>
var cuentas=new Array();
var mailTarjetas=new Array();
<c:forEach var="cuenta" items="${cuentas}">
     var unaCuenta=new Cuenta('<c:out value="${cuenta.numeroCuenta}"/>','<c:out value="${cuenta.descripcionCuenta}"/>')
     unaCuenta.setSaldoCuenta('<fmt:formatNumber value="${cuenta.saldoDisponible}" pattern="#,##0"/>')
         unaCuenta.setSaldoSobreGiro('<fmt:formatNumber value="${cuenta.sobregiroDisponible}" pattern="#,##0"/>')
         unaCuenta.setTotalDisponible('<fmt:formatNumber value="${cuenta.saldoDisponible + cuenta.sobregiroDisponible}" pattern="#,##0"/>')
         unaCuenta.setSaldoCuentaNumber('<c:out value="${cuenta.saldoDisponible}" />')
         unaCuenta.setSaldoSobreGiroNumber('<c:out value="${cuenta.sobregiroDisponible}" />')
         unaCuenta.setTotalDisponibleNumber('<c:out value="${cuenta.saldoDisponible + cuenta.sobregiroDisponible}"/>')
         unaCuenta.setSaldoLineaDeSobregiro('<fmt:formatNumber value="${cuenta.saldoLineaDeSobregiro}" pattern="#,##0"/>')
         unaCuenta.setSaldoLineaDeEmergencia('<fmt:formatNumber value="${cuenta.saldoLineaDeEmergencia}" pattern="#,##0"/>')
         unaCuenta.setTipoCuenta('<c:out value="${cuenta.tipoCuenta}"/>')
        cuentas['<c:out value="${cuenta.numeroCuenta}"/>']=unaCuenta;
</c:forEach>
<logic:present name="tarjetas">
<logic:iterate id="unaTarjeta" name="tarjetas">
      mailTarjetas['<bean:write name="unaTarjeta" property="numeroTarjeta"/>']='<bean:write name="unaTarjeta" property="mail"/>'
  </logic:iterate>
</logic:present>
  <logic:notPresent name="tarjetas">
                    mailTarjetas['']=''
  </logic:notPresent>
  function limpiar(){
   document.recargaTarjetaBipForm.entradaTarjeta1.value="";
   document.recargaTarjetaBipForm.entradaTarjeta2.value="";
   document.recargaTarjetaBipForm.guardaComo.value="";

  }
function algunaEspecifica(){

<logic:present name="numTaj">
   <logic:notEmpty name="numTaj">
   try{
     var numeroTarjeta='<bean:write name="numTaj"/>'
     var ind=0;
     var index=0;
     for (i=0;i<document.recargaTarjetaBipForm.selectTarjea.options.length;i++)
     if (parseInt(document.recargaTarjetaBipForm.selectTarjea.options[i].value)==parseInt(numeroTarjeta))
     index=i;
     document.recargaTarjetaBipForm.selectTarjea.selectedIndex=index
   }catch(err){}
     </logic:notEmpty>
  </logic:present>
}
<%
	String mensaje=(TablaValores.getValor("LineaEmergencia.parametros","110-TTFF","mensaje")!=null ? TablaValores.getValor("LineaEmergencia.parametros","110-TTFF","mensaje"):TablaValores.getValor("LineaEmergencia.parametros","110","mensaje"));
	mensaje=HTMLUtil.codificaEntidades(mensaje);
	String mensaje1 = TablaValores.getValor("transantiago.parametros","inicio","desc");
	String mensaje2 = TablaValores.getValor("transantiago.parametros","inicio2","desc");
 %>

var rangosRestringidos = new Array();
var rango;
<c:if test="${!empty rangoTarjetasRestringidas}">
<c:forEach var="rango" items="${rangoTarjetasRestringidas}" varStatus="status">
     rango = new RangoRestringido(<c:out value="${rango.limiteInferior}"/>, <c:out value="${rango.limiteSuperior}"/>)
     rangosRestringidos[<c:out value="${status.index}"/>] = rango;
</c:forEach>
</c:if>
tarjetasRestringidas = rangosRestringidos
setTarjetasRestringidas.call(tarjetasRestringidas);

</script>
</head>
<style type="text/css">

#mensaje_plano{
    padding-left: 10px;
	vertical-align: top;
	padding: 15px;
	color: #000;
	background: url(/bcinetwls/gestorpagostransferencias/img/ic_alerta.gif) no-repeat 18px 10px #FF9;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	line-height: 100%;
	padding-top: 18px;
	padding-right: 10px;
	padding-bottom: 18px;
	padding-left: 73px;
}

#mensaje_plano a:visited {
	color: #005298;
	text-decoration: none;
}

#mensaje_plano a:link {
	color: #005298;
	text-decoration: none;
}

#mensaje_plano a:hover{
	color: #005298;
	text-decoration: underline;
}

</style>
<body onLoad="algunaEspecifica();seleccionaTarjeta();initPagina();muestraSaldo();">
 <html:form action="/confirmaRecargaTarjetaBip">
 <input type="hidden" name="metodo"/>
 <input type="hidden" name="validaSegundaClave" value="true"/>
 <html:hidden property="numeroTarjeta"/>
 <html:hidden property="nombreCuentaCte"/>
 <html:hidden property="saldoCuentaCte"/>
 <html:hidden property="saldoSobreGiroCte"/>
 <html:hidden property="saldoDisponibleCte"/>
 <html:hidden property="aliasTarjeta"/>
 <html:hidden property="tipoCuenta"/>
 <html:hidden property="rutCliente"/>
 <html:hidden property="digitoVerificadorCliente"/>
 <html:hidden property="mailClienteCuenta"/>
 <html:hidden property="banco"/>
 <html:hidden property="email"/>
 <html:hidden property="cuenta"/>
 <html:hidden property="periodoPrograma"/>




<table width="100%" border="0" cellspacing="0" cellpadding="0" id="contenidos">
  <tr>
    <td width="1030" id="tdcontenido">
    <h1>Tarjeta  bip! </h1>
    <table width="280" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td><h2>Cargar Tarjeta  </h2></td>        
        </tr>        
    </table>
	<div class="mensaje_info">
		<table>
			<tr>
				<td align="justify" style="font:normal 12px Arial, Helvetica, sans-serif;">
					<bean:write name="mensaje_info_1" /><br>
					<bean:write name="mensaje_info_2" />
					<a  class="Gnegro_2" href="/bcinetwls/transantiago/mas_info.html">M�s informaci�n</a> 
				</td>
			</tr>
		</table>
	</div>
	<div id="mapitas"><strong>1. Ingresar Datos</strong> > 2. Confirmar &gt; 3. Comprobante </div>
	<table border="0" cellpadding="0" cellspacing="0" id="borde">
      <tr>
        <th width="6"><img src="images/curvaizquierdaarriba.gif" width="6" height="6" /></th>
        <td id="bordearribatabla"></td>
        <td width="6" height="6" align="right"><img src="images/curvaderechaarriba.gif" width="6" height="6" /></td>
      </tr>
      <tr>
        <td width="6" height="6" id="bordeizqmorado">&nbsp;</td>
        <th class="morado"></th>
        <td id="bordederechomorado">&nbsp;</td>
      </tr>
      <tr>
        <td id="tdbordeizq">&nbsp;</td>
        <td class="centrotablaformularios"><table border="0" cellpadding="0" cellspacing="0" id="datostabuladosformularios" >

              <tr  >
                <td colspan="2" class="titulosdatos">Cuenta de origen </td>
              </tr>

              <tr  >
                <td width="22%" align="right" class="nombredatox_l" style="text-align:right"><strong>Cargar desde la cuenta : </strong></td>
                <td width="45%" align="left" class="nombredatox_l"><select name="cuentaSelect" onChange="muestraSaldo();">
                  <c:forEach var="cuenta" items="${cuentas}">
                    <option value="<c:out value='${cuenta.numeroCuenta}'/>">
                      <c:out value="${cuenta.descripcionCuenta}"/>
                      </option>
                  </c:forEach>
                </select><label style="color:#FF0000" id="idCuentas"></label></td>
              </tr>
              <tr >
                <td align="right" class="nombredatox_l" style="text-align:right"><strong>Saldo actual en cuenta : </strong></td>
                <td align="left" class="nombredatox_l">$<span id="nn">
                  <label id="saldo_cuenta"></label></span></td>
              </tr>
              <tr style="display:none;">
                <td align="right" class="nombredatox_l" style="text-align:right"><strong>Saldo disponible en l&iacute;nea de sobregiro : </strong></td>
                <td align="left" class="nombredatox_l">$<span id="vv"><label id="saldo_sobregiro"></label><span class="destacadochico"></label><span class="destacadochico" style="vertical-align:top;margin-left:10px;margin-top:0px"><%=mensaje %></span></td>
              </tr>
              <tr >
                <td align="right" class="nombredatox_l" style="text-align:right"><strong>Saldo disponible en l&iacute;nea de sobregiro : </strong></td>
                <td align="left" class="nombredatox_l">$<span id="vv"><label id="saldoLineaDeSobregiro"></label></span></td>
              </tr>
              <tr >
                <td align="right" class="nombredatox_l" style="text-align:right"><strong>Saldo disponible en l&iacute;nea de emergencia : </strong></td>
                <td align="left" class="nombredatox_l">$<span id="vv"><label id="saldoLineaDeEmergencia"></label></span></td>
              </tr>
              <tr >
                <td align="right" class="nombredatox_l" style="text-align:right"><strong>Disponible para cargas :</strong></td>
                <td align="left" class="importante"><strong class="saldo-destacado">&nbsp;$<span id="gg"><label id="saldo_recarga" style="width:inherit"></label></span></strong></td>
              </tr>
              <tr >
                <td rowspan="2" align="right" valign="top" class="nombredatox_l" style="text-align:right"><strong>Monto a cargar($) : </strong></td>
                <td align="left" class="nombredatox_l">
				<INPUT type="text" onKeyPress="JValidaCaracter('Numerico','', true);" onBlur="formatear_monto(this.form);" onFocus="formatear2_monto(this.form);" maxLength="15" size="15" name="monto_temp">
				<html:hidden property="montoCarga" size="15" maxlength="5"/><label style="color:#FF0000"><html:errors  property="montoCarga"/></label>
			<!--	<label id="errorMonto" style="color:#FF0000"/>-->				</td>
              </tr>
              <tr >
                <td>
				<span class="destacadochico" style="vertical-align:top;margin-left:10px;margin-top:0px">El monto permitido de recarga es de $1.000 a $20.000. </span> </br>
				<span class="destacadochico" style="vertical-align:top;margin-left:10px;margin-top:0px">S&oacute;lo se pueden realizar recargas de tarjetas bip emitidas por Bci, Banco Nova, Tbanc y AFT.</span></td>
              </tr>
            </table>
          <table border="0" cellpadding="0" cellspacing="0" id="datostabuladosformularios" >
            <tr >
              <td colspan="2" class="titulosdatos">Destino</td>
            </tr>
            <tr >
              <td width="22%" align="right" class="nombredatox_l" style="text-align:right"><strong>Tarjeta bip! de destino : </strong></td>
              <td width="45%" align="left" class="nombredatox_l"><label>
                <html:radio styleClass="checkboxes" value="0" property="nueva"  onclick="makeInvisible('datos-nueva-cuenta');uiEnableFormElement('cuenta-freq');"/>
                Cargar Tarjeta bip!  </label>
                  <select name="selectTarjea" onChange="seleccionaTarjeta();" >
                   <logic:present name="tarjetas">
                   <logic:iterate id="unaTarjeta" name="tarjetas">
                      <option value="<bean:write name="unaTarjeta" property="numeroTarjeta"/>"><bean:write name='unaTarjeta' property='alias'/></option>
                    </logic:iterate>
                   </logic:present>
                   <logic:notPresent name="tarjetas">
                     <option value="">No tiene Ninguna Tarjeta Registrada</option>
                   </logic:notPresent>
                </select><label style="color:#FF0000"><html:errors  property="numeroTarjeta"/></label></td>
            </tr>
            <tr >
              <td align="right" class="nombredatox_l" style="text-align:right">&nbsp;</td>
              <td align="left" class="nombredatox_l"><html:radio styleClass="checkboxes" value="1" property="nueva" onclick="makeVisible('datos-nueva-cuenta');uiDisableFormElement('cuenta-freq-sel');Fat.fade_element('datos-nueva-cuenta',60,1000);limpiar()"/>
              <label>Cargar nueva Tarjeta bip!</label></td>
            </tr>
            <tr >
              <td colspan="2"><div id="datos-nueva-cuenta" class="invisible" >
				<table width="100%" border="0" cellpadding="0" cellspacing="0" id="cuenta-freq">
					<tr>
						<td width="22%" align="right" class="nombredatox_l" style="text-align:right">N&uacute;mero de Tarjeta bip! :</td>
						<td width="45%" class="nombredatox_l"><html:text  property="entradaTarjeta1" size="15" maxlength="8"/><label style="color:#FF0000"><html:errors  property="entradaTarjeta1"/></label>
						 </td></tr>
					<tr id="imagen" class="invisible">
					  <td align="right" class="nombredatox_l" style="text-align:right">&nbsp;</td>
					  <td class="nombredatox_l"><img name="" src="" width="269" height="97" alt=""></td>
					  </tr>
					<tr>
					  <td align="right" class="nombredatox_l" style="text-align:right"><span class="nombredatox_l" style="text-align:right">Reingreso de n&uacute;mero de Tarjeta bip! :</span></td>
					  <td class="nombredatox_l"><html:text  property="entradaTarjeta2" size="15" maxlength="8"/><label style="color:#FF0000"><html:errors  property="entradaTarjeta2"/></label></td>
					  </tr>
					<tr>
						<td align="right" class="nombredatox_l" style="margin-right:-10px;text-align:right">
						Guardar tarjeta bip! como :<span class="destacadochico"><br>
						(opcional)</span></td>
						<td class="nombredatox_l">
						     <input type="text" maxlength="50" size="30" name="guardaComo" value="<bean:write name='recargaTarjetaBipForm' property='aliasTarjeta'/>"/><label style="color:#FF0000" id="idAlias"><html:errors  property="aliasTarjeta"/></label>
							</td>
					</tr>
					<tr>
						<td class="nombredatox_l">&nbsp;</td>
						<td class="destacadochico" style="vertical-align:top;">Al guardar los datos de esta carga no tendr� que volver a ingresarlos en el futuro</td>
					</tr>
					<tr>
                      <td align="right" class="nombredatox_l" style="text-align:right">Email destinario:
                      <span class="destacadochico"><br>(opcional)</span>
                      </td>
					  <td class="nombredatox_l">
                                                <input type="text" name="emailDeTArjeta" size="30" maxlength="50" value="<bean:write name='recargaTarjetaBipForm' property='email'/>"/>
                                                <label style="color:#FF0000"><html:errors  property="email"/></label></td>
					  </tr>
				</table>
				
			</div></td>
              </tr>
            <tr >
              <td align="right" class="nombredatox_l" style="text-align:right"><strong >Mensaje para destinatario :</strong>
              <span class="destacadochico"><br>(opcional)</span>
              </td>
              <td align="left" class="nombredatox_l"><html:textarea rows="3" property="msgDestino" style="width:90%;" ></html:textarea></td>
            </tr>
          </table>
		  <div class="invisible">
          <table border="0" cellpadding="0" cellspacing="0" id="datostabuladosformularios">
            <tr  >
              <td colspan="2" class="titulosdatos">Programar Carga </td>
            </tr>


            <tr  >
              <td height="25" colspan="2" style="padding-left:10px;">Usted puede <strong>programar una carga </strong>para que se realice con la periodicidad que desee</td>
              </tr>
            <tr >
              <td width="22%" align="right" class="nombredatox_l" style="text-align:right"><strong>&iquest;Desea programar esta carga?</strong> : </td>
              <td width="45%" class="nombredatox_l">
<html:radio styleClass="checkboxes" value="1" property="programaSN" onclick="makeVisible('programacion-transf');llenaDias();"/>
S&iacute;...
&nbsp;&nbsp;

<html:radio styleClass="checkboxes" value="2" property="programaSN"  onclick="makeInvisible('programacion-transf')"/>
No </td>
            </tr>
            <tr >
              <td colspan="2"><div id="programacion-transf" class="invisible">
				<table width="100%" border="0" cellpadding="0" cellspacing="0" >
                  
					<tr>
						<td width="22%" class="nombredatox_l" style="text-align:right">Periodicidad :</td>
						<td width="45%" class="nombredatox_l">
						<html:select property="tipoPrograma" onChange="llenaDias()">
							  <html:option value="2">Semanal</html:option>
							  <html:option value="3">Mensual</html:option>
						    </html:select></td>
					</tr>
					<tr id="diames">
					  <td class="nombredatox_l " style="vertical-align:top;text-align:right" >&nbsp;</td>
					  <td class="nombredatox_l">
					  <select name="dias" id="inicio-dia-1">
                      </select></td>
					  </tr>
					 </table>

				</div></td>
              </tr>
          </table>        </div>  </td>
        <td id="tdbordeder">&nbsp;</td>
      </tr>
      <tr>
        <td><img src="images/curvaabajoizquierda.gif" width="6" height="6"/></td>
        <td id="bordebajotabla"><img src="images/bordeblancoabajo.gif" width="6" height="6" /></td>
        <td><img src="images/curvaabajoderecha.gif" width="6" height="6" /></td>
      </tr>
    </table>
	<table border="0" cellspacing="0" cellpadding="0" id="botonera">
    <tr>
          <td><a href="#" onClick="enviarFormulario()"><img src="images/btn_continuar.gif" width="90" height="21" border="0" title="Continuar" alt="Continuar"  style="margin-left:10px" /></a></td>
        </tr>
  </table>    </td>
  </tr>
</table>
</html:form>
</body>
</html:html>

<logic:notPresent name="tarjetas">
  <script>
      document.recargaTarjetaBipForm.nueva[1].checked=true
      makeVisible('datos-nueva-cuenta');
      uiDisableFormElement('cuenta-freq-sel');
  </script>
</logic:notPresent>
<%--
**************************************************************************************************************
*
* Archivo:          recargaTarjetaPaso1.jsp
*
* Descripci�n:      Pagina Inicial de la recarga de tarjeta bip. 
*
* Path:             /bcinetwls/transantiago
* " Todos los derechos reservados por Banco de Cr�dito e Inversiones."
*
* Historia de cambios
*  versi�n   fecha      autor            		 cambios
*  =======   ========== =======           		============================================================
   1.0       ??			 ????               	 versi�n inicial.
   1.1       21-04-2009 Eva Burgos Leal (SEntra) Se rescata mensaje para indicar qeu saldo L�nea de Sobregiro
   												 incluye saldo de L�nea de Emergencia.
   1.2       03/09/2010 Eduardo Mascayano (TInet)        Se a�ade obtenci�n y validaci�n de tarjetas con restricci�n de recarga en la llamada
                                                         a enviarFormulario() mediante fuciones javascript.
   1.3       06/09/2010 Pablo Carre�o V. (TINet) Se agrega campo que muestra el saldo de la l�nea
	                                             de emergencia, ademas se modifica el campo de
                                                 saldo disponible en l�nea de sobregiro quedando
                                                 separada de la l�nea de emergencia.
   1.4       27/04/2012 Eduardo Mascayano (TINet) Se modifica costo a: 0,18UF (IVA inc.).
   1.5	     28/05/2012	 Fernando Mateluna B. (SEnTRA) Se modifica link solicitud multipass seg�n nuevo menu cenefa de BCI personas.													 
   1.6       04/12/2012 Diego Urrutia Guerra (Imagemaker IT): Se agrega mensaje informativo acerca de la recarga de tarjeta Bip.   
   1.7       05/06/2013 Ver�nica Cestari (ORAND) Se agrega la etiqueta mensaje.inicio y mensaje.inicio2
   												 para mostrar el mensaje de multipass movil.                                               											
*  1.8      30/08/2017  Marcelo Fuentes (SEnTRA) - Ricardo Carrasco (Ing. Soft. BCI) Se elimina mensaje que indicaba que era necesario tener multipass.
***************************************************************************************************************
--%>
